<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	https://codeigniter.com/user_guide/general/routing.htmlleaseDocument
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/
$route['default_controller'] = 'login';
$route['404_override'] = 'utility/does_not_exist';
$route['not_permitted'] = 'utility/not_permitted';

$route['webhook'] = 'subscribe/webhook';
$route['unsubscribe'] = 'subscribe/unsubscribe';
$route['change_plan'] = 'subscribe/change_plan';
$route['change_card'] = 'subscribe/change_card';

//Property dashboard
$route['dashboard/(:any)'] = 'property/property_dashboard/$1';
$route['Allproperty/(:any)'] = 'property/index';



$route['deactivateLease/(:any)'] = 'property/deactivate_lease/$1';

//Route for Step
$route['steps/(:any)/(:any)/(:any)'] = 'property/steps/$1/$2/$3';
$route['steps/(:any)/(:any)'] = 'property/steps/$1/$2';

$route['stepOne'] = 'property/step_one';
$route['stepTwo/(:any)'] = 'property/step_two/$1';
$route['stepTwo'] = 'property/step_two';
$route['stepThree/(:any)'] = 'property/step_three/$1';
$route['stepFour/(:any)'] = 'property/step_four/$1';
$route['stepFive/(:any)/(:any)/(:any)'] = 'property/step_five/$1/$2/$3';
$route['stepSix/(:any)/(:any)'] = 'property/step_six/$1/$2';
$route['insert_property_step_two'] = 'property/insert_property_step_two';
$route['confirmSchedule/(:any)/(:any)'] = 'property/confirmSchedule/$1/$2';
$route['Dashboard/(:any)'] = 'property/property_details/$1';
$route['Dashboard/(:any)/(:any)'] = 'property/property_details/$1/$2';

$route['dashboard_modal_open/(:any)'] = 'property/dashboard_modal_open/$1';
$route['dashboard_modal_shown/(:any)'] = 'property/dashboard_modal_shown/$1';

$route['insert_lease_and_rent_details'] = 'property/insert_lease_and_rent_details';
$route['change_lease_schedule_by_ajax'] = 'property/change_lease_schedule_by_ajax';

//Lease Section
$route['leaseDescription/(:any)'] = 'lease/lease_details/$1';
$route['leaseBonds/(:any)'] = 'lease/all_bonds/$1';
$route['leaseTenants/(:any)'] = 'lease/add_tenants/$1';
$route['leaseDocument/(:any)'] = 'lease/all_lease_doc/$1';
$route['leaseDocumentAdd/(:any)'] = 'lease/add_lease_doc/$1';
$route['leaseDocumentEdit/(:any)/(:any)'] = 'lease/edit_lease_doc/$1/$2';
$route['leaseExtend'] = 'lease/extend_lease';
$route['leaseEnd'] = 'lease/end_lease';
$route['changeRent'] = 'lease/rent_change';

//Rent Section
$route['rentSchedule/(:any)/(:any)'] = 'rent/rent_schedule/$1/$2';
$route['invoiceSetting/(:any)'] = 'rent/invoice_settings/$1';
$route['moneyIn/(:any)'] =  'rent/money_in/$1';

//rent and cron section
$route['statement/(:any)'] = 'cronjob/statement/$1';


//Maintenance Section
$route['Allmaintenance/(:any)'] = 'maintenance/view_all_maintenance/$1';
$route['Addmaintenance/(:any)'] = 'maintenance/add_maintenance/$1';
$route['Editmaintenance/(:any)/(:any)'] = 'maintenance/edit_maintenance/$1/$2';
$route['addMaintenanceOrderInvoice/(:any)'] = 'maintenance/add_maintenance_order_invoice/$1';
$route['editMaintenanceOrderInvoice/(:any)'] = 'maintenance/edit_maintenance_order_invoice/$1';
$route['insertMaintenanceOrderInvoice'] = 'maintenance/insert_maintenance_order_invoice';
$route['updateMaintenanceOrderInvoice'] = 'maintenance/update_maintenance_order_invoice';
$route['maintenanceInvoiceDocument'] = 'maintenance/maintenance_invoice_document';

//Inspection Section
$route['inspectionList/(:any)/(:any)'] = 'inspection/inspection_list/$1/$2';
$route['addInspection/(:any)'] = 'inspection/add_inspection/$1';
$route['insertInspection'] = 'inspection/insert_inspection';
$route['editInspection/(:any)'] = 'inspection/edit_inspection/$1';
$route['deleteInspection/(:any)/(:any)/(:any)'] = 'inspection/delete_inspection/$1/$2/$3';
$route['updateInspection'] = 'inspection/update_inspection';
$route['deleteInspectionFile/(:any)'] = 'inspection/delete_inspection_file/$1';
$route['inspectionDocument'] = 'inspection/inspection_document';

//Note Section
$route['noteList/(:any)'] = 'note/note_list/$1';
$route['addNote/(:any)'] = 'note/add_note/$1';
$route['insertNote'] = 'note/insert_note';
$route['editNote/(:any)'] = 'note/edit_note/$1';
$route['deleteNote/(:any)/(:any)'] = 'note/delete_note/$1/$2';
$route['updateNote'] = 'note/update_note';
$route['noteDocument'] = 'note/note_document';
$route['noteView/(:any)'] = 'note/note_view/$1';
$route['noteSeriesView/(:any)'] = 'note/note_series_view/$1';
$route['deleteNoteFile/(:any)'] = 'note/delete_note_file/$1';

//Water Section
$route['waterInvoiceList/(:any)'] = 'water/water_invoice_list/$1';
$route['addWaterInvoice/(:any)'] = 'water/add_water_invoice/$1';
$route['insertWaterInvoice'] = 'water/insert_water_invoice';
$route['editWaterInvoice/(:any)'] = 'water/edit_water_invoice/$1';
$route['updateWaterInvoice'] = 'water/update_water_invoice';
$route['payWaterInvoice/(:any)'] = 'water/pay_water_invoice/$1';
$route['markCompleteWaterInvoice/(:any)'] = 'water/mark_complete_water_invoice/$1';
$route['insertWaterInvoiceLog'] = 'water/insert_water_invoice_log';
$route['sendInvoiceToTenants/(:any)'] = 'water/send_invoice_to_tenants/$1';
$route['cancelWaterInvoice/(:any)'] = 'water/cancel_water_invoice/$1';
$route['waterBill'] = 'water/water_bill';
$route['waterInvoicePdf/(:any)'] = 'water/water_invoice_pdf/$1';

//Expense section
$route['viewExpense/(:any)'] = 'expense/view_expense/$1';
$route['addExpense/(:any)'] = 'expense/view_expense/$1/';
$route['expense/mark_expense_as_paid'] = 'expense/mark_expense_as_paid';

//Message section
$route['Message/(:any)'] = 'message/all_message/$1';
$route['ajaxChunkMessage'] = 'message/ajax_chunk_message';
$route['MessageDetail/(:any)'] = 'message/message_detail/$1';
$route['MessageDetailPdf/(:any)'] = 'message/message_detail_pdf/$1';
$route['AddMessage/(:any)/(:any)'] = 'message/add_message/$1/$2';


//Other Section
$route['propertyInfo/(:any)'] = 'other/property_photos_info/$1';

//Cronjob
//$route['cron_mark_completed_leases_as_inactive'] = 'cronjob/mark_completed_leases_as_inactive'; // do not add

$route['auto_send_unpaid_remainder_of_expense'] = 'cronjob/auto_send_unpaid_remainder_of_expense';
$route['auto_send_reminder_for_arrears_payment'] = 'cronjob/sendReminderForArrearsPayment';
$route['auto_send_monthly_statements'] = 'cronjob/auto_send_monthly_statements';
$route['auto_mark_rent_as_arrear'] = 'cronjob/autoMarkRentAsArrear';
$route['auto_calculate_arrear'] = 'cronjob/autoCalculateArrears';
$route['auto_send_water_due_invoice_emails'] = 'cronjob/autoSendWaterDueInvoiceEmails';
$route['auto_send_inspection_reminder'] = 'cronjob/autoSendInspectionReminder';

//need only for testing
$route['auto_truncate_statement_table'] = 'cronjob/auto_truncate_statement_table';

//vacant
$route['property/vacate_tenants/(:any)'] = 'property/vacate_tenants/$1';

//archive
$route['ArchivedLeases/(:any)'] = 'property/inactive_lease_list/$1';

$route['archive/leaseDescription/(:any)/(:any)'] = 'archive/lease_details/$1/$2';
$route['archive/leaseBonds/(:any)/(:any)'] = 'archive/all_bonds/$1/$2';
$route['archive/leaseTenants/(:any)/(:any)'] = 'archive/add_tenants/$1/$2';
$route['archive/leaseDocument/(:any)/(:any)'] = 'archive/all_lease_doc/$1/$2';
$route['archive/lease_detail_doc/(:any)/(:any)'] = 'archive/lease_detail_doc/$1/$2';
$route['archive/leaseDocumentEdit/(:any)/(:any)'] = 'archive/edit_lease_doc/$1/$2';

$route['archive/Allmaintenance/(:any)/(:any)'] = 'archive/view_all_maintenance/$1/$2';
$route['archive/Editmaintenance/(:any)/(:any)'] = 'archive/edit_maintenance/$1/$2';
$route['archive/editMaintenanceOrderInvoice/(:any)'] = 'archive/edit_maintenance_order_invoice/$1';

$route['archive/Message/(:any)/(:any)'] = 'archive/all_message/$1/$2';
$route['archivedAjaxChunkMessage'] = 'archive/archived_ajax_chunk_message';
$route['archive/MessageDetail/(:any)'] = 'archive/message_detail/$1';
$route['archive/MessageDetailPdf/(:any)'] = 'archive/message_detail_pdf/$1';

$route['archive/waterInvoiceList/(:any)/(:any)'] = 'archive/water_invoice_list/$1/$2';
$route['archive/editWaterInvoice/(:any)'] = 'archive/edit_water_invoice/$1';

