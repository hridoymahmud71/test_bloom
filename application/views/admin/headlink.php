<!DOCTYPE html>
<base href="<?php echo base_url();?>">
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en">
    <!--<![endif]-->
    <!-- BEGIN HEAD -->

<head>
    <meta charset="utf-8" />
    <title><?=$head_data[0]['name'];?> | Admin Panel</title>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta content="width=device-width, initial-scale=1" name="viewport" />
    <meta content="Preview page of Metronic Admin Theme #4 for statistics, charts, recent events and reports" name="description" />
    <meta content="" name="author" />
    <!-- BEGIN GLOBAL MANDATORY STYLES -->
    <link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css" />
    <link href="back_assets/global/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
    <link href="back_assets/global/plugins/simple-line-icons/simple-line-icons.min.css" rel="stylesheet" type="text/css" />
    <link href="back_assets/global/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <link href="back_assets/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css" rel="stylesheet" type="text/css" />
    <!-- END GLOBAL MANDATORY STYLES -->
    <!-- BEGIN PAGE LEVEL PLUGINS -->
    <link href="back_assets/global/plugins/bootstrap-daterangepicker/daterangepicker.min.css" rel="stylesheet" type="text/css" />
    <link href="back_assets/global/plugins/morris/morris.css" rel="stylesheet" type="text/css" />
    <link href="back_assets/global/plugins/fullcalendar/fullcalendar.min.css" rel="stylesheet" type="text/css" />
    <link href="back_assets/global/plugins/jqvmap/jqvmap/jqvmap.css" rel="stylesheet" type="text/css" />
    <!-- END PAGE LEVEL PLUGINS -->
    <!-- BEGIN THEME GLOBAL STYLES -->
    <link href="back_assets/global/css/components.min.css" rel="stylesheet" id="style_components" type="text/css" />
    <link href="back_assets/global/css/plugins.min.css" rel="stylesheet" type="text/css" />
    <!-- END THEME GLOBAL STYLES -->
    <!-- BEGIN THEME LAYOUT STYLES -->
    <link href="back_assets/layouts/layout4/css/layout.min.css" rel="stylesheet" type="text/css" />
    <link href="back_assets/layouts/layout4/css/themes/default.min.css" rel="stylesheet" type="text/css" id="style_color" />
    <link href="back_assets/layouts/layout4/css/custom.min.css" rel="stylesheet" type="text/css" />
    <!-- END THEME LAYOUT STYLES -->
    <link rel="shortcut icon" href="favicon.ico" /> 


    <!-- BEGIN DataTable PLUGINS -->
    <link href="back_assets/global/plugins/datatables/datatables.min.css" rel="stylesheet" type="text/css" />
    <link href="back_assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css" rel="stylesheet" type="text/css" />
    <link href="back_assets/global/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css" rel="stylesheet" type="text/css" />
    <!-- END DataTable PLUGINS -->

    <!-- BEGIN Autoload PLUGINS -->
    <link href="back_assets/global/plugins/typeahead/typeahead.css" rel="stylesheet" type="text/css" />
    <!-- END Autoload PLUGINS -->

    <!-- BEGIN Image-input PLUGINS -->
    <link href="back_assets/global/plugins/bootstrap-fileinput/bootstrap-fileinput.css" rel="stylesheet" type="text/css" />
    <!-- END Image-input PLUGINS -->

    <!-- BEGIN Select2 PLUGINS -->
    <link href="back_assets/global/plugins/select2/css/select2.min.css" rel="stylesheet" type="text/css" />
    <link href="back_assets/global/plugins/select2/css/select2-bootstrap.min.css" rel="stylesheet" type="text/css" />
    <!-- END Select2 PLUGINS -->

    <!-- BEGIN Editor PLUGINS -->
    <link href="back_assets/global/plugins/bootstrap-summernote/summernote.css" rel="stylesheet" type="text/css" />
    <!-- END Editor PLUGINS -->

    <!--BEGIN Touchspin PLUGINS-->
    <link href="back_assets/global/plugins/bootstrap-touchspin/bootstrap.touchspin.css" rel="stylesheet" type="text/css" />
    <!--END Touchspin PLUGINS-->

    <!--BEGIN ICheck PLUGINS-->
    <link href="back_assets/global/plugins/icheck/skins/all.css" rel="stylesheet" type="text/css" />
    <!--END ICheck PLUGINS-->


    <link href="back_assets/global/plugins/ladda/ladda-themeless.min.css" rel="stylesheet" type="text/css" />


    <link href="back_assets/global/plugins/bootstrap-timepicker/css/bootstrap-timepicker.min.css" rel="stylesheet" type="text/css" />
    <link href="back_assets/global/plugins/bootstrap-datetimepicker/css/bootstrap-datetimepicker.min.css" rel="stylesheet" type="text/css" />
<link href="https://fonts.googleapis.com/css?family=Josefin+Sans:100,100i,300,300i,400,400i,600,600i,700,700i" rel="stylesheet">
    <style type="text/css">
        .table-image
        {
            width: 65px !important;
            height: 35px !important;
            border: 1px solid #eee !important;
        }
    </style>

</head>
    <!-- END HEAD -->