
<div class="page-sidebar navbar-collapse collapse page-sidebar-fixed" style="margin-top: 10px;">

    <ul class="page-sidebar-menu   " data-keep-expanded="false" data-auto-scroll="true" data-slide-speed="200">


<!--************************* Admin Section Start****************************-->
     <?php if($this->session->userdata('type')==0 || $this->session->userdata('type')==1){ ?>
        <li class="nav-item start <?php if($active=='Dashboard'){echo 'active open';}?>">
            <a href="admin" class="nav-link nav-toggle">
                <i class="icon-home"></i>
                <span class="title"><?=$this->lang->line('dashboard_label');?></span>
                <span class="selected"></span>
            </a>
        </li>

    <!-- User Feature Starts-->
        <li class="heading">
            <h3 class="uppercase"><?=$this->lang->line('users_label').' '.$this->lang->line('feature_label');?></h3>
        </li>

        <li class="nav-item  <?php if($active=='user_list' || $active=='add_user' || $active=='user_list_un'){echo 'active open';}?>">
            <a href="javascript:;" class="nav-link nav-toggle">
                <i class="fa fa-user"></i>
                <span class="title"><?=$this->lang->line('users_label');?></span>
                <span class="arrow <?php if($active=='user_list' || $active=='add_user' || $active=='user_list_un'){echo 'open';}?>"></span>
            </a>
            <ul class="sub-menu">
                
                <li class="nav-item  <?php if($active=='add_user'){echo 'active open';}?>">
                    <a href="admin/add_user" class="nav-link ">
                        <span class="title"><?=$this->lang->line('btn_add_label').' '.$this->lang->line('user_label');?></span>
                    </a>
                </li>
                <li class="nav-item  <?php if($active=='user_list'){echo 'active open';}?>">
                    <a href="admin/user_list" class="nav-link ">
                        <span class="title"><?=$this->lang->line('verified_label').' '.$this->lang->line('users_list_label');?></span>
                    </a>
                </li>
                <li class="nav-item  <?php if($active=='user_list_un'){echo 'active open';}?>">
                    <a href="admin/unverfied_user_list" class="nav-link ">
                        <span class="title"><?=$this->lang->line('unverified_label').' '.$this->lang->line('users_list_label');?></span>
                    </a>
                </li>
            </ul>
        </li>
    <!-- User Feature End-->

    <!-- Doctor Feature Starts-->

        <li class="heading">
            <h3 class="uppercase"><?=$this->lang->line('doctor_label').' '.$this->lang->line('feature_label');?></h3>
        </li>

        <li class="nav-item  <?php if($active=='doctor' || $active=='package' || $active=='appointment' || $active=='degree' || $active=='specialization'){echo 'active open';}?>">
            <a href="javascript:;" class="nav-link nav-toggle">
                <i class="fa fa-user-md"></i>
                <span class="title"><?=$this->lang->line('doctor_label').'/'.$this->lang->line('dentist_label');?></span>
                <span class="arrow <?php if($active=='doctor' || $active=='package' || $active=='appointment' || $active=='degree' || $active=='specialization'){echo 'open';}?>"></span>
            </a>
            <ul class="sub-menu">
                <li class="nav-item  <?php if($active=='doctor'){echo 'active open';}?>">
                    <a href="admin/doctor" class="nav-link ">
                        <span class="title"><?=$this->lang->line('doctor_list_label');?></span>
                    </a>
                </li>
                <li class="nav-item  <?php if($active=='package'){echo 'active open';}?>">
                    <a href="admin/package" class="nav-link ">
                        <span class="title"><?=$this->lang->line('package_list_label');?></span>
                    </a>
                </li>
                <li class="nav-item  <?php if($active=='appointment'){echo 'active open';}?>">
                    <a href="admin/appointment" class="nav-link ">
                        <span class="title"><?=$this->lang->line('appointment_list_label');?></span>
                    </a>
                </li>
                <li class="nav-item  <?php if($active=='specialization'){echo 'active open';}?>">
                    <a href="admin/specialization" class="nav-link ">
                        <span class="title"><?=$this->lang->line('specialization_list_label');?></span>
                    </a>
                </li>  
                <li class="nav-item  <?php if($active=='degree'){echo 'active open';}?>">
                    <a href="admin/degree" class="nav-link ">
                        <span class="title"><?=$this->lang->line('degree_list_label');?></span>
                    </a>
                </li>                
            </ul>
        </li>

        <li class="nav-item  <?php if($active=='conversation'){echo 'active open';}?>">
            <a href="javascript:;" class="nav-link nav-toggle">
                <i class="fa fa-comments-o"></i>
                <span class="title"><?=$this->lang->line('conversation_label');?></span>
                <span class="arrow <?php if($active=='conversation'){echo 'open';}?>"></span>
            </a>
            <ul class="sub-menu">
                <li class="nav-item  <?php if($active=='text'){echo 'active open';}?>">
                    <a href="admin/conversation/text" class="nav-link ">
                        <span class="title"><?=$this->lang->line('text_label');?></span>
                    </a>
                </li>
                <li class="nav-item  <?php if($active=='audio'){echo 'active open';}?>">
                    <a href="admin/conversation/audio" class="nav-link ">
                        <span class="title"><?=$this->lang->line('audio_label');?></span>
                    </a>
                </li>
                <li class="nav-item  <?php if($active=='video'){echo 'active open';}?>">
                    <a href="admin/conversation/video" class="nav-link ">
                        <span class="title"><?=$this->lang->line('video_label');?></span>
                    </a>
                </li>                
            </ul>
        </li>
        
        <li class="nav-item  <?php if($active=='doctor_alter'){echo 'active open';}?>">
            <a href="javascript:;" class="nav-link nav-toggle">
                <i  class="fa fa-heartbeat"></i>
                <span class="title"><?=$this->lang->line('alt_doctor_label');?></span>
                <span class="arrow <?php if($active=='doctor_alter'){echo 'open';}?>"></span>
            </a>
            <ul class="sub-menu">
                <li class="nav-item  <?php if($active=='doctor_alter'){echo 'active open';}?>">
                    <a href="admin/doctor_alter" class="nav-link ">
                        <span class="title"><?=$this->lang->line('alt_doctor_list_label');?></span>
                    </a>
                </li>                
            </ul>
        </li>

        <li class="nav-item  <?php if($active=='hospital'){echo 'active open';}?>">
            <a href="javascript:;" class="nav-link nav-toggle">
                <i class="fa fa-hospital-o"></i>
                <span class="title"><?=$this->lang->line('hospital_label');?></span>
                <span class="arrow <?php if($active=='hospital'){echo 'open';}?>"></span>
            </a>
            <ul class="sub-menu">
                <li class="nav-item  <?php if($active=='hospital'){echo 'active open';}?>">
                    <a href="admin/hospital" class="nav-link ">
                        <span class="title"><?=$this->lang->line('hospital_list_label');?></span>
                    </a>
                </li>                
            </ul>
        </li>

        <li class="nav-item  <?php if($active=='pharmacy'){echo 'active open';}?>">
            <a href="javascript:;" class="nav-link nav-toggle">
                <i  class="fa fa-map-marker"></i>
                <span class="title"><?=$this->lang->line('pharmacy_label');?></span>
                <span class="arrow <?php if($active=='pharmacy'){echo 'open';}?>"></span>
            </a>
            <ul class="sub-menu">
                <li class="nav-item  <?php if($active=='pharmacy'){echo 'active open';}?>">
                    <a href="admin/pharmacy" class="nav-link ">
                        <span class="title"><?=$this->lang->line('pharmacy_list_label');?></span>
                    </a>
                </li>                
            </ul>
        </li>

        <li class="nav-item  <?php if($active=='bsl'){echo 'active open';}?>">
            <a href="javascript:;" class="nav-link nav-toggle">
                <i  class="fa fa-eye"></i>
                <span class="title"><?=$this->lang->line('bsl_label');?></span>
                <span class="arrow <?php if($active=='bsl'){echo 'open';}?>"></span>
            </a>
            <ul class="sub-menu">
                <li class="nav-item  <?php if($active=='bsl'){echo 'active open';}?>">
                    <a href="admin/beauty_spa_saloon" class="nav-link ">
                        <span class="title"><?=$this->lang->line('bsl_list_label');?></span>
                    </a>
                </li>                
            </ul>
        </li>

    <!-- Doctor Feature End-->
    <!-- Patient Feature Starts-->
        <li class="heading">
            <h3 class="uppercase"><?=$this->lang->line('patient_label').' '.$this->lang->line('feature_label');?></h3>
        </li>

        <li class="nav-item  <?php if($active=='patient'){echo 'active open';}?>">
            <a href="javascript:;" class="nav-link nav-toggle">
                <i class="fa fa-user-plus"></i>
                <span class="title"><?=$this->lang->line('patient_label');?></span>
                <span class="arrow <?php if($active=='patient'){echo 'open';}?>"></span>
            </a>
            <ul class="sub-menu">
                <li class="nav-item  <?php if($active=='patient'){echo 'active open';}?>">
                    <a href="admin/patient" class="nav-link ">
                        <span class="title"><?=$this->lang->line('patient_list_label');?></span>
                    </a>
                </li>
            </ul>
        </li>
    <!-- Patient Feature End-->

    <!-- Extra Feature Starts-->
        <li class="heading">
            <h3 class="uppercase"><?=$this->lang->line('extra_feature_label');?></h3>
        </li>

        <li class="nav-item  <?php if($active=='magazine' || $active=='m_tags' || $active=='m_category' ){echo 'active open';}?>">
            <a href="javascript:;" class="nav-link nav-toggle">
                <i class="fa fa-newspaper-o"></i>
                <span class="title"><?=$this->lang->line('magazine_label');?></span>
                <span class="arrow <?php if($active=='magazine' || $active=='m_tags' || $active=='m_category'){echo 'open';}?>"></span>
            </a>
            <ul class="sub-menu">
                <li class="nav-item  <?php if($active=='magazine'){echo 'active open';}?>">
                    <a href="admin/magazine" class="nav-link ">
                        <span class="title"><?=$this->lang->line('magazine_list_label');?></span>
                    </a>
                </li>
                <li class="nav-item  <?php if($active=='m_category'){echo 'active open';}?>">
                    <a href="admin/magazine_category" class="nav-link ">
                        <span class="title"><?=$this->lang->line('cat_tag_label');?></span>
                    </a>
                </li>
                <li class="nav-item  <?php if($active=='m_tags'){echo 'active open';}?>">
                    <a href="admin/magazine_tags" class="nav-link ">
                        <span class="title"><?=$this->lang->line('tag_label');?></span>
                    </a>
                </li>
            </ul>
        </li>


        <li class="nav-item  <?php if($active=='coupon'){echo 'active open';}?>">
            <a href="javascript:;" class="nav-link nav-toggle">
                <i class="fa fa-cc"></i>
                <span class="title"><?=$this->lang->line('coupon_label');?></span>
                <span class="arrow <?php if($active=='coupon'){echo 'open';}?>"></span>
            </a>
            <ul class="sub-menu">
                <li class="nav-item  <?php if($active=='coupon'){echo 'active open';}?>">
                    <a href="admin/coupon" class="nav-link ">
                        <span class="title"><?=$this->lang->line('coupon_list_label');?></span>
                    </a>
                </li>
            </ul>
        </li>
    <!-- Extra Feature End-->

        <!-- Manage Feature Starts-->
        <li class="heading">
            <h3 class="uppercase"><?=$this->lang->line('manage_label').' '.$this->lang->line('feature_label');?></h3>
        </li>

        <li class="nav-item  <?php if($active=='groups'){echo 'active open';}?>">
            <a href="javascript:;" class="nav-link nav-toggle">
                <i class="fa fa-group"></i>
                <span class="title"><?=$this->lang->line('groups_label');?></span>
                <span class="arrow <?php if($active=='groups'){echo 'open';}?>"></span>
            </a>
            <ul class="sub-menu">
                <li class="nav-item  <?php if($active=='groups'){echo 'active open';}?>">
                    <a href="admin/groups" class="nav-link ">
                        <span class="title"><?=$this->lang->line('groups_list_label');?></span>
                    </a>
                </li>
            </ul>
        </li>

        <li class="nav-item  <?php if($active=='adv'){echo 'active open';}?>">
            <a href="javascript:;" class="nav-link nav-toggle">
                <i class="fa fa-file-photo-o"></i>
                <span class="title"><?=$this->lang->line('Adv_label');?></span>
                <span class="arrow <?php if($active=='adv'){echo 'open';}?>"></span>
            </a>
            <ul class="sub-menu">
                <li class="nav-item  <?php if($active=='adv'){echo 'active open';}?>">
                    <a href="admin/groups" class="nav-link ">
                        <span class="title"><?=$this->lang->line('Adv_list_label');?></span>
                    </a>
                </li>
            </ul>
        </li>

        <li class="nav-item  <?php if($active=='sms_content' || $active=='sms_report'){echo 'active open';}?>">
            <a href="javascript:;" class="nav-link nav-toggle">
                <i class="fa fa-envelope"></i>
                <span class="title"><?=$this->lang->line('sms_label');?></span>
                <span class="arrow <?php if($active=='sms_content' || $active=='sms_report'){echo 'open';}?>"></span>
            </a>
            <ul class="sub-menu">
                <li class="nav-item  <?php if($active=='sms_report'){echo 'active open';}?>">
                    <a href="admin/sms_report" class="nav-link ">
                        <span class="title"><?=$this->lang->line('sms_report_label');?></span>
                    </a>
                </li>
                <li class="nav-item  <?php if($active=='sms_content'){echo 'active open';}?>">
                    <a href="admin/sms_content" class="nav-link ">
                        <span class="title"><?=$this->lang->line('sms_content_label');?></span>
                    </a>
                </li>
            </ul>
        </li>

        <!-- Agent Admin Starts-->

        <li class="nav-item  <?php if($active=='agent_admin'){echo 'active open';}?>">
            <a href="javascript:;" class="nav-link nav-toggle">
                <i class="fa fa-user-secret"></i>
                <span class="title"><?=$this->lang->line('agent_admin_label');?></span>
                <span class="arrow <?php if($active=='agent_admin'){echo 'open';}?>"></span>
            </a>
            <ul class="sub-menu">
                <li class="nav-item  <?php if($active=='agent_admin'){echo 'active open';}?>">
                    <a href="admin/agent_admin" class="nav-link ">
                        <span class="title"><?=$this->lang->line('agent_admin_list_label');?></span>
                    </a>
                </li>
            </ul>
        </li>

        <!-- Manage Feature End-->

        <li class="heading">
            <h3 class="uppercase"><?=$this->lang->line('payment_label').' '.$this->lang->line('feature_label');?></h3>
        </li>
        <!-- Transaction Starts-->

        <li class="nav-item  <?php if($active=='order'){echo 'active open';}?>">
            <a href="javascript:;" class="nav-link nav-toggle">
                <i class="fa fa-exchange"></i>
                <span class="title"><?=$this->lang->line('subscribe_label');?></span>
                <span class="arrow <?php if($active=='order'){echo 'open';}?>"></span>
            </a>
            <ul class="sub-menu">
                <li class="nav-item  <?php if($active=='order'){echo 'active open';}?>">
                    <a href="admin/order" class="nav-link ">
                        <span class="title"><?=$this->lang->line('doctor_label').' '.$this->lang->line('subscribe_label');?></span>
                    </a>
                </li>

                <li class="nav-item  <?php if($active=='order'){echo 'active open';}?>">
                    <a href="admin/order" class="nav-link ">
                        <span class="title"><?=$this->lang->line('hospital_label').' '.$this->lang->line('subscribe_label');?></span>
                    </a>
                </li>

                <li class="nav-item  <?php if($active=='order'){echo 'active open';}?>">
                    <a href="admin/order" class="nav-link ">
                        <span class="title"><?=$this->lang->line('pharmacy_label').' '.$this->lang->line('subscribe_label');?></span>
                    </a>
                </li>

                <li class="nav-item  <?php if($active=='order'){echo 'active open';}?>">
                    <a href="admin/order" class="nav-link ">
                        <span class="title"><?=$this->lang->line('bsl_label').' '.$this->lang->line('subscribe_label');?></span>
                    </a>
                </li>
            </ul>
        </li>

        <li class="nav-item  <?php if($active=='consult'){echo 'active open';}?>">
            <a href="admin/order" class="nav-link ">
                <i class="fa fa-comments"></i>
                <span class="title"><?=$this->lang->line('consult_label');?></span>
            </a>
        </li>

        <!-- Transaction End-->

        
    

        <?php }  ?>

    </ul>
    <!-- END SIDEBAR MENU -->
</div>
 