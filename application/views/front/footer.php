
<div class="modal" id="ptl-1">
        <div class="modal-dialog">
            <div class="modal-content login-content">
                <div class="modal-header">
                    <div class="modal-title clearfix">
                        <h4>Login</h4>
                        <button class="btn btn-info button-more pull-right" data-dismiss="modal" type="button">&times;</button>
                    </div>
                </div>
                <div class="modal-body">
                    <div class="login-area">

                        <div id="loading_img" style="display:none;position: fixed;z-index: 10;top: 33%;left: 28%;width: 20%;">
                           <img style="width: 100%" src="back_assets/loading-spinner-blue.gif">     
                        </div>

                        <div class="alert alert-danger" id="err_div" style="display: none">
                          <strong id="err_msg"></strong> 
                        </div>
                    
                        <div class="form-group">
                            <label for="fullName">Email or Phone(*)</label>
                            <input autocomplete="new-email" value="" type="text" class="form-control height35" id="username_login" placeholder="Full Name">
                        </div>
                        <div class="form-group">
                            <label for="password">Password(*)</label>
                            <input autocomplete="new-password" value="" type="password" class="form-control height35" id="password_login" placeholder="Password">
                            <p><a href="#" class="font_class_p psstrong pull-right">Forgot Password?</a> </p>
                        </div>
                        <!-- <div class="form-group">
                            <div class="checkbox checkbox-primary">
                                <input id="checkbox2" type="checkbox" checked>
                                <label for="checkbox2">
                                    Remember me for 30 days 
                                </label>
                            </div>
                            <div class="checkbox checkbox-primary">
                                <input id="checkbox3" type="checkbox" checked>
                                <label for="checkbox3">
                                    Signup OTP instead of Email/Phone
                                </label>
                            </div>
                        </div> -->
                        <button id="login_submit" type="button" onclick="login_check()" class="btn btn-event">Login</button>
                        <h5 class="font_class_5 text-center">OR</h5>
                        <button type="submit" class="btn btn-fb"><i class="fa fa-facebook-official fa-btn-fb" aria-hidden="true"></i>Connect with facebook</button>
                        <!-- /form -->
                    </div>
                </div>
                <div class="modal-footer">

                </div>
            </div>
        </div>
    </div>


<div class="modal" id="mynotify">
    <div class="modal-dialog">
        <div class="arrow-up"></div>
        <div class="modal-content notify-content">
            <div class="modal-header">
                <div class="modal-title clearfix">
                    <h6>Notifications <a class="edit-notify pull-right" href="#">Mark all as read</a></h6>
                </div>
            </div>
            <div class="mCustomScrollbar" data-mcs-theme="minimal-dark">
                <div class="modal-body notify-box">
                    <div class="notify-cover">
                        <div class="notify-noti">
                            <div class="row">
                                <div class="col-xs-2">
                                    <img src="images/doctor-coment.jpg">
                                </div>
                                <div class="col-xs-10">
                                    <p class="font_class_p"><span class="bold">Kibria khondoker</span> commented on your post.</p>
                                    <p class="font_class_p">a hour ago</p>
                                </div>
                            </div>
                        </div>
                        <div class="notify-noti">
                            <div class="row">
                                <div class="col-xs-2">
                                    <img src="images/doctor-coment.jpg">
                                </div>
                                <div class="col-xs-10">
                                    <p class="font_class_p"><span class="bold">Kibria khondoker</span> commented on your post.</p>
                                    <p class="font_class_p">a hour ago</p>
                                </div>
                            </div>
                        </div>
                        <div class="notify-noti">
                            <div class="row">
                                <div class="col-xs-2">
                                    <img src="images/doctor-coment.jpg">
                                </div>
                                <div class="col-xs-10">
                                    <p class="font_class_p"><span class="bold">Kibria khondoker</span> commented on your post.</p>
                                    <p class="font_class_p">a hour ago</p>
                                </div>
                            </div>
                        </div>
                        <div class="notify-noti">
                            <div class="row">
                                <div class="col-xs-2">
                                    <img src="images/doctor-coment.jpg">
                                </div>
                                <div class="col-xs-10">
                                    <p class="font_class_p"><span class="bold">Kibria khondoker</span> commented on your post.</p>
                                    <p class="font_class_p">a hour ago</p>
                                </div>
                            </div>
                        </div>
                        <div class="notify-noti">
                            <div class="row">
                                <div class="col-xs-2">
                                    <img src="images/doctor-coment.jpg">
                                </div>
                                <div class="col-xs-10">
                                    <p class="font_class_p"><span class="bold">Kibria khondoker</span> commented on your post.</p>
                                    <p class="font_class_p">a hour ago</p>
                                </div>
                            </div>
                        </div>
                        <div class="notify-noti">
                            <div class="row">
                                <div class="col-xs-2">
                                    <img src="images/doctor-coment.jpg">
                                </div>
                                <div class="col-xs-10">
                                    <p class="font_class_p"><span class="bold">Kibria khondoker</span> commented on your post.</p>
                                    <p class="font_class_p">a hour ago</p>
                                </div>
                            </div>
                        </div>
                        <div class="notify-noti">
                            <div class="row">
                                <div class="col-xs-2">
                                    <img src="images/doctor-coment.jpg">
                                </div>
                                <div class="col-xs-10">
                                    <p class="font_class_p"><span class="bold">Kibria khondoker</span> commented on your post.</p>
                                    <p class="font_class_p">a hour ago</p>
                                </div>
                            </div>
                        </div>
                        <div class="notify-noti">
                            <div class="row">
                                <div class="col-xs-2">
                                    <img src="images/doctor-coment.jpg">
                                </div>
                                <div class="col-xs-10">
                                    <p class="font_class_p"><span class="bold">Kibria khondoker</span> commented on your post.</p>
                                    <p class="font_class_p">a hour ago</p>
                                </div>
                            </div>
                        </div>
                        <div class="notify-noti">
                            <div class="row">
                                <div class="col-xs-2">
                                    <img src="images/doctor-coment.jpg">
                                </div>
                                <div class="col-xs-10">
                                    <p class="font_class_p"><span class="bold">Kibria khondoker</span> commented on your post.</p>
                                    <p class="font_class_p">a hour ago</p>
                                </div>
                            </div>
                        </div>
                        <div class="notify-noti">
                            <div class="row">
                                <div class="col-xs-2">
                                    <img src="images/doctor-coment.jpg">
                                </div>
                                <div class="col-xs-10">
                                    <p class="font_class_p"><span class="bold">Kibria khondoker</span> commented on your post.</p>
                                    <p class="font_class_p">a hour ago</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <p class="font_class_p text-center"><a href="#">See All</a></p>
            </div>
        </div>
    </div>
</div>





<div class="modal" id="mynotify-msg">
    <div class="modal-dialog">
        <div class="arrow-up-msg"></div>
        <div class="modal-content notify-content-msg">
            <div class="modal-header">
                <div class="modal-title clearfix">
                    <h6>Notifications <a class="edit-notify pull-right" href="#">Mark all as read</a></h6>
                </div>
            </div>
            <div class="mCustomScrollbar" data-mcs-theme="minimal-dark">
                <div class="modal-body notify-box">
                    <div class="notify-cover">
                        <div class="notify-noti">
                            <div class="row">
                                <div class="col-xs-2">
                                    <img src="images/doctor-coment.jpg">
                                </div>
                                <div class="col-xs-10">
                                    <p class="font_class_p"><span class="bold">Kibria khondoker</span> commented on your post.</p>
                                    <p class="font_class_p">a hour ago</p>
                                </div>
                            </div>
                        </div>
                        <div class="notify-noti">
                            <div class="row">
                                <div class="col-xs-2">
                                    <img src="images/doctor-coment.jpg">
                                </div>
                                <div class="col-xs-10">
                                    <p class="font_class_p"><span class="bold">Kibria khondoker</span> commented on your post.</p>
                                    <p class="font_class_p">a hour ago</p>
                                </div>
                            </div>
                        </div>
                        <div class="notify-noti">
                            <div class="row">
                                <div class="col-xs-2">
                                    <img src="images/doctor-coment.jpg">
                                </div>
                                <div class="col-xs-10">
                                    <p class="font_class_p"><span class="bold">Kibria khondoker</span> commented on your post.</p>
                                    <p class="font_class_p">a hour ago</p>
                                </div>
                            </div>
                        </div>
                        <div class="notify-noti">
                            <div class="row">
                                <div class="col-xs-2">
                                    <img src="images/doctor-coment.jpg">
                                </div>
                                <div class="col-xs-10">
                                    <p class="font_class_p"><span class="bold">Kibria khondoker</span> commented on your post.</p>
                                    <p class="font_class_p">a hour ago</p>
                                </div>
                            </div>
                        </div>
                        <div class="notify-noti">
                            <div class="row">
                                <div class="col-xs-2">
                                    <img src="images/doctor-coment.jpg">
                                </div>
                                <div class="col-xs-10">
                                    <p class="font_class_p"><span class="bold">Kibria khondoker</span> commented on your post.</p>
                                    <p class="font_class_p">a hour ago</p>
                                </div>
                            </div>
                        </div>
                        <div class="notify-noti">
                            <div class="row">
                                <div class="col-xs-2">
                                    <img src="images/doctor-coment.jpg">
                                </div>
                                <div class="col-xs-10">
                                    <p class="font_class_p"><span class="bold">Kibria khondoker</span> commented on your post.</p>
                                    <p class="font_class_p">a hour ago</p>
                                </div>
                            </div>
                        </div>
                        <div class="notify-noti">
                            <div class="row">
                                <div class="col-xs-2">
                                    <img src="images/doctor-coment.jpg">
                                </div>
                                <div class="col-xs-10">
                                    <p class="font_class_p"><span class="bold">Kibria khondoker</span> commented on your post.</p>
                                    <p class="font_class_p">a hour ago</p>
                                </div>
                            </div>
                        </div>
                        <div class="notify-noti">
                            <div class="row">
                                <div class="col-xs-2">
                                    <img src="images/doctor-coment.jpg">
                                </div>
                                <div class="col-xs-10">
                                    <p class="font_class_p"><span class="bold">Kibria khondoker</span> commented on your post.</p>
                                    <p class="font_class_p">a hour ago</p>
                                </div>
                            </div>
                        </div>
                        <div class="notify-noti">
                            <div class="row">
                                <div class="col-xs-2">
                                    <img src="images/doctor-coment.jpg">
                                </div>
                                <div class="col-xs-10">
                                    <p class="font_class_p"><span class="bold">Kibria khondoker</span> commented on your post.</p>
                                    <p class="font_class_p">a hour ago</p>
                                </div>
                            </div>
                        </div>
                        <div class="notify-noti">
                            <div class="row">
                                <div class="col-xs-2">
                                    <img src="images/doctor-coment.jpg">
                                </div>
                                <div class="col-xs-10">
                                    <p class="font_class_p"><span class="bold">Kibria khondoker</span> commented on your post.</p>
                                    <p class="font_class_p">a hour ago</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <p class="font_class_p text-center"><a href="#">See All</a></p>
            </div>
        </div>
    </div>
</div>



<!--Big footer-->
    <div class="big-footer">
        <div class="container">
            <div class="footer-menu text-capitalize">
                <div class="col-md-2 col-sm-4">
                    <h4>About us</h4>
                    <ul class="list-unstyled">
                        <li><a href="#">About </a></li>
                        <li><a href="#">Stoneworks</a></li>
                        <li><a href="#">how it works</a></li>
                        <li><a href="#">team</a></li>
                        <li><a href="#">investors</a></li>
                        <li><a href="#">Finance Report</a></li>
                    </ul>
                </div>
            </div>
            <div class="footer-menu text-capitalize">
                <div class="col-md-2 col-sm-4">
                    <h4>About us</h4>
                    <ul class="list-unstyled">
                        <li><a href="#">About </a></li>
                        <li><a href="#">Stoneworks</a></li>
                        <li><a href="#">how it works</a></li>
                        <li><a href="#">team</a></li>
                        <li><a href="#">investors</a></li>
                        <li><a href="#">Finance Report</a></li>
                    </ul>
                </div>
            </div>
            <div class="footer-menu text-capitalize">
                <div class="col-md-2 col-sm-4">
                    <h4>About us</h4>
                    <ul class="list-unstyled">
                        <li><a href="#">About </a></li>
                        <li><a href="#">Stoneworks</a></li>
                        <li><a href="#">how it works</a></li>
                        <li><a href="#">team</a></li>
                        <li><a href="#">investors</a></li>
                        <li><a href="#">Finance Report</a></li>
                    </ul>
                </div>
            </div>
            <div class="footer-menu text-capitalize">
                <div class="col-md-2 col-sm-4">
                    <h4>Press</h4>
                    <ul class="list-unstyled">
                        <li><a href="#">About </a></li>
                        <li><a href="#">Stoneworks</a></li>
                        <li><a href="#">how it works</a></li>
                        <li><a href="#">team</a></li>
                        <li><a href="#">investors</a></li>
                        <li><a href="#">Finance Report</a></li>
                    </ul>
                </div>
            </div>
            <div class="footer-menu text-capitalize">
                <div class="col-md-2 col-sm-4">
                    <h4>get in touch</h4>
                    <ul class="list-unstyled">
                        <li><a href="#">About </a></li>
                        <li><a href="#">Stoneworks</a></li>
                        <li><a href="#">how it works</a></li>
                        <li><a href="#">team</a></li>
                        <li><a href="#">investors</a></li>
                        <li><a href="#">Finance Report</a></li>
                    </ul>
                </div>
                <div class="footer-menu text-capitalize">
                    <div class="col-md-2 col-sm-4">
                        <h4>About us</h4>
                        <ul class="list-unstyled">
                            <li><a href="#">About </a></li>
                            <li><a href="#">Stoneworks</a></li>
                            <li><a href="#">how it works</a></li>
                            <li><a href="#">team</a></li>
                            <li><a href="#">investors</a></li>
                            <li><a href="#">Finance Report</a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--footer-->
    <div class="footer">
        <div class="container">
            <div class="row">
                <div class="col-md-12 col-sm-12">
                    <div class="footer-copy text-center">
                        <p>Copyright &copy; 2017, All Rights Reserved by <a href="http://www.rcreation-bd.com/"><span class="red">R-creation</span></a></p>
                    </div>
                </div>
            </div>
        </div>
    </div>