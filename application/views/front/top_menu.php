<div class="top_dashbord_menu">
    <nav class="navbar navbar-default">
        <div class="container">
            <div class="container-fluid">
                <!-- Brand and toggle get grouped for better mobile display -->

                <?php
                $is_archived = $this->utility_model->isArchived();
                $archived_string = $this->utility_model->getArchivedString();
                ?>

                <!-- Collect the nav links, forms, and other content for toggling -->
                <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                    <ul class="nav navbar-nav">
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button"
                               aria-haspopup="true" aria-expanded="false">Dashboard <span class="caret"></span></a>
                            <ul class="dropdown-menu">
                                <li><a href="Dashboard/<?= $property_id; ?><?= $archived_string ?>">Property
                                        dashboard</a></li>
                                <li><a href="property">View all properties</a></li>
                                <li><a href="stepTwo">Add a property</a></li>
                                <li><a href="ArchivedLeases/<?= $property_id; ?>">Archived profiles</a></li>
                            </ul>
                        </li>
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button"
                               aria-haspopup="true" aria-expanded="false">Lease Details <span class="caret"></span></a>
                            <ul class="dropdown-menu">
                                <li><a href="leaseDescription/<?= $property_id; ?><?= $archived_string ?>">Lease</a>
                                </li>
                                <li><a href="leaseBonds/<?= $property_id; ?><?= $archived_string ?>">Bonds</a></li>
                                <li><a href="leaseTenants/<?= $property_id; ?><?= $archived_string ?>">Tenants</a></li>
                                <li><a href="leaseDocument/<?= $property_id; ?><?= $archived_string ?>">Tenancy
                                        documentation</a></li>
                            </ul>
                        </li>
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button"
                               aria-haspopup="true" aria-expanded="false">Rent <span class="caret"></span></a>
                            <ul class="dropdown-menu">
                                <li><a href="rentSchedule/<?= $property_id; ?>/0<?= $archived_string ?>">Rent
                                        schedule</a></li>
                                <?php if (!$is_archived) { ?>
                                    <li><a href="rentSchedule/<?= $property_id; ?>/3">Mark rent received</a></li>
                                    <li><a href="rentSchedule/<?= $property_id; ?>/2">Print tenant ledger</a></li>
                                <?php } ?>
                            </ul>
                        </li>
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button"
                               aria-haspopup="true" aria-expanded="false">Maintenance <span class="caret"></span></a>
                            <ul class="dropdown-menu">
                                <li><a href="Allmaintenance/<?= $property_id; ?><?= $archived_string ?>">View all</a>
                                </li>
                                <?php if (!$is_archived) { ?>
                                    <li><a href="Addmaintenance/<?= $property_id; ?>">Add new maintenance order</a></li>
                                <?php } ?>
                            </ul>
                        </li>
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button"
                               aria-haspopup="true" aria-expanded="false">Inspection<span class="caret"></span></a>
                            <ul class="dropdown-menu">
                                <li><a href="inspectionList/<?= $property_id; ?>/upcoming<?= $archived_string ?>">View
                                        upcoming inspections</a>
                                </li>
                                <li><a href="inspectionList/<?= $property_id; ?>/completed<?= $archived_string ?>">View
                                        completed
                                        inspections</a></li>
                                <?php if (!$is_archived) { ?>
                                    <li><a href="addInspection/<?= $property_id; ?>">Add new inspection</a></li>
                                <?php } ?>
                            </ul>
                        </li>
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button"
                               aria-haspopup="true" aria-expanded="false">Notes<span class="caret"></span></a>
                            <ul class="dropdown-menu">
                                <li><a href="noteList/<?= $property_id; ?><?= $archived_string ?>">View notes</a></li>
                                <?php if (!$is_archived) { ?>
                                    <li><a href="addNote/<?= $property_id; ?>">Add a new note</a></li>
                                <?php } ?>
                            </ul>
                        </li>
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button"
                               aria-haspopup="true" aria-expanded="false">Water<span class="caret"></span></a>
                            <ul class="dropdown-menu">
                                <li><a href="waterInvoiceList/<?= $property_id; ?><?= $archived_string ?>">Invoice
                                        list</a></li>
                                <?php if (!$is_archived) { ?>
                                    <li><a href="addWaterInvoice/<?= $property_id; ?>">Add a new water invoice</a></li>
                                <?php } ?>
                            </ul>
                        </li>
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button"
                               aria-haspopup="true" aria-expanded="false">Expenses <span class="caret"></span></a>
                            <ul class="dropdown-menu">
                                <li><a href="viewExpense/<?= $property_id; ?><?= $archived_string ?>">View expenses</a>
                                </li>
                                <?php if (!$is_archived) { ?>
                                    <li><a href="addExpense/<?= $property_id; ?>">Add a new expense</a></li>
                                <?php } ?>
                            </ul>
                        </li>

                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button"
                               aria-haspopup="true" aria-expanded="false">Other <span class="caret"></span></a>
                            <ul class="dropdown-menu">
                                <li><a href="moneyIn/<?= $property_id; ?><?= $archived_string ?>">Other money in</a>
                                </li>
                                <?php if (!$is_archived) { ?>
                                    <li><a href="Dashboard/<?= $property_id; ?>/1">Property photos & info</a></li>
                                <?php } ?>
                            </ul>
                        </li>

                    </ul>
                </div><!-- /.navbar-collapse -->
            </div><!-- /.container-fluid -->
        </div>
    </nav>
</div>
<div class="ss_main_container"> 