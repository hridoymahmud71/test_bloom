<!DOCTYPE html>
<base href="<?php echo base_url(); ?>">
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Rent Simple</title>
    <link href="assets/css/bootstrap.min.css" rel="stylesheet">
    <link href="assets/css/style.css" rel="stylesheet">
    <!-- <link href="assets/css/custom_style.css" rel="stylesheet"> -->

    <link rel="stylesheet" href="https://code.jquery.com/ui/1.12.0/themes/smoothness/jquery-ui.css" type="text/css"/>
    <link rel="stylesheet" href="assets/css/ss_daterangepicker.css" type="text/css"/>
    <!-- <link rel="stylesheet" href="assets/css/ui.daterangepicker.css" type="text/css"> -->
    <link rel="stylesheet" href="assets/css/dropzone.min.css" type="text/css"/>
    <link rel="stylesheet" href="assets/css/jquery.timepicker.css" type="text/css"/>
    <link rel="stylesheet" href="assets/css/jquery.custom-scrollbar.css" type="text/css"/>
    <link rel="stylesheet" href="assets/css/morris.css" type="text/css"/>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/prettify/r224/prettify.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <script type="text/javascript" src="https://code.jquery.com/ui/1.12.0/jquery-ui.min.js"></script>

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.css" />
    <script src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.js"></script>


    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body>