<style>
    .disableClick {
        pointer-events: none;
    }
</style>

<div class="col-xs-12">
    <ul class="nav nav-pills nav-justified thumbnail setup-panel">
        <li <?php if ($step_num == 'one') {
            echo 'class="active">';
        } else {
            echo 'class="disabled" style="display:none" >';
        } ?>
        <a class="disableClick">
            <h4 class="list-group-item-heading">Step 1</h4>
            <p class="list-group-item-text">Let's get Started!</p>
        </a>
        </li>
        <li <?php if ($step_num == 'two') {
            echo 'class="active">';
        } else {
            echo 'class="disabled">';
        } ?>
        <a class="disableClick">
            <h4 class="list-group-item-heading">Step 2</h4>
            <p class="list-group-item-text">Add Rental Property</p>
        </a>
        </li>
        <li <?php if ($step_num == 'three') {
            echo 'class="active">';
        } else {
            echo 'class="disabled">';
        } ?>
        <a class="disableClick">

            <h4 class="list-group-item-heading">Step 3</h4>
            <p class="list-group-item-text">Add Tenants</p>
        </a>
        </li>
        <li <?php if ($step_num == 'four') {
            echo 'class="active">';
        } else {
            echo 'class="disabled">';
        } ?>
        <a class="disableClick">
            <h4 class="list-group-item-heading">Step 4</h4>
            <p class="list-group-item-text">Lease & Rent Details</p>
        </a>
        </li>
        <li <?php if ($step_num == 'five') {
            echo 'class="active">';
        } else {
            echo 'class="disabled">';
        } ?>
        <a class="disableClick">

            <h4 class="list-group-item-heading">Step 5</h4>
            <p class="list-group-item-text">Confirm Details</p>
        </a>
        </li>
        <!--<li <?php /*if ($step_num == 'six') {
            echo 'class="active">';
        } else {
            echo 'class="disabled">';
        } */?>
        <a class="disableClick">
            <h4 class="list-group-item-heading">Step 6</h4>
            <p class="list-group-item-text">Pays the rent</p>
        </a>
        </li>-->
    </ul>
</div>