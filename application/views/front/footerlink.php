<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<script type="text/javascript" src="https://code.jquery.com/ui/1.12.0/jquery-ui.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/raphael/2.1.2/raphael-min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/prettify/r224/prettify.min.js"></script>
<script type="text/javascript" src="assets/js/daterangepicker.jQuery.js"></script>

<script src="assets/js/bootstrap.min.js"></script>
<script src="assets/js/custom.js"></script>
<script src="assets/js/morris.min.js"></script>

<script type="text/javascript" src="assets/js/dropzone.min.js"></script>
<script type="text/javascript" src="assets/js/owl.carousel.js"></script>
<script type="text/javascript" src="assets/js/jquery.custom-scrollbar.min.js"></script>
<!--datetimepicker by https://cdnjs.com/libraries/bootstrap-datetimepicker, https://eonasdan.github.io/bootstrap-datetimepicker/  -->
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.22.1/moment.min.js"></script>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/eonasdan-bootstrap-datetimepicker/4.17.47/css/bootstrap-datetimepicker.min.css" type="text/css"/>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/eonasdan-bootstrap-datetimepicker/4.17.47/js/bootstrap-datetimepicker.min.js"></script>

<script type="text/javascript" src="assets/js/jquery.timepicker.js"></script>
<script type="text/javascript" src="assets/js/jquery.datepair.min.js"></script>
<script type="text/javascript" src="assets/js/monthpicker.js"></script>

<script type='text/javascript' data-cfasync='false'>window.purechatApi = { l: [], t: [], on: function () { this.l.push(arguments); } }; (function () { var done = false; var script = document.createElement('script'); script.async = true; script.type = 'text/javascript'; script.src = 'https://app.purechat.com/VisitorWidget/WidgetScript'
    ; document.getElementsByTagName('HEAD').item(0).appendChild(script); script.onreadystatechange = script.onload = function (e) { if (!done && (!this.readyState || this.readyState == 'loaded' || this.readyState == 'complete')) { var w = new PCWidget({c: '2fc607aa-d9db-422e-85ee-9a6aebff0734', f: true }); done = true; } }; })();</script>

<style>
    .ui-widget-header{ color: #fff;}
    .ui-daterangepickercontain{ top: 511px;}
    .ui-widget-header{ background: #337ab7; border: #1f496d;}
    .ui-state-default, .ui-widget-content .ui-state-default, .ui-widget-header .ui-state-default{     color: #337ab7;}
    .ui-state-active, .ui-widget-content .ui-state-active, .ui-widget-header .ui-state-active { border: 1px solid #1f496d;}
</style>