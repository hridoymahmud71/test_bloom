<nav class="navbar navbar-expand fixed-top be-top-header">
    <div class="container-fluid">
        <div class="be-navbar-header"><a href="/" class="navbar-brand"><img class="img-responsive"
                                                                            src="assets/img/logo.png"/></a>
        </div>
        <div class="be-right-navbar">
            <ul class="nav navbar-nav float-right be-user-nav">

                <?php

                $user_id = $this->session->userdata('user_id') ? $this->session->userdata('user_id') : 0;

                if ($user_id && isset($property_id)) {
                    $property_belongs_to_user = $this->utility_model->propertyBelongsToUser($property_id, $user_id);
                    if (!$property_belongs_to_user) {
                        redirect('not_permitted');
                    }
                }

                ?>

                <?php //$if_user_has_properties = $this->utility_model->if_user_has_properties($user_id) ?>

                <?php
                $is_archived = $this->utility_model->isArchived();
                $archived_string = $this->utility_model->getArchivedString();
                ?>


                <?php
                $archive_routes = ['ArchivedLeases', 'archive'];
                $step_routes = ['stepOne', 'stepTwo', 'stepThree', 'stepFour', 'stepFive', 'stepSix'];
                $segments = $this->uri->segment_array();

                $show_message = true;
                $show_menu_toggle = true;

                $c1 = in_array_any($archive_routes, $segments);
                $c2 = in_array_any($step_routes, $segments);

                //echo "[$c1][$c2]";

                if (in_array_any($archive_routes, $segments)
                    || in_array_any($step_routes, $segments)
                    || (in_array('property',$segments) && !in_array('property_details',$segments))
                ) {
                    $show_message = false;
                }

                if (in_array_any($step_routes, $segments)
                    ||
                    (in_array('property',$segments) && !in_array('property_details',$segments))
                ) {
                    $show_menu_toggle = false;
                }

                function in_array_any($needles, $haystack)
                {
                    return count(array_intersect($needles, $haystack));
                }

                ?>


                <?php
                /*echo "<br> show_message[$show_message]</br>";
                echo "<br> show_menu_toggle[$show_menu_toggle]</br>";*/
                ?>


                <?php if ($show_message) { ?>
                    <li>
                        <a href="Message/<?= $property_id; ?><?= $archived_string ?>">
                            <span class="glyphicon glyphicon-comment" aria-hidden="true"></span>
                        </a>
                    </li>
                <?php } ?>

                <li class="dropdown ss_menu_right" id="noti">
                    <a href="#" class="dropdown-toggle mr_notice" data-toggle="dropdown" role="button"
                       aria-haspopup="true" aria-expanded="false">
                        <span class="glyphicon glyphicon-bell" aria-hidden="true"></span>
                        <span id="noti_count" noti-count="0" class="badge badge-light noti_count">0</span>
                    </a>

                    <ul class="dropdown-menu mr_ul">
                        <li><a href="#">No new Notifications...</a></li>
                    </ul>
                </li>
                <?php if ($show_menu_toggle) { ?>
                    <li>
                        <a href="javascript:;" id="ss_menu_bar"><span class="glyphicon glyphicon-align-justify"
                                                                      aria-hidden="true"></span></a>
                    </li>
                <?php } ?>
                <li class="dropdown ss_user_menu"><a href="#" data-toggle="dropdown" role="button" aria-expanded="false"
                                                     class="nav-link dropdown-toggle">
                        <span class="glyphicon glyphicon-user" aria-hidden="true"></span> Welcome, <b
                                id="user_name"><?= $this->session->userdata('user_fname'); ?></b></a>

                    <ul class="dropdown-menu">
                        <li>
                            <a href="subscribe">
                                Subscription
                            </a>
                        </li>
                        <li>
                            <a id="signout_href" href="logout">Sign out</a>
                        </li>
                    </ul>
                </li>
            </ul>
        </div>
    </div>
</nav>

<script type="text/javascript">
    $(function () {

        loadNotifications();


        function loadNotifications() {
            $.ajax({
                url: "<?php echo base_url() . 'property/notification_list'?>",
                type: 'GET',
                success: function (noti) {
                    if (noti) {
                        $('#noti').html(noti);
                        attach_mark_notification_seen_event();
                    }
                }
            });
        }


        function attach_mark_notification_seen_event() {
            $(".sp_noti_mark").on("click", function (e) {

                e.preventDefault();

                var $mark = $(this);
                var noti_id = $mark.attr("notification-id");
                var $noti_count = $("#noti_count");
                var noti_count = $noti_count.attr('noti-count');
                $.ajax({
                    url: "<?php echo base_url() . 'property/mark_notification_seen/'?>" + noti_id,
                    type: 'GET',
                    success: function (res) {
                        if (res == "success") {
                            $mark.removeClass("glyphicon-ok");
                            $mark.addClass(" glyphicon-eye-open");
                            $mark.unbind("click");
                            $mark.attr("title", "Seen");

                            if (noti_count > 0) {
                                noti_count--;
                            }
                            $noti_count.attr("noti-count", noti_count).html(noti_count);
                        } else {
                            alert("Could not mark as seen");
                        }
                    }

                });

            })

        }


    });
</script>
