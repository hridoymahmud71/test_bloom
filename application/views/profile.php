<!DOCTYPE html>
<html lang="en">
<head>
  <title>Demo Project</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <link rel="stylesheet" href="<?=base_url();?>assets/css/font-awesome.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.0/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
</head>
<style type="text/css">
  .green{
    color:green;
  }

  .red{
    color:red;
  }

  .white{
    color:#fff;
  }

</style>
<body>

<div class="container">
<nav class="navbar navbar-inverse" id="my_refresh_div">
  <div class="container-fluid">
    <div class="navbar-header">
      <a class="navbar-brand" href="#"><i class="fa fa-home white"></i>&nbsp;<?=$this->session->userdata('email');?></a>
    </div>
    <div id="refresh_div">
    <?php $status=$this->session->userdata('status');?>
      <select onchange="status_change()" id="status" class="form-control pull-right" style="width: 15%;">
        <option value="1" <?php if($status==1){echo 'selected';}?>>Online</option>
        <option value="2" <?php if($status==2){echo 'selected';}?>>Away</option>
        <option value="3" <?php if($status==3){echo 'selected';}?>>Offline</option>
      </select>
    </div>
</nav>
</div>




<script type="text/javascript">
  
  function status_change() {
   var status= $("#status").val();
   $.ajax({
            type: "POST",
            url : "<?=base_url();?>user/status_change",
            data : {status:status},                  
            success : function(data)
            {
              $("#status").find('option:selected').removeAttr("selected");
              $('#status option[value='+status+']').attr('selected','selected');
            }
        });   
  }



  function loadlink(){
     $.ajax({
            type: "POST",
            url : "<?=base_url();?>status/check_inactive_user",
            data : {status:status},                  
            success : function(data)
            {
              console.log(data);
              if(data>=1)
              {
                $("#status").find('option:selected').removeAttr("selected");
                setTimeout(
                  function() 
                  {
                    $('#status').val('2');
                     $('#status option[value=2]').attr('selected','selected');
                  }, 1000);
               
              }
             // $("#refresh_div").load(location.href + " #refresh_div");
            // $('#my_refresh_div').load(document.URL +  ' #my_refresh_div');
            }
        });   
}


setInterval(function(){
    loadlink() // this will run after every 5 seconds
}, 30000);
</script>

<!-- <script type="text/javascript">
    var new_var = true;
    window.onbeforeunload = function () {
        if (new_var) {
             return "You will go offline and logout from your application";
             alert('sdf');
        }
    }
    function unhook() {
        new_var = false;

    }

    function hook() {
       $.ajax({
            type: "POST",
            url : "<?=base_url();?>status/close_browser",
            data : {status:status},                  
            success : function(data)
            {
             
            }
        });   
    }
</script>

<script type="text/javascript">
  window.onbeforeunload = function() {
       $.ajax({
            type: "POST",
            url : "<?=base_url();?>status/close_browser",
            data : {status:status},                  
            success : function(data)
            {
             
            }
        });   
  }
</script> -->


<!-- <script type='text/javascript'>
window.onbeforeunload = function(){
    //Ajax request to update the database
   //return "You will go offline and logout from your application";
   $.ajax({
            type: "POST",
             async: false,
            url : "<?=base_url();?>status/close_browser",
            data : {status:status},                  
            success : function(data)
            {
             
            }
        });   
}
</script>
 -->

<script type="text/javascript">
  $(window).bind("beforeunload", function(e) {
 $.ajax({
            type: "POST",
             async: false,
            url : "<?=base_url();?>status/close_browser",
            data : {status:status},                  
            success : function(data)
            {
             
            }
        });          

});


</script>
</body>
</html>
