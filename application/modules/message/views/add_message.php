<?php $this->load->view('front/headlink'); ?>
<?php $this->load->view('front/top_menu'); ?>
<div class="container">
    <?php $this->load->view('front/head_nav'); ?>
    <div>
        <div class="row">
            <div class="ss_container">
                <div class="ss_bound_content">
                    <!-- Nav tabs -->
                    <div class="ss_expensesinvoices">
                        <div class="ss_expensesinvoices_top">

                            <?php if ($this->session->flashdata('success')) { ?>
                                <div class="panel panel-primary">
                                    <div class="panel-heading">Success!</div>
                                    <div class="panel-body">
                                        <?php if ($this->session->flashdata('message_add_success')) { ?>
                                            <?= $this->session->flashdata('message_add_success') ?>
                                        <?php } ?>
                                    </div>
                                </div>
                            <?php } ?>

                            <?php if ($this->session->flashdata('error')) { ?>
                                <div class="panel panel-danger">
                                    <div class="panel-heading">Unsuccessful!</div>
                                    <div class="panel-body">
                                        <?php if ($this->session->flashdata('select_a_tenant')) { ?>
                                            <?= $this->session->flashdata('select_a_tenant') ?>
                                        <?php } ?>
                                    </div>
                                </div>
                            <?php } ?>

                            <div class="container">
                                <form action="message/insert_message" method="post">

                                    <div class="row col-md-12"
                                         style="background:aliceblue;padding:15px;margin-bottom:25px;">
                                        <div class="col-md-4" style="border-right: 2px dashed #ccc;">
                                            <h4>Tenants selected <input disabled type="checkbox" checked> will receive
                                                notifications via email and will have access to this entire
                                                conversation.</h4>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="col-md-1">TO :</div>
                                            <div class="col-md-5">
                                                <?php foreach ($all_tenant as $key => $row) { ?>
                                                    <label for="tenant_id">
                                                        <input type="checkbox" id="tenant_id" name="tenant_id[]"
                                                               value="<?= $row['tenant_id']; ?>"> <?= $row['user_fname']; ?> <?= $row['user_lname']; ?>
                                                    </label>
                                                <?php } ?>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-12" style="padding: 10px 0 10px 0;">
                                        <div class="col-md-12">
                                            <div class="col-md-3">
                                                <h2>Subject line</h2>
                                            </div>
                                            <div class="col-md-9">
                                                <div class="col-md-12">
                                                    <input type="text" class="form-control" name="subject">
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-12" style="padding: 10px 0 10px 0;">
                                        <div class="col-md-12">
                                            <div class="col-md-3">
                                                <h2>Message</h2>
                                            </div>
                                            <div class="col-md-9">
                                                <div class="col-md-12">
                                                    <textarea class="form-control" rows="8" name="comment"></textarea>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-12" style="padding: 10px 0 10px 0;">
                                        <div class="col-md-12">
                                            <div class="col-md-3">
                                                <h4>Upload photos, videos, documents:</h4>
                                            </div>
                                            <div class="col-md-9">
                                                <div class="col-md-12">
                                                    <div class="dropzone">
                                                        <div class="dz-message">
                                                            <h3>Click Here to upload your files</h3>
                                                        </div>
                                                    </div>
                                                    <div>* Max 20 MB per file</div>
                                                    <div class="previews" id="preview"></div>
                                                </div>
                                                <div class="col-md-12 ss_container" style="background: none;">
                                                    <div class="col-md-3">
                                                        <input type="hidden" name="lease_id" value="<?= $lease_id ?>">
                                                        <input type="hidden" name="property_id"
                                                               value="<?= $property_id ?>">
                                                        <input type="submit" class="btn btn-primary"
                                                               value="Send Message">
                                                    </div>
                                                    <div class="col-md-6">
                                                        <!--<a class="btn">
                                                          <span class="glyphicon glyphicon-remove" aria-hidden="true"></span>
                                                           Cancel
                                                        </a>-->
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </form>

                            </div>
                            <!-- <div class="col-md-2"><a href="#"  data-toggle="modal" data-target="#taxreport">Generate Tax Report</a></div> -->
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


<?php $this->load->view('front/footerlink'); ?>

<script type="text/javascript">
    Dropzone.autoDiscover = false;
    var foto_upload = new Dropzone(".dropzone", {
        url: "<?php echo base_url('message/message_document') ?>",
        maxFilesize: 20,
        method: "post",
        acceptedFiles: ".jpg,.jpeg,.png,.gif",
        paramName: "userfile",
        dictInvalidFileType: "This File Type Not Supported",
        addRemoveLinks: true,
        init: function () {
            var count = 0;
            thisDropzone = this;
            this.on("success", function (file, json) {
                var obj = json;
                $('.previews').append(
                    "<input type='hidden' name='message_in_doc[]' value='" + obj + "'>\n\
            <input type='hidden' name='message_in_width[]' value='" + file.width + "'>\n\
            <input type='hidden' name='message_in_height[]' value='" + file.height + "'>"
                );
            });
        }
    });
</script>