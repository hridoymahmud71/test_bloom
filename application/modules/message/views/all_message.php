<?php $this->load->view('front/headlink'); ?>
<?php $this->load->view('front/top_menu'); ?>
<div class="container">
    <?php $this->load->view('front/head_nav'); ?>
    <div>
        <h4>Current lease: <?= $lease_detail[0]['lease_name']; ?> (365 days to go) <a href="#" data-toggle="modal"
                                                                                      data-target="#myModal"><span
                        class="glyphicon glyphicon-cog" aria-hidden="true"></span></a></h4>
        <div class="container">
            <div class="modal fade" id="myModal" role="dialog">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                            <h4 class="modal-title">Select a Lease or add a new lease</h4>
                        </div>
                        <form action="Message/<?= $property_id; ?>" method="post">
                            <div class="modal-body">
                                <h4>Lease Option</h4>
                                <select class="form-control" name="lease_id">
                                    <option value="new_lease">Add a new lease</option>
                                    <?php foreach ($all_lease as $row) { ?>
                                        <option <?php if ($row['lease_id'] == $lease_detail[0]['lease_id']) {
                                            echo "selected";
                                        } ?> value="<?= $row['lease_id']; ?>"><?= $row['lease_name']; ?></option>
                                    <?php } ?>
                                </select>
                            </div>
                            <div class="modal-footer">
                                <input type="submit" class="btn btn-success" value="Confirm">
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="ss_container">
                <h3 class="extra_heading">Messages</h3>
                <?php if(!$is_archived) { ?>
                <a href="AddMessage/<?= $property_id; ?>/<?= $lease_id; ?>"
                   class="pull-right btn btn-primary">
                    <span class="glyphicon glyphicon-plus" aria-hidden="true"></span> Write a new Message
                </a>
                <?php } ?>
            </div>
            <div class="ss_expensesinvoices_top">
                <div class="container message_list">

                <?= $chunked_message_view ?>

                </div>
            </div>
            <div id="load_more_wrapper" class="text-center">
                <button id="load_more_btn" style="padding: 10px 10px 10px 10px" class="btn btn-primary">Load More</button>
            </div>
            <div class="ss_expensesinvoices_content">
            </div>

        </div>
    </div>
</div>
<?php $this->load->view('front/footerlink'); ?>
<script>
    $(".ss_expanse_top_h").click(function () {
        $(".ss_wxpance_ifo").toggle("slow");
    });
    $("#paid_expense_edit").click(function () {
        $(".ss_expanse_top_h, .ss_wxpance_ifo ").hide(1000);
        $("#paid_expense_editable ").show(1000);
    });
    $("#paid_expense_edit_save, #paid_expense_edit_cancel").click(function () {
        $(".ss_expanse_top_h, .ss_wxpance_ifo ").show(1000);
        $("#paid_expense_editable ").hide(1000);
    });
    $(".delete_expense").click(function () {
        $(".ss_expensesinvoices_c_view, #paid_expense_editable").remove();
    });
</script>

<script>
    $(function () {
        init_pdf_button();
    })

    function init_pdf_button(){
        $('.pdf_btn').on('click', function (e) {
            e.preventDefault();
            //alert("pdf");
            var message_id = $(this).attr('message_id');
            window.location.href = "MessageDetailPdf/" + message_id;
        })
    }
</script>

<script>
    $(function () {

        var page = 1;
        var offset = 0;
        var dots = 0;
        var state = "";

        var count_message = '<?= $count_message?>';
        var limit = '<?= $limit?>';

        count_message = parseInt(count_message);
        limit = parseInt(limit);

        var property_id = '<?= $property_id?>';
        var lease_id = '<?= $lease_id?>';

        $('#load_more_btn').on('click', function (e) {

            e.preventDefault();

            items_shown = countItemShown();
            console.log('items_shown: ' + items_shown + ' compare ' + 'count_message:' + count_message);

            page++;

            offset = (page - 1) * limit;


            if (items_shown < count_message) {

                callAjax("ajaxChunkMessage");

            } else {
                $(this).prop("disabled", true);
                $(this).html('No more results');
            }

        });

        function callAjax(url) {

            $.ajax({
                type: "POST",
                dataType: 'html',
                url: url,
                data: {
                    lease_id: lease_id,
                    property_id: property_id,
                    limit: limit,
                    offset: offset,
                    page: page,
                },

                cache: false,

                beforeSend: function () {
                    state = 'beforeSend';
                    $('#load_more_btn').html('Loading');
                    setInterval(type_loading_dot, 600);
                },

                success: function (result) {

                    state = 'success';
                    //console.log(result);

                    $('#load_more_btn').html('Load More');
                    $(".message_list").append(result);
                    init_pdf_button();

                }
            });
        }

        function type_loading_dot() {

            if (state == 'beforeSend') {

                if (dots < 3) {
                    $('#load_more_btn').append('.');
                    dots++;
                }
                else {
                    $('#load_more_btn').html('Loading');
                    dots = 0;
                }

            }

        }

        function countItemShown() {
            var cnt = 0;
            $(".message_item").each(function () {
                cnt++;
            });

            return cnt;
        }

    })
</script>