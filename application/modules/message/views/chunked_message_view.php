<?php if ($all_message) { ?>
    <?php foreach ($all_message as $row) { ?>
        <div class="col-md-12 message_item"
             style="border-bottom: 2px dashed #ccc;padding: 10px 20px;">
            <!--<h3><?/*= $row['message_id']; */?></h3>-->
            <a style="color: black" href="MessageDetail/<?= $row['message_id']; ?>?only_view=ok">
                <div class="col-md-6 pull-left">
                    <div>
                        <h4><span style="font-size: larger;font-weight: bold"><?= $row['subject']; ?></span></h4>
                        <h4>
                            <span style="color: dodgerblue"><?= $row['user_name']; ?></span>:
                            <span>
                                                    <?= $row['last_comment']; ?>
                                                </span>
                        </h4>
                    </div>
                </div>
                <div class="col-md-6 text-right">
                    <div class="col-md-12">
                        <div class="row">
                            <div class="col-md-6">
                                <button class="btn btn-small pdf_btn"
                                        message_id="<?= $row['message_id']; ?>">
                                                        Download PDF copy
                                </button>
                            </div>
                            <div class="col-md-6">
                                <h4><?= date("d/m/Y h:i:s A", strtotime($row['created_at'])); ?></h4>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12" style="font-size: 14px;">
                                          <span style="padding-right: 15px;">
                                                <span>
                                                  <?= $row['comment_count'] ?>
                                                </span>
                                            <span class="glyphicon glyphicon-envelope" aria-hidden="true"></span>
                                          </span>
                                            <span style="padding-right: 15px;display: none">
                                                <span>
                                                  <?= $row['tenant_count'] ?>
                                                </span>
                                            <span class="glyphicon glyphicon-home" aria-hidden="true"></span>
                                          </span>
                                            <span style="padding-right: 15px;">
                                                <span>
                                                    <?= $row['document_count'] ?>
                                                </span>
                                            <span class="glyphicon glyphicon-paperclip" aria-hidden="true"></span>
                                            </span>
                    </div>
                </div>
            </a>
        </div>
    <?php } ?>
<?php } ?>