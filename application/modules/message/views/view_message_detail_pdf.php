<?php $this->load->view('front/headlink'); ?>

<div class="container">

    <style>
        body {
            background: white !important;
        }
        .lb{
            font-weight: bold;
        }
    </style>


    <div class="row" style="padding-top: 10px">
        <div class="text-center">
            <img class="" src="assets/img/logo.png">
            <h5>
                Conversation History
            </h5>
        </div>
        <h3>
            <?= $this->utility_model->getFullPropertyAddress($property_id); ?>
        </h3>
    </div>


        <div class="row">
            <div class="ss_container" style="margin-top: 0">
                <div class="ss_bound_content" style="margin-top: 0;padding-top: 0">
                    <!-- Nav tabs -->
                    <div class="ss_expensesinvoices">
                        <div class="ss_expensesinvoices_top">
                            <div class="container">
                                <div class="col-md-12">
                                    <? $t = array()?>
                                    <?php foreach ($all_tenant as $row) { ?>
                                        <?php foreach ($all_message_tenant as $rows) { ?>
                                            <?php if ($row['tenant_id'] == $rows['tenant_id']) { ?>
                                                    <?php $t[] = "{$row['user_fname']} {$row['user_lname']} ({$row['email']})" ?>

                                            <?php } ?>
                                        <?php } ?>
                                    <?php } ?>

                                    <?php if(!empty($t)) { ?>
                                       <?php $ts = implode(" & ",$t) ?>
                                        <div class="lb">

                                            To: <?= $ts ?>

                                        </div>
                                    <?php } ?>
                                </div>
                                <div class="col-md-12">
                                    <div class="lb">Subject: <?= $message_detail[0]['subject']; ?></div></div>
                                <?php foreach ($all_message_comment as $com_row) { ?>
                                    <div class="col-md-12">

                                        <div class="lb">From: <?= $message_detail[0]['user_name']; ?></div>
                                        <div class="lb">Date: <?= date("d/m/Y H:i:s A", strtotime($message_detail[0]['created_at'])); ?></div>

                                        <div class="lb">Message:</div><br>
                                        <div class="col-md-12">
                                            <p><?= $com_row['comment']; ?></p><br><br>

                                            <div class="col-md-12">
                                                <?php foreach ($all_message_document as $doc_row) { ?>
                                                    <?php $ext = '';
                                                    $ext = strtolower(pathinfo($doc_row['document'], PATHINFO_EXTENSION));
                                                    $image_file_types = ['jpg', 'jpeg', 'png', 'gif'];
                                                    ?>
                                                    <?php if ($doc_row['message_com_id'] == $com_row['message_comment']) { ?>

                                                        <?php if (in_array($ext, $image_file_types)) { ?>
                                                            <img style="height:100px;width:100px;"
                                                                 src="uploads/message_document/<?= $doc_row['document']; ?>">
                                                        <?php } ?>

                                                        <?php if (!in_array($ext, $image_file_types)) { ?>
                                                            <a target="_blank"
                                                               href="uploads/message_document/<?= $doc_row['document']; ?>"><?= $doc_row['document']; ?></a>
                                                        <?php } ?>

                                                    <?php } ?>
                                                <?php } ?>
                                            </div>
                                        </div>
                                    </div>
                                <?php } ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>



<?php $this->load->view('front/footerlink'); ?> 
