<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');


class Message extends MX_Controller
{

    //public $counter=0;
    function __construct()
    {
        parent::__construct();
        $this->load->model('message_model');
        $this->load->model('lease/lease_model');
        $this->load->model('utility/utility_model');
        // $this->load->helper('inflector');
        // $this->load->library('encrypt');


        ini_set('memory_limit', '128M');
        ini_set('max_execution_time', 900); //300 seconds = 5 minutes

        $this->utility_model->check_auth();
    }

    public function index($property_id)
    {
        if (!isset($property_id)) {
            show_404();
            exit;
        }

        if (!$property_id) {
            show_404();
            exit;
        }
        $this->load->view('dashboard');
    }

    public function all_message($property_id)
    {
        if (!isset($property_id)) {
            show_404();
        }
        if (!$property_id) {
            show_404();
        }

        $limit = 10;

        if ($this->input->post('lease_id')) {
            if (($this->input->post('lease_id')) == 'new_lease') {
                redirect('property/step_three/' . $property_id);
            } else {
                $this->lease_model->update_with_set_value('lease_current_status', '0', 'property_id', $property_id, 'lease');
                $lease_id = $this->input->post('lease_id');
                $data['lease_current_status'] = 1;
                $this->lease_model->update_active_lease($property_id, $lease_id, $data);
            }
        }
        $data['lease_detail'] = $this->message_model->select_with_where('*', 'property_id=' . $property_id . ' AND lease_current_status=1', 'lease');

        //-------------------------------------------------------------------------
        $lease_id = !empty($data['lease_detail']) ? $data['lease_detail'][0]['lease_id'] : 0;
        $is_archived = $this->utility_model->isArchived();
        $archived_string = $this->utility_model->getArchivedString();
        if ($is_archived) {
            $lease_id = $this->utility_model->getArchivedLeaseId();
            $data['lease_detail'][0] = $this->utility_model->getLease($lease_id);
        }
        $data['is_archived'] = $is_archived;
        $data['archived_string'] = $archived_string;
        //-------------------------------------------------------------------------

        $data['all_tenant'] = $this->message_model->get_specific_tenant($lease_id, $property_id);
        $data['property_id'] = $property_id;
        $data['lease_id'] = $lease_id;

        $data['all_message'] = $this->getAllMessages($property_id, $lease_id, $limit, $offset = 0);
        $data['count_message'] = $this->message_model->message_nums('*', 'property_id=' . $property_id . ' AND lease_id=' . $lease_id . '', 'message');


        $data['all_lease'] = $this->message_model->select_with_where('*', 'property_id=' . $property_id . '', 'lease');
        $data['limit'] = $limit;

        $data['chunked_message_view'] = $this->load->view('chunked_message_view', $data, true);
        $this->load->view('all_message', $data);
    }

    public function getAllMessages($property_id, $lease_id, $limit, $offset)
    {
        $all_message = $this->message_model->select_with_where_order_by('*', 'property_id=' . $property_id . ' AND lease_id=' . $lease_id . '', 'message', 'message_id', 'DESC', $limit, $offset);


        if ($all_message) {
            $all_message[0]['query'] = $this->db->last_query();
            foreach ($all_message as $key => $value) {
                $user_name = $this->message_model->get_tenant_name($value['user_id']);
                $all_message[$key]['user_name'] = $user_name[0]['user_fname'] . ' ' . $user_name[0]['user_lname'];

                $last_comment = $this->message_model->select_last_comment($all_message[$key]['message_id']);

                if ($last_comment) {
                    $all_message[$key]['last_comment'] = $last_comment[0]['comment'];
                } else {
                    $all_message[$key]['last_comment'] = '';
                }

                $all_message[$key]['comment_count'] = $this->message_model->getMessageCommentCount($all_message[$key]['message_id']);
                $all_message[$key]['document_count'] = $this->message_model->getMessageDocumentCount($all_message[$key]['message_id']);
                $all_message[$key]['tenant_count'] = $this->message_model->getMessageTenantCount($all_message[$key]['message_id']);

            }
        }


        return $all_message;
    }


    public function ajax_chunk_message()
    {
        $property_id = $_REQUEST['property_id'];
        $lease_id = $_REQUEST['lease_id'];
        $limit = $_REQUEST['limit'];
        $offset = $_REQUEST['offset'];
        $data['all_message'] = $this->getAllMessages($property_id, $lease_id, $limit, $offset);

        $chunked_message_view = $this->load->view('chunked_message_view', $data, true);

        //echo $data['all_message'][0]['query'];
        echo $chunked_message_view;
        exit;
    }

    public function message_detail($message_id)
    {
        if (isset($_GET['only_view'])) {
            if ($_GET['only_view'] == "ok") {
                $this->message_detail_pdf($message_id);
                exit;
            }

        }

        $data['message_detail'] = $this->message_model->select_with_where('*', 'message_id=' . $message_id . '', 'message');

        $user_name = $this->message_model->get_tenant_name($data['message_detail'][0]['user_id']);
        $data['message_detail'][0]['user_name'] = $user_name[0]['user_fname'] . ' ' . $user_name[0]['user_lname'];

        $data['all_tenant'] = $this->message_model->get_specific_tenant($data['message_detail'][0]['lease_id'], $data['message_detail'][0]['property_id']);
        $data['all_message_comment'] = $this->message_model->select_with_where('*', 'message_id=' . $data['message_detail'][0]['message_id'] . '', 'message_comment');
        $data['all_message_document'] = $this->message_model->select_with_where('*', 'message_id=' . $data['message_detail'][0]['message_id'] . '', 'message_document');
        $data['all_message_tenant'] = $this->message_model->select_with_where('*', 'message_id=' . $data['message_detail'][0]['message_id'] . '', 'message_tenant');
        $data['property'] = $this->message_model->select_with_where('*', 'property_id=' . $data['message_detail'][0]['property_id'] . '', 'property');
        $data['property_id'] = $data['message_detail'][0]['property_id'];

        $data['property_with_landlord'] = $this->utility_model->getPropertyWithLandlord($data['property_id']);

        $this->load->view('view_message_detail', $data);
    }

    public function message_detail_pdf($message_id)
    {
        $data['message_detail'] = $this->message_model->select_with_where('*', 'message_id=' . $message_id . '', 'message');

        $user_name = $this->message_model->get_tenant_name($data['message_detail'][0]['user_id']);
        $data['message_detail'][0]['user_name'] = $user_name[0]['user_fname'] . ' ' . $user_name[0]['user_lname'];

        $data['all_tenant'] = $this->message_model->get_specific_tenant($data['message_detail'][0]['lease_id'], $data['message_detail'][0]['property_id']);
        $data['all_message_comment'] = $this->message_model->select_with_where('*', 'message_id=' . $data['message_detail'][0]['message_id'] . '', 'message_comment');
        $data['all_message_document'] = $this->message_model->select_with_where('*', 'message_id=' . $data['message_detail'][0]['message_id'] . '', 'message_document');
        $data['all_message_tenant'] = $this->message_model->select_with_where('*', 'message_id=' . $data['message_detail'][0]['message_id'] . '', 'message_tenant');
        $data['property'] = $this->message_model->select_with_where('*', 'property_id=' . $data['message_detail'][0]['property_id'] . '', 'property');
        $data['property_id'] = $data['message_detail'][0]['property_id'];

        $data['property_with_landlord'] = $this->utility_model->getPropertyWithLandlord($data['property_id']);

        $html = $this->load->view('view_message_detail_pdf', $data, true);

        if (isset($_GET['only_view'])) {
            if ($_GET['only_view'] == "ok") {
                echo $html;
                exit;
            }
        }


        $this->draw_message_detail_pdf($html, $data);
    }

    private function draw_message_detail_pdf($html, $data)
    {
        $this->load->library('MPDF/mpdf');
        $mpdf = new mPDF('win-1252', 'A4', '', '', 5, 5, 16, 60, 5, 5, 'L');
        $mpdf->useOnlyCoreFonts = true;    // false is default
        $mpdf->SetProtection(array('print'));
        $mpdf->SetTitle('Rent Schedule');
        $mpdf->SetAuthor('Bloom');
        $mpdf->SetDisplayMode('fullpage');

        $mpdf->debug = true;

        /*echo $html;
        die();*/

        $name = 'Message_' . $data['message_detail'][0]['message_id'] . $data['message_detail'][0]['lease_id'] . $data['message_detail'][0]['property_id'] . date('YmdHis') . '.pdf';

        //echo  $link;die();
        $mpdf->WriteHTML($html);
        $mpdf->Output($name, 'I');

        exit;

    }

    public function add_message($property_id, $lease_id)
    {
        $data['all_tenant'] = $this->message_model->get_specific_tenant($lease_id, $property_id);
        $data['property_id'] = $property_id;
        $data['lease_id'] = $lease_id;
        $this->load->view('add_message', $data);
    }

    public function upload_new_message()
    {
        $message_id = $this->input->post('message_id');
        $cmt_data['message_id'] = $this->input->post('message_id');
        $user_full_name = $this->input->post('user_full_name');
        $subject = $this->input->post('subject');
        $cmt_data['comment'] = $this->input->post('comment');
        $tenant_id = $this->input->post('tenant_id');
        $message_com_id = $this->message_model->insert_ret('message_comment', $cmt_data);
        // $message_com_id = 1;
        $message_in_doc = $this->input->post('message_in_doc');

        $ms = $this->message_model->getMessage($message_id);

        //--------------------------------
        $property_with_landlord = $this->utility_model->getPropertyWithLandlord($ms['property_id']);
        $tenants_with_lease_detail = $this->utility_model->getTenantsWithLeaseDetails($ms['lease_id']);

        $full_property_address = $this->utility_model->getFullPropertyAddress($ms['property_id']);

        $landlord_name = "";
        $landlord_phone = "";
        if (!empty($property_with_landlord)) {
            $landlord_name = $property_with_landlord['user_fname'] . ' ' . $property_with_landlord['user_lname'];
            $landlord_phone = $property_with_landlord['phone'];
        }
        $tenant_names = "";
        if (!empty($tenants_with_lease_detail)) {
            $user_fname = array_column($tenants_with_lease_detail, 'user_fname');
            $user_lname = array_column($tenants_with_lease_detail, 'user_lname');

            $tenant_names = "";
            $tenant_names_array = (array_map("self::joinNameParts", $user_fname, $user_lname));

            if (count($tenant_names_array) > 0) {
                $tenant_names = implode(' & ', $tenant_names_array);
            }
        }
        //--------------------------------


        $all_doc_file = array();
        if ($message_in_doc) {
            foreach ($message_in_doc as $key => $value) {
                $doc_data['message_id'] = $message_id;
                $doc_data['message_com_id'] = $message_com_id;
                $doc_data['document'] = $value;
                // $all_doc_file[] = 'D:\xampp\htdocs\bloom\uploads\message_document\''.$all_doc_file;
                $all_doc_file[] = FCPATH . 'uploads/message_document/' . $value;
                $this->message_model->insert('message_document', $doc_data);
            }
        }

        for ($i = 0; $i < count($tenant_id); $i++) {
            // $tnt_data['message_id'] = $message_id;
            // $tnt_data['tenant_id'] = $tenant_id[$i];
            // $this->message_model->insert('message_tenant',$tnt_data);

            $tenant_email = $this->message_model->get_tenant_name($tenant_id[$i]);
            $landlord_name = $user_full_name;

            $email = $tenant_email[0]['email'];
            // $this->load->model('forget_model');

            // $user_id=$info[0]['user_id'];
            $this->load->helper('html');
            $this->load->library('email');

            $this->load->library('session');
            $newdata = array(
                'email' => $email
            );
            $this->session->set_userdata($newdata);

            $site_name = $this->config->item('site_name');
            $site_email = $this->config->item('site_email');

            $this->load->library('email');
            $this->email->clear();
            $this->email->initialize(array('priority' => 1));
            $this->email->set_newline("\r\n");

            $this->email->from($site_email, $site_name);
            $this->email->to($email);
            $this->email->cc('mahmud@sahajjo.com');
            $this->email->bcc('mahmud@sahajjo.com');
            $base = $this->config->base_url();
            $img_src = "{$base}assets/img/logo.png";
            $img = "<img src='$img_src'><br>";

            $message = "$img<br>";

            if (!empty($property_with_landlord)) {
                $message .= "Hi {$tenant_names} ,<br>
                         Your landlord {$landlord_name} has sent you a message.<br>
                         --------------------------------- <br>                      
                         <b>Tenants:</b> $tenant_names<br>
                         <b>Address:</b> {$full_property_address} <br><br>";
                $message .= "<span style='font-style: italic;font-weight:bold'>{$cmt_data['comment']}</span><br><br>";
                $message .= "Should you need to contact {$property_with_landlord['user_fname']} {$property_with_landlord['user_lname']}, Please email {$property_with_landlord['email']} or call {$property_with_landlord['phone']}<br>";
                $message .= "This email has been generated by {$site_name} Software";
            }

            $server_base_url = $this->config->item('server_base_url');
            $message .= "<p><img src='{$server_base_url}/assets/img/logo.png' alt='logo'></p>";


            $this->email->subject($subject);
            $this->email->message($message);
            foreach ($all_doc_file as $key => $val) {
                $this->email->attach($all_doc_file[$key]);
            }
            $this->email->set_mailtype("html");
            @$this->email->send();

            //echo $message . '<br>';

        }
        //exit;
        redirect('MessageDetail/' . $message_id);
    }

    private function joinNameParts($fname, $lname)
    {
        return $fname . ' ' . $lname;
    }


    public function insert_message()
    {
        $property_id = $this->input->post('property_id');
        $lease_id = $this->input->post('lease_id');
        $tenant_id = $this->input->post('tenant_id');

        $property_owner = $this->utility_model->getPropertyOwner($property_id);

        $full_property_address = $this->utility_model->getFullPropertyAddress($property_id);

        //--------------------------------
        $property_with_landlord = $this->utility_model->getPropertyWithLandlord($property_id);
        $tenants_with_lease_detail = $this->utility_model->getTenantsWithLeaseDetails($lease_id);

        $landlord_name = "";
        $landlord_phone = "";
        if (!empty($property_with_landlord)) {
            $landlord_name = $property_with_landlord['user_fname'] . ' ' . $property_with_landlord['user_lname'];
            $landlord_phone = $property_with_landlord['phone'];
        }
        $tenant_names = "";
        if (!empty($tenants_with_lease_detail)) {
            $user_fname = array_column($tenants_with_lease_detail, 'user_fname');
            $user_lname = array_column($tenants_with_lease_detail, 'user_lname');

            $tenant_names = "";
            $tenant_names_array = (array_map("self::joinNameParts", $user_fname, $user_lname));

            if (count($tenant_names_array) > 0) {
                $tenant_names = implode(' & ', $tenant_names_array);
            }
        }
        //--------------------------------

        if (count($tenant_id) == 0) {
            $this->session->set_flashdata('error', 'error');
            $this->session->set_flashdata('select_a_tenant', 'You have to select a tenant');
        }

        if (count($tenant_id) > 0) {
            $data['subject'] = $this->input->post('subject');
            $data['created_at'] = date("Y-m-d h:i:s");
            $data['user_id'] = $this->session->userdata('user_id');
            $user_full_name = $this->message_model->get_tenant_name($data['user_id']);

            $data['property_id'] = $property_id;
            $data['lease_id'] = $lease_id;
            $message_id = $this->message_model->insert_ret('message', $data);

            $cmt_data['message_id'] = $message_id;
            $cmt_data['comment'] = $this->input->post('comment');
            $message_com_id = $this->message_model->insert_ret('message_comment', $cmt_data);

            $this->session->set_flashdata('success', 'success');
            $this->session->set_flashdata('message_add_success', 'Message sent successfully');

            $message_in_doc = $this->input->post('message_in_doc');
            $all_doc_file = array();
            if ($message_in_doc) {
                foreach ($message_in_doc as $key => $value) {
                    $doc_data['message_id'] = $message_id;
                    $doc_data['message_com_id'] = $message_com_id;
                    $doc_data['document'] = $value;
                    // $all_doc_file[] = 'D:\xampp\htdocs\bloom\uploads\message_document\''.$all_doc_file;
                    $all_doc_file[] = FCPATH . 'uploads/message_document/' . $value;
                    $this->message_model->insert('message_document', $doc_data);
                }
            }

            $tenant_id = $this->input->post('tenant_id');
            for ($i = 0; $i < count($tenant_id); $i++) {
                $tnt_data['message_id'] = $message_id;
                $tnt_data['tenant_id'] = $tenant_id[$i];
                $this->message_model->insert('message_tenant', $tnt_data);

                $tenant_email = $this->message_model->get_tenant_name($tenant_id[$i]);
                $landlord_name = $user_full_name[0]['user_fname'] . ' ' . $user_full_name[0]['user_lname'];

                $email = $tenant_email[0]['email'];


                $this->load->helper('html');
                $this->load->library('email');

                $this->load->library('session');
                $newdata = array(
                    'email' => $email
                );
                $this->session->set_userdata($newdata);

                $site_name = $this->config->item('site_name');
                $site_email = $this->config->item('site_email');

                $this->load->library('email');
                $this->email->initialize(array('priority' => 1));
                $this->email->set_newline("\r\n");

                $this->email->from($site_email, $site_name);
                $this->email->to($email);
                $base = $this->config->base_url();

                $img_src = "{$base}assets/img/logo.png";
                $img = "<img src='$img_src'><br>";

                $message = "";

                if (!empty($property_with_landlord)) {
                    $message .= "Hi {$tenant_names} ,<br>
                         Your landlord {$landlord_name} has sent you a message.<br>
                         --------------------------------- <br>                      
                         <b>Tenants:</b> $tenant_names<br>
                         <b>Address:</b> {$full_property_address} <br><br>";
                    $message .= "<span style='font-style: italic;font-weight:bold'>{$cmt_data['comment']}</span><br><br>";
                    $message .= "Should you need to contact {$property_with_landlord['user_fname']} {$property_with_landlord['user_lname']}, Please email {$property_with_landlord['email']} or call {$property_with_landlord['phone']}<br>";
                    $message .= "This email has been generated by {$site_name} Software<br><br>{$img}";
                }

                $this->email->subject($data['subject']);
                $this->email->message($message);
                foreach ($all_doc_file as $key => $val) {
                    $this->email->attach($all_doc_file[$key]);
                }
                $this->email->set_mailtype("html");
                @$this->email->send();

                //echo $message . '<br>';

            }

        }


        redirect('AddMessage/' . $property_id . '/' . $lease_id);
    }


    public function message_document()
    {
        $files = $_FILES;
        if (isset($_FILES['userfile']) && !empty($_FILES['userfile']['name'])) {
            $config['remove_spaces'] = TRUE;
            $config['encrypt_name'] = TRUE;
            $config['upload_path'] = 'uploads/message_document';
            $config['allowed_types'] = 'jpg|png|gif|jpeg|xls|doc|docx|pdf|csv|avi|mp4|flv|mpg';
            $config['max_size'] = '0';
            $this->load->library('upload', $config);
            $this->upload->initialize($config);
            if (!$this->upload->do_upload('userfile')) {
                $status = 'error';
                $msg = $this->upload->display_errors('', '');
//                echo json_encode($msg);
            } else {
                $fileinfo = $this->upload->data();
                echo $fileinfo['file_name'];
            }
        }
    }


}
