/*subscriptions*/
SELECT u.user_id,sc.stripe_customer_id,scs.stripe_subscription_id, u.user_fname,u.user_lname,u.email FROM stripe_customer as sc join user as u on u.user_id= sc.user_id left join stripe_customer_subscription as scs on u.user_id=scs.user_id

/*cancelled subscriptions*/
SELECT u.user_id,sc.stripe_customer_id,sccs.stripe_subscription_id, u.user_fname,u.user_lname,u.email FROM stripe_customer as sc join user as u on u.user_id= sc.user_id left join stripe_customer_cancelled_subscription as sccs on u.user_id=sccs.user_id