<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Fb_login extends MX_Controller {
//CI_Controller
    //public $counter=0;
    function __construct() {
        parent::__construct();
        $this->load->model('fb_login_model');
        $this->load->model('forget/forget_model');
        // $this->load->model('home/home_model');
        // $this->load->helper('inflector');
        // $this->load->library('encrypt');
        


    }

    public function index()
     {
        //$config['appId']='1309905509056541';
    //     $config['secret']='7c87971642afe0551a69e75305867f92';
    //     $access_token_url = "https://graph.facebook.com/oauth/access_token?client_id=".$config['appId']."&client_secret=".$config['secret']."&grant_type=client_credentials";

    //     $access_token = file_get_contents($access_token_url);
    //   echo '<pre>';print_r($access_token);die();
        
        $user = $this->facebook->request('get', '/me');
        //$response = $fb->get('/me?fields=id,name', $token);
        //$user = $response->getGraphUser();
            if (!isset($user['error']))
            {
                $data['user'] = $user;
            }
            // echo '<pre>';print_r($user);die();
       if ($this->facebook->is_authenticated())
        {
            // User logged in, get user details
            $user = $this->facebook->request('get', '/me?fields=id,name,email');
            if (!isset($user['error']))
            {
                $chk_email_exist = $this->fb_login_model->chk_email($user['email']);
                if(count($chk_email_exist)>0)
                {
                    echo "<pre>";print_r($chk_email_exist);die();
                    if($chk_email_exist[0]['token_id']=='')
                    {
                        $val['token_id'] = $user['id'];
                        $this->fb_login_model->update_function('email',$user['email'],'user',$val);
                    }

                    $this->session->set_userdata('user_id', $chk_email_exist[0]['user_id']);
                    $this->session->set_userdata('email' , $chk_email_exist[0]['email']);
                    $this->session->set_userdata('role_type' ,$chk_email_exist[0]['role_type']);
                    $this->session->set_userdata('user_fname' , $chk_email_exist[0]['user_fname']);
                    $this->session->set_userdata('user_lname' , $chk_email_exist[0]['user_lname']);

                    redirect('property','refresh');
                }
                else
                {
                    $reg_data['user_fname'] = $user['name'];
                    $reg_data['email'] = $user['email'];
                    // $reg_data['password']=$this->encryptIt('123456');
                    $reg_data['role_type'] = 1;
                    $user_id = $this->fb_login_model->insert_ret('user',$reg_data);

                    $this->load->helper('html');    
                    $this->load->library('email');
                    
                     $this->load->library('session');
                                $newdata = array(
                                   'email'  => $user['email']
                               );
                     $this->session->set_userdata($newdata);

                    $tmp_password=$this->forget_model->update_tmp_password($user_id);

                    $site_name = $this->config->item('site_name');
                    $site_email = $this->config->item('site_email');

                    $this->load->library('email');
                    $this->email->set_newline("\r\n");

                    $this->email->from($site_email,$site_name);
                    $this->email->to($user['email']);
                    $base=$this->config->base_url();
                    $message=heading('Your Email:');
                    $message.=$user['email'];
                    $message.=br(2);
                    $message.=heading('Your Password : 123456');
                    $message.=br(2);
                    $message.="Thank You";
                    //$message="<a href='fsdkfjdsfjsdfjdsf'>sdfsdfsdf</a>";
                    $this->email->subject('Password Recovery');
                    $this->email->message($message);
                    $this->email->set_mailtype("html");
                    $this->email->send();

                    $this->session->set_userdata('log_scc','Please check your email to get your password');
                    redirect('login','refresh');
                }
            }
        }
    }

    public function login_check() 
    {

        $email = $this->input->post('email');
        $password = $this->encryptIt($this->input->post('password'));


        $res = $this->fb_login_model->check_login($email, $password);
        //print_r($res);
        if (count($res) > 0) 
        {
            
            $this->session->set_userdata('login_id', $res[0]['id']);
            $this->session->set_userdata('reg_id' , $res[0]['reg_id']);
            $this->session->set_userdata('type' ,$res[0]['type']);
            $this->session->set_userdata('district_id', $res[0]['district_id']);

            // $this->session->set_userdata('login_id', 1);
            // $this->session->set_userdata('reg_id' , 6);
            // $this->session->set_userdata('type' ,0);
            // $this->session->set_userdata('district_id', 0);
           

            $name=$this->fb_login_model->get_name($res[0]['id']);
            
            $this->session->set_userdata('name' , $name[0]['name']);
        

            $this->session->set_userdata('login_scc', 'Welcome to Admin Dashboard.');

            if($res[0]['type']==0 || $res[0]['type']==1)
            {
                redirect('admin','refresh');
            }

            elseif($res[0]['type']==2)
            {
                redirect('market_admin','refresh');
            }

            elseif($res[0]['type']==3)
            {
                redirect('shop_admin','refresh');
            }    
        }

        
        else 
        {
            $this->session->set_userdata('log_err','Email or Password is Incorrect.');
        	redirect('login', 'refresh');
        }
        
    }

    public function ajax_login_check()
    {
        $email=$this->input->post('email');
        $password = $this->encryptIt($this->input->post('password'));

        $res = $this->fb_login_model->check_login($email, $password);
        if(count($res)>0)
        {
            echo 1;
        }
        else
        {
            echo 0;
        }
    }

    public function fb_login()
    {
        if ($this->facebook->is_authenticated())
        {
            // User logged in, get user details
            $user = $this->facebook->request('get', '/me?fields=id,name,email');
            if (!isset($user['error']))
            {
                $data['user'] = $user;
            }
            var_dump($user);
        }
    }

    public function pass_gen()
    {
        $ss=$this->encryptIt('123456');
        echo $ss;
    }

    public function change_password()
    {
    	$email = $this->input->post('email');
    	$pass = $this->encryptIt($this->input->post('pass_change'));
        $data = array('password' => $pass,);
        $this->fb_login_model->password_change($email,$data);
        $this->session->set_userdata('log_scc','Successfully changed your password! Login with your "NEW" password.');
        redirect('login/index', 'refresh');
    }

   public function registration()
   {
     $data['comp_name']=$this->input->post('comp_name');
     $data['comp_title']=$this->input->post('comp_title');
     $data['comp_address']=$this->input->post('comp_address');
     $data['comp_phone']=$this->input->post('comp_phone');

     $this->db->insert('company',$data);
   }


    function decryptIt($q) {
        $cryptKey = 'Lf6Q5htqdgnSn0AABqlsSddj1QNu0fJs';
        $qDecoded = rtrim(mcrypt_decrypt(MCRYPT_RIJNDAEL_256, md5($cryptKey), base64_decode($q), MCRYPT_MODE_CBC, md5(md5($cryptKey))), "\0");
        return( $qDecoded );
    }

    function encryptIt($q) {
        $cryptKey = 'Lf6Q5htqdgnSn0AABqlsSddj1QNu0fJs';
        $qEncoded = base64_encode(mcrypt_encrypt(MCRYPT_RIJNDAEL_256, md5($cryptKey), $q, MCRYPT_MODE_CBC, md5(md5($cryptKey))));
        return( $qEncoded );
    }

}
