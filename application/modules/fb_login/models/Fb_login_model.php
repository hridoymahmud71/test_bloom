<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Fb_login_model extends CI_Model {

    public function __construct() {
        parent::__construct();
    }


    public function chk_email($email)
    {
        $this->db->select('*');
        $this->db->from('user');
        $this->db->where('email',$email);

        $result = $this->db->get();
        return $result->result_array();
    }

    public function insert_ret($tablename, $tabledata)
    {
        $this->db->insert($tablename, $tabledata);
        return $this->db->insert_id();
    }

    public function insert($table_name,$data)
    {
        $this->db->insert($table_name, $data);
    }

    public function update_function($columnName, $columnVal, $tableName, $data)
    {
        $this->db->where($columnName, $columnVal);
        $this->db->update($tableName, $data);
    }

    public function update_tmp_password($user_id)
    {
        $this->load->helper('string');
        $tmp_password=random_string('alnum', 40);
        $data = array(

               'tmp_password' => $tmp_password
            );
                $this->load->library('session');
                
                $newdata = array(
                   'tmp_password'  => $tmp_password
               );

        $this->db->where('user_id', $user_id);
        $this->db->update('user', $data); 
        $this->session->set_userdata($newdata);
        return $tmp_password;
    }


    public function check_login($email, $pass) {
        $this->db->select('*');
        $this->db->from('login');
        $this->db->where('email', $email);
        $this->db->where('password', $pass);
        $this->db->where('status', 1);
        $result = $this->db->get();
        return $result->result_array();
    }

    public function get_name($id)
    {
        $this->db->select('*');
        $this->db->from('registration');
        $this->db->where('id', $id);
        $result = $this->db->get();
        return $result->result_array();
    }

     public function password_change($email,$data)
    {
        $this->db->where('email',$email);
        $this->db->update('login',$data);
    }

}

?>