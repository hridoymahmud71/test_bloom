<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Logout extends MX_Controller {
 //public $counter=0;
    function __construct() {
        parent::__construct();
       
    }
	
	public function index($id='')
	{
		
		$this->session->unset_userdata('user_id');
		$this->session->unset_userdata('email');
		$this->session->unset_userdata('user_fname');
		$this->session->unset_userdata('user_lname');
		$this->session->unset_userdata('role_type');
		$this->session->set_userdata('log_scc','Successfully Logged-Out!');
		if($id=='home')
		{
			redirect('home','refresh');
		}
		else
		{
			redirect('login','refresh');
		}
	}

}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */