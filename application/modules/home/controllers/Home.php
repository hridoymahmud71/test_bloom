<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Home extends MX_Controller {


    function __construct()
	{
		$this->load->model('home_model');
		// $this->load->model('search/search_model');

		date_default_timezone_set('Asia/Dhaka');
		if($this->session->userdata('language_select')=='bangla')
        {
            $this->lang->load('front', 'bangla');
        }
        else
        {
            $this->lang->load('front', 'english');
        } 

    }
	public function index()
	{
		// $data['head_data']=$this->head_data();
  //       $data['top_magazine']=$this->home_model->select_order_limit_condition('*','magazine_post','status=1','total_seen','DESC',12);
  //       $data['division_list']=$this->home_model->select_all_acending('divisions','position');
  //       $data['speciality_list']=$this->home_model->select_all('doctor_specialization_list');
  //       $data['count_sp']=array();
  //       foreach ($data['speciality_list'] as $key => $value) 
  //       {
  //          $data['count_sp'][$key]=$this->home_model->count_result_row('doctor_details','FIND_IN_SET('.$value['id'].',doctor_details.specialist)>0');
  //       }
  //       $data['feature_in']=array();
  //       //echo "<pre>";print_r($data['count_sp']);die();
		$this->load->view('index');
        // $this->load->view('welcome_message');
	}

 //    public function search_submit()
 //    {
 //        $division_name=$this->input->post('division_name');
 //        $type_name=$this->input->post('type_name');
 //        $sp_name=$this->input->post('sp_name');

 //        $this->session->set_userdata('top_division_name',$division_name);
 //        $this->session->set_userdata('top_type_name',$type_name);
 //        $this->session->set_userdata('top_sp_name',$sp_name);
 //        redirect('search/'.$division_name.'/'.$type_name.'/'.$sp_name,'refresh');
 //    }
	


 //    public function get_user_nofication()
 //    {
 //        $login_id=$this->session->userdata('login_id');
 //        //$notification_list=$this->home_model->select_();

 //    }
	// public function head_data()
 //    {
 //        $id = 1;
 //        $data=$this->home_model->select_with_where('*', 'id='.$id, 'company');
 //        return $data;
 //    }


}
