<?php $this->load->view('front/headlink');?>
<body data-spy="scroll" data-target="#myScrollspy" data-offset="20">

    <!--  Navbar -->
    <?php $this->load->view('front/head_nav');?>
    <!-- Hero area -->
    <div class="hero-area-3">
        <div class="overlay-1">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12">
                        <div class="patient-signup-area">
                            
                            <h4 class="font_class_4">Registration Confirmation</h4>
                            <div class="patient-signup-area-details">
                                <form action="registration/verification_code_match" method="post">
                                	
                                	<?php if($this->session->userdata('scc_alt')){ ?>
			                        <div class="alert alert-success alert-dismissable">
			                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>
			                            <a href="javascript:;" class="alert-link"><?=$this->session->userdata('scc_alt');?></a>
			                        </div>
			                        <?php } $this->session->unset_userdata('scc_alt');?>

			                        <?php if($this->session->userdata('err_alt')){ ?>
			                        <div class="alert alert-danger alert-dismissable">
			                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>
			                            <a href="javascript:;" class="alert-link"><?=$this->session->userdata('err_alt');?></a>
			                        </div>
			                        <?php } $this->session->unset_userdata('err_alt');?>
                                    
                                    <div class="form-group">
                                        <label for="fullName">Verification Code</label>
                                        <input  type="text"  name="verify_code" class="form-control" id="verify_code" placeholder="Verification Code">
                                    </div>

                                    <div class="form-group">
                                        <label for="fullName">Full Name</label>
                                        <input disabled="" type="text" value="<?php if($this->session->userdata('tmp_name')){echo $this->session->userdata('tmp_name');}?>" name="name" class="form-control" id="username" placeholder="Full Name">
                                    </div>
                                    <div class="form-group">
                                        <label for="email">Email</label>
                                        <input disabled="" value="<?php if($this->session->userdata('tmp_email')){echo $this->session->userdata('tmp_email');}?>" id="email" name="email" type="email" class="form-control" id="email" placeholder="Email">
                                    </div>
                                    <div class="form-group">
                                        <label for="phoneNumber">Phone Number</label>
                                        <p><span class="fixedno-patient">+880</span></p>
                                        <input type="hidden" value="880" name="country_code" id="country_code">
                                        <input disabled="" value="<?php if($this->session->userdata('tmp_mobile_no')){echo $this->session->userdata('tmp_mobile_no');}?>" name="mobile_no" type="text" class="form-control" id="phonenumber" placeholder="Phone Number">
                                    </div>
                                   
                                    <button type="submit" class="btn btn-event">Confirm Verification</button>
                                   
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--Big footer-->
    <?php $this->load->view('front/footer');?>
    <!-- Latest compiled and minified JavaScript -->
   <?php $this->load->view('front/footerlink');?>


</body>

</html>
