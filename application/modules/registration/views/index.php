<!DOCTYPE html>
<base href="<?php echo base_url();?>">
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Rent Simple</title>
    <link href="assets/css/bootstrap.min.css" rel="stylesheet">
    <link href="assets/css/style.css" rel="stylesheet">
<!--[if lt IE 9]>
<script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
<![endif]-->
</head>
<body>
    <div class="login">  
        <div class="container">
            <div class="row">

                <div class="main">
                    <div class="logo"> <img src="assets/img/logo.png"/></div>
                    <h3>Set up your first Rent Simple Property now!<br> or <br><a href="login">Sign In</a></h3>
                    <div class="row">

                        <span style="font-style: italic;color:crimson">*Please note: The email address & phone number you provide the tenant will have access to.</span>
                        <br><br>

                        <form role="form" onsubmit="return check_registration()" action="registration/add_registration_post" method="post">
                            
                            <?php if($this->session->flashdata('scc_alt')){ ?>
                            <div class="alert alert-success alert-dismissable">
                                <button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>
                                <a href="javascript:;" class="alert-link"><?=$this->session->flashdata('scc_alt');?></a>
                            </div>
                            <?php } ?>

                            <?php if($this->session->flashdata('err_alt')){ ?>
                            <div class="alert alert-danger alert-dismissable">
                                <button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>
                                <a href="javascript:;" class="alert-link"><?=$this->session->flashdata('err_alt');?></a>
                            </div>
                            <?php } ?>
                                    
                            <div class="alert alert-danger" id="err_div" style="display: none">
                              <strong id="err_msg"></strong> 
                            </div>
                            <div class="form-group">
                                <label for="inputFristName">First Name</label>
                                <input type="text" required name="user_fname" class="form-control" id="inputFristName">
                            </div>
                            <div class="form-group">
                                <label for="inputLastName">Last Name</label>
                                <input type="text" required name="user_lname" class="form-control" id="inputLastName">
                            </div>
                            <div class="form-group">
                                <label for="inputEmail">Email Address</label>
                                <input type="email" required name="email" class="form-control" id="inputEmail">
                            </div>
                            <div class="form-group">
                                <label for="inputPhonw">Phone</label>
                                <input type="text" required name="phone" class="form-control" id="inputPhonw">
                            </div>
                            <div class="form-group">
                                <label for="inputPassword">Create Password</label>
                                <input type="password" required name="password" class="form-control" id="inputPassword">
                            </div>
                            <button type="submit" class="btn btn btn-primary ">
                                Create Account
                            </button>
                        </form>
                    </div>
                    <!--<div class="brand">
                        <h4> As seen on:</h4>
                        <img src="assets/img/logo6.jpg" alt="logo"/>
                        <img src="assets/img/logo7.jpg" alt="logo"/>
                    </div>-->
                </div>
            </div>
        </div>
    </div>

    <script>
        function check_registration() 
        {
            $("#err_div").hide();
            //var regex = new RegExp("^(?:\\+?88)?8801[15-9]\\d{8}$");
            var ok=true;
            var fname= $("#inputFristName").val();
            var lname= $("#inputLastName").val();
            var email= $("#inputEmail").val();
            var password= $("#inputPassword").val();

            if(fname=='' || lname=='' || email=='' || password=="")
            {
                $("#err_div").show();
                $("#err_msg").html('* All fields must be filled out');
                ok=false;
            }
            return ok;
        }
    </script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <script src="assets/js/bootstrap.min.js"></script>
    <script src="assets/js/custom.js"></script>
</body>
</html>
