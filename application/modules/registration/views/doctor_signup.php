<?php $this->load->view('front/headlink');?>
<body data-spy="scroll" data-target="#myScrollspy" data-offset="20">

    <!--  Navbar -->
    <?php $this->load->view('front/head_nav');?>
    <!-- Hero area -->
    <div class="hero-area-3">
        <div class="overlay-1">
            <div class="container">
                <div class="row">
                    <div class="col-md-6">

                    </div>
                    <div class="col-md-6">
                        <div class="signup-area clearfix">
                            <div class="signup-online-tab-area clearfix">
                                <ul class="nav  nav-tabs tabs-hori">
                                    <li class="<?php if($tab_active=='login'){echo 'active';}?>"><a href="#login" data-toggle="tab">Login</a></li>
                                    <li class="<?php if($tab_active=='signup'){echo 'active';}?>"><a href="#register" data-toggle="tab">Register</a></li>
                                </ul>
                            </div>
                            <div class="tab-content">
                                <div class="tab-pane <?php if($tab_active=='login'){echo 'active';}?>" id="login">
                                    <div class="login-area">
                                        <form onsubmit="return check_registration()" action="login/check_login" method="post">

                                        <?php if($this->session->userdata('scc_alt')){ ?>
                                        <div class="alert alert-success alert-dismissable">
                                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>
                                            <a href="javascript:;" class="alert-link"><?=$this->session->userdata('scc_alt');?></a>
                                        </div>
                                        <?php } $this->session->unset_userdata('scc_alt');?>

                                        <?php if($this->session->userdata('err_alt')){ ?>
                                        <div class="alert alert-danger alert-dismissable">
                                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>
                                            <a href="javascript:;" class="alert-link"><?=$this->session->userdata('err_alt');?></a>
                                        </div>
                                        <?php } $this->session->unset_userdata('err_alt');?>
                                            <div class="form-group">
                                                <label for="fullName">Email or Phone</label>
                                                <input autocomplete="new-password" type="text" class="form-control" id="username" placeholder="Full Name">
                                            </div>
                                            <div class="form-group">
                                                <label for="password">Password</label>
                                                <input  type="password" autocomplete="new-password" class="form-control" id="password_log" placeholder="Password">
                                                <p><a href="#" class="font_class_p psstrong pull-right">Forgot Password?</a> </p>
                                            </div>
                                            
                                            <button type="submit" class="btn btn-event">Login</button>
                                            <h5 class="font_class_5 text-center">OR</h5>
                                            <button type="submit" class="btn btn-fb"><i class="fa fa-facebook-official fa-btn-fb" aria-hidden="true"></i>Connect with facebook</button>
                                        </form>
                                        <!-- /form -->
                                    </div>
                                </div>
                                <div class="tab-pane <?php if($tab_active=='signup'){echo 'active';}?>" id="register">
                                    <div class="register-area">
                                    <form onsubmit="return check_registration()" action="registration/add_registration_post" method="post">

                                            <?php if($this->session->userdata('scc_alt')){ ?>
                                    <div class="alert alert-success alert-dismissable">
                                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>
                                        <a href="javascript:;" class="alert-link"><?=$this->session->userdata('scc_alt');?></a>
                                    </div>
                                    <?php } $this->session->unset_userdata('scc_alt');?>

                                    <?php if($this->session->userdata('err_alt')){ ?>
                                    <div class="alert alert-danger alert-dismissable">
                                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>
                                        <a href="javascript:;" class="alert-link"><?=$this->session->userdata('err_alt');?></a>
                                    </div>
                                    <?php } $this->session->unset_userdata('err_alt');?>

                                            <div class="alert alert-danger" id="err_div" style="display: none">
                                              <strong id="err_msg"></strong> 
                                            </div>
                                            <input type="hidden" value="3" name="user_type">
                                            <div class="form-group">
                                                <label for="fullName">Full Name(*)</label>
                                                <p><span class="fixedno">Dr.</span></p>
                                                <input type="text" value="<?php if($this->session->userdata('tmp_name')){echo $this->session->userdata('tmp_name');}?>" name="name" class="form-control" id="fullname" placeholder="Full Name">
                                            </div>
                                            <div class="form-group">
                                                <label for="email">Email(*)</label>
                                                <input value="<?php if($this->session->userdata('tmp_email')){echo $this->session->userdata('tmp_email');}?>" id="email" name="email" type="email" class="form-control" id="email" placeholder="Email">
                                            </div>
                                            <div class="form-group">
                                                <label for="phoneNumber">Phone Number</label>
                                                <input type="hidden" value="880" name="country_code" id="country_code">
                                                <p><span class="fixedno">+880</span></p>
                                                <input value="<?php if($this->session->userdata('tmp_mobile_no')){echo $this->session->userdata('tmp_mobile_no');}?>" name="mobile_no" type="text" class="form-control" id="phonenumber" placeholder="Phone Number">
                                            </div>
                                            <input type="hidden" value="registration/doctor" name="redirect_url">
                                            <div class="form-group">
                                                <label for="password">Password</label>
                                                <input name="password" type="password" class="form-control" id="password" placeholder="Password">
                                                
                                            </div>
                                            <div class="form-group">
                                                <label for="password">Confirm Password(*)</label>
                                                <input type="password" class="form-control" id="confirm_password" placeholder="Password">
                                            </div>
                                            <button type="submit" class="btn btn-event">Register</button>

                                        </form>
                                        <!-- /form -->
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--Big footer-->
    <?php $this->load->view('front/footer');?>
    <!-- Latest compiled and minified JavaScript -->
   <?php $this->load->view('front/footerlink');?>

    

    <script>
		function check_registration() 
		{
			$("#err_div").hide();
			//var regex = new RegExp("^(?:\\+?88)?8801[15-9]\\d{8}$");
			var ok=true;
			var name= $("#fullname").val();
			var email= $("#email").val();
			var phone= $("#phonenumber").val();
			var c_code= $("#country_code").val();
			//var mb_no=phone;
			var password= $("#password").val();
			var con_password= $("#confirm_password").val();
			//var is_phone = regex.test(mb_no);

			if(name=='' || email=='' || phone=='' || password=="")
			{
				$("#err_div").show();
				$("#err_msg").html('* Fields must be filled out');
				ok=false;

			}
			

			if(password!=con_password)
			{
				$("#err_div").show();
				$("#err_msg").html('Password Not Matched');
				ok=false;

			}
			return ok;
		}

	</script>
</body>

</html>
