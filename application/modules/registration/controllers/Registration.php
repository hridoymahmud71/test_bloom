<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Registration extends MX_Controller
{
    function __construct()
    {
        if ($this->session->userdata('language_select') == 'bangla') {
            $this->lang->load('admin', 'bangla');
        } else {
            $this->lang->load('admin', 'english');
        }

        $this->load->model('admin/admin_model');
        $this->load->model('registration_model');
        $this->load->model('utility/utility_model');
        // $this->load->model('home/home_model');
        //$this->load->library('sendsms_library');
        //$this->load->library('sms_api');

    }

    public function index()
    {
        // $data['head_data']=$this->head_data();
        $this->load->view('index');
    }

    // public function doctor()
    // {
    //     $data['head_data']=$this->head_data();
    //     $data['tab_active']='signup';
    //     $this->load->view('doctor_signup',$data);
    // }

    public function add_registration_post()
    {
        $data['user_fname'] = $this->input->post('user_fname');
        $data['user_lname'] = $this->input->post('user_lname');
        $data['email'] = $this->input->post('email');
        $data['phone'] = $this->input->post('phone');
        $data['password'] = $this->encryptIt($this->input->post('password'));
        $data['role_type'] = 1;
        $this->form_validation->set_rules('user_fname', 'First Name', 'required');
        $this->form_validation->set_rules('user_lname', 'Last Name', 'required');
        $this->form_validation->set_rules('email', 'Email', 'required');
        $this->form_validation->set_rules('password', 'Password', 'required');
        if ($this->form_validation->run() == false) {
            $this->session->set_flashdata('err_alt', validation_errors());
            redirect('registration');
        }

        //$exist_email=$this->admin_model->select_with_where('*','email="'.$data['email'].'"','user');

        $exist_email = $this->admin_model->select_with_where('*', "email='{$data['email']}' and role_type = 1 ", 'user');
        if (count($exist_email) > 0) {
            $this->session->set_flashdata('err_alt', 'The email you have provided is already in use. Please enter in an alternative email');
            redirect('registration');
        } else {
            $user_id = $this->registration_model->insert_ret('user', $data);
            $result = $this->registration_model->select_with_where('*', 'user_id=' . $user_id . '', 'user');

            $this->session->set_userdata('user_id', $result[0]['user_id']);
            $this->session->set_userdata('email', $result[0]['email']);
            $this->session->set_userdata('role_type', $result[0]['role_type']);
            $this->session->set_userdata('user_fname', $result[0]['user_fname']);
            $this->session->set_userdata('user_lname', $result[0]['user_lname']);

            $this->session->set_flashdata('scc_alt', 'Registration Successfully completed');
            if (!$this->utility_model->get_software_validity($this->session->userdata('user_id'))) {
                redirect('subscribe');
            }
            //redirect('stepOne','refresh');
        }
    }

    // public function add_registration_post()
    // {
    //        $redirect_url=$this->input->post('redirect_url');
    // 	$email=$this->input->post('email');
    //        $mobile_no=$this->input->post('country_code').''.$this->input->post('mobile_no');
    //        $only_mobile_no=$this->input->post('mobile_no');
    //        $exist_email=$this->admin_model->select_with_where('*','email="'.$email.'"','verify_user');
    //        $exist_phone=$this->admin_model->select_with_where('*','mobile_no="'.$only_mobile_no.'"','verify_user');

    //        $data['name']=ucwords($this->input->post('name'));
    //        $data['password']=$this->encryptIt($this->input->post('password'));
    //        $data['email']=$email;
    //        $data['mobile_no']=$only_mobile_no;
    //        $data['user_type']=$this->input->post('user_type');//3=doctor;8=patient
    //        $data['verify_status']=1;
    //        $data['verify_code']=sprintf("%06d", mt_rand(1, 999999));
    //        $data['created_at']=date('Y-m-d H:i:s');

    //        $this->session->set_userdata('tmp_name', $data['name']);
    //        $this->session->set_userdata('tmp_mobile_no', $this->input->post('mobile_no'));
    //        $this->session->set_userdata('tmp_email', $data['email']);


    //        if(count($exist_email)>0)
    //        {
    //            $this->session->set_userdata('err_alt', $this->lang->line('email_exist_msg'));
    //            redirect($redirect_url,'refresh');
    //        }

    //        else if(count($exist_phone)>0)
    //        {
    //            $this->session->set_userdata('err_alt', $this->lang->line('mobile_exist_msg'));
    //            redirect($redirect_url,'refresh');
    //        }

    //        else
    //        {

    //        	$verf_id=$this->admin_model->insert_ret('verify_user',$data);
    //            $exist_code=$this->admin_model->select_with_where('*','verify_code="'.$data['verify_code'].'"','verify_user');

    //            $data_pro['username'] = preg_replace('/[ ,]+/', '_', trim($data['name']));
    //            $data_pro['username']=$data_pro['username'].'_'.$verf_id;
    //            $data_pro['username'] = str_replace("'", '', $data_pro['username']);
    //            $this->admin_model->update_function('id', $verf_id, 'verify_user', $data_pro);


    //            // Sending SMS portion
    //            $api_code=$this->admin_model->select_with_where('*','id=1','mobile_sms_api');

    //            if( $user_type==8)
    //            {
    //                $sms_content=$this->admin_model->select_with_where('*','id=1','sms_content');
    //                $email_content=$this->admin_model->select_with_where('*','id=1','email_content');

    //                if($sms_content[0]['is_active']==1)
    //                {
    //                    $msg_to=$mobile_no;
    //                    $head_data=$this->head_data();
    //                    $company_name=$head_data[0]['name'];
    //                    $company_website=$head_data[0]['website'];
    //                    $company_fb_link=$head_data[0]['fb_link'];
    //                    $arr = explode(' ',trim($data['name']));

    //                    $msg_des='Dear '.$arr[0].',\n'.$sms_content[0]['content'].' '.$data['verify_code'].'\n'.$company_name;


    //                    $ret_data=$this->sendsms_library->send_single_sms($company_name,$msg_to,$msg_des);


    //                    //echo $api_code[0]['api_code'];

    //                    if ($ret_data=='err')
    //                    {
    //                    	$data_sms['send_sms']=$api_code[0]['send_sms']+1;
    //                    	$data_sms['failed']=$api_code[0]['failed']+1;
    //                    	$this->admin_model->update_function('id',$api_code[0]['id'],'mobile_sms_api',$data_sms);
    //                      	//echo "cURL Error #:" . $err;
    //                    }
    //                    else
    //                    {
    //                      	$data_sms['send_sms']=$api_code[0]['send_sms']+1;
    //                    	$data_sms['delivered']=$api_code[0]['delivered']+1;
    //                    	$this->admin_model->update_function('id',$api_code[0]['id'],'mobile_sms_api',$data_sms);
    //                    }
    //                }


    //                if ($email_content[0]['is_active']==1)
    //                {
    //                    $this->send_email($email_content[0]['title'],$email,1);
    //                }

    //                $this->session->set_userdata('scc_alt', $this->lang->line('enter_verify_msg'));
    //                redirect('registration/account_activation','refresh');
    //            }
    //            else
    //            {
    //                $sms_content=$this->admin_model->select_with_where('*','id=3','sms_content');
    //                $email_content=$this->admin_model->select_with_where('*','id=3','email_content');

    //                if($sms_content[0]['is_active']==1)
    //                {
    //                    $msg_to=$mobile_no;
    //                    $head_data=$this->head_data();
    //                    $company_name=$head_data[0]['name'];
    //                    $company_website=$head_data[0]['website'];
    //                    $company_fb_link=$head_data[0]['fb_link'];
    //                    $arr = explode(' ',trim($data['name']));

    //                    $msg_des='Dear '.$arr[0].',\n'.$sms_content[0]['content'].'\n'.$company_name;


    //                    $ret_data=$this->sendsms_library->send_single_sms($company_name,$msg_to,$msg_des);


    //                    //echo $api_code[0]['api_code'];

    //                    if ($ret_data=='err')
    //                    {
    //                        $data_sms['send_sms']=$api_code[0]['send_sms']+1;
    //                        $data_sms['failed']=$api_code[0]['failed']+1;
    //                        $this->admin_model->update_function('id',$api_code[0]['id'],'mobile_sms_api',$data_sms);
    //                        //echo "cURL Error #:" . $err;
    //                    }
    //                    else
    //                    {
    //                        $data_sms['send_sms']=$api_code[0]['send_sms']+1;
    //                        $data_sms['delivered']=$api_code[0]['delivered']+1;
    //                        $this->admin_model->update_function('id',$api_code[0]['id'],'mobile_sms_api',$data_sms);
    //                    }
    //                }


    //                if ($email_content[0]['is_active']==1)
    //                {
    //                    $this->send_email($email_content[0]['title'],$email,1);
    //                }

    //                $this->session->set_userdata('scc_alt', $this->lang->line('enter_verify_msg'));
    //                redirect('registration/account_activation','refresh');
    //            }
    //        }
    // }

    public function account_activation($value = '')
    {
        if ($this->session->userdata('tmp_email') != '') {
            $data['head_data'] = $this->head_data();
            $this->load->view('account_activation', $data);
        } else {
            redirect('registration', 'refresh');
        }
    }

    public function verification_code_match()
    {
        $verify_code = $this->input->post('verify_code');
        $email = $this->session->userdata('tmp_email');
        $mobile = $this->session->userdata('tmp_mobile_no');
        $name = $this->session->userdata('tmp_name');


        $vf_code_match = $this->admin_model->select_with_where('*', 'mobile_no="' . $mobile . '" AND email="' . $email . '" AND verify_code="' . $verify_code . '"', 'verify_user');

        if (count($vf_code_match) >= 1) {
            // Sending SMS portion
            $api_code = $this->admin_model->select_with_where('*', 'id=1', 'mobile_sms_api');
            $sms_content = $this->admin_model->select_with_where('*', 'id=2', 'sms_content');
            $email_content = $this->admin_model->select_with_where('*', 'id=2', 'email_content');

            if ($sms_content[0]['is_active'] == 1) {
                $msg_to = $mobile;
                $head_data = $this->head_data();
                $company_name = $head_data[0]['name'];
                $company_website = $head_data[0]['website'];
                $company_fb_link = $head_data[0]['fb_link'];
                $arr = explode(' ', trim($name));

                $msg_des = 'Dear ' . $arr[0] . ',\n' . $sms_content[0]['content'] . '\n' . $company_name;


                $ret_data = $this->sendsms_library->send_single_sms($company_name, $msg_to, $msg_des);


                //echo $api_code[0]['api_code'];

                if ($ret_data == 'err') {
                    $data_sms['send_sms'] = $api_code[0]['send_sms'] + 1;
                    $data_sms['failed'] = $api_code[0]['failed'] + 1;
                    $this->admin_model->update_function('id', $api_code[0]['id'], 'mobile_sms_api', $data_sms);
                    //echo "cURL Error #:" . $err;
                } else {
                    $data_sms['send_sms'] = $api_code[0]['send_sms'] + 1;
                    $data_sms['delivered'] = $api_code[0]['delivered'] + 1;
                    $this->admin_model->update_function('id', $api_code[0]['id'], 'mobile_sms_api', $data_sms);
                }
            }


            if ($email_content[0]['is_active'] == 1) {
                $this->send_email($email_content[0]['title'], $email, 1);
            }

            $data_pro['verify_status'] = 1;
            $this->admin_model->update_function('mobile_no', $mobile, 'verify_user', $data_pro);

            $data_login['email'] = $email;
            $data_login['mobile_no'] = $mobile;
            $data_login['username'] = $vf_code_match[0]['username'];
            $data_login['password'] = $vf_code_match[0]['password'];
            $data_login['user_type'] = $vf_code_match[0]['user_type'];
            $data_login['verify_status'] = 1;
            $data_login['created_at'] = date('Y-m-d H:i:s');
            $login_id = $this->admin_model->insert_ret('login', $data_login);

            $data_reg['name'] = $name;
            $data_reg['login_id'] = $login_id;
            $reg_id = $this->admin_model->insert_ret('registration', $data_reg);


            $this->session->set_userdata('name', $name);
            $this->session->set_userdata('email', $email);
            $this->session->set_userdata('type', 8);
            $this->session->set_userdata('login_id', $login_id);

            $this->session->unset_userdata('tmp_name');
            $this->session->unset_userdata('tmp_mobile_no');
            $this->session->unset_userdata('tmp_email');

            redirect('home', 'refresh');
        } else {
            $this->session->set_userdata('err_alt', $this->lang->line('verify_err_msg'));
            redirect('registration/account_activation', 'refresh');
        }

    }

    public function test_mail()
    {
        $this->send_email('registration', 'aman.rabby@gmail.com', 1);
    }

    public function send_email($subject = '', $email = '', $id)
    {
        $company_info = $this->head_data();
        $email_content = $this->admin_model->select_with_where('*', 'id=' . $id, 'email_content');

        $this->load->library('email');
        $this->email->initialize(array('priority' => 1));
        $this->email->set_newline("\r\n");

        $this->email->from($company_info[0]['email'], $company_info[0]['name']);
        $this->email->to($email);
        $base = $this->config->base_url();
        // $message=heading('Your Email:');
        // $message.=$email;
        // $message.=br(2);
        // $message.=heading('Password Recovery Link:');
        // $message.="<a href='".$base."forget/getlink/".$tmp_password."'>Click Here</a>";
        // $message.=br(2);
        $message = '<img style="width:200px;height:80px" src="' . $base . '/uploads/email_logo.jpg"/>';
        $message .= "<br><br>";
        $message .= $email_content[0]['content'];
        //$message.="<a href='fsdkfjdsfjsdfjdsf'>sdfsdfsdf</a>";
        $this->email->subject($subject);
        $this->email->message($message);
        $this->email->set_mailtype("html");
        $this->email->send();
    }


    public function head_data()
    {
        $id = 1;
        $data = $this->admin_model->select_with_where('*', 'id=' . $id, 'company');
        return $data;
    }

    function encryptIt($q)
    {
        $cryptKey = 'Lf6Q5htqdgnSn0AABqlsSddj1QNu0fJs';
        $qEncoded = base64_encode(mcrypt_encrypt(MCRYPT_RIJNDAEL_256, md5($cryptKey), $q, MCRYPT_MODE_CBC, md5(md5($cryptKey))));
        return ($qEncoded);
    }

}
