<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Subscribe extends MX_Controller
{


    function __construct()
    {
        if ($this->session->userdata('language_select') == 'bangla') {
            $this->lang->load('admin', 'bangla');
        } else {
            $this->lang->load('admin', 'english');
        }

        require_once(APPPATH . 'libraries/stripe-php/init.php');
        $this->load->model('utility/utility_model');

    }

    public function index()
    {
        $user_id = $this->session->userdata('user_id');
        if ($user_id == '') {
            $this->session->set_userdata('log_err', 'Please log in first');
            redirect('login', 'refresh');
        }

        if ($this->utility_model->is_user_inactive($this->session->userdata('user_id'))) {
            $this->session->set_flashdata('deactivated', 'User is deactivated');
            redirect('login');
        }

        if ($this->input->post()) {
            $this->process_subscription_request();
            exit;
        }

        $user = $this->utility_model->getUser($user_id);

        $email = '';
        if (!empty($user)) {
            $email = $user['email'];
        }

        $sk = $this->utility_model->get_stripe_secret_key();
        $pk = $this->utility_model->get_stripe_public_key();


        //--------------------------------------------------------------------------------------------------------------
        $current_plan_id = null;
        $current_subscription_id = $this->utility_model->user_stripe_customer_subscription_id($user_id);
        $current_subscription_error = null;
        if (!empty($current_subscription_id)) {
            $current_stripe_subscription = $this->getSubscription($current_subscription_id);

            $current_subscription = $current_stripe_subscription['subscription'];
            if (empty($current_subscription) || !empty($current_stripe_subscription['errors'])) {

                $current_subscription_error = "Could not retrieve subscription";
                if (!empty($current_stripe_subscription['errors'])) {
                    $current_subscription_error = implode(" | ", $current_stripe_subscription['errors']);
                }
            } else {
                $current_plan_id = $current_subscription->items->data[0]->plan->id;
            }
        }
        //--------------------------------------------------------------------------------------------------------------

        $user_need_to_subscribe = false;
        if ($this->utility_model->does_user_need_to_subscribe($user_id)) {
            $user_need_to_subscribe = true;
        }

        $plans = $this->utility_model->getPlans();


        //-----------------------
        $current_plan_name = "Unavailable";
        if (!empty($plans) && !empty($current_plan_id)) {
            $plan_keys = array_keys($plans);
            if (in_array($current_plan_id, $plan_keys)) {
                $current_plan_name = $plans[$current_plan_id];
            }
        }

        $subscription_exists = $this->utility_model->stripe_customer_subscription_exists($user_id);


        //-----------------------

        $data = array();
        $data['email'] = $email;
        $data['plans'] = $plans;
        $data['pk'] = $pk;

        $software_validity = $this->utility_model->get_software_validity($user_id);
        $software_validity_text = $this->utility_model->get_software_validity_text($user_id);

        $data['software_validity'] = $software_validity;
        $data['software_validity_text'] = $software_validity_text;

        $data['current_subscription_error'] = $current_subscription_error;
        $data['current_plan_id'] = $current_plan_id;
        $data['user_need_to_subscribe'] = $user_need_to_subscribe;
        $data['current_plan_name'] = $current_plan_name;
        $data['subscription_exists'] = $subscription_exists;
        $data['user'] = $user;
        $data['user_card'] = $this->getUserCard($user_id);

        $this->load->view('index', $data);
    }

    public function getUserCard($user_id)
    {

        $card = array();
        $card['existence'] = false;
        $card['message'] = "";
        $card['default_source'] = "";
        $card['last_4'] = "";

        $user_stripe_customer = $this->utility_model->user_stripe_customer($user_id);

        if (empty($user_stripe_customer)) {
            $card['message'] = "Customer is not stored";
            return $card;
        }


        $customer_id = $user_stripe_customer['stripe_customer_id'];

        /*---------------------------------------------------------------------*/

        $customer = null;

        $stripe_customer = $this->getCustomer($customer_id);

        $customer = $stripe_customer['customer'];
        if (!$customer|| !empty($stripe_customer['errors'])) {

            $message = "Could not fetch customer";
            if (!empty($stripe_customer['errors'])) {
                $message = implode(" | ", $stripe_customer['errors']);
            }

            $card['message'] = $message;
            return $card;

        }

        $card['existence'] = true;
        $card['message'] = "Card found";
        $card['default_source'] = $customer->default_source->brand;
        $card['last_4'] = $customer->default_source->last4;

        return $card;
    }

    public function unsubscribe()
    {
        $user_id = $this->session->userdata('user_id');

        $sub = $this->utility_model->user_stripe_customer_subscription($user_id);

        $sub_err_1 = false;
        if (empty($sub)) {
            $sub_err_1 = true;
        }
        if (!empty($sub)) {
            if (empty($sub['stripe_subscription_id'])) {
                $sub_err_1 = true;
            }
        }

        if ($sub_err_1) {
            $this->session->set_flashdata('subscription_cancel_error', 'Could not find subscription in database');
            redirect('subscribe', 'refresh');
        }

        //---------------------------- hopefully everything was fine----------------------------------------------------

        $subscription = null;
        $stripe_subscription = $this->cancel_stripe_subscription($sub['stripe_subscription_id']);
        $subscription = $stripe_subscription['subscription'];

        if (empty($subscription) || !empty($stripe_subscription['errors'])) {

            $message = "Could not cancel subscription";
            if (!empty($stripe_subscription['errors'])) {
                $message = implode(" | ", $stripe_subscription['errors']);
            }

            $this->session->set_flashdata('subscription_cancel_error', $message);
            redirect('subscribe', 'refresh');
        }

        if ($subscription->status != "canceled") {
            $message = "Subscription was not cancelled";
            $this->session->set_flashdata('subscription_cancel_error', $message);
            redirect('subscribe', 'refresh');
        }

        $set_subscription_canceled = $this->set_stripe_customer_subscription_canceled($subscription->id, $user_id);

        if (!$set_subscription_canceled) {
            $this->session->set_flashdata('subscription_cancel_incomplete', "Could not set subscription cancelled");
            redirect('subscribe', 'refresh');
        }

        $usr_upd_data = array();
        $usr_upd_data['software_is_valid'] = 2;
        $usr_upd_data['software_is_valid_till_date'] = date("Y-m-d", $subscription->current_period_end);
        $this->db->update('user', $usr_upd_data, array('user_id' => $user_id));

        $this->session->set_flashdata('subscription_cancel_complete', "Successfully unsubscribed");
        redirect('subscribe', 'refresh');
    }

    public function change_plan()
    {
        $user_id = $this->session->userdata('user_id');

        $user = $this->utility_model->getUser($user_id);

        if (empty($user)) {
            $this->session->set_flashdata('subscription_incomplete', "User does not exist");
            redirect('subscribe', 'refresh');
        }
        //--------------------------------------------------------------------------------------------------------------
        $repeat_same_plan = false;
        $new_plan_id = $this->input->post('new_plan_id');
        $existing_plan_id = $this->input->post('existing_plan_id');

        $email = $this->input->post('email');
        $token = $this->input->post('token');

        if ($token == "") {
            $this->session->set_flashdata('subscription_change_error', "token is not given");
            redirect('subscribe', 'refresh');
        }

        if ($email == "") {
            $this->session->set_flashdata('subscription_change_error', "email is not given");
            redirect('subscribe', 'refresh');
        }

        if ($new_plan_id == $existing_plan_id) {
            $repeat_same_plan = true;
        }

        if ($new_plan_id == "") {
            $this->session->set_flashdata('subscription_change_error', 'New plan need to be selected');
            redirect('subscribe', 'refresh');
        }
        //--------------------------------------------------------------------------------------------------------------

        $sub = $this->utility_model->user_stripe_customer_subscription($user_id);
        $cus = $this->utility_model->user_stripe_customer($user_id);

        $sub_err_1 = false;
        $cus_err_1 = false;
        //--------------------------------------------------------------------------------------------------------------
        if (empty($sub)) {
            $sub_err_1 = true;
        }
        if (!empty($sub)) {
            if (empty($sub['stripe_subscription_id'])) {
                $sub_err_1 = true;
            }
        }
        if ($sub_err_1) {
            $this->session->set_flashdata('subscription_change_error', 'Could not find subscription in database');
            redirect('subscribe', 'refresh');
        }
        //--------------------------------------------------------------------------------------------------------------
        if (empty($cus)) {
            $cus_err_1 = true;
        }

        if (!empty($cus)) {
            if (empty($cus['stripe_customer_id'])) {
                $cus_err_1 = true;
            }
        }

        if ($cus_err_1) {
            $this->session->set_flashdata('subscription_change_error', 'Could not find a customer in database');
            redirect('subscribe', 'refresh');
        }

        //---------------------------- hopefully everything was fine----------------------------------------------------
        $subscription = null;
        $stripe_subscription = $this->cancel_stripe_subscription($sub['stripe_subscription_id']);
        $subscription = $stripe_subscription['subscription'];

        if (empty($subscription) || !empty($stripe_subscription['errors'])) {

            $message = "Could not cancel subscription";
            if (!empty($stripe_subscription['errors'])) {
                $message = implode(" | ", $stripe_subscription['errors']);
            }

            $this->session->set_flashdata('subscription_change_error', $message);
            redirect('subscribe', 'refresh');
        }

        if ($subscription->status != "canceled") {
            $message = "Subscription was not cancelled";
            $this->session->set_flashdata('subscription_change_error', $message);
            redirect('subscribe', 'refresh');
        }

        $set_subscription_canceled = $this->set_stripe_customer_subscription_canceled($subscription->id, $user_id);

        if (!$set_subscription_canceled) {
            $this->session->set_flashdata('subscription_change_error', "Could not set subscription cancelled");
            redirect('subscribe', 'refresh');
        }


        $usr_upd_data = array();
        $usr_upd_data['software_is_valid'] = 2;
        $usr_upd_data['software_is_valid_till_date'] = date("Y-m-d", $subscription->current_period_end);
        $this->db->update('user', $usr_upd_data, array('user_id' => $user_id));

        //-------------------------------------Hopefully cancellation is completed---------------------------------------


        $billing_starts = false;

        if ($repeat_same_plan) {
            $billing_starts = strtotime($subscription->current_period_end);//this is from the subscription that is just got cancelled
        }

        $subscription = null;
        $stripe_subscription = $this->create_stripe_subscription($cus['stripe_customer_id'], $new_plan_id, $billing_starts);
        $subscription = $stripe_subscription['subscription'];

        if (empty($subscription) || !empty($stripe_subscription['errors'])) {

            $message = "Could not create subscription";
            if (!empty($stripe_subscription['errors'])) {
                $message = implode(" | ", $stripe_subscription['errors']);
            }

            $this->session->set_flashdata('subscription_incomplete', $message);
            redirect('subscribe', 'refresh');
        }

        $set_subscription = $this->set_stripe_customer_subscription($cus['stripe_customer_id'], $subscription->id, $user_id);

        if (!$set_subscription) {
            $this->session->set_flashdata('subscription_incomplete', "Could not set subscription");
            redirect('subscribe', 'refresh');
        }

        $usr_upd_data = array();
        $usr_upd_data['software_is_valid'] = 1;
        $usr_upd_data['software_is_valid_till_date'] = null;
        $this->db->update('user', $usr_upd_data, array('user_id' => $user_id));


        $this->session->set_flashdata('subscription_complete', "User is successfully subscribed");

        $property_ids = $this->utility_model->propertyIdsOfAnUser($user_id);
        $redirect = empty($property_ids) ? "stepOne" : "property";

        redirect($redirect, 'refresh');
    }

    public function getCustomer($customer_id)
    {
        $sk = $this->utility_model->get_stripe_secret_key();
        $errors = array();
        $stripe_customer = array();
        $customer = null;

        try {
            \Stripe\Stripe::setApiKey($sk);
            $customer = \Stripe\Customer::Retrieve(
            ["id" => $customer_id, "expand" => ["default_source"]]
            );
        } catch (\Stripe\Error\RateLimit $e) {
            // Too many requests made to the API too quickly
            $errors[] = $e->getMessage();
        } catch (\Stripe\Error\InvalidRequest $e) {
            // Invalid parameters were supplied to Stripe's API
            $errors[] = $e->getMessage();
        } catch (\Stripe\Error\Authentication $e) {
            // Authentication with Stripe's API failed
            // (maybe you changed API keys recently)
            $errors[] = $e->getMessage();
        } catch (\Stripe\Error\ApiConnection $e) {
            // Network communication with Stripe failed
            $errors[] = $e->getMessage();
        } catch (\Stripe\Error\Base $e) {
            // Display a very generic error to the user, and maybe send
            // yourself an email
            $errors[] = $e->getMessage();
        } catch (Exception $e) {
            // Something else happened, completely unrelated to Stripe
            $errors[] = $e->getMessage();
        }

        $stripe_customer['customer'] = $customer;
        $stripe_customer['errors'] = $errors;
        return $stripe_customer;
    }

    public function getSubscription($subscription_id)
    {
        $sk = $this->utility_model->get_stripe_secret_key();
        $errors = array();
        $stripe_subscription = array();
        $subscription = null;

        try {
            \Stripe\Stripe::setApiKey($sk);
            $subscription = \Stripe\Subscription::retrieve($subscription_id);
        } catch (\Stripe\Error\Card $e) {
            // Since it's a decline, \Stripe\Error\Card will be caught
            $errors[] = $e->getMessage();
        } catch (\Stripe\Error\RateLimit $e) {
            // Too many requests made to the API too quickly
            $errors[] = $e->getMessage();
        } catch (\Stripe\Error\InvalidRequest $e) {
            // Invalid parameters were supplied to Stripe's API
            $errors[] = $e->getMessage();
        } catch (\Stripe\Error\Authentication $e) {
            // Authentication with Stripe's API failed
            // (maybe you changed API keys recently)
            $errors[] = $e->getMessage();
        } catch (\Stripe\Error\ApiConnection $e) {
            // Network communication with Stripe failed
            $errors[] = $e->getMessage();
        } catch (\Stripe\Error\Base $e) {
            // Display a very generic error to the user, and maybe send
            // yourself an email
            $errors[] = $e->getMessage();
        } catch (Exception $e) {
            // Something else happened, completely unrelated to Stripe
            $errors[] = $e->getMessage();
        }

        $stripe_subscription['subscription'] = $subscription;
        $stripe_subscription['errors'] = $errors;
        return $stripe_subscription;
    }

    public function process_subscription_request()
    {
        $user_id = $this->session->userdata('user_id');
        $user = $this->utility_model->getUser($user_id);

        if (empty($user)) {
            $this->session->set_flashdata('subscription_incomplete', "User does not exist");
            redirect('subscribe', 'refresh');
        }

        $plan_id = $this->input->post('plan_id');
        $email = $this->input->post('email');
        $token = $this->input->post('token');

        if ($plan_id == "") {
            $this->session->set_flashdata('subscription_incomplete', "A plan must me selected");
            redirect('subscribe', 'refresh');
        }

        if ($token == "") {
            $this->session->set_flashdata('subscription_incomplete', "token is not given");
            redirect('subscribe', 'refresh');
        }

        $customer = null;
        $stripe_customer = $this->create_stripe_customer($email, $token);

        $customer = $stripe_customer['customer'];
        if (empty($customer) || !empty($stripe_customer['errors'])) {

            $message = "Could not create customer";
            if (!empty($stripe_customer['errors'])) {
                $message = implode(" | ", $stripe_customer['errors']);
            }

            $this->session->set_flashdata('subscription_incomplete', $message);
            redirect('subscribe', 'refresh');
        }

        //----------------------------------------------------------------------
        $customer_exist = $this->utility_model->stripe_customer_exists($user_id);

        if (!$customer_exist) {
            $set_customer = $this->set_stripe_customer($customer->id, $user_id);

            if (!$set_customer) {
                $this->session->set_flashdata('subscription_incomplete', "Could not set customer");
                redirect('subscribe', 'refresh');
            }
        }
        //----------------------------------------------------------------------


        //--------------------
        $billing_starts = false;

        if (($user['software_is_valid'] == 2) && ($user['software_is_valid_till_date'] != null)) {
            $billing_starts = strtotime($user['software_is_valid_till_date']);
        }

        $subscription = null;
        $stripe_subscription = $this->create_stripe_subscription($customer->id, $plan_id, $billing_starts);
        $subscription = $stripe_subscription['subscription'];

        if (empty($subscription) || !empty($stripe_subscription['errors'])) {

            $message = "Could not create subscription";
            if (!empty($stripe_subscription['errors'])) {
                $message = implode(" | ", $stripe_subscription['errors']);
            }

            $this->session->set_flashdata('subscription_incomplete', $message);
            redirect('subscribe', 'refresh');
        }

        $set_subscription = $this->set_stripe_customer_subscription($customer->id, $subscription->id, $user_id);

        if (!$set_subscription) {
            $this->session->set_flashdata('subscription_incomplete', "Could not set subscription");
            redirect('subscribe', 'refresh');
        }

        $usr_upd_data = array();
        $usr_upd_data['software_is_valid'] = 1;
        $usr_upd_data['software_is_valid_till_date'] = null;
        $this->db->update('user', $usr_upd_data, array('user_id' => $user_id));


        $this->session->set_flashdata('subscription_complete', "User is successfully subscribed");

        $property_ids = $this->utility_model->propertyIdsOfAnUser($user_id);
        $redirect = empty($property_ids) ? "stepOne" : "property";

        redirect($redirect, 'refresh');
    }

    public function change_card()
    {
        $token = $this->input->post('token');

        if ($token == "") {
            $this->session->set_flashdata('card_change_error', "token is not given");
            redirect('subscribe', 'refresh');
        }

        $user_id = $this->session->userdata('user_id');

        $user_stripe_customer = $this->utility_model->user_stripe_customer($user_id);

        if (empty($user_stripe_customer)) {
            $this->session->set_flashdata('customer_error',"Customer is not stored") ;
            redirect('subscribe');
        }

        $customer_id = $user_stripe_customer['stripe_customer_id'];

        /*---------------------------------------------------------------------*/

        $customer = null;

        $stripe_customer = $this->getCustomer($customer_id);

        $customer = $stripe_customer['customer'];
        if (!$customer|| !empty($stripe_customer['errors'])) {

            $message = "Could not fetch customer";
            if (!empty($stripe_customer['errors'])) {
                $message = implode(" | ", $stripe_customer['errors']);
            }

            $this->session->set_flashdata('customer_error',$message) ;
            redirect('subscribe');
        }

        $errors = array();

        try {

            $customer->source = $token; // obtained with Checkout
            $customer->save();
        }
        catch (\Stripe\Error\Card $e) {
            // Since it's a decline, \Stripe\Error\Card will be caught
            $errors[] = $e->getMessage();
        } catch (\Stripe\Error\RateLimit $e) {
            // Too many requests made to the API too quickly
            $errors[] = $e->getMessage();
        } catch (\Stripe\Error\InvalidRequest $e) {
            // Invalid parameters were supplied to Stripe's API
            $errors[] = $e->getMessage();
        } catch (\Stripe\Error\Authentication $e) {
            // Authentication with Stripe's API failed
            // (maybe you changed API keys recently)
            $errors[] = $e->getMessage();
        } catch (\Stripe\Error\ApiConnection $e) {
            // Network communication with Stripe failed
            $errors[] = $e->getMessage();
        } catch (\Stripe\Error\Base $e) {
            // Display a very generic error to the user, and maybe send
            // yourself an email
            $errors[] = $e->getMessage();
        } catch (Exception $e) {
            // Something else happened, completely unrelated to Stripe
            $errors[] = $e->getMessage();
        }

        if(!empty($errors)){
            $this->session->set_flashdata('card_change_error',implode(" | ", $errors)) ;
            redirect('subscribe');
        }

        $this->session->set_flashdata('card_change_success',"Card changed successfully") ;
        redirect('subscribe');

    }

    public function set_stripe_customer($stripe_customer_id, $user_id)
    {
        $user_stripe_customer = $this->utility_model->user_stripe_customer($user_id);
        $set_customer = false;
        if (empty($user_stripe_customer)) {
            $ins_data = array();
            $ins_data['user_id'] = $user_id;
            $ins_data['stripe_customer_id'] = $stripe_customer_id;

            $this->db->insert('stripe_customer', $ins_data);
            $set_customer = true;
        }
        if (!empty($user_stripe_customer)) {
            if (empty($user_stripe_customer['stripe_customer_id'])) {
                $upd_data = array();
                $upd_data['stripe_customer_id'] = $stripe_customer_id;

                $this->db->update('stripe_customer', $upd_data, array('user_id' => $user_id));
                $set_customer = true;
            }
        }
        return $set_customer;
    }

    public function set_stripe_customer_subscription($stripe_customer_id, $stripe_subscription_id, $user_id)
    {
        $user_stripe_customer_subscription = $this->utility_model->user_stripe_customer_subscription($user_id);
        $set_subscription = false;
        if (empty($user_stripe_customer_subscription)) {
            $ins_data = array();
            $ins_data['user_id'] = $user_id;
            $ins_data['stripe_subscription_id'] = $stripe_subscription_id;

            $this->db->insert('stripe_customer_subscription', $ins_data);
            $set_subscription = true;
        }
        if (!empty($user_stripe_customer_subscription)) {
            if (empty($user_stripe_customer_subscription['stripe_customer_id'])) {
                $upd_data = array();
                $upd_data['stripe_subscription_id'] = $stripe_subscription_id;

                $this->db->update('stripe_customer_subscription', $upd_data, array('user_id' => $user_id));
                $set_subscription = true;
            }
        }

        return $set_subscription;

    }

    public function create_stripe_customer($email, $token)
    {
        $sk = $this->utility_model->get_stripe_secret_key();
        $errors = array();
        $stripe_customer = array();
        $customer = null;

        try {
            \Stripe\Stripe::setApiKey($sk);
            $customer = \Stripe\Customer::create([
                "email" => $email,
                "source" => $token
            ]);
        } catch (\Stripe\Error\Card $e) {
            // Since it's a decline, \Stripe\Error\Card will be caught
            $errors[] = $e->getMessage();
        } catch (\Stripe\Error\RateLimit $e) {
            // Too many requests made to the API too quickly
            $errors[] = $e->getMessage();
        } catch (\Stripe\Error\InvalidRequest $e) {
            // Invalid parameters were supplied to Stripe's API
            $errors[] = $e->getMessage();
        } catch (\Stripe\Error\Authentication $e) {
            // Authentication with Stripe's API failed
            // (maybe you changed API keys recently)
            $errors[] = $e->getMessage();
        } catch (\Stripe\Error\ApiConnection $e) {
            // Network communication with Stripe failed
            $errors[] = $e->getMessage();
        } catch (\Stripe\Error\Base $e) {
            // Display a very generic error to the user, and maybe send
            // yourself an email
            $errors[] = $e->getMessage();
        } catch (Exception $e) {
            // Something else happened, completely unrelated to Stripe
            $errors[] = $e->getMessage();
        }

        $stripe_customer['customer'] = $customer;
        $stripe_customer['errors'] = $errors;
        return $stripe_customer;
    }

    public function create_stripe_subscription($stripe_customer_id, $stripe_plan_id, $billing_starts)
    {
        $sk = $this->utility_model->get_stripe_secret_key();
        $errors = array();
        $stripe_subscription = array();
        $subscription = null;


        try {
            \Stripe\Stripe::setApiKey($sk);

            $s_c = [
                "customer" => $stripe_customer_id,
                "items" => [
                    [
                        "plan" => $stripe_plan_id,
                    ],
                ]
            ];

            if ($billing_starts) {
                $s_c['billing_cycle_anchor'] = $billing_starts;
                $s_c['trial_end'] = $billing_starts - 3600 * 2; //two hours before trial end
            }

            $subscription = \Stripe\Subscription::create($s_c);
        } catch (\Stripe\Error\Card $e) {
            // Since it's a decline, \Stripe\Error\Card will be caught
            $errors[] = $e->getMessage();
        } catch (\Stripe\Error\RateLimit $e) {
            // Too many requests made to the API too quickly
            $errors[] = $e->getMessage();
        } catch (\Stripe\Error\InvalidRequest $e) {
            // Invalid parameters were supplied to Stripe's API
            $errors[] = $e->getMessage();
        } catch (\Stripe\Error\Authentication $e) {
            // Authentication with Stripe's API failed
            // (maybe you changed API keys recently)
            $errors[] = $e->getMessage();
        } catch (\Stripe\Error\ApiConnection $e) {
            // Network communication with Stripe failed
            $errors[] = $e->getMessage();
        } catch (\Stripe\Error\Base $e) {
            // Display a very generic error to the user, and maybe send
            // yourself an email
            $errors[] = $e->getMessage();
        } catch (Exception $e) {
            // Something else happened, completely unrelated to Stripe
            $errors[] = $e->getMessage();
        }

        $stripe_subscription['subscription'] = $subscription;
        $stripe_subscription['errors'] = $errors;
        return $stripe_subscription;
    }

    public function cancel_stripe_subscription($subscription_id)
    {
        $sk = $this->utility_model->get_stripe_secret_key();
        $errors = array();
        $stripe_subscription = array();
        $subscription = null;

        try {
            \Stripe\Stripe::setApiKey($sk);
            $subscription = \Stripe\Subscription::retrieve($subscription_id);
            $subscription->cancel();
        } catch (\Stripe\Error\Card $e) {
            // Since it's a decline, \Stripe\Error\Card will be caught
            $errors[] = $e->getMessage();
        } catch (\Stripe\Error\RateLimit $e) {
            // Too many requests made to the API too quickly
            $errors[] = $e->getMessage();
        } catch (\Stripe\Error\InvalidRequest $e) {
            // Invalid parameters were supplied to Stripe's API
            $errors[] = $e->getMessage();
        } catch (\Stripe\Error\Authentication $e) {
            // Authentication with Stripe's API failed
            // (maybe you changed API keys recently)
            $errors[] = $e->getMessage();
        } catch (\Stripe\Error\ApiConnection $e) {
            // Network communication with Stripe failed
            $errors[] = $e->getMessage();
        } catch (\Stripe\Error\Base $e) {
            // Display a very generic error to the user, and maybe send
            // yourself an email
            $errors[] = $e->getMessage();
        } catch (Exception $e) {
            // Something else happened, completely unrelated to Stripe
            $errors[] = $e->getMessage();
        }

        $stripe_subscription['subscription'] = $subscription;
        $stripe_subscription['errors'] = $errors;
        return $stripe_subscription;
    }

    public function set_stripe_customer_subscription_canceled($stripe_subscription_id, $user_id)
    {
        $user_stripe_customer_subscription = $this->utility_model->user_stripe_customer_subscription($user_id);
        $set_subscription_cancelled = false;

        if (!empty($user_stripe_customer_subscription)) {
            if (empty($user_stripe_customer_subscription['stripe_customer_id'])) {
                $ins_data['user_id'] = $user_id;
                $ins_data['stripe_subscription_id'] = $stripe_subscription_id;
                $ins_data['stripe_customer_cancelled_subscription_at'] = date('Y-m-d H:i:s');

                $this->db->insert('stripe_customer_cancelled_subscription', $ins_data);

                $upd_data = array();
                $upd_data['stripe_subscription_id'] = null;

                $this->db->update('stripe_customer_subscription', $upd_data, array('user_id' => $user_id));
                $set_subscription_cancelled = true;
            }
        }

        return $set_subscription_cancelled;

    }


    public function webhook()
    {
        $subscription_canceled = false;

        $endpoint_secret = $this->utility_model->getSettings("stripe", "signing_secret", true);//only value
        $sig_header = $_SERVER["HTTP_STRIPE_SIGNATURE"];
        $event = null;
        $sk = $this->utility_model->get_stripe_secret_key();
        $payload = @file_get_contents("php://input");

        $errors = array();
        $error_message = "";
        try {
            \Stripe\Stripe::setApiKey($sk);

            $event = \Stripe\Webhook::constructEvent(
                $payload, $sig_header, $endpoint_secret
            );

        } catch (\Stripe\Error\RateLimit $e) {
            // Too many requests made to the API too quickly
            $errors[] = $e->getMessage();
        } catch (\Stripe\Error\InvalidRequest $e) {
            // Invalid parameters were supplied to Stripe's API
            $errors[] = $e->getMessage();
        } catch (\Stripe\Error\Authentication $e) {
            // Authentication with Stripe's API failed
            // (maybe you changed API keys recently)
            $errors[] = $e->getMessage();
        } catch (\Stripe\Error\ApiConnection $e) {
            // Network communication with Stripe failed
            $errors[] = $e->getMessage();
        } catch (\Stripe\Error\Base $e) {
            // Display a very generic error to the user, and maybe send
            // yourself an email
            $errors[] = $e->getMessage();
        } catch (Exception $e) {
            // Something else happened, completely unrelated to Stripe
            $errors[] = $e->getMessage();
        } catch (\UnexpectedValueException $e) {
            // Invalid payload
            $errors[] = $e->getMessage();
        } catch (\Stripe\Error\SignatureVerification $e) {
            // Invalid signature
            $errors[] = $e->getMessage();
        }

        if (!empty($errors)) {
            $error_message = implode(" | ", $errors);
        }

        //show error
        if ($error_message != "") {
            echo "Hook error: $error_message";
            exit;
        }

        if (isset($event) && $event->type == "customer.subscription.deleted") {

            //got canceled automatically after some fails
            //remove || 1, only for test purpose
            if ($event->request == null || 1) {
                $subscription_canceled = $this->process_subscription_deleted_by_webhook($event);
            }

        }

        echo $subscription_canceled ? "Subscription cancellation process is complete." : "Subscription cancellation process is in complete.";
        exit;

    }

    public function process_subscription_deleted_by_webhook($event)
    {
        $process_completed = false;

        $stripe_customer_id = $event->data->object->customer;
        $stripe_subscription_id = $event->data->object->id;
        $current_period_end = $event->data->object->current_period_end;

        $user_id = false;
        $user_id = $this->utility_model->user_id_by_stripe_subscription($stripe_subscription_id);

        if (!$user_id) {
            echo "User Id is missing";
            exit;
        }

        $set_subscription_cancelled = $this->set_stripe_customer_subscription_canceled($stripe_subscription_id, $user_id);

        if ($set_subscription_cancelled) {
            $usr_upd_data = array();
            $usr_upd_data['software_is_valid'] = 2;
            $usr_upd_data['software_is_valid_till_date'] = date("Y-m-d", $current_period_end);
            $this->db->update('user', $usr_upd_data, array('user_id' => $user_id));

            $process_completed = true;

        }


        return $process_completed;


    }

    public function mock()
    {
        $data['billing_cycle_anchor'] = 1545831000;
        $data['created'] = 1543241587;

        $data['current_period_end'] = 1545823800;
        $data['current_period_start'] = 1543241587;


        $data['start'] = 1543241587;
        $data['trial_end'] = 1545823800;
        $data['trial_start'] = 1543241587;

        function cdate($v)
        {
            return date("Y-m-d", $v);
        }

        echo "<pre>";
        print_r(array_map("cdate", $data));

        /*
         Array
            (
                [billing_cycle_anchor] => 2018-12-26
                [created] => 2018-11-26
                [current_period_end] => 2018-12-26
                [current_period_start] => 2018-11-26
                [start] => 2018-11-26
                [trial_end] => 2018-12-26
                [trial_start] => 2018-11-26
            )

        */
    }


    /*

    before trial end
    Array
        (
            [billing_cycle_anchor] => 2018-11-27
            [created] => 2018-11-27
            [current_period_end] => 2018-11-27
            [current_period_start] => 2018-11-27
            [start] => 2018-11-27
            [trial_end] => 2018-11-27
            [trial_start] => 2018-11-27
        )

    after trial end
    Array
        (
            [billing_cycle_anchor] => 2018-11-27
            [created] => 2018-11-27
            [current_period_end] => 2018-11-27
            [current_period_start] => 2018-11-27
            [start] => 2018-11-27
            [trial_end] => 2018-11-27
            [trial_start] => 2018-11-27
        )

    */
}
