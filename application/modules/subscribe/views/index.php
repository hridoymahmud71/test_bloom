<!DOCTYPE html>
<base href="<?php echo base_url(); ?>">
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Rent Simple</title>
    <link href="assets/css/bootstrap.min.css" rel="stylesheet">
    <link href="assets/css/style.css" rel="stylesheet">
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body>
<style>
    .dn {
        display: none;
    }

    .logout-mr {

    }

    @media (max-width: 480px) {
        .btn-group {
            display: flex;
            flex-direction: column;
        }
    }

    #subscription_cancel_alert_modal{
        z-index: 2222;
    }
    
    .mr-alert-top {
        position: sticky;
        z-index: 1111;
        width: 100%;
        left: 0;
        top: 0;
    }

    .db-mr {
        background-color: darkblue;
        border: none;
        font-weight: bolder;
    }

    .db-mr:hover {
        border: 1px solid darkblue;
    }
</style>
<?php $user_id = $this->session->userdata('user_id'); ?>

<div class="login">
    <div class="container">
        <div class="row">
            <div class="mr-alert-top">
                <div class="alert alert-info text-center " style="color: black">
                    <?php if ($user_id) { ?>
                        <a title="Log Out" class="logout-mr pull-left btn btn-sm btn-danger" href="logout"><span
                                    class="glyphicon">&#xe163;</span></a>
                    <?php } ?>
                    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                    <strong class="text-center">
                        Current Plan: <?= $current_plan_name ?>
                    </strong>

                    <span class="text-center">
                        (<?= $software_validity_text ?>)
                    </span>
                    <div class="btn-group">
                        <?php if ($software_validity && $subscription_exists) { ?>
                            <button id="sc_btn" class="btn btn-info btn-sm db-mr">
                                Cancel Subscription
                            </button>
                            <button id="cp_btn" class="btn btn-info btn-sm db-mr">Change Plan</button>
                        <?php } ?>

                        <button id="cc_btn" <?= !$user_card['existence'] ? " style='display:none;' " : "" ?>
                                class="btn btn-info btn-sm db-mr">Change Card
                        </button>
                    </div>


                </div>
            </div>


            <div class="main">
                <div class="logo"><img src="assets/img/logo.png"/></div>
                <h3 style="margin-bottom: 5px">Subscription</h3>


                <?php if ($user_id) { ?>

                    <p style="margin-bottom: 20px" class="text-center">
                        <?php if ($this->utility_model->if_user_has_properties($user_id)) { ?>
                            <a href="property" style="color: #45abc1;font-size: 16px;text-decoration: underline">
                                See your properties.
                            </a>
                        <?php } else { ?>
                            <a href="stepTwo" style="color: #45abc1;font-size: 16px;text-decoration: underline">
                                Add a property.
                            </a>
                        <?php } ?>
                    </p>
                <?php } ?>
                <div class="row">

                    <div class="panel" style="background-color: beige">

                        <a data-toggle="collapse" href="#payment-form"
                           style="border-bottom:1px solid #dddddd; font-size: 24px; color:darkgrey ;text-align: center;display: block;padding:5px 0">
                            Enter card details
                            <span class="card-collapse-span pull-right"></span>
                        </a>
                        <form action="" method="post" id="payment-form" class="panel-body">

                            <div class="form-group">

                                <label for="card-element">
                                    Credit or debit card
                                </label>

                                <div id="card-element">
                                    <!-- A Stripe Element will be inserted here. -->
                                </div>

                                <div class="form-group">
                                    <div id="card-number-element" class=" form-control">
                                        <!-- A Stripe Element will be inserted here. -->
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div id="card-expiry-element" class=" form-control">
                                        <!-- A Stripe Element will be inserted here. -->
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div id="card-cvc-element" class=" form-control">
                                        <!-- A Stripe Element will be inserted here. -->
                                    </div>
                                </div>

                                <!-- Used to display Element errors. -->
                                <div style="font-size:larger;font-weight: bold;color: firebrick" id="card-errors"
                                     role="alert"></div>
                                <button class="btn btn btn-primary">Submit Card</button>
                            </div>


                        </form>
                    </div>

                    <?php if (!empty($current_subscription_error)) { ?>
                        <div class="alert alert-danger alert-dismiss">
                            <?= $current_subscription_error ?>
                        </div>
                    <?php } ?>

                    <?php if ($this->session->flashdata('subscription_incomplete')) { ?>
                        <div class="alert alert-danger alert-dismiss">
                            <?= $this->session->flashdata('subscription_incomplete') ?>
                        </div>
                    <?php } ?>

                    <?php if ($this->session->flashdata('subscription_change_error')) { ?>
                        <div class="alert alert-danger alert-dismiss">
                            <?= $this->session->flashdata('subscription_change_error') ?>
                        </div>
                    <?php } ?>

                    <?php if ($this->session->flashdata('subscription_cancel_error')) { ?>
                        <div class="alert alert-danger alert-dismiss">
                            <?= $this->session->flashdata('subscription_cancel_error') ?>
                        </div>
                    <?php } ?>
                    <?php if ($this->session->flashdata('card_change_error')) { ?>
                        <div class="alert alert-danger alert-dismiss">
                            <?= $this->session->flashdata('card_change_error') ?>
                        </div>
                    <?php } ?>
                    <?php if ($this->session->flashdata('customer_error')) { ?>
                        <div class="alert alert-danger alert-dismiss">
                            <?= $this->session->flashdata('customer_error') ?>
                        </div>
                    <?php } ?>
                    <?php if ($this->session->flashdata('card_change_success')) { ?>
                        <div class="alert alert-success alert-dismiss">
                            <?= $this->session->flashdata('card_change_success') ?>
                        </div>
                    <?php } ?>


                    <!-- ------------------------------------------------------------------------------------------ -->
                    <div id="s_panel" class="panel"
                         style="background-color: aliceblue;<?= !$user_need_to_subscribe ? 'display:none;' : '' ?>">
                        <h4 class="text-center text-danger">
                            *Please choose a subscription plan before proceeding</h4>
                        <form id="subscription-form" role="form" onsubmit="return check_validation()" action="subscribe"
                              method="post" class="panel-body">
                            <div class="alert alert-danger" id="err_div" style="display: none">
                                <strong id="err_msg"></strong>
                            </div>
                            <div class="form-group">
                                <label for="">Available Plans</label>
                                <select name="plan_id" id="plan" class="form-control">
                                    <option value="">Choose a plan</option>
                                    <?php if (!empty($plans)) { ?>
                                        <?php foreach ($plans as $plan_k => $plan_v) { ?>
                                            <option value="<?= $plan_k ?>"><?= $plan_v ?></option>
                                        <?php } ?>
                                    <?php } ?>
                                </select>
                            </div>
                            <div class="form-group">
                                <span style="color:crimson ;text-align: center;display: block;padding:5px 0">*Enter card information to generate token</span>
                                <label for="">Token</label>
                                <input type="text" name="token" value="" id="token" class="form-control" readonly
                                       placeholder="Enter card information first to generate token">
                            </div>
                            <div class="form-group">
                                <label for="">Email</label>
                                <input type="text" name="email" value="<?= $email ?>" id="email" class="form-control"
                                       readonly>
                            </div>
                            <button type="submit" class="btn btn btn-primary">
                                Subscribe
                            </button>
                        </form>
                    </div>

                    <!-- ------------------------------------------------------------------------------------------- -->

                    <div id="sc_panel" class="panel" style="background-color: aliceblue;display:none">
                        <h4 class="text-center">You are currently subscribed to the plan below</h4>
                        <form id="sc-form" role="form" onsubmit="return check_unsubscribe_validation()"
                              action="unsubscribe"
                              method="post" class="panel-body">
                            <div class="alert alert-danger" id="sc_err_div" style="display: none">
                                <strong id="sc_err_msg"></strong>
                            </div>
                            <div class="form-group">
                                <label for="">Selected Plan</label>
                                <select name="plan_id" id="sc_plan" class="form-control" disabled>
                                    <?php if (!empty($plans)) { ?>
                                        <?php foreach ($plans as $plan_k => $plan_v) { ?>
                                            <option value="<?= $plan_k ?>" <?= $current_plan_id == $plan_k ? " selected " : "" ?>>
                                                <?= $plan_v ?>
                                            </option>
                                        <?php } ?>
                                    <?php } ?>
                                </select>
                            </div>

                            <button type="submit"
                                    onclick="return confirm('Are you sure to cancel the subscription?\n\nYou will only be able to use this software till the end of this billing cycle')"
                                    class="btn btn btn-primary">
                                Confirm subscription cancel
                            </button>
                        </form>
                    </div>

                    <!-- ------------------------------------------------------------------------------------------- -->

                    <div id="cp_panel" class="panel" style="background-color: aliceblue;display:none">
                        <h4 class="text-center">Choose a new/different plan</h4>
                        <form id="change-subscription-form" role="form" onsubmit="return check_change_plan_validation()"
                              action="change_plan"
                              method="post" class="panel-body">
                            <div class="alert alert-danger" id="cp_err_div" style="display: none">
                                <strong id="cp_err_msg"></strong>
                            </div>
                            <div class="form-group">
                                <label for="">Existing Plan</label>
                                <select name="existing_plan_id" id="cp_existing_plan" disabled class="form-control">
                                    <option value="">No plan</option>
                                    <?php if (!empty($plans)) { ?>
                                        <?php foreach ($plans as $plan_k => $plan_v) { ?>
                                            <option value="<?= $plan_k ?>" <?= $current_plan_id == $plan_k ? " selected " : "" ?> >
                                                <?= $plan_v ?>
                                            </option>
                                        <?php } ?>
                                    <?php } ?>
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="">New Plan</label>
                                <select name="new_plan_id" id="cp_new_plan" class="form-control">
                                    <option value="">Choose a new plan</option>
                                    <?php if (!empty($plans)) { ?>
                                        <?php foreach ($plans as $plan_k => $plan_v) { ?>
                                            <option value="<?= $plan_k ?>" <?= $current_plan_id == $plan_k ? " selected " : "" ?> >
                                                <?= $plan_v ?>
                                            </option>
                                        <?php } ?>
                                    <?php } ?>
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="">Token</label>
                                <input type="text" name="token" value="" id="cp_token" class="form-control" readonly
                                       placeholder="Enter card information first to generate token">
                            </div>
                            <div class="form-group">
                                <label for="">Email</label>
                                <input type="text" name="email" value="<?= $email ?>" id="cp_email" class="form-control"
                                       readonly>
                            </div>
                            <button type="submit"
                                    onclick="return confirm('Are you sure to modify the subscription?\n\nYour current subscription will be cancelled\n and a new subscription will be created\n according to the chosen plan ')"
                                    class="btn btn btn-primary">
                                Subscribe
                            </button>
                        </form>
                    </div>

                    <!-------------------------------------------------------------------------------------------->
                    <div id="cc_panel" class="panel"
                         style="background-color: #eeeeee;display:none;">
                        <form id="subscription-form" role="form" onsubmit="return check_card_change_validation()"
                              action="change_card"
                              method="post" class="panel-body">
                            <div class="alert alert-danger" id="card_change_err_div" style="display: none">
                                <strong id="card_change_err_msg"></strong>
                            </div>

                            <?php if ($user_card['existence']) { ?>
                                <div class="text-center">
                                    <strong>
                                        Current Card: <?= $user_card['default_source'] ?>
                                    </strong>
                                    <br>
                                    <strong class="text-center">
                                        Last 4 digits: <?= $user_card['last_4'] ?>
                                    </strong>
                                </div>

                            <?php } ?>

                            <div class="form-group">
                                <span style="color:crimson ;text-align: center;display: block;padding:5px 0">*Enter card information to generate token</span>
                                <label for="">Token</label>
                                <input type="text" name="token" value="" id="cc_token" class="form-control"
                                       readonly
                                       placeholder="Enter card information first to generate token">
                            </div>

                            <button type="submit" class="btn btn btn-primary">
                                Request Card Change
                            </button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div id="pk-div" stripe-publishable-key="<?= $pk ?>">

</div>
<div id="subscription_cancel_alert_modal" class="modal fade" style="">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Successfully Unsubscribed</h4>
                <?php if ($user_id) { ?>

                    <p style="margin-bottom: 20px" class="text-center">
                        <?php if ($this->utility_model->if_user_has_properties($user_id)) { ?>
                            <a href="property" class="btn btn-info">
                                See your properties.
                            </a>
                        <?php } else { ?>
                            <a href="stepTwo" class="btn btn-info">
                                Add a property.
                            </a>
                        <?php } ?>
                    </p>
                <?php } ?>
            </div>

        </div>
    </div>
</div>

<script src="https://js.stripe.com/v3/"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<script src="assets/js/bootstrap.min.js"></script>
<script>
    document.addEventListener("DOMContentLoaded", function (e) {

        <?php if ($this->session->flashdata('subscription_cancel_complete')) { ?>
            $("#subscription_cancel_alert_modal").modal('show');
        <?php } ?>


        <?php if($user_need_to_subscribe){ ?>
        $('#payment-form').collapse('show');
        <?php } ?>

        $("#payment-form").on("hide.bs.collapse", function () {
            $(".card-collapse-span").html('<span class="glyphicon glyphicon-collapse-down"></span>');
        });
        $("#payment-form").on("show.bs.collapse", function () {
            $(".card-collapse-span").html('<span class="glyphicon glyphicon-collapse-up"></span>');
        });

        $("#sc_panel").hide();
        $("#cp_panel").hide();
        //--------------------------------------------------------------------------------------------------------------
        var $pk_div = document.getElementById('pk-div');
        var pk = $pk_div.getAttribute('stripe-publishable-key');
        var stripe = Stripe(pk);
        var elements = stripe.elements();

        // style elements
        var style = {
            base: {
                iconColor: '#666EE8',
                color: '#31325F',
                fontSize: '16px',

                '::placeholder': {
                    color: '#CFD7E0',
                },
            },
        };

        // Create an instance of the Card Element
        var cardNumberElement = elements.create('cardNumber', {
            style: style
        });
        cardNumberElement.mount('#card-number-element');

        // Create an instance of the Card Expiry Element
        var cardExpiryElement = elements.create('cardExpiry', {
            style: style
        });
        cardExpiryElement.mount('#card-expiry-element');

        // Create an instance of the Card CVC Element
        var cardCvcElement = elements.create('cardCvc', {
            style: style
        });
        cardCvcElement.mount('#card-cvc-element');

        // check for card errors
        cardNumberElement.addEventListener('change', function (event) {
            var displayError = document.getElementById('card-errors');
            if (event.error) {
                displayError.textContent = event.error.message;
            } else {
                displayError.textContent = '';
            }
        });

        //--------------------------------------------------------------------------------------------------------------
        var pform = document.getElementById('payment-form');
        pform.addEventListener('submit', function (event) {
            event.preventDefault();

            stripe.createToken(cardNumberElement).then(function (result) {
                if (result.error) {
                    // Inform the user if there was an error
                    var errorElement = document.getElementById('card-errors');
                    errorElement.textContent = result.error.message;
                } else {
                    // Send the token to your server
                    stripeTokenHandler(result.token);
                }
            });
        });

        function stripeTokenHandler(token) {

            var $token = document.getElementById('token');
            var $cp_token = document.getElementById('cp_token');
            var $cc_token = document.getElementById('cc_token');

            $token.value = token.id;
            $cp_token.value = token.id;
            $cc_token.value = token.id;
        }

        //--------------------------------------------------------------------------------------------------------------


        function check_change_plan_validation() {
            $("#cp_err_div").hide();
            var validity = true;

            var plan = $("#cp_plan").val();
            var email = $("#cp_email").val();
            var token = $("#cp_token").val();


            if (plan == '' || email == '' || token == '') {
                $("#cp_err_div").show();
                $("#cp_err_msg").html('* All fields must be filled out');
                validity = false;
            }

            return validity;
        }

        $("#sc_btn").on("click", function () {
            $('#payment-form').collapse('hide');
            $("#sc_panel").show();
            $("#cp_panel").hide();
            $("#cc_panel").hide();
        })

        $("#cp_btn").on("click", function () {
            $('#payment-form').collapse('show');
            $("#cp_panel").show();
            $("#sc_panel").hide();
            $("#cc_panel").hide();
        })

        $("#cc_btn").on("click", function () {
            $('#payment-form').collapse('show');
            $("#cc_panel").show();
            $("#cp_panel").hide();
            $("#sc_panel").hide();

        })
    });

    function check_validation() {
        $("#err_div").hide();
        var validity = true;

        var plan = $("#plan").val();
        var email = $("#email").val();
        var token = $("#token").val();


        if (plan == '' || email == '' || token == '') {
            $("#err_div").show();
            $("#err_msg").html('* All fields must be filled out');
            validity = false;
        }

        return validity;
    }

    function check_card_change_validation() {
        $("#card_change_err_div").hide();
        var validity = true;

        var token = $("#card_change_token").val();

        if (token == '') {
            $("#card_change_err_div").show();
            $("#card_change_err_msg").html('* All fields must be filled out');
            validity = false;
        }

        return validity;
    }

    function check_unsubscribe_validation() {
        $("#sc_err_div").hide();
        var validity = true;

        var plan = $("#sc_plan").val();

        if (plan == '') {
            $("#sc_err_div").show();
            $("#sc_err_msg").html('* All fields must be filled out');
            validity = false;
        }

        return validity;
    }

</script>

</body>
</html>
