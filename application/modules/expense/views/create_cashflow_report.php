<?php $this->load->view('front/headlink'); ?>

        <div class="row">  
            <div class="ss_container">
                <h2>Your cashflow reports<br/><small> </small></h2>
                
                
                
                
                
                <br/><br/>
                    <div class="yourcashflowreports_area">
                            <ul class="nav nav-tabs" role="tablist">
                                    <li role="presentation" class="active"><a href="#viewcategory" aria-controls="home" role="tab" data-toggle="tab">View by category</a></li>
                                    <li role="presentation"><a href="#viewbydate" aria-controls="profile" role="tab" data-toggle="tab">View by date (chronological order)</a></li>
                             </ul>
                                
                                  <!-- Tab panes -->
                                  <div class="tab-content">
                                      <div class="ss_d_top"> <div class="col-md-4">Cash flow for</div>
                                        <div class="col-md-8"><strong> california, New work 1023</strong></div>
                                        <div class="col-md-4">Period</div>
                                        <div class="col-md-8"><strong>14 February 2018 - 14 February 2019</strong></div>
                                        
                                        <div class="clear"></div></div> 

                                    <div role="tabpanel" class="tab-pane active" id="viewcategory">
                                            <div class="row-fluid table_title">
                                                    <div class="col-md-3"></div>
                                                    <div class="col-md-3">Date</div>
                                                    <div class="col-md-2 money_in">Money In</div>
                                                    <div class="col-md-2 money_out">Money Out</div>
                                                    <div class="col-md-2"></div>
                                                </div>
                                                <div class="row-fluid" id="money_in_title_div">
                                                        <div class="col-md-3"><h5>Money In</h5></div>
                                                        <div class="col-md-3"></div>
                                                        <div class="col-md-2 money_in"></div>
                                                        <div class="col-md-2 money_out"></div>
                                                        <div class="col-md-2"></div>
                                                </div>
                                                <div id="rental_incomes_div">
                                                        <div class="row-fluid tax_entry">
                                                            <div class="col-md-3"><p>Rental Income</p></div>
                                                            <div class="col-md-3 entry_date">14 February 2018</div>
                                                            <div class="col-md-2 money_in">$85.71</div>
                                                            <div class="col-md-2 money_out"> </div>
                                                            <div class="col-md-2"></div>
                                                        </div>
                                                       <div class="row-fluid" id="rental_income_total_div">
                                                            <div class="col-md-3"></div>
                                                            <div class="col-md-3"></div>
                                                            <div class="col-md-2 money_in"><p class="total">$85.71</p></div>
                                                            <div class="col-md-2 money_out"></div>
                                                            <div class="col-md-2"></div>
                                                        </div>
                                                </div>
                                                <div id="other_incomes_div">
                                                        <div class="row-fluid" id="other_income_title_div">
                                                            <div class="col-md-3"><h5>Other Income</h5></div>
                                                            <div class="col-md-3"></div>
                                                            <div class="col-md-2 money_in"></div>
                                                            <div class="col-md-2 money_out"></div>
                                                            <div class="col-md-2"></div>
                                                        </div>
                                                        
                                                        
                                                        <div class="row-fluid" id="other_incomes_total_div">
                                                            <div class="col-md-3"></div>
                                                            <div class="col-md-3"></div>
                                                            <div class="col-md-2 money_in"><p class="total">$0.00</p></div>
                                                            <div class="col-md-2 money_out"></div>
                                                            <div class="col-md-2"></div>
                                                        </div>
                                                </div>
                                                <div class="row-fluid special_row">
                                                        <div class="col-md-3"><h5>Total money in</h5></div>
                                                        <div class="col-md-3"></div>
                                                        <div class="col-md-2 money_in"><p class="total">$85.71</p></div>
                                                        <div class="col-md-2 money_out"></div>
                                                        <div class="col-md-2"></div>
                                                </div>
                                                <div class="row-fluid" id="money_out_title_div">
                                                        <div class="col-md-3"><h5>Money Out</h5></div>
                                                        <div class="col-md-3"></div>
                                                        <div class="col-md-2 money_in"></div>
                                                        <div class="col-md-2 money_out"></div>
                                                        <div class="col-md-2"></div>
                                                </div>
                                                <div id="interest_paid_div">
                                                  <div class="row-fluid" id="interest_paid_total_div">
                                                            <div class="col-md-3"><p>Total Interest Paid</p></div>
                                                            <div class="col-md-3"></div>
                                                            <div class="col-md-2 money_in"></div>
                                                            <div class="col-md-2 money_out"><p class="total">($0.00)</p></div>
                                                            <div class="col-md-2"></div>
                                                        </div>
                                                </div>
                                                <div id="other_expenses_div">
                                                        <div class="row-fluid" id="other_expenses_title_div">
                                                            <div class="col-md-3"><h5>Other Expenses</h5></div>
                                                            <div class="col-md-3"></div>
                                                            <div class="col-md-2 money_in"></div>
                                                            <div class="col-md-2 money_out"></div>
                                                            <div class="col-md-2"></div>
                                                        </div>
                                                         <div class="row-fluid" id="other_expenses_total_div">
                                                            <div class="col-md-3"><p>Total Other Expenses</p></div>
                                                            <div class="col-md-3"></div>
                                                            <div class="col-md-2 money_in"></div>
                                                            <div class="col-md-2 money_out"><p class="total">($0.00)</p></div>
                                                            <div class="col-md-2"></div>
                                                        </div>
                                                </div>
                                                <div class="row-fluid special_row">
                                                        <div class="col-md-3"><h5>Total money out</h5></div>
                                                        <div class="col-md-3"></div>
                                                        <div class="col-md-2 money_in"></div>
                                                        <div class="col-md-2 money_out"><p class="total">($0.00)</p></div>
                                                        <div class="col-md-2"></div>
                                                </div>
                                                <div class="row-fluid grand_total">
                                                        <div class="col-md-6"><h5>CASH FLOW Gain or (Loss)</h5></div>
                                                        <div class="col-md-2 money_in"></div>
                                                        <div class="col-md-2 money_out"></div>
                                                        <div class="col-md-2 total">$85.71</div>
                                                </div>








                                    </div>
                                    <div role="tabpanel" class="tab-pane" id="viewbydate"> 

                                            <div class="row-fluid table_title">
                                                    <div class="col-md-3">CategoryDateMoney </div>
                                                    <div class="col-md-3">Date</div>
                                                    <div class="col-md-2 money_in">Money In</div>
                                                    <div class="col-md-2 money_out">Money Out</div>
                                                    <div class="col-md-2"></div>
                                                </div>
                                                <div class="row-fluid" id="money_in_title_div">
                                                        <div class="col-md-3"><h5>Rental Income</h5></div>
                                                        <div class="col-md-3">14 February 2018</div>
                                                        <div class="col-md-2 money_in">$85.71</div>
                                                        <div class="col-md-2 money_out"></div>
                                                        <div class="col-md-2"></div>
                                                </div>
                                                <div id="rental_incomes_div">
                                                        <div class="row-fluid tax_entry">
                                                            <div class="col-md-3"></div>
                                                            <div class="col-md-3 entry_date"> </div>
                                                            <div class="col-md-2 money_in">$85.71</div>
                                                            <div class="col-md-2 money_out"> ($0.00)</div>
                                                            <div class="col-md-2"></div>
                                                        </div>
                                                     
                                                </div>
                                                 
                                                <div class="row-fluid special_row">
                                                        <div class="col-md-3"><h5>Total money out</h5></div>
                                                        <div class="col-md-3"></div>
                                                        <div class="col-md-2 money_in"></div>
                                                        <div class="col-md-2 money_out"><p class="total">($0.00)</p></div>
                                                        <div class="col-md-2"></div>
                                                </div>
                                                <div class="row-fluid grand_total">
                                                        <div class="col-md-6"><h5>CASH FLOW Gain or (Loss)</h5></div>
                                                        <div class="col-md-2 money_in"></div>
                                                        <div class="col-md-2 money_out"></div>
                                                        <div class="col-md-2 total">$85.71</div>
                                                </div>





                                    </div>
 
                                  </div>
                    </div>
                         
                        <div class="clear extra_padding text-center">
                            <br/><br/>
                            <button class="btn btn-primary" data-toggle="modal" data-target="#report_download_list" >Download</button>  
                            <button class="btn btn-light" data-toggle="modal" data-target="#report_share_list">Share</button>
                        </div>
                        </div>
            </div>
        </div>    
        </div>
        </div>	
    
       
    
    </div>
 
 <!-- Modal -->
<div class="modal fade" id="report_download_list" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
          <div class="modal-content">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
              <h4 class="modal-title" id="myModalLabel">Download Cash Flow</h4>
            </div>
            <div class="modal-body">
              <p>Select a format to Download Cash Flow</p>
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-light" data-dismiss="modal">Close</button>
              <button type="button" class="btn btn-primary">PDF</button>
              <button type="button" class="btn btn-primary">Excel</button>
              <button type="button" class="btn btn-primary">CSV</button>
              
            </div>
          </div>
        </div>
      </div>

 <!-- Modal -->
 <div class="modal fade" id="report_share_list" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
          <div class="modal-content">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
              <h4 class="modal-title" id="myModalLabel">Share with</h4>
            </div>
            <div class="modal-body">
               <form>
                    <div class="form-group">
                            <label for="exampleInputEmail1">Email address</label>
                            <input type="email" class="form-control" id="exampleInputEmail1" placeholder="address@mail.com">
                    </div>
                    <div class="form-group">
                            <label for="exampleInputName2">Name</label>
                            <input type="text" class="form-control" id="exampleInputName2" placeholder="Jane Doe">
                    </div>
               </form>
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-light pull-left" data-dismiss="modal">Close</button>
              <button type="button" class="btn btn-primary pull-right">Share</button>
 
              
            </div>
          </div>
        </div>
      </div>
    
      
 
 <?php $this->load->view('front/footerlinkk'); ?> 