<?php $this->load->view('front/headlink'); ?>
<div class="row">  
  <div class="ss_container">
    <h3>Expenses and Invoices <small>for the property at yu, Waterbury, VT 056</small> <a data-toggle="modal" data-target="#addnewpaiddueexpense" class="pull-right btn btn-primary"  ><span class="glyphicon glyphicon-plus" aria-hidden="true"></span> Add New Paid or Due Expense</a></h3>
    <div class="ss_bound_content">
      <br/><br/><br/>
      <!-- Nav tabs -->
      <div class="ss_expensesinvoices">
        <div class="ss_expensesinvoices_top">
          <div class="col-md-2"><h2>Period</h2></div>
          <div class="col-md-4">
            <div id="fromDate2" class="input-group date" data-date-format="mm-dd-yyyy">
              <input class="form-control" type="text" readonly />
              <span class="input-group-addon"><i class="glyphicon glyphicon-calendar"></i></span>
            </div>
          </div>
          <div class="col-md-4">
            <div id="fromDate3" class="input-group date" data-date-format="mm-dd-yyyy">
              <input class="form-control" type="text" readonly />
              <span class="input-group-addon"><i class="glyphicon glyphicon-calendar"></i></span>
            </div>
          </div>
          <div class="col-md-2"><a href="#"  data-toggle="modal" data-target="#taxreport">Generate Tax Report</a></div>
        </div>
      </div>
    </div>
    <div class="ss_expensesinvoices_content">
      <h3>Paid Expenses</h3>
      <div class="expense_title_bar">
        <div class="row-fluid top-table-name">
          <div class="col-md-2">
            <p>Due Date</p>
          </div>
          <div class="col-md-3">
            <p>Paying Who?</p>
          </div>
          <div class="col-md-2">
            <p>Amount Due</p>
          </div>
          <div class="col-md-1">
          </div>
          <div class="col-md-1">
          </div>
          <div class="col-md-2">
            <p>Tax Code</p>
          </div>
          <div class="col-md-1">
          </div>
        </div>
      </div>
      <div class="ss_expensesinvoices_c_view" >
        <div class="row-fluid row-inner ss_expanse_top_h">
          <div class="col-md-2">
            <h4 class="table-name">Due Date</h4>
            <p class="unpaid_expense_date">Feb 01, 2018</p>
          </div>
          <div class="col-md-3">
            <h4 class="table-name">Paying Who?</h4>
            <p class="unpaid_paying_who">uqqqqqqq</p>
          </div>
          <div class="col-md-2">
            <h4 class="table-name">Amount Due</h4>
            <p class="unpaid_amountdue expense_amountdue">$123.00</p>
          </div>
          <div class="col-md-1">
            <p class="unpaid_duetime">12 days ago</p>
          </div>
          <div class="col-md-1">
          </div>
          <div class="col-md-2">
            <h4 class="table-name">Tax Code</h4>
            <p class="unpaid_taxcode">Property repair and maintenance</p>
          </div>
          <div class="unpaid_overdue_label">
            <p><span class="label label-warning">Overdue</span></p>
          </div>
        </div>
        <div class="row ss_wxpance_ifo" style="display:none;">
          <ul class="payment-info">
            <div class="row-fluid">
              <div class="col-md-8">
                <div class="row-fluid">
                  <div class="col-md-3">
                    <p class="bottom-text">Payments:</p>
                  </div>
                  <div class="col-md-9">
                    <p class="paid_payments"> <span class="paid_payment">$222,222.00 on Feb 28, 2018 paid via 1</span>  </p>
                  </div>
                </div>
                <div class="row-fluid">
                  <div class="col-md-3">
                    <p class="bottom-text">Attachments:</p>
                  </div>
                  <div class="col-md-9">
                    <p class="attachments_links">
                      <a class="paid_attachments" target="_blank" href="img/rent.png">
                        <span class="paid_attachment">rent.png, </span>
                      </a><a class="paid_attachments" target="_blank" href="img/loginbg.jpg">
                        <span class="paid_attachment">loginbg.jpg</span>
                      </a>
                      <a class="alast_flag" id="last_flag10" href="#" style="display:none;">I am not visible</a>
                    </p>
                  </div>
                </div>
                <div class="row-fluid">
                  <div class="col-md-3">
                    <p class="bottom-text">Comments:</p>
                  </div>
                  <div class="col-md-9">
                    <p class="paid_comments">ddws  www</p>
                  </div>
                </div>
              </div>
              <div class="col-md-1"></div>
              <div class="col-md-3">
                <div class="bottom-link">
                  <p><a href="#_" id="paid_expense_edit" class="paid_expense_edit">Edit expense</a></p>                                                                   
                  <p> <a href="javascript:void(0);" class="add_attachment">Add attachment</a>  </p>                                                                                                                                   
                  <p> <a class="delete_expense" href="#_">Delete this expense</a> </p>
                </div>
              </div>
            </div>
          </ul>
        </div>
      </div>
      <div id="paid_expense_editable" style="display: none;">
        <form action="" method="POST" name="">
          <div class="row-fluid">
            <div class="col-md-2">
              <p><input class="form-control" type="text" value="Feb 02, 2018" id="dp1518596682052"></p>
            </div>
            <div class="col-md-3">
              <p><input class="form-control" id="" type="text" value="aminaaa"></p>
            </div>
            <div class="col-md-2">
              <p class="paid_editable_amountpaid">222,222.00</p>
            </div>
            <div class="col-md-2">
              <p class="paid_editable_paymentmethod">1</p>
            </div>
            <div class="col-md-3">
              <p>
                <select class="form-control paid-other"  id="paid_editable_taxcode">
                  <optgroup label="General Expenses">
                    <option value="1">Accounting/bookkeeping fees</option><option value="2">Bank fees</option><option value="3">Council rates</option><option value="4">GST</option><option value="5" selected="selected">Insurance</option><option value="6">Land tax</option><option value="7">Legal fees</option><option value="8">Loan interest repayments</option><option value="9">Management &amp; admin fees</option><option value="10">Other</option><option value="11">Property advertising</option><option value="12">Property cleaning</option><option value="13">Property repair and maintenance</option><option value="14">Real estate agent fees</option><option value="15">Stationary and postage</option><option value="16">Strata and body corporate</option><option value="17">Utilities (electricity, gas, water)</option></optgroup><optgroup label="Capital Expenses">
                      <option value="18">Building inspection</option><option value="19">Legal costs</option><option value="20">Stamp duty</option><option value="21">Pest control</option><option value="22">Other purchase costs</option></optgroup><optgroup label="Property Sale Expenses">
                        <option value="23">Legal costs</option><option value="24">Real estate commission</option><option value="25">Other sales expenses</option></optgroup></select>
                      </p>
                      <div class="col-md-3 " id="paid_other_expense_div">
                        <input  id="paid_other_expense" type="text" placeholder="Received by" value="" style="display:none;">
                      </div>
                    </div>
                  </div>
                  <ul>
                    <div class="row-fluid">
                      <div class="col-md-8">
                        <div class="row-fluid">
                          <div class="col-md-3">
                            <p class="bottom-text">Payments:</p>
                          </div>
                          <div class="col-md-9">
                            <p class="paid_editable_payments">
                              <span class="paid_editable_payment">$222,222.00 on Feb 28, 2018 paid via 1</span>
                              <a class="delete_payment" href="#">
                                <img src="img/expense_delete.png">
                              </a>
                            </p>
                            <p id="last_flag17" style="display:none;"> </p>
                          </div>
                        </div>
                        <div class="row-fluid">
                          <div class="col-md-3">
                            <p class="bottom-text">Attachments:</p>
                          </div>
                          <div class="col-md-9">
                            <p class="attachments_links">
                            </p><div class="paid_editable_attachments list-item">
                              <a href="img/rent.png">
                                <span class="paid_editable_attachment">rent.png, </span>
                              </a>
                              <a class="delete_attachment" href="#">
                                <img src="img/expense_delete.png">
                              </a>
                            </div><div class="paid_editable_attachments list-item">
                              <a href="img/loginbg.jpg">
                                <span class="paid_editable_attachment">loginbg.jpg</span>
                              </a>
                              <a class="delete_attachment" href="#">
                                <img src="img/expense_delete.png">
                              </a>
                            </div>
                            <div id="last_flag18" style="display:none;"> </div>
                            <p></p>
                          </div>
                        </div>
                        <div class="row-fluid">
                          <div class="col-md-3">
                            <p class="bottom-text">Comments:</p>
                          </div>
                          <div class="col-md-9">
                            <p>
                              <textarea placeholder="" name="paid_editable_comments" class="form-control" rows="4">ddws  www</textarea>
                            </p>
                          </div>
                        </div>
                      </div>
                      <div class="span1"></div>
                      <div class="span3 text-right">
                        <a href="#_" class="btn btn-primary" id="paid_expense_edit_save">Save changes</a>
                        <p><a href="#_" class="btn btn-light" id="paid_expense_edit_cancel">Cancel changes</a></p>
                      </div>
                    </div>
                  </ul>
                </form>
              </div>
            </div>
            <div class="clear extra_padding">
              <a class="btn btn-light" href="dashboard_poperty.html">Back To Dashboard</a>
            </div>
          </div>
        </div>
      </div>    
    </div>
  </div>	
</div>
<div class="modal fade" id="addnewpaiddueexpense" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Add an Expense</h4>
      </div>
      <div class="modal-body">
        <div class="stepwizard">
          <div class="stepwizard-row">
            <div class="stepwizard-step">
              <a class="btn btn-default btn-circle active-step" href="#step-1" data-toggle="tab" onclick="stepnext(1)" >1</a>
            </div>
            <div class="stepwizard-step">
              <a class="btn btn-default btn-circle" disabled="disabled" href="#step-2" data-toggle="tab">2</a>
            </div>
          </div>
          <div class="rate-updates">
            <div class="tab-content margintop0" style="border:none !important;">
              <div class="tab-pane fade active in padding20" id="step-1" >
                <div class="step001"> 
                  <form>
                    <div class="form-group">
                      <div class="col-md-5"><label >Who is this bill from *<br/>
                        <small>(person or business you are paying)</small></label>
                      </div>
                        <div class="col-md-7">
                          <input type="text" class="form-control"  placeholder="e.g. SA Water">
                        </div>    
                      </div>  
                      <div class="form-group">
                        <div class="col-md-5"><label >Total amount on this invoice *</label></div>
                        <div class="col-md-7">
                          <input type="number" class="form-control"  placeholder="$0.00">
                        </div>    
                      </div>
                      <div class="form-group">
                        <div class="col-md-5"><label >This bill has been *</label></div>
                        <div class="col-md-7">
                          <label class="radio-inline">
                            <input type="radio" name="inlineRadioOptions" id="inlineRadio1" value="option1">  Paid in full
                          </label>
                          <label class="radio-inline">
                            <input type="radio" name="inlineRadioOptions" id="inlineRadio2" value="option2"> Part paid
                          </label>
                          <label class="radio-inline">
                            <input type="radio" name="inlineRadioOptions" id="inlineRadio3" value="option3"> Not paid
                          </label>
                        </div>    
                      </div> 
                      <div class="form-group">
                        <div class="col-md-5"><label >This bill is due on *</label></div>
                        <div class="col-md-7">
                          <input type="text" id="datepicker" class="hasDatepicker form-control" placeholder="Nov 09, 2013">                   
                        </div>    
                      </div>
                      <div class="form-group">
                        <div class="col-md-5"><label >Paid details *<br/><small>(amount paid, date, how)</small></label></div>
                        <div class="col-md-7">
                          <div class="row">
                            <div class="col-md-4"><input type="number"  class=" form-control" placeholder="$0.00"></div>
                            <div class="col-md-4"><input type="text" id="fromDate1" class="hasDatepicker form-control" placeholder="Nov 09, 2013"></div>
                            <div class="col-md-4"><input type="text"  class="col-md-4  form-control" placeholder="e.g. Cash"></div>
                          </div>                   
                        </div>    
                      </div>
                    </form>  
                  </div>
                  <button class="btn btn-xs btn-primary" onclick="stepnext(2);" type="button"><i class="icon-next"></i> Next (Final) Step</button>
                </div>
                <div class="tab-pane fade padding20 " id="step-2">
                  <div class="step002"> 
                    <h5><strong> Let's quickly include additional details for tax reporting purposes?</strong><br/>
                    We recommend doing this now, but you can skip this and come back to it later</h5>
                    <br/><br/>
                    <div class="form-group">
                      <div class="col-md-5"><label >Upload a scanned copy <small> (or photo)</small>  of the invoice or receipt</label></div>
                      <div class="col-md-7">
                        <div class="form-group inputDnD">
                          <input type="file" class="form-control-file text-primary font-weight-bold" id="inputFile" accept="image/*" onchange="readUrl(this)" data-title="Drag and drop a file">
                        </div>
                      </div>    
                    </div>
                    <div class="form-group">
                      <div class="col-md-5"><label >Tax Code (helps you create accurate tax reports for you and your accountant)</label></div>
                      <div class="col-md-7">
                        <select name=""  class="form-control"  >
                          <optgroup>
                            <option value="">Please select a tax category</option>
                          </optgroup>
                          <optgroup label="General Expenses">
                            <option value="1">Accounting/bookkeeping fees</option>
                            <option value="2">Bank fees</option>
                            <option value="3">Council rates</option>
                            <option value="4">GST</option>
                            <option value="5">Insurance</option>
                            <option value="6">Land tax</option>
                            <option value="7">Legal fees</option>
                            <option value="8">Loan interest repayments</option>
                            <option value="9">Management &amp; admin fees</option>
                            <option value="10">Other</option>
                            <option value="11">Property advertising</option>
                            <option value="12">Property cleaning</option>
                            <option value="13">Property repair and maintenance</option>
                            <option value="14">Real estate agent fees</option>
                            <option value="15">Stationary and postage</option>
                            <option value="16">Strata and body corporate</option>
                            <option value="17">Utilities (electricity, gas, water)</option>
                          </optgroup>
                          <optgroup label="Capital Expenses">
                            <option value="18">Building inspection</option>
                            <option value="19">Legal costs</option>
                            <option value="20">Stamp duty</option>
                            <option value="21">Pest control</option>
                            <option value="22">Other purchase costs</option>
                          </optgroup>
                          <optgroup label="Property Sale Expenses">
                              <option value="23">Legal costs</option>
                              <option value="24">Real estate commission</option>
                              <option value="25">Other sales expenses</option>
                          </optgroup>
                        </select>
                      </div>    
                    </div>
                            <div class="form-group">
                              <div class="col-md-5"><label >Additional notes</label></div>
                              <div class="col-md-7">
                                <textarea  class="form-control"></textarea>
                              </div>    
                            </div>
                          </div>
                          <button class="btn btn-xs btn-primary" data-dismiss="modal" type="button"><i class="icon-next"></i> Finish</button>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div id="taxreport"  class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
            <div class="modal-dialog" role="document">
              <div class="modal-content">
                <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                  <h4 class="modal-title" id="myModalLabel">Let's create a Tax Report</h4>
                </div>
                <div class="modal-body">
                  <p>P.S Have you entered all your income + expenses ?</p>
                  <ul>
                    <li>interest payments</li>
                    <li> rental income</li>
                    <li>advertising expenses  </li>
                  </ul> 
                </div>
                <div class="modal-footer">
                  <button type="button" class="btn btn-light" data-dismiss="modal">Close</button>
                  <a href="yourcashflowreports.html" type="button" class="btn btn-primary">Yes? ... Let's Generate Your Report</a>
                </div>
              </div>
            </div>
          </div>
          <?php $this->load->view('front/footerlinkk'); ?> 
          <script>
            $(".ss_expanse_top_h").click(function(){
              $(".ss_wxpance_ifo").toggle("slow");
            });
            $("#paid_expense_edit").click(function(){
              $(".ss_expanse_top_h, .ss_wxpance_ifo ").hide(1000);
              $("#paid_expense_editable ").show(1000);
            });
            $("#paid_expense_edit_save, #paid_expense_edit_cancel").click(function(){
              $(".ss_expanse_top_h, .ss_wxpance_ifo ").show(1000);
              $("#paid_expense_editable ").hide(1000);
            });
            $(".delete_expense").click(function(){
              $(".ss_expensesinvoices_c_view, #paid_expense_editable").remove();
            });
            function stepnext(n){
              if(n != 0){
//$(".stepwizard-row a").switchClass('btn-primary','btn-default');
$(".stepwizard-row a").removeClass('btn-primary');
$(".stepwizard-row a").addClass('btn-default');
$('.stepwizard a[href="#step-'+n+'"]').tab('show');
//$('.stepwizard-row a[href="#step-'+n+'"]').switchClass('btn-default','btn-primary');
$('.stepwizard-row a[href="#step-'+n+'"]').removeClass('btn-default');
$('.stepwizard-row a[href="#step-'+n+'"]').addClass('btn-primary');
}
}
stepnext(1);
</script>