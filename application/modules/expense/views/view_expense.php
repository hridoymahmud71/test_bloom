<?php $this->load->view('front/headlink'); ?>
<?php $this->load->view('front/top_menu'); ?>

<div class="container">
    <?php $this->load->view('front/head_nav'); ?>
    <style>
        #maintenance_id_wrapper {
            display: none;
        }

        .maintenance_f_wrapper {
            display: none;
        }

        .maintenance_p {
            background-color: #13b6dc;
            padding: 4px;
            border: 2px solid black;
            border-radius: 4px;
        }

        .maintenance_p a {
            color: white;
            font-weight: bold;
        }
    </style>
    <div class="row">
        <div class="ss_container">
            <h3 class="extra_heading">Expenses and Invoices
                <small> for the property at

                    <?= $this->utility_model->getFullPropertyAddress($property_info[0]['property_id'])?>
                </small>

            </h3>
            <div>
                <?php if (!$is_archived) { ?>
                    <a data-toggle="modal" data-target="#addnewpaiddueexpense" class="pull-right btn btn-primary"><span
                                class="glyphicon glyphicon-plus" aria-hidden="true"></span> Add New Paid or Due Expense</a>
                <?php } ?>
            </div>
            <div class="ss_bound_content" style="display: none">
                <br/><br/><br/>
                <!-- Nav tabs -->
                <div class="ss_expensesinvoices">
                    <div class="ss_expensesinvoices_top">
                        <div class="col-md-2"><h2>Period</h2></div>
                        <div class="col-md-4">
                            <div id="fromDate2" class="input-group date" data-date-format="mm-dd-yyyy">
                                <input class="form-control" type="text" readonly/>
                                <span class="input-group-addon"><i class="glyphicon glyphicon-calendar"></i></span>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div id="fromDate3" class="input-group date" data-date-format="mm-dd-yyyy">
                                <input class="form-control" type="text" readonly/>
                                <span class="input-group-addon"><i class="glyphicon glyphicon-calendar"></i></span>
                            </div>
                        </div>
                        <div class="col-md-2"><a href="#" data-toggle="modal" data-target="#taxreport">Generate Tax
                                Report</a></div>
                    </div>
                </div>
            </div>
            <div class="ss_expensesinvoices_content">

                <?php
                $full_expense = 0;
                $part_expense = 0;
                $not_expense = 0;
                ?>
                <?php foreach ($all_expense as $row) {
                    if ($row['expense_paying_status'] == 1) {
                        $full_expense = $full_expense + 1;
                    }
                    if ($row['expense_paying_status'] == 2) {
                        $part_expense = $part_expense + 1;
                    }
                    if ($row['expense_paying_status'] == 3) {
                        $not_expense = $not_expense + 1;
                    }
                } ?>

                <?php if (!$all_expense) { ?>
                    <h4 style="font-style: italic;color:grey">No expenses have been entered yet</h4>
                <?php } ?>
                <?php if ($full_expense > 0) { ?>
                    <h4 style="font-weight: bold;margin-top=20px">Paid Expenses</h4>
                    <div class="expense_title_bar">
                        <div class="row-fluid top-table-name">
                            <div class="col-md-2">
                                <p>Due Date</p>
                            </div>
                            <div class="col-md-3">
                                <p>Creditor</p>
                            </div>
                            <div class="col-md-2">
                                <p>Amount Paid</p>
                            </div>
                            <div class="col-md-2">
                                <p>Pay Method</p>
                            </div>
                            <div class="col-md-3">
                                <p>Tax Code</p>
                            </div>
                        </div>
                    </div>

                    <?php foreach ($all_expense as $row) { ?>
                        <form action="expense/update_expense_info" class="f_form" method="post">
                            <?php
                            $a_mntnc = $this->expense_model->getAMaintenance($row['maintenance_id']);
                            ?>
                            <?php if ($row['expense_paying_status'] == 1) { ?>
                                <div class="ss_expensesinvoices_c_view">
                                    <div class="row-fluid row-inner ss_expanse_top_h">
                                        <div class="col-md-2">
                                            <h4 class="table-name">Due Date</h4>
                                            <p class="unpaid_expense_date">
                                                <?= date("M d, Y", strtotime($row['expense_payment_due'])); ?>
                                            </p>
                                        </div>
                                        <div class="col-md-3">
                                            <h4 class="table-name">Creditor</h4>
                                            <p class="unpaid_paying_who"><?= $row['expense_bill_from']; ?></p>
                                        </div>
                                        <div class="col-md-2">
                                            <h4 class="table-name">Amount Due</h4>
                                            <p class="unpaid_amountdue expense_amountdue">
                                                $<?= $row['total_invoice_amount']; ?></p>
                                        </div>
                                        <div class="col-md-2">
                                            <p class="unpaid_duetime"><?= $row['expense_payment_method']; ?></p>
                                        </div>

                                        <div class="col-md-3">
                                            <h4 class="table-name">Tax Code</h4>
                                            <p class="unpaid_taxcode"><?= $row['tax_code']; ?></p>
                                            <?php if ($a_mntnc) { ?>
                                                <p class="maintenance_p">
                                                    <a href="Editmaintenance/<?= $property_id ?>/<?= $a_mntnc['maintenance_id'] ?>">
                                                        <?= $a_mntnc['maintenance_issue'] ?>
                                                        | <?= $a_mntnc['maintenance_location'] ?><br>
                                                        <?= $a_mntnc['create_at'] ? $a_mntnc['create_at'] : ''; ?>
                                                    </a>
                                                </p>
                                            <?php } ?>
                                        </div>
                                    </div>
                                    <div class="row-fluid row-inner ss_expanse_top_h_edit" style="display: none;">
                                        <div class="col-md-2">
                                            <h4 class="table-name">Due Date</h4>
                                            <p class="unpaid_expense_date">
                                                <input class="form-control expense_payment_due_chk" type="text" readonly
                                                       name="expense_payment_due"
                                                       value="<?= date("M d Y", strtotime($row['expense_payment_due'])); ?>">
                                            </p>
                                        </div>
                                        <div class="col-md-3">
                                            <h4 class="table-name">Creditor</h4>
                                            <p>
                                                <input class="form-control" type="text" name="expense_bill_from"
                                                       value="<?= $row['expense_bill_from']; ?>">
                                            </p>
                                        </div>
                                        <div class="col-md-2">
                                            <h4 class="table-name">Amount Due</h4>
                                            <p class="unpaid_amountdue expense_amountdue">
                                                $<?= $row['total_invoice_amount']; ?></p>
                                        </div>
                                        <div class="col-md-2">
                                            <p class="unpaid_duetime"><?= $row['expense_payment_method']; ?></p>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="row f_row">
                                                <div class="col-md-12">
                                                    <h4 class="table-name">Invoice Type</h4>
                                                    <p class="unpaid_taxcode">
                                                        <select name="tax_code" id="tax_code_validate"
                                                                class="form-control tax_code_validate_f">
                                                            <option <?php if ($row['tax_code'] == "council rates") {
                                                                echo "selected";
                                                            } ?> value="council rates">Council Rates
                                                            </option>
                                                            <option <?php if ($row['tax_code'] == "insurance") {
                                                                echo "selected";
                                                            } ?> value="insurance">Insurance
                                                            </option>
                                                            <option <?php if ($row['tax_code'] == "land tax") {
                                                                echo "selected";
                                                            } ?> value="land tax">Land Tax
                                                            </option>
                                                            <option <?php if ($row['tax_code'] == "general repairs") {
                                                                echo "selected";
                                                            } ?> value="general repairs">General Repairs
                                                            </option>
                                                            <option <?php if ($row['tax_code'] == "electricity") {
                                                                echo "selected";
                                                            } ?> value="electricity">Electricity
                                                            </option>
                                                            <option <?php if ($row['tax_code'] == "plumbing") {
                                                                echo "selected";
                                                            } ?> value="plumbing">Plumbing
                                                            </option>
                                                            <option <?php if ($row['tax_code'] == "strata") {
                                                                echo "selected";
                                                            } ?> value="strata">Strata
                                                            </option>
                                                            <option <?php if ($row['tax_code'] == "water") {
                                                                echo "selected";
                                                            } ?> value="water">Water
                                                            </option>
                                                            <option <?php if ($row['tax_code'] == "other") {
                                                                echo "selected";
                                                            } ?> value="other">Other
                                                            </option>
                                                        </select>
                                                    </p>
                                                </div>
                                                <div class="col-md-12 maintenance_f_wrapper <?= in_array($row['tax_code'], $maintenance_related_tax_codes) ? ' maintenance_f_wrapper_show ' : '' ?> ">
                                                    <h4 class="table-name">Maintenance</h4>

                                                    <?php if ($a_mntnc) { ?>
                                                        <p class="maintenance_p">
                                                            <a href="Editmaintenance/<?= $property_id ?>/<?= $a_mntnc['maintenance_id'] ?>">
                                                                <?= $a_mntnc['maintenance_issue'] ?>
                                                                | <?= $a_mntnc['maintenance_location'] ?><br>
                                                                <?= $a_mntnc['create_at'] ? $a_mntnc['create_at'] : ''; ?>
                                                            </a>
                                                        </p>
                                                    <?php } ?>
                                                    <p>
                                                        <select name="maintenance_id"
                                                                class="form-control maintenance_f">
                                                            <option value="" <?= $row['maintenance_id'] == 0 ? ' selected ' : ''; ?>>
                                                                Please select a maintenance
                                                            </option>
                                                            <?php if ($maintenance_list) { ?>
                                                                <?php foreach ($maintenance_list as $a_maintenance) { ?>
                                                                    <option value="<?= $a_maintenance['maintenance_id'] ?>" <?= $row['maintenance_id'] == $a_maintenance['maintenance_id'] ? ' selected ' : ''; ?>>
                                                                        <?= $a_maintenance['maintenance_issue'] ?>
                                                                        | <?= $a_maintenance['maintenance_location'] ?>
                                                                        | <?= $a_maintenance['create_at'] ? $a_maintenance['create_at'] : ''; ?>
                                                                    </option>
                                                                <?php } ?>
                                                            <?php } ?>
                                                        </select>
                                                    </p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row ss_wxpance_ifo" style="display:none;">
                                        <ul class="payment-info">
                                            <div class="row-fluid">
                                                <div class="col-md-8">
                                                    <div class="row-fluid">
                                                        <div class="col-md-3">
                                                            <p class="bottom-text">Payments:</p>
                                                        </div>
                                                        <div class="col-md-9">
                                                            <p class="paid_payments">
                                                                <?php foreach ($all_expense_in_receive_amount as $amnt_row) { ?>
                                                                    <?php if ($amnt_row['expense_id'] == $row['expense_id']) { ?>
                                                                        <!--<pre>
                                                                        <?php /*print_r($amnt_row)*/?>
                                                                        </pre>-->
                                                                        <span class="paid_payment">
                                                                            $<?= $amnt_row['total_invoice_amount'] ?>
                                                                            <?php if(!empty($amnt_row['expense_paid_date'])) { ?>
                                                                                on <?= date("M d Y", strtotime($amnt_row['expense_paid_date'])); ?>                                                                                &nbsp;
                                                                            <?php } ?>

                                                                            <?php if(!empty($amnt_row['expense_paid_date_new'])) { ?>
                                                                                paid via <?= $amnt_row['expense_payment_method_new'] ?>
                                                                            <?php } ?>
                                                                        </span>
                                                                    <?php } ?>
                                                                <?php } ?>
                                                            </p>
                                                        </div>
                                                    </div>

                                                    <div class="row-fluid">
                                                        <div class="col-md-3">
                                                            <p class="bottom-text">Attachments:</p>
                                                        </div>
                                                        <div class="col-md-9">
                                                            <p class="attachments_links">
                                                                <?php foreach ($expense_doc as $doc_row) { ?>
                                                                    <?php if ($doc_row['expense_id'] == $row['expense_id']) { ?>
                                                                        <a class="paid_attachments" download
                                                                           href="uploads/expense_document/<?= $doc_row['expense_in_document'] ?>">
                                                                            <span class="paid_attachment"><?= $doc_row['expense_in_document'] ?></span>
                                                                        </a>
                                                                    <?php } ?>
                                                                <?php } ?>
                                                            </p>
                                                        </div>
                                                    </div>


                                                    <div class="row-fluid">
                                                        <div class="col-md-3">
                                                            <p class="bottom-text">Receipt Files :</p>
                                                        </div>
                                                        <div class="col-md-9">
                                                            <p class="attachments_links">
                                                                <?php foreach ($expense_receipt_file as $an_expense_receipt_file) { ?>
                                                                    <?php if ($an_expense_receipt_file['expense_id'] == $row['expense_id']) { ?>
                                                                        <a class="paid_attachments" download
                                                                           href="uploads/expense_receipt_file/<?= $an_expense_receipt_file['expense_receipt_file'] ?>">
                                                                            <span class="paid_attachment"><?= $an_expense_receipt_file['expense_receipt_file'] ?></span>
                                                                        </a>
                                                                    <?php } ?>
                                                                <?php } ?>
                                                            </p>
                                                        </div>
                                                    </div>

                                                    <div class="row-fluid">
                                                        <div class="col-md-3">
                                                            <p class="bottom-text">Comments:</p>
                                                        </div>
                                                        <div class="col-md-9">
                                                            <p class="paid_comments"><?= $row['description'] ?></p>
                                                            <p class="paid_comments_chng" style="display: none;">
                                                                <textarea cols="60" rows="5"
                                                                          name="description"><?= $row['description']; ?></textarea>
                                                            </p>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-1"></div>
                                                <div class="col-md-3">
                                                    <div class="bottom-link submit_btn">
                                                        <?php if (!$is_archived) { ?>
                                                            <p><a style="cursor:pointer;" id="paid_expense_edit"
                                                                  class="paid_expense_edit">Edit expense</a></p>
                                                            <p><a href="javascript:void(0);"
                                                                  class="add_attachment OpenImgUpload">Add
                                                                    attachment</a>
                                                            </p>
                                                            <input type="hidden" class="expense_in_close_id"
                                                                   name="expense_id" value="<?= $row['expense_id'] ?>">
                                                            <p><a class="delete_expense"
                                                                  href="expense/delete_expense_in/<?= $row['expense_id'] ?>/<?= $property_id; ?>">Delete
                                                                    this expense</a></p>
                                                        <?php } ?>
                                                    </div>
                                                    <div class="span3 text-right submit_btn_chng"
                                                         style="display: none;">
                                                        <input type="hidden" name="property_id"
                                                               value="<?= $property_id; ?>">
                                                        <p><input type="submit"
                                                                  class="btn btn-primary paid_expense_edit_save"
                                                                  value="Save changes"></p>
                                                        <p><a class="btn btn-light paid_expense_edit_cancel">Cancel
                                                                changes</a></p>
                                                    </div>
                                                </div>
                                            </div>
                                        </ul>
                                    </div>
                                </div>
                            <?php } ?>
                        </form>
                    <?php } ?>

                <?php } ?>

                <?php if ($part_expense > 0) { ?>
                    <h4 style="font-weight: bold;margin-top:40px">Part-Paid Expenses</h4>
                    <div class="expense_title_bar">
                        <div class="row-fluid top-table-name">
                            <div class="col-md-2">
                                <p>Due Date</p>
                            </div>
                            <div class="col-md-2">
                                <p>Creditor</p>
                            </div>
                            <div class="col-md-2">
                                <p>Amount Due</p>
                            </div>
                            <div class="col-md-2">
                                <p>Amount Owing</p>
                            </div>
                            <div class="col-md-2">
                                <p>Tax Code</p>
                            </div>
                            <div class="col-md-1">
                            </div>
                            <div class="col-md-1">
                            </div>
                        </div>
                    </div>

                    <?php foreach ($all_expense as $row) { ?>
                        <form action="expense/update_expense_info" class="f_form" method="post">
                            <?php
                            $a_mntnc = $this->expense_model->getAMaintenance($row['maintenance_id']);
                            ?>
                            <?php if ($row['expense_paying_status'] == 2) { ?>
                                <div class="ss_expensesinvoices_c_view"
                                    <?php if ($row['expense_payment_due'] <= date('Y-m-d')) { ?> id="overdue-<?= $row['expense_id'] ?>" <?php } ?>
                                >
                                    <div class="row-fluid row-inner ss_expanse_top_h">
                                        <div class="col-md-2">
                                            <h4 class="table-name">Due Date</h4>
                                            <p class="unpaid_expense_date">
                                                <?= date("M d, Y", strtotime($row['expense_payment_due'])); ?>
                                            </p>
                                        </div>
                                        <div class="col-md-2">
                                            <h4 class="table-name">Creditor</h4>
                                            <p class="unpaid_paying_who"><?= $row['expense_bill_from']; ?></p>
                                        </div>
                                        <div class="col-md-2">
                                            <h4 class="table-name">Amount Due</h4>
                                            <p class="unpaid_amountdue expense_amountdue">
                                                $<?= $row['total_invoice_amount']; ?></p>
                                        </div>
                                        <div class="col-md-2">
                                            <p class="unpaid_duetime">
                                                <?php $count_val = 0;
                                                foreach ($all_expense_in_receive_amount as $key => $value) { ?>
                                                    <?php if ($value['expense_id'] == $row['expense_id']) { ?>
                                                        <?php $count_val = $count_val + $value['total_invoice_recieving_amount']; ?>
                                                    <?php } ?>
                                                <?php } ?>
                                                $<?php echo $row['total_invoice_amount'] - $count_val; ?>
                                            </p>
                                        </div>
                                        <div class="col-md-2">
                                            <h4 class="table-name">Tax Code</h4>
                                            <p class="unpaid_taxcode"><?= $row['tax_code']; ?></p>
                                            <?php if ($a_mntnc) { ?>
                                                <p class="maintenance_p">
                                                    <a href="Editmaintenance/<?= $property_id ?>/<?= $a_mntnc['maintenance_id'] ?>">
                                                        <?= $a_mntnc['maintenance_issue'] ?>
                                                        | <?= $a_mntnc['maintenance_location'] ?><br>
                                                        <?= $a_mntnc['create_at'] ? $a_mntnc['create_at'] : ''; ?>
                                                    </a>
                                                </p>
                                            <?php } ?>
                                        </div>
                                        <div class="unpaid_overdue_label col-md-1">
                                            <p>
                                                <?php if ($row['expense_payment_due'] <= date('Y-m-d')) { ?>
                                                    <span class="btn btn-danger btn-sm" style="cursor:auto">
                                                        Overdue
                                                    </span>
                                                <?php } ?>
                                            </p>
                                        </div>
                                        <div class="unpaid_overdue_label col-md-1">
                                            <?php if (!$is_archived) { ?>
                                                <p>
                                                    <button class="btn btn-success btn-sm mark-paid-btn"
                                                            mark-paid-expense-id="<?= $row['expense_id'] ?>"
                                                            mark-paid-property-id="<?= $row['property_id'] ?>">
                                                        Mark Paid
                                                    </button>
                                                </p>
                                            <?php } ?>
                                        </div>


                                    </div>

                                    <div class="row-fluid row-inner ss_expanse_top_h_edit" style="display: none;">
                                        <div class="col-md-2">
                                            <h4 class="table-name">Due Date</h4>
                                            <p class="unpaid_expense_date">
                                                <input class="form-control expense_payment_due_chk" type="text" readonly
                                                       name="expense_payment_due"
                                                       value="<?= date("M d Y", strtotime($row['expense_payment_due'])); ?>">
                                            </p>
                                        </div>
                                        <div class="col-md-2">
                                            <h4 class="table-name">Creditor</h4>
                                            <p>
                                                <input class="form-control" type="text" name="expense_bill_from"
                                                       value="<?= $row['expense_bill_from']; ?>">
                                            </p>
                                        </div>
                                        <div class="col-md-2">
                                            <h4 class="table-name">Amount Due</h4>
                                            <p class="unpaid_amountdue expense_amountdue">
                                                <input class="form-control" type="text" name="total_invoice_amount"
                                                       value="<?= $row['total_invoice_amount']; ?>">
                                            </p>
                                        </div>
                                        <div class="col-md-2">
                                            <p class="unpaid_duetime"><?= $row['expense_payment_method']; ?></p>
                                        </div>

                                        <div class="col-md-2">
                                            <div class="row f_row">
                                                <div class="col-md-12">
                                                    <h4 class="table-name">Tax Code</h4>
                                                    <p class="unpaid_taxcode">
                                                        <select name="tax_code" id="tax_code_validate"
                                                                class="form-control tax_code_validate_f">
                                                            <option <?php if ($row['tax_code'] == "council rates") {
                                                                echo "selected";
                                                            } ?> value="council rates">Council Rates
                                                            </option>
                                                            <option <?php if ($row['tax_code'] == "insurance") {
                                                                echo "selected";
                                                            } ?> value="insurance">Insurance
                                                            </option>
                                                            <option <?php if ($row['tax_code'] == "land tax") {
                                                                echo "selected";
                                                            } ?> value="land tax">Land Tax
                                                            </option>
                                                            <option <?php if ($row['tax_code'] == "general repairs") {
                                                                echo "selected";
                                                            } ?> value="general repairs">General Repairs
                                                            </option>
                                                            <option <?php if ($row['tax_code'] == "electricity") {
                                                                echo "selected";
                                                            } ?> value="electricity">Electricity
                                                            </option>
                                                            <option <?php if ($row['tax_code'] == "plumbing") {
                                                                echo "selected";
                                                            } ?> value="plumbing">Plumbing
                                                            </option>
                                                            <option <?php if ($row['tax_code'] == "strata") {
                                                                echo "selected";
                                                            } ?> value="strata">Strata
                                                            </option>
                                                            <option <?php if ($row['tax_code'] == "water") {
                                                                echo "selected";
                                                            } ?> value="water">Water
                                                            </option>
                                                            <option <?php if ($row['tax_code'] == "other") {
                                                                echo "selected";
                                                            } ?> value="other">Other
                                                            </option>
                                                        </select>
                                                    </p>
                                                </div>
                                                <div class="col-md-12 maintenance_f_wrapper <?= in_array($row['tax_code'], $maintenance_related_tax_codes) ? ' maintenance_f_wrapper_show ' : '' ?> ">
                                                    <h4 class="table-name">Maintenance</h4>
                                                    <?php if ($a_mntnc) { ?>
                                                        <p class="maintenance_p">
                                                            <a href="Editmaintenance/<?= $property_id ?>/<?= $a_mntnc['maintenance_id'] ?>">
                                                                <?= $a_mntnc['maintenance_issue'] ?>
                                                                | <?= $a_mntnc['maintenance_location'] ?>
                                                                <?= $a_mntnc['create_at'] ? $a_mntnc['create_at'] : ''; ?>
                                                            </a>
                                                        </p>
                                                    <?php } ?>
                                                    <p>
                                                        <select name="maintenance_id"
                                                                class="form-control maintenance_f">
                                                            <option value="" <?= $row['maintenance_id'] == 0 ? ' selected ' : ''; ?>>
                                                                Please select a maintenance
                                                            </option>
                                                            <?php if ($maintenance_list) { ?>
                                                                <?php foreach ($maintenance_list as $a_maintenance) { ?>
                                                                    <option value="<?= $a_maintenance['maintenance_id'] ?>" <?= $row['maintenance_id'] == $a_maintenance['maintenance_id'] ? ' selected ' : ''; ?>>
                                                                        <?= $a_maintenance['maintenance_issue'] ?>
                                                                        | <?= $a_maintenance['maintenance_location'] ?>
                                                                        | <?= $a_maintenance['create_at'] ? $a_maintenance['create_at'] : ''; ?>
                                                                    </option>
                                                                <?php } ?>
                                                            <?php } ?>
                                                        </select>
                                                    </p>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-1"></div>
                                        <div class="col-md-1"></div>
                                    </div>

                                    <div class="row ss_wxpance_ifo" style="display:none;">
                                        <ul class="payment-info">
                                            <div class="row-fluid">
                                                <div class="col-md-8">
                                                    <div class="row-fluid">
                                                        <div class="col-md-3">
                                                            <p class="bottom-text">Payments:</p>
                                                        </div>
                                                        <div class="col-md-9">
                                                            <p class="paid_payments">
                                                                <?php foreach ($all_expense_in_receive_amount as $amnt_row) { ?>
                                                                    <?php if ($amnt_row['expense_id'] == $row['expense_id']) { ?>
                                                                        <span class="paid_payment">
                                        $<?= $amnt_row['total_invoice_recieving_amount'] ?>
                                                                            on <?= date("M d Y", strtotime($amnt_row['expense_paid_date_new'])); ?>
                                                                            paid via <?= $amnt_row['expense_payment_method_new'] ?>
                                      </span>
                                                                    <?php } ?>
                                                                <?php } ?>
                                                            </p>
                                                        </div>
                                                    </div>
                                                    <div class="row-fluid">
                                                        <div class="col-md-3">
                                                            <p class="bottom-text">Attachments:</p>
                                                        </div>
                                                        <div class="col-md-9">
                                                            <p class="attachments_links">
                                                                <?php foreach ($expense_doc as $doc_row) { ?>
                                                                    <?php if ($doc_row['expense_id'] == $row['expense_id']) { ?>
                                                                        <a class="paid_attachments" download
                                                                           href="uploads/expense_document/<?= $doc_row['expense_in_document'] ?>">
                                                                            <span class="paid_attachment"><?= $doc_row['expense_in_document'] ?></span>
                                                                        </a>
                                                                    <?php } ?>
                                                                <?php } ?>
                                                            </p>
                                                        </div>
                                                    </div>
                                                    <div class="row-fluid">
                                                        <div class="col-md-3">
                                                            <p class="bottom-text">Comments:</p>
                                                        </div>
                                                        <div class="col-md-9">
                                                            <p class="paid_comments"><?= $row['description'] ?></p>
                                                            <p class="paid_comments_chng" style="display: none;">
                                                                <textarea cols="60" rows="5"
                                                                          name="description"><?= $row['description']; ?></textarea>
                                                            </p>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-1"></div>
                                                <div class="col-md-3">
                                                    <div class="bottom-link submit_btn">
                                                        <?php if (!$is_archived) { ?>
                                                            <p><a style="cursor:pointer;" id="paid_expense_edit"
                                                                  class="paid_expense_edit">Edit expense</a></p>
                                                            <p><a href="javascript:void(0);"
                                                                  class="add_attachment OpenImgUpload">Add
                                                                    attachment</a>
                                                            </p>
                                                            <input type="hidden" class="expense_in_close_id"
                                                                   name="expense_id" value="<?= $row['expense_id'] ?>">
                                                            <p><a class="delete_expense"
                                                                  href="expense/delete_expense_in/<?= $row['expense_id'] ?>/<?= $property_id; ?>">Delete
                                                                    this expense</a></p>
                                                        <?php } ?>
                                                    </div>
                                                    <div class="span3 text-right submit_btn_chng"
                                                         style="display: none;">
                                                        <input type="hidden" name="property_id"
                                                               value="<?= $property_id; ?>">
                                                        <p><input type="submit"
                                                                  class="btn btn-primary paid_expense_edit_save"
                                                                  value="Save changes"></p>
                                                        <p><a class="btn btn-light paid_expense_edit_cancel">Cancel
                                                                changes</a></p>
                                                    </div>
                                                </div>
                                            </div>
                                        </ul>
                                    </div>
                                </div>
                            <?php } ?>
                        </form>
                    <?php } ?>

                <?php } ?>

                <?php if ($not_expense > 0) { ?>
                    <h4 style="font-weight: bold;margin-top: 40px">Upcoming (or Unpaid) Expenses</h4>
                    <div class="expense_title_bar">
                        <div class="row-fluid top-table-name">
                            <div class="col-md-2">
                                <p>Due Date</p>
                            </div>
                            <div class="col-md-2">
                                <p>Creditor</p>
                            </div>
                            <div class="col-md-2">
                                <p>Amount Due</p>
                            </div>
                            <div class="col-md-2">
                            </div>
                            <div class="col-md-2">
                                <p>Tax Code</p>
                            </div>
                            <div class="col-md-1">
                            </div>
                            <div class="col-md-1">
                            </div>
                        </div>
                    </div>

                    <?php foreach ($all_expense as $row) { ?>
                        <form action="expense/update_expense_info" class="f_form" method="post">
                            <?php
                            $a_mntnc = $this->expense_model->getAMaintenance($row['maintenance_id']);
                            ?>
                            <?php if ($row['expense_paying_status'] == 3) { ?>
                                <div class="ss_expensesinvoices_c_view"
                                    <?php if ($row['expense_payment_due'] <= date('Y-m-d')) { ?> id="overdue-<?= $row['expense_id'] ?>" <?php } ?>
                                >
                                    <div class="row-fluid row-inner ss_expanse_top_h">
                                        <div class="col-md-2">
                                            <h4 class="table-name">Due Date</h4>
                                            <p class="unpaid_expense_date">
                                                <?= date("M d, Y", strtotime($row['expense_payment_due'])); ?>
                                            </p>
                                        </div>
                                        <div class="col-md-2">
                                            <h4 class="table-name">Creditor</h4>
                                            <p class="unpaid_paying_who"><?= $row['expense_bill_from']; ?></p>
                                        </div>
                                        <div class="col-md-2">
                                            <h4 class="table-name">Amount Due</h4>
                                            <p class="unpaid_amountdue expense_amountdue">
                                                $<?= $row['total_invoice_amount']; ?></p>
                                        </div>
                                        <div class="col-md-2">
                                            <p class="unpaid_duetime">
                                                <?php if ($row['expense_payment_due'] <= date('Y-m-d')) { ?>
                                                    <?php echo (round((abs(strtotime(date('Y-m-d', strtotime($row['expense_payment_due']))) - strtotime(date('Y-m-d'))) / 86400))) . ' days ago'; ?>
                                                <?php } else { ?>
                                                    <?php echo 'in ' . (round((abs(strtotime(date('Y-m-d', strtotime($row['expense_payment_due']))) - strtotime(date('Y-m-d'))) / 86400))) . ' days'; ?>
                                                <?php } ?>
                                            </p>
                                        </div>

                                        <div class="col-md-2">
                                            <h4 class="table-name">Tax Code</h4>
                                            <p class="unpaid_taxcode"><?= $row['tax_code']; ?></p>
                                            <?php if ($a_mntnc) { ?>
                                                <p class="maintenance_p">
                                                    <a href="Editmaintenance/<?= $property_id ?>/<?= $a_mntnc['maintenance_id'] ?>">
                                                        <?= $a_mntnc['maintenance_issue'] ?>
                                                        | <?= $a_mntnc['maintenance_location'] ?>
                                                        <?= $a_mntnc['create_at'] ? $a_mntnc['create_at'] : ''; ?>
                                                    </a>
                                                </p>
                                            <?php } ?>
                                        </div>
                                        <div class="unpaid_overdue_label col-md-1">
                                            <p>
                                                <?php if ($row['expense_payment_due'] <= date('Y-m-d')) { ?>
                                                    <span class="btn btn-danger btn-sm" style="cursor:auto">
                                                        Overdue
                                                    </span>
                                                <?php } ?>
                                            </p>
                                        </div>
                                        <div class="unpaid_overdue_label col-md-1">
                                            <?php if (!$is_archived) { ?>
                                                <p>
                                                    <button class="btn btn-success btn-sm mark-paid-btn"
                                                            mark-paid-expense-id="<?= $row['expense_id'] ?>"
                                                            mark-paid-property-id="<?= $row['property_id'] ?>">
                                                        Mark Paid
                                                    </button>
                                                </p>
                                            <?php } ?>
                                        </div>

                                    </div>

                                    <div class="row-fluid row-inner ss_expanse_top_h_edit" style="display: none;">
                                        <div class="col-md-2">
                                            <h4 class="table-name">Due Date</h4>
                                            <p class="unpaid_expense_date">
                                                <input class="form-control expense_payment_due_chk" type="text" readonly
                                                       name="expense_payment_due"
                                                       value="<?= date("M d Y", strtotime($row['expense_payment_due'])); ?>">
                                            </p>
                                        </div>
                                        <div class="col-md-2">
                                            <h4 class="table-name">Creditor</h4>
                                            <p>
                                                <input class="form-control" type="text" name="expense_bill_from"
                                                       value="<?= $row['expense_bill_from']; ?>">
                                            </p>
                                        </div>
                                        <div class="col-md-2">
                                            <h4 class="table-name">Amount Due</h4>
                                            <p class="unpaid_amountdue expense_amountdue">
                                                <input class="form-control" type="text" name="total_invoice_amount"
                                                       value="<?= $row['total_invoice_amount']; ?>">
                                            </p>
                                        </div>
                                        <div class="col-md-2">
                                            <p class="unpaid_duetime"><?= $row['expense_payment_method']; ?></p>
                                        </div>
                                        <div class="col-md-2">
                                            <div class="row f_row">
                                                <div class="col-md-12">
                                                    <h4 class="table-name">Tax Code</h4>
                                                    <p class="unpaid_taxcode">
                                                        <select name="tax_code" id="tax_code_validate"
                                                                class="form-control tax_code_validate_f">
                                                            <option <?php if ($row['tax_code'] == "council rates") {
                                                                echo "selected";
                                                            } ?> value="council rates">Council Rates
                                                            </option>
                                                            <option <?php if ($row['tax_code'] == "insurance") {
                                                                echo "selected";
                                                            } ?> value="insurance">Insurance
                                                            </option>
                                                            <option <?php if ($row['tax_code'] == "land tax") {
                                                                echo "selected";
                                                            } ?> value="land tax">Land Tax
                                                            </option>
                                                            <option <?php if ($row['tax_code'] == "general repairs") {
                                                                echo "selected";
                                                            } ?> value="general repairs">General Repairs
                                                            </option>
                                                            <option <?php if ($row['tax_code'] == "electricity") {
                                                                echo "selected";
                                                            } ?> value="electricity">Electricity
                                                            </option>
                                                            <option <?php if ($row['tax_code'] == "plumbing") {
                                                                echo "selected";
                                                            } ?> value="plumbing">Plumbing
                                                            </option>
                                                            <option <?php if ($row['tax_code'] == "strata") {
                                                                echo "selected";
                                                            } ?> value="strata">Strata
                                                            </option>
                                                            <option <?php if ($row['tax_code'] == "water") {
                                                                echo "selected";
                                                            } ?> value="water">Water
                                                            </option>
                                                            <option <?php if ($row['tax_code'] == "other") {
                                                                echo "selected";
                                                            } ?> value="other">Other
                                                            </option>
                                                        </select>
                                                    </p>
                                                </div>
                                                <div class="col-md-12 maintenance_f_wrapper <?= in_array($row['tax_code'], $maintenance_related_tax_codes) ? ' maintenance_f_wrapper_show ' : '' ?> ">
                                                    <h4 class="table-name">Maintenance</h4>
                                                    <?php if ($a_mntnc) { ?>
                                                        <p class="maintenance_p">
                                                            <a href="Editmaintenance/<?= $property_id ?>/<?= $a_mntnc['maintenance_id'] ?>">
                                                                <?= $a_mntnc['maintenance_issue'] ?>
                                                                | <?= $a_mntnc['maintenance_location'] ?><br>
                                                                <?= $a_mntnc['create_at'] ? $a_mntnc['create_at'] : ''; ?>
                                                            </a>
                                                        </p>
                                                    <?php } ?>
                                                    <p>
                                                        <select name="maintenance_id"
                                                                class="form-control maintenance_f">
                                                            <option value="" <?= $row['maintenance_id'] == 0 ? ' selected ' : ''; ?>>
                                                                Please select a maintenance
                                                            </option>
                                                            <?php if ($maintenance_list) { ?>
                                                                <?php foreach ($maintenance_list as $a_maintenance) { ?>
                                                                    <option value="<?= $a_maintenance['maintenance_id'] ?>" <?= $row['maintenance_id'] == $a_maintenance['maintenance_id'] ? ' selected ' : ''; ?>>
                                                                        <?= $a_maintenance['maintenance_issue'] ?>
                                                                        | <?= $a_maintenance['maintenance_location'] ?>
                                                                        | <?= $a_maintenance['create_at'] ? $a_maintenance['create_at'] : ''; ?>
                                                                    </option>
                                                                <?php } ?>
                                                            <?php } ?>
                                                        </select>
                                                    </p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-1"></div>
                                    <div class="col-md-1"></div>

                                    <div class="row ss_wxpance_ifo" style="display:none;">
                                        <ul class="payment-info">
                                            <div class="row-fluid">
                                                <div class="col-md-8">
                                                    <div class="row-fluid">
                                                        <div class="col-md-3">
                                                            <p class="bottom-text">Payments:</p>
                                                        </div>
                                                        <div class="col-md-9">
                                                            <p class="paid_payments">
                                                                -
                                                            </p>
                                                        </div>
                                                    </div>
                                                    <div class="row-fluid">
                                                        <div class="col-md-3">
                                                            <p class="bottom-text">Attachments:</p>
                                                        </div>
                                                        <div class="col-md-9">
                                                            <p class="attachments_links">
                                                                <?php foreach ($expense_doc as $doc_row) { ?>
                                                                    <?php if ($doc_row['expense_id'] == $row['expense_id']) { ?>
                                                                        <a class="paid_attachments" download
                                                                           href="<?= base_url() ?>uploads/expense_document/<?= $doc_row['expense_in_document'] ?>">
                                                                            <span class="paid_attachment"><?= $doc_row['expense_in_document'] ?></span>
                                                                        </a>
                                                                    <?php } ?>
                                                                <?php } ?>
                                                            </p>
                                                        </div>
                                                    </div>
                                                    <div class="row-fluid">
                                                        <div class="col-md-3">
                                                            <p class="bottom-text">Comments:</p>
                                                        </div>
                                                        <div class="col-md-9">
                                                            <p class="paid_comments">
                                                                <?php if ($row['description'] == '') {
                                                                    echo "-";
                                                                } else {
                                                                    echo $row['description'];
                                                                } ?>
                                                            </p>
                                                            <p class="paid_comments_chng" style="display: none;">
                                                                <textarea cols="60" rows="5"
                                                                          name="description"><?= $row['description']; ?></textarea>
                                                            </p>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-1"></div>
                                                <div class="col-md-3">
                                                    <div class="bottom-link submit_btn">
                                                        <?php if (!$is_archived) { ?>
                                                            <p><a style="cursor:pointer;" id="paid_expense_edit"
                                                                  class="paid_expense_edit">Edit expense</a></p>
                                                            <p><a href="javascript:void(0);"
                                                                  class="add_attachment OpenImgUpload">Add
                                                                    attachment</a>
                                                            </p>
                                                            <input type="hidden" class="expense_in_close_id"
                                                                   name="expense_id" value="<?= $row['expense_id'] ?>">
                                                            <p><a class="delete_expense"
                                                                  href="expense/delete_expense_in/<?= $row['expense_id'] ?>/<?= $property_id; ?>">Delete
                                                                    this expense</a></p>
                                                        <?php } ?>
                                                    </div>
                                                    <div class="span3 text-right submit_btn_chng"
                                                         style="display: none;">
                                                        <input type="hidden" name="property_id"
                                                               value="<?= $property_id; ?>">
                                                        <p><input type="submit"
                                                                  class="btn btn-primary paid_expense_edit_save"
                                                                  value="Save changes"></p>
                                                        <p><a class="btn btn-light paid_expense_edit_cancel">Cancel
                                                                changes</a></p>
                                                    </div>
                                                </div>
                                            </div>
                                        </ul>
                                    </div>
                                </div>
                            <?php } ?>
                        </form>
                    <?php } ?>
                <?php } ?>
            </div>
            <div class="clear extra_padding">
                <form id="form_submit_trigger" action="expense/upload_expense_in_image/<?= $property_id; ?>"
                      method="post" enctype="multipart/form-data">
                    <input type="file" id="imgupload" name="expense_in_document" style="display:none;">
                    <input type="text" name="expense_in_id" id="expense_in_id_paste" style="display:none;">
                </form>
                <a class="btn btn-light pull-right" href="Dashboard/<?= $property_id ?>">Back To Dashboard</a>
            </div>
        </div>
    </div>
    <div class="modal fade" id="addnewpaiddueexpense" <?php if ($url == 'addExpense') {
        echo 'data-backdrop="static" data-keyboard="false"';
    } ?> tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">Add expense invoice</h4>
                </div>
                <div class="modal-body">
                    <div class="stepwizard">
                        <div class="stepwizard-row">
                            <div class="stepwizard-step">
                                <a class="btn btn-default btn-circle active-step" href="#step-1" data-toggle="tab"
                                   onclick="stepnext(1)">1</a>
                            </div>
                            <div class="stepwizard-step">
                                <a class="btn btn-default btn-circle" disabled="disabled" href="#step-2"
                                   data-toggle="tab">2</a>
                            </div>
                        </div>
                        <div class="rate-updates">
                            <div class="tab-content margintop0" style="border:none !important;">
                                <form onsubmit="return expense_input_check()" action="expense/insert_expense"
                                      method="post">
                                    <div class="tab-pane fade active in padding20" id="step_1">
                                        <div class="step001">
                                            <div class="form-group">
                                                <div class="col-md-5"><label>Tradesman *<br/>
                                                        <small>(person or business you are paying)</small>
                                                    </label>
                                                </div>
                                                <div class="col-md-7">
                                                    <input type="text" name="expense_bill_from" id="expense_bill_from"
                                                           class="form-control" placeholder="e.g. SA Water">
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <div class="col-md-5"><label>Invoice amount*</label></div>
                                                <div class="col-md-7">
                                                    <input type="number" name="total_invoice_amount"
                                                           id="total_invoice_amount" class="form-control"
                                                           placeholder="$0.00">
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <div class="col-md-5"><label>Bill status *</label></div>
                                                <div class="col-md-7">
                                                    <label class="radio-inline">
                                                        <input type="radio" name="expense_paying_status"
                                                               id="inlineRadio1" value="1"> Paid in full
                                                    </label>
                                                    <label class="radio-inline">
                                                        <input type="radio" name="expense_paying_status"
                                                               id="inlineRadio2" value="2"> Part paid
                                                    </label>
                                                    <label class="radio-inline">
                                                        <input type="radio" name="expense_paying_status"
                                                               id="inlineRadio3" value="3"> Not paid
                                                    </label>
                                                </div>
                                            </div>
                                            <div id="bill_detail_display" style="display: none;">
                                                <div class="form-group">
                                                    <div class="col-md-5" style="display:none;" id="bill_is_due"><label>This
                                                            bill was due on *</label></div>
                                                    <div class="col-md-5" style="display:none;" id="bill_was_due">
                                                        <label>This bill is due on *</label></div>
                                                    <div class="col-md-7">
                                                        <input type="text" id="expense_payment_due" readonly
                                                               name="expense_payment_due" class=" form-control"
                                                               placeholder="Nov 09, 2013">
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <div class="col-md-5" id="full_pay_detail"><label>Date paid and
                                                            payment method? *<br/></label></div>
                                                    <div class="col-md-5" id="half_pay_detail"><label>Paid details
                                                            *<br/>
                                                            <small>(amount paid, date, how)</small>
                                                        </label></div>
                                                    <div class="col-md-7">
                                                        <div class="row">
                                                            <div style="overflow: hidden">
                                                                <div class="col-md-12" id="amount_paid"><input
                                                                            type="number"
                                                                            name="expense_partial_amount_paid"
                                                                            id="expense_partial_amount_paid"
                                                                            class=" form-control"
                                                                            placeholder="$0.00">
                                                                </div>
                                                            </div>

                                                            <div class="" style="margin-top: 10px">
                                                                <div class="col-md-6" id="payment_date"><input readonly
                                                                            type="text"
                                                                            name="expense_paid_date"
                                                                            id="expense_paid_date"
                                                                            class="form-control"
                                                                            placeholder="Nov 09, 2013">
                                                                </div>
                                                                <div class="col-md-6" id="payment_method"><input
                                                                            type="text"
                                                                            name="expense_payment_method"
                                                                            id="expense_payment_method"
                                                                            class="col-md-4  form-control"
                                                                            placeholder="e.g. Cash">
                                                                </div>
                                                            </div>

                                                        </div>
                                                    </div>
                                                </div>
                                                <button class="btn btn-xs btn-primary" id="final_step" type="button"><i
                                                            class="icon-next"></i>Enter
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="tab-pane fade padding20" style="display:none;" id="step_2">
                                        <div class="step002">
                                            <h5><strong>Add an invoice for the expense created</strong><br/>
                                                We recommend doing this now, but you can skip this and come back to it
                                                later</h5>
                                            <br/><br/>
                                            <div class="form-group">
                                                <div class="col-md-5"><label>Upload a scanned copy
                                                        <small> (or photo)</small>
                                                        of the invoice or receipt</label></div>
                                                <div class="col-md-7">
                                                    <div class="form-group inputDnD">
                                                        <div class="dropzone">
                                                            <span style="display: none"
                                                                  class="my-dz-message pull-right">
                                                                <h3>Click Here to upload another file</h3>
                                                            </span>
                                                            <div class="dz-message">
                                                                <h3>Click Here to upload your files</h3>
                                                            </div>
                                                        </div>
                                                        <div class="previews" id="preview"></div>
                                                    </div>
                                                    <div>* Max 20 MB per file</div>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <div class="col-md-5"><label>Invoice type</label></div>
                                                <div class="col-md-7">
                                                    <select name="tax_code" id="tax_code_validate"
                                                            class="form-control tax_code_validate">
                                                        <option value="">Please select a tax category</option>
                                                        <option value="council rates">Council rates</option>
                                                        <option value="insurance">Insurance</option>
                                                        <option value="land tax">Land tax</option>
                                                        <option value="general repairs">General repairs</option>
                                                        <option value="electricity">Electricity</option>
                                                        <option value="plumbing">Plumbing</option>
                                                        <option value="strata">Strata</option>
                                                        <option value="water">Water</option>
                                                        <option value="Other">Other</option>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="form-group" id="maintenance_id_wrapper">
                                                <div class="col-md-5"><label>Select Maintenance</label></div>
                                                <div class="col-md-7">
                                                    <select name="maintenance_id" id="maintenance_id"
                                                            class="form-control">
                                                        <option value="">Please select a maintenance</option>
                                                        <?php if ($maintenance_list) { ?>
                                                            <?php foreach ($maintenance_list as $a_maintenance) { ?>
                                                                <option value="<?= $a_maintenance['maintenance_id'] ?>"><?= $a_maintenance['maintenance_issue'] ?>
                                                                    | <?= $a_maintenance['maintenance_location'] ?>
                                                                    | <?= $a_maintenance['create_at'] ? $a_maintenance['create_at'] : ''; ?>
                                                                </option>
                                                            <?php } ?>
                                                        <?php } ?>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <div class="col-md-5"><label>Additional notes</label></div>
                                                <div class="col-md-7">
                                                    <textarea name="description" class="form-control"></textarea>
                                                </div>
                                            </div>
                                        </div>
                                        <input type="hidden" name="property_id" value="<?= $property_id; ?>">
                                        <input class="btn btn-xs btn-primary" value="Finish" type="submit">
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div id="taxreport" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">Let's create a Tax Report</h4>
                </div>
                <div class="modal-body">
                    <p>P.S Have you entered all your income + expenses ?</p>
                    <ul>
                        <li>interest payments</li>
                        <li> rental income</li>
                        <li>advertising expenses</li>
                    </ul>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-light" data-dismiss="modal">Close</button>
                    <a href="yourcashflowreports.html" type="button" class="btn btn-primary">Yes? ... Let's Generate
                        Your Report</a>
                </div>
            </div>
        </div>
    </div>

    <div id="mark-paid-modal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">Mark expense as paid</h4>
                </div>
                <div class="modal-body">
                    <form class="form-horizontal" method="post" action="expense/mark_expense_as_paid"
                          enctype="multipart/form-data">
                        <input type="hidden" id="expense_id_in_mark_expense" name="expense_id" value="">
                        <input type="hidden" id="property_id_in_mark_expense" name="property_id" value="">
                        <div class="form-group">
                            <label class="control-label col-sm-2" for="pwd">Expense Paid Date</label>
                            <div class="col-sm-10">
                                <input readonly type="text" class="form-control" id="expense_paid_date_in_mark_expense"
                                       placeholder="" name="expense_paid_date">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-sm-2" for="pwd">Payment Method</label>
                            <div class="col-sm-10">
                                <input type="text" class="form-control" id="expense_payment_method_in_mark_expense"
                                       placeholder="" name="expense_payment_method">
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="control-label col-sm-2" for="pwd">Expense Reciept File</label>
                            <div class="col-sm-10">
                                <input type="file" class="form-control" id="expense_receipt_file_in_mark_expense"
                                       name="expense_receipt_file">
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-sm-offset-2 col-sm-10">
                                <button type="submit" class="btn btn-default">Submit</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

<?php if ($url == 'addExpense') { ?>
    <script type="text/javascript">
        $(window).on('load', function () {
            $('#addnewpaiddueexpense').modal('show');
        });
    </script>
<?php } ?>
<?php $this->load->view('front/footerlink'); ?>

<script type="text/javascript">
    $('.OpenImgUpload').click(function () {
        var expense_in_id = $(this).closest('.ss_expensesinvoices_c_view').find('.expense_in_close_id').val();
        $("#expense_in_id_paste").val(expense_in_id);
        $('#imgupload').trigger('click');
    });
    $('#imgupload').on('change', function () {
        $("#form_submit_trigger").trigger("submit");
    })
</script>

<script type="text/javascript">
    $(function () {
        $('#expense_payment_due, #expense_paid_date, .expense_payment_due_chk').datepicker({
            dateFormat: 'd M yy'
        });
    });
</script>

<script type="text/javascript">
    function expense_input_check() {
        var flag = true;
        var tax_code_validate = $('.tax_code_validate').val();
        var maintenance_id = $('#maintenance_id').val();

        if (tax_code_validate == '' || tax_code_validate == null) {
            $('#tax_code_validate').closest('div').addClass("has-error");
            flag = false;
        } else {
            $('#tax_code_validate').closest('div').removeClass("has-error");
        }

        if (tax_code_validate == 'maintenance' && maintenance_id == '') {
            $('#maintenance_id').closest('div').addClass("has-error");
            flag = false;
        } else {
            $('#maintenance_id').closest('div').removeClass("has-error");
        }

        return flag;
    }

    $('.f_form').on('submit', function (e) {

        var flag = true;
        var tax_code_validate_f = $(this).find('.tax_code_validate_f');
        var maintenance_f = $(this).find('.maintenance_f');

        $(this).css('color', 'blue');
        console.log($(this));

        if (tax_code_validate_f.val() == '' || tax_code_validate_f.val() == null) {
            tax_code_validate_f.closest('div').addClass("has-error");
            flag = false;
        } else {
            tax_code_validate_f.closest('div').removeClass("has-error");
        }

        if (tax_code_validate_f.val() == 'maintenance' && maintenance_f.val() == '') {
            maintenance_f.closest('div').addClass("has-error");
            flag = false;
        } else {
            maintenance_f.closest('div').removeClass("has-error");
        }

        return flag;
    })
</script>

<script type="text/javascript">
    $('#inlineRadio1').click(function () {
        $('#bill_was_due').hide();
        $('#bill_is_due').show();
        $('#bill_detail_display').show();
        $('#amount_paid').hide();
        $('#payment_date').show();
        $('#payment_method').show();
        $('#full_pay_detail').show();
        $('#half_pay_detail').hide();
    });
    $('#inlineRadio2').click(function () {
        $('#bill_is_due').hide();
        $('#bill_was_due').show();
        $('#bill_detail_display').show();
        $('#amount_paid').show();
        $('#payment_date').show();
        $('#payment_method').show();
        $('#full_pay_detail').hide();
        $('#half_pay_detail').show();
    });
    $('#inlineRadio3').click(function () {
        $('#bill_is_due').hide();
        $('#bill_was_due').show();
        $('#bill_detail_display').show();
        $('#amount_paid').hide();
        $('#payment_date').hide();
        $('#payment_method').hide();
        $('#full_pay_detail').hide();
        $('#half_pay_detail').hide();
    });
</script>

<script>
    $(function () {
        var maintenance_related_tax_codes = <?= json_encode($maintenance_related_tax_codes)?>;
        $(".tax_code_validate").on('change', function () {


            console.log(maintenance_related_tax_codes);

            if (maintenance_related_tax_codes.indexOf($(this).val()) != -1) {
                $("#maintenance_id_wrapper").show();
            } else {
                $("#maintenance_id_wrapper").hide();
            }
        });


        $('.maintenance_f_wrapper_show').show();

        $(".tax_code_validate_f").on('change', function () {
            show_maintenance_selection($(this));
        });

        function show_maintenance_selection(tax_code_input) {
            console.log(maintenance_related_tax_codes);
            //if (tax_code_input.val() == 'maintenance') {
            if (maintenance_related_tax_codes.indexOf(tax_code_input.val()) != -1) {
                tax_code_input.closest('.f_row').find('.maintenance_f_wrapper').show();
            } else {
                tax_code_input.closest('.f_row').find('.maintenance_f_wrapper').hide();
            }
        }
    })
</script>

<script type="text/javascript">
    $('#final_step').click(function () {
        var expense_bill_from = $('#expense_bill_from').val();
        var total_invoice_amount = parseFloat($('#total_invoice_amount').val());
        (expense_bill_from == '') ? $('#expense_bill_from').closest('div').addClass("has-error") : $('#expense_bill_from').closest('div').removeClass("has-error");
        (total_invoice_amount == '') ? $('#total_invoice_amount').closest('div').addClass("has-error") : $('#total_invoice_amount').closest('div').removeClass("has-error");


        if ($("#inlineRadio1").is(":checked")) {
            var expense_payment_due = $('#expense_payment_due').val();
            var expense_paid_date = $('#expense_paid_date').val();
            var expense_payment_method = $('#expense_payment_method').val();

            (expense_payment_due == '') ? $('#expense_payment_due').closest('div').addClass("has-error") : $('#expense_payment_due').closest('div').removeClass("has-error");
            (expense_paid_date == '') ? $('#expense_paid_date').closest('div').addClass("has-error") : $('#expense_paid_date').closest('div').removeClass("has-error");
            (expense_payment_method == '') ? $('#expense_payment_method').closest('div').addClass("has-error") : $('#expense_payment_method').closest('div').removeClass("has-error");

            if (expense_partial_amount_paid == '' || expense_payment_due == '' || expense_paid_date == '' || expense_payment_method == '') {
                return false;
            }
        }

        if ($("#inlineRadio2").is(":checked")) {
            var expense_payment_due = $('#expense_payment_due').val();
            var expense_partial_amount_paid = parseFloat($('#expense_partial_amount_paid').val());
            var expense_paid_date = $('#expense_paid_date').val();
            var expense_payment_method = $('#expense_payment_method').val();

            if (expense_partial_amount_paid == total_invoice_amount || expense_partial_amount_paid > total_invoice_amount) {
                $('#expense_partial_amount_paid').closest('div').addClass("has-error");
                return false;
            }


            (expense_payment_due == '') ? $('#expense_payment_due').closest('div').addClass("has-error") : $('#expense_payment_due').closest('div').removeClass("has-error");
            (expense_partial_amount_paid == total_invoice_amount) ? $('#expense_partial_amount_paid').closest('div').addClass("has-error") : $('#expense_partial_amount_paid').closest('div').removeClass("has-error");
            (expense_partial_amount_paid == '') ? $('#expense_partial_amount_paid').closest('div').addClass("has-error") : $('#expense_partial_amount_paid').closest('div').removeClass("has-error");
            (expense_paid_date == '') ? $('#expense_paid_date').closest('div').addClass("has-error") : $('#expense_paid_date').closest('div').removeClass("has-error");
            (expense_payment_method == '') ? $('#expense_payment_method').closest('div').addClass("has-error") : $('#expense_payment_method').closest('div').removeClass("has-error");

            if (expense_partial_amount_paid == '' || expense_partial_amount_paid == '' || expense_payment_due == '' || expense_paid_date == '' || expense_payment_method == '') {
                return false;
            }
        }


        if ($("#inlineRadio3").is(":checked")) {
            var expense_payment_due = $('#expense_payment_due').val();
            (expense_payment_due == '') ? $('#expense_payment_due').closest('div').addClass("has-error") : $('#expense_payment_due').closest('div').removeClass("has-error");

            if (expense_payment_due == '') {
                return false;
            }
        }

        if (expense_bill_from == '' || total_invoice_amount == '') {
            return false;
        }


        $('#step_1').hide();
        $('#step_2').show();
        $('#step_2').removeClass('fade');
    });
</script>

<script type="text/javascript">
    Dropzone.autoDiscover = false;
    var foto_upload = new Dropzone(".dropzone", {
        url: "<?php echo base_url('expense/expense_document') ?>",
        maxFilesize: 20,
        method: "post",
        acceptedFiles: ".jpg,.jpeg,.png,.gif,.doc,docx,.pdf,.xls,.xlsx, application/msword, \n" +
        "    application/vnd.openxmlformats-officedocument.wordprocessingml.document,\n" +
        "    application/vnd.ms-excel,\n" +
        "    application/vnd.openxmlformats-officedocument.spreadsheetml.sheet,\n" +
        "    application/pdf'",
        paramName: "userfile",
        dictInvalidFileType: "This File Type Not Supported",
        addRemoveLinks: true,
        init: function () {
            var count = 0;
            thisDropzone = this;
            this.on("success", function (file, json) {
                var obj = json;
                $('.previews').append(
                    "<input type='hidden' name='expense_in_doc[]' value='" + obj + "'>\n\
            <input type='hidden' name='expense_in_width[]' value='" + file.width + "'>\n\
            <input type='hidden' name='expense_in_height[]' value='" + file.height + "'>"
                );
                $(".my-dz-message").show();
            });
        }
    });
</script>

<script>
    $(".ss_expanse_top_h").click(function () {
        $(this).closest('.ss_expensesinvoices_c_view').find('.ss_wxpance_ifo').toggle("slow");
    });
    $(".paid_expense_edit").click(function () {

        $(this).closest('.ss_expensesinvoices_c_view').find('.ss_expanse_top_h').hide();
        $(this).closest('.ss_expensesinvoices_c_view').find('.ss_expanse_top_h_edit').show();

        $(this).closest('.ss_expensesinvoices_c_view').find('.unpaid_paying_who').hide();
        $(this).closest('.ss_expensesinvoices_c_view').find('.unpaid_paying_who_chng').show();

        $(this).closest('.ss_expensesinvoices_c_view').find('.money_in_purpose_view').hide();
        $(this).closest('.ss_expensesinvoices_c_view').find('.money_in_purpose_view_chng').show();

        $(this).closest('.ss_expensesinvoices_c_view').find('.paid_comments').hide();
        $(this).closest('.ss_expensesinvoices_c_view').find('.paid_comments_chng').show();

        $(this).closest('.ss_expensesinvoices_c_view').find('.submit_btn').hide();
        $(this).closest('.ss_expensesinvoices_c_view').find('.submit_btn_chng').show();
    });


    $(".paid_expense_edit_cancel").click(function () {
        $(this).closest('.ss_expensesinvoices_c_view').find('.ss_expanse_top_h').show();
        $(this).closest('.ss_expensesinvoices_c_view').find('.ss_expanse_top_h_edit').hide();

        // $(this).closest('.ss_expensesinvoices_c_view').find('.money_in_receiving_date').show();
        // $(this).closest('.ss_expensesinvoices_c_view').find('.money_in_receiving_date_chng').hide();

        $(this).closest('.ss_expensesinvoices_c_view').find('.unpaid_paying_who').show();
        $(this).closest('.ss_expensesinvoices_c_view').find('.unpaid_paying_who_chng').hide();

        $(this).closest('.ss_expensesinvoices_c_view').find('.money_in_purpose_view').show();
        $(this).closest('.ss_expensesinvoices_c_view').find('.money_in_purpose_view_chng').hide();

        $(this).closest('.ss_expensesinvoices_c_view').find('.paid_comments').show();
        $(this).closest('.ss_expensesinvoices_c_view').find('.paid_comments_chng').hide();

        $(this).closest('.ss_expensesinvoices_c_view').find('.submit_btn').show();
        $(this).closest('.ss_expensesinvoices_c_view').find('.submit_btn_chng').hide();

    });
</script>


<script>
    document.addEventListener("DOMContentLoaded", function (event) {

        $("#expense_paid_date_in_mark_expense").datepicker({dateFormat: 'd M yy'});

        $('.mark-paid-btn').on("click", function (e) {
            e.preventDefault();
            $("#mark-paid-modal").modal("show");
            var expense_id = $(this).attr('mark-paid-expense-id');
            var property_id = $(this).attr('mark-paid-property-id');

            console.log(expense_id);
            console.log(property_id);

            $("#expense_id_in_mark_expense").val(expense_id);
            $("#property_id_in_mark_expense").val(property_id);

        });

        $("body").on("click", ".maintenance_p", function (e) {
            e.preventDefault();
            $a = $(this).find('a');
            window.open($a.attr("href"), '_blank');
        })
    });

</script>