<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Expense extends MX_Controller
{

    //public $counter=0;
    function __construct()
    {
        parent::__construct();
        $this->load->model('expense_model');
        $this->load->model('utility/utility_model');
        $this->load->model('Maintenance/Maintenance_model');
        //$this->load->model('home/home_model');
        // $this->load->helper('inflector');
        // $this->load->library('encrypt');

        $this->utility_model->check_auth();

        $this->load->library('email');

        ini_set('memory_limit', '128M');
        ini_set('max_execution_time', 900); //300 seconds = 5 minutes
    }

    public function index()
    {
        

        $this->load->view('view_expense');
    }

    public function view_expense($property_id)
    {
        

        $data['property_info'] = $this->expense_model->select_with_where('*', 'property_id=' . $property_id . '', 'property');
        $data['all_expense'] = $this->expense_model->expensesInAProperty($property_id);
        $data['expense_doc'] = $this->expense_model->expenseDocsInAProperty($property_id);
        $data['expense_receipt_file'] = $this->expense_model->expenseReceiptFilesAProperty($property_id);

        $data['maintenance_list'] = null;
        $last_active_lease = $this->utility_model->getLastActiveLeaseOfProperty($property_id);

        //-------------------------------------------------------------------------
        $lease_id = !empty($last_active_lease) ? $last_active_lease['lease_id'] : 0;
        $is_archived = $this->utility_model->isArchived();
        $archived_string = $this->utility_model->getArchivedString();
        if ($is_archived) {
            $lease_id = $this->utility_model->getArchivedLeaseId();
            $data['lease_detail'][0] = $this->utility_model->getLease($lease_id);
        }
        $data['is_archived'] = $is_archived;
        $data['archived_string'] = $archived_string;
        //-------------------------------------------------------------------------
        $data['maintenance_list'] = $this->expense_model->all_possible_maintenance_by_lease($property_id, $lease_id);

        $data['all_expense_in_receive_amount'] = $this->expense_model->select_left_join('*', 'expense_in_cash_receive_amount', 'expense', 'expense_in_cash_receive_amount.expense_id=expense.expense_id');
        $data['maintenance_related_tax_codes'] = $this->utility_model->get_maintenance_related_tax_codes_in_expense();

        /*echo "<pre>";
        print_r($data['maintenance_related_tax_codes']);
        echo "</pre>";
        exit;*/
        $data['url'] = $this->uri->segment('1');
        $data['property_id'] = $property_id;
        $this->load->view('view_expense', $data);
    }

    public function insert_expense()
    {
        //echo "gg";die();

        

        $data['property_id'] = $this->input->post('property_id');
        $data['expense_bill_from'] = $this->input->post('expense_bill_from');
        $data['total_invoice_amount'] = $this->input->post('total_invoice_amount');

        $data['expense_paying_status'] = $this->input->post('expense_paying_status');
        $expense_payment_due = $this->input->post('expense_payment_due');
        $data['expense_payment_due'] = date("Y-m-d", strtotime($expense_payment_due));

        $expense_paid_date = $this->input->post('expense_paid_date');
        $data['expense_paid_date'] = date("Y-m-d", strtotime($expense_paid_date));

        $data['expense_payment_method'] = $this->input->post('expense_payment_method');
        $data['expense_partial_amount_paid'] = $this->input->post('expense_partial_amount_paid');
        $data['tax_code'] = $this->input->post('tax_code');
        $data['description'] = $this->input->post('description');

        // echo "<pre>";
        // print_r($data);
        // die();

        $maintenance_related_tax_codes = $this->utility_model->get_maintenance_related_tax_codes_in_expense();
        if (in_array($data['tax_code'], $maintenance_related_tax_codes)) {
            //$this->form_validation->set_rules('maintenance_id', 'Maintenance', 'required');
            $data['maintenance_id'] = $this->input->post('maintenance_id');
        }

        $this->form_validation->set_rules('expense_bill_from', 'Bill Name', 'required');
        $this->form_validation->set_rules('total_invoice_amount', 'Bill Amount', 'required');

        if ($data['expense_paying_status'] == 1) {
            $this->form_validation->set_rules('expense_payment_due', 'Payment Due', 'required');
            $this->form_validation->set_rules('expense_paid_date', 'Payment Date', 'required');
            $this->form_validation->set_rules('expense_payment_method', 'Payment Method', 'required');
        }
        if ($data['expense_paying_status'] == 2) {
            $this->form_validation->set_rules('expense_payment_due', 'Payment Due', 'required');
            $this->form_validation->set_rules('expense_paid_date', 'Payment Date', 'required');
            $this->form_validation->set_rules('expense_payment_method', 'Payment Method', 'required');
            $this->form_validation->set_rules('expense_partial_amount_paid', 'Partial Amount', 'required');
        }
        if ($data['expense_paying_status'] == 3) {
            $this->form_validation->set_rules('expense_payment_due', 'Payment Due', 'required');
        }

        if ($this->form_validation->run() == FALSE) {
            redirect('viewExpense/' . $data['property_id']);
        } else {
            $get_expense_id = $this->expense_model->insert_ret('expense', $data);
        }

        $expns_recieve_data['expense_id'] = $get_expense_id;
        $expns_recieve_data['total_invoice_recieving_amount'] = $data['expense_partial_amount_paid'];
        $expns_recieve_data['expense_payment_method_new'] = $data['expense_payment_method'];
        $expns_recieve_data['expense_paid_date_new'] = $data['expense_paid_date'];

        $this->expense_model->insert('expense_in_cash_receive_amount', $expns_recieve_data);


        $expense_in_doc = $this->input->post('expense_in_doc');
        if ($expense_in_doc) {
            foreach ($expense_in_doc as $key => $value) {
                $doc_data['expense_id'] = $get_expense_id;
                $doc_data['expense_in_document'] = $value;
                $this->expense_model->insert('expense_in_document', $doc_data);
            }
        }

        $this->sendInvoiceCreateEmail($get_expense_id);

        redirect('viewExpense/' . $data['property_id']);
    }

    public function update_expense_info()
    {
        

        $property_id = $this->input->post('property_id');
        $expense_id = $this->input->post('expense_id');
        $expense_payment_due = $this->input->post('expense_payment_due');
        $data['expense_payment_due'] = date("Y-m-d", strtotime($expense_payment_due));
        $data['expense_bill_from'] = $this->input->post('expense_bill_from');
        $data['tax_code'] = $this->input->post('tax_code');
        $data['description'] = $this->input->post('description');
        $data['maintenance_id'] = $this->input->post('maintenance_id');

        if ($this->input->post('total_invoice_amount')) {
            $data['total_invoice_amount'] = $this->input->post('total_invoice_amount');
        }
        $this->expense_model->update_function('expense_id', $expense_id, 'expense', $data);
        redirect('viewExpense/' . $property_id);
    }

    public function upload_expense_in_image($property_id)
    {

        

        $doc_data['expense_id'] = $this->input->post('expense_in_id');

        $files = $_FILES;
        if (isset($_FILES['expense_in_document']) && !empty($_FILES['expense_in_document']['name'])) {

            $config['remove_spaces'] = TRUE;
            $config['encrypt_name'] = TRUE;
            $config['upload_path'] = 'uploads/expense_document';
            $config['allowed_types'] = 'jpg|png|gif|jpeg|pdf|doc|docx|xls';
            $config['max_size'] = '0';
            $this->load->library('upload', $config);
            $this->upload->initialize($config);
            if (!$this->upload->do_upload('expense_in_document')) {
                $status = 'error';
                $msg = $this->upload->display_errors('', '');

            } else {
                $fileinfo = $this->upload->data();


                $doc_data['expense_in_document'] = $fileinfo['file_name'];
                $this->expense_model->insert('expense_in_document', $doc_data);
            }
        }

        redirect('viewExpense/' . $property_id);
    }

    public function delete_expense_in($expense_id, $property_id)
    {

        

        $this->expense_model->delete_function_cond('expense', 'expense_id=' . $expense_id . '');
        $this->expense_model->delete_function_cond('expense_in_cash_receive_amount', 'expense_id=' . $expense_id . '');
        $this->expense_model->delete_function_cond('expense_in_document', 'expense_id=' . $expense_id . '');
        redirect('viewExpense/' . $property_id);
    }

    public function expense_document()
    {
        

        $files = $_FILES;
        if (isset($_FILES['userfile']) && !empty($_FILES['userfile']['name'])) {
            $config['remove_spaces'] = TRUE;
            $config['encrypt_name'] = TRUE;
            $config['upload_path'] = 'uploads/expense_document';
            $config['allowed_types'] = 'jpg|png|gif|jpeg|xls|doc|docx|pdf|xlsx|xls';
            $config['max_size'] = '0';
            $this->load->library('upload', $config);
            $this->upload->initialize($config);
            if (!$this->upload->do_upload('userfile')) {
                $status = 'error';
                $msg = $this->upload->display_errors('', '');
//                echo json_encode($msg);
            } else {
                $fileinfo = $this->upload->data();
                echo $fileinfo['file_name'];
            }
        }
    }

    public function add_expense()
    {
        

        $this->load->view('add_expense');
    }

    public function cashflow_report()
    {
        

        $this->load->view('create_cashflow_report');
    }

    private function joinNameParts($fname, $lname)
    {
        return $fname . ' ' . $lname;
    }

    public function sendInvoiceCreateEmail($expense_id)
    {
        $site_name = $this->config->item('site_name');
        $base = $this->config->base_url();
        $img_src = "{$base}assets/img/logo.png";
        $img = "<img src='$img_src'><br>";

        $expense = $this->expense_model->getExpense($expense_id);
        if (empty($expense)) {
            exit;
        }

        $dollar_sign = "$";
        $total_invoice_amount = number_format($expense['total_invoice_amount'], 2, '.', '');
        $expense_partial_amount_paid = number_format($expense['expense_partial_amount_paid'], 2, '.', '');
        $inv_num = '00' . $expense['property_id'] . $expense['expense_id'] . date('Ymd', strtotime($expense['created_at'])) . date('Ymd', strtotime($expense['expense_payment_due'])) . '00';
        $property = $this->expense_model->getProperty($expense['property_id']);
        $active_lease = $this->expense_model->getActiveLeaseOfAProperty($expense['property_id']);
        $property_with_landlord = $this->utility_model->getPropertyWithLandlord($expense['property_id']);
        $tenants_with_lease_detail = $this->utility_model->getTenantsWithLeaseDetails($active_lease['lease_id']);

        $full_property_address = $this->utility_model->getFullPropertyAddress($expense['property_id']);

        $tenant_names = "";
        if (!empty($tenants_with_lease_detail)) {
            $user_fname = array_column($tenants_with_lease_detail, 'user_fname');
            $user_lname = array_column($tenants_with_lease_detail, 'user_lname');
            $tenant_names_array = (array_map("self::joinNameParts", $user_fname, $user_lname));
        }


        if (count($tenant_names_array) > 0) {
            $tenant_names = implode(' & ', $tenant_names_array);
        }

        $landlord_name = "";
        $landlord_phone = "";
        if (!empty($property_with_landlord)) {
            $landlord_name = $property_with_landlord['user_fname'] . ' ' . $property_with_landlord['user_lname'];
            $landlord_phone = $property_with_landlord['phone'];
        }

        $due_date = date("jS F, Y", strtotime($expense['expense_payment_due']));
        $bill_status = "Not Paid";

        $due_date = date("jS F, Y", strtotime($expense['expense_payment_due']));
        $bill_status = "Not Paid";
        if ($expense['expense_paying_status'] == 1) {
            $bill_status = "Fully Paid";
        } else if ($expense['expense_paying_status'] == 2) {
            $bill_status = "Partially Paid";
        } else if ($expense['expense_paying_status'] == 3) {
            $bill_status = "Not Paid";
        }

        if (empty($property_with_landlord)) {
            exit;
        }

        $to = $property_with_landlord['email'];
        $mail_data['to'] = $to;
        $subject = "An expense has been created";

        $message = "Hi {$landlord_name} ,<br>
                        This email is to confirm you have entered in an expense against the following property:<br>
                         --------------------------------- <br>
                         <b>Address:</b> {$full_property_address} <br><br>
                         <b>Tenant(s):</b> $tenant_names<br> <br>                        
                         <b>Details of the invoice below:</b><br>
                         --------------------------------- <br>  
                         <b>Amount Received to Date:</b> {$expense_partial_amount_paid}<br> 
                         <b>Tradesman:</b> {$expense['expense_bill_from']}<br>   
                         <b>Invoice Amount:</b> {$dollar_sign}{$total_invoice_amount}<br> 
                         <b>Bill Status:</b> {$bill_status}<br>
                         <b>Invoice Number:</b> {$inv_num}<br> 
                         <b>Due Date:</b> {$due_date} <br>    
                         <b>Invoice Type:</b> {$expense['tax_code']}<br>   
                         <b>Additional Notes:</b> {$expense['description']}<br>
                         --------------------------------- <br><br>                         
                         This email has been generated by {$site_name} Software.<br><br>
                         {$img}
                         ";

        $mail_data['subject'] = $subject;
        $mail_data['message'] = $message;

        $this->sendEmail($mail_data);


    }

    public function sendInvoicePaidEmail($expense_id)
    {
        $site_name = $this->config->item('site_name');
        $base = $this->config->base_url();
        $img_src = "{$base}assets/img/logo.png";
        $img = "<img src='$img_src'><br>";

        $expense = $this->expense_model->getExpense($expense_id);
        if (empty($expense)) {
            exit;
        }

        $dollar_sign = "$";
        $total_invoice_amount = number_format($expense['total_invoice_amount'], 2, '.', '');
        $expense_partial_amount_paid = number_format($expense['expense_partial_amount_paid'], 2, '.', '');
        $inv_num = '00' . $expense['property_id'] . $expense['expense_id'] . date('Ymd', strtotime($expense['created_at'])) . date('Ymd', strtotime($expense['expense_payment_due'])) . '00';
        $property = $this->expense_model->getProperty($expense['property_id']);
        $active_lease = $this->expense_model->getActiveLeaseOfAProperty($expense['property_id']);
        $property_with_landlord = $this->utility_model->getPropertyWithLandlord($expense['property_id']);
        $tenants_with_lease_detail = $this->utility_model->getTenantsWithLeaseDetails($active_lease['lease_id']);

        $full_property_address = $this->utility_model->getFullPropertyAddress($expense['property_id']);

        $tenant_names = "";
        if (!empty($tenants_with_lease_detail)) {
            $user_fname = array_column($tenants_with_lease_detail, 'user_fname');
            $user_lname = array_column($tenants_with_lease_detail, 'user_lname');
            $tenant_names_array = (array_map("self::joinNameParts", $user_fname, $user_lname));
        }


        if (count($tenant_names_array) > 0) {
            $tenant_names = implode(' & ', $tenant_names_array);
        }

        $landlord_name = "";
        $landlord_phone = "";
        if (!empty($property_with_landlord)) {
            $landlord_name = $property_with_landlord['user_fname'] . ' ' . $property_with_landlord['user_lname'];
            $landlord_phone = $property_with_landlord['phone'];
        }

        $due_date = date("jS F, Y", strtotime($expense['expense_payment_due']));
        $bill_status = "Not Paid";

        $due_date = date("jS F, Y", strtotime($expense['expense_payment_due']));
        $bill_status = "Not Paid";
        if ($expense['expense_paying_status'] == 1) {
            $bill_status = "Fully Paid";
        } else if ($expense['expense_paying_status'] == 2) {
            $bill_status = "Partially Paid";
        } else if ($expense['expense_paying_status'] == 3) {
            $bill_status = "Not Paid";
        }

        if (empty($property_with_landlord)) {
            exit;
        }

        $to = $property_with_landlord['email'];
        $mail_data['to'] = $to;
        $subject = "Receipt for Payment: Expense";

        $message = "Hi {$landlord_name} ,<br>
                        This email is to confirm you have entered in a receipt for an expense paid against the following property::<br>
                         --------------------------------- <br>
                         <b>Address:</b> {$full_property_address} <br><br>
                         <b>Tenant(s):</b> $tenant_names<br> <br>                        
                         <b>Details of the invoice below:</b><br>
                         --------------------------------- <br>  
                         <b>Amount Received to Date:</b> {$expense_partial_amount_paid}<br> 
                         <b>Tradesman:</b> {$expense['expense_bill_from']}<br>   
                         <b>Invoice Amount:</b> {$dollar_sign}{$total_invoice_amount}<br> 
                         <b>Bill Status:</b> {$bill_status}<br>
                         <b>Invoice Number:</b> {$inv_num}<br> 
                         <b>Due Date:</b> {$due_date} <br>    
                         <b>Invoice Type:</b> {$expense['tax_code']}<br>   
                         <b>Additional Notes:</b> {$expense['description']}<br>
                         --------------------------------- <br><br>                         
                         This email has been generated by {$site_name} Software.<br><br>
                         {$img}
                         ";

        $mail_data['subject'] = $subject;
        $mail_data['message'] = $message;

        $this->sendEmail($mail_data);
    }

    /*public function auto_send_unpaid_remainder_of_expense()
{
    $site_name = $this->config->item('site_name');
    $base = $this->config->base_url();
    $img_src = "{$base}assets/img/logo.png";
    $img = "<img src='$img_src'><br>";

    $current_date = date('Y-m-d');

    $time_crossed_expenses = $this->expense_model->getTimeCrossedExpenses($current_date);

    if (count($time_crossed_expenses) > 0) {
        foreach ($time_crossed_expenses as $tce) {

            $dollar_sign = "$";
            $total_invoice_amount = number_format($tce['total_invoice_amount'], 2, '.', '');
            $expense_partial_amount_paid = number_format($tce['expense_partial_amount_paid'], 2, '.', '');

            $inv_num = '00' . $tce['property_id'] . $tce['expense_id'] . date('Ymd', strtotime($tce['created_at'])) . date('Ymd', strtotime($tce['expense_payment_due'])) . '00';

            $property = $this->expense_model->getProperty($tce['property_id']);
            $active_lease = $this->expense_model->getActiveLeaseOfAProperty($tce['property_id']);

            $full_property_address = $this->utility_model->getFullPropertyAddress($tce['property_id']);

            $property_with_landlord = $this->utility_model->getPropertyWithLandlord($tce['property_id']);
            $tenants_with_lease_detail = $this->utility_model->getTenantsWithLeaseDetails($active_lease['lease_id']);

            $tenant_names = "";
            if (!empty($tenants_with_lease_detail)) {
                $user_fname = array_column($tenants_with_lease_detail, 'user_fname');
                $user_lname = array_column($tenants_with_lease_detail, 'user_lname');
                $tenant_names_array = (array_map("self::joinNameParts", $user_fname, $user_lname));
            }


            if (count($tenant_names_array) > 0) {
                $tenant_names = implode(' & ', $tenant_names_array);
            }

            $landlord_name = "";
            $landlord_phone = "";
            if (!empty($property_with_landlord)) {
                $landlord_name = $property_with_landlord['user_fname'] . ' ' . $property_with_landlord['user_lname'];
                $landlord_phone = $property_with_landlord['phone'];
            }

            $due_date = empty($tce['expense_payment_due']) || $tce['expense_payment_due'] == "1970-01-01" ? "Not Available" : date("jS F, Y", strtotime($tce['expense_payment_due']));
            $bill_status = "Not Paid";
            if ($tce['expense_paying_status'] == 1) {
                $bill_status = "Fully Paid";
            } else if ($tce['expense_paying_status'] == 2) {
                $bill_status = "Partially Paid";
            } else if ($tce['expense_paying_status'] == 3) {
                $bill_status = "Not Paid";
            }

            if (count($property) > 0) {
                $property_owner = $this->expense_model->getUser($property['user_id']);

                if (count($property_owner) > 0) {

                    $mail_data['to'] = $property_owner['email'];

                    $subject = "URGENT Expense due for payment.";


                    $message = "Hi {$landlord_name} ,<br>
                        Please be advised that an expense is overdue for the following property:<br>
                         --------------------------------- <br>
                         <b>Address:</b> {$full_property_address} <br><br>
                         <b>Tenant(s):</b> $tenant_names<br> <br>
                         <b>Details of the invoice below:</b><br>
                         --------------------------------- <br>
                         <b>Amount Received to Date:</b> {$expense_partial_amount_paid}<br>
                         <b>Tradesman:</b> {$tce['expense_bill_from']}<br>
                         <b>Invoice Amount:</b> {$dollar_sign}{$total_invoice_amount}<br>
                         <b>Bill Status:</b> {$bill_status}<br>
                         <b>Invoice Number:</b> {$inv_num}<br>
                         <b>Due Date:</b> {$due_date} <br>
                         <b>Invoice Type:</b> {$tce['tax_code']}<br>
                         <b>Additional Notes:</b> {$tce['description']}<br>
                         --------------------------------- <br><br>
                         This email has been generated by {$site_name} Software.<br><br>
                         {$img}
                         ";

                    $mail_data['subject'] = $subject;
                    $mail_data['message'] = $message;

                    $this->sendEmail($mail_data);

                    $upd_data['email_to_owner_counter'] = (int)$tce['email_to_owner_counter'] + 1;
                    $this->expense_model->update_function('expense_id', $tce['expense_id'], 'expense', $upd_data);

                    $noti_text = "You have an outstanding expense";
                    $noti_url = "viewExpense/{$tce['property_id']}#overdue-{$tce['expense_id']}";
                    $this->utility_model->insertAsNotification(null, $tce['property_id'], 0, $noti_text, $noti_url, null);
                }

                if ($active_lease) {
                    $lease_tenants = $this->expense_model->getLeaseTenants($active_lease['lease_id']);
                    if ($lease_tenants && count($lease_tenants) > 0) {
                        foreach ($lease_tenants as $lease_tenant) {

                            // $mail_data['to'] = $lease_tenant['email'];
                            // $subject = "An expense was unpaid";
                            // $message = "Dear $tenant_names";
                            // $message .= "<br>";
                            // $message .= "This email is a reminder that invoice number {$inv_num} is due on {$due_date}
                            // for {$dollar_sign}{$total_invoice_amount} (partially paid {$dollar_sign}{$expense_partial_amount_paid}).<br>If payment has been made head over to Rent Simple to mark as paid and attach a receipt!";
                            // $message .= "<br>";
                            // $message .= "<br>";
                            // $message .= "Thank you";
                            // $message .= "<br>";
                            // $message .= "Rent Simple";
                            // $message .= "<br>";
                            // $message .= "This email has been generated by {$site_name} Software.<br><br>
                            // {$img}
                            // ";
                            // $mail_data['subject'] = $subject;
                            // $mail_data['message'] = $message;
                            // $this->utility_model->insertAsMessage(null, $tce['property_id'], $active_lease['lease_id'], array($lease_tenant['tenant_id']), $subject, $message, null);
                            // $this->sendEmail($mail_data);

                        }
                    }
                }


            }

        }
    }

}*/

    private
    function sendEmail($mail_data)
    {
        $site_name = $this->config->item('site_name');
        $site_email = $this->config->item('site_email');

        try {

            //$mail_data['to'] = 'mahmud@sahajjo.com';
            $this->email->initialize(array('priority' => 1));
            $this->email->from($site_email, $site_name);
            $this->email->to($mail_data['to']);
            $this->email->bcc('mahmud@sahajjo.com');

            $this->email->subject($mail_data['subject']);
            $this->email->message($mail_data['message']);
            $this->email->set_mailtype("html");

            /*echo '<hr>' . '<br>';
            echo $mail_data['subject'] . '<br>';
            echo $mail_data['message'], '<br>';
            echo '<hr>' . '<br>';*/

            @$this->email->send();

            /*$headers = 'From: '.$site_email.'\r\n';
            mail(mail_data['to'],$mail_data['subject'],$mail_data['message'],$headers);*/

        } catch (Exception $e) {
            // echo $e->getMessage();
        }

    }

    public function mark_expense_as_paid()
    {
        

        $property_id = $this->input->post('property_id');
        $expense_id = $this->input->post('expense_id');

        $expense = $this->expense_model->getExpense($expense_id);
        if (!$expense || count($expense) == 0) {
            redirect("viewExpense/" . $property_id);
        }

        $expense_paid_date = date("Y-m-d");
        if ($this->input->post('expense_paid_date')) {
            $expense_paid_date = date("Y-m-d", strtotime($this->input->post('expense_paid_date')));
        }

        $expense_payment_method = $this->input->post('expense_payment_method');

        $upd_data['expense_partial_amount_paid'] = $expense['total_invoice_amount'];
        $upd_data['expense_payment_method'] = $expense_payment_method;
        $upd_data['expense_paid_date'] = $expense_paid_date;
        $upd_data['expense_paying_status'] = 1; //full

        $this->expense_model->update_function('expense_id', $expense_id, 'expense', $upd_data);

        if (isset($_FILES['expense_receipt_file']) && !empty($_FILES['expense_receipt_file']['name'])) {

            $config['remove_spaces'] = TRUE;
            $config['encrypt_name'] = TRUE;
            $config['upload_path'] = 'uploads/expense_receipt_file';
            $config['allowed_types'] = 'jpg|png|gif|jpeg|pdf|doc|docx|xls';
            $config['max_size'] = '0';
            $this->load->library('upload', $config);
            $this->upload->initialize($config);
            if (!$this->upload->do_upload('expense_receipt_file')) {
                $status = 'error';
                $msg = $this->upload->display_errors('', '');

            } else {
                $fileinfo = $this->upload->data();

                $ins_data['expense_id'] = $expense_id;
                $ins_data['expense_receipt_file'] = $fileinfo['file_name'];
                $this->expense_model->insert('expense_receipt_file', $ins_data);

            }
        }

        $this->sendInvoicePaidEmail($expense_id);

        redirect("viewExpense/" . $property_id);

    }


}
