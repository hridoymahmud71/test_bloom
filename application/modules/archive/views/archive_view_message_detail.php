<?php $this->load->view('front/headlink'); ?>
<?php $this->load->view('front/archived_top_menu'); ?>
<div class="container">
  <?php $this->load->view('front/head_nav'); ?>
  <div>
    <h3><?php if ($property) {echo $property[0]['property_address'];}?></h3>
    <div class="row">  
      <div class="ss_container">
        <div class="ss_bound_content">
          <!-- Nav tabs -->
          <div class="ss_expensesinvoices">
            <div class="ss_expensesinvoices_top">
              <div class="container">
                <div class="col-md-12">
                  <div class="col-md-4" style="border-right: 2px dashed #ccc;">
                    <h4>Tenants selected <input type="checkbox" checked> have received notifications via email and will have access to this entire conversation.</h4>
                  </div>
                  <div class="col-md-6">
                    <div class="col-md-1">TO :</div>
                    <div class="col-md-5">
                      <?php foreach($all_tenant as $row){?>
                        <?php foreach($all_message_tenant as $rows){?>
                          <?php if($row['tenant_id']==$rows['tenant_id']){?>
                              <label for="tenant_id">
                                <input disabled type="checkbox" checked id="tenant_id" name="tenant_id[]" value="<?=$row['tenant_id'];?>"> <?=$row['user_fname'];?> <?=$row['user_lname'];?>
                              </label>
                          <?php }else{?>
                            <label for="tenant_id">
                              <input disabled type="checkbox" id="tenant_id" name="tenant_id[]" value="<?=$row['tenant_id'];?>"> <?=$row['user_fname'];?> <?=$row['user_lname'];?>
                            </label>
                          <?php }?>
                        <?php }?>
                      <?php }?>
                    </div>
                  </div>
                </div>

                  <div class="col-md-12" style="padding: 10px 0 10px 0;">
                    <div class="col-md-12">
                      <div>
                        <h4><?=$message_detail[0]['subject'];?></h4>
                      </div>
                    </div>
                  </div>
                <?php foreach($all_message_comment as $com_row){?>
                  <div class="col-md-12" style="padding: 10px 0 10px 0;">
                    <div class="col-md-12">
                      <!-- <div>
                        <h4>fdhgsrg</h4>
                      </div> -->
                      <div class="col-md-3">
                        <div><?=$message_detail[0]['user_name'];?></div>
                        <div><?=date("d/m/Y H:i:s A", strtotime($message_detail[0]['created_at']));?></div>
                      </div>
                      <div class="col-md-9">
                        <div class="col-md-12">
                          <p><?=$com_row['comment'];?></p>
                        </div>
                        <div class="col-md-12">
                          <?php foreach($all_message_document as $doc_row){?>
                            <?php if($doc_row['message_com_id']==$com_row['message_comment']){?>
                              <img style="height:100px;width:100px;" src="uploads/message_document/<?=$doc_row['document'];?>">
                            <?php }?>
                          <?php }?>
                        </div>
                      </div>
                    </div>
                  </div>
                <?php }?>

                <form action="message/upload_new_message" method="post" enctype="multipart/form-data">
                  
                  <!--<div class="col-md-12" style="padding: 10px 0 10px 0;">
                    <div class="col-md-12">
                      <div class="col-md-3">
                        <h2>Add Comment</h2>
                      </div>
                      <div class="col-md-9">
                        <div class="col-md-12">
                          <textarea class="form-control" rows="8" name="comment"></textarea>
                        </div>
                      </div>
                    </div>
                  </div>-->

                      <?php foreach($all_tenant as $row){?>
                        <?php foreach($all_message_tenant as $rows){?>
                          <?php if($row['tenant_id']==$rows['tenant_id']){?>
                              <label for="tenant_id">
                                <input type="hidden" name="tenant_id[]" value="<?=$row['tenant_id'];?>">
                              </label>
                          <?php }?>
                        <?php }?>
                      <?php }?>

                  <div class="col-md-12" style="padding: 10px 0 10px 0;">
                    <div class="col-md-12">
                      <!--<div class="col-md-3">
                        <h4>Upload photos, videos, documents:</h4>
                      </div>-->
                      <div class="col-md-9">
                        <!--<div class="col-md-12">
                           <div class="dropzone" >
                               <span style="display: none" class="my-dz-message pull-right">
                                   <h3>upload more</h3>
                               </span>
                            <div class="dz-message">
                              <h3>Click Here to upload your files</h3>
                            </div>
                          </div>
                            <div>* Max 20 MB per file</div>
                          <div class="previews" id="preview"></div>
                        </div>-->
                        <div class="col-md-12 ss_container" style="background: none;">
                          <div class="col-md-4">
                            <input type="hidden" name="message_id" value="<?=$message_detail[0]['message_id'];?>">
                            <input type="hidden" name="user_full_name" value="<?=$message_detail[0]['user_name'];?>">
                            <input type="hidden" name="subject" value="<?=$message_detail[0]['subject'];?>">
                           <!-- <input type="submit" class="btn btn-primary" value="Send Message">-->
                          </div>
                          <div class="col-md-6 pull-right">
                            <!--<a class="btn btn-info">
                              <span class="glyphicon glyphicon-remove" aria-hidden="true"></span>
                               cancel
                            </a>-->
                          </div>
                        </div>
                        <div style="clear: both;"></div>
                      </div>
                    </div>
                  </div>
                </form>
              </div>
              <!-- <div class="col-md-2"><a href="#"  data-toggle="modal" data-target="#taxreport">Generate Tax Report</a></div> -->
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>    
</div>


<?php $this->load->view('front/footerlink'); ?> 
<script>
  $(".ss_expanse_top_h").click(function(){
    $(".ss_wxpance_ifo").toggle("slow");
  });
  $("#paid_expense_edit").click(function(){
    $(".ss_expanse_top_h, .ss_wxpance_ifo ").hide(1000);
    $("#paid_expense_editable ").show(1000);
  });
  $("#paid_expense_edit_save, #paid_expense_edit_cancel").click(function(){
    $(".ss_expanse_top_h, .ss_wxpance_ifo ").show(1000);
    $("#paid_expense_editable ").hide(1000);
  });
  $(".delete_expense").click(function(){
    $(".ss_expensesinvoices_c_view, #paid_expense_editable").remove();
  });
</script>

  <script type="text/javascript">
    Dropzone.autoDiscover = false;
    var foto_upload = new Dropzone(".dropzone", {
      url: "<?php echo base_url('message/message_document') ?>",
      maxFilesize: 20,
      method: "post",
      acceptedFiles: ".jpg,.jpeg,.png,.gif",
      paramName: "userfile",
      dictInvalidFileType: "This File Type Not Supported",
      addRemoveLinks: true,
      init: function () {
        var count = 0;
        thisDropzone = this;
        this.on("success", function (file, json) {
          var obj = json;
          $('.previews').append(
            "<input type='hidden' name='message_in_doc[]' value='" + obj + "'>\n\
            <input type='hidden' name='message_in_width[]' value='" + file.width + "'>\n\
            <input type='hidden' name='message_in_height[]' value='" + file.height + "'>"
            );
            $(".my-dz-message").show();
        });
      }
    });
  </script>