<?php $this->load->view('front/headlink'); ?>
<?php $this->load->view('front/archived_top_menu'); ?>
  <div class="container">
    <?php $this->load->view('front/head_nav');?>
    <div class="row"> 
     
      <div class="ss_container">
        <h2>Files </h2>
        <div class="ss_bound_content">
          <form onsubmit="return chk_document_upload()" action="lease/update_lease_doc" method="post" enctype="multipart/form-data">
            <!--<div class="row-fluid document-actions">
                <div class="col-md-8  text-left">
                    <p><strong> Upload your file</strong></p>
                    <p>
                        If you are uploading multiple files you can save them under the one title.
                    </p>
                </div>
                <div class="col-md-4  text-left">
                    <ol><strong>Instructions:</strong>
                        <li>Upload the file/s</li>
                        <li>Add a title & description.</li>
                        <li>Click save!</li>
                    </ol>
                </div>
            </div>-->
            <div class="row ">

                <div class="form-group col-md-12">
                    <?php foreach($doc_detail_image as $row){?>
                        <li><a target="_blank" href="uploads/lease_document/<?=$row['document_detail']?>"><?=$row['document_detail']?></a></li>
                    <?php }?>
                </div>


              <div class="form-group col-md-12" style="display: none">
                <label class="col-md-3" for="">Upload File(s)</label>
                <div class="col-md-6"> 
                  <div class="form-group inputDnD">
                     <div class="dropzone" >
                         <span style="display: none" class="my-dz-message pull-right">
                             <h3>Click Here to upload another file</h3>
                         </span>
                      <div class="dz-message" >
                        <h3> Click Here to upload your files</h3>
                      </div>
                    </div>
                    <div class="previews" id="preview"></div>
                  </div>
                    <div>* Max 20 MB per file</div>
                </div>
                <div class="col-md-3">
                  <?php foreach($doc_detail_image as $row){?>
                    <li><a target="_blank" href="uploads/lease_document/<?=$row['document_detail']?>"><?=$row['document_detail']?></a></li>
                  <?php }?>
                </div>
              </div>
            </div>
            <div class="row-fluid document-actions" style="display:none">
              <div class="col-md-4  text-left">
                <p> Tenants selected  will receive notifications via email and will have access to these files.</p>
              </div>
              <div class="col-md-8  text-left">
                <div class="col-md-3 text-center"><h4>To</h4></div>
                <div class="col-md-9">
                  <?php foreach($all_tenant as $key=>$row){?>
                  <div class="checkbox">
                    <label>
                      <!--<input name="user_id[]" <?php /*if(in_array($row['user_id'],$chk_user_data)){echo "checked";}*/?> value="<?/*=$row['user_id'];*/?>" type="checkbox"> <?/*=$row['user_fname'];*/?> --><?/*=$row['user_lname'];*/?>
                        <input name="user_id[]" <?php if(1){echo "checked";}?> value="<?=$row['user_id'];?>" type="checkbox"> <?=$row['user_fname'];?> <?=$row['user_lname'];?>
                    </label>
                  </div>
                  <?php }?>
                </div>
              </div>
            </div>
            <div class="row extra_padding">
              <div class="col-md-3">
                Title
              </div>
              <div class="col-md-6">
                <input type="text" readonly name="doc_title" value="<?=$doc_detail[0]['doc_title']?>" id="inpu_title" class="form-control">
              </div>
              <div class="col-md-3">
              </div>
            </div>
            <div style="display: none"  class="row extra_padding">
              <div class="col-md-3">
                Category
              </div>
              <div class="col-md-6">
                <select id="select_title_option" class="form-control" name="doc_cat">
                  <option value="">Select an option</option>
                  <!--<option <?php /*if($doc_detail[0]['doc_cat']==1){echo "selected";}*/?> value="1">Receipts - Maintenance &amp; Repairs</option>
                  <option <?php /*if($doc_detail[0]['doc_cat']==2){echo "selected";}*/?> value="2">Receipts - Strata</option>
                  <option <?php /*if($doc_detail[0]['doc_cat']==3){echo "selected";}*/?> value="3">Lease &amp; Bond</option>
                  <option <?php /*if($doc_detail[0]['doc_cat']==4){echo "selected";}*/?> value="4">Inspection - Entry</option>
                  <option <?php /*if($doc_detail[0]['doc_cat']==5){echo "selected";}*/?> value="5">Inspection - Routine</option>
                  <option <?php /*if($doc_detail[0]['doc_cat']==6){echo "selected";}*/?> value="6">Inspection - Exit</option>
                  <option <?php /*if($doc_detail[0]['doc_cat']==7){echo "selected";}*/?> value="7">Maintenance</option>
                  <option <?php /*if($doc_detail[0]['doc_cat']==8){echo "selected";}*/?> value="8">Other</option>
                  <option <?php /*if($doc_detail[0]['doc_cat']==9){echo "selected";}*/?> value="9">Receipts - Accounting &amp; Bookkeeping</option>
                  <option <?php /*if($doc_detail[0]['doc_cat']==10){echo "selected";}*/?> value="10">Receipts - Bank fees</option>
                  <option <?php /*if($doc_detail[0]['doc_cat']==11){echo "selected";}*/?> value="11">Receipts - Council rates</option>
                  <option <?php /*if($doc_detail[0]['doc_cat']==12){echo "selected";}*/?> value="12">Receipts - GST</option>
                  <option <?php /*if($doc_detail[0]['doc_cat']==13){echo "selected";}*/?> value="13">Receipts - Real estate commission</option>
                  <option <?php /*if($doc_detail[0]['doc_cat']==14){echo "selected";}*/?> value="14">Receipts - Insurance</option>
                  <option <?php /*if($doc_detail[0]['doc_cat']==15){echo "selected";}*/?> value="15">Receipts - Land tax</option>
                  <option <?php /*if($doc_detail[0]['doc_cat']==16){echo "selected";}*/?> value="16">Receipts - Legal fees</option>
                  <option <?php /*if($doc_detail[0]['doc_cat']==17){echo "selected";}*/?> value="17">Receipts - Loan interest repayments</option>
                  <option <?php /*if($doc_detail[0]['doc_cat']==18){echo "selected";}*/?> value="18">Receipts - Management &amp; admin fees</option>
                  <option <?php /*if($doc_detail[0]['doc_cat']==19){echo "selected";}*/?> value="19">Receipts - Property advertising</option>
                  <option <?php /*if($doc_detail[0]['doc_cat']==20){echo "selected";}*/?> value="20">Receipts - Property cleaning</option>
                  <option <?php /*if($doc_detail[0]['doc_cat']==21){echo "selected";}*/?> value="21">Receipts - Real estate agent fees</option>
                  <option <?php /*if($doc_detail[0]['doc_cat']==22){echo "selected";}*/?> value="22">Receipts - Stationary and postage</option>
                  <option <?php /*if($doc_detail[0]['doc_cat']==23){echo "selected";}*/?> value="23">Receipts - Strata and body corporate</option>
                  <option <?php /*if($doc_detail[0]['doc_cat']==24){echo "selected";}*/?> value="24">Receipts - Utilities (electricity, gas, water)</option>
                  <option <?php /*if($doc_detail[0]['doc_cat']==25){echo "selected";}*/?> value="25">Receipts - Building inspection</option>
                  <option <?php /*if($doc_detail[0]['doc_cat']==26){echo "selected";}*/?> value="26">Receipts - Legal costs</option>
                  <option <?php /*if($doc_detail[0]['doc_cat']==27){echo "selected";}*/?> value="27">Receipts - Stamp duty</option>
                  <option <?php /*if($doc_detail[0]['doc_cat']==28){echo "selected";}*/?> value="28">Receipts - Pest control</option>
                  <option <?php /*if($doc_detail[0]['doc_cat']==29){echo "selected";}*/?> value="29">Receipts - Other purchase costs</option>
                  <option <?php /*if($doc_detail[0]['doc_cat']==30){echo "selected";}*/?> value="30">Receipts - Other sales expenses</option>-->
                </select>
              </div>
              <div class="col-md-3">
              </div>
            </div>
            <div class="row extra_padding">
              <div class="col-md-3">
                Description
                  <br>
                  (you may want to note the date or time photos were taken or what the documents include and when they were received)
              </div>
              <div class="col-md-6">
                <textarea name="doc_desc" readonly rows="6" class="form-control"><?=$doc_detail[0]['doc_desc'];?></textarea>
              </div>
              <div class="col-md-3">
              </div>
            </div>
            <div class="col-md-3"></div>
              <div class="col-md-6">
                <div class="modal-footer clear">
                  <br><br>
                  <button type="button" class="btn btn-light" data-dismiss="modal">Close</button>
                  <input type="hidden" name="property_id" value="<?=$property_id;?>">
                  <input type="hidden" name="doc_detail_id" value="<?=$doc_detail[0]['doc_detail_id'];?>">
                  <!--<input type="submit" class="btn btn-primary" value="Save Upload">-->
                  <br><br>
                </div>
              </div>
            <div class="col-md-3"></div>
            <div style="clear: both;"></div>
          </form>
        </div>
      </div>
      <div style="clear: both;"></div>
    </div>    
  </div>
  <script src="//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
  <?php $this->load->view('front/footerlink');?>
  <script type="text/javascript">
    Dropzone.autoDiscover = false;
    var foto_upload = new Dropzone(".dropzone", {
      url: "<?php echo base_url('lease/lease_document') ?>",
      maxFilesize: 20,
      method: "post",
      acceptedFiles: ".jpg,.jpeg,.png,.gif",
      paramName: "userfile",
      dictInvalidFileType: "This File Type Not Supported",
      addRemoveLinks: true,
      init: function () {
        var count = 0;
        thisDropzone = this;
        this.on("success", function (file, json) {
          var obj = json;
          $('.previews').
          append(
            "<input type='hidden' name='lease_doc[]' value='" + obj + "'>\n\
            <input type='hidden' name='lease_width[]' value='" + file.width + "'>\n\
            <input type='hidden' name='lease_height[]' value='" + file.height + "'>"
            );
            $(".my-dz-message").show();
        });
      }
    });
  </script>
  <script type="text/javascript">
    function chk_document_upload()
    {
      var ok = 0;
      var inpu_title = $('#inpu_title').val();
      var select_title_option = $('#select_title_option').val();
      if(inpu_title=='')
      {
        $('#inpu_title').closest('div').addClass("has-error");
        ok = ok +1;
      }
      else
      {
        $('#inpu_title').closest('div').removeClass("has-error");
      }
      /*if(select_title_option == '')
      {
        $('#select_title_option').closest('div').addClass("has-error");
        ok = ok +1;
      }
      else
      {
        $('#select_title_option').closest('div').removeClass("has-error");
      }*/


      if(ok == 0)
      {
        ok = true;
      }
      else
      {
        ok = false;
      }
      return ok;
    }
  </script>
</body>
</html>