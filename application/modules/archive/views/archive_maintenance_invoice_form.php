<?php $this->load->view('front/headlink'); ?>
<?php $this->load->view('front/top_menu'); ?>
<div class="container">
    <?php $this->load->view('front/head_nav'); ?>
    <div class="row">
        
        <div class="ss_container">
            <h2><?= ucwords(str_replace("_", "", $which_form)) ?> Invoice </h2>
            <div class="ss_bound_content">
                <form onsubmit="return chk_document_upload()" action="<?= $form_action ?>" method="post"
                      enctype="multipart/form-data">
                    <div class="row extra_padding">
                        <div class="col-md-3">
                        </div>
                        <div class="col-md-6">
                            <?php if ($this->session->flashdata('validation_errors')) { ?>
                                <div class="panel panel-danger">
                                    <div class="panel-heading">Error!</div>
                                    <div class="panel-body"><?php echo $this->session->flashdata('validation_errors'); ?></div>
                                </div>
                            <?php } ?>
                            <?php if ($this->session->flashdata('warning')) { ?>
                                <div class="panel panel-warning">
                                    <div class="panel-heading">Warning!</div>
                                    <div class="panel-body"><?php echo $this->session->flashdata('warning'); ?></div>
                                </div>
                            <?php } ?>
                            <?php if ($this->session->flashdata('add_success')) { ?>
                                <div class="panel panel-primary">
                                    <div class="panel-heading">Success!</div>
                                    <div class="panel-body">
                                        Successfully added Invoice
                                        &nbsp;
                                        <a class="btn btn-primary btn-sm"
                                           href="Allmaintenance/<?= $maintenance?$maintenance['property_id']:'' ?>">View Maintenance List
                                        </a>
                                    </div>
                                </div>
                            <?php } ?>
                            <?php if ($this->session->flashdata('update_success')) { ?>
                                <div class="panel panel-primary">
                                    <div class="panel-heading">Success!</div>
                                    <div class="panel-body">
                                        Successfully updated invoice
                                        &nbsp;
                                        <a class="btn btn-primary btn-sm"
                                           href="Allmaintenance/<?= $maintenance?$maintenance['property_id']:'' ?>">View Maintenance List
                                        </a>
                                    </div>
                                </div>
                            <?php } ?>
                        </div>
                    </div>
                    <div class="col-md-3">
                    </div>
            </div>

            <div class="row extra_padding">
                <div class="col-md-3">
                    Description
                    <br>
                </div>
                <div class="col-md-6">
                            <textarea name="maintenance_invoice_description" id="maintenance_invoice_description" readonly
                                      rows="6"
                                      class="form-control"><?= ($maintenance_invoice) ? $maintenance_invoice['maintenance_invoice_description'] : ''; ?></textarea>
                </div>
                <div class="col-md-3">
                </div>
            </div>
            <div class="row">
                <div class="col-md-3">
                    Invoice amount
                </div>
                <div class="col-md-6">
                    <input id="maintenance_invoice_amount" type="text" name="maintenance_invoice_amount" readonly
                           class="form-control"
                           value="<?= ($maintenance_invoice) ? $maintenance_invoice['maintenance_invoice_amount'] : ''; ?>"
                    >
                </div>
                <div class="col-md-3">
                </div>
            </div>
            <br>
            <div class="row">
                <div class="col-md-3">
                    Invoice due date
                </div>
                <div class="col-md-6">
                    <input id="maintenance_invoice_due_date" readonly type="text" name="maintenance_invoice_due_date"
                           class="form-control"
                           value="<?= ($maintenance_invoice) ? date('jS F, Y', strtotime($maintenance_invoice['maintenance_invoice_due_date'])) : ''; ?>"
                    >
                </div>
                <div class="col-md-3">
                </div>
            </div>

            <div class="col-md-3"></div>
            <div class="col-md-6">
                <div class="modal-footer clear">
                    <br><br>
                    <input type="hidden" name="maintenance_id"
                           value="<?= ($maintenance) ? $maintenance['maintenance_id'] : ''; ?>">
                    <input type="hidden" name="maintenance_invoice_id"
                           value="<?= ($maintenance_invoice) ? $maintenance_invoice['maintenance_invoice_id'] : ''; ?>">
                    <input type="hidden" name="property_id" value="<?= $property_id; ?>">
                    <!--<input type="submit" class="btn btn-primary" value="Save">-->
                    <br><br>
                </div>
            </div>
            <div class="col-md-3"></div>
            <div style="clear: both;"></div>
            </form>
        </div>
    </div>
</div>
</div>
<script src="//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
<?php $this->load->view('front/footerlink'); ?>


<script type="text/javascript">

    function chk_document_upload() {
        var err = 0;
        var ret = false;
        var maintenance_invoice_description = $('#maintenance_invoice_description');
        var maintenance_invoice_amount = $('#maintenance_invoice_amount');
        var maintenance_invoice_due_date = $('#maintenance_invoice_due_date');

        if (maintenance_invoice_description.val() == '') {
            maintenance_invoice_description.closest('div').addClass("has-error");
            err = err + 1;
        }
        else {
            maintenance_invoice_description.closest('div').removeClass("has-error");
        }

        if (maintenance_invoice_amount.val() == '' || isNaN(maintenance_invoice_amount.val()) || parseFloat(maintenance_invoice_amount.val()) <= 0) {
            maintenance_invoice_amount.closest('div').addClass("has-error");
            err = err + 1;
        }
        else {
            maintenance_invoice_amount.closest('div').removeClass("has-error");
        }
        if (maintenance_invoice_due_date.val() == '') {
            maintenance_invoice_due_date.closest('div').addClass("has-error");
            err = err + 1;
        }
        else {
            maintenance_invoice_due_date.closest('div').removeClass("has-error");
        }

        if (err == 0) {
            ret = true;
        }
        return ret;
    }
</script>

<script type="text/javascript">
    $(function () {
        $('#maintenance_invoice_due_date').datepicker({
            required: true,
            dateFormat: 'd M, yy'
        });
    });
</script>


</body>
</html>