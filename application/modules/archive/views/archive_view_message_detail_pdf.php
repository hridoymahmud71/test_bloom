<?php $this->load->view('front/headlink'); ?>

<div class="container">

  <div>
      <h3><?php if ($property) {echo $property[0]['property_address'];}?></h3>
    <div class="row">  
      <div class="ss_container">
        <div class="ss_bound_content">
          <!-- Nav tabs -->
          <div class="ss_expensesinvoices">
            <div class="ss_expensesinvoices_top">
              <div class="container">
                <div class="col-md-12">
                  <div class="col-md-4" style="border-right: 2px dashed #ccc;">
                    <h4>Tenants selected <input type="checkbox" checked> will receive notifications via email and will have access to this entire conversation.</h4>
                  </div>
                  <div class="col-md-6">
                    <div class="col-md-1">TO :</div>
                    <div class="col-md-5">
                      <?php foreach($all_tenant as $row){?>
                        <?php foreach($all_message_tenant as $rows){?>
                          <?php if($row['tenant_id']==$rows['tenant_id']){?>
                              <label for="tenant_id">
                                <?=$row['user_fname'];?> <?=$row['user_lname'];?>
                              </label>
                          <?php } ?>
                        <?php }?>
                      <?php }?>
                    </div>
                  </div>
                </div>

                  <div class="col-md-12" style="padding: 10px 0 10px 0;">
                    <div class="col-md-12">
                      <div>
                        <h4><?=$message_detail[0]['subject'];?></h4>
                      </div>
                    </div>
                  </div>
                <?php foreach($all_message_comment as $com_row){?>
                  <div class="col-md-12" style="padding: 10px 0 10px 0;">
                    <div class="col-md-12">
                      <!-- <div>
                        <h4>fdhgsrg</h4>
                      </div> -->
                      <div class="col-md-3">
                        <div><?=$message_detail[0]['user_name'];?></div>
                        <div><?=date("d/m/Y H:i:s A", strtotime($message_detail[0]['created_at']));?></div>
                      </div>
                      <div class="col-md-9">
                        <div class="col-md-12">
                          <p><?=$com_row['comment'];?></p>
                        </div>
                        <div class="col-md-12">
                          <?php foreach($all_message_document as $doc_row){?>
                            <?php if($doc_row['message_com_id']==$com_row['message_comment']){?>
                              <img style="height:100px;width:100px;" src="uploads/message_document/<?=$doc_row['document'];?>">
                            <?php }?>
                          <?php }?>
                        </div>
                      </div>
                    </div>
                  </div>
                <?php }?>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>    
</div>


<?php $this->load->view('front/footerlink'); ?> 
