<?php $this->load->view('front/headlink'); ?>
<?php $this->load->view('front/archived_top_menu'); ?>
<div class="container">
    <?php $this->load->view('front/head_nav'); ?>
    <div class="row">
        <div class="ss_container">
            <h3 class="extra_heading"> Water Invoices
                <?php if($property_id > 0) { ?>
                <!--<a href="addWaterInvoice/<?/*= $property_id; */?>" class="pull-right btn btn-primary"
                   style="    margin-left: 10px;">
                    <span class="glyphicon glyphicon-plus" aria-hidden="true"></span>
                    Add new Invoice
                </a>-->
                <?php } ?>

            </h3>
            <div>
                <button class="btn btn-light pull-right" id="ss_refreah"><span class="glyphicon glyphicon-refresh"
                                                                               aria-hidden="true"></span></button>
            </div>
            <?php if ($this->session->flashdata('mail_send_success')) { ?>
                <div class="row">
                    <div class="col-md-4"></div>
                    <div class="col-md-4">
                        <div class="panel panel-primary">
                            <div class="panel-heading">Success!</div>
                            <div class="panel-body"><?php echo $this->session->flashdata('mail_send_success'); ?></div>
                        </div>
                    </div>
                    <div class="col-md-4"></div>
                </div>

            <?php } ?>
            <div class="ss_bound_content">

                <!-- Tab panes -->
                <div class="tab-content">
                    <div class="row-fluid document-list-header top-table-name">
                        <!--<div class="col-md-2">Label</div>-->
                        <div class="col-md-2">Invoice Number</div>
                        <div class="col-md-2">Invoice Description</div>
                        <div class="col-md-1">Invoice Due date</div>
                        <div class="col-md-1">Period</div>
                        <div class="col-md-1">Total Invoice Amount</div>
                        <div class="col-md-1">Metre</div>
                        <div class="col-md-1">Amount Paid</div>
                        <div class="col-md-1">Amount Outstanding</div>
                        <div class="col-md-2">Action</div>
                    </div>
                    <div role="tabpanel" class="tab-pane active" id="dall">
                        <form id="all_submit_form" action="lease/doc_download" method="post">
                            <?php if (count($water_invoices) > 0) { ?>
                                <?php foreach ($water_invoices as $water_invoice) { ?>
                                    <div class="row-fluid documents-list_area">
                                        <a class="rep_href">
                                            <div class="col-md-2">
                                                <h4 class="table-name">Document Title</h4>
                                                <h5 style="word-break: break-all;" id="document_title"><?= $water_invoice['water_invoice_number'] ?></h5>
                                            </div>
                                            <div class="col-md-2">
                                                <h4 class="table-name">Document Description</h4>
                                                <h5 id="document_title"><?= $water_invoice['water_invoice_description'] ?></h5>
                                            </div>
                                            <div class="col-md-1 uploaded-date"><?= date("jS F, Y", strtotime($water_invoice['water_invoice_due_date'])); ?></div>
                                            <div class="col-md-1 uploaded-date"><?= date("F, Y", strtotime($water_invoice['water_invoice_period_start'])); ?> to <?= date("F, Y", strtotime($water_invoice['water_invoice_period_end'])); ?></div>
                                            <div class="col-md-1 uploaded-date">$<?= $water_invoice['water_invoice_amount']; ?></div>
                                            <div class="col-md-1 uploaded-date"><?= $water_invoice['water_invoice_water_metre']; ?></div>
                                            <div class="col-md-1 uploaded-date">$<?= $water_invoice['water_invoice_amount_paid']; ?></div>
                                            <div class="col-md-1 uploaded-date">$<?= $water_invoice['water_invoice_amount_due']; ?></div>
                                            <div class="col-md-2 ">
                                                <a class="btn btn-sm btn-default"
                                                   href="archive/editWaterInvoice/<?= $water_invoice['water_invoice_id'] ?>">View</a>
                                                <!--<a class="btn btn-sm btn-default"
                                                   href="sendInvoiceToTenants/<?/*= $water_invoice['water_invoice_id'] */?>">Send Invoice</a>
                                                <?php /*if($water_invoice['water_invoice_status'] == 0) {*/?>
                                                <a class="btn btn-sm btn-default"
                                                   href="payWaterInvoice/<?/*= $water_invoice['water_invoice_id'] */?>">Pay</a>
                                                --><?php /*} */?>
                                            </div>
                                        </a>
                                    </div>
                                <?php } ?>
                            <?php } ?>
                            <div id="get_val_here"></div>
                        </form>
                    </div>
                    <div>
                        <form id="each_submit_form" action="lease/each_doc_download" method="post">
                            <div id="get_each_here"></div>
                    </div>


                </div>
            </div>
        </div>
    </div>
</div>
<script src="//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
<?php $this->load->view('front/footerlink'); ?>
</body>
</html>