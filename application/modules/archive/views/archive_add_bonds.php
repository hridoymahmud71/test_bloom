<?php $this->load->view('front/headlink'); ?>
<?php $this->load->view('front/archived_top_menu'); ?>
    <div class="container">
        <div class="logo text-left"> 
        <?php $this->load->view('front/head_nav'); ?>
        <div class="container">
          <div class="modal fade" id="myModal" role="dialog">
            <div class="modal-dialog">
              <div class="modal-content">
                <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal">&times;</button>
                  <h4 class="modal-title">Select a Lease or add a new lease</h4>
                </div>
                <form action="lease/all_bonds/<?=$property_id;?>" method="post">
                  <div class="modal-body">
                    <h4>Lease Option</h4>
                    <select class="form-control" name="lease_id">
                      <option value="new_lease">Add a new lease</option>
                      <?php foreach($all_lease as $row){?>
                      <option <?php if($row['lease_id']==$lease_detail[0]['lease_id']){echo "selected";}?> value="<?=$row['lease_id'];?>"><?=$row['lease_name'];?></option>
                      <?php }?>
                    </select>
                  </div>
                  <div class="modal-footer">
                    <input type="submit" class="btn btn-success" value="Confirm" name="">
                  </div>
                </form>
              </div>
            </div>
          </div>
        </div>
        </div>
        <div class="row">
          <h4>Lease: <?=$lease_detail[0]['lease_name'];?>
              <!--(<?php /*echo ((abs(strtotime(date('Y-m-d'))-strtotime($lease_detail[0]['lease_end_date']))/86400))*/?> days to go) <a href="#" data-toggle="modal" data-target="#myModal"><span class="glyphicon glyphicon-cog" aria-hidden="true"></span></a>-->
          </h4>
          
            <div class="ss_container">
                <h2>Bond Information</h2>
                <div class="ss_bound_content">
                    <!-- Nav tabs -->
                    <ul class="nav nav-tabs" role="tablist">
                        <li role="presentation" class="active"><a href="#startlease" aria-controls="startlease" role="tab" data-toggle="tab">Start of lease</a></li>
                        <li role="presentation"><a href="#endlease" aria-controls="profile" role="tab" data-toggle="tab">End of lease</a></li>
                    </ul>
                    <!-- Tab panes -->
                    <form class="col-md-10 offset-md-2" onsubmit="return check_bonds()" method="post" action="lease/save_bonds" enctype="multipart/form-data">
                        <div class="tab-content">
                            <div role="tabpanel" class="tab-pane active" id="startlease">
                                <h3>Bond Received</h3>
                                <div class="form-group col-md-12">
                                    <div class=" col-sm-4"><label for="bondamount">Bond amount</label></div>
                                    <div class=" col-sm-8"><input type="number" readonly name="bond_amount" value="<?php if($bond_info){echo $bond_info[0]['bond_amount'];}?>" class="form-control" id="bondamount" placeholder="$0.00"></div>
                                </div>
                                <div class="form-group col-md-12">
                                    <div class=" col-sm-4"><label for="bondamount">Date lodged</label></div>
                                    <div class=" col-sm-8"><div class="input-group">
                                        <input class="form-control" type="text"  id="bond_start_date" name="lodge_start_date" value="<?php if($bond_info && $bond_info[0]['lodge_start_date']!=null){echo date("M d, Y", strtotime($bond_info[0]['lodge_start_date']));}else{echo date("M d, Y");}?>" readonly />
                                        <span class="input-group-addon"><i class="glyphicon glyphicon-calendar"></i></span>
                                    </div></div>
                                </div>
                                <div class="form-group col-md-12">
                                    <div class=" col-sm-4"> <label for="bondamount">Lodged with</label></div>
                                    <div class=" col-sm-8"><input type="text" name="lodged_with" readonly value="<?php if($bond_info){echo $bond_info[0]['lodged_with'];}?>" class="form-control" id="lodgedwith" placeholder=""></div>
                                </div>
                                <div class="form-group col-md-12">
                                    <div class=" col-sm-4"><label for="bondamount">Additional notes</label></div>
                                    <div class=" col-sm-8"><textarea class="form-control col-md-12" readonly name="bond_start_add_note" rows="3"><?php if($bond_info){echo $bond_info[0]['bond_start_add_note'];}?></textarea></div>
                                </div> 
                                <div class="form-group clear" style="display: none">
                                    <div class=" col-sm-4"> <label for="bondamount">Upload bond documents</label></div>
                                    <div class="col-sm-8 ">
                                        <div class="form-group inputDnD">
                                           <div class="dropzone" >
                                               <span style="display: none" class="my-dz-message pull-right">
                                                    <h3>Click Here to upload another file</h3>
                                                </span>
                                            <div class="dz-message" >
                                              <h3> Click Here to upload your files</h3>
                                            </div>
                                          </div>
                                          <div class="previews" id="preview"></div>
                                        </div>
                                        <div>* Max 20 MB per file</div>
                                        <ul style="margin-left:3%;">
                                          <?php foreach($bond_img_recieve_info as $row){?>
                                            <li><a target="_blank" href="uploads/lease/<?=$row['recieve_image']?>"><?=$row['recieve_image']?></a></li>
                                          <?php }?>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                            <div role="tabpanel" class="tab-pane" id="endlease">
                                <h3>Bond allocation at termination of the tenancy</h3>
                                <div class="form-group col-md-12">
                                    <div class=" col-sm-4"><label for="bondamount">Amount to be refunded to tenant</label></div>
                                    <div class=" col-sm-8"><input type="number" readonly name="bond_tenant_return" value="<?php if($bond_info){echo $bond_info[0]['bond_tenant_return'];}?>" class="form-control" id="bondamount" placeholder="$0.00"></div>
                                </div>
                                <div class="form-group col-md-12">
                                    <div class=" col-sm-4"><label for="bondamount">Amount to be refunded to landlord</label></div>
                                    <div class=" col-sm-8"><input type="number" readonly name="bond_lanlord_keep" value="<?php if($bond_info){echo $bond_info[0]['bond_lanlord_keep'];}?>" class="form-control" id="bondamount" placeholder="$0.00"></div>
                                </div>
                                <div class="form-group col-md-12">
                                    <div class=" col-sm-4"><label for="bondamount">Date lodged</label></div>
                                    <div class=" col-sm-8"><div class="input-group date">
                                        <input class="form-control" id="bond_end_date" name="lodge_return_date" value="<?php if($bond_info && $bond_info[0]['lodge_return_date']!=null){echo date("M d, Y", strtotime($bond_info[0]['lodge_return_date']));}else{echo date("M d, Y");}?>" type="text" readonly />
                                        <span class="input-group-addon"><i class="glyphicon glyphicon-calendar"></i></span>
                                    </div></div>
                                </div>
                                <div class="form-group col-md-12">
                                    <div class=" col-sm-4"><label for="bondamount">Expenses landlord is claiming</label></div>
                                    <div class=" col-sm-8"><textarea class="form-control col-md-12" readonly rows="3" name="bond_return_add_note" ><?php if($bond_info){echo $bond_info[0]['bond_return_add_note'];}?></textarea></div>
                                </div> 
                                <div class="form-group clear"  style="display: none">
                                    <div class=" col-sm-4"> <label for="bondamount">Evidence of bond refund</label></div>
                                    <div class="col-sm-8 ">
                                        <div class="form-group inputDnD">
                                           <div class="dropzone dropzone_2" >
                                               <span style="display: none" class="my-dz-message pull-right">
                                                    <h3>Click Here to upload another file</h3>
                                               </span>
                                            <div class="dz-message" >
                                              <h3> Click Here to upload your files</h3>
                                            </div>
                                          </div>
                                          <div class="preview_2" id="preview_1"></div>
                                        </div>
                                        <div>* Max 20 MB per file</div>
                                        <ul style="margin-left:3%;">
                                          <?php foreach($bond_img_return_info as $row){?>
                                            <li><a target="_blank" href="uploads/lease/<?=$row['return_image']?>"><?=$row['return_image']?></a></li>
                                          <?php }?>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                            <div class="modal-footer clear">
                                <br/><br/>
                                <!--<button type="button" class="btn btn-light">Close</button>-->
                                <input type="hidden" name="property_id" value="<?=$property_id;?>">
                                <input type="hidden" name="lease_id" value="<?=$lease_detail[0]['lease_id'];?>">
                                <input type="hidden" name="bond_id" value="<?php if($bond_info){echo $bond_info[0]['bond_id'];}?>">
                                <!--<button type="submit" class="btn btn-primary"> Save change</button>-->
                                <br/><br/>
                            </div>
                        </div>
                        <div style="clear: both;"></div>
                    </form>
                </div>
            </div>
            
        </div>    
    </div>
</div>
</div>
</div>

<script src="//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
<script type="text/javascript" src="assets/js/dropzone.min.js"></script>
<script type="text/javascript">
    Dropzone.autoDiscover = false;
    var foto_upload = new Dropzone(".dropzone", {
      url: "<?php echo base_url('lease/property_photo') ?>",
      maxFilesize: 20,
      method: "post",
      acceptedFiles: ".jpg,.jpeg,.png,.gif,.doc,.docx,.xls,.pdf",
      paramName: "userfile",
      dictInvalidFileType: "This File Type Not Supported",
      addRemoveLinks: true,
      init: function () {
        var count = 0;
        thisDropzone = this;
        this.on("success", function (file, json) {
          var obj = json;
          $('.previews').
          append(
            "<input type='hidden' name='recieve_image[]' value='" + obj + "'>\n\
            <input type='hidden' name='recieve_width[]' value='" + file.width + "'>\n\
            <input type='hidden' name='recieve_height[]' value='" + file.height + "'>"
            );
            $(".my-dz-message").show();
        });
      }
    });
    var foto_upload_new = new Dropzone(".dropzone_2", {
      url: "<?php echo base_url('lease/property_photo') ?>",
      maxFilesize: 20,
      method: "post",
      acceptedFiles: ".jpg,.jpeg,.png,.gif,.doc,.docx,.xls,.pdf",
      paramName: "userfile",
      dictInvalidFileType: "This File Type Not Supported",
      addRemoveLinks: true,
      init: function () {
        var count = 0;
        thisDropzone = this;
        this.on("success", function (file, json) {
          var obj = json;
          $('.preview_2').
          append(
            "<input type='hidden' name='return_image[]' value='" + obj + "'>\n\
            <input type='hidden' name='return_width[]' value='" + file.width + "'>\n\
            <input type='hidden' name='return_height[]' value='" + file.height + "'>"
            );        
        });
      }
    });
</script>
<script type="text/javascript">
  function check_bonds()
  {
    var ok = true;
    var start_date = $('#bond_start_date').val();
    var end_date = $('#bond_end_date').val();
    if(new Date(start_date) > new Date(end_date))
    {
      alert('Date lodged for Bonds Receivig should be less than Bonds Returning...');
      $('#bond_start_date').closest('div').addClass("has-error");
      $('#bond_end_date').closest('div').addClass("has-error");
      ok = false;
    }
    else
    {
      ok = true;
    }
    return ok;
  }
</script>


<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<script type="text/javascript" src="https://code.jquery.com/ui/1.12.0/jquery-ui.min.js"></script>
<script type="text/javascript" src="assets/js/daterangepicker.jQuery.js"></script>
<script type="text/javascript">
$(function(){
    $('#bond_start_date, #bond_end_date').datepicker({
        dateFormat: 'M d, yy'
    });
});
</script>
<script src="assets/js/bootstrap.min.js"></script>
<!-- <script src="assets/js/custom.js"></script> -->
<style>
    .ui-widget-header{ color: #fff;}
    .ui-daterangepickercontain{ top: 511px;}
    .ui-widget-header{ background: #337ab7; border: #1f496d;}
    .ui-state-default, .ui-widget-content .ui-state-default, .ui-widget-header .ui-state-default{     color: #337ab7;}
    .ui-state-active, .ui-widget-content .ui-state-active, .ui-widget-header .ui-state-active { border: 1px solid #1f496d;}
</style>
</body>
</html>