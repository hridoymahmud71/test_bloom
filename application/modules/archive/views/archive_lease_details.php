<?php $this->load->view('front/headlink'); ?>
    <style>
        #signed_lease_agreement {
            display: none;
        }
        #uploaded_file {
            display: none;
        }
        #remove_file{
            display: none;
        }
    </style>
<?php $this->load->view('front/archived_top_menu'); ?>
<div class="row">
    <form class="container" onsubmit="return chk_lease_detail()" action="lease/update_lease" method="post" enctype="multipart/form-data">
        <?php $this->load->view('front/head_nav'); ?>
        <h4>Lease: <?=$lease_detail[0]['lease_name'];?><!-- (<?php /*echo ((abs(strtotime(date('Y-m-d'))-strtotime($lease_detail[0]['lease_end_date']))/86400))*/?> days to go) <a href="#" data-toggle="modal" data-target="#myModal"><span class="glyphicon glyphicon-cog" aria-hidden="true"></span></a>-->
        </h4>
        <div class="row" >
            <div class="col-xs-12">
                <div class="col-md-12 well text-center">
                    <h1 class="text-center"> Lease & Rent Details for the property at <span><?= $this->utility_model->getFullPropertyAddress($property_info[0]['property_id'])?></span></h1>
                    <div class="row">
                        <br/> 
                        <h4 for="inputLeaseName" class="col-sm-2 control-label">Lease Name</h4>
                        <div class="col-sm-10">
                            <input type="text" readonly name="lease_name" value="<?=$lease_detail[0]['lease_name'];?>" class="form-control" id="inputLeaseName" placeholder="Lease Name">
                        </div>
                    </div>
                    <div class="row">
                        <br/> 
                        <h4 for="inputLeaseName" class="col-sm-2 control-label">Lease dates</h4>
                        <label class="col-sm-1 pull-left ss_exta_lebal">from:</label>  
                        <div class="col-sm-4">
                            <input class="form-control" readonly type="text" id="rangeBa" name="lease_start_date" value="<?=date("jS F, Y", strtotime($lease_detail[0]['lease_start_date']));?>">
                        </div>
                        <label class="col-sm-1 pull-left ss_exta_lebal">to:</label>
                        <div class="col-sm-4">
                            <input class="form-control" readonly type="text" id="rangeBb" name="lease_end_date" value="<?=date("jS F, Y", strtotime($lease_detail[0]['lease_end_date']));?>">
                        </div>
                    </div>   
                    <div class="row">
                        <br/> 
                        <p class="text-left" style=" padding-left:30px;">
                            <label>Add signed lease agreement</label> 
                            <span class="file_upload_wrapper" style="display: inline;">
                                <input id="signed_lease_agreement" name="lease_file" type="file"
                                style="position: relative; left: 0px; ">
                                <?php
                                $uploaded_file_exists = false;
                                $uploaded_file_url = false;
                                $uploaded_file_name = false;
                                $uploaded_file_href = ' href="#" ';
                                if (($lease_detail)) {
                                    $uploaded_file_exists = true;
                                    if ($lease_detail[0]['lease_file'] != null && $lease_detail[0]['lease_file'] != false && trim($lease_detail[0]['lease_file']) != '') {
                                        $uploaded_file_name = $lease_detail[0]['lease_file'];
                                        $uploaded_file_href = ' href="' .base_url(). '/uploads/image_and_file/file/' . $lease_detail[0]['lease_file'] . '" ';
                                    }
                                } ?>

                            </span>
                                <span>
                                    <a download="download"
                                    id="uploaded_file" <?= $uploaded_file_href ?> ><?= $uploaded_file_name ? $uploaded_file_name : '' ?>
                                </a>
                                <!--<i id="remove_file" class="glyphicon glyphicon-remove"></i>-->
                            </span>
                        </p>
                    </div>  
                    <div class="row text-left">
                        <br/> 
                        <h4 style="padding-left:30px;">Rent payments</h4>
                        <div class="row">
                            <div class="col-md-4">
                                <label class="col-md-5 ss_exta_lebal text-center">How often?</label>
                                <div class="col-md-7">
                                    <select disabled class="form-control" id="inputday" name="lease_pay_type">
                                        <option <?php if($lease_detail[0]['lease_pay_type']==1){echo "selected";}?> id="Weekly" value="1" selected="selected">Weekly</option>
                                        <option <?php if($lease_detail[0]['lease_pay_type']==2){echo "selected";}?> id="Fortnightly" value="2">Fortnightly</option>
                                        <option <?php if($lease_detail[0]['lease_pay_type']==3){echo "selected";}?> id="4Weekly" value="3">4 Weekly</option>
                                        <option <?php if($lease_detail[0]['lease_pay_type']==4){echo "selected";}?> id="Monthly" value="4">Monthly</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <label class="col-md-5 ss_exta_lebal"> How much per period?</label>
                                <div class="col-md-7">
                                    <input type="text" readonly class="form-control" name="lease_per_period_payment" value="<?=$lease_detail[0]['lease_per_period_payment'];?>" id="howmuchperperiod" placeholder="$0">
                                </div>
                            </div>
                            <div class="col-md-4">
                                <label class="col-md-2 ss_exta_lebal">Payday:</label>
                                <div class="col-md-7">
                                    <select disabled class="form-control" id="inputhowOftenday" <?php if(($lease_detail)){if($lease_detail[0]['lease_pay_type'] ==4){echo "style='display: none;'";}else{echo "name='lease_pay_day'";}}else{echo "name='lease_pay_day'";}?>>
                                        <option <?php if($lease_detail[0]['lease_pay_day']==1){echo "selected";}?> id="Monday" value="1" selected="selected">Monday</option>
                                        <option <?php if($lease_detail[0]['lease_pay_day']==2){echo "selected";}?> id="Tuesday" value="2">Tuesday</option>
                                        <option <?php if($lease_detail[0]['lease_pay_day']==3){echo "selected";}?> id="Wednesday" value="3">Wednesday</option>
                                        <option <?php if($lease_detail[0]['lease_pay_day']==4){echo "selected";}?> id="Thursday" value="4">Thursday</option>
                                        <option <?php if($lease_detail[0]['lease_pay_day']==5){echo "selected";}?> id="Friday" value="5">Friday</option>
                                        <option <?php if($lease_detail[0]['lease_pay_day']==6){echo "selected";}?> id="Saturday" value="6">Saturday</option>
                                        <option <?php if($lease_detail[0]['lease_pay_day']==7){echo "selected";}?> id="Sunday" value="7">Sunday</option>
                                    </select>
                                    <select readonly class="form-control" id="inputmonthDetail" <?php if(($lease_detail)){if($lease_detail[0]['lease_pay_type'] !=4){echo "style='display: none;'";}else{echo "name='lease_pay_day'";}}else{echo "style='display: none;'";}?>>
                                        <?php for($i=1;$i<=28;$i++){?>
                                            <option <?php if($lease_detail[0]['lease_pay_day']==$i){echo "selected";}?> value="<?=$i;?>">
                                                <?=$i;?>
                                                <?php if($i==1){echo 'st';}elseif($i==2){echo 'nd';}elseif($i==3){echo 'rd';}else{echo "th";}?> of the month
                                            </option>
                                        <?php }?>
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-12" style="padding-left:30px; display:none">
                                <h3>Which tenant(s) actually pay you the rent?</h3>
                                <table class="table p-summary" border="0" cellspacing="0" cellpadding="4">
                                    <tbody>
                                        <tr class="table_tenants_title">
                                            <th scope="col" class="pull-right">Name</th>
                                            <th scope="col"></th>
                                            <th scope="col">Share Paid</th>
                                            <th scope="col">Pay Method</th>
                                        </tr>
                                        <?php foreach($all_tenant as $key=>$row){?>
                                        <tr class="tenant_payment_detail">
                                            <td class="pull-right">
                                                <input class="form-control first_name_checkbox custom-checkbox" style="margin-bottom: 9px;width:40px;" type="checkbox" id="payment_status<?=$key;?>" name="payment_status[]" value="<?=$key;?>" <?php if($row['payment_status']==1){echo 'checked="checked"';}?>>
                                                <input type="hidden" name="payment_status[]" value="0" />
                                            </td>
                                            <td>
                                                <p style="display:inline-block; margin: 0px;" class="first_name_text"><?=$row['user_fname']?> <?=$row['user_lname']?></p>
                                            </td>
                                            <td>
                                                <input class="form-control shared_amount_cls" type="text" id="rent_amount_<?=$key;?>" name="share_paid_amount[]" value="<?=$row['share_paid_amount']?>">
                                            </td>
                                            <td>
                                                <select class="form-control" id="payment_method_<?=$key;?>" name="payment_method[]">
                                                    <option <?php if($row['payment_method']==1){echo "selected";}?> value="1">Bank Transfer (EFT)</option>
                                                    <option <?php if($row['payment_method']==2){echo "selected";}?> value="2">Cash</option>
                                                    <option <?php if($row['payment_method']==3){echo "selected";}?> value="3">Cheque</option>
                                                    <option <?php if($row['payment_method']==4){echo "selected";}?> value="4">Credit Card</option>
                                                    <option <?php if($row['payment_method']==5){echo "selected";}?> value="5">Other method</option>
                                                </select>
                                            </td>
                                            <input type="hidden" name="tenant_id[]" value="<?=$row['tenant_id'];?>">
                                        </tr>
                                        <?php }?>
                                    </tbody>
                                </table>
                            </div>
                        </div>    
                    </div>
                    <div>
                        <h4>Total paid per period: $<span id="total_amount" class="ss_price_text"><?=$lease_detail[0]['lease_per_period_payment'];?></span></h4>
                    </div>
                    <br/><br/>
                    <input type="hidden" name="property_id" value="<?=$property_id;?>">
                    <input type="hidden" name="lease_id" value="<?=$lease_detail[0]['lease_id'];?>">
                    <!--<input type="submit" id="sub_btn" value="Save Changes" class="print_ledgers btn btn-primary btn-lg">
                    <a href="Dashboard/<?/*=$property_id;*/?>" class="btn btn-light btn-lg mark_paid">
                    Back to Dashboard</a>-->
                    <br/>
                    <br/>
                </div>
            </div>
        </div>
    </form>
</div>
<div class="container">
  <div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Select a Lease or add a new lease</h4>
        </div>
        <form action="lease/lease_details/<?=$property_id;?>" method="post">
          <div class="modal-body">
            <h4>Lease Option</h4>
            <select class="form-control" name="lease_id">
              <option value="new_lease">Add a new lease</option>
              <?php foreach($all_lease as $row){?>
              <option <?php if($row['lease_id']==$lease_detail[0]['lease_id']){echo "selected";}?> value="<?=$row['lease_id'];?>"><?=$row['lease_name'];?></option>
              <?php }?>
            </select>
          </div>
          <div class="modal-footer">
            <input type="submit" class="btn btn-success" value="Confirm" name="">
          </div>
        </form>
      </div>
    </div>
  </div>
</div>
<script src="//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>

<script>
    $( document ).ready(function() {
        var uploaded_file_exist = <?php if($uploaded_file_exists) { ?> true <?php } else { ?> false <?php } ?> ;
        if (uploaded_file_exist) {
            show_file_anchor_not_input();
        } else {
            show_file_input_not_anchor();
        }
        $('#remove_file').on('click', function () {
            show_file_input_not_anchor();
        });
    });
    function show_file_input_not_anchor() {
        $('#signed_lease_agreement').show();
        $('#uploaded_file').hide();
        $('#remove_file').hide();
    }
    function show_file_anchor_not_input() {
        $('#signed_lease_agreement').hide();
        $('#uploaded_file').show();
        $('#remove_file').show();
    }
</script>

<script type="text/javascript">
    $( document ).ready(function() {
        var selectedtime = "<?=$lease_detail[0]['lease_pay_type'];?>";
        if(selectedtime == 4)
        {
            $('#inputhowOftenday').hide();
            $('#inputhowOftenday').removeAttr('name');
            $('#inputmonthDetail').show();
            $('#inputmonthDetail').attr('name', 'lease_pay_day');
        }
        else
        { 
            $('#inputhowOftenday').show();
            $('#inputhowOftenday').attr('name', 'lease_pay_day');
            $('#inputmonthDetail').hide();
            $('#inputmonthDetail').removeAttr('name');
        }
    });
    $( "#inputday" ).change(function() {

      var selectedtime = $("#inputday").val();
      if(selectedtime == 4)
      {
        $('#inputhowOftenday').hide();
        $('#inputhowOftenday').removeAttr('name');
        $('#inputmonthDetail').show();
        $('#inputmonthDetail').attr('name', 'lease_pay_day');
      }
      else
      { 
        $('#inputhowOftenday').show();
        $('#inputhowOftenday').attr('name', 'lease_pay_day');
        $('#inputmonthDetail').hide();
        $('#inputmonthDetail').removeAttr('name');
      }
    });
</script>
<script type="text/javascript">
    var how_much_per_period = $('#howmuchperperiod').val();
    $(document).on("keyup", "#howmuchperperiod", function () {
        $('#total_amount').html($('#howmuchperperiod').val());
    });
    $(document).on("change", ".first_name_checkbox", function () {
        var how_much_per_period = $('#howmuchperperiod').val();
        var all_sum = 0;
        $(".first_name_checkbox:checked").each(function () {
            var curr_amount = $(this).closest('.tenant_payment_detail').find('.shared_amount_cls').val();
            all_sum = parseInt(all_sum) + parseInt(curr_amount);
        });
        /*if(all_sum !=$('#howmuchperperiod').val())
        {
            $('.ss_price_text').closest('div').addClass('ss_price_text');
            $('#total_amount').css("color", "red");
            $("#sub_btn").attr("disabled", true);
        }
        else
        {
            $('.ss_price_text').closest('div').removeClass('ss_price_text');
            $('#total_amount').css("color", "green");
            $("#sub_btn").attr("disabled", false);
        }*/

        /*if($(this).is(":checked"))
        {
            $(this).next('input').val("1");
        }
        else
        {
            $(this).next('input').val("0");
        }*/
    });
    $(document).on("keyup", ".shared_amount_cls", function () {
        var all_sum = 0;
        $(".first_name_checkbox:checked").each(function () {
            var curr_amount = $(this).closest('.tenant_payment_detail').find('.shared_amount_cls').val();
            all_sum = parseInt(all_sum) + parseInt(curr_amount);
            if(all_sum==NaN)
            {
                all_sum = 0;
            }

        });
            // $('#total_amount').text(all_sum);
        /*if(all_sum !=$('#howmuchperperiod').val())
        {
            $('.ss_price_text').closest('div').addClass('ss_price_text');
            $('#total_amount').css("color", "red");
            $("#sub_btn").attr("disabled", true);
        }
        else
        {
            $('.ss_price_text').closest('div').removeClass('ss_price_text');
            $('#total_amount').css("color", "green");
            $("#sub_btn").attr("disabled", false);
        }*/

        /*if($(this).is(":checked"))
        {
            $(this).next('input').val("1");
        }
        else
        {
            $(this).next('input').val("0");
        }*/
    });
    $(document).ready(function () {
        var all_sum = 0;
        /*$(".first_name_checkbox:checked").each(function () {
            var curr_amount = $(this).closest('.tenant_payment_detail').find('.shared_amount_cls').val();
            all_sum = parseInt(all_sum) + parseInt(curr_amount);
            if(all_sum==NaN)
            {
                all_sum = 0;
            }

        });*/
            // $('#total_amount').text(all_sum);
        /*if(all_sum !=$('#howmuchperperiod').val())
        {
            $('.ss_price_text').closest('div').addClass('ss_price_text');
            $('#total_amount').css("color", "red");
            $("#sub_btn").attr("disabled", true);
        }
        else
        {
            $('.ss_price_text').closest('div').removeClass('ss_price_text');
            $('#total_amount').css("color", "green");
            $("#sub_btn").attr("disabled", false);
        }*/
        $('#total_amount').css("color", "green");
        /*$('.first_name_checkbox').each(function(){
            if($(this).is(":checked"))
            {
                $(this).next('input').val("1");
            }
            else
            {
                $(this).next('input').val("0");
            }
        });*/
    });
    function chk_lease_detail()
    {
        var ok = true;
        var input_Lease_Name = $('#inputLeaseName').val();
        var how_much_per_period = $('#howmuchperperiod').val();
        var payment_total_amount = $('#total_amount').text();
        if(how_much_per_period == '' || how_much_per_period == null)
        {
            $('#howmuchperperiod').closest('div').addClass('has-error');
            ok = false;
        }
        else
        {
            $('#howmuchperperiod').closest('div').removeClass('has-error');
            ok = true;
        }
        if(input_Lease_Name=='' || input_Lease_Name==null)
        {
            $('#inputLeaseName').closest('div').addClass('has-error');
            ok = false;
        }
        else
        {
            $('#inputLeaseName').closest('div').removeClass('has-error');
            ok = true;
        }
        /*if(how_much_per_period != payment_total_amount)
        {
            alert('Per period amount and Total paid per period not matched');
            $('.ss_price_text').closest('div').addClass('ss_price_text');
            $('#total_amount').css("color", "red");
            ok = false;
        }
        else
        {
            $('.ss_price_text').closest('div').removeClass('ss_price_text');
            $('#total_amount').css("color", "green");
            ok = true;
        }*/
        var all_sum = 0;
        /*$(".first_name_checkbox:checked").each(function () {
            var curr_amount = $(this).closest('.tenant_payment_detail').find('.shared_amount_cls').val();
            all_sum = parseInt(all_sum) + parseInt(curr_amount);
        });*/
        /*if(all_sum !=$('#howmuchperperiod').val())
        {
            $('.ss_price_text').closest('div').addClass('ss_price_text');
            $('#total_amount').css("color", "red");
            $("#sub_btn").attr("disabled", true);
            ok = false;
        }
        else
        {
            $('.ss_price_text').closest('div').removeClass('ss_price_text');
            $('#total_amount').css("color", "green");
            $("#sub_btn").attr("disabled", false);
            ok = true;
        }*/

        /*$(".first_name_checkbox:checked").each(function () {
            var sharing_amount = $(this).closest('.tenant_payment_detail').find('.shared_amount_cls').val();
            if(sharing_amount==0 || sharing_amount=='')
            {
                $(this).closest('.tenant_payment_detail').find('.shared_amount_cls').closest('td').addClass('has-error');
                ok = false;
            }
            else
            {
                $(this).closest('.tenant_payment_detail').find('.shared_amount_cls').closest('td').removeClass('has-error');
                ok = true;
            }
        });*/
        return ok;
    }

</script>
<script type="text/javascript" src="https://code.jquery.com/ui/1.12.0/jquery-ui.min.js"></script>
<script type="text/javascript" src="assets/js/daterangepicker.jQuery.js"></script>
<script type="text/javascript">
$(function(){
    $('#rangeBa, #rangeBb').daterangepicker();
});
</script>
<script src="assets/js/bootstrap.min.js"></script>
<!-- <script src="assets/js/custom.js"></script> -->
<style>
    .ui-widget-header{ color: #fff;}
    .ui-daterangepickercontain{ top: 511px;}
    .ui-widget-header{ background: #337ab7; border: #1f496d;}
    .ui-state-default, .ui-widget-content .ui-state-default, .ui-widget-header .ui-state-default{     color: #337ab7;}
    .ui-state-active, .ui-widget-content .ui-state-active, .ui-widget-header .ui-state-active { border: 1px solid #1f496d;}
</style>
</body>
</html>