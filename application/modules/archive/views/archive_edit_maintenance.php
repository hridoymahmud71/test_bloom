<?php $this->load->view('front/headlink'); ?>
<div class="dashbord_poperty">
    <?php $this->load->view('front/archived_top_menu'); ?>
    <div class="container">
        <?php $this->load->view('front/head_nav'); ?>
        <div class="row">
            <?php
            $time = strtotime($result[0]['create_at']);
            $time2 = strtotime($result[0]['updated_at']);
            $created_at = date("M d Y", $time);
            $updated_at = date("M d Y", $time2);

            if (!$time || $time < 0) {
                $time = 0;
            }
            if (!$time2 || $time2 < 0) {
                $time2 = 0;
            }

            $created_time = $myFormatForView = date("g:i A", $time);
            $updated_time = $myFormatForView = date("g:i A", $time2);
            ?>
            
            <div class="ss_container">
                <h2><?php echo $result[0]['maintenance_issue']; ?></h2>
                <h5> Lease Name: <strong><?= $this->session->userdata('lease_name'); ?></strong></h5>
                <h5> Created By: <strong><?php echo $result[0]['user_fname']; ?></strong> | <?php echo $created_at; ?>
                    at <?php echo $created_time; ?> | <span> <a style="cursor:pointer;" data-toggle="modal"
                                                                data-target="#myModal"> View History Log </a> </span>
                </h5>
                <?php if ($time2) { ?> <h5> Latest Update By: <strong><?php echo $result[0]['user_fname']; ?></strong>
                    | <?php echo $updated_at; ?> at <?php echo $updated_time; ?> </h5> <?php } ?>
                <form action="maintenance/update_maintenance" method="post" enctype="multipart/form-data" id="myForm">
                    <div class="row">
                        <div class="col-md-8">
                            <div class="form-group">
                                <br/><br/>
                                <h5 for="inputLeaseName" class="col-sm-4 control-label">Status</h5>
                                <div class="col-sm-8">
                                    <select class="form-control" id="inputStatus" name="maintenance_status" readonly required="">
                                        <option value="1" <?php if ($result[0]['maintenance_status'] == 1) {
                                            echo 'selected';
                                        } ?>>New
                                        </option>
                                        <option value="2" <?php if ($result[0]['maintenance_status'] == 2) {
                                            echo 'selected';
                                        } ?>>Open
                                        </option>
                                        <option value="4" <?php if ($result[0]['maintenance_status'] == 4) {
                                            echo 'selected';
                                        } ?>>Closed
                                        </option>
                                        <option value="5" <?php if ($result[0]['maintenance_status'] == 5) {
                                            echo 'selected';
                                        } ?>>Archive
                                        </option>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <br/><br/>
                                <h5 for="inputLeaseName" class="col-sm-4 control-label">Urgency</h5>
                                <div class="col-sm-8">
                                    <select class="form-control" id="inputUrgency" name="maintenance_urgency" readonly
                                            required="">
                                        <option value="Low Priority" <?php if ('Low Priority' == $result[0]['maintenance_urgency']) {
                                            echo 'selected';
                                        } ?>>Low Priority
                                        </option>
                                        <option value="Within a Week" <?php if ('Within a Week' == $result[0]['maintenance_urgency']) {
                                            echo 'selected';
                                        } ?>>Within a Week
                                        </option>
                                        <option value="High Priority" <?php if ('High Priority' == $result[0]['maintenance_urgency']) {
                                            echo 'selected';
                                        } ?>>High Priority
                                        </option>
                                        <option value="Urgent" <?php if ('Urgent' == $result[0]['maintenance_urgency']) {
                                            echo 'selected';
                                        } ?>>Urgent
                                        </option>
                                    </select>
                                </div>
                            </div>
                            <div class="clear"></div>
                            <div class="form-group">
                                <br/>
                                <h5 for="inputLeaseName" class="col-sm-4 control-label">What's the issue?<br/>
                                    <small>(Title) - 30 characters max.</small>
                                </h5>
                                <div class="col-sm-8">
                                    <input type="text" name="maintenance_issue" maxlength="30" readonly
                                           placeholder="Broken Pipe in Kitchen" name="title" class="form-control"
                                           value="<?php echo $result[0]['maintenance_issue']; ?>" required>
                                </div>
                            </div>
                            <div class="clear"></div>
                            <div class="form-group">
                                <br/>
                                <h5 for="inputLeaseName" class="col-sm-4 control-label">Property Address</h5>
                                <div class="col-sm-8">
                                    <input type="text" name="maintenance_location" maxlength="30" placeholder=" " readonly
                                           name="title" class="form-control"
                                           value="<?php echo $result[0]['maintenance_location']; ?>" required>
                                </div>
                            </div>
                            <div class="clear"></div>
                            <div class="form-group">
                                <br/>
                                <h5 for="inputLeaseName" class="col-sm-4 control-label">More details<br/></h5>
                                <div class="col-sm-8">
                                    <textarea class="form-control" name="maintenance_desc" readonly
                                              rows="3"><?php echo $result[0]['maintenance_desc']; ?></textarea>
                                </div>
                            </div>
                            <div class="clear"></div>
                            <?php
                            foreach ($result2 as $data2) { ?>
                                <div style="background-color: white; padding: 5px; border-bottom: 1px solid black; margin-left: 10px; padding-left: 25px; margin-top: 10px;">
                                    <h5> <?php echo $data2['comment_by']; ?> </h5>
                                    <h5> <?php echo $data2['created_date']; ?> </h5>
                                    <h5> <?php echo $data2['created_time']; ?> </h5>
                                    <h5> <?php echo $data2['maintenance_comments']; ?></h5>
                                    <?php foreach ($result3 as $data3) {
                                        if (($data3['maintenance_comments_id']) == ($data2['maintenance_comments_id'])) {
                                            $last3chars = substr($data3['maintenance_comment_file'], -3); ?>
                                            <?php if (($last3chars) == 'jpg' || ($last3chars) == 'peg' || (strtolower($last3chars)) == 'png') { ?>
                                                <a href="<?php echo base_url(); ?>uploads/maintenance_file/<?php echo $data3['maintenance_comment_file']; ?>">
                                                    <img style="height: 100px; width: 100px;"
                                                         src="<?php echo base_url(); ?>uploads/maintenance_file/<?php echo $data3['maintenance_comment_file']; ?>"></a>
                                            <?php } ?>
                                            <?php if (($last3chars) == 'doc' || ($last3chars) == 'ocx') { ?>
                                                <a href="<?php echo base_url(); ?>uploads/maintenance_file/<?php echo $data3['maintenance_comment_file']; ?>">
                                                    <img style="height: 100px; width: 100px;"
                                                         src="<?php echo base_url(); ?>assets/img/docxLogo.png ?>"></a>
                                            <?php } ?>
                                            <?php if (($last3chars) == 'pdf') { ?>
                                                <a href="<?php echo base_url(); ?>uploads/maintenance_file/<?php echo $data3['maintenance_comment_file']; ?>">
                                                    <img style="height: 100px; width: 100px;"
                                                         src="<?php echo base_url(); ?>assets/img/pdfLogo.png ?>"></a>
                                            <?php } ?>
                                            <?php if (($last3chars) == 'csv' || ($last3chars) == 'svx') { ?>
                                                <a href="<?php echo base_url(); ?>uploads/maintenance_file/<?php echo $data3['maintenance_comment_file']; ?>">
                                                    <img style="height: 100px; width: 100px;"
                                                         src="<?php echo base_url(); ?>assets/img/excelLogo.png ?>"></a>
                                            <?php } ?>
                                            <?php if (($last3chars) == 'mp4' || ($last3chars) == 'mpg') { ?>
                                                <a href="<?php echo base_url(); ?>uploads/maintenance_file/<?php echo $data3['maintenance_comment_file']; ?>">
                                                    <img style="height: 100px; width: 100px;"
                                                         src="<?php echo base_url(); ?>assets/img/videoLogo.png ?>"></a>
                                            <?php } ?>
                                        <?php } ?>
                                    <?php } ?>
                                </div>
                            <?php } ?>
                            <div class="clear"></div>
                            <div style="display: none" class="form-group">
                                <br/>
                                <h5 for="inputComment" class="col-sm-4 control-label">Add Comments<br/></h5>
                                <div class="col-sm-8">
                                    <textarea class="form-control" name="maintenance_comment" rows="3"></textarea>
                                </div>
                            </div>
                            <div style="display: none" class="form-group">
                                <h5 for="inputCmntFile" class="col-sm-4 control-label">
                                    Upload photos, videos, documents in relation to the maintenance:<br/>
                                </h5>
                                <div class="col-sm-8">
                                    <div class="inputDnD">
                                        <br>
                                        <div class="dropzone">
                                            <div class="dz-message">
                                                <h3>Upload Files Here</h3>
                                            </div>
                                        </div>
                                        <div class="previews" id="preview"></div>
                                    </div>
                                    <div>* Max 20 MB per file</div>
                                </div>
                                <input type="hidden" class="form-control" id="" name="property_id"
                                       value="<?php echo $property_id; ?>" required="">
                                <input type="hidden" class="form-control" id="inputLocation" name="maintenance_id"
                                       value="<?php echo $maintenance_id; ?>" required="">
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div style="display: none" class="tenant_access">
                                <p>Which tenants (will) have access to this maintenance issue?</p>
                                <ul class="repeate">Please First send invite to tenants to send maintenance issue.</ul>
                            </div>
                            <?php
                            echo "<br>";
                            foreach ($result as $documents) {
                                if ($documents['image']) {
                                    $last3chars = strtolower(substr($documents['image'], -3));  ?>
                                    <?php if (($last3chars) == 'jpg' || ($last3chars) == 'peg' || ($last3chars) == 'png') { ?>
                                        <a href="<?php echo base_url(); ?>uploads/maintenance_file/<?php echo $documents['image']; ?>">
                                            <img style="height: 100px; width: 100px;"
                                                 src="<?php echo base_url(); ?>uploads/maintenance_file/<?php echo $documents['image'] ?>"></a>
                                    <?php }
                                    if (($last3chars) == 'doc' || ($last3chars) == 'ocx') { ?>
                                        <a href="<?php echo base_url(); ?>uploads/maintenance_file/<?php echo $documents['image']; ?>">
                                            <img style="height: 100px; width: 100px;"
                                                 src="<?php echo base_url(); ?>assets/img/docxLogo.png ?>"></a>
                                    <?php }
                                    if (($last3chars) == 'pdf') { ?>
                                        <a href="<?php echo base_url(); ?>uploads/maintenance_file/<?php echo $documents['image']; ?>">
                                            <img style="height: 100px; width: 100px;"
                                                 src="<?php echo base_url(); ?>assets/img/pdfLogo.png ?>"></a>
                                    <?php }
                                    if (($last3chars) == 'csv' || ($last3chars) == 'svx') { ?>
                                        <a href="<?php echo base_url(); ?>uploads/maintenance_file/<?php echo $documents['image']; ?>">
                                            <img style="height: 100px; width: 100px;"
                                                 src="<?php echo base_url(); ?>assets/img/pdfLogo.png ?>"></a>
                                    <?php }
                                    if (($last3chars) == 'mp4' || ($last3chars) == 'mpg') { ?>
                                        <a href="<?php echo base_url(); ?>uploads/maintenance_file/<?php echo $documents['image']; ?>">
                                            <img style="height: 100px; width: 100px;"
                                                 src="<?php echo base_url(); ?>assets/img/videoLogo.png ?>"></a>
                                    <?php }
                                }
                            } ?>
                        </div>
                        <div class="clear"></div>
                    </div>
                    <div class="clear"></div>
                    <!--<br/><br/>
                    <button type="submit" class="print_ledgers btn btn-primary btn-lg">
                        Save Changes
                    </button>
                    <a href="Allmaintenance/<?/*= $property_id; */?>" class="btn btn-light btn-lg mark_paid">
                        Cancel</a>
                    <br/>
                    <br/>-->
                </form>
            </div>
<div style="clear: both;"></div>
            <!-- Modal -->
            <div class="modal fade" id="myModal" role="dialog">
                <div class="modal-dialog">
                    <!-- Modal content-->
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                            <h3 class="modal-title">Maintenance Issue Log</h3>
                        </div>
                        <div class="modal-body" style="background: #ebebeb;">
                            <?php foreach ($maintenance_log as $row) { ?>
                                <div>
                                    <div style="background: #d7d7d7;padding: 5px 5px;margin: -15px -15px 15px;">
                                        <h5>Changes made to maintenance order at
                                            <strong><?= date("D, M d,Y g:i a", strtotime($row['log_time'])) ?></strong>
                                        </h5>
                                        <h5>Changes made by
                                            <strong><?= $row['user_fname'] . ' ' . $row['user_lname']; ?></strong></h5>
                                    </div>
                                    <div>
                                        <?php if ($row['maintenance_create_status'] == 1) { ?>
                                            <h5><b><?= $row['maintenance_issue'] ?></b> is created</h5>
                                            <table class="table">
                                                <tr>
                                                    <td style="width:30%">Status</td>
                                                    <td>New</td>
                                                </tr>
                                                <tr>
                                                    <td style="width:30%">Urgency</td>
                                                    <td><?= $row['maintenance_urgency'] ?></td>
                                                </tr>
                                                <tr>
                                                    <td style="width:30%">Where</td>
                                                    <td><?= $row['maintenance_location'] ?></td>
                                                </tr>
                                                <?php if ($row['maintenance_desc'] != '') { ?>
                                                    <tr>
                                                        <td style="width:30%">More details</td>
                                                        <td><?= $row['maintenance_desc'] ?></td>
                                                    </tr>
                                                <?php } ?>
                                                <?php if ($row['log_attachement']) { ?>
                                                    <tr>
                                                        <td style="width:30%">Attachment</td>
                                                        <td>
                                                            <?php foreach ($maintenance_log_image as $row2) { ?>
                                                                <?php foreach ($row2 as $row3) { ?>
                                                                    <?php $last3chars = substr($row3['log_image'], -3); ?>
                                                                    <?php if ($row['maintenance_log_id'] == $row3['maintenance_log_id']) { ?>

                                                                        <?php if($row3['log_image']) {?>

                                                                            <?php $last3chars = strtolower(substr($row3['log_image'], -3)) ; ?>

                                                                            <?php if (($last3chars) == 'jpg' || ($last3chars) == 'peg' || ($last3chars == 'png')) { ?>
                                                                                <a href="<?php echo base_url(); ?>uploads/maintenance_file/<?php echo $row3['log_image']; ?>">
                                                                                    <img style="height: 100px; width: 100px;"
                                                                                         src="<?php echo base_url(); ?>uploads/maintenance_file/<?php echo $row3['log_image'] ?>"></a>
                                                                            <?php }
                                                                            if (($last3chars) == 'doc' || ($last3chars) == 'ocx') { ?>
                                                                                <a href="<?php echo base_url(); ?>uploads/maintenance_file/<?php echo $row3['log_image']; ?>">
                                                                                    <img style="height: 100px; width: 100px;"
                                                                                         src="<?php echo base_url(); ?>assets/img/docxLogo.png ?>"></a>
                                                                            <?php }
                                                                            if (($last3chars) == 'pdf') { ?>
                                                                                <a href="<?php echo base_url(); ?>uploads/maintenance_file/<?php echo $row3['log_image']; ?>">
                                                                                    <img style="height: 100px; width: 100px;"
                                                                                         src="<?php echo base_url(); ?>assets/img/pdfLogo.png ?>"></a>
                                                                            <?php }
                                                                            if (($last3chars) == 'csv' || ($last3chars) == 'svx') { ?>
                                                                                <a href="<?php echo base_url(); ?>uploads/maintenance_file/<?php echo $row3['log_image']; ?>">
                                                                                    <img style="height: 100px; width: 100px;"
                                                                                         src="<?php echo base_url(); ?>assets/img/pdfLogo.png ?>"></a>
                                                                            <?php }
                                                                            if (($last3chars) == 'mp4' || ($last3chars) == 'mpg') { ?>
                                                                                <a href="<?php echo base_url(); ?>uploads/maintenance_file/<?php echo $row3['log_image']; ?>">
                                                                                    <img style="height: 100px; width: 100px;"
                                                                                         src="<?php echo base_url(); ?>assets/img/videoLogo.png ?>"></a>
                                                                            <?php }?>

                                                                        <?php }?>

                                                                    <?php } ?>
                                                                <?php } ?>
                                                            <?php } ?>
                                                        </td>
                                                    </tr>
                                                <?php } ?>
                                            </table>
                                        <?php } else { ?>
                                            <table class="table">
                                                <?php if ($row['maintenance_status'] != '') { ?>
                                                    <tr>
                                                        <td style="width:30%">Status</td>
                                                        <td><?php if ($row['maintenance_status'] == 1) {
                                                                echo "new";
                                                            } elseif ($row['maintenance_status'] == 2) {
                                                                echo "Opened";
                                                            } elseif ($row['maintenance_status'] == 4) {
                                                                echo "Closed";
                                                            } else {
                                                                echo "Archived";
                                                            } ?></td>
                                                    </tr>
                                                <?php } ?>
                                                <?php if ($row['maintenance_urgency'] != '') { ?>
                                                    <tr>
                                                        <td style="width:30%">Urgency</td>
                                                        <td><?= $row['maintenance_urgency'] ?></td>
                                                    </tr>
                                                <?php } ?>
                                                <?php if ($row['maintenance_location'] != '') { ?>
                                                    <tr>
                                                        <td style="width:30%">Where</td>
                                                        <td><?= $row['maintenance_location'] ?></td>
                                                    </tr>
                                                <?php } ?>
                                                <?php if ($row['maintenance_desc'] != '') { ?>
                                                    <tr>
                                                        <td style="width:30%">More details</td>
                                                        <td><?= $row['maintenance_desc'] ?></td>
                                                    </tr>
                                                <?php } ?>
                                                <?php if ($row['log_comment'] != '') { ?>
                                                    <tr>
                                                        <td style="width:30%">Comment added</td>
                                                        <td><?= $row['log_comment'] ?></td>
                                                    </tr>
                                                <?php } ?>
                                                <?php if ($row['log_attachement']) { ?>
                                                    <tr>
                                                        <td style="width:30%">Attachment</td>
                                                        <td>
                                                            <?php foreach ($maintenance_log_image as $row2) { ?>
                                                                <?php foreach ($row2 as $row3) { ?>
                                                                    <?php if ($row['maintenance_log_id'] == $row3['maintenance_log_id']) { ?>
                                                                        <?php if ($row3['log_image']) { ?>

                                                                            <?php $last3chars = strtolower(substr($row3['log_image'], -3)) ; ?>

                                                                            <?php if (($last3chars) == 'jpg' || ($last3chars) == 'peg' || ($last3chars == 'png')) { ?>
                                                                                <a href="<?php echo base_url(); ?>uploads/maintenance_file/<?php echo $row3['log_image']; ?>">
                                                                                    <img style="height: 100px; width: 100px;"
                                                                                         src="<?php echo base_url(); ?>uploads/maintenance_file/<?php echo $row3['log_image'] ?>"></a>
                                                                            <?php }
                                                                            if (($last3chars) == 'doc' || ($last3chars) == 'ocx') { ?>
                                                                                <a href="<?php echo base_url(); ?>uploads/maintenance_file/<?php echo $row3['log_image']; ?>">
                                                                                    <img style="height: 100px; width: 100px;"
                                                                                         src="<?php echo base_url(); ?>assets/img/docxLogo.png ?>"></a>
                                                                            <?php }
                                                                            if (($last3chars) == 'pdf') { ?>
                                                                                <a href="<?php echo base_url(); ?>uploads/maintenance_file/<?php echo $row3['log_image']; ?>">
                                                                                    <img style="height: 100px; width: 100px;"
                                                                                         src="<?php echo base_url(); ?>assets/img/pdfLogo.png ?>"></a>
                                                                            <?php }
                                                                            if (($last3chars) == 'csv' || ($last3chars) == 'svx') { ?>
                                                                                <a href="<?php echo base_url(); ?>uploads/maintenance_file/<?php echo $row3['log_image']; ?>">
                                                                                    <img style="height: 100px; width: 100px;"
                                                                                         src="<?php echo base_url(); ?>assets/img/pdfLogo.png ?>"></a>
                                                                            <?php }
                                                                            if (($last3chars) == 'mp4' || ($last3chars) == 'mpg') { ?>
                                                                                <a href="<?php echo base_url(); ?>uploads/maintenance_file/<?php echo $row3['log_image']; ?>">
                                                                                    <img style="height: 100px; width: 100px;"
                                                                                         src="<?php echo base_url(); ?>assets/img/videoLogo.png ?>"></a>
                                                                            <?php }?>


                                                                        <?php } ?>
                                                                    <?php } ?>
                                                                <?php } ?>
                                                            <?php } ?>
                                                        </td>
                                                    </tr>
                                                <?php } ?>
                                            </table>
                                        <?php } ?>
                                    </div>
                                </div>
                            <?php } ?>
                        </div>
                        <!-- <div class="modal-footer"> -->
                        <!-- <button type="button" class="btn btn-default" data-dismiss="modal">Close</button> -->
                        <!-- </div> -->
                    </div>
                </div>
            </div>


        </div>
    </div>
</div>
<?php $this->load->view('front/footerlink'); ?>
<script type="text/javascript">
    Dropzone.autoDiscover = false;
    var foto_upload = new Dropzone(".dropzone", {
        url: "<?php echo base_url('maintenance/maintenance_comment_file') ?>",
        maxFilesize: 20,
        method: "post",
        acceptedFiles: ".jpg,.jpeg,.png,.gif,.pdf,.doc,.docx,.csv,.csvx,.mp4,.mov,.avi,.mpeg4,.flv",
        paramName: "userfile",
        dictInvalidFileType: "This File Type Not Supported",
        addRemoveLinks: true,
        init: function () {
            var count = 0;
            thisDropzone = this;
            this.on("success", function (file, json) {
                var obj = json;
                $('.previews').append(
                    "<input type='hidden' name='image[]' value='" + obj + "'>\n\
                                  <input type='hidden' name='width[]' value='" + file.width + "'>\n\
                                  <input type='hidden' name='height[]' value='" + file.height + "'>"
                );
            });
        }
    });
</script>