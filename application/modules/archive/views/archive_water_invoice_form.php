<?php $this->load->view('front/headlink'); ?>
<?php $this->load->view('front/archived_top_menu'); ?>
<div class="container">
    <?php $this->load->view('front/head_nav'); ?>
    <div class="row">
        <div class="ss_container">
            <h2><?= ucwords(str_replace("_", "", $which_form)) ?> Water Invoice </h2>
            <div class="ss_bound_content">
                <form onsubmit="return chk_document_upload()" action="<?= $form_action ?>" method="post"
                      enctype="multipart/form-data">
                    <div class="row extra_padding">
                        <div class="col-md-3">
                        </div>
                        <div class="col-md-6">
                            <?php if ($this->session->flashdata('validation_errors')) { ?>
                                <div class="panel panel-danger">
                                    <div class="panel-heading">Error!</div>
                                    <div class="panel-body"><?php echo $this->session->flashdata('validation_errors'); ?></div>
                                </div>
                            <?php } ?>
                            <?php if ($this->session->flashdata('warning')) { ?>
                                <div class="panel panel-warning">
                                    <div class="panel-heading">Warning!</div>
                                    <div class="panel-body"><?php echo $this->session->flashdata('warning'); ?></div>
                                </div>
                            <?php } ?>
                            <?php if ($this->session->flashdata('add_success')) { ?>
                                <div class="panel panel-primary">
                                    <div class="panel-heading">Success!</div>
                                    <div class="panel-body">
                                        Successfully added Invoice
                                        &nbsp;
                                        <a class="btn btn-primary btn-sm"
                                           href="waterInvoiceList/<?= $property_id ?>">View Invoice List
                                        </a>
                                    </div>
                                </div>
                            <?php } ?>
                            <?php if ($this->session->flashdata('update_success')) { ?>
                                <div class="panel panel-primary">
                                    <div class="panel-heading">Success!</div>
                                    <div class="panel-body">
                                        Successfully updated invoice
                                        &nbsp;
                                        <a class="btn btn-primary btn-sm"
                                           href="waterInvoiceList/<?= $property_id ?>">View Invoice List
                                            </a>
                                    </div>
                                </div>
                            <?php } ?>
                        </div>
                    </div>
                    <div class="col-md-3">
                    </div>
            </div>

            <div class="row extra_padding">
                <div class="col-md-3">
                    Description
                    <br>
                </div>
                <div class="col-md-6">
                            <textarea name="water_invoice_description" id="water_invoice_description" rows="6" readonly
                                      class="form-control"><?= ($water_invoice) ? $water_invoice['water_invoice_description'] : ''; ?></textarea>
                </div>
                <div class="col-md-3">
                </div>
            </div>
            <div class="row">
                <div class="col-md-3">
                    Invoice amount
                </div>
                <div class="col-md-6">
                    <div class="input-group">
						<span class="input-group-addon">
							$
						</span>
                        <input id="water_invoice_amount" type="text" name="water_invoice_amount" readonly
                               class="form-control"
                               value="<?= ($water_invoice) ? $water_invoice['water_invoice_amount'] : ''; ?>"
                        >
                    </div>
                </div>
                <div class="col-md-3">
                </div>
            </div>
            <br>
            <div class="row">
                <div class="col-md-3">
                    Invoice due date
                </div>
                <div class="col-md-6">
                    <input id="water_invoice_due_date" readonly type="text" name="water_invoice_due_date"
                           class="form-control"
                           value="<?= ($water_invoice) ? date('jS F, Y', strtotime($water_invoice['water_invoice_due_date'])) : ''; ?>"
                    >
                </div>
                <div class="col-md-3">
                </div>
            </div>
            <br>
            <div class="row">
                <div class="col-md-3">
                    Water Usage Period
                    <br>
                    <small>You can find out what period this charge is by looking at the back page of your water bill</small>
                </div>
                <div class="col-md-6">
                    <input id="water_invoice_water_metre"  type="text" name="water_invoice_water_metre" readonly
                           class="form-control"
                           value="<?= ($water_invoice) ? $water_invoice['water_invoice_water_metre'] : ''; ?>"
                    >
                </div>
                <div class="col-md-3">
                </div>
            </div>
            <br>
            <div class="row">
                <div class="col-md-3">
                    Period
                </div>
                <div class="col-md-6">
                    <div class="row">
                        <div class="col-md-5">
                            <input id="water_invoice_period_start" readonly type="text" name="water_invoice_period_start"
                                   class="form-control form-inine"
                                   value="<?= ($water_invoice) ? date('F, Y', strtotime($water_invoice['water_invoice_period_start'])) : ''; ?>"
                            >
                        </div>
                        <div class="col-md-2 text-center">to</div>
                        <div class="col-md-5">
                            <input id="water_invoice_period_end" readonly type="text" name="water_invoice_period_end"
                                   class="form-control form-inine"
                                   value="<?= ($water_invoice) ? date('F, Y', strtotime($water_invoice['water_invoice_period_end'])) : ''; ?>"
                            >
                        </div>
                    </div>


                </div>
                <div class="col-md-3">
                </div>
            </div>

            <div class="col-md-3"></div>
            <div class="col-md-6">
                <div class="modal-footer clear">
                    <br><br>
                    <input type="hidden" name="water_invoice_id"
                           value="<?= ($water_invoice) ? $water_invoice['water_invoice_id'] : ''; ?>">
                    <input type="hidden" name="property_id" value="<?= $property_id; ?>">
                    <input type="hidden" name="lease_id" value="<?= $lease_id; ?>">
                    <!--<input type="submit" class="btn btn-primary" value="Save">-->
                    <br><br>
                </div>
            </div>
            <div class="col-md-3"></div>
            </form>
        </div>
    </div>
</div>
</div>
<script src="//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
<?php $this->load->view('front/footerlink'); ?>


<script type="text/javascript">

    function chk_document_upload() {
        var err = 0;
        var ret = false;
        var water_invoice_description = $('#water_invoice_description');
        var water_invoice_amount = $('#water_invoice_amount');
        var water_invoice_due_date = $('#water_invoice_due_date');

        if (water_invoice_description.val() == '') {
            water_invoice_description.closest('div').addClass("has-error");
            err = err + 1;
        }
        else {
            water_invoice_description.closest('div').removeClass("has-error");
        }

        if (water_invoice_amount.val() == '' || isNaN(water_invoice_amount.val()) || parseFloat(water_invoice_amount.val()) <= 0) {
            water_invoice_amount.closest('div').addClass("has-error");
            err = err + 1;
        }
        else {
            water_invoice_amount.closest('div').removeClass("has-error");
        }
        if (water_invoice_due_date.val() == '') {
            water_invoice_due_date.closest('div').addClass("has-error");
            err = err + 1;
        }
        else {
            water_invoice_due_date.closest('div').removeClass("has-error");
        }

        if (err == 0) {
            ret = true;
        }
        return ret;
    }
</script>

<script type="text/javascript">
    $(function () {
        $('#water_invoice_due_date').datepicker({
            required: true,
            dateFormat: 'd M, yy'
        });
    });
</script>


</body>
</html>