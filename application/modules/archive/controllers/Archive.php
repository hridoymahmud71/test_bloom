<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Archive extends MX_Controller
{

    function __construct()
    {
        $this->load->model('property/property_model');
        $this->load->model('lease/lease_model');
        $this->load->model('rent/rent_model');
        $this->load->model('message/message_model');
        $this->load->model('water/water_model');
        $this->load->model('inspection/inspection_model');
        $this->load->model('maintenance/Maintenance_model');

        $user_id = $this->session->userdata('user_id');
        $email_id = $this->session->userdata('email');
        $name = $this->session->userdata('name');
        $type = $this->session->userdata('role_type');
        $user_lname = $this->session->userdata('user_fname');
        $user_lname = $this->session->userdata('user_lname');

        if ($user_id == '' || $email_id = '') {
            $this->session->unset_userdata('user_id');
            $this->session->unset_userdata('email');
            $this->session->unset_userdata('name');
            $this->session->unset_userdata('role_type');
            $this->session->unset_userdata('user_fname');
            $this->session->set_userdata('log_err', 'Login First');
            redirect('login', 'refresh');
        }
    }

    public function lease_details($property_id, $lease_id)
    {
        /*if($this->input->post('lease_id'))
        {
            if(($this->input->post('lease_id'))=='new_lease')
            {
                redirect('property/step_three/'.$property_id);
            }
            else
            {
                $this->lease_model->update_with_set_value('lease_current_status','0','property_id', $property_id,'lease');
                $lease_id = $this->input->post('lease_id');
                $data['lease_current_status'] = 1;
                $this->lease_model->update_active_lease($property_id,$lease_id,$data);
            }
        }*/

        $data['property_id'] = $property_id;
        $data['property_info'] = $this->lease_model->select_with_where('*', 'property_id=' . $property_id . '', 'property');
        $data['lease_detail'] = $this->lease_model->select_with_where('*', 'property_id=' . $property_id . ' AND lease_id=' . $lease_id . '', 'lease');
        $data['all_lease'] = $this->lease_model->select_with_where('*', 'property_id=' . $property_id . '', 'lease');
        $data['all_tenant'] = $this->lease_model->get_specific_tenant($data['lease_detail'][0]['lease_id'], $property_id);

        $this->load->view('archive_lease_details', $data);
    }

    public function all_bonds($property_id, $lease_id)
    {
        /*if($this->input->post('lease_id'))
        {
            if(($this->input->post('lease_id'))=='new_lease')
            {
                redirect('property/step_three/'.$property_id);
            }
            else
            {
                $this->lease_model->update_with_set_value('lease_current_status','0','property_id', $property_id,'lease');
                $lease_id = $this->input->post('lease_id');
                $data['lease_current_status'] = 1;
                $this->lease_model->update_active_lease($property_id,$lease_id,$data);
            }
        }*/
        $data['property_id'] = $property_id;
        $data['lease_detail'] = $this->lease_model->select_with_where('*', 'property_id=' . $property_id . ' AND lease_id=' . $lease_id . '', 'lease');
        $data['all_lease'] = $this->lease_model->select_with_where('*', 'property_id=' . $property_id . '', 'lease');
        $data['bond_info'] = $this->lease_model->select_with_where('*', 'property_id=' . $property_id . ' AND lease_id=' . $data['lease_detail'][0]['lease_id'] . '', 'bond');
        if ($data['bond_info']) {
            $data['bond_img_recieve_info'] = $this->lease_model->select_with_where('*', 'bond_id=' . $data['bond_info'][0]['bond_id'] . ' AND status=1', 'bond_recieve_image');
            $data['bond_img_return_info'] = $this->lease_model->select_with_where('*', 'bond_id=' . $data['bond_info'][0]['bond_id'] . ' AND status=1', 'bond_return_image');
        } else {
            $data['bond_img_recieve_info'] = $this->lease_model->select_with_where('*', 'status=1', 'bond_recieve_image');
            $data['bond_img_return_info'] = $this->lease_model->select_with_where('*', 'status=1', 'bond_return_image');
        }
        $data['all_tenant'] = $this->lease_model->get_specific_tenant($data['lease_detail'][0]['lease_id'], $property_id);
        $this->load->view('archive_add_bonds', $data);
    }

    public function add_tenants($property_id, $lease_id)
    {
        /*if($this->input->post('lease_id'))
        {
            if(($this->input->post('lease_id'))=='new_lease')
            {
                redirect('property/step_three/'.$property_id);
            }
            else
            {
                $this->lease_model->update_with_set_value('lease_current_status','0','property_id', $property_id,'lease');
                $lease_id = $this->input->post('lease_id');
                $data['lease_current_status'] = 1;
                $this->lease_model->update_active_lease($property_id,$lease_id,$data);
            }
        }*/
        $data['property_id'] = $property_id;
        $data['property_info'] = $this->lease_model->select_with_where('*', 'property_id=' . $property_id . '', 'property');
        $data['lease_detail'] = $this->lease_model->select_with_where('*', 'property_id=' . $property_id . ' AND lease_id=' . $lease_id . '', 'lease');
        $data['all_lease'] = $this->lease_model->select_with_where('*', 'property_id=' . $property_id . '', 'lease');
        $data['all_tenant'] = $this->lease_model->get_specific_tenant($data['lease_detail'][0]['lease_id'], $property_id);
        // echo "<pre>";print_r($data['all_tenant']);die();
        $this->load->view('archive_add_tenants', $data);
    }

    public function all_lease_doc($property_id, $lease_id)
    {
        $data['lease_category'] = $this->lease_model->select_with_group_by_distinct('lease_cat_name', 'lease_doc_cat_id,lease_cat_name', 'lease_document_category', 'lease_cat_name');
        $data['property_leases'] = $this->lease_model->getPropertyLeases($property_id);
        // echo "<pre>";print_r($data['lease_category']);die();
        $data['all_lease_doc_info'] = $this->lease_model->select_where_join_left('*', 'lease_document_detail', 'lease_document_category', 'lease_document_category.lease_doc_cat_id=lease_document_detail.doc_cat', 'property_id=' . $property_id . '');
        $data['all_lease_doc'] = $this->lease_model->select_all('lease_document');
        $data['all_lease_doc_tenant'] = $this->lease_model->select_all('lease_document_user');
        //echo "<pre>";print_r($data['all_lease_doc_info']);die();
        $data['property_id'] = $property_id;
        $this->load->view('archive_all_lease_doc', $data);
    }

    public function lease_detail_doc($property_id, $lease_doc_id)
    {
        $data['all_lease_doc_info'] = $this->lease_model->select_with_where('*', 'property_id=' . $property_id . ' AND doc_detail_id=' . $lease_doc_id . '', 'lease_document_detail');
        $data['property_id'] = $property_id;
        $this->load->view('archive_all_lease_doc', $data);
    }

    public function edit_lease_doc($property_id, $doc_id)
    {
        $data['doc_detail'] = $this->lease_model->select_with_where('*', 'property_id=' . $property_id . ' AND doc_detail_id=' . $doc_id . '', 'lease_document_detail');
        $data['doc_detail_image'] = $this->lease_model->select_with_where('*', 'doc_detail_id=' . $doc_id . '', 'lease_document');
        $data['doc_detail_user'] = $this->lease_model->select_with_where('*', 'doc_detail_id=' . $doc_id . '', 'lease_document_user');
        $data['chk_user_data'] = array();
        foreach ($data['doc_detail_user'] as $row) {
            array_push($data['chk_user_data'], $row['user_id']);
        }
        $data['doc_detail_cat'] = $this->lease_model->select_all('lease_document_category');
        $data['lease_detail'] = $this->lease_model->select_with_where('*', 'property_id=' . $property_id . ' AND lease_current_status=1', 'lease');
        $data['all_tenant'] = null;
        // echo "<pre>";print_r($data['all_tenant']);die();
        $data['property_id'] = $property_id;
        $this->load->view('archive_edit_lease_doc', $data);
    }


    public function all_message($property_id, $lease_id)
    {
        $limit = 10;

        $data['lease_detail'] = $this->message_model->select_with_where('*', 'property_id=' . $property_id . ' AND lease_current_status=1', 'lease');
        $data['all_tenant'] = $this->message_model->get_specific_tenant($data['lease_detail'][0]['lease_id'], $property_id);
        $data['property_id'] = $property_id;
        $data['lease_id'] = $data['lease_detail'][0]['lease_id'];

        $data['all_message'] = $this->getAllMessages($property_id, $lease_id, $limit, $offset = 0);
        $data['count_message'] = $this->message_model->message_nums('*', 'property_id=' . $property_id . ' AND lease_id=' . $lease_id . '', 'message');


        $data['all_lease'] = $this->message_model->select_with_where('*', 'property_id=' . $property_id . '', 'lease');
        $data['limit'] = $limit;

        $data['property_id'] = $property_id;
        $data['lease_id'] = $lease_id;

        $data['chunked_message_view'] = $this->load->view('archived_chunked_message_view', $data, true);
        $this->load->view('archive_all_message', $data);
    }

    public function getAllMessages($property_id, $lease_id, $limit, $offset)
    {
        $all_message = $this->message_model->select_with_where_order_by('*', 'property_id=' . $property_id . ' AND lease_id=' . $lease_id . '', 'message', 'message_id', 'DESC', $limit, $offset);


        if ($all_message) {
            $all_message[0]['query'] = $this->db->last_query();
            foreach ($all_message as $key => $value) {
                $user_name = $this->message_model->get_tenant_name($value['user_id']);
                $all_message[$key]['user_name'] = $user_name[0]['user_fname'] . ' ' . $user_name[0]['user_lname'];

                $last_comment = $this->message_model->select_last_comment($all_message[$key]['message_id']);

                if ($last_comment) {
                    $all_message[$key]['last_comment'] = $last_comment[0]['comment'];
                } else {
                    $all_message[$key]['last_comment'] = '';
                }

                $all_message[$key]['comment_count'] = $this->message_model->getMessageCommentCount($all_message[$key]['message_id']);
                $all_message[$key]['document_count'] = $this->message_model->getMessageDocumentCount($all_message[$key]['message_id']);
                $all_message[$key]['tenant_count'] = $this->message_model->getMessageTenantCount($all_message[$key]['message_id']);

            }
        }


        return $all_message;
    }


    public function archived_ajax_chunk_message()
    {
        $property_id = $_REQUEST['property_id'];
        $lease_id = $_REQUEST['lease_id'];
        $limit = $_REQUEST['limit'];
        $offset = $_REQUEST['offset'];
        $data['all_message'] = $this->getAllMessages($property_id, $lease_id, $limit, $offset);

        $chunked_message_view = $this->load->view('archived_chunked_message_view', $data, true);

        //echo $data['all_message'][0]['query'];
        echo $chunked_message_view;
        exit;
    }

    public function message_detail($message_id)
    {
        $data['message_detail'] = $this->message_model->select_with_where('*', 'message_id=' . $message_id . '', 'message');

        $user_name = $this->message_model->get_tenant_name($data['message_detail'][0]['user_id']);
        $data['message_detail'][0]['user_name'] = $user_name[0]['user_fname'] . ' ' . $user_name[0]['user_lname'];

        $data['all_tenant'] = $this->message_model->get_specific_tenant($data['message_detail'][0]['lease_id'], $data['message_detail'][0]['property_id']);
        $data['all_message_comment'] = $this->message_model->select_with_where('*', 'message_id=' . $data['message_detail'][0]['message_id'] . '', 'message_comment');
        $data['all_message_document'] = $this->message_model->select_with_where('*', 'message_id=' . $data['message_detail'][0]['message_id'] . '', 'message_document');
        $data['all_message_tenant'] = $this->message_model->select_with_where('*', 'message_id=' . $data['message_detail'][0]['message_id'] . '', 'message_tenant');
        $data['property'] = $this->message_model->select_with_where('*', 'property_id=' . $data['message_detail'][0]['property_id'] . '', 'property');
        $data['property_id'] = $data['message_detail'][0]['property_id'];

        $this->load->view('archive_view_message_detail', $data);
    }

    public function message_detail_pdf($message_id)
    {
        $data['message_detail'] = $this->message_model->select_with_where('*', 'message_id=' . $message_id . '', 'message');

        $user_name = $this->message_model->get_tenant_name($data['message_detail'][0]['user_id']);
        $data['message_detail'][0]['user_name'] = $user_name[0]['user_fname'] . ' ' . $user_name[0]['user_lname'];

        $data['all_tenant'] = $this->message_model->get_specific_tenant($data['message_detail'][0]['lease_id'], $data['message_detail'][0]['property_id']);
        $data['all_message_comment'] = $this->message_model->select_with_where('*', 'message_id=' . $data['message_detail'][0]['message_id'] . '', 'message_comment');
        $data['all_message_document'] = $this->message_model->select_with_where('*', 'message_id=' . $data['message_detail'][0]['message_id'] . '', 'message_document');
        $data['all_message_tenant'] = $this->message_model->select_with_where('*', 'message_id=' . $data['message_detail'][0]['message_id'] . '', 'message_tenant');
        $data['property'] = $this->message_model->select_with_where('*', 'property_id=' . $data['message_detail'][0]['property_id'] . '', 'property');
        $data['property_id'] = $data['message_detail'][0]['property_id'];

        $html = $this->load->view('archive_view_message_detail_pdf', $data, true);
        //echo $html;die();

        $this->draw_message_detail_pdf($html, $data);
    }

    private function draw_message_detail_pdf($html, $data)
    {
        $this->load->library('MPDF/mpdf');
        $mpdf = new mPDF('win-1252', 'A4', '', '', 5, 5, 16, 60, 5, 5, 'L');
        $mpdf->useOnlyCoreFonts = true;    // false is default
        $mpdf->SetProtection(array('print'));
        $mpdf->SetTitle('Rent Schedule');
        $mpdf->SetAuthor('Bloom');
        $mpdf->SetDisplayMode('fullpage');

        $mpdf->debug = true;

        /*echo $html;
        die();*/

        $name = 'Message_' . $data['message_detail'][0]['message_id'] . $data['message_detail'][0]['lease_id'] . $data['message_detail'][0]['property_id'] . date('YmdHis') . '.pdf';

        //echo  $link;die();
        $mpdf->WriteHTML($html);
        $mpdf->Output($name, 'I');

        exit;

    }

    public function water_invoice_list($property_id, $lease_id)
    {
        $data['property_id'] = $property_id;
        $data['water_invoices'] = null;
        $data['active_lease'] = null;

        $active_lease = $this->water_model->getLease($lease_id);

        if ($active_lease) {
            $data['active_lease'] = $active_lease;
        }

        $data['water_invoices'] = $this->water_model->getWaterInvoiceList($lease_id);
        $data['lease_id'] = $lease_id;

        $this->load->view('archive_water_invoice_list', $data);
    }

    public function edit_water_invoice($water_invoice_id)
    {
        $water_invoice = $this->water_model->getWaterInvoice($water_invoice_id);

        $data['which_form'] = 'View';
        $data['property_id'] = 0;
        $data['lease_id'] = 0;
        if ($water_invoice) {
            $data['property_id'] = $water_invoice['property_id'];
        }

        $data['form_action'] = 'updateWaterInvoice';
        $data['water_invoice'] = $water_invoice;

        $this->load->view('archive_water_invoice_form', $data);
    }

    public function inspection_list($property_id,$lease_id,$which_inspection_list)
    {
        $data['property_id'] = $property_id;
        $data['inspections'] = null;
        $data['active_lease'] = null;
        $data['which_inspection_list'] = $which_inspection_list;

        $active_lease = $this->inspection_model->getActiveLease($property_id);

        if ($active_lease) {
            $data['active_lease'] = $active_lease;
        }

        $inspection_status = '';
        if ($which_inspection_list == 'upcoming') {
            $inspection_status = 0;
        } elseif ($which_inspection_list == 'completed') {
            $inspection_status = 1;
        }

        $inspections = null;
        if ($active_lease || 1) {
            $inspections = $this->inspection_model->getInspectionList($property_id, $inspection_status);
        }

        $data['inspections'] = $inspections;
        $this->load->view('inspection_list', $data);
    }

    public function view_all_maintenance($property_id,$lease_id)
    {

        if (!$this->session->userdata('user_id')) {
            $this->session->set_userdata('log_err', 'Login First');
            redirect('login', 'refresh');
        }

        $property_idd = $property_id;

        $lease = $this->Maintenance_model->select_with_where('lease_name', "property_id={$property_idd} AND lease_id={$lease_id}", 'lease');
        $lease_name = $lease[0]['lease_name'];
        $this->session->set_userdata('lease_name', $lease_name);
        $data['property_id'] = $property_idd;

        //$data['result'] = $this->Maintenance_model->all_maintenance($property_idd);
        $data['result'] = $this->Maintenance_model->all_maintenance_by_lease($property_idd, $lease_id);

        //$data['result2'] = $this->Maintenance_model->select_with_where('*', "property_id=" . $property_idd . "", 'maintenance_image');
        $data['result2'] = $this->Maintenance_model->select_with_where('*', "property_id={$property_idd} AND lease_id={$lease_id}", 'maintenance_image');

        //$data['new'] = $this->Maintenance_model->maintenance_todo($property_idd, '1');
        //$data['open'] = $this->Maintenance_model->maintenance_todo($property_idd, '2');
        //$data['todo'] = $this->Maintenance_model->maintenance_todo($property_idd, '3');
        //$data['closed'] = $this->Maintenance_model->maintenance_todo($property_idd, '4');
        //$data['archive'] = $this->Maintenance_model->maintenance_todo($property_idd, '5');

        $data['new'] = $this->Maintenance_model->maintenance_todo_with_lease($property_idd, $lease_id, '1');
        $data['open'] = $this->Maintenance_model->maintenance_todo_with_lease($property_idd, $lease_id, '2');
        $data['todo'] = $this->Maintenance_model->maintenance_todo_with_lease($property_idd, $lease_id, '3');
        $data['closed'] = $this->Maintenance_model->maintenance_todo_with_lease($property_idd, $lease_id, '4');
        $data['archive'] = $this->Maintenance_model->maintenance_todo_with_lease($property_idd, $lease_id, '5');
        $this->load->view('archive_view_all_maintenance', $data);
    }

    public function edit_maintenance($property_id, $maintenance_id)
    {

        if (!$this->session->userdata('user_id')) {
            $this->session->set_userdata('log_err', 'Login First');
            redirect('login', 'refresh');
        }

        $id = $maintenance_id;
        $data['property_id'] = $property_id;
        $data['result'] = $this->Maintenance_model->view_maintenance($id);

        $data['maintenance_log'] = $this->Maintenance_model->get_maintance_log_info($data['result'][0]['maintenance_id']);

        foreach ($data['maintenance_log'] as $key => $val) {
            $data['maintenance_log_image'][] = $this->Maintenance_model->select_with_where('*', 'maintenance_log_id=' . $data['maintenance_log'][$key]['maintenance_log_id'] . '', 'maintenance_log_image');
        }
        // echo "<pre>";
        // print_r($data['maintenance_log']);
        // print_r($data['maintenance_log_image']);
        // die();
        $data['result2'] = $this->Maintenance_model->view_maintenance_comment($id);
        $data['result3'] = $this->Maintenance_model->view_maintenance_comment_file($id);
        $data['maintenance_id'] = $maintenance_id;
        // echo '<pre>'; print_r($data['result']); die;
        $this->load->view('archive_edit_maintenance', $data);
    }

    public function edit_maintenance_order_invoice($maintenance_id)
    {
        $data['property_id'] = 0;
        $data['maintenance'] = $this->Maintenance_model->getMaintenance($maintenance_id);
        if ($data['maintenance']) {
            $data['property_id'] = $data['maintenance']['property_id'];
        }

        $data['maintenance_invoice'] = $this->Maintenance_model->getMaintenanceInvoice($maintenance_id);

        $data['which_form'] = 'view';
        $data['form_action'] = 'updateMaintenanceOrderInvoice';

        $data['maintenance_order_invoice'] = null;
        $data['water_invoice_logs'] = null;

        $this->load->view('archive_maintenance_invoice_form', $data);
    }


}