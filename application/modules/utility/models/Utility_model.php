<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Utility_model extends CI_Model
{

    public function count_properties_by_user($user_id)
    {
        $this->db->select('user_id');
        $this->db->from('property');
        $this->db->where('user_id', $user_id);

        return $this->db->get()->num_rows();
    }

    public function if_user_has_properties($user_id)
    {
        return $this->count_properties_by_user($user_id) > 0 ? true : false;
    }


    public function insertAsMessage($sender_id, $property_id, $lease_id, $tenant_ids, $subject, $message, $documents)
    {
        // ----- precaution <starts>-----
        if (!$sender_id) {
            $sender_id = $this->getPropertyOwnerId($property_id);

            if (!$sender_id) {
                $sender_id = 0; //should not be happening
            }
        }

        if (!$property_id && !$lease_id) {
            exit;
        }

        if (!$property_id && $lease_id) {
            $lease = $this->getLease($lease_id);
            if ($lease) {
                $property_id = $lease['property_id'];
            } else {
                $property_id = 0; //should not be happening
            }
        }

        if ($property_id && !$lease_id) {
            $last_active_lease = $this->getLastActiveLeaseOfProperty($property_id);
            if ($last_active_lease) {
                $lease_id = $last_active_lease['lease_id'];
            } else {
                $lease_id = 0; //should not be happening
            }
        }

        if (!$tenant_ids) {
            $tenant_ids = $this->getTenantIds($lease_id);
        }
        // ----- precaution <ends> -----

        $ins_msg_data = array();
        $ins_msg_data['user_id'] = $sender_id;
        $ins_msg_data['property_id'] = $property_id;
        $ins_msg_data['lease_id'] = $lease_id;
        $ins_msg_data['subject'] = $subject;
        $ins_msg_data['created_at'] = date('Y-m-d H:i:s');
        $this->db->insert('message', $ins_msg_data);
        $message_id = $this->db->insert_id();


        $ins_msg_cmt_data['message_id'] = $message_id;
        $ins_msg_cmt_data['comment'] = $message;
        $this->db->insert('message_comment', $ins_msg_cmt_data);
        $comment_id = $this->db->insert_id();


        if (!empty($tenant_ids)) {
            foreach ($tenant_ids as $tenant_id) {

                $ins_msg_tnt_data = array();
                $ins_msg_tnt_data['message_id'] = $message_id;
                $ins_msg_tnt_data['tenant_id'] = $tenant_id;
                $this->db->insert('message_tenant', $ins_msg_tnt_data);
            }
        }

        if (!empty($documents)) {
            foreach ($documents as $document) {
                $ins_doc_data = array();
                $ins_doc_data['message_id'] = $message_id;
                $ins_doc_data['message_com_id'] = $comment_id;
                $ins_doc_data['document'] = $document;
                $this->db->insert('message_document', $ins_doc_data);
            }
        }


    }

    public function insertAsNotification($sender_id, $property_id, $lease_id, $text, $url, $given_date)
    {
        // ----- precaution <starts>-----
        if (!$sender_id) {
            $sender_id = $this->getPropertyOwnerId($property_id);

            if (!$sender_id) {
                $sender_id = 0; //should not be happening
            }
        }

        if (!$property_id && !$lease_id) {
            exit;
        }

        if (!$property_id && $lease_id) {
            $lease = $this->getLease($lease_id);
            if ($lease) {
                $property_id = $lease['property_id'];
            } else {
                $property_id = 0; //should not be happening
            }
        }

        if ($property_id && !$lease_id) {
            $last_active_lease = $this->getLastActiveLeaseOfProperty($property_id);
            if ($last_active_lease) {
                $lease_id = $last_active_lease['lease_id'];
            } else {
                $lease_id = 0; //should not be happening
            }
        }
        // ----- precaution <ends> -----

        $ins_noti_data = array();
        $ins_noti_data['user_id'] = $sender_id;
        $ins_noti_data['property_id'] = $property_id;
        $ins_noti_data['lease_id'] = $lease_id;
        $ins_noti_data['text'] = $text;
        $ins_noti_data['url'] = $url;
        $ins_noti_data['created_on'] = date('Y-m-d H:i:s');
        $ins_noti_data['meant_to_be_seen_after'] = !empty($given_date) ? $given_date : date('Y-m-d H:i:s');
        $this->db->insert('notification', $ins_noti_data);
        $notification_id = $this->db->insert_id();

        return $notification_id;

    }

    public function getPropertyOwnerId($property_id)
    {
        $this->db->select('user_id');
        $this->db->from('property');
        $this->db->where('property_id', $property_id);
        $query = $this->db->get();

        $row = $query->row_array();

        $id = false;
        if (!empty($row)) {
            $id = $row['user_id'];
        }

        return $id;
    }

    public function getPropertyOwner($property_id)
    {
        $owner_id = $this->getPropertyOwnerId($property_id);

        if ($owner_id) {
            $this->db->select('*');
            $this->db->from('user');
            $this->db->where('user_id', $owner_id);
            $query = $this->db->get();

            $row = $query->row_array();
            return $row;
        }

        return null;

    }

    public function getUser($user_id)
    {
        $this->db->select('*');
        $this->db->from('user');
        $this->db->where('user_id', $user_id);
        $query = $this->db->get();

        $row = $query->row_array();
        return $row;

    }

    public function getLease($lease_id)
    {
        $this->db->select('*');
        $this->db->from('lease');
        $this->db->where('lease_id', $lease_id);
        $query = $this->db->get();

        return $query->row_array();
    }

    public function getTenantsWithLeaseDetails($lease_id)
    {
        $this->db->select('*');
        $this->db->from('lease_detail');
        $this->db->where('lease_id', $lease_id);
        $this->db->join('user', 'lease_detail.tenant_id=user.user_id');

        $query = $this->db->get();
        $result = $query->result_array();

        return $result;
    }

    public function getProperty($property_id)
    {
        $this->db->select('*');
        $this->db->from('property');
        $this->db->where('property_id', $property_id);
        $query = $this->db->get();

        return $query->row_array();
    }

    public function getPropertyWithLandlord($property_id)
    {
        $this->db->select('*');
        $this->db->from('property');
        $this->db->where('property.property_id', $property_id);
        $this->db->join('user', 'property.user_id=user.user_id');
        $query = $this->db->get();

        return $query->row_array();
    }

    public function getLastActiveLeaseOfProperty($property_id)
    {
        $this->db->select('*');
        $this->db->from('lease');
        $this->db->where('property_id', $property_id);
        $this->db->where('lease_current_status', 1);
        $this->db->order_by('lease_id', 'desc');
        $query = $this->db->get();

        $row = $query->row_array();

        return $row;
    }

    public function getLastActiveLeaseIdOfProperty($property_id)
    {
        $id = 0;
        $lease = $this->getLastActiveLeaseOfProperty($property_id);

        if(!empty($lease)){
            $id = $lease['lease_id'];
        }

        return $id;
    }

    public function getTenantIds($lease_id)
    {
        $this->db->select('tenant_id');
        $this->db->from('lease_detail');
        $this->db->where('lease_id', $lease_id);
        $query = $this->db->get();

        $rows = $query->result_array();

        $ids = null;
        if (count($rows) > 0) {
            $ids = array_column($rows, 'tenant_id');
        }

        return $ids;
    }

    public function getTenants($lease_id)
    {
        $rows = null;
        $tenant_ids = $this->getTenantIds($lease_id);

        if ($tenant_ids) {
            $this->db->select('*');
            $this->db->from('user');
            $this->db->where_in('user_id', $tenant_ids);
            $query = $this->db->get();

            $rows = $query->result_array();
        }

        return $rows;
    }

    //------------------------------------------------------------------------------------------------------------------

    public function rentIncomeDatesWithAmountByLease($lease_id, $fiscal_year_starts, $fiscal_year_ends)
    {
        $this->db->select('DATE(lease_log_payment_time) as date,lease_log_payment_amount_recieved as income,0.00 as expense', false);
        $this->db->from('lease_log_payment');

        $this->db->where('DATE(lease_log_payment_time)>=', $fiscal_year_starts);
        $this->db->where('DATE(lease_log_payment_time)<=', $fiscal_year_ends);

        $this->db->where('lease_id', $lease_id);

        $query = $this->db->get();

        $rows = $query->result_array();
        return $rows;
    }

    public function moneyInIncomeDatesWithAmountByLease($lease_id, $fiscal_year_starts, $fiscal_year_ends)
    {
        $this->db->select('DATE(money_in_receiving_date_new) as date,cash_receiving_amount as income,0.00 as expense', false);
        $this->db->from('money_in_cash_receive_amount');
        $this->db->where('lease_id', $lease_id);

        $this->db->where('DATE(money_in_receiving_date_new)>=', $fiscal_year_starts);
        $this->db->where('DATE(money_in_receiving_date_new)<=', $fiscal_year_ends);

        $query = $this->db->get();

        $rows = $query->result_array();
        return $rows;
    }

    public function waterIncomeDatesWithAmountByLease($lease_id, $fiscal_year_starts, $fiscal_year_ends)
    {
        $this->db->select('DATE(water_invoice_log.water_invoice_payment_date) as date,water_invoice_log.water_invoice_log_amount as income,0.00 as expense', false);
        $this->db->from('water_invoice_log');
        $this->db->join('water_invoice', 'water_invoice_log.water_invoice_id=water_invoice.water_invoice_id', 'left');
        $this->db->where('water_invoice.lease_id', $lease_id);

        $this->db->where('DATE(water_invoice_log.water_invoice_payment_date)>=', $fiscal_year_starts);
        $this->db->where('DATE(water_invoice_log.water_invoice_payment_date)<=', $fiscal_year_ends);

        $query = $this->db->get();

        $rows = $query->result_array();
        return $rows;
    }

    public function mainExpenseDatesWithAmountByProperty($property_id, $full_or_partial, $fiscal_year_starts, $fiscal_year_ends)
    {
        if ($full_or_partial == 'full') {
            $this->db->select('DATE(expense_paid_date) as date,0.00 as income,total_invoice_amount as expense', false);
            $this->db->where('expense_paying_status', 1);
        } else if ($full_or_partial == 'partial') {
            $this->db->select('DATE(expense_paid_date) as date,0.00 as income,expense_partial_amount_paid as expense', false);
            $this->db->where('expense_paying_status', 2);
        } else {
            $empty_arr = array();
            return $empty_arr;
        }

        $this->db->from('expense');
        $this->db->where('property_id', $property_id);

        $this->db->where('DATE(expense_paid_date)>=', $fiscal_year_starts);
        $this->db->where('DATE(expense_paid_date)<=', $fiscal_year_ends);

        $query = $this->db->get();

        $rows = $query->result_array();
        return $rows;
    }


    //------------------------------------------------------------------------------------------------------------------


    public function getMinimumDateOfIncomeExpense($property_id, $lease_id)
    {
        $min_date = date('Y-m-d');
        $min_dates = array();

        $min_dates[] = $this->rentIncomeMinDateByLease($lease_id);
        $min_dates[] = $this->moneyInIncomeMinDateByLease($lease_id);
        $min_dates[] = $this->waterIncomeMinDate($lease_id);
        $min_dates[] = $this->mainExpenseMinDateByProperty($property_id, 'full');
        $min_dates[] = $this->mainExpenseMinDateByProperty($property_id, 'partial');

        $min_dates_timestamps = (array_map("self::string_to_integerime", $min_dates));
        $min_date_timestamp = intval(min($min_dates_timestamps));
        $min_date = date('Y-m-d', $min_date_timestamp);

        return $min_date;
    }

    function string_to_integerime($string)
    {
        return strtotime($string);
    }

    public function rentIncomeMinDateByLease($lease_id)
    {
        $min_date = date('Y-m-d');

        $this->db->select('MIN(DATE(lease_log_payment_time)) as min_date', false);
        $this->db->from('lease_log_payment');
        $this->db->where('lease_id', $lease_id);
        $this->db->where('lease_log_payment_time !=', null);
        $this->db->where('lease_log_payment_time !=', '');
        $this->db->where('DATE(lease_log_payment_time) !=', '0000-00-00', false);

        $query = $this->db->get();

        $row = $query->row_array();

        if (!empty($row)) {
            if (!empty($row['min_date'])) {
                $min_date = $row['min_date'];
            }
        }

        return $min_date;

    }

    public function moneyInIncomeMinDateByLease($lease_id)
    {
        $min_date = date('Y-m-d');

        $this->db->select('MIN(DATE(money_in_receiving_date_new)) as min_date', false);
        $this->db->from('money_in_cash_receive_amount');
        $this->db->where('lease_id', $lease_id);
        $this->db->where('money_in_receiving_date_new !=', null);
        $this->db->where('money_in_receiving_date_new !=', '');
        $this->db->where('DATE(money_in_receiving_date_new) !=', '0000-00-00', false);

        $query = $this->db->get();

        $row = $query->row_array();

        if (!empty($row)) {
            if (!empty($row['min_date'])) {
                $min_date = $row['min_date'];
            }
        }

        return $min_date;
    }

    public function waterIncomeMinDate($lease_id)
    {
        $min_date = date('Y-m-d');

        $this->db->select('MIN(DATE(water_invoice_log.water_invoice_payment_date)) as min_date', false);
        $this->db->from('water_invoice_log');
        $this->db->join('water_invoice', 'water_invoice_log.water_invoice_id=water_invoice.water_invoice_id', 'left');
        $this->db->where('water_invoice.lease_id', $lease_id);
        $this->db->where('water_invoice_payment_date !=', null);
        $this->db->where('water_invoice_payment_date !=', '');
        $this->db->where('DATE(water_invoice_payment_date) !=', '0000-00-00', false);

        $query = $this->db->get();

        $row = $query->row_array();

        if (!empty($row)) {
            if (!empty($row['min_date'])) {
                $min_date = $row['min_date'];
            }
        }

        return $min_date;
    }

    public function mainExpenseMinDateByProperty($property_id, $full_or_partial)
    {
        $min_date = date('Y-m-d');

        if ($full_or_partial == 'full') {
            $this->db->select('MIN(DATE(expense_paid_date)) as min_date', false);
            $this->db->where('expense_paying_status', 1);
        } else if ($full_or_partial == 'partial') {
            $this->db->select('MIN(DATE(expense_paid_date)) as min_date', false);
            $this->db->where('expense_paying_status', 2);
        }

        $this->db->where('expense_paid_date !=', null);
        $this->db->where('expense_paid_date !=', "");
        $this->db->where('DATE(expense_paid_date) !=', '0000-00-00');

        $this->db->from('expense');
        $this->db->where('property_id', $property_id);
        $query = $this->db->get();

        $row = $query->row_array();

        if (!empty($row)) {
            if (!empty($row['min_date'])) {
                $min_date = $row['min_date'];
            }
        }

        return $min_date;
    }

    //------------------------------------------------------------------------------------------------------------------

    public function getIncomes($property_id, $lease_id, $start_date, $end_date)
    {
        $incomes = array();

        $arrs['rent_incomes'] = $this->rentIncomesByLease($lease_id, $start_date, $end_date);
        $arrs['money_in_incomes'] = $this->moneyInIncomesByLease($lease_id, $start_date, $end_date);
        $arrs['water_incomes'] = $this->waterIncomesByLease($lease_id, $start_date, $end_date);

        foreach ($arrs as $arr_key => $arr_val) {
            if (!empty($arr_val)) {
                $incomes = array_merge($incomes, $arr_val);
            }
        }

        return $incomes;
    }

    public function getExpenses($property_id, $lease_id, $start_date, $end_date)
    {
        $expenses = array();

        $arrs['full_main_expenses'] = $this->mainExpensesByProperty($property_id, 'full', $start_date, $end_date);
        $arrs['partial_main_expenses'] = $this->mainExpensesByProperty($property_id, 'partial', $start_date, $end_date);

        foreach ($arrs as $arr_key => $arr_val) {
            if (!empty($arr_val)) {
                $expenses = array_merge($expenses, $arr_val);
            }
        }

        return $expenses;
    }

    public function rentIncomesByLease($lease_id, $start_date, $end_date)
    {
        $data = array();

        $this->db->select(
            "lease_log_payment_id as id,
            DATE(lease_log_payment_time) as date,
            lease_log_payment_amount_recieved as income,
            'rent' as type,
            'nothing' as sub_type"
            , false);
        $this->db->from('lease_log_payment');
        $this->db->where('lease_id', $lease_id);

        $this->db->where("DATE(lease_log_payment_time)>=", $start_date);
        $this->db->where("DATE(lease_log_payment_time)<=", $end_date);

        $query = $this->db->get();

        $rows = $query->result_array();
        if (!empty($rows)) {
            $data = $rows;
        }

        return $data;
    }

    public function moneyInIncomesByLease($lease_id, $start_date, $end_date)
    {
        $data = array();

        $this->db->select(
            "money_in.money_in_id as id,
            DATE(money_in_cash_receive_amount.money_in_receiving_date_new) as date,
             money_in_cash_receive_amount.cash_receiving_amount as income,'money_in' as type,
             money_in.money_in_description as sub_type"
            , false);
        $this->db->from('money_in_cash_receive_amount');
        $this->db->join('money_in', 'money_in_cash_receive_amount.money_in_id=money_in.money_in_id', 'left');
        $this->db->where('money_in_cash_receive_amount.lease_id', $lease_id);

        $this->db->where("DATE(money_in_cash_receive_amount.money_in_receiving_date_new)>=", $start_date);
        $this->db->where("DATE(money_in_cash_receive_amount.money_in_receiving_date_new)<=", $end_date);

        $query = $this->db->get();

        $rows = $query->result_array();
        if (!empty($rows)) {
            $data = $rows;
        }


        return $data;
    }

    public function waterIncomesByLease($lease_id, $start_date, $end_date)
    {
        $data = array();

        $this->db->select(
            "water_invoice_log.water_invoice_log_id as id,            
            DATE(water_invoice_log.water_invoice_payment_date) as date,
             water_invoice_log.water_invoice_log_amount as income,'water' as type,
             CONCAT(water_invoice.water_invoice_period_start,' to ',water_invoice.water_invoice_period_end) as sub_type"
            , false);
        $this->db->from('water_invoice_log');
        $this->db->join('water_invoice', 'water_invoice_log.water_invoice_id=water_invoice.water_invoice_id', 'left');
        $this->db->where('water_invoice.lease_id', $lease_id);

        $this->db->where("DATE(water_invoice_log.water_invoice_payment_date)>=", $start_date);
        $this->db->where("DATE(water_invoice_log.water_invoice_payment_date)<=", $end_date);

        $query = $this->db->get();

        $rows = $query->result_array();
        if (!empty($rows)) {
            $data = $rows;
        }


        return $data;
    }

    public function mainExpensesByProperty($property_id, $full_or_partial, $start_date, $end_date)
    {
        $data = array();

        if ($full_or_partial == 'full') {
            $this->db->select(
                "expense_id as id,
                 DATE(expense_paid_date) as date,
                 total_invoice_amount as expense,
                 'expense' as type,tax_code as sub_type"
                , false);
            $this->db->where('expense_paying_status', 1);
        } else if ($full_or_partial == 'partial') {
            $this->db->select(
                "expense_id as id,
                 DATE(expense_paid_date) as date,
                 expense_partial_amount_paid as expense,
                 'expense' as type,tax_code as sub_type"
                , false);
            $this->db->where('expense_paying_status', 2);
        } else {
            $empty_arr = array();
            return $empty_arr;
        }

        $this->db->from('expense');
        $this->db->where('property_id', $property_id);

        $this->db->where("DATE(expense_paid_date)>=", $start_date);
        $this->db->where("DATE(expense_paid_date)<=", $end_date);

        $query = $this->db->get();

        $rows = $query->result_array();
        if (!empty($rows)) {
            $data = $rows;
        }


        return $data;
    }

    public function getStateName($state_id)
    {
        $state_name = "";
        $states = array();

        $states[246] = 'Australian Capital Territory';
        $states[266] = 'New South Wales';
        $states[267] = 'Northern Territory';
        $states[269] = 'Queensland';
        $states[270] = 'South Australia';
        $states[271] = 'Tasmania';
        $states[273] = 'Victoria';
        $states[275] = 'Western Australia';

        if (array_key_exists($state_id, $states)) {
            $state_name = $states[$state_id];
        }

        return $state_name;

    }

    public function getStateFromDB($state_id)
    {
        $this->db->select('*');
        $this->db->from('state');
        $this->db->where('state_id', $state_id);
        $query = $this->db->get();

        $row = $query->row_array();

        return $row;
    }

    public function getStateNameFromDB($state_id)
    {
        $state_name = "";

        $state = $this->getStateFromDB($state_id);
        if (!empty($state)) {
            $state_name = $state['state_name'];
        }

        return $state_name;
    }

    public function getCountryFromDB($country_id)
    {

        $this->db->select('*');
        $this->db->from('country');
        $this->db->where('country_id', $country_id);
        $query = $this->db->get();

        $row = $query->row_array();

        return $row;
    }

    public function getCountryNameFromDB($country_id)
    {
        $country_name = "";

        $country = $this->getCountryFromDB($country_id);
        if (!empty($country)) {
            $country_name = $country['country_name'];
        }

        return $country_name;
    }

    public function getFullPropertyAddress($property_id)
    {
        $addr = "";
        $addr_arr = array();

        $property = $this->getProperty($property_id);

        if (!empty($property)) {

            if (!empty($property['property_address'])) {
                $addr_arr[] = $property['property_address'];
            }

            if (!empty($property['city'])) {
                $addr_arr[] = $property['city'];
            }

            if ($property['state'] > 0) {
                $addr_arr[] = $this->getStateNameFromDB($property['state']);
            }

            if ($property['country'] > 0) {
                $addr_arr[] = $this->getCountryNameFromDB($property['country']);
            }

            if (!empty($property['zip_code'])) {
                $addr_arr[] = $property['zip_code'];
            }

            $addr = implode(' , ', $addr_arr);



        }

        return $addr;
    }

    public function getWeekDay($key)
    {
        $day_name = "";
        $days = array();

        $days[1] = "Monday";
        $days[2] = "Tuesday";
        $days[3] = "Wednesday";
        $days[4] = "Thursday";
        $days[5] = "Friday";
        $days[6] = "Saturday";
        $days[7] = "Sunday";

        if (array_key_exists($key, $days)) {
            $day_name = $days[$key];
        }

        return $day_name;
    }

    public function addOrdinalNumberSuffix($num)
    {
        if (!in_array(($num % 100), array(11, 12, 13))) {
            switch ($num % 10) {
                // Handle 1st, 2nd, 3rd
                case 1:
                    return $num . 'st';
                case 2:
                    return $num . 'nd';
                case 3:
                    return $num . 'rd';
            }
        }
        return $num . 'th';
    }

    public function getLeaseRentPeriodByGivenDate($lease_id, $date)
    {
        $this->db->select('*');
        $this->db->from('lease_payment_scedule');
        $this->db->where('lease_id', $lease_id);
        $this->db->where('payment_start_period<=', $date);
        $this->db->where('payment_end_period>=', $date);
        $query = $this->db->get();

        $row = $query->row_array();
        return $row;
    }

    public function getCurrentLeaseRentPeriod($lease_id)
    {
        $date = date('Y-m-d');

        $this->db->select('*');
        $this->db->from('lease_payment_scedule');
        $this->db->where('lease_id', $lease_id);
        $this->db->where('payment_start_period<=', $date);
        $this->db->where('payment_end_period>=', $date);
        $query = $this->db->get();

        $row = $query->row_array();
        return $row;
    }

    public function getNextOfCurrentLeaseRentPeriod($lease_id, $list_or_single)
    {
        $current_lease_rent_period = $this->getCurrentLeaseRentPeriod($lease_id);

        $data = null;

        if (!empty($current_lease_rent_period)) {

            $this->db->select('*');
            $this->db->from('lease_payment_scedule');
            $this->db->where('lease_id', $lease_id);
            $this->db->where('payment_start_period>', $current_lease_rent_period['payment_end_period']);

            $this->db->order_by('lease_id', $lease_id);

            $query = $this->db->get();

            if ($list_or_single == 'list') {
                $data = $query->result_array();
            } else if ($list_or_single == 'single') {
                $data = $query->row_array();
            }

        }

        return $data;

    }

    //-----------------------------------------------------------------------------------------------------------------

    public function getBondByLease($lease_id)
    {
        $this->db->select('*');
        $this->db->from('bond');
        $this->db->where('lease_id', $lease_id);
        $query = $this->db->get();

        $row = $query->row_array();
        return $row;
    }

    public function getBondIdByLease($lease_id)
    {
        $this->db->select('bond_id');
        $this->db->from('bond');
        $this->db->where('lease_id', $lease_id);
        $query = $this->db->get();

        $row = $query->row_array();

        $id = false;

        if (!empty($row)) {
            $id = $row['bond_id'];
        }

        return $id;
    }

    public function getBondRecieveImagesByLease($lease_id)
    {
        $bond_id = $this->getBondIdByLease($lease_id);
        $rows = null;

        if ($bond_id) {
            $this->db->select('*');
            $this->db->from('bond_recieve_image');
            $this->db->where('bond_id', $bond_id);
            $this->db->where('status', 1);
            $query = $this->db->get();

            $rows = $query->result_array();
        }

        return $rows;

    }

    public function getBondReturnImagesByLease($lease_id)
    {
        $bond_id = $this->getBondIdByLease($lease_id);
        $rows = null;

        if ($bond_id) {
            $this->db->select('*');
            $this->db->from('bond_return_image');
            $this->db->where('bond_id', $bond_id);
            $this->db->where('status', 1);
            $query = $this->db->get();

            $rows = $query->result_array();
        }

        return $rows;

    }

    //-------------------------------------
    public function getAllLeaseCount($only_active)
    {
        $this->db->select('*');
        $this->db->from('lease');

        if ($only_active) {
            $this->db->where('lease_current_status', 1);
        }

        $query = $this->db->get();
        return $query->num_rows();
    }

    public function getAllLease($only_active)
    {
        $this->db->select('*');
        $this->db->from('lease');

        if ($only_active) {
            $this->db->where('lease_current_status', 1);
        }

        $query = $this->db->get();
        $rows = $query->result_array();
        return $rows;
    }

    public function getAllLeaseIds($only_active)
    {
        $ids = null;

        $this->db->select('lease_id');
        $this->db->from('lease');

        if ($only_active) {
            $this->db->where('lease_current_status', 1);
        }

        $query = $this->db->get();
        $rows = $query->result_array();

        if (!empty($rows)) {
            $ids = array_column($rows, 'lease_id');
        }

        return $ids;
    }

    public function countStatementByMonth($month)
    {
        $this->db->select('*');
        $this->db->from('lease_monthly_statement');
        $this->db->where('lease_monthly_statement_month', $month);

        $query = $this->db->get();
        return $query->num_rows();
    }

    public function getInspectionIdsForNotification($notification_count, $limit, $day_before)
    {
        $ids = null;

        $date = date('Y-m-d');
        $chk_date = date('Y-m-d', strtotime("+{$day_before} day", strtotime($date)));

        $this->db->select('inspection_id');
        $this->db->from('inspection');

        $this->db->where('inspection_status', 0);
        $this->db->where('inspection_notification_count', $notification_count);
        $this->db->where('inspection_date', $chk_date);
        $this->db->limit($limit);

        $query = $this->db->get();
        $rows = $query->result_array();

        if (!empty($rows)) {
            $ids = array_column($rows, 'inspection_id');
        }

        return $ids;
    }

    public function getInspection($inspection_id)
    {
        $this->db->select('*');
        $this->db->from('inspection');

        $this->db->where('inspection_id', $inspection_id);

        $query = $this->db->get();
        return $query->row_array();
    }

    public function getNotificationListByUser($user_id, $seen, $limit)
    {
        $date = date("Y-m-d H:i:s");
        $this->db->select('*');
        $this->db->from('notification');

        $this->db->where('user_id', $user_id);
        $this->db->where('seen', $seen);
        $this->db->where('meant_to_be_seen_after < ', $date);

        $this->db->order_by('meant_to_be_seen_after');
        $this->db->limit($limit);

        $query = $this->db->get();
        return $query->result_array();
    }

    public function propertyBelongsToUser($property_id, $user_id)
    {
        $bool = false;

        $property_ids = $this->propertyIdsOfAnUser($user_id);

        if (!empty($property_ids) && is_array($property_ids)) {
            if (in_array($property_id, $property_ids)) {
                $bool = true;
            }
        }
        return $bool;
    }

    public function propertyIdsOfAnUser($user_id)
    {
        $ids = null;

        $this->db->select('property_id');
        $this->db->from('property');

        $this->db->where('user_id', $user_id);

        $query = $this->db->get();
        $result = $query->result_array();


        if (!empty($result)) {
            $ids = array_column($result, 'property_id');
        }

        return $ids;
    }

    public function leaseBelongsToOwner($lease_id, $user_id)
    {
        $bool = false;

        $lease_ids = $this->LeaseIdsOfOwner($user_id);

        if (!empty($lease_ids) && is_array($lease_ids)) {
            if (in_array($lease_id, $lease_ids)) {
                $bool = true;
            }
        }
        return $bool;
    }

    public function LeaseIdsOfOwner($user_id)
    {
        $ids = null;
        $result = array();

        $property_ids = $this->propertyIdsOfAnUser($user_id);

        if (!empty($property_ids) && is_array($property_ids)) {
            $this->db->select('lease_id');
            $this->db->from('lease');
            $this->db->where_in('property_id', $property_ids);
            $query = $this->db->get();
            $result = $query->result_array();
        }

        if (!empty($result)) {
            $ids = array_column($result, 'lease_id');
        }

        return $ids;
    }


    public function getArchivedString($_with_ampersand = false)
    {
        $archived_string = "";
        if ($this->isArchived()) {
            $sign = $_with_ampersand ? "&" : "?";
            $archived_string = "{$sign}archived_lease_id={$_GET['archived_lease_id']}";
        }

        return $archived_string;
    }


    public function getArchivedLeaseId()
    {
        $id = null;
        if ($this->isArchived()) {
            $id = $_GET['archived_lease_id'];
        }

        return $id;
    }

    public function isArchived()
    {
        $param_cond = false;
        $db_cond = false;
        if (isset($_GET['archived_lease_id'])) {
            if (!empty($_GET['archived_lease_id']) && is_numeric($_GET['archived_lease_id'])) {
                $param_cond = true;
                $db_cond = $this->isLeaseArchived($_GET['archived_lease_id']);
            }
        }

        return $param_cond && $db_cond ? true : false;

    }

    public function isLeaseArchived($lease_id)
    {
        $this->db->select('*');
        $this->db->from('lease');

        $this->db->where('lease_id', $lease_id);
        $this->db->where('lease_current_status', 0);

        $query = $this->db->get();
        $num_rows = $query->num_rows();

        return $num_rows > 0 ? true : false;
    }

    public function get_maintenance_related_tax_codes_in_expense()
    {
        $maintenance_related_tax_codes = array();

        $maintenance_related_tax_codes[] = "general repairs";
        $maintenance_related_tax_codes[] = "plumbing";
        $maintenance_related_tax_codes[] = "electricity";

        return $maintenance_related_tax_codes;

    }

    //------------------------------------------------------------------------------------------------------------------

    public function does_user_need_to_subscribe($user_id)
    {
        $ns = true;

        $customer_exists = $this->stripe_customer_exists($user_id);
        $subscription_exists = $this->stripe_customer_subscription_exists($user_id);

        if ($customer_exists && $subscription_exists) {
            $ns = false;
        }

        return $ns;
    }

    public function stripe_customer_exists($user_id)
    {
        $exists = false;

        $user_stripe_customer = $this->user_stripe_customer($user_id);
        if (!empty($user_stripe_customer)) {
            if (!empty($user_stripe_customer['stripe_customer_id'])) {
                $exists = true;
            }
        }
        return $exists;
    }

    public function stripe_customer_subscription_exists($user_id)
    {
        $exists = false;

        $user_stripe_customer_subscription = $this->user_stripe_customer_subscription($user_id);
        if (!empty($user_stripe_customer_subscription)) {
            if (!empty($user_stripe_customer_subscription['stripe_subscription_id'])) {
                $exists = true;
            }
        }
        return $exists;
    }


    public function user_stripe_customer($user_id)
    {
        $this->db->select('*');
        $this->db->from('stripe_customer');
        $this->db->where('user_id', $user_id);
        $query = $this->db->get();

        $row = $query->row_array();
        return $row;
    }

    public function user_stripe_customer_subscription($user_id)
    {
        $this->db->select('*');
        $this->db->from('stripe_customer_subscription');
        $this->db->where('user_id', $user_id);
        $query = $this->db->get();

        $row = $query->row_array();
        return $row;
    }

    public function user_stripe_customer_subscription_id($user_id)
    {
        $id = null;
        $this->db->select('stripe_subscription_id');
        $this->db->from('stripe_customer_subscription');
        $this->db->where('user_id', $user_id);
        $query = $this->db->get();

        $row = $query->row_array();
        if (!empty($row)) {
            $id = !empty($row['stripe_subscription_id']) ? $row['stripe_subscription_id'] : null;
        }
        return $id;
    }

    public function user_id_by_stripe_subscription($stripe_subscription_id)
    {
        $id = false;
        $this->db->select('user_id');
        $this->db->from('stripe_customer_subscription');
        $this->db->where('stripe_subscription_id', $stripe_subscription_id);
        $query = $this->db->get();

        $row = $query->row_array();
        if (!empty($row)) {
            $id = !empty($row['user_id']) ? $row['user_id'] : false;
        }
        return $id;
    }


    public function getSettings($type, $key, $only_value)
    {
        $ret = null;

        $this->db->select('*');
        $this->db->from('settings');

        $this->db->where('settings_type', $type);
        $this->db->where('settings_key', $key);

        $query = $this->db->get();

        $row = $query->row_array();

        if (!empty($row)) {
            $ret = $row['settings_val'];
        }

        return $only_value ? $ret : $row;

    }

    public function get_stripe_test_mode()
    {
        $test_mode = 'on';
        $res = $this->getSettings('stripe', 'test_mode', $only_value = true);

        if (!empty($res)) {
            $test_mode = $res;
        }

        return $test_mode;
    }

    public function get_stripe_secret_key()
    {
        $test_mode = $this->get_stripe_test_mode();
        $key = $test_mode == 'on' ? 'tsk' : 'lsk';
        return $this->getSettings('stripe', $key, $only_value = true);

    }

    public function get_stripe_public_key()
    {
        $test_mode = $this->get_stripe_test_mode();
        $key = $test_mode == 'on' ? 'tpk' : 'lpk';
        return $this->getSettings('stripe', $key, $only_value = true);

    }

    public function getPlans()
    {
        $plans = array();

        $monthly_plan_1_id = $this->utility_model->getSettings("stripe", "monthly_plan_1_id", true);//only value
        $monthly_plan_1_name = $this->utility_model->getSettings("stripe", "monthly_plan_1_name", true);//only value
        $yearly_plan_1_id = $this->utility_model->getSettings("stripe", "yearly_plan_1_id", true);//only value
        $yearly_plan_1_name = $this->utility_model->getSettings("stripe", "yearly_plan_1_name", true);//only value

        if (!empty($monthly_plan_1_id) && !empty($monthly_plan_1_name)) {
            $plans[$monthly_plan_1_id] = $monthly_plan_1_name;
        }

        if (!empty($yearly_plan_1_id) && !empty($yearly_plan_1_name)) {
            $plans[$yearly_plan_1_id] = $yearly_plan_1_name;
        }

        return $plans;
    }

    public function is_user_inactive($user_id)
    {
        $is_inactive = false;
        $user = $this->getUser($user_id);

        if (!empty($user)) {
            if ($user['user_status'] == 0) {
                $is_inactive = true;
            }
        }

        return $is_inactive;
    }

    public function get_software_validity($user_id)
    {
        $validity = false;
        $user = $this->getUser($user_id);

        if (!empty($user)) {
            if ($user['software_is_valid'] == 1) {
                $validity = true;
            } else if ($user['software_is_valid'] == 2 && $user['software_is_valid_till_date'] != null) {

                if (date('Y-m-d') <= $user['software_is_valid_till_date']) {
                    $validity = true;
                }

            }
        }

        return $validity;
    }

    public function get_software_validity_text($user_id)
    {
        $text = "The software is not valid to use currently";
        $user = $this->getUser($user_id);

        if (!empty($user)) {
            if ($user['software_is_valid'] == 1) {
                $text = "The software is valid to use currently";
            } else if ($user['software_is_valid'] == 2 && $user['software_is_valid_till_date'] != null) {

                if (date('Y-m-d') <= $user['software_is_valid_till_date']) {
                    $formated_date =
                    $formated_date = date_format(date_create_from_format('Y-m-d', $user['software_is_valid_till_date']), 'F j,Y');
                    $text = "The software is valid to use until $formated_date";
                }

            }
        }

        return $text;
    }

    //https://joshtronic.com/2013/09/23/sorting-associative-array-specific-key/
    public function sortBy($field, &$array, $direction = 'asc')
    {
        usort($array, create_function('$a, $b', '
        $a = $a["' . $field . '"];
        $b = $b["' . $field . '"];

        if ($a == $b) return 0;

        $direction = strtolower(trim($direction));

        return ($a ' . ($direction == 'desc' ? '>' : '<') . ' $b) ? -1 : 1;
    '));

        return true;
    }

    public function get_rent_payment_ref($id)
    {
        return $this->my_simple_crypt($id, "e");
    }

    public function my_simple_crypt($string, $action = 'e')
    {
        // you may change these values to your own
        $secret_key = 'my_simple_secret_key';
        $secret_iv = 'my_simple_secret_iv';

        $output = false;
        $encrypt_method = "AES-256-CBC";
        $key = hash('sha256', $secret_key);
        $iv = substr(hash('sha256', $secret_iv), 0, 16);

        if ($action == 'e') {
            $output = base64_encode(openssl_encrypt($string, $encrypt_method, $key, 0, $iv));
        } else if ($action == 'd') {
            $output = openssl_decrypt(base64_decode($string), $encrypt_method, $key, 0, $iv);
        }

        return $output;
    }

    public function calculate_rent_status_by_lease($lease_id)
    {
        $curr_date = date("Y-m-d");

        $lps = $this->get_lease_payment_scedule($lease_id, false, $curr_date, "");
        $lps_till_curr_date = $this->get_lease_payment_scedule($lease_id, true, $curr_date, $mode = "<=");
        $lps_after_curr_date = $this->get_lease_payment_scedule($lease_id, true, $curr_date, $mode = ">");

        $rent_status = array();
        $rent_status['amount'] = 0.00;
        $rent_status['status'] = "Unknown";
        $rent_status['label'] = "primary";

        if (empty($lps)) {
            return $rent_status;
        }
        //-------------------------------------

        $total_due = array_sum(array_column($lps, 'payment_due_amount'));
        $total_received = array_sum(array_column($lps, 'lease_rent_recieved'));

        $total_due_till_curr_date = 0.00;
        $total_received_till_curr_date = 0.00;

        $total_due_after_curr_date = 0.00;
        $total_received_after_curr_date = 0.00;

        if (!empty($lps_till_curr_date)) {
            $total_due_till_curr_date = array_sum(array_column($lps_till_curr_date, 'payment_due_amount'));
            $total_received_till_curr_date = array_sum(array_column($lps_till_curr_date, 'lease_rent_recieved'));
        }

        if (!empty($lps_after_curr_date)) {
            $total_due_after_curr_date = array_sum(array_column($lps_after_curr_date, 'payment_due_amount'));
            $total_received_after_curr_date = array_sum(array_column($lps_after_curr_date, 'lease_rent_recieved'));
        }

        //--------------------------------------------------------------------------------------------------------------

        if ($total_due_till_curr_date == $total_received_till_curr_date) {
            $rent_status['amount'] = 0.00;
            $rent_status['status'] = "Up to date";
            $rent_status['label'] = "primary";
        }

        if ($total_due_till_curr_date > $total_received_till_curr_date) {
            $rent_status['amount'] = $total_due_till_curr_date - $total_received_till_curr_date;
            $rent_status['status'] = "Arrears";
            $rent_status['label'] = "danger";
        }

        if ($total_received_after_curr_date > 0) {
            $rent_status['amount'] = $total_received_after_curr_date;
            $rent_status['status'] = "Ahead";
            $rent_status['label'] = "success";
        }

        return $rent_status;

    }

    public function get_lease_payment_scedule($lease_id, $partial, $date, $mode)
    {
        $this->db->select('*');
        $this->db->from('lease_payment_scedule');
        $this->db->where('lease_id', $lease_id);
        if ($partial) {
            $this->db->where("payment_due_date{$mode}", $date);
        }

        $query = $this->db->get();

        return $query->result_array();

    }

    public function check_auth()
    {
        if(!$this->session->userdata('user_id'))
        {
            $this->session->set_userdata('log_err','Login First');
            redirect('login','refresh');
        }

        if ($this->is_user_inactive($this->session->userdata('user_id'))) {
            $this->session->set_flashdata('deactivated', 'User is deactivated');
            redirect('login');
        }

        if (!$this->get_software_validity($this->session->userdata('user_id'))) {
            redirect('subscribe');
        }
    }


}