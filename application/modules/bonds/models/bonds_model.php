<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Bonds_model extends CI_Model
{
    ////// Basic Model Function Starts ///////
    public function insert($table_name,$data)
    {
        $this->db->insert($table_name, $data);
        return TRUE;
    }

    public function insert_ret($tablename, $tabledata)
    {
        $this->db->insert($tablename, $tabledata);
        return $this->db->insert_id();
    }

    public function update_function($columnName, $columnVal, $tableName, $data)
    {
        $this->db->where($columnName, $columnVal);
        $this->db->update($tableName, $data);
    }

     /////////////////////


     public function update_bonds($data)
     {
      
        $this->db->set('bond_amount', $data['bond_amount']);
         $this->db->set('lodge_start_date', $data['lodge_start_date']);
         $this->db->set('lodged_with', $data['lodged_with']);
         $this->db->set('bond_start_add_note', $data['bond_start_add_note']);        
         $this->db->set('bond_tenant_return', $data['bond_tenant_return']);
         $this->db->set('bond_lanlord_keep', $data['bond_lanlord_keep']);
         $this->db->set('lodge_return_date', $data['lodge_return_date']);
         $this->db->set('bond_return_add_note', $data['bond_return_add_note']); 
         $this->db->where('lease_id', $data['lease_id']);
         return  $query=$this->db->update('bond');
     }

    //////////////////

   


    public function delete_function_cond($tableName, $cond)
    {
        $where = '( ' . $cond . ' )';
        $this->db->where($where);
        $this->db->delete($tableName);
    }
    public function delete_function($tableName, $columnName, $columnVal)
    {
        $this->db->where($columnName, $columnVal);
        $this->db->delete($tableName);
    }

    public function select_all($table_name)
    {
        $this->db->select('*');
        $this->db->from($table_name);
        $query = $this->db->get();
        $result = $query->result_array();
        return $result;
    }

    public function select_all_name_ascending($col_name,$table_name)
    {
        $this->db->select('*');
        $this->db->from($table_name);
        $this->db->order_by($col_name,'ASC');
        $query = $this->db->get();
        $result = $query->result_array();
        return $result;
    }

    public function select_all_decending($table_name)
    {
        $this->db->select('*');
        $this->db->from($table_name);
        $this->db->order_by('created_at','DESC');
        $query = $this->db->get();
        $result = $query->result_array();
        return $result;
    }

    public function union_query()
    {
        $this->db->select('chamber.name as chamber_name,divisions.name as division_name,districts.name as district_name,area_title');
        $this->db->from('chamber_name');
        $this->db->join('country','country.id=chamber_name.country_id','LEFT');
        $this->db->join('divisions','divisions.id=chamber_name.division_id','LEFT');
        $this->db->join('districts','districts.id=chamber_name.district_id','LEFT');
        $this->db->join('area','area.area_id=chamber_name.area_id','LEFT');
        $this->db->get();
        $query1 = $this->db->last_query();

        $this->db->select('registration.name as chamber_name,divisions.name as division_name,districts.name as district_name,area_title');
        $this->db->from('registration');
        $this->db->join('login','login.id=registration.login_id');
        $this->db->join('country','country.id=registration.country_id','LEFT');
        $this->db->join('divisions','divisions.id=registration.division_id','LEFT');
        $this->db->join('districts','districts.id=registration.district_id','LEFT');
        $this->db->join('area','area.area_id=registration.area_id','LEFT');
        $this->db->where('login.verify_status',1);
        $this->db->where('login.user_type',9);
        $this->db->get();
        $query2 = $this->db->last_query();
        $query = $this->db->query($query1." UNION  ".$query2);
        return $query->result_array();
    }


    public function select_with_where($selector, $condition, $tablename)
    {
        $this->db->select($selector);
        $this->db->from($tablename);
        $where = '(' . $condition . ')';
        $this->db->where($where);
        $result = $this->db->get();
        return $result->result_array();

    }


    public function count_with_where($selector, $condition, $tablename)
    {
        $this->db->select($selector);
        $this->db->from($tablename);
        $where = '(' . $condition . ')';
        $this->db->where($where);
        $result = $this->db->get();
        return $result->num_rows();
    }

    public function select_join($selector,$table_name,$join_table,$join_condition)
    {
        $this->db->select($selector);
        $this->db->from($table_name);
        $this->db->join($join_table,$join_condition);
        $result=$this->db->get();
        return $result->result_array();
    }

    public function select_left_join($selector,$table_name,$join_table,$join_condition)
    {
        $this->db->select($selector);
        $this->db->from($table_name);
        $this->db->join($join_table,$join_condition,'left');
        $result=$this->db->get();
        return $result->result_array();
    }



    public function select_where_join($selector,$table_name,$join_table,$join_condition,$condition)
    {
        $this->db->select($selector);
        $this->db->from($table_name);
        $this->db->join($join_table,$join_condition);
        $where = '(' . $condition . ')';
        $this->db->where($where);
        //$this->db->order_by($order_col,$order_action);
        $result=$this->db->get();
        return $result->result_array();
    }



    public function select_where_left_join($selector,$table_name,$join_table,$join_condition,$condition)
    {
        $this->db->select($selector);
        $this->db->from($table_name);
        $this->db->join($join_table,$join_condition,'left');
        $where = '(' . $condition . ')';
        $this->db->where($where);
        //$this->db->order_by($order_col,$order_action);
        $result=$this->db->get();
        return $result->result_array();
    }

    public function get_userlist_with_address($condition)
    {
        $this->db->select('country.*,country.name as country_name,divisions.*,divisions.name as division_name,districts.*,districts.name as district_name,area.*,login.id as log_id,registration.id as reg_id,registration.name');
        $this->db->from('registration');
        $this->db->join('login','login.id=registration.login_id');
        $this->db->join('country','country.id=registration.country_id','LEFT');
        $this->db->join('divisions','divisions.id=registration.division_id','LEFT');
        $this->db->join('districts','districts.id=registration.district_id','LEFT');
        $this->db->join('area','area.area_id=registration.area_id','LEFT');
        $where = '(' . $condition . ')';
        $this->db->order_by('registration.name','ASC');
        $this->db->where($where);
        $result=$this->db->get();
        return $result->result_array();
    }


    public function get_individual_address($condition)
    {
        $this->db->select('country.*,country.name as country_name,divisions.*,divisions.name as division_name,districts.*,districts.name as district_name,area.*');
        $this->db->from('registration');
        $this->db->join('country','country.id=registration.country_id','LEFT');
        $this->db->join('divisions','divisions.id=registration.division_id','LEFT');
        $this->db->join('districts','districts.id=registration.district_id','LEFT');
        $this->db->join('area','area.area_id=registration.area_id','LEFT');
        $where = '(' . $condition . ')';
        $this->db->where($where);
        $result=$this->db->get();
        return $result->result_array();
    }



    public function get_text_consult_list($condition)
    {
        $this->db->select('doctor_package_list.package_title,consult_text.group_id,consult_text.status as chat_active,r1.name as doctor_name,r2.name as patient_name');
        $this->db->from('consult_text');
        $this->db->join('buy_package','buy_package.id=consult_text.buy_package_id');
        $this->db->join('doctor_package','doctor_package.id=buy_package.dr_package_id');
        $this->db->join('doctor_package_list','doctor_package_list.id=doctor_package.package_id');
        $this->db->join("login l1", "l1.id =doctor_package.doctor_id");
        $this->db->join("login l2", "l2.id = buy_package.patient_id");
        $this->db->join("registration r1", "r1.login_id =l1.id");
        $this->db->join("registration r2", "r2.login_id = l2.id");
        $where = '(' . $condition . ')';
        $this->db->where($where);
        $this->db->group_by('consult_text.group_id');
        $this->db->order_by('consult_text.created_at','DESC');

        $result=$this->db->get();
        return $result->result_array();
    }


    public function get_text_consult_details($condition)
    {
        $this->db->select('consult_text.status as chat_active,consult_text.message,consult_text.created_at as c_date,r1.name as doctor_name,r2.name as patient_name,l1.user_type as u_type,l2.user_type as u1_type, consult_text.chat_from, consult_text.chat_to,doctor_package.doctor_id,buy_package.patient_id,r1.profile_image as doctor_image,r2.profile_image as patient_image');
        $this->db->from('consult_text');
        $this->db->join('buy_package','buy_package.id=consult_text.buy_package_id');
        $this->db->join('doctor_package','doctor_package.id=buy_package.dr_package_id');
        $this->db->join('doctor_package_list','doctor_package_list.id=doctor_package.package_id');
        $this->db->join("login l1", "l1.id =doctor_package.doctor_id");
        $this->db->join("login l2", "l2.id = buy_package.patient_id");
        $this->db->join("registration r1", "r1.login_id =l1.id");
        $this->db->join("registration r2", "r2.login_id = l2.id");
        $where = '(' . $condition . ')';
        $this->db->where($where);
        $result=$this->db->get();
        return $result->result_array();
    }
    


     public function select_where_two_join($selector,$table_name,$join_table,$join_condition,$join_table2,$join_condition2,$condition)
    {
        $this->db->select($selector);
        $this->db->from($table_name);
        $this->db->join($join_table,$join_condition);
        $this->db->join($join_table2,$join_condition2);
        $where = '(' . $condition . ')';
        $this->db->where($where);
        //$this->db->order_by($order_col,$order_action);
        $result=$this->db->get();
        return $result->result_array();
    }


      public function select_where_join_order_by($selector,$table_name,$join_table,$join_condition,$condition,$order_col,$order_action)
    {
        $this->db->select($selector);
        $this->db->from($table_name);
        $this->db->join($join_table,$join_condition);
        $where = '(' . $condition . ')';
        $this->db->where($where);
        $this->db->order_by($order_col,$order_action);
        $result=$this->db->get();
        return $result->result_array();
    }

    ////// Basic Model Function End ///////


    public function exist_email($email)
    {
        $this->db->select('email');
        $this->db->from('login');
        $this->db->where('email',$email);
        $result=$this->db->get();
        return $result->result_array();
    }

    public function get_single_row($table,$condition,$order_col,$order_type)
    {
        $this->db->select('*');
        $this->db->from($table);
        $this->db->order_by($order_col,$order_type);
        $where = '(' . $condition . ')';
        $this->db->where($where);
        $this->db->limit(1);
        $query = $this->db->get();
        $result = $query->result_array();
        return $result;
    }

     public function get_last_sell_code()
    {
        $this->db->select('sell_code');
        $this->db->from('sell');
        $this->db->order_by('sell_id',"desc");
        $this->db->limit(1);
        $query = $this->db->get();
        $result = $query->result_array();
        return $result;
    }


     public function get_last_buy_code()
    {
        $this->db->select('buy_code');
        $this->db->from('buy');
        $this->db->order_by('buy_id',"desc");
        $this->db->limit(1);
        $query = $this->db->get();
        $result = $query->result_array();
        return $result;
    }

    public function get_product_details($p_id)
    {
        $this->db->select('*');
        $this->db->from('product');
        $this->db->where('p_id',$p_id); 
        $query = $this->db->get();
        $result = $query->result_array();
        return $result;
    }


    public function columns($database, $table)
    {
        //$query = "SELECT COLUMN_NAME, DATA_TYPE, IS_NULLABLE, COLUMN_DEFAULT, COLUMN_TYPE FROM INFORMATION_SCHEMA.COLUMNS  WHERE table_name = '$table'AND table_schema = '$database'";  
        $query = "SELECT COLUMN_NAME FROM INFORMATION_SCHEMA.COLUMNS 
            WHERE table_name = '$table'
            AND table_schema = '$database'";    
        $result = $this->db->query($query) or die ("Schema Query Failed"); 
        $result=$result->result_array();
        return $result;
    }

    
    ///  For Union Two table
     function get_merged_result()
    {                   
        $this->db->select("name,id,phone_second as web");
       // $this->db->distinct();
        $this->db->from("registration");
        //$this->db->where_in("id",$model_ids);
        $this->db->get(); 
        $query1 = $this->db->last_query();

        $this->db->select("name,id,website as web");
       // $this->db->distinct();
        $this->db->from("company");
       // $this->db->where_in("id",$model_ids);

        $this->db->get(); 
        $query2 =  $this->db->last_query();
        $query = $this->db->query($query1." UNION ".$query2);

        return $query->result_array();
    }



}
?>