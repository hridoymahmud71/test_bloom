<?php $this->load->view('front/headlink'); ?>

            <!-- Modal -->
         
            <h4>Current lease: dfd (365 days to go) <a href="#" data-toggle="modal" data-target="#myModal"><span class="glyphicon glyphicon-cog" aria-hidden="true"></span></a></h4>
             <div class="modal fade" id="myModal" role="dialog">
            <div class="modal-dialog">
            <form method="post" action="bonds/add_bonds">
              <!-- Modal content-->
              <div class="modal-content">
                <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal">&times;</button>
                  <h4 class="modal-title">Select a Lease</h4>
                </div>
                <div class="modal-body">
                  <p>Lease Option:</p>
                  <select name="lease_id">
                <?php
                foreach($lease_option as $data){ 
                ?>
                    <option value="<?php  echo $data['lease_id']; ?>"><?php  echo $data['lease_id']; ?></option>
               <?php } ?>
                 </select>

                </div>
                <div class="modal-footer">
                  <button type="submit" class="btn btn-success">Confirm</button>
                </div>
              </div> 
                 </form>            
            </div>
          </div>

       


        <div class="row">  
            <div class="ss_container">
                <h2>Bonds</h2>
                <div class="ss_bound_content">
                        
                          <!-- Nav tabs -->
                          <ul class="nav nav-tabs" role="tablist">
                            <li>View:</li>
                            <li role="presentation" class="active"><a href="#startlease" aria-controls="startlease" role="tab" data-toggle="tab">Start of lease</a></li>
                            <li role="presentation"><a href="#endlease" aria-controls="profile" role="tab" data-toggle="tab">End of lease</a></li>
                           </ul>
                        

                          <!-- Tab panes -->
                          <div class="tab-content">
                            <div role="tabpanel" class="tab-pane active" id="startlease">
                                <h3>Receiving the bond</h3>

             <?php  foreach($result as $data){ } ?>



                                <form class="col-md-10  offset-md-2" method="post" action="bonds/update_bonds" enctype="multipart/form-data" id="myForm">
                                        <div class="form-group col-md-12">
                                                <div class=" col-sm-4"><label for="bondamount">Bond amount</label></div>
                                                <div class=" col-sm-8"><input type="number" name="bond_amount" class="form-control" value="<?php echo $data['bond_amount']; ?>" id="bondamount" placeholder="$0.00"></div>
                                        </div>
                                         
                                        <div class="form-group col-md-12">
                                                <div class=" col-sm-4"><label for="bondamount">Date lodged</label></div>
                                                <div class=" col-sm-8"><div id="fromDate3" class="input-group date" data-date-format="yyyy-mm-dd">
                                                        <input class="form-control" name="lodge_start_date" id="lodge_start_date" value="<?php echo $data['lodge_start_date']; ?>" type="text" readonly />
                                                        <span class="input-group-addon"><i class="glyphicon glyphicon-calendar"></i></span>
                                                    </div></div>
                                        </div>
                                        <div class="form-group col-md-12">
                                                <div class=" col-sm-4"> <label for="bondamount">Lodged with</label></div>
                                                <div class=" col-sm-8"><input type="text" name="lodged_with" class="form-control" value="<?php echo $data['lodged_with']; ?>" id="lodgedwith" placeholder=""></div>
                                        </div>
                                        
                                        <div class="form-group col-md-12">
                                                <div class=" col-sm-4"><label for="bondamount">Additional notes</label></div>
                                                <div class=" col-sm-8"><textarea class="form-control col-md-12" name="bond_start_add_note" rows="3" ><?php echo $data['bond_start_add_note'] ?></textarea></div>
                                       <input type="hidden" name="lease_id" value="<?php echo $data['lease_id']; ?>">
                                        </div> 
                                        <div class="form-group clear">
                                               <div class=" col-sm-4"> <label for="bondamount">Upload bond documents</label></div>
                                               
                                                        
                                                <div class="col-sm-8 ">
                                                            <div class="form-group inputDnD">
                                                               <input type="file" name="bond_start_file" class="form-control-file text-primary font-weight-bold" id="inputFile" accept="image/*" onchange="readUrl(this)" data-title="Drag and drop a file">
                                                            </div>


                                                </div>
                                                       
                                                  
                                        </div>
                                        <div class="modal-footer  clear">
                                            <br/><br/>
                                                <button type="button" class="btn btn-light" data-dismiss="modal" >Close</button>
                                                <button type="submit" class="btn btn-primary "> Save changes</button>

                                                <br/><br/>
                                              </div>
                                     

                            </div>
                            <div role="tabpanel" class="tab-pane" id="endlease">
                                    <h3>Returning the bond</h3>
                                    <div class="col-md-10  offset-md-2">
                                            <div class="form-group col-md-12">
                                                    <div class=" col-sm-4"><label for="bondamount">Returning to tenants</label></div>
                                                    <div class=" col-sm-8"><input type="number" name="bond_tenant_return" class="form-control" id="bondamount" placeholder="$0.00" value="<?php echo $data['bond_tenant_return']; ?>"></div>
                                            </div>
                                            <div class="form-group col-md-12">
                                                    <div class=" col-sm-4"><label for="bondamount">Landlord is keeping</label></div>
                                                    <div class=" col-sm-8"><input type="number" name="bond_lanlord_keep" class="form-control" id="bondamount" placeholder="$0.00" value="<?php echo $data['bond_lanlord_keep']; ?>" ></div>
                                            </div>
                                            <div class="form-group col-md-12">
                                                    <div class=" col-sm-4"><label for="bondamount">Date lodged</label></div>
                                                    <div class=" col-sm-8"><div id="fromDate2" class="input-group date" data-date-format="yyyy-mm-dd">
                                                            <input class="form-control" id="lodge_return_date" value="<?php echo $data['lodge_return_date']; ?>" name="lodge_return_date" type="text" readonly />
                                                            <span class="input-group-addon"><i class="glyphicon glyphicon-calendar"></i></span>

                                                             <span class="" id="maintainance_case" style="color:red"></span>
                                                        </div></div>
                                            </div>
                                            
                                            
                                            <div class="form-group col-md-12">
                                                    <div class=" col-sm-4"><label for="bondamount">Additional notes</label></div>
                                                    <div class=" col-sm-8"><textarea class="form-control col-md-12" rows="3" name="bond_return_add_note" ><?php echo $data['bond_return_add_note']; ?></textarea></div>
                                            </div> 
                                            <div class="form-group clear">
                                                   <div class=" col-sm-4"> <label for="bondamount">Upload bond documents</label></div>
                                                   
                                                            
                                                    <div class="col-sm-8 ">
                                                                 <div class="form-group inputDnD">
                                                                   <input type="file" name="bond_return_file" class="form-control-file text-primary font-weight-bold" id="inputFile" accept="image/*" onchange="readUrl(this)" data-title="Drag and drop a file">
                                                                </div>                                                             
                                                    </div>                                                          
                                                      
                                            </div>
                                            <div class="modal-footer  clear">
                                                <br/><br/>
                                                    <button type="button" class="btn btn-light" data-dismiss="modal" >Close</button>
                                                    <button type="submit" class="btn btn-primary "> Save changes</button>
    
                                                    <br/><br/>
                                                  </div>
                                    </div> 
                                   </form>         
                            </div>
                          </div>
                        
                        </div>
            </div>
        </div>    
        </div>
        </div> 
    
    </div>

 <?php $this->load->view('front/footerlinkk'); ?> 
