<?php $this->load->view('front/headlink'); ?>
<h4>Current lease: dfd (365 days to go) <a href="#" data-toggle="modal" data-target="#myModal"><span class="glyphicon glyphicon-cog" aria-hidden="true"></span></a></h4>
<div class="modal fade" id="myModal" role="dialog">
  <div class="modal-dialog">
    <form method="post" action="bonds/add_bonds">
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Select a Lease</h4>
        </div>
        <div class="modal-body">
          <p>Lease Option:</p>
          <select name="lease_id">
            <?php
            foreach($lease_option as $data){ 
              ?>
              <option value="<?php  echo $data['lease_name']; ?>"><?php  echo $data['lease_name']; ?></option>
              <!-- <option value=""><?php  echo "Add New Lease" ?></option> -->
              <?php } ?>
            </select>
          </div>
          <div class="modal-footer">
            <button type="submit" class="btn btn-success">Confirm</button>
          </div>
        </div> 
      </form>            
    </div>
  </div>
  <div class="row">  
    <div class="ss_container">
      <h2>Bonds</h2>
      <div class="ss_bound_content">
        <!-- Nav tabs -->
        <ul class="nav nav-tabs" role="tablist">
          <li>View:</li>
          <li role="presentation" class="active"><a href="#startlease" aria-controls="startlease" role="tab" data-toggle="tab">Start of lease</a></li>
          <li role="presentation"><a href="#endlease" aria-controls="profile" role="tab" data-toggle="tab">End of lease</a></li>
        </ul>
        <!-- Tab panes -->
        <div class="tab-content">
          <form class="col-md-10  offset-md-2" method="post" action="bonds/save_bonds" enctype="multipart/form-data">
            <div role="tabpanel" class="tab-pane active" id="startlease">
            <h3>Receiving the bond</h3>
              <div class="form-group col-md-12">
                <div class=" col-sm-4"><label for="bondamount">Bond amount</label></div>
                <div class=" col-sm-8"><input type="text"  name="bond_amount" class="form-control wheelable" id="bondamount" placeholder="$0.00"></div>
              </div>
              <div class="form-group col-md-12">
                <div class=" col-sm-4"><label for="bondamount">Date lodged</label></div>
                <div class=" col-sm-8"><div id="fromDate3" class="input-group date" data-date-format="dd-mm-yyyy">
                  <input class="form-control" name="lodge_start_date" type="text" readonly />
                  <span class="input-group-addon"><i class="glyphicon glyphicon-calendar"></i></span>
                </div></div>
              </div>
              <div class="form-group col-md-12">
                <div class=" col-sm-4"> <label for="bondamount">Lodged with</label></div>
                <div class=" col-sm-8"><input type="text" name="lodged_with" class="form-control" id="lodgedwith" placeholder=""></div>
              </div>
              <div class="form-group col-md-12">
                <div class=" col-sm-4"><label for="bondamount">Additional notes</label></div>
                <div class=" col-sm-8"><textarea class="form-control col-md-12" name="bond_start_add_note" rows="3" ></textarea></div>
              </div> 
              <div class="form-group clear">
                <div class=" col-sm-4"> <label for="bondamount">Upload bond documents</label></div>
                <div class="col-sm-8 ">
                  <div class="form-group inputDnD">
                    <input type="file" name="bond_start_file" class="form-control-file text-primary font-weight-bold" id="inputFile" accept="image/*" onchange="readUrl(this)" data-title="Drag and drop a file">
                  </div>
                </div>
              </div>
              <div class="modal-footer  clear">
                <br/><br/>
                <button type="button" class="btn btn-light" data-dismiss="modal" >Close</button>
                <button type="submit" class="btn btn-primary "> Save changes</button>
                <br/><br/>
              </div>
            </div>
            <div role="tabpanel" class="tab-pane" id="endlease">
              <h3>Returning the bond</h3>
              <div class="col-md-10  offset-md-2">
                <div class="form-group col-md-12">
                  <div class=" col-sm-4"><label for="bondamount">Returning to tenants</label></div>
                  <div class=" col-sm-8"><input type="number" name="bond_tenant_return" class="form-control" id="bondamount" placeholder="$0.00"></div>
                </div>
                <div class="form-group col-md-12">
                  <div class=" col-sm-4"><label for="bondamount">Landlord is keeping</label></div>
                  <div class=" col-sm-8"><input type="number" name="bond_lanlord_keep" class="form-control" id="bondamount" placeholder="$0.00"></div>
                </div>
                <div class="form-group col-md-12">
                  <div class=" col-sm-4"><label for="bondamount">Date lodged</label></div>
                  <div class=" col-sm-8"><div id="fromDate2" class="input-group date" data-date-format="dd-mm-yyyy">
                    <input class="form-control" name="lodge_return_date" type="text" readonly />
                    <span class="input-group-addon"><i class="glyphicon glyphicon-calendar"></i></span>
                  </div></div>
                </div>
                <div class="form-group col-md-12">
                  <div class=" col-sm-4"><label for="bondamount">Additional notes</label></div>
                  <div class=" col-sm-8"><textarea class="form-control col-md-12" rows="3" name="bond_return_add_note" ></textarea></div>
                </div> 
                <div class="form-group clear">
                  <div class=" col-sm-4"> <label for="bondamount">Upload bond documents</label></div>
                  <div class="col-sm-8 ">
                    <div class="form-group inputDnD">
                      <input type="file" name="bond_return_file" class="form-control-file text-primary font-weight-bold" id="inputFile" accept="image/*" onchange="readUrl(this)" data-title="Drag and drop a file">
                    </div>
                  </div>
                </div>
                <div class="modal-footer  clear">
                  <br/><br/>
                  <button type="button" class="btn btn-light" data-dismiss="modal" >Close</button>
                  <button type="submit" class="btn btn-primary "> Save changes</button>
                  <br/><br/>
                </div>
              </div>
            </div>
            <div style="clear: both;"></div>
            </form>
          </div>
        </div>
      </div>
    </div>
  </div>    
</div>
</div>
</div>

<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-mousewheel/3.1.13/jquery.mousewheel.js" ></script>
<script>
    $(function() {
        var BondAmountIcr = 50.00;

        $("#bondamount").bind("mousewheel", function(event, delta) {

            if(this.value == '' || isNaN(this.value)){
                this.value = 0;
            }

            if (delta > 0) {
                this.value = parseInt(this.value) + BondAmountIcr;
            } else {
                if (parseInt(this.value) > 0) {
                    this.value = parseInt(this.value) - BondAmountIcr;
                }else{
                    alert('Bond Amount Cannot be negative');
                }
            }
            return false;
        });

    });
</script>


<?php $this->load->view('front/footerlinkk'); ?> 