<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Bonds extends MX_Controller {

    //public $counter=0;
    function __construct() {
        parent::__construct();
        $this->load->model('bonds_model');
        $this->load->model('utility/utility_model');
       //$this->load->model('home/home_model');
       // $this->load->helper('inflector');
       // $this->load->library('encrypt');
	   
	    $this->utility_model->check_auth();
		

    }

    public function index()
    {        
    	//echo "abcdef";
		$this->load->view('add_bonds');
    }

    public function add_bonds()
      {
         $property_id = $this->session->userdata('property_id');
         $lease = $this->bonds_model->select_with_where('lease_id', "property_id=$property_id", 'lease');
         
         $lease_id = $lease[0]['lease_id']; 
         //echo $lease_id; die;
         $this->session->set_userdata('lease_id', $lease_id);     
         $data['result'] = $this->bonds_model->select_with_where('*', "lease_id=$lease_id", 'bond');
         $data['lease_option'] = $this->bonds_model->select_with_where('*', "property_id=$property_id", 'lease'); 
         //echo '<pre>'; print_r($data['lease_option']); die;
          
         if($data['result']){
            $this->load->view('update_bond', $data);
          }
          else
          {           
             $this->load->view('add_bonds', $data);
          }        
      }


    public function save_bonds()
    {        
        $property_id = $this->session->userdata('property_id');
        $lease_id = $this->session->userdata('lease_id');
        
        $data['property_id'] = $property_id;
        $data['lease_id'] = $lease_id;
        //$data['user_id'] = 1; //this property_id will be dynamic value //
        $data['user_id'] = $this->session->userdata('user_id');

        $data['bond_amount'] = $this->input->post('bond_amount');
        $data['lodge_start_date'] = $this->input->post('lodge_start_date');
        $data['lodged_with'] = $this->input->post('lodged_with');
        $data['bond_start_add_note'] = $this->input->post('bond_start_add_note');
        //$data['bond_start_file'] = $this->input->post('bond_start_file');
        $data['bond_tenant_return'] = $this->input->post('bond_tenant_return');
        $data['bond_lanlord_keep'] = $this->input->post('bond_lanlord_keep');
        $data['lodge_return_date'] = $this->input->post('lodge_return_date');
        $data['bond_return_add_note'] = $this->input->post('bond_return_add_note');

        $this->form_validation->set_rules('bond_amount', 'Bond Amount', 'trim');
        $this->form_validation->set_rules('lodge_start_date', 'Lodge Start Date', 'trim|required');
        $this->form_validation->set_rules('lodged_with', 'Lodge With', 'trim');
        $this->form_validation->set_rules('bond_start_add_note', 'Bond Start Note', 'trim');

         if($this->form_validation->run()==FALSE)
            { 
              //echo "validation faild";
               redirect('bonds/add_bonds');           
            }

        else
            {
             $result = $this->bonds_model->insert('bond',$data);                   

                if($result){  
                                    
                   redirect('bonds/add_bonds');
                }               
                else{                  
                    $this->session->set_flashdata('errorMessage', 'Something May Wrong!!');
                     redirect('bonds/add_bonds');                 
                }
            }
    }    



     public function update_bonds()
      {        
       
          $data['lease_id'] = $this->input->post('lease_id');
          $data['bond_amount'] = $this->input->post('bond_amount');
          $data['lodge_start_date'] = $this->input->post('lodge_start_date');
          $data['lodged_with'] = $this->input->post('lodged_with');
          $data['bond_start_add_note'] = $this->input->post('bond_start_add_note');
          //$data['bond_start_file'] = $this->input->post('bond_start_file');
          $data['bond_tenant_return'] = $this->input->post('bond_tenant_return');
          $data['bond_lanlord_keep'] = $this->input->post('bond_lanlord_keep');
          $data['lodge_return_date'] = $this->input->post('lodge_return_date');
          $data['bond_return_add_note'] = $this->input->post('bond_return_add_note');
         // $data['bond_return_file'] = $this->input->post('bond_return_file');

          

          $this->form_validation->set_rules('bond_amount', 'Bond Amount', 'trim');
          $this->form_validation->set_rules('lodge_start_date', 'Lodge Start Date', 'trim|required');
          $this->form_validation->set_rules('lodged_with', 'Lodge With', 'trim');
          $this->form_validation->set_rules('bond_start_add_note', 'Bond Start Note', 'trim');
          //$this->form_validation->set_rules('bond_start_file', 'Bond Start File', 'trim');
          
         // $this->form_validation->set_rules('lease_id', 'Lease ID', 'trim|is_unique[bond.lease_id]');
         

          if($this->form_validation->run()==FALSE)
              { 
                echo "validation faild";
                 //redirect('bonds/add_bonds');           
              }

          else
              {
                $result = $this->bonds_model->update_bonds($data);                  

                  if($result){  
                                     
                      redirect('bonds/add_bonds');
                  }               
                  else{
                    
                      $this->session->set_flashdata('errorMessage', 'Something May Wrong!!');
                      redirect('add_new_maintenance');                    
                  }
              }


      } 


      


   

}
