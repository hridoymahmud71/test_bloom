<?php $this->load->view('front/headlink'); ?>
<div class="dashbord_poperty">
  <?php $this->load->view('front/top_menu'); ?>
  <div class="container">
    <?php $this->load->view('front/head_nav'); ?> 
    <div class="row hi salim">  
     <div class="col-sm-8" >
      <div class="ss_dashboart"> 
        <div class="row" id="ss_d_top_content">
          <div class="col-md-4">
            <h2>Rent</h2>
            <h4 class="">$<span class="ss_price_text">0.00</span></h4>
            <p><span id="rent_status" class="label-paid">Up to date</span></p>
            <br/><br/>
            <a href="rent/rent_schedule" class="btn btn btn-primary">Go to schedule</a>
          </div>
          <div class="col-md-4">
            <h2>Expenses</h2>
            <p><strong>Paid</strong> this month <span class="ss_price_text">$0.00</span></p>
            <p><strong>Due</strong> in next 30 days <span class="ss_price_text">$0.00</span></p><br/><br/>
            <a href="expense/view_expense" class="btn btn btn-primary">Go to expenses</a>
          </div>
          <div class="col-md-4">
            <h2>Maintenance</h2>
            <p><strong id="count">0</strong> <span class="m_status m_status-new">New</span></p>
            <p><strong id="active">0</strong> <span class="m_status m_status-active">Active</span></p><br/><br/>
            <a href="maintenance/view_all_maintenance" class="btn btn btn-primary">View All</a>
          </div>
        </div>
        <div class="dashboard_element rent-payments">
          <h1>Rent payments</h1>
          <a href="rent/mark_rent_recive"  class="btn btn btn-light"><span>Mark Rent Received</span></a>
          <a href="rent/rent_schedule" id="by_rent_received" class="btn btn btn-light"><span>Show Rent Received</span></a>
          <div class="clear" style="height: 15px;"></div>
          <ul class="payments_list">
            <li class="tenant_name" id="tenant-name">Your name</li>
            <li>
              <p>
                <strong><span id="tenant-total"></span></strong>
                <span id="tenant-status" class="label label-paid">Up to date</span>
              </p>
            </li>
            <div id="item_1" style="display:none;">
              <a href="#" class="reminder" id="reminder">Send <span id="u-name">Kk</span> a reminder about outstanding rents</a>
            </div>
          </ul>
          <ul id="last_flag1" style="display:none;"></ul>
          <div class="clear"></div>
        </div>
      </div>
    </div>
    <div class=" col-sm-4 ss_side_right">
      <div class="ss_dashboart">
        <a href="#" target="_blank">
          <div class="row">
            <div class="col-sm-4">
              <div class="prop_placeholder_wrapper">
                <img src="<?php base_url(); ?> assets/img/t4.png" class="img-responsive img-circle ">
              </div>
            </div>
            <div class="col-sm-8">
              <p>What's new at RentingSmart?<br>
              Click to find out. We're always adding new features.</p>
            </div>
          </div>
        </a>
      </div>
      <div class="ss_dashboart">
        <a href="lease/lease_details" type="submit" class="btn btn btn-primary ">
          Lease details:
        </a>
        <p>12 February 2018 - 12 February 2019 </p> 
      </div>
      <div class="ss_dashboart">
        <a href="rent/rent_schedule" type="submit" class="btn btn btn-light ">
          Rent details:
        </a>
        <p><span class="ss_price_text">$12.00</span> per week</p>
        <p>Due on monday of each week  Next payment due in 7 days</p> 
      </div>
      <div class="ss_dashboart">
        <a href="lease/add_tenants" type="submit" class="btn btn btn-light ">
          Tenants:
        </a> <div class="row">
          <div class="col-sm-4">
            <div class="prop_placeholder_wrapper">
              <img src="<?php base_url(); ?> assets/img/t4.png" class="img-responsive img-circle ">
            </div>
          </div>
          <div class="col-sm-8">
            <p>Your Name<br>
            Number isn't provided Email isn't provided</p>
          </div>
        </div>
        <br><br>
        <a href="#" type="submit" class="btn btn btn-light ">
          Invite to RentingSmart
        </a>
        <br><br>
      </div> 
    </div>
  </div>    
</div>
</div>	
</div>
<?php 
$user_fname = $this->session->userdata('user_fname');
$user_lname = $this->session->userdata('user_lname');
?>
<?php foreach ($result as $data) { } ?>
<!-- Modal -->
<div class="modal fade" data-backdrop="static" data-keyboard="false" id="property_photos_info" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel"><?php echo $data['property_address']; ?></h4>
      </div>
      <div class="modal-body">
        <form method="POST" action="other/update_property_photos_info/<?=$property_id;?>" enctype="multipart/form-data">
          <?php foreach ($result2 as $data2) {  ?>  
          <span id="pImg<?php echo $data2['property_photos_id']; ?>">
            <img  style="width: 80px; height: 80px;" src="<?php echo base_url(); ?>uploads/property_file/<?php echo $data2['property_photo']; ?>">  
            <!--<span class="cross_2">x</span>   -->                 
            <button type="button" name="delete_btn" data-id3="<?php echo $data2['property_photos_id']; ?>" class="btn btn-xs btn-danger btn_delete">x</button> 
          </span>           
          <?php } ?> <br> <br>
          <label for="add_property_photos"><span>Click to add</span> more photos of your property</label>
          <div class="form-group inputDnD">
           <div class="dropzone" >
            <div class="dz-message" >
              <h3> Drop File</h3>
            </div>
          </div>
          <div class="previews" id="preview"></div>
        </div>
        <div class="row-fluid">
          <div class="col-md-6">
            <label for="owner">Owner of the property (if not name of landlord)</label>
            <input type="text" id="owner" name="owner" value="<?php echo $user_fname; echo " "; echo $user_lname; ?>" class="form-control" disabled="disabled">
          </div>
        </div>
        <div class="row-fluid">
          <div class="col-md-6 address_c">
            <label class="" for="address">Property Address <span>*</span></label>
            <input type="text" id="address" name="property_address" value="<?php echo $data['property_address']; ?>" class="form-control" required>
          </div>
          <div class="col-md-6">
            <label class="" for="city">City/Suburb<span>*</span></label>
            <input type="text" id="city" name="city" value="<?php echo $data['city']; ?>" class="form-control" required>
          </div>
        </div>
        <div class="row-fluid">
          <div class="col-md-4">
            <label class="" for="postal">Zip/Postal Code<span>*</span></label>
            <input type="text" id="postal" name="zip_code" value="<?php echo $data['zip_code']; ?>" class="form-control" required>
          </div>
          <input type="hidden" id="countryValue" name="country_val" value="<?php echo $data['country']; ?>">
          <div class="col-md-4">
            <div class="form-group">
              <label for="inputCountry">Country <span>*</span></label>
              <select class="form-control" name="country" id="inputCountry" onChange="getState(this.value);" required>
                <option value="">Select Country</option>
                <?php foreach($all_country as $row){?>
                <option value="<?=$row['country_id'];?>" <?php if($row['country_id']==$data['country']){echo "selected";} ?>><?=$row['country_name'];?></option>
                <?php } ?>
              </select>
              <strong class="text-danger" id="err_cntry_msg"></strong>
            </div>
          </div>
          <div class="col-md-4" id="state_select_option">
            <?php $this->load->view('state_div'); ?>
          </div>
        </div>
        <div class="row-fluid">
          <div class="col-md-4">
            <label for="bedrooms">Bedrooms</label>
            <select name="bedrooms" id="bedrooms" class="form-control">
              <option value="1" <?php if($data['bedrooms']==1){echo "selected";} ?> >1</option>
              <option value="2" <?php if($data['bedrooms']==2){echo "selected";} ?>>2</option>
              <option value="3" <?php if($data['bedrooms']==3){echo "selected";} ?>>3</option>
              <option value="4+" <?php if($data['bedrooms']=='4+'){echo "selected";} ?>>4+</option>
            </select>
          </div>
          <div class="col-md-4">
            <label for="bathrooms">Bathrooms</label>
            <select name="bathrooms" class="form-control">
              <option value="1" <?php if($data['bathrooms']==1){echo "selected";} ?>>1</option>
              <option value="2" <?php if($data['bathrooms']==2){echo "selected";} ?>>2</option>
              <option value="3" <?php if($data['bathrooms']==3){echo "selected";} ?>>3</option>
              <option value="4+" <?php if($data['bathrooms']=='4+'){echo "selected";} ?>>4+</option>
            </select>
          </div>
          <div class="col-md-4">
            <label for="carspaces">Car spaces</label>
            <select name="carspaces" class="form-control">
              <option value="1" <?php if($data['carspaces']==1){echo "selected";} ?>>1</option>
              <option value="2" <?php if($data['carspaces']==2){echo "selected";} ?>>2</option>
              <option value="3" <?php if($data['carspaces']==3){echo "selected";} ?>>3</option>
              <option value="4+" <?php if($data['carspaces']=='4+'){echo "selected";} ?>>4+</option>
            </select>
          </div>
        </div>
        <div class="row-fluid text_main_div">
          <div class="col-md-6">
            <label for="purchase_details">Purchase and valuation details</label>
            <textarea rows="4" class="form-control" id="PV_detail" name="purchase_details"><?php echo $data['purchase_details']; ?></textarea>
          </div>
          <!-- will be foreach -->
          <?php 
          if($property_note){
           foreach ($property_note as $note) { ?>
           <div class="col-md-6 notes_add" style="margin-left: 0;position: relative;" id="rep_notes<?php echo $note['property_note_id']; ?>">
            <span>
              <label for="property_notes">Notes about property</label>
              <textarea rows="4" id="NA_property" class="form-control" name="property_notess[]"><?php echo $note['property_note']; ?></textarea>
              <input type="hidden" value="<?php echo $note['property_note_id'] ?>" name="property_note_id[]">
            </span>
            <button type="button" name="btn_delete_note" data-id4="<?php echo $note['property_note_id']; ?>" class="btn btn-xs btn-danger btn_delete_note">x</button>            
            <input type="hidden" name="note_id" value="<?php echo $note['property_note_id']; ?>">
            <span class="cross_2" style="display:none;">x</span>
          </div>
          <!-- will be foreach -->
          <?php } } ?>
          <div class="property_notes"> </div>
          <div id="stop_notes" style="display: none;"></div>
        </div>
        <div class="row-fluid">
          <div class="col-md-12">
            <label for="new-property-note"><a class="new-property-note" id="add_property_note" href="javascript:void(0);">Click to add</a> another note about your property</label>
          </div>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary">Save changes</button>
        <a href="other/delete_property?pid=<?php echo $data['property_id']; ?>" class="btn btn-danger"><i class="glyphicon glyphicon-trash"></i> Delete Property </a>
      </div>
    </form>
  </div>
</div>
</div>
<script src="//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
<script type="text/javascript">
    $(window).on('load',function(){
        $('#property_photos_info').modal('show');
    });
</script>
<script>
  $('#add_property_note').click(function(){         
    $('.property_notes').append('<div class="col-md-6 notes_add" style="margin-left: 0;position: relative;" id="rep_notes"> <span> <label for="property_notes">Notes about property</label> <textarea rows="4" id="NA_property" class="form-control" name="property_notess[]"></textarea><input type="hidden" value="0" name="property_note_id[]"></span> <span id="property_note_hide_span" class="btn btn-xs btn-danger" >X</span></div>'); 

  });       
</script>
<script>
  $(document).on('click', '#property_note_hide_span', function () {
    $(this).parents('#rep_notes').remove();
  }); 
</script>
<script type="text/javascript">
  function getState(val) {
    $.ajax({
      type: "POST",
      url: "other/ajax_get_state",
      data:'country_id='+val,
      success: function(data){
        var res = JSON.parse(data);
        $('#state_select_option').html(res.stateDiv);
      }
    });
  }
</script>
<!-- delete property image --> 
<script type="text/javascript">
  $(document).on('click', '.btn_delete', function(){  
   var id=$(this).data("id3");  
          //alert(id);
          if(confirm("Are you sure you want to delete this?"))  
          {  
            $.ajax({  
             url:"other/delete_property_image",  
             method:"get",  
             data:{id:id},

             dataType:"text",  
             success:function(data){  
                      //alert("abcdef");
                      $("#pImg"+id).fadeOut();
                          // fetch_data();  
                        }  
                      });  
          }  
        }); 
      </script>
      <!-- delete property image end -->
      <!-- delete property note --> 
      <script type="text/javascript">
        $(document).on('click', '.btn_delete_note', function(){  
         var id=$(this).data("id4");  
          //alert(id);
          if(confirm("Are you sure you want to delete this?"))  
          {  
            $.ajax({  
             url:"other/delete_property_note",  
             method:"get",  
             data:{id:id},

             dataType:"text",  
             success:function(data){  
                      //alert("abcdef");
                      $("#rep_notes"+id).fadeOut();
                          // fetch_data();  
                        }  
                      });  
          }  
        }); 
      </script>
      <!-- delete property note end -->
      <script>
        $(document).ready(function(){

          //alert("dfsdfsdfdsf");
          var idd = '<?=$result[0]['country'];?>'
          //alert(idd); 
          $.ajax({
            type: "POST",
            url: "other/ajax_get_state2",
            data:{country_idd: idd},
                //alert(country_id); die;
                success: function(data){
                  var res = JSON.parse(data);
                  $('#state_select_option').html(res.stateDiv);
                      // alert("success");
                    }
                  });

        });
      </script>
      <?php $this->load->view('front/footerlink'); ?>
      <script type="text/javascript">
        Dropzone.autoDiscover = false;
        var foto_upload = new Dropzone(".dropzone", {
          url: "<?php echo base_url('other/property_photo') ?>",
          maxFilesize: 20,
          method: "post",
          acceptedFiles: ".jpg,.jpeg,.png,.gif",
          paramName: "userfile",
          dictInvalidFileType: "This File Type Not Supported",
          addRemoveLinks: true,
          init: function () {
            var count = 0;
            thisDropzone = this;
            this.on("success", function (file, json) {
              var obj = json;
              $('.previews').
              append(
                "<input type='hidden' name='image[]' value='" + obj + "'>\n\
                <input type='hidden' name='width[]' value='" + file.width + "'>\n\
                <input type='hidden' name='height[]' value='" + file.height + "'>"
                );        
            });
          }
        });
      </script>