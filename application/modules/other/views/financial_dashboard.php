<?php $this->load->view('front/headlink'); ?>

<div class="dashbord_poperty">
    <?php $this->load->view('front/top_menu'); ?>
    <div class="container">
        
        <?php $this->load->view('front/head_nav'); ?> 

        <div class="row" id="ss_f_dashboard">  
            <div class="ss_container">
                <h3>Financial Dashboard <small> <?php echo $property[0]['property_address'].", " ?> <?php echo $property[0]['city']; ?></small></h3>
                <div class="row" id="ss_d_top_content">
                        <div class="col-md-4">
                            <h2>Annual Income</h2>
                            <h4 class="">$<span class="ss_price_text"><?=$total_annual_income;?></span></h4>
                          
                            <br><br>
                             
                        </div>
                        <div class="col-md-4">
                            <h2>Current Valuation</h2>
                            <h4 class="" id="current_valuation"></h4>
                            
                        </div>
                        <div class="col-md-4">
                            <h2>Gross Yield</h2>
                            <h4 class="">$<span class="ss_price_text">00</span></h4>
                        </div>
                      </div>


                      <div class="ss_f_dashboard_content">
                              <!-- Nav tabs -->
                              <ul class="nav nav-tabs" role="tablist">
                                <li role="presentation" class="active"  ><a href="#home" aria-controls="home" role="tab" data-toggle="tab">Purchase & Capital Expenditure</a></li>
                                <li role="presentation"><a href="#profile" aria-controls="profile" role="tab" data-toggle="tab">Valuations</a></li>
                                <li role="presentation"><a href="#messages" aria-controls="messages" role="tab" data-toggle="tab">Mortgage details</a></li>
                                <li role="presentation" ><a href="#settings" aria-controls="settings" role="tab" data-toggle="tab">Depreciation details</a></li>
                              </ul>
                            
                              <!-- Tab panes -->
                              <div class="tab-content">
                                <div role="tabpanel" class="tab-pane active" id="home">
                                    <div class="col-md-8 ss_left_bar">
                                    <h3>Registered Owner</h3>
                                    <p>Who is the legal owner of this property - is it you, a company, a trust or other?
                                            The name you enter here will be the name seen on all invoices and tax reports.</p>

                                            <!-- purchase details code start form here -->


                    <?php 
                    $user_fname = $this->session->userdata('user_fname');
                    $user_lname = $this->session->userdata('user_lname');
                    ?>
                  <form class="container form-horizontal"  id="purchaseDetailForm" action="other/add_purchase_detail" method="post" enctype="multipart/form-data">
                    <div class="row">
                        <div class="col-md-12">   
                            <div class="form-group">
                                <br/> 
                                <label for="inputRegisterOwner" class="col-sm-4 control-label">Registered Owner</label>
                                <div class="col-sm-8">
                                    <input type="text" name="register_owner" placeholder="" class="form-control"  value="<?php echo $user_fname; echo " "; echo $user_lname; ?>" id="register_owner">
                                    <input type="hidden" name="property_id" value="<?=$property_id;?>">
                                </div>
                            </div> 
                            <div class="clear"></div> <br> <br>

                            <h5 class="form_heading">Purchase Details</h5>
                            <div class="row">
                            <div class="form-group">
                                <br/> 
                                <label for="inputPurchasePrice" class="col-sm-4 control-label">Purchase Price</label>
                                <div class="col-sm-8">
                                    <input type="number" name="purchase_price" id="purchase_price" placeholder="$" class="form-control"  value="" onkeyup="total(); purchasePrice(); " >
                                   
                                </div>
                            </div>
                            </div>
                            <div class="clear"></div>
                            <div class="row">
                            <div class="form-group">
                                <br/> 
                                <label for="inputValuationDate" class="col-sm-4 control-label">Settlement Date</label>
                                <div class="col-sm-8">
                                    <input type="text" id="datepicker2" name="settlement_date" placeholder=" " class="form-control" required  value="" >
                                     <span class="" id="settlementError" style="color:red"></span>
                                   
                                </div>
                            </div>
                            </div>
                            <div class="clear"></div> <br> <br>

                            <p> Purchase costs include, but not limited to stamp duties, legal fees, building and inspection reports. </p> <br>
                            
                             

                            



                            <div class="form-group">
                                         <div class="row ">

                                               <label for="settlement_date" class=" col-sm-4 control-label">Purchase costs</label>
                                        
                                         <div class="col-sm-8">
                                       <input onclick="option1()" type="radio" name="purchase_cost" id="ss_check1" value="1" checked="" >  don't know the exact costs, let's estimate at 5% of purchase price (<span id="purchase_percen">$0.00</span>).
        </label><br/>
        <input onclick="option2()" type="radio" name="purchase_cost" id="ss_check2" value="2" >  Don't add any purchase costs
        </label><br/>

         <input onclick="option3()" type="radio" id="ss_check3"  name="purchase_cost"  value="3" > I know my purchase details
        </label>
                                                  
                                          </div>
                                        </div>
                                       </div>


                            <div class="form-group">
                                

                                  <div class="clear"></div>
                                  
                            <div class="form-group">
                                <br/> 
                               
                                <div class="row">
                                <label for="inputStamDuty" class="col-sm-4 control-label">Stamp Duty</label>
                                <div class="col-sm-8">
                                    <input type="number" name="stamp_duty" placeholder="$" id="stamp_dutyy"  class="form-control" value="" disabled="disabled" onkeyup="stampDuty(); total();">  
                                     <span class="" id="stampDutyError" style="color:red"></span>
                                </div>
                            </div>



                            
                            <div class="clear"></div>

                             <div class="row">
                            <div class="form-group">
                                <br/> 
                                <label for="inputLegal" class="col-sm-4 control-label">Legals</label>
                                <div class="col-sm-8">
                                    <input type="number" name="legal" placeholder="$" class="form-control" required id="legall" required value="" disabled="disabled" onkeyup="legalFunc(); total();">
                                     <span class="" id="legalError" style="color:red"></span>
                                   
                                </div>
                            </div>
                            </div>
                            <div class="clear"></div>                            


                            <div class="form-group">
                                <br/> 
                                <div class="row">
                                <label for="inputInspection" class="col-sm-4 control-label">Inspection Reports</label>
                                <div class="col-sm-8">
                                    <input type="number" name="inspection_report" placeholder="$" class="form-control" required value="" id="inspection_reportt" disabled="disabled" onkeyup="inspectionReport(); total();">
                                     <span class="" id="inspectionError" style="color:red"></span>
                                   
                                </div>
                                </div>
                            </div>
                            </div>

                            <div class="clear"></div>

                           <div id="ss_checked">
                                                                               
                                         <div class="form-group ss_other_add" id="ss_m" >
                                          <div class="row">
                                                 <label for="legals" class="col-sm-3 control-label">Other</label>
                                            <div class="col-md-4">
                                                  <input id="legals" class="form-control" type="text" name="purchase_other_des[]" required="required" value="" disabled >
                                            </div>
                                            <div class="col-md-4">
                                                  <input id="legals" class="form-control" type="number" name="purchase_other_cost[]" placeholder="$" value="" disabled>
                                            </div>
                                            <div><span class="glyphicon glyphicon-remove ss_remove_ot" aria-hidden="true"  ></span></div>
                                          </div>
                                        </div>
                                        <div class="ss_add_div_con"></div><br>

                                        <div class="row">
                                              <div class="col-md-3">
                                                </div>
                                            <div class="col-md-4">
                                                 
                                            </div>
                                            <div class="col-md-4">
                                             <div id="add_purchasse_cost" class="add_purchasse_cost_r" >
                                             <a href="javascript:void(0)" > <span class="glyphicon glyphicon-plus" aria-hidden="true" ></span> Add another purchase cost</a>
                                         </div>
                                                 
                                            </div>
                                           
                                          </div>                                       

                                     <br/>
                                     <br/>
                                     <br/>
                                    

                                 </div>

                               
                            </div>


                            <div class="clear"></div>



                         <div id="capital_cost_section">
                            <h5 class="form_heading">Any Other Capital Costs?</h5>

                            <div id="capital_cost_div">
                            <div class="form-group">
                                <br/> 
                                <label for="inputOther" class="col-sm-3 control-label">Description:</label>
                            <div class="col-sm-8">
                                    <input type="text" name="capital_expense_description[]" placeholder=" " class="form-control" id="capital_cost_descripiton" value="">
                                </div>                               
                                <input type="hidden" name="capital_cost_id[]" placeholder=" " class="form-control" value="0">
                                 <div class="col-sm-1" style="margin-top: 6px;">
                                    <span id="hide_capital_div" class="btn btn-xs btn-danger"> X </span>
                                </div>
                            </div>

                            <div class="clear"></div>
                              <div class="form-group">
                                <br/> 
                                <label for="inputOther" class="col-sm-3 control-label">Expense Amount:</label>
                                <div class="col-sm-3">
                                    <input type="number" name="capital_expense_amount[]" id="capital_expense_amount" placeholder="$" class="form-control" value="">                                   
                                </div>  

                                <h5 for="inputOther" class="col-sm-1 control-label">Date:</h5>
                                <div class="col-sm-4">
                                    <input type="text" name="capital_expense_date[]" placeholder=" " class="form-control datepicker3" value="">                                   
                                </div>
                            </div>
                            <div class="clear"></div> 

                        </div> 
                           <div class="another_capital_div"></div>

                            <div class="form-group">
                                <br/> 
                                <h5 for="inputOther" class="col-sm-3 control-label"></h5>
                                <div class="col-sm-4"> 
                                </div> 
                                <div class="col-sm-5">
                                   <a href="javascript:void(0);" id="add_another_capital"><h5><span class="glyphicon glyphicon-plus-sign"></span> Add another capital expense </h5> </a>                                
                                </div>
                            </div>

                            <div class="clear"></div> 

                             <div class="form-group">
                                <br/> 
                                <label for="inputOther" class="col-sm-3 control-label">Attachment:</label>
                                

                               
                                <div class="col-sm-9">
                                  <div class="dropzone capitalFile">
                                        <div class="dz-message" >
                                            <h3 style="text-align: center;"> Drop File</h3>
                                        </div>
                                    </div>
                                    <div class="previewsss" id="previews"></div>                            
                                </div>
                            </div>


                         

                        </div>

                        </div>
                       
                        <div class="clear"></div>



                    </div>
                    <div class="clear"></div>

                    <br/><br/>





                    <button type="submit" id="save_purchase_detail"  class="print_ledgers btn btn-primary btn-lg">
                        Save Details
                    </button> 
                    <a href="other/financial_dashboard/<?=$property_id;?>" class="btn btn-light btn-lg mark_paid">
                        Cancel</a>
                    <br/>
                    <br/>

                </form>







                                            <!-- purchase details code end here -->
                                
                                    </div>
                                    <div class="col-md-4 ss_sidebar">

                                            <div class="total_purchase_costs">
                                                    <div class="row-fluid cost_item html_template" style="display: none;">
                                                        <p class="col-md-7 cost_title">
                                                        </p><p class="col-md-5 cost_amount">$0</p>
                                                    </div>
                                                    <h3>Total Purchase and Capital Costs</h3>
                                                    <div id="purchase_price_value">      
                                                    </div>
                                                    <div id="stamp_dutyy_value">      
                                                    </div>
                                                    <div id="legal_value">      
                                                    </div><div id="inspection_report_value">      
                                                    </div>
                                                     
                                                    <!-- <div class="row-fluid cost_item rep_capital_cost" id="repeat_capital_cost" data-expense-id="" style="">
                                                        <p class="col-md-7 cost_title">Other</p>
                                                        <p class="col-md-5 cost_amount" id="renovations_value">$222,222.00</p>
                                                    </div> -->

                                                    <div style="dispaly:none" id="stop_rep_capital_cost"></div>
                                                    
                                                    <div id="other_value_stops" style="display: none"> </div>

                                                    <div id="total_purchase_value">
                                                    </div>
                                                 
                                                </div>
                                        

                                        <div class="clear"></div>
                                        <br/><br/>
                                        <h3>What is Gross Yield?</h3>
                                        <p><strong>The yield on an investment before the deduction of taxes and expenses.</strong></p>
                                        
                                        <p>Gross yield is expressed in percentage terms. It is calucalted as the annual return on an investment prior to taxes and expenses divided by the current price of the investment.</p>


                                    </div>
                                </div>
                                <div role="tabpanel" class="tab-pane" id="profile">
                                        <div class="col-md-8 ss_left_bar">
                                                <h4 class="form_heading">Add a Valuation</h4>
                                                
                                                <p> Please enter your current or historical valuations here, together with dates and any attachments you may have. The most recent valuation will be used to calculate your yield.</p>
                                            
                                <!-- my form work start                 -->

            <form class="container form-horizontal" id="myForm">
                    <div class="row">
                        <div class="col-md-12">   
                            <div class="form-group">
                                <br/> 
                                <label for="inputValuation" class="col-sm-4 control-label">Valuation</label>
                                <div class="col-sm-8">
                                    <input type="number" name="valuation_cost" placeholder="$ 720000.00" class="form-control"  value="" id="valuation_cost" required="required">
                                     
                                     <span class="" id="valuation_costt" style="color:red"></span>
                                    <input type="hidden" name="property_id" value="<?=$property_id;?>">
                                </div>
                            </div> 
                            <div class="clear"></div>
                            <div class="form-group">
                                <br/> 
                                <label for="inputValuationDate" class="col-sm-4 control-label">Valuation Date</label>
                                <div class="col-sm-8">
                                    <input type="text" id="datepicker" name="valuation_date" placeholder=" " class="form-control"  value="" required="required">
                                      <span class="" id="valuation_datee" style="color:red"></span>
                                   
                                </div>

                            </div>
                            <div class="clear"></div>
                            <div class="form-group">
                                <br/> 
                                <label for="inputValuationNote" class="col-sm-4 control-label">Notes<br/> </label>
                                <div class="col-sm-8">
                                    <textarea class="form-control" name="valuation_note" rows="3"></textarea>
                                </div>
                            </div> 

                            <div class="clear"></div>
                            <div class="form-group">
                                <br/> 
                                <label for="inputLeaseName" class="col-sm-4 control-label">Add Attachment<br/> </label>
                                <div class="col-sm-8" id="dropZ">
                                    <div class="dropzone valuationFile" >
                                        <div class="dz-message" >
                                            <h3> Drop File</h3>
                                        </div>
                                    </div>

                                    <div class="previews" id="preview"></div>                                  
                                </div>
                            </div>
                        </div>
                       
                        <div class="clear"></div>

                    </div>
                    <div class="clear"></div>

                    <br/><br/>
					<div class="col-sm-4">
					</div>
					<div class="col-sm-8">
                    <div type="submit" id="save_valuation"  class="print_ledgers btn btn-primary btn-lg">
                        Save Valuation
                    </div> 
                    <a href="other/financial_dashboard/<?=$property_id;?>" class="btn btn-light btn-lg mark_paid">
                        Cancel</a>
					</div>
                    <br/>
                    <br/>

                </form>
                <br>
                  <div class="alert alert-success" id="success_msg" style="text-align: center; display: none;"> Data Inserted Sucessfully </div>
                                <!-- my form work end -->

                                                </div>
                                                <div class="col-md-4 ss_sidebar">
            
                                                    <p><strong> our Doc & fact sheets to enter your property under 'Dashboard' in the menu.</strong></p>
                                                     <p> Please provide the correct documents and notice period to avoid issues with your tenants.</p>
                                                </div> 


                                               
<!-- display code start from here -->
                                                
                                                <div class="col-md-12">
                                                         <!-- Tab panes -->
                                                         <h2>Valuations for this property</h2>
                                                        <div id="view_valuation">
                                                         
                                                          </div>
                          
                                                </div>
                                   
                                </div>
                                <div role="tabpanel" class="tab-pane" id="messages">
                                        <div class="col-md-8 ss_left_bar">
                                                <h3>Your Mortgage Details</h3>
                                                <p>We will be adding more here in the future. For now, feel free to write all of your mortgage details below so that everything is stored in one place. Include institution, rates, amount outstanding, monthly payments, etc.</p>
                                                <br>


                                                
                                                <form class="form-horizontal" id="mortgageForm">
                                                 <span id="mortgageDetail">
                                                 <textarea class="form-control" id="mortgageDetaill" name="mortgage_detail" rows="8"></textarea>
                                                 </span>
                                                 <input type="hidden" name="property_id" id="property_id" value="<?php echo $property_id; ?>">
                                                
                                                <br>
                                                <div class="alert alert-success" id="mortgageMsg" style="text-align: center; display: none;"> Mortgage Detail Updated </div>
                                                <br>
                                                <div id="view_mortgage_file">                    
                                                
                                                </div>

                                                <div class="clear"></div>

                                                
                                                    <br/> 
													<div class="row">
                                                    <h5 for="inputMortgageFile" class="col-sm-12">Add Attachment<br/> </h5>
                                                    <div class="col-sm-12">
                                                        <div class="dropzone mortgageFile">
                                                            <div class="dz-message" >
                                                                <h3> Drop File</h3>
                                                            </div>
                                                        </div>

                                                        <div class="previewss" id="previews"></div>                                  
                                                    </div>
                                                    </div>
                                                
                                                 <div class="clear"></div>
                                                    <br/><br/>                                              
                                                    <div id="save_mortgage"  class="print_ledgers btn btn-primary btn-lg">
                                                        Save Details
                                                    </div> 
                                                    <a href="other/financial_dashboard/<?=$property_id;?>" class="btn btn-light btn-lg mark_paid">
                                                        Cancel</a>
                                                </form>
                                                
                                                </div>
                                                <div class="col-md-4 ss_sidebar">
            
                                                        <h3>What information would you like us to provide you?</h3>
                                                        <p> Please give us your feedback here: </p>
                                                </div>
                                    
                                </div>
                                <div role="tabpanel" class="tab-pane" id="settings">
                                        <div class="col-md-8 ss_left_bar">
                                                <h3>Do you know your Depreciation Schedule?s</h3>
                                                <p>Just like you claim wear and tear on a car purchased for income producing purposes, you can also claim the depreciation of your investment property against your taxable income. What is incredible is that 80% of Australian property investors are not taking advantage of this!</p>


                                                
                                            
  <h3>Have you determined what depreciation is available for this property?</h3>

  <?php if($depreciation_detail){ ?> 

 

<form id="depreciationForm" action="other/add_depreciation_detail" method="post">
      <div class="row">
            <div class="form-group">
            <br/>            
            <div class="col-sm-12">
            <input type="radio" name="depreciation_option" value="1" onclick="depreciation1()" <?php if($depreciation_detail[0]['depreciation_option']==1){echo "checked";} ?> >
                No, I do not know my annual depreciation amount but may be interested to learn more.
            </div>

            <div class="col-sm-12" id="not_know_div" style="display: none;">               
                    <p style="padding: 10px;">At RentingSmart, we speak to Australia's leading Quantity Surveyors. Email <strong>depreciation@rentingsmart.com</strong> and we can recommend a local provider to assist you. Before emailing us, please read the <a href="#" target="_blank">10 tips to understanding property depreciation first</a></p>
            </div>
          


             </div>

             <div class="form-group">
            <br/>            
            <div class="col-sm-12">
            <input type="radio" name="depreciation_option" value="2" onclick="depreciation2()" <?php if($depreciation_detail[0]['depreciation_option']==2){echo "checked";} ?> >
                Yes, I know my annual depreciation amount.
            </div>
            <input type="hidden" name="property_id" value="<?=$property_id;?>">
            </div>

      </div> <br>

      <div class="row">

      <div id="know_div" style="display: none;">
         <div class="form-group">
            <br/> 
            <div class="col-md-1"></div>
            <h5 for="inputYearOfConstruction" class="col-sm-4 control-label">Year of Construction:</h5>
            <div class="col-sm-5">
                <select class="form-control" name="construction_year" >
                   
                    <option value="2012" <?php if($depreciation_detail[0]['construction_year']==2012){echo "selected";} ?>>2012</option>
                    <option value="2013" <?php if($depreciation_detail[0]['construction_year']==2013){echo "selected";} ?>>2013</option>
                    <option value="2014" <?php if($depreciation_detail[0]['construction_year']==2014){echo "selected";} ?>>2014</option>
                    <option value="2015" <?php if($depreciation_detail[0]['construction_year']==2015){echo "selected";} ?>>2015</option>               
                    <option value="2016" <?php if($depreciation_detail[0]['construction_year']==2016){echo "selected";} ?>>2016</option>               
                    <option value="2017" <?php if($depreciation_detail[0]['construction_year']==2017){echo "selected";} ?>>2017</option>               
                              
              </select>
            </div>
        </div>


        <div class="clear"></div> <br>
        <div class="form-group">
            <br/> 
            <div class="col-md-1"></div>
            <h5 for="inputValuationDate" class="col-sm-4 control-label"></h5>
            <div class="col-sm-5">
                <a href="javascript:void(0);" id="add_previous_depreciation_year"><h5><span class="glyphicon glyphicon-plus-sign" ></span> Add previous year </h5> </a>                                
            </div>
        </div>


        <div class="depreciation_div_top"></div>
  <?php if($depreciation_detail_amount){ ?>     
<?php foreach ($depreciation_detail_amount as $depAmount) { ?>
  

        <div class="clear"></div>
         <div class="form-group" id="depreciation_div">
            <br/> 
            <div class="col-md-1"></div>
            <h5 for="inputValuationDate" class="col-sm-4 control-label">Depreciation for <?php echo $depAmount['depreciation_year'] ?></h5>
            <div class="col-sm-5">
                <input type="number" name="depreciation_amount[]" placeholder="$" class="form-control"  value="<?php echo $depAmount['depreciation_amount'] ?>"> 
                <input type="hidden" name="depreciation_year[]" value="<?php echo $depAmount['depreciation_year'] ?>">                              
            </div>
            <div class="col-md-1"> <span id="hide_depreciation_year" class="btn btn-xs"> X </span></div>
        </div>

<?php } } ?>

<?php if(!$depreciation_detail_amount) { ?>

<div class="clear"></div>
         <div class="form-group" id="depreciation_div">
            <br/> 
            <div class="col-md-1"></div>
            <h5 for="inputValuationDate" class="col-sm-4 control-label">Depreciation for 2014</h5>
            <div class="col-sm-5">
                <input type="text" name="depreciation_amount[]" placeholder="$" class="form-control"  value=""> 
                <input type="hidden" name="depreciation_year[]" value="2014">                              
            </div>
            <div class="col-md-1"> <span id="hide_depreciation_year" class="btn btn-xs"> X </span></div>
        </div>

<?php } ?>

        <div class="depreciation_div_bottom"></div>
       
        <div class="clear"></div>

         <div class="form-group">
            <br/> 
            <div class="col-md-1"></div>
            <h5 for="inputValuationDate" class="col-sm-4 control-label"></h5>
            <div class="col-sm-5">
                <a href="javascript:void(0);" id="add_next_depreciation_year" ><h5><span class="glyphicon glyphicon-plus-sign"></span > Add next year </h5> </a>                                
            </div>
        </div>
        <div class="clear"></div><br>

         <div class="form-group">
            <br/> 
            <div class="col-md-1"></div>
            <h5 for="inputTaxRate" class="col-sm-4 control-label">Tax Rate:</h5>
            <div class="col-sm-5">
                <select class="form-control" name="tax_rate" >
                    <option value="19" <?php if($depreciation_detail[0]['tax_rate']==19){echo "selected";} ?> >19%</option>
                    <option value="30" <?php if($depreciation_detail[0]['tax_rate']==30){echo "selected";} ?>>30%</option>               
                    <option value="32.5" <?php if($depreciation_detail[0]['tax_rate']==32.5){echo "selected";} ?>>32.5%</option>               
                    <option value="37" <?php if($depreciation_detail[0]['tax_rate']==37){echo "selected";} ?>>37%</option>               
                    <option value="45" <?php if($depreciation_detail[0]['tax_rate']==45){echo "selected";} ?>>45%</option>               
              </select>
            </div>
        </div>
        <div class="clear"></div> 
        </div>
        <div class="form-group" id="save_button_div" ><br> <br> <br>
            <br/> 
            <div class="col-sm-12">
                        <button type="submit" id="save_depreciation" class="print_ledgers btn btn-primary btn-lg">
                Save Depreciation Schedule
                    </button> 
                    <a href="other/financial_dashboard/<?=$property_id;?>" class="btn btn-light btn-lg mark_paid">
                        Cancel</a>
            </div>
        </div>
        <div class="clear"></div>

      </div>

      </form>











  <?php } ?>














<!-- form if else work midPoint -->













<?php if(!$depreciation_detail){ ?>

    <form id="depreciationForm" action="other/add_depreciation_detail" method="post">
      <div class="row">
            <div class="form-group">
            <br/>            
            <div class="col-sm-12">
            <input type="radio" name="depreciation_option" value="1" onclick="depreciation1()">
                No, I do not know my annual depreciation amount but may be interested to learn more.
            </div>

            <div class="col-sm-12" id="not_know_div" style="display: none;">               
                    <p style="padding: 10px;">At RentingSmart, we speak to Australia's leading Quantity Surveyors. Email <strong>depreciation@rentingsmart.com</strong> and we can recommend a local provider to assist you. Before emailing us, please read the <a href="#" target="_blank">10 tips to understanding property depreciation first</a></p>
            </div>
             </div>

             <div class="form-group">
            <br/>            
            <div class="col-sm-12">
            <input type="radio" name="depreciation_option" value="2" onclick="depreciation2()">
                Yes, I know my annual depreciation amount.
            </div>
            <input type="hidden" name="property_id" value="<?=$property_id;?>">
            </div>

      </div> <br>

      <div class="row">

      <div id="know_div" style="display: none;">
         <div class="form-group">
            <br/> 
            <div class="col-md-1"></div>
            <h5 for="inputYearOfConstruction" class="col-sm-4 control-label">Year of Construction:</h5>
            <div class="col-sm-5">
                <select class="form-control" name="construction_year" >
                    <option value="2010">2010</option>
                    <option value="2011">2011</option>
                    <option value="2012">2012</option>
                    <option value="2013">2013</option>
                    <option value="2014" selected>2014</option>
                    <option value="2015">2015</option>               
                    <option value="2016">2016</option>               
                    <option value="2017">2017</option>               
                    <option value="2018">2018</option>               
                    <option value="2019">2019</option>               
                    <option value="2020">2020</option>               
              </select>
            </div>
        </div>


        <div class="clear"></div> <br>
        <div class="form-group">
            <br/> 
            <div class="col-md-1"></div>
            <h5 for="inputValuationDate" class="col-sm-4 control-label"></h5>
            <div class="col-sm-5">
                <a href="javascript:void(0);" id="add_previous_depreciation_year"><h5><span class="glyphicon glyphicon-plus-sign" ></span> Add previous year </h5> </a>                                
            </div>
        </div>


        <div class="depreciation_div_top"></div>
        <div class="clear"></div>
         <div class="form-group" id="depreciation_div">
            <br/> 
            <div class="col-md-1"></div>
            <h5 for="inputValuationDate" class="col-sm-4 control-label">Depreciation for 2014</h5>
            <div class="col-sm-5">
                <input type="text" name="depreciation_amount[]" placeholder="$" class="form-control"  value=""> 
                <input type="hidden" name="depreciation_year[]" value="2014">                              
            </div>
            <div class="col-md-1"> <span id="hide_depreciation_year" class="btn btn-xs"> X </span></div>
        </div>
        <div class="depreciation_div_bottom"></div>
       
        <div class="clear"></div>

         <div class="form-group">
            <br/> 
            <div class="col-md-1"></div>
            <h5 for="inputValuationDate" class="col-sm-4 control-label"></h5>
            <div class="col-sm-5">
                <a href="javascript:void(0);" id="add_next_depreciation_year" ><h5><span class="glyphicon glyphicon-plus-sign"></span > Add next year </h5> </a>                                
            </div>
        </div>
        <div class="clear"></div><br>

         <div class="form-group">
            <br/> 
            <div class="col-md-1"></div>
            <h5 for="inputTaxRate" class="col-sm-4 control-label">Tax Rate:</h5>
            <div class="col-sm-5">
                <select class="form-control" name="tax_rate" >
                    <option value="19">19%</option>
                    <option value="30" selected>30%</option>               
                    <option value="32.5">32.5%</option>               
                    <option value="32.5">37%</option>               
                    <option value="45">45%</option>               
              </select>
            </div>
        </div>
        <div class="clear"></div> 
        </div>
        <div class="form-group" id="save_button_div" style="display: none;" ><br> <br> <br>
            <br/> 
            <div class="col-sm-12">
                        <button type="submit" id="save_depreciation" class="print_ledgers btn btn-primary btn-lg">
                Save Depreciation Schedule
                    </button> 
                    <a href="other/financial_dashboard/<?=$property_id;?>" class="btn btn-light btn-lg mark_paid">
                        Cancel</a>
            </div>
        </div>
        <div class="clear"></div>

      </div>

      </form>
<?php } ?>





                                                </div>
                                                <div class="col-md-4 ss_sidebar">
            
                                                      <h3>What is a Tax Depreciation Schedule?</h3>  
                                                       <p><strong> Tax Depreciation Schedule is a fancy name for a document that tells your accountant how much depreciation to clain on your property.</strong></p> 
                                                        
                                                       <p> Read here: 10 tips to understanding property depreciation</p>
                                                </div>
                                    
                                </div>
                              </div>
                            
                            </div> 
                            <hr>
                            

                            
 </div>
        </div>  


<script type="text/javascript">
 
 $( document ).ready(function() {

 $( document ).delegate( "#ss_check1:radio", 'click',function(){
 
    $("#ss_checked :input").prop('disabled', true);
 
     $("#add_purchasse_cost").removeClass('add_purchasse_cost_r');
        
}); 
  $( document ).delegate( "#ss_check2:radio", 'click',function(){

    $("#ss_checked :input").prop('disabled', true);
    $("#add_purchasse_cost").removeClass('add_purchasse_cost_r');
}); 
 $( document ).delegate( "#ss_check3:radio", 'click',function(){

    $("#ss_checked :input").prop('disabled', false);
     $("#add_purchasse_cost, this").addClass('add_purchasse_cost_r');
 
}); 
      $i =0;
    
    $( document ).delegate( ".ss_remove_ot", 'click',function(){
   var abc=$(this).parents('.ss_other_add').remove().attr('id') ;
   
});
    $( document ).delegate( ".ss_remove_ot1", 'click',function(){
   var abc=$(this).parents('.ss_capital_cost_repeat').remove().attr('id') ;
   
});
 
     $(".add_purchasse_cost_r").click(function () {
     if($(this).hasClass('add_purchasse_cost_r')){
           var incre=+$i++;
        $(".ss_add_div_con").append(' <div class="form-group ss_other_add" id="ss_m'+incre+'"> <div class="row"><div class="col-md-3"> <h5 for="legals" class="control-label">Other</h5></div><div class="col-md-4"> <input id="legals" class="form-control" type="text" name="purchase_other_des[]" value=""> </div><div class="col-md-4"> <input id="legals" class="form-control" type="number" name="purchase_other_cost[]" placeholder="$" value="" ></div><div><span class="glyphicon glyphicon-remove ss_remove_ot" aria-hidden="true"></span></div> </div></div>'); 
     }
      });
     
       $("#add_purchasse_cost").removeClass('add_purchasse_cost_r');
     });
</script>
    
       
 <?php $this->load->view('front/footerlink'); ?> 

  
<?php require('financial_dashboard_script.php'); ?>
<?php require('purchase_capital_script.php'); ?>
<?php require('depreciation_detail_script.php'); ?>
