<?php $this->load->view('front/headlink'); ?>

<div class="dashbord_poperty">
    <?php $this->load->view('front/top_menu'); ?>
    <div class="container">
        
        <?php $this->load->view('front/head_nav'); ?> 

        <div class="row">  
            <div class="ss_container">
                <h2>Other Money In <br/><small>for the property at california, New work</small> <a data-toggle="modal" data-target="#moneyincreateinvoice" class="pull-right btn btn-primary"  ><span class="glyphicon glyphicon-plus" aria-hidden="true"></span> Money Received or Owed</a></h2>
                <div class="ss_bound_content">
                        <br/><br/><br/>
                          <!-- Nav tabs -->
                         <div class="ss_expensesinvoices">
                           <div class="ss_expensesinvoices_top">
                             <div class="col-md-2"><h2>Period</h2></div>
                             <div class="col-md-4">
                                <div id="fromDate2" class="input-group date" data-date-format="mm-dd-yyyy">
                                    <input class="form-control" type="text" readonly />
                                    <span class="input-group-addon"><i class="glyphicon glyphicon-calendar"></i></span>
                                </div>
                             </div>
                             <div class="col-md-4">
                                <div id="fromDate3" class="input-group date" data-date-format="mm-dd-yyyy">
                                    <input class="form-control" type="text" readonly />
                                    <span class="input-group-addon"><i class="glyphicon glyphicon-calendar"></i></span>
                                </div>
                             </div>
                             <div class="col-md-2"> </div>
                             </div>
                           </div>
                         </div>
                         
                         <div class="ss_expensesinvoices_content">
                           <h3>Money in - Fully received</h3>
                           <div class="expense_title_bar">
                              <div class="row-fluid top-table-name">
                                  <div class="col-md-2">
                                      <p>Received Date</p>
                                  </div>
                                  <div class="col-md-3">
                                      <p>Who owned you?</p>
                                  </div>
                                  <div class="col-md-2">
                                      <p>Amount Paid</p>
                                  </div>
                                
                                  <div class="col-md-2">
                                      <p>Pay Method</p>
                                  </div>
                                  <div class="col-md-3">
                                      <p>   Owning for?</p>
                                  </div>
                              </div>
                          </div>
                          <div class="ss_expensesinvoices_c_view" >
                              <div class="row-fluid row-inner ss_expanse_top_h">
                                  <div class="col-md-2">
               <h4 class="table-name">Due Date</h4>
                                      <p class="unpaid_expense_date">Feb 01, 2018</p>
                                  </div>
                                  <div class="col-md-3">
               <h4 class="table-name">Paying Who?</h4>
                                      <p class="unpaid_paying_who">uqqqqqqq</p>
                                  </div>
                                  <div class="col-md-2">
               <h4 class="table-name">Amount Due</h4>
                                      <p class="unpaid_amountdue expense_amountdue">$123.00</p>
                                  </div>
                                  
                                  
                                  <div class="col-md-2">
               <h4 class="table-name">Tax Code</h4>
                                      <p class="unpaid_taxcode">Cash</p>
                                  </div>
                                  <div class="col-md-2">
                                      <p> 1234</p>
                                  </div>
                              </div>

                              <div class="row ss_wxpance_ifo" style="display:none;">
                                  <ul class="payment-info">
                                   
                                                            <div class="row-fluid">
                                                                <div class="col-md-8">
                                                                    <div class="row-fluid">
                                                                        <div class="col-md-3">
                                                                            <p class="bottom-text">Payments:</p>
                                                                        </div>
                                                                        <div class="col-md-9">
                                                                            <p class="paid_payments"> <span class="paid_payment">$222,222.00 on Feb 28, 2018 paid via 1</span>  </p>
                                                                            
                                                                        </div>
                                                                    </div>
                                                                    <div class="row-fluid">
                                                                        <div class="col-md-3">
                                                                            <p class="bottom-text">Attachments:</p>
                                                                        </div>
                                                                        <div class="col-md-9">
                                                                            <p class="attachments_links">
                                                                                <a class="paid_attachments" target="_blank" href="img/rent.png">
                                                                                    <span class="paid_attachment">rent.png, </span>
                                                                                     
                                                                                </a><a class="paid_attachments" target="_blank" href="img/loginbg.jpg">
                                                                                    <span class="paid_attachment">loginbg.jpg</span>
                                                                                     
                                                                                </a>
                                                                                <a class="alast_flag" id="last_flag10" href="#" style="display:none;">I am not visible</a>
                                                                                
                                                                            </p>
                                                                        </div>
                                                                    </div>
                                                                    <div class="row-fluid">
                                                                        <div class="col-md-3">
                                                                            <p class="bottom-text">Comments:</p>
                                                                        </div>
                                                                        <div class="col-md-9">
                                                                            <p class="paid_comments">ddws  www</p>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="col-md-1"></div>
                                                                <div class="col-md-3">
                                           <div class="bottom-link">
                                                                    <p><a href="#_" id="paid_expense_edit" class="paid_expense_edit">Edit 'Money in'</a></p>                                                                   
                                                                        <p> <a href="javascript:void(0);" class="add_attachment">Add attachment</a>  </p>                                                                                                                                   
                                                                        <p> <a class="delete_expense" href="#_">Delete this Money in</a> </p>
                                                                        <p> <a  class="delete_expense" href="#_">print invoice</a> </p>
                                                                        <p> <a class="delete_expense" href="#_">send invoice</a> </p>
                                                                        
                                                                        
                                                                        
                                                                        
                                                                        
                                                                        
                                                                        
                                                                    
                                            </div>
                                                                </div>
                                                            </div>
                                                        </ul>
                                
                              </div>
                          </div>

                          <div id="paid_expense_editable" style="display: none;">
                              <form action="" method="POST" name="">
                                           
                                  <div class="row-fluid">
                                      <div class="col-md-2">
                                          <p><input class="form-control" type="text" value="Feb 02, 2018" id="dp1518596682052"></p>
                                      </div>
                                      <div class="col-md-3">
                                          <p><input class="form-control" id="" type="text" value="aminaaa"></p>
                                      </div>
                                      <div class="col-md-2">
                                          <p class="paid_editable_amountpaid">222,222.00</p>
                                      </div>
                                      <div class="col-md-2">
                                          <p class="paid_editable_paymentmethod">1</p>
                                      </div>
                                      <div class="col-md-3">
                                          <p>
                                              <select class="form-control paid-other"  id="paid_editable_taxcode">
                                    
                                              <optgroup label="General Expenses">
                                      
                                      
                                                  <option value="1">Accounting/bookkeeping fees</option><option value="2">Bank fees</option><option value="3">Council rates</option><option value="4">GST</option><option value="5" selected="selected">Insurance</option><option value="6">Land tax</option><option value="7">Legal fees</option><option value="8">Loan interest repayments</option><option value="9">Management &amp; admin fees</option><option value="10">Other</option><option value="11">Property advertising</option><option value="12">Property cleaning</option><option value="13">Property repair and maintenance</option><option value="14">Real estate agent fees</option><option value="15">Stationary and postage</option><option value="16">Strata and body corporate</option><option value="17">Utilities (electricity, gas, water)</option></optgroup><optgroup label="Capital Expenses">
                                      
                                      
                                                  <option value="18">Building inspection</option><option value="19">Legal costs</option><option value="20">Stamp duty</option><option value="21">Pest control</option><option value="22">Other purchase costs</option></optgroup><optgroup label="Property Sale Expenses">
                                      
                                      
                                                  <option value="23">Legal costs</option><option value="24">Real estate commission</option><option value="25">Other sales expenses</option></optgroup></select>
                                          </p>
                                <div class="col-md-3 " id="paid_other_expense_div">
                                  <input  id="paid_other_expense" type="text" placeholder="Received by" value="" style="display:none;">
                                </div>
                                      </div>
                                      
                                  </div>
                                  <ul>
                                      <div class="row-fluid">
                                          <div class="col-md-8">
                                              <div class="row-fluid">
                                                  <div class="col-md-3">
                                                      <p class="bottom-text">Payments:</p>
                                                  </div>
                                                  <div class="col-md-9">
                                                      <p class="paid_editable_payments">
                                                          <span class="paid_editable_payment">$222,222.00 on Feb 28, 2018 paid via 1</span>
                                                          <a class="delete_payment" href="#">
                                                              <img src="img/expense_delete.png">
                                                          </a>
                                                                </p>
                                                      <p id="last_flag17" style="display:none;"> </p>
                                                  </div>
                                              </div>
                                              <div class="row-fluid">
                                                  <div class="col-md-3">
                                                      <p class="bottom-text">Attachments:</p>
                                                  </div>
                                                  <div class="col-md-9">
                                                      <p class="attachments_links">
                                                          </p><div class="paid_editable_attachments list-item">
                                                              <a href="img/rent.png">
                                                                  <span class="paid_editable_attachment">rent.png, </span>
                                                              </a>
                                                              <a class="delete_attachment" href="#">
                                                                  <img src="img/expense_delete.png">
                                                              </a>
                                                                  </div><div class="paid_editable_attachments list-item">
                                                              <a href="img/loginbg.jpg">
                                                                  <span class="paid_editable_attachment">loginbg.jpg</span>
                                                              </a>
                                                              <a class="delete_attachment" href="#">
                                                                  <img src="img/expense_delete.png">
                                                              </a>
                                                                     </div>
                                                          <div id="last_flag18" style="display:none;"> </div>
                                                          
                                                      <p></p>
                                                  </div>
                                              </div>
                                              <div class="row-fluid">
                                                  <div class="col-md-3">
                                                      <p class="bottom-text">Comments:</p>
                                                  </div>
                                                  <div class="col-md-9">
                                                      <p>
                                                          <textarea placeholder="" name="paid_editable_comments" class="form-control" rows="4">ddws  www</textarea>
                                                      </p>
                                                  </div>
                                              </div>
                                          </div>
                                          <div class="span1"></div>
                                          <div class="span3 text-right">
                                              <a href="#_" class="btn btn-primary" id="paid_expense_edit_save">Save changes</a>
                                              <p><a href="#_" class="btn btn-light" id="paid_expense_edit_cancel">Cancel changes</a></p>
                                          </div>
                                      </div>
                                  </ul>
                              </form>
                          </div>
                         </div>
                        <a class="btn btn-light pull-right" href="Dashboard/<?=$property_id?>">Back To Dashboard</a>

            </div>
            </div>
        </div>    
        </div>
        </div>	
    
       
    
    </div>
 
 


    <div class="modal fade" id="moneyincreateinvoice" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
          <div class="modal-content">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
              <h4 class="modal-title" id="myModalLabel">Money In (+ Create Invoices)</h4>
            </div>
            <div class="modal-body">
            
                      
                      
                            <div class="stepwizard">
                   
                      <div class="rate-updates">
                          
                          <div class="tab-content margintop0" style="border:none !important;">
                                  
                                  <div class="" >
                                      <div class="step001"> 
                                        <form>
                                            <div class="form-group">
                                              <div class="col-md-5"><label >Which tenant owes (or has paid) you?*<br/>
                                                  </label></div>
                                              <div class="col-md-7">
                                                    <select  class="form-control">
                                                        <option value="0">Select a tenant</option><option value="4959">shakil ahamel</option><option value="4960">ami aha</option><option value="other">Other...</option></select>
                                              </div>    
                                            </div>  

                                            <div class="form-group">
                                                <div class="col-md-5"><label >Total amount owed (or paid)?*</label></div>
                                                <div class="col-md-7">
                                                    <input type="number" class="form-control"  placeholder="$0.00">
                                                </div>    
                                            </div>
                                            <div class="form-group">
                                                <div class="col-md-5"><label >This bill has been *</label></div>
                                                <div class="col-md-7">
                                                    <label class="radio-inline">
                                                        <input type="radio" name="inlineRadioOptions" id="inlineRadio1" value="option1">  Received in full
                                                      </label>
                                                      <label class="radio-inline">
                                                        <input type="radio" name="inlineRadioOptions" id="inlineRadio2" value="option2"> Part received
                                                      </label>
                                                      <label class="radio-inline">
                                                        <input type="radio" name="inlineRadioOptions" id="inlineRadio3" value="option3"> Still owing
                                                      </label>
                                                </div>    
                                            </div> 
   
                                            <div class="form-group">
                                                <div class="col-md-5"><label >Date received and payment method?* </label></div>
                                                <div class="col-md-7">
                                                    <div class="row">
       
                                                    <div class="col-md-6"><input type="text" id="fromDate1" class="hasDatepicker form-control" placeholder="Nov 09, 2013"></div>
                                                    <div class="col-md-6"><input type="text"  class="col-md-4  form-control" placeholder="e.g. Cash"></div>
                                                  </div>                   
                                                </div>    
                                            </div>
                                            <div class="form-group">
                                                    <div class="col-md-5"><label >Add notes (incl. payment details)<br/>
                                                            
                                                           <small> Your tenants will see those notes if
                                                            you create and send an invoice/receipt</small></label></div>
                                                    <div class="col-md-7">
                                                        <textarea  class="form-control"></textarea>
                                                    </div>    
                                                </div>
                                                <div class="form-group">
                                                        <div class="col-md-5"><label >Upload supporting docs <small> (These will be shared with your
                                                              tenants if you send an invoice/receipt)</small>  </label></div>
                                                        <div class="col-md-7">
                                                            <div class="form-group inputDnD">
                                                                <input type="file" class="form-control-file text-primary font-weight-bold" id="inputFile" accept="image/*" onchange="readUrl(this)" data-title="Drag and drop a file">
                                                             </div>
                                                            
                                                        </div>    
                                                    </div>    
                                                    <div class="clear"></div>

                                        </form>
                                      </div>
                                  
                                    </div>
                                  
                                   
                                   
                          </div>
                      </div>
                      
                  </div>
                      
               
            </div>
           
          </div>
        </div>
      </div>
      <div id="taxreport"  class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
          <div class="modal-dialog" role="document">
              <div class="modal-content">
                <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                  <h4 class="modal-title" id="myModalLabel">Let's create a Tax Report</h4>
                </div>
                <div class="modal-body">
                  <p>P.S Have you entered all your income + expenses ?</p>
                  <ul>
                    <li>interest payments</li>
                        
                    <li> rental income</li>
                        
                     <li>advertising expenses  </li>
                  </ul> 
                </div>
                <div class="modal-footer">
                  <button type="button" class="btn btn-light" data-dismiss="modal">Close</button>
                  <a href="yourcashflowreports.html" type="button" class="btn btn-primary">Yes? ... Let's Generate Your Report</a>
                </div>
              </div>
            </div>
      </div>
	  
  <?php $this->load->view('front/footerlink'); ?> 
  
    <script>
      $(".ss_expanse_top_h").click(function(){
    $(".ss_wxpance_ifo").toggle("slow");
});
$("#paid_expense_edit").click(function(){
    $(".ss_expanse_top_h, .ss_wxpance_ifo ").hide(1000);
    $("#paid_expense_editable ").show(1000);
    
});

$("#paid_expense_edit_save, #paid_expense_edit_cancel").click(function(){
    $(".ss_expanse_top_h, .ss_wxpance_ifo ").show(1000);
    $("#paid_expense_editable ").hide(1000);
});
$(".delete_expense").click(function(){
  $(".ss_expensesinvoices_c_view, #paid_expense_editable").remove();
});
function stepnext(n){

    if(n != 0){
		//$(".stepwizard-row a").switchClass('btn-primary','btn-default');
        $(".stepwizard-row a").removeClass('btn-primary');
        $(".stepwizard-row a").addClass('btn-default');
		$('.stepwizard a[href="#step-'+n+'"]').tab('show');
		//$('.stepwizard-row a[href="#step-'+n+'"]').switchClass('btn-default','btn-primary');
        $('.stepwizard-row a[href="#step-'+n+'"]').removeClass('btn-default');
        $('.stepwizard-row a[href="#step-'+n+'"]').addClass('btn-primary');
    }
}
stepnext(1);

      
    </script>

