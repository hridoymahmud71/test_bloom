
<div class="form-group">
	<label for="inputState">State <span>*</span></label>
	<select class='form-control' name='state' id='inputState' required>
		
		<option value="">Select State</option>
		<?php foreach($get_state as $state_row){?>
		<option value="<?=$state_row['state_id'];?>" ><?=$state_row['state_name'];?></option>
		<?php }?>
	</select>
	<strong class="text-danger" id="err_state_msg"></strong>
</div>

