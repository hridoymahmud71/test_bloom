<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Other extends MX_Controller
{

    //public $counter=0;
    function __construct()
    {
        parent::__construct();
        $this->load->model('other_model');
        $this->load->model('utility/utility_model');
        //$this->load->model('home/home_model');
        // $this->load->helper('inflector');
        // $this->load->library('encrypt');

        $this->utility_model->check_auth();

    }

    public function index()
    {
        $this->load->view('money_in');
    }

    public function money_in($property_id)
    {
        $data['property_id'] = $property_id;
        $this->load->view('money_in');
    }

    public function financial_dashboard($property_id)
    {
        $data['property_id'] = $property_id;
        $data['lease_detail'] = $this->other_model->select_with_where('*', 'property_id=' . $property_id . ' AND lease_current_status=1', 'lease');
        $data['all_payment_infos'] = $this->other_model->select_condition_order('lease_payment_scedule', 'lease_id=' . $data['lease_detail'][0]['lease_id'] . ' AND property_id=' . $property_id . '', 'lease_payment_scedule.payment_schedule_id', 'ASC');

        $total_annual_income = 0;
        foreach ($data['all_payment_infos'] as $key => $row) {
            $total_annual_income = $total_annual_income + $row['payment_due_amount'];
        }
        $data['total_annual_income'] = $total_annual_income;
        $data['property'] = $this->other_model->select_with_where('*', "property_id=$property_id", 'property');

        $data['purchase_detail'] = $this->other_model->select_with_where('*', "property_id=$property_id", 'purchase_detail');
        $data['purchase_other_cost'] = $this->other_model->select_with_where('*', "property_id=$property_id", 'purchase_cost_other');
        $data['capital_cost'] = $this->other_model->select_with_where('*', "property_id=$property_id", 'capital_cost');
        $data['capital_cost_file'] = $this->other_model->select_with_where('*', "property_id=$property_id", 'capital_cost_file');

        $data['depreciation_detail'] = $this->other_model->select_with_where('*', "property_id=$property_id", 'depreciation_detail');
        $data['depreciation_detail_amount'] = $this->other_model->select_with_where('*', "property_id=$property_id", 'depreciation_detail_amount');

// print_r($data['depreciation_detail']); die;
        if ($data['purchase_detail']) {
            // echo "adsfawefgaw";
            // die();
            $this->load->view('financial_dashboard2', $data);
        } else {
            // die();
            $this->load->view('financial_dashboard', $data);
        }
    }


    public function view_valuation()
    {
        $property_id = $this->input->post('property_id');
        $data = $this->other_model->get_valuation($property_id);
        $data2 = $this->other_model->select_with_where('*', "property_id=$property_id", 'valuation_file');
        $data3 = $this->other_model->valuation_value($property_id);
        $resArray = [$data, $data2, $data3];
        echo json_encode($resArray);
    }


    public function view_mortgage_file()
    {
        $property_id = $this->input->post('property_id');
        //$data = $this->other_model->get_valuation($property_id);

        $data = $this->other_model->select_with_where('*', "property_id=$property_id", 'mortgage_detail_file');
        $data2 = $this->other_model->select_with_where('*', "property_id=$property_id", 'mortgage_detail');
        $resArray = [$data, $data2];
        echo json_encode($resArray);
    }


    public function mortgage_detail()
    {
        //return $a = $property_id;
        $data['property_id'] = $this->input->post('property_id');
        $property_id = $data['property_id'];
        // echo $data2['mortgage_detail_file'] = $this->other_model->select_with_where('*', "property_id=$property_id", 'mortgage_detail_file');
        echo $data2 = $this->other_model->view_mortgage_detail($property_id);
    }


    public function add_valuation()
    {
        $property_id = $this->input->post('property_id');
        $data['property_id'] = $this->input->post('property_id');
        $data['valuation_cost'] = $this->input->post('valuation_cost');
        $data['valuation_date'] = $this->input->post('valuation_date');
        $data['valuation_note'] = $this->input->post('valuation_note');


        $this->form_validation->set_rules('valuation_cost', 'Valuation Cost', 'required');
        $this->form_validation->set_rules('valuation_date', 'Valuation Date', 'required');

        if ($this->form_validation->run() == FALSE) {
            echo "FALSE";
        } else {
            $valuation_id = $this->other_model->insertId('valuation', $data);
            $image = $this->input->post('image');

            if ($image) {
                foreach ($image as $key => $value) {
                    $image_data['property_id'] = $property_id;
                    $image_data['valuation_id'] = $valuation_id;
                    $image_data['valuation_file'] = $value;
                    $result_set = $this->other_model->insert('valuation_file', $image_data);
                }
            }
        }
    }

    public function valuation_file()
    {
        $files = $_FILES;
        if (isset($_FILES['userfile']) && !empty($_FILES['userfile']['name'])) {
            $config['remove_spaces'] = TRUE;
            $config['encrypt_name'] = TRUE;
            $config['upload_path'] = 'uploads/valuation_file';
            $config['allowed_types'] = 'jpg|png|gif|jpeg|xls|doc|docx|pdf|csv|avi|mp4|flv|mpg';
            $config['max_size'] = '0';
            $this->load->library('upload', $config);
            $this->upload->initialize($config);
            if (!$this->upload->do_upload('userfile')) {
                $status = 'error';
                $msg = $this->upload->display_errors('', '');
            } else {
                $fileinfo = $this->upload->data();
                echo $fileinfo['file_name'];
            }
        }
    }


    public function delete_valuation()
    {
        $id = $this->input->get('id');
        $result = $this->other_model->delete_valuation($id);
        $result2 = $this->other_model->delete_valuation_file($id);
    }

    public function delete_mortgage_file()
    {
        $id = $this->input->get('id');
        $result = $this->other_model->delete_mortgage_file($id);

    }

    public function add_mortgage_detail()
    {

        $property_id = $this->input->post('property_id');
        $data['property_id'] = $this->input->post('property_id');
        $data['mortgage_detail'] = $this->input->post('mortgage_detail');
        $checkMortgage = $this->other_model->select_with_where('*', "property_id=$property_id", 'mortgage_detail');
        $mortgage_idd = $checkMortgage[0]['mortgage_detail_id'];
        // echo json_encode($checkMortgage);
        if ($checkMortgage) {
            $image = $this->input->post('image2');
            if ($image) {
                foreach ($image as $key => $value) {
                    $image_data['property_id'] = $property_id;
                    $image_data['mortgage_detail_id'] = $mortgage_idd;
                    $image_data['mortgage_file'] = $value;
                    $result_set = $this->other_model->insert('mortgage_detail_file', $image_data);
                }
            }
            $result_set = $this->other_model->update_mortgage($data);
        } else {
            $mortgage_id = $this->other_model->insertId('mortgage_detail', $data);
            $image = $this->input->post('image2');
            if ($image) {
                foreach ($image as $key => $value) {
                    $image_data['property_id'] = $property_id;
                    $image_data['mortgage_detail_id'] = $mortgage_id;
                    $image_data['mortgage_file'] = $value;
                    $result_set = $this->other_model->insert('mortgage_detail_file', $image_data);
                }
            }
        }
    }


    public function add_purchase_detail()
    {


        $property_id = $this->input->post('property_id');
        $data2['purchase_other_des'] = $this->input->post('purchase_other_des');
        $data2['purchase_other_cost'] = $this->input->post('purchase_other_cost');
        $data2['purchase_cost_other_id'] = $this->input->post('purchase_cost_other_id');

        $countOtherDes = count($data2['purchase_other_des']);
        $result = $this->other_model->delete_other_purchase_cost($property_id);

        if ($countOtherDes > 0) {
            //$data3['property_id'] = $property_id;
            for ($j = 0; $j < $countOtherDes; $j++) {
                $data3['property_id'] = $property_id;
                $data3['purchase_description'] = $data2['purchase_other_des'][$j];
                $data3['purchase_cost'] = $data2['purchase_other_cost'][$j];
                $data3['purchase_cost_other_id'] = $data2['purchase_cost_other_id'][$j];

                //echo $data3['purchase_cost_other_id'];
                $data4['purchase_description'] = $data2['purchase_other_des'][$j];
                $data4['purchase_cost'] = $data2['purchase_other_cost'][$j];
                $data5['purchase_cost_other_id'] = $data2['purchase_cost_other_id'][$j];

                if (!empty($data3['purchase_description']) && !empty($data3['purchase_cost'])) {
                    $result_set = $this->other_model->insert('purchase_cost_other', $data3);
                }
                // $result_set = $this->other_model->purhcase_cost_other_update($data4, $data5);

            }
        }


        $data9['capital_expense_description'] = $this->input->post('capital_expense_description');
        $data9['capital_expense_amount'] = $this->input->post('capital_expense_amount');
        $data9['capital_expense_date'] = $this->input->post('capital_expense_date');

        $countCapital = count($data9['capital_expense_description']);
        $result2 = $this->other_model->delete_capital_cost($property_id);

        if ($countCapital > 0) {
            for ($k = 0; $k < $countCapital; $k++) {

                $data8['property_id'] = $property_id;
                $data8['capital_expense_description'] = $data9['capital_expense_description'][$k];
                $data8['capital_expense_amount'] = $data9['capital_expense_amount'][$k];
                $data8['capital_expense_date'] = $data9['capital_expense_date'][$k];

                if (!empty($data8['capital_expense_description']) && $data8['capital_expense_amount'] && $data8['capital_expense_date']) {
                    $result_set = $this->other_model->insert('capital_cost', $data8);
                }

            }
        }

        $image = $this->input->post('image9');
        $result3 = $this->other_model->delete_capital_file($property_id);
        if ($image) {
            foreach ($image as $key => $value) {
                $image_data['property_id'] = $property_id;
                $image_data['capital_cost_file'] = $value;
                $result_set = $this->other_model->insert('capital_cost_file', $image_data);
            }
        }

        $property_id = $this->input->post('property_id');
        $data['property_id'] = $this->input->post('property_id');
        $data['register_owner'] = $this->input->post('register_owner');
        $data['purchase_price'] = $this->input->post('purchase_price');

        $data['settlement_date'] = $this->input->post('settlement_date');
        $data['purchase_cost'] = $this->input->post('purchase_cost');

        if ($data['purchase_cost'] == 1) {
            $result = $this->other_model->delete_other_purchase_cost($property_id);
            $data['purchase_percen'] = $data['purchase_price'] * 0.05;
            $data['stamp_duty'] = "";
            $data['legal'] = "";
            $data['inspection_report'] = "";
        }
        if ($data['purchase_cost'] == 2) {
            $result = $this->other_model->delete_other_purchase_cost($property_id);
            $data['purchase_percen'] = "";
            $data['stamp_duty'] = "";
            $data['legal'] = "";
            $data['inspection_report'] = "";
        }
        if ($data['purchase_cost'] == 3) {
            $data['purchase_percen'] = "";
            $data['stamp_duty'] = $this->input->post('stamp_duty');
            $data['legal'] = $this->input->post('legal');
            $data['inspection_report'] = $this->input->post('inspection_report');
        }

        $checkPurchaseDetail = $this->other_model->select_with_where('*', "property_id=$property_id", 'purchase_detail');
        $purchase_detail_id = $checkPurchaseDetail[0]['purchase_detail_id'];

        if ($checkPurchaseDetail) {
            $result_set = $this->other_model->update_purchase_detail($data);
            //redirect('other/financial_dashboard');
            redirect('other/financial_dashboard/' . $property_id);
        } else {
            $result_set = $this->other_model->insert('purchase_detail', $data);
            redirect('other/financial_dashboard/' . $property_id);
        }
    }


    public function capital_file()
    {
        $files = $_FILES;

        if (isset($_FILES['userfile9']) && !empty($_FILES['userfile9']['name'])) {
            $config['remove_spaces'] = TRUE;
            $config['encrypt_name'] = TRUE;
            $config['upload_path'] = 'uploads/capital_file';
            $config['allowed_types'] = 'jpg|png|gif|jpeg|xls|doc|docx|pdf|csv|avi|mp4|flv|mpg';
            $config['max_size'] = '0';
            $this->load->library('upload', $config);
            $this->upload->initialize($config);
            if (!$this->upload->do_upload('userfile9')) {
                $status = 'error';
                $msg = $this->upload->display_errors('', '');
            } else {
                $fileinfo = $this->upload->data();
                echo $fileinfo['file_name'];
            }
        }
    }


    public function mortgage_detail_file()
    {
        $files = $_FILES;
        if (isset($_FILES['userfile2']) && !empty($_FILES['userfile2']['name'])) {
            $config['remove_spaces'] = TRUE;
            $config['encrypt_name'] = TRUE;
            $config['upload_path'] = 'uploads/mortgage_file';
            $config['allowed_types'] = 'jpg|png|gif|jpeg|xls|doc|docx|pdf|csv|avi|mp4|flv|mpg';
            $config['max_size'] = '0';
            $this->load->library('upload', $config);
            $this->upload->initialize($config);
            if (!$this->upload->do_upload('userfile2')) {
                $status = 'error';
                $msg = $this->upload->display_errors('', '');
            } else {
                $fileinfo = $this->upload->data();
                echo $fileinfo['file_name'];
            }
        }
    }


    public function property_photos_info($property_id)
    {
        $property_idd = $property_id;
        $user_fname = $this->session->userdata('user_fname');
        $user_id = $this->session->userdata('user_id');
        $data['property_id'] = $property_id;

        $data['result'] = $this->other_model->select_with_where('*', "property_id=" . $property_idd . "", 'property');
        $data['result2'] = $this->other_model->select_with_where('*', "property_id=" . $property_idd . "", 'property_photos');
        $data['property_note'] = $this->other_model->select_with_where('*', "property_id=" . $property_idd . "", 'property_notes');
        $data['all_country'] = $this->other_model->select_all_acending('country', 'country_name');
        $this->load->view('property_photos_info', $data);

    }

    public function ajax_get_state()
    {
        $country_id = $this->input->post('country_id');
        $data['get_state'] = $this->other_model->select_with_where('*', 'country_id=' . $country_id, 'state');
        $state_data_json = array();
        $state_data_json['stateDiv'] = $this->load->view('state_div', $data, TRUE);
        echo json_encode($state_data_json);
    }

    public function ajax_get_state2()
    {
        $country_id = $this->input->post('country_idd');
        $data['get_state'] = $this->other_model->select_with_where('*', 'country_id=' . $country_id, 'state');


        $data['property_state'] = $this->other_model->select_with_where('*', 'country=' . $country_id, 'property');

        $state_data_json = array();
        $state_data_json['stateDiv'] = $this->load->view('state_div2', $data, TRUE);
        echo json_encode($state_data_json);
    }

    public function update_property_photos_info($property_id)
    {
        $data['property_id'] = $property_id;
        $data3['property_note'] = $this->input->post('property_notess');
        $data3['property_note_id'] = $this->input->post('property_note_id');
        $countNote = count($data3['property_note_id']);
        //echo $countNote; die;
        if ($countNote > 0) {
            $data4['property_id'] = $property_id;
            for ($j = 0; $j < $countNote; $j++) {
                $data4['property_id'] = $property_id;

                $data4['property_note'] = $data3['property_note'][$j];
                $data5['property_note_id'] = $data3['property_note_id'][$j];

                if ($data5['property_note_id'] == 0) {
                    $result_set = $this->other_model->insert('property_notes', $data4);
                } else {
                    $result_set = $this->other_model->note_update($data4, $data5);
                }
            }
        }

        $image = $this->input->post('image');
        if ($image) {
            foreach ($image as $key => $value) {
                $image_data['property_photo'] = $value;
                $image_data['property_id'] = $property_id;
                $result_set = $this->other_model->insert('property_photos', $image_data);
            }
        }

        $data['property_id'] = $this->session->userdata('property_id');
        $data['property_address'] = $this->input->post('property_address');
        $data['city'] = $this->input->post('city');
        $data['zip_code'] = $this->input->post('zip_code');
        $data['country'] = $this->input->post('country');
        $data['state'] = $this->input->post('state');
        $data['bedrooms'] = $this->input->post('bedrooms');
        $data['bathrooms'] = $this->input->post('bathrooms');
        $data['carspaces'] = $this->input->post('carspaces');
        $data['purchase_details'] = $this->input->post('purchase_details');

        $this->form_validation->set_rules('property_address', 'Property Address', 'required');
        $this->form_validation->set_rules('city', 'City', 'required');
        $this->form_validation->set_rules('zip_code', 'Zip Code', 'required');
        $this->form_validation->set_rules('country', 'Country', 'required');
        $this->form_validation->set_rules('state', 'State', 'required');

        if ($this->form_validation->run() == FALSE) {
            redirect('property_photos_info/' . $property_id);
        } else {
            $result = $this->other_model->update_property_info($data);
        }


        if ($result) {
            redirect('property_photos_info/' . $property_id);
        }
    }

    private function set_upload_options_multiple()
    {
        //upload an image options
        $config = array();
        $config['upload_path'] = './uploads/property_file/';
        $config['allowed_types'] = 'jpg|png|gif|jpeg';
        $config['max_size'] = '10000';
        $config['remove_spaces'] = TRUE;
        $config['encrypt_name'] = TRUE;
        $config['overwrite'] = FALSE;

        return $config;
    }

    public function delete_property_image()
    {
        $id = $this->input->get('id');
        $result = $this->other_model->delete_property_photo($id);
    }

    public function delete_property_note()
    {
        $id = $this->input->get('id');
        $result = $this->other_model->delete_property_note($id);
    }

    public function delete_property()
    {
        $property_id = $this->input->get('pid');
        $result = 1;
        if ($result) {
            redirect('property', 'refresh');
        }
    }

    public function property_photo()
    {
        $files = $_FILES;
        if (isset($_FILES['userfile']) && !empty($_FILES['userfile']['name'])) {
            $config['remove_spaces'] = TRUE;
            $config['encrypt_name'] = TRUE;
            $config['upload_path'] = 'uploads/property_file';
            $config['allowed_types'] = 'jpg|png|gif|jpeg';
            $config['max_size'] = '0';
            $this->load->library('upload', $config);
            $this->upload->initialize($config);
            if (!$this->upload->do_upload('userfile')) {
                $status = 'error';
                $msg = $this->upload->display_errors('', '');

            } else {
                $fileinfo = $this->upload->data();
                echo $fileinfo['file_name'];
            }
        }
    }

    public function add_depreciation_detail()
    {
        $property_id = $this->input->post('property_id');
        $data['property_id'] = $this->input->post('property_id');
        $data['depreciation_option'] = $this->input->post('depreciation_option');
        $data['construction_year'] = $this->input->post('construction_year');
        $data['tax_rate'] = $this->input->post('tax_rate');

        $result = $this->other_model->delete_depreciation_detail($property_id);

        $result_set = $this->other_model->insert('depreciation_detail', $data);

        $data3['depreciation_year'] = $this->input->post('depreciation_year');
        $data3['depreciation_amount'] = $this->input->post('depreciation_amount');

        if ($data['depreciation_option'] == 1) {
            $result9 = $this->other_model->delete_depreciation_detail($property_id);
            $result9 = $this->other_model->delete_depreciation_detail_amount($property_id);
            $data9['construction_year'] = 2014;
            $data9['tax_rate'] = 30;
            $data9['depreciation_option'] = $this->input->post('depreciation_option');
            $data9['property_id'] = $this->input->post('property_id');
            $result_set9 = $this->other_model->insert('depreciation_detail', $data9);
        } else {
            $countDepreciation = count($data3['depreciation_amount']);

            //if($countDepreciation>0){
            $result3 = $this->other_model->delete_depreciation_detail_amount($property_id);
            for ($j = 0; $j < $countDepreciation; $j++) {
                $data2['property_id'] = $this->input->post('property_id');
                $data2['depreciation_amount'] = $data3['depreciation_amount'][$j];
                $data2['depreciation_year'] = $data3['depreciation_year'][$j];

                $dyear = $data3['depreciation_year'][$j];
                $pid = $this->input->post('property_id');
                $result_dp = $this->other_model->delete_duplicate_year_data($dyear, $pid);


                if ($data2['depreciation_amount']) {
                    $result2 = $this->other_model->insert('depreciation_detail_amount', $data2);
                }
            }
            //}

        }

        if ($result_set) {
            // redirect('other/financial_dashboard/'.$property_id);
            redirect('other/financial_dashboard/' . $property_id);

        }

    }

    public function view_depreciation_detail($property_id)
    {
        $data['property_id'] = $property_id;
        $data['property'] = $this->other_model->select_with_where('*', "property_id=$property_id", 'property');

        $data['depreciation_detail'] = $this->other_model->select_with_where('*', "property_id=$property_id", 'depreciation_detail');
        // echo "<pre>";
        // print_r($data['depreciation_detail']); die;
        $data['depreciation_detail_amount'] = $this->other_model->select_with_where('*', "property_id=$property_id", 'depreciation_detail_amount');
        // $data['tab_value'] = 4;
        $this->session->set_userdata('tab1', 4);
        // $this->session->set_userdata('tab2',4);

        // $this->session->unset_userdata('tab2');
        // echo $this->session->userdata('tab2'); die;
        // echo "<pre>"; print_r($data); die;

        $this->load->view('financial_dashboard', $data);
    }


    public function view_total()
    {
        $property_id = $this->input->post('property_id');

        //$data = $this->other_model->get_valuation($property_id);
        // $data = $this->other_model->select_with_where('*', "property_id=$property_id", 'purchase_cost_other');
        $data = $this->other_model->get_purchase_cost_other($property_id);
        $data2 = $this->other_model->select_with_where('*', "property_id=$property_id", 'capital_cost');
        $data3 = $this->other_model->select_with_where('*', "property_id=$property_id", 'purchase_detail');
        //$data3 = $this->other_model->valuation_value($property_id);
        $resArrayy = [$data, $data2, $data3];
        echo json_encode($resArrayy);

    }

}
