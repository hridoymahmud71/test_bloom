<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Cronjob extends MX_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model('cronjob_model');
        $this->load->model('property/property_model');
        $this->load->model('lease/lease_model');
        $this->load->model('rent/rent_model');
        $this->load->model('water/water_model');
        $this->load->model('utility/utility_model');
        $this->load->model('expense/expense_model');

        ini_set('memory_limit', '128M');
        ini_set('max_execution_time', 900); //300 seconds = 5 minutes
    }

    public function auto_truncate_statement_table()
    {
        $this->db->empty_table('lease_monthly_statement');
    }

    public function autoMarkRentAsArrear()
    {
        $current_date = date('Y-m-d');
        $current_time = date('Y-m-d H:i:s');

        $last_processing_row = $this->db->select('*')->from('processing_auto_mark_rent_arrear')->get()->row_array();

        $last_processing_time = null;
        if (!empty($last_processing_row)) {
            if (!empty($last_processing_row['last_processing_time'])) {
                $last_processing_time = $last_processing_row['last_processing_time'];
            }
        }

        $go_process = false;

        //first time
        if(empty($last_processing_time)){
            $this->db->truncate('processing_auto_mark_rent_arrear');
            $this->db->insert('processing_auto_mark_rent_arrear', array('last_processing_time' => $current_time));
            exit;
        }

        if (!empty($last_processing_time)) {

            $current_time_numeric = strtotime($current_time);
            $last_processing_time_numeric = strtotime($last_processing_time);

            $gap = 3600*6; //should be 3600*6

            $current_date = date('Y-m-d',$current_time_numeric);
            $last_processing_date = date('Y-m-d',$last_processing_time_numeric);


            //$go_process =  ($current_time_numeric - $gap) > $last_processing_time_numeric;
            $go_process =  $current_date > $last_processing_date;



            //echo " ($current_time_numeric - $gap) > $last_processing_time_numeric) ";
            //echo " $current_date > $last_processing_date ";
            //echo " =  [$go_process] ";exit;

        }


        if ($go_process) {
            //do not need to write in model
            $this->db->set('lease_present_status', 3);
            $this->db->where('payment_due_date <', $current_date);
            $this->db->where('payment_due_amount >', 'lease_rent_recieved', false);
            $this->db->update('lease_payment_scedule');

            //fully paid
            $this->db->set('lease_present_status', 1);
            $this->db->where('payment_due_date <', $current_date);
            $this->db->where('payment_due_amount', 'lease_rent_recieved', false);
            $this->db->update('lease_payment_scedule');

            //insert processing time
            $this->db->truncate('processing_auto_mark_rent_arrear');
            $this->db->insert('processing_auto_mark_rent_arrear', array('last_processing_time' => $current_time));


            //echo $this->db->last_query();
        }

    }

    public function autoCalculateArrears()
    {
        $limit = 50;

        $lease_list = $this->cronjob_model->getLeaseList($limit);
        if (!empty($lease_list)) {
            foreach ($lease_list as $lease) {
                $this->autoCalculateArrearsForOneLease($lease);
            }
        }
    }

    public function autoCalculateArrearsForOneLease($lease)
    {
        $current_date = date('Y-m-d');
        $arrear_payments = $this->cronjob_model->getArrearPaymentsForALease($lease['lease_id'], $current_date);

        $arrear = 0.00;
        if (!empty($arrear_payments)) {

            $total_due = array_sum(array_column($arrear_payments, 'payment_due_amount'));
            $total_received = array_sum(array_column($arrear_payments, 'lease_rent_recieved'));

            if ($total_due > $total_received) {
                $arrear = $total_due - $total_received;
            }

        }

        $lease_details = $this->lease_model->getLeaseDetails($lease['lease_id']);
        if (!empty($lease_details)) {
            $share_count = count($lease_details);
            if ($share_count == 0) {
                $share_count = 1;
            }

            foreach ($lease_details as $lease_detail_item) {


                $upd_lease_details = array();
                $upd_lease_details['payment_update_status'] = 2;
                $upd_lease_details['payment_update_by'] = $arrear / $share_count;

                //uncomment
                $this->db->where('lease_detail_id', $lease_detail_item['lease_detail_id']);
                $this->db->update('lease_detail', $upd_lease_details);


            }
        }
    }

    public function sendReminderForArrearsPayment()
    {
        $limit = 10;

        $this->sendReminderForArrearsPaymentGivenReminderNumber(1, $limit);
        $this->sendReminderForArrearsPaymentGivenReminderNumber(2, $limit);


    }

    private function getTargetDate($reminder_number)
    {
        $daycount = $this->getDayCount($reminder_number);
        $target_date = date('Y-m-d', strtotime("-{$daycount} days"));

        return $target_date;
    }

    private function getDayCount($reminder_number)
    {
        //first reminder will be send after 3 days, second one wil be sent 7 days after due date
        $daycount = 1;
        if ($reminder_number == 1) {
            $daycount = 3;
        } else if ($reminder_number = 2) {
            $daycount = 7;
        }

        return $daycount;

    }

    private function sendReminderForArrearsPaymentGivenReminderNumber($reminder_number, $limit)
    {
        $target_date = $this->getTargetDate($reminder_number);

        // for 1st reminder 0 email was sent before, for 2nd reminder 1 email was sent before ....
        $arrears_reminder_mail_count = $reminder_number - 1;

        $payments = $this->cronjob_model->getRentsInArrears($target_date, $limit, $arrears_reminder_mail_count);

        /*echo "<pre>";
        echo $this->db->last_query().'<br>';
        print_r($payments);
        echo "</pre>";
        die();*/
        if ($payments) {
            foreach ($payments as $payment) {
                $this->sendReminderForPayment($payment, $reminder_number);
            }
        }
    }

    private function joinNameParts($fname, $lname)
    {
        return $fname . ' ' . $lname;
    }

    public function sendReminderForPayment($payment, $reminder_number)
    {
        $site_name = $this->config->item('site_name');
        $base = $this->config->base_url();
        $img_src = "{$base}assets/img/logo.png";
        $img = "<img src='$img_src'><br>";

        $full_property_address = $this->utility_model->getFullPropertyAddress($payment['property_id']);

        $property = $this->cronjob_model->getProperty($payment['property_id']);
        $property_with_landlord = $this->utility_model->getPropertyWithLandlord($payment['property_id']);
        $lease = $this->cronjob_model->getLease($payment['lease_id']);
        $tenants_with_lease_detail = $this->utility_model->getTenantsWithLeaseDetails($payment['lease_id']);
        $daycount = $this->getDayCount($reminder_number);
        $dollar_sign = "$";

        $landlord_name = "";
        $landlord_phone = "";
        if (!empty($property_with_landlord)) {
            $landlord_name = $property_with_landlord['user_fname'] . ' ' . $property_with_landlord['user_lname'];
            $landlord_phone = $property_with_landlord['phone'];
        }


        if ($property && $lease && $tenants_with_lease_detail) {
            $tenant_ids = array();

            $user_fname = array_column($tenants_with_lease_detail, 'user_fname');
            $user_lname = array_column($tenants_with_lease_detail, 'user_lname');

            $tenant_names = "";
            $tenant_names_array = (array_map("self::joinNameParts", $user_fname, $user_lname));

            if (count($tenant_names_array) > 0) {
                $tenant_names = implode(' & ', $tenant_names_array);
            }

            $due_date = date("jS F, Y", strtotime($payment['payment_due_date']));
            $payment_start_period = date("jS F, Y", strtotime($payment['payment_start_period']));
            $payment_end_period = date("jS F, Y", strtotime($payment['payment_start_period']));

            $rectify_text = "";
            if ($reminder_number == 1) {
                $rectify_text = "Can you please ensure rent is transferred as soon as possible.";
            }
            if ($reminder_number == 2) {
                $rectify_text = "Please rectify immediately to prevent further action.";
            }

            $subject = "URGENT MESSAGE: Rent Arrears.Due date: {$due_date}";
            $message = '';
            $message .= "<span style='background-color:yellow'>Hi {$tenant_names} ,<br>
                         Your rent was due on {$due_date}. {$rectify_text}<br>
                         You are {$daycount} days behind.<br>
                         --------------------------------- <br>
                         <b>Address:</b> {$full_property_address} <br>
                         <b>Lease:</b> {$lease['lease_name']} <br>                                      
                         <b>Rent Due Date:</b> {$due_date} <br>      
                         <b>Rent Period:</b> {$payment_start_period} to {$payment_end_period} <br>    
                         <b>Amount:</b> {$dollar_sign}{$payment['payment_due_amount']} <br>                                    
                         --------------------------------- <br><br>
                         Thank you <br><br>
                         
                         Please contact {$landlord_name} on {$landlord_phone} if you have any questions or would like to discuss payment in relation to this email.<br>
                         This email has been generated by {$site_name} Software.<br><br>
                         {$img}
                         </span>";
            foreach ($tenants_with_lease_detail as $tenant_with_lease_detail) {

                $mail_data['to'] = $tenant_with_lease_detail['email'];
                $mail_data['subject'] = $subject;
                $mail_data['message'] = $message;

                $this->utility_model->insertAsMessage(null, $payment['property_id'], $payment['lease_id'], array($tenant_with_lease_detail['user_id']), $subject, $message, null);

                if ($this->sendEmail($mail_data)) {
                    $this->updatePaymentArrearsMailSent($payment, $reminder_number);
                }
            }

        }

    }

    public function autoSendWaterDueInvoiceEmails()
    {
        $due_water_invoices = $this->cronjob_model->getDueWaterInvoices($limit = 10, $mail_sent = 0);

        if (!empty($due_water_invoices)) {

            foreach ($due_water_invoices as $due_water_invoice) {

                $this->autoSendAnWaterDueInvoiceEmail($due_water_invoice);

            }
        }

    }

    public function autoSendAnWaterDueInvoiceEmail($due_water_invoice)
    {
        $site_name = $this->config->item('site_name');
        $base = $this->config->base_url();
        $img_src = "{$base}assets/img/logo.png";
        $img = "<img src='$img_src'><br>";

        $full_property_address = $this->utility_model->getFullPropertyAddress($due_water_invoice['property_id']);

        $property = $this->cronjob_model->getProperty($due_water_invoice['property_id']);
        $property_with_landlord = $this->utility_model->getPropertyWithLandlord($due_water_invoice['property_id']);
        $lease = $this->cronjob_model->getLease($due_water_invoice['lease_id']);
        $tenants_with_lease_detail = $this->utility_model->getTenantsWithLeaseDetails($due_water_invoice['lease_id']);

        $landlord_name = "";
        $landlord_phone = "";
        $landlord_email = "";
        if (!empty($property_with_landlord)) {
            $landlord_name = $property_with_landlord['user_fname'] . ' ' . $property_with_landlord['user_lname'];
            $landlord_phone = $property_with_landlord['phone'];
            $landlord_email = $property_with_landlord['email'];
        }
        $tenant_names = "";
        if (!empty($tenants_with_lease_detail)) {
            $user_fname = array_column($tenants_with_lease_detail, 'user_fname');
            $user_lname = array_column($tenants_with_lease_detail, 'user_lname');

            $tenant_names = "";
            $tenant_names_array = (array_map("self::joinNameParts", $user_fname, $user_lname));

            if (count($tenant_names_array) > 0) {
                $tenant_names = implode(' & ', $tenant_names_array);
            }
        }
        $dollar_sign = "$";

        $water_invoice_due_date = date("jS F, Y", strtotime($due_water_invoice['water_invoice_due_date']));
        $billing_period = date("F, Y", strtotime($due_water_invoice['water_invoice_period_start'] . "-01")) . ' to ' . date("F, Y", strtotime($due_water_invoice['water_invoice_period_end'] . "-01"));


        //---------------------------------To Tenants-------------------------------------------------------------------

        $subject = "URGENT: Overdue Water Invoice";
        $message = '';
        $message .= "Hi {$tenant_names} ,<br>
                         Your water invoice for period $billing_period is overdue. Please find the details below:<br>
                         --------------------------------- <br>                      
                         <b>Tenants:</b> $tenant_names<br>
                         <b>Address:</b> {$full_property_address} <br><br>
                         <b>Details of the invoice below:</b><br>
                         --------------------------------- <br>   
                         <b>Invoice Number:</b> {$due_water_invoice['water_invoice_number']}<br>                                                         
                         <b>Due Date:</b> {$water_invoice_due_date} <br>      
                         <b>Period:</b> {$billing_period} <br>    
                         <b>Invoice Amount:</b> {$dollar_sign}{$due_water_invoice['water_invoice_amount']} <br>   
                         <b>Metre Read:</b> {$due_water_invoice['water_invoice_water_metre']}<br>                                
                         <b>Amount Received to Date:</b> {$dollar_sign}{$due_water_invoice['water_invoice_amount_paid']} <br>                                    
                         <b>Amount Due:</b> {$dollar_sign}{$due_water_invoice['water_invoice_amount_due']} <br>  
                         Thank you <br><br>
                         Please make payment directly to {$landlord_name}. If you have any questions in relation to this 
                         invoice please contact {$landlord_name} on {$landlord_phone}<br>
                        
                         This email has been generated by {$site_name} Software.<br><br>
                         {$img}
                         ";
        foreach ($tenants_with_lease_detail as $tenant_with_lease_detail) {

            $mail_data = array();
            $mail_data['to'] = $tenant_with_lease_detail['email'];
            $mail_data['subject'] = $subject;
            $mail_data['message'] = $message;

            $this->utility_model->insertAsMessage(null, $due_water_invoice['property_id'], $due_water_invoice['lease_id'], array($tenant_with_lease_detail['user_id']), $subject, $message, null);

        }

        $noti_text = "You have an over-due invoice for water";
        $noti_url = "editWaterInvoice/{$due_water_invoice['water_invoice_id']}";
        $this->utility_model->insertAsNotification(null, $due_water_invoice['property_id'], $due_water_invoice['lease_id'], $noti_text, $noti_url, null);

        $this->sendEmail($mail_data);
        //--------------------------------------------------------------------------------------------------------------

        //----------------------------------To Landlords----------------------------------------------------------------

        $subject = "URGENT: Tenants water invoice due";
        $message = '';
        $message .= "Hi {$landlord_name} ,<br>
                         Please be advised that water invoice number {$due_water_invoice['water_invoice_number']} is 
                         overdue for the following tenancy:<br>
                         --------------------------------- <br>
                         <b>Tenants:</b> $tenant_names<br>
                         <b>Address:</b> {$full_property_address} <br><br>
                         <b>Details of the invoice below:</b><br>
                         --------------------------------- <br>   
                         <b>Invoice Number:</b> {$due_water_invoice['water_invoice_number']}<br>                                                         
                         <b>Due Date:</b> {$water_invoice_due_date} <br>      
                         <b>Period:</b> {$billing_period} <br>    
                         <b>Invoice Amount:</b> {$dollar_sign}{$due_water_invoice['water_invoice_amount']} <br>   
                         <b>Metre Read:</b> {$due_water_invoice['water_invoice_water_metre']}<br>                                
                         <b>Amount Received to Date:</b> {$dollar_sign}{$due_water_invoice['water_invoice_amount_paid']} <br>                                    
                         <b>Amount Due:</b> {$dollar_sign}{$due_water_invoice['water_invoice_amount_due']} <br>                                      
                         --------------------------------- <br><br>                         
                         This email has been generated by {$site_name} Software.<br><br>
                         {$img}
                         ";

        $mail_data = array();
        $mail_data['to'] = $landlord_email;
        $mail_data['subject'] = $subject;
        $mail_data['message'] = $message;

        $this->sendEmail($mail_data);
        //--------------------------------------------------------------------------------------------------------------

        $this->db->set('water_invoice_due_email_count', $due_water_invoice['water_invoice_due_email_count'] + 1);
        $this->db->where('water_invoice_id', $due_water_invoice['water_invoice_id']);
        $this->db->update('water_invoice');


    }


    public
    function sendEmail($mail_data)
    {
        $this->load->library('email');
        $site_name = $this->config->item('site_name');
        $site_email = $this->config->item('site_email');

        try {
            //$mail_data['to'] = 'mahmud@sahajjo.com';
            $this->email->initialize(array('priority' => 1));
            $this->email->clear(TRUE);
            $this->email->from($site_email, $site_name);
            $this->email->to($mail_data['to']);
            $this->email->bcc('mahmud@sahajjo.com');

            $this->email->subject($mail_data['subject']);
            $this->email->message($mail_data['message']);
            $this->email->set_mailtype("html");

            if (array_key_exists('single_pdf_content', $mail_data)
                &&
                array_key_exists('pdf_file_name', $mail_data)) {
                $this->email->attach($mail_data['single_pdf_content'], 'attachment', $mail_data['pdf_file_name'], 'application/pdf');
            }


            @$this->email->send();

            //echo '<hr>' . '<br>';
            //echo $mail_data['subject'] . '<br>';
            //echo $mail_data['message'], '<br>';
            //echo '<hr>' . '<br>';
            //echo "<pre>";print_r($mail_data);"</pre><br><hr>";

            /*$headers = 'From: '.$site_email.'\r\n';
            mail($mail_data['to'],$mail_data['subject'],$mail_data['message'],$headers);*/

            return true;

        } catch (Exception $e) {
            echo $e->getMessage();
        }
        //exit;

    }

    private function updatePaymentArrearsMailSent($payment, $reminder_number)
    {
        $upd_data['arrears_reminder_mail_count'] = $reminder_number;

        $this->cronjob_model->update_function('payment_schedule_id', $payment['payment_schedule_id'], 'lease_payment_scedule', $upd_data);

    }

    private function insertPaymentArrearsMailSentAsMessages($payment, $property, $lease, $reminder_number, $subject, $message, $tenant_ids)
    {
        $owner_id = $property['user_id'];

        $ins_msg_data['user_id'] = $owner_id;
        $ins_msg_data['property_id'] = $payment['property_id'];
        $ins_msg_data['lease_id'] = $payment['lease_id'];
        $ins_msg_data['subject'] = $subject;
        $ins_msg_data['created_at'] = date('Y-m-d H:i:s');
        $message_id = $this->cronjob_model->insert_ret('message', $ins_msg_data);

        $ins_msg_cmt_data['message_id'] = $message_id;
        $ins_msg_cmt_data['comment'] = $message;
        $comment_id = $this->cronjob_model->insert_ret('message_comment', $ins_msg_cmt_data);

        if (!empty($tenant_ids)) {
            foreach ($tenant_ids as $tenant_id) {

                $ins_msg_tnt_data['message_id'] = $message_id;
                $ins_msg_tnt_data['tenant_id'] = $tenant_id;
                $this->cronjob_model->insert_ret('message_tenant', $ins_msg_tnt_data);
            }
        }
    }

    public function mark_completed_leases_as_inactive()
    {
        $curr_date = date('Y-m-d');
        $this->cronjob_model->mark_completed_leases_as_inactive($curr_date);
    }

//------------------------------------------------------------------------------------------------------------------
    public function auto_send_monthly_statements()
    {
        $today = date('Y-m-d');
        $last_month = date('Y-m', strtotime('-1 months'));
        $first_day_of_current_month = date('Y-m-01', strtotime(date('Y-m-d')));

        $stmt_by_mon = $this->utility_model->countStatementByMonth($last_month);


        if ($stmt_by_mon == 0) {

            $this->create_lease_monthly_statements($last_month);
            echo "<created><br>";

        } else {

            //remove || 1 , it is only for testing
            if (($today == $first_day_of_current_month) || 1) {
                $this->send_lease_monthly_statements(10, $last_month);
                echo "<sent><br>";
            }

        }

    }

    private function send_lease_monthly_statements($limit, $month)
    {
        $lease_monthly_statements = $this->cronjob_model->getUnsentLeaseMonthlyStatements($limit, $month);

        if ($lease_monthly_statements) {
            foreach ($lease_monthly_statements as $lms) {
                //$this->tenant_ledgers_pdf_email($lms);
                $this->statement($lms['lease_id'], true, $lms['lease_monthly_statement_month'], "email");

                $this->db->set('lease_monthly_statement_sent', 1);
                $this->db->where('lease_monthly_statement_id', $lms['lease_monthly_statement_id']);
                $this->db->update('lease_monthly_statement');

            }
        }

    }


    private function create_lease_monthly_statements($month)
    {
        $active_leases = $this->utility_model->getAllLease($only_active = true);

        $ins_list_data = array();

        if ($active_leases) {
            foreach ($active_leases as $lease) {
                $ins_data = array();
                $ins_data['property_id'] = $lease['property_id'];
                $ins_data['lease_id'] = $lease['lease_id'];
                $ins_data['lease_monthly_statement_month'] = $month;

                $ins_list_data[] = $ins_data;
            }
        }

        if (!empty($ins_list_data)) {
            $this->db->insert_batch('lease_monthly_statement', $ins_list_data);
        }


    }

//------------------------------------------------------------------------------------------------------------------

    public function autoSendInspectionReminder()
    {
        $limit = 10;
        $notification_count = 0; //no notifications yet
        $day_before = "1"; //one day before inspection
        $inspection_ids = $this->utility_model->getInspectionIdsForNotification($notification_count, $limit, $day_before);

        if (!empty($inspection_ids) && is_array($inspection_ids)) {
            foreach ($inspection_ids as $inspection_id) {
                $this->sendInspectionReminder($inspection_id);
            }
        }

    }

    public function sendInspectionReminder($inspection_id)
    {
        $inspection = $this->utility_model->getInspection($inspection_id);

        if (!empty($inspection)) {

            //---------------------------------------------------
            $site_name = $this->config->item('site_name');
            $base = $this->config->base_url();
            $img_src = "{$base}assets/img/logo.png";
            $img = "<img src='$img_src'><br>";

            $full_property_address = $this->utility_model->getFullPropertyAddress($inspection['property_id']);

            $property = $this->cronjob_model->getProperty($inspection['propearty_id']);
            $property_with_landlord = $this->utility_model->getPropertyWithLandlord($inspection['property_id']);
            $lease = $this->cronjob_model->getLease($inspection['lease_id']);
            $tenants_with_lease_detail = $this->utility_model->getTenantsWithLeaseDetails($inspection['lease_id']);

            $landlord_name = "";
            $landlord_phone = "";
            if (!empty($property_with_landlord)) {
                $landlord_name = $property_with_landlord['user_fname'] . ' ' . $property_with_landlord['user_lname'];
                $landlord_phone = $property_with_landlord['phone'];
            }
            $tenant_names = "";
            if (!empty($tenants_with_lease_detail)) {
                $user_fname = array_column($tenants_with_lease_detail, 'user_fname');
                $user_lname = array_column($tenants_with_lease_detail, 'user_lname');

                $tenant_names = "";
                $tenant_names_array = (array_map("self::joinNameParts", $user_fname, $user_lname));

                if (count($tenant_names_array) > 0) {
                    $tenant_names = implode(' & ', $tenant_names_array);
                }
            }
            $dollar_sign = "$";
            $inspection_date = date("jS F, Y", strtotime($inspection['inspection_date']));
            $inspection_time_st = date("g:ia", strtotime($inspection['inspection_start_time']));
            $inspection_time_en = date("g:ia", strtotime($inspection['inspection_end_time']));
            //------------------------------------------------------------------------------------------------

            //---------------------------------To Tenants-------------------------------------------------------------------

            $subject = "Inspection: On {$inspection_date}, from {$inspection_time_st} to {$inspection_time_en}";
            $message = '';
            $message .= "Hi {$tenant_names} ,<br>
                         An inspection is scheduled on {$inspection_date} between {$inspection_time_st} and {$inspection_time_en}. Please find the details below:.<br>
                         --------------------------------- <br>                      
                         <b>Tenants:</b> $tenant_names<br>
                         <b>Address:</b> {$full_property_address} <br><br>
                         --------------------------------- <br>  
                         <b>Details of the inspection below:</b><br>                        
                         <b>Inspection Title:</b> {$inspection['inspection_title']}<br>                                                         
                         <b>Inspection Description:</b> {$inspection['inspection_description']} <br>  
                         <b>Date:</b> {$inspection_date} <br>         
                         <b>Time:</b> {$inspection_time_st} - {$inspection_time_en} <br>
                         --------------------------------- <br>                               
                         Thank you <br><br>
                         If you have any questions in relation to this 
                         inspection please contact {$landlord_name} on {$landlord_phone}<br>
                        
                         This email has been generated by {$site_name} Software.<br><br>
                         {$img}
                         ";
            foreach ($tenants_with_lease_detail as $tenant_with_lease_detail) {

                $mail_data = array();
                $mail_data['to'] = $tenant_with_lease_detail['email'];
                $mail_data['subject'] = $subject;
                $mail_data['message'] = $message;

                $this->utility_model->insertAsMessage(null, $inspection['property_id'], $inspection['lease_id'], array($tenant_with_lease_detail['user_id']), $subject, $message, null);

                $this->sendEmail($mail_data);

            }
            //--------------------------------------------------------------------------------------------------------------

            //----------------------------------To Landlords----------------------------------------------------------------

            $subject = "Inspection: On {$inspection_date}, from {$inspection_time_st} to {$inspection_time_en}";
            $message = '';
            $message .= "Hi {$landlord_name} ,<br>
                         An inspection is scheduled on {$inspection_date} between {$inspection_time_st} and {$inspection_time_en}  . Please find the details below:.<br>
                         --------------------------------- <br>                        
                         <b>Tenants:</b> $tenant_names<br>
                         <b>Address:</b> {$full_property_address} <br><br>
                         --------------------------------- <br>  
                         <b>Details of the inspection below:</b><br>
                         <b>Inspection Title:</b> {$inspection['inspection_title']}<br>                                                         
                         <b>Inspection Description:</b> {$inspection['inspection_description']} <br>         
                         <b>Date:</b> {$inspection_date} <br>         
                         <b>Time:</b> {$inspection_time_st} - {$inspection_time_en} <br>         
                         --------------------------------- <br><br>                         
                         This email has been generated by {$site_name} Software.<br><br>
                         {$img}
                         ";


            $mail_data = array();
            $mail_data['to'] = $property_with_landlord['email'];
            $mail_data['subject'] = $subject;
            $mail_data['message'] = $message;

            $this->sendEmail($mail_data);

            $noti_text = "Inspection: On {$inspection_date}, from {$inspection_time_st} to {$inspection_time_en}";
            $noti_url = "editInspection/{$inspection['inspection_id']}";
            $this->utility_model->insertAsNotification(null, $inspection['property_id'], $inspection['lease_id'], $noti_text, $noti_url, null);
            //--------------------------------------------------------------------------------------------------------------

            $this->db->set('inspection_notification_count', $inspection['inspection_notification_count'] + 1);
            $this->db->where('inspection_id', $inspection['inspection_id']);
            $this->db->update('inspection');

        }
    }

//------------------------------------------------------------------------------------------------------------------


//------------------------------------------------------------------------------------------------------------------

    public function modify_water_period($item)
    {

        if (isset($item['sub_type']) && $item['type'] == "water") {
            if (!empty($item['sub_type'])) {
                $exp = explode(" to ", $item['sub_type']);

                $d1 = isset($exp[0]) ? date("M,Y", strtotime($exp[0] . '-01')) : "";
                $d2 = isset($exp[1]) ? date("M,Y", strtotime($exp[1] . '-01')) : "";

                if ($d1 && $d2) {

                    $item['sub_type'] = implode(" to ", array($d1, $d2));
                }
            }

        }

        return $item;


    }

    public function modify_rent_issue($item)
    {

        if (isset($item['sub_type']) && $item['type'] == "rent") {
            if (!empty($item['id'])) {
                $item['sub_type'] = $this->utility_model->get_rent_payment_ref($item['id']);
            }

        }

        return $item;


    }

    public function sort_count($a, $b)
    {
        if ($a['date'] === $b['date']) {
            return 0;
        } else {
            return ($a['date'] > $b['date'] ? 1 : -1);
        }
    }

    public function statement($lease_id, $via_cron = null, $given_month = null, $given_mode = null)
    {
        $lease = $this->utility_model->getLease($lease_id);

        $site_name = $this->config->item('site_name');
        $base = $this->config->base_url();
        $img_src = "{$base}assets/img/logo.png";
        $img = "<img src='$img_src'><br>";

        $mode = "show";
        $range = "";
        $month = "";

        $start_date = "";
        $end_date = "";

        $statement_title = "";
        $already_got_month = false;

        if (isset($_GET['mode'])) {
            if (!empty($_GET['mode'])) {
                $mode = $_GET['mode'];
            }
        }

        if ($given_mode) {
            $mode = $given_mode;
        }

        if (isset($_GET['range'])) {
            if (!empty($_GET['range'])) {
                $range = $_GET['range'];
                $exploded_range = explode("to", $range);

                $start_date = $exploded_range[0];
                $end_date = $exploded_range[1];
                $statement_title = "Income & Expenditure Report";
            }
        }

        if ($statement_title == "") {
            $statement_title = "Monthly Statement";
        }

        if ($given_month) {
            $month = $given_month;
            $already_got_month = true;
        }
        if (isset($_GET['month']) && !$already_got_month) {
            if (!empty($_GET['month'])) {
                $month = $_GET['month'];
            }
        }

        if (!empty($month)) {
            $start_date = $month . '-01';
            $end_date = date("Y-m-t", strtotime($start_date));
        }


        if ($lease && !empty($start_date) && !empty($end_date)) {

            $property_id = $lease['property_id'];

            $incomes = $this->utility_model->getIncomes($property_id, $lease_id, $start_date, $end_date);
            $expenses = $this->utility_model->getExpenses($property_id, $lease_id, $start_date, $end_date);

            $data['total_income'] = 0.00;
            $data['total_expense'] = 0.00;


            if (!empty($incomes)) {
                $data['total_income'] = array_sum(array_column($incomes, 'income'));
            }
            if (!empty($expenses)) {

                $data['total_expense'] = array_sum(array_column($expenses, 'expense'));
            }

//----------------------------------------------------------------------------------------------------------
            $property = $this->utility_model->getProperty($property_id);
            $property_owner = $this->utility_model->getPropertyOwner($property_id);

            $full_property_address = $this->utility_model->getFullPropertyAddress($property_id);

            $data['full_property_address'] = $full_property_address;

            $tenants = $this->utility_model->getTenants($lease_id);
            $data['property'] = $property;
            $data['lease'] = $lease;
            $data['state_name'] = $this->utility_model->getStateName($property['state']);
            $data['property_owner'] = $property_owner;

            $data['statement_start_date'] = "Unknown Date";
            $data['statement_end_date'] = "Unknown Date";

            if ($start_date && $end_date) {
                $data['statement_start_date'] = date('d.m.Y', strtotime($start_date));
                $data['statement_end_date'] = date('d.m.Y', strtotime($end_date));
            }

            $incomes = array_map("self::modify_water_period", $incomes);
            $incomes = array_map("self::modify_rent_issue", $incomes);

            if (!empty($incomes)) {
                $this->utility_model->sortBy("date", $incomes);
            }

            if (!empty($expenses)) {
                $this->utility_model->sortBy("date", $expenses);
            }

            /*echo "<pre>";
            print_r($incomes);
            print_r($expenses);
            echo "</pre>";*/


            $data['incomes'] = $incomes;
            $data['expenses'] = $expenses;

            //----------------------------------------------------------------------------------------------------------

            $html = $this->load->view('statement_pdf', $data, true);
            //echo $html;exit;

            $pdf = $this->prepare_statement_pdf($html, $mode);

            if ($mode == "email" and $pdf) {

                $message = "";
                $message .= "Hi {$property_owner['user_fname']}  {$property_owner['user_lname']} ,<br>
                                        You have a statement of Income & Expenses for the period {$data['statement_start_date']} to {$data['statement_end_date']} for the following property: <br>
                                        {$full_property_address}<br><br>
                                        
                                        This email has been generated by {$site_name} Software.<br><br>
                                        {$img}
                                        ";

                $subject = "You have a statement of Income & Expenses for the period {$data['statement_start_date']} to {$data['statement_end_date']}";

                $mail_data['to'] = $property_owner['email'];
                $mail_data['subject'] = $subject;
                $mail_data['message'] = $message;
                $mail_data['single_pdf_content'] = $pdf['single_pdf_content'];
                $mail_data['pdf_file_name'] = $pdf['pdf_file_name'];

                $this->utility_model->insertAsMessage(null, $property_id, $lease_id, array($property_owner['user_id']), $subject, $message, array($pdf['pdf_file_name']));

                $this->sendEmail($mail_data);
                if (!$via_cron) {
                    $this->session->set_flashdata('success', "success");
                    $this->session->set_flashdata('mail_send_success', "mail_send_successs");
                    redirect("rentSchedule/{$property_id}/0/");
                }

            }


        }


    }

    public function auto_send_unpaid_remainder_of_expense()
    {
        $site_name = $this->config->item('site_name');
        $base = $this->config->base_url();
        $img_src = "{$base}assets/img/logo.png";
        $img = "<img src='$img_src'><br>";

        $current_date = date('Y-m-d');

        $time_crossed_expenses = $this->expense_model->getTimeCrossedExpenses($current_date);

        if (count($time_crossed_expenses) > 0) {
            foreach ($time_crossed_expenses as $tce) {

                $dollar_sign = "$";
                $total_invoice_amount = number_format($tce['total_invoice_amount'], 2, '.', '');
                $expense_partial_amount_paid = number_format($tce['expense_partial_amount_paid'], 2, '.', '');

                $inv_num = '00' . $tce['property_id'] . $tce['expense_id'] . date('Ymd', strtotime($tce['created_at'])) . date('Ymd', strtotime($tce['expense_payment_due'])) . '00';

                $property = $this->expense_model->getProperty($tce['property_id']);
                $active_lease = $this->expense_model->getActiveLeaseOfAProperty($tce['property_id']);

                $full_property_address = $this->utility_model->getFullPropertyAddress($tce['property_id']);

                $property_with_landlord = $this->utility_model->getPropertyWithLandlord($tce['property_id']);
                $tenants_with_lease_detail = $this->utility_model->getTenantsWithLeaseDetails($active_lease['lease_id']);

                $tenant_names = "";
                if (!empty($tenants_with_lease_detail)) {
                    $user_fname = array_column($tenants_with_lease_detail, 'user_fname');
                    $user_lname = array_column($tenants_with_lease_detail, 'user_lname');
                    $tenant_names_array = (array_map("self::joinNameParts", $user_fname, $user_lname));
                }


                if (count($tenant_names_array) > 0) {
                    $tenant_names = implode(' & ', $tenant_names_array);
                }

                $landlord_name = "";
                $landlord_phone = "";
                if (!empty($property_with_landlord)) {
                    $landlord_name = $property_with_landlord['user_fname'] . ' ' . $property_with_landlord['user_lname'];
                    $landlord_phone = $property_with_landlord['phone'];
                }

                $due_date = empty($tce['expense_payment_due']) || $tce['expense_payment_due'] == "1970-01-01" ? "Not Available" : date("jS F, Y", strtotime($tce['expense_payment_due']));
                $bill_status = "Not Paid";
                if ($tce['expense_paying_status'] == 1) {
                    $bill_status = "Fully Paid";
                } else if ($tce['expense_paying_status'] == 2) {
                    $bill_status = "Partially Paid";
                } else if ($tce['expense_paying_status'] == 3) {
                    $bill_status = "Not Paid";
                }

                if (count($property) > 0) {
                    $property_owner = $this->expense_model->getUser($property['user_id']);

                    if (count($property_owner) > 0) {

                        $mail_data['to'] = $property_owner['email'];

                        $subject = "URGENT Expense due for payment.";


                        $message = "Hi {$landlord_name} ,<br>
                        Please be advised that an expense is overdue for the following property:<br>
                         --------------------------------- <br>
                         <b>Address:</b> {$full_property_address} <br><br>
                         <b>Tenant(s):</b> $tenant_names<br> <br>                        
                         <b>Details of the invoice below:</b><br>
                         --------------------------------- <br>  
                         <b>Amount Received to Date:</b> {$expense_partial_amount_paid}<br> 
                         <b>Tradesman:</b> {$tce['expense_bill_from']}<br>   
                         <b>Invoice Amount:</b> {$dollar_sign}{$total_invoice_amount}<br> 
                         <b>Bill Status:</b> {$bill_status}<br>
                         <b>Invoice Number:</b> {$inv_num}<br> 
                         <b>Due Date:</b> {$due_date} <br>    
                         <b>Invoice Type:</b> {$tce['tax_code']}<br>   
                         <b>Additional Notes:</b> {$tce['description']}<br>
                         --------------------------------- <br><br>                         
                         This email has been generated by {$site_name} Software.<br><br>
                         {$img}
                         ";

                        $mail_data['subject'] = $subject;
                        $mail_data['message'] = $message;

                        $this->sendEmail($mail_data);

                        $upd_data['email_to_owner_counter'] = (int)$tce['email_to_owner_counter'] + 1;
                        $this->expense_model->update_function('expense_id', $tce['expense_id'], 'expense', $upd_data);

                        $noti_text = "You have an outstanding expense";
                        $noti_url = "viewExpense/{$tce['property_id']}#overdue-{$tce['expense_id']}";
                        $this->utility_model->insertAsNotification(null, $tce['property_id'], 0, $noti_text, $noti_url, null);
                    }

                    if ($active_lease) {
                        $lease_tenants = $this->expense_model->getLeaseTenants($active_lease['lease_id']);
                        if ($lease_tenants && count($lease_tenants) > 0) {
                            foreach ($lease_tenants as $lease_tenant) {

                                // $mail_data['to'] = $lease_tenant['email'];
                                // $subject = "An expense was unpaid";
                                // $message = "Dear $tenant_names";
                                // $message .= "<br>";
                                // $message .= "This email is a reminder that invoice number {$inv_num} is due on {$due_date}
                                // for {$dollar_sign}{$total_invoice_amount} (partially paid {$dollar_sign}{$expense_partial_amount_paid}).<br>If payment has been made head over to Rent Simple to mark as paid and attach a receipt!";
                                // $message .= "<br>";
                                // $message .= "<br>";
                                // $message .= "Thank you";
                                // $message .= "<br>";
                                // $message .= "Rent Simple";
                                // $message .= "<br>";
                                // $message .= "This email has been generated by {$site_name} Software.<br><br>
                                // {$img}
                                // ";
                                // $mail_data['subject'] = $subject;
                                // $mail_data['message'] = $message;
                                // $this->utility_model->insertAsMessage(null, $tce['property_id'], $active_lease['lease_id'], array($lease_tenant['tenant_id']), $subject, $message, null);
                                // $this->sendEmail($mail_data);

                            }
                        }
                    }


                }

            }
        }

    }

    private function prepare_statement_pdf($html, $mode)
    {
        $this->load->library('MPDF/mpdf');

        $mpdf = new mPDF('win-1252', 'A4', '', '', 5, 5, 16, 60, 5, 5, 'C');
        $mpdf->useOnlyCoreFonts = true;    // false is default
        $mpdf->SetProtection(array('print'));
        $mpdf->SetTitle('Statement');
        $mpdf->SetAuthor('Rent simple');
        $mpdf->SetDisplayMode('fullpage');

        $mpdf->debug = true;
        $filename = 'statement' . rand(10000, 99999) . '.pdf';
        $mpdf->WriteHTML($html);
        $content = '';

        if ($mode == 'email') {
            $mpdf->Output('uploads/message_document/' . $filename, 'F');
            $content = $mpdf->Output($filename, 'S');
        } else if ($mode == 'show') {
            $mpdf->Output($filename, 'I');
            exit;
        }


        return array('single_pdf_content' => $content, 'pdf_file_name' => $filename);
    }


}