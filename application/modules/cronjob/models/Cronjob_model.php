<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Cronjob_model extends CI_Model
{

    public function __construct()
    {
        parent::__construct();
    }

    public function insert_ret($tablename, $tabledata)
    {
        $this->db->insert($tablename, $tabledata);
        return $this->db->insert_id();
    }

    public function update_function($columnName, $columnVal, $tableName, $data)
    {
        $this->db->where($columnName, $columnVal);
        $this->db->update($tableName, $data);
    }


    public function getRentsInArrears($target_date, $limit, $arrears_reminder_mail_count)
    {
        $this->db->select('*');
        $this->db->from('lease_payment_scedule');
        $this->db->where('lease_present_status', 3);
        $this->db->where('payment_due_date < ', $target_date);
        $this->db->where('arrears_reminder_mail_count', $arrears_reminder_mail_count);
        $this->db->limit($limit);

        $query = $this->db->get();
        $result = $query->result_array();
        return $result;
    }

    public function getProperty($property_id)
    {
        $this->db->select('*');
        $this->db->from('property');
        $this->db->where('property_id', $property_id);

        $query = $this->db->get();
        $row = $query->row_array();
        return $row;
    }

    public function getLease($lease_id)
    {
        $this->db->select('*');
        $this->db->from('lease');
        $this->db->where('lease_id', $lease_id);

        $query = $this->db->get();
        $row = $query->row_array();
        return $row;
    }

    public function getTenantsWithLeaseDetails($lease_id)
    {
        $this->db->select('*');
        $this->db->from('lease_detail');
        $this->db->where('lease_id', $lease_id);
        $this->db->join('user', 'lease_detail.tenant_id=user.user_id');

        $query = $this->db->get();
        $result = $query->result_array();

        return $result;
    }


    public function mark_completed_leases_as_inactive($curr_date)
    {
        $this->db->set('lease_current_status', 0);
        $this->db->where('lease_end_date <', $curr_date);
        $this->db->where('lease_current_status', 1);
        $this->db->update('lease');
    }

    public function countLeasesWithoutMonthlyStatements($month)
    {
        $this->db->select('*');
        $this->db->from('lease');
        $this->db->where('lease_has_monthly_statements', 0);
        $this->db->where('lease_monthly_statement_month', $month);

        $query = $this->db->get();
        $result = $query->num_rows();
        return $result;
    }

    public function getLimitedLeasesWithoutMonthlyStatements($limit)
    {
        $this->db->select('*');
        $this->db->from('lease');
        $this->db->where('lease_has_monthly_statements', 0);
        $this->db->limit($limit);

        $query = $this->db->get();
        $result = $query->result_array();
        return $result;
    }

    public function countLeaseMonthlyStatementsByLease($lease_id)
    {
        $this->db->select('*');
        $this->db->from('lease_monthly_statement');
        $this->db->where('lease_id', $lease_id);

        $query = $this->db->get();
        $result = $query->num_rows();
        return $result;
    }

    public function getUnsentLeaseMonthlyStatements($limit, $month)
    {
        $this->db->select('*');
        $this->db->from('lease_monthly_statement');
        $this->db->where('lease_monthly_statement_sent', 0);
        $this->db->where('lease_monthly_statement_month <=', $month);
        $this->db->limit($limit);

        $query = $this->db->get();
        $result = $query->result_array();
        return $result;
    }

    public function getLeaseList($limit)
    {
        $this->db->select('*');
        $this->db->from('lease');
        $this->db->limit($limit);

        $query = $this->db->get();
        $rows = $query->result_array();
        return $rows;
    }

    public function getLeaseDetails($lease_id)
    {
        $this->db->select('*');
        $this->db->from('lease_detail');
        $this->db->where('lease_id', $lease_id);

        $query = $this->db->get();
        $rows = $query->result_array();
        return $rows;
    }

    public function getArrearPaymentsForALease($lease_id,$current_date)
    {
        $this->db->select('*');
        $this->db->from('lease_payment_scedule');
        $this->db->where('lease_id', $lease_id);
        $this->db->where('payment_due_date <', $current_date);
        $this->db->where('payment_due_amount >', 'lease_rent_recieved', false);

        $query = $this->db->get();
        $rows = $query->result_array();
        return $rows;
    }

    public function getDueWaterInvoices($limit ,$mail_sent)
    {
        $date = date("Y-m-d");

        $this->db->select('*');
        $this->db->from('water_invoice');
        $this->db->where('water_invoice_due_email_count', $mail_sent);
        $this->db->where('water_invoice_status', 0);
        $this->db->where('water_invoice_due_date<', $date);

        $this->db->limit($limit);

        $query = $this->db->get();
        $rows = $query->result_array();
        return $rows;
    }



}