<!DOCTYPE html>
<base href="<?php echo base_url(); ?>">
<html>
<head>
    <link href="assets/css/bootstrap.min.css" rel="stylesheet">
    <link href="assets/css/style.css" rel="stylesheet">
    <style>
        body {
            background: none;
        }

        .colhead {

        }
    </style>

</head>
<body>
<div class="container">
    <?php
    if ($lease['lease_pay_type'] == 1) {
        $pay_type_duration = "Week";
        $lease_paying_type = 7;
    } elseif ($lease['lease_pay_type'] == 2) {
        $pay_type_duration = "Fortnight";
        $lease_paying_type = 14;
    } elseif ($lease['lease_pay_type'] == 3) {
        $pay_type_duration = "Four week";
        $lease_paying_type = 28;
    } else {
        $pay_type_duration = "Month";
        $lease_paying_type = 30;
    }
    ?>
    <div class="col-md-12">
        <div style="width:50%;float:left">
            <h3><?= $statement_title ?></h3>
        </div>
        <div style="width:50%;text-align:right;">

            <h3><img style="alignment:center;max-width: 200px;" src="<?= base_url() ?>assets/img/logo.png"></h3>
        </div>
    </div>
    <br>
    <div>
        <p>
            <span style="font-weight: bold">Landlord: </span><?= "{$property_owner['user_fname']} {$property_owner['user_lname']}" ?>
        </p>
        <p><span style="font-weight: bold">Property: </span><?= $full_property_address?>
        </p>
        <p><span style="font-weight: bold">Statement Date: </span><?= $statement_start_date; ?>
            to <?= $statement_end_date; ?> </p>
    </div>

    <br><br>
    <h4>Income</h4>
    <table class="table table-bordered table-hover" style="font-size: 15px;">
        <thead>
        <tr>
            <td style="font-size:large;font-weight:bold;background-color: #0bafd2;text-align:center;width:25%;">Type
            </td>
            <td style="font-size:large;font-weight:bold;background-color: #0bafd2;text-align:center;width:25%;">Issue
            </td>
            <td style="font-size:large;font-weight:bold;background-color: #0bafd2;text-align:center;width:25%;">Date
                Paid
            </td>
            <td style="font-size:large;font-weight:bold;background-color: #0bafd2;text-align:center;width:25%;">Amount
            </td>
        </tr>
        </thead>
        <tbody>
        <?php if (!empty($incomes)) { ?>
            <?php foreach ($incomes as $income) { ?>
                <tr>
                    <td style="text-align:center;width:25%;"><?= ucfirst(str_replace("_", " ", $income['type'])); ?></td>
                    <?php
                    $sub_type = "";
                    if ($income['type'] == "rent") {
                        if ($income['type'] != "nothing") {
                            $sub_type = $income['sub_type'];
                        }
                    } else {
                        if ($income['type'] != "nothing") {
                            $sub_type = ucfirst(str_replace("_", " ", $income['sub_type']));
                        }
                    }
                    ?>
                    <td style="text-align:center;width:25%;"><?= $sub_type ?></td>
                    <td style="text-align:center;width:25%;"><?= date('j F Y', strtotime($income['date'])); ?></td>
                    <td style="text-align:center;width:25%;">$<?= number_format($income['income'], 2, '.', ","); ?></td>
                </tr>
            <?php } ?>
        <?php } ?>
        <tr>
            <td colspan="4"></td>
        </tr>
        <tr>
            <td colspan="2"><span style="font-style: italic;font-weight: bold">Total</span></td>
            <td colspan="2"><span
                        style="font-style: italic;font-weight: bold">$<?= number_format($total_income, 2, '.', ","); ?></span>
            </td>
        </tr>
        </tbody>
    </table>

    <br><br>
    <h4>Expenses</h4>
    <table class="table table-bordered table-hover" style="font-size: 15px;">
        <thead>
        <tr>
            <td style="font-size:large;font-weight:bold;background-color: #0bafd2;text-align:center;width:25%;">Type
            </td>
            <td style="font-size:large;font-weight:bold;background-color: #0bafd2;text-align:center;width:25%;">Issue
            </td>
            <td style="font-size:large;font-weight:bold;background-color: #0bafd2;text-align:center;width:25%;">Date Paid
            </td>
            <td style="font-size:large;font-weight:bold;background-color: #0bafd2;text-align:center;width:25%;">Amount
            </td>
        </tr>
        </thead>
        <tbody>
        <?php if (!empty($expenses)) { ?>
            <?php foreach ($expenses as $expense) { ?>
                <tr>
                    <td style="text-align:center;width:25%;"><?= ucfirst(str_replace("_", " ", $expense['type'])); ?></td>
                    <td style="text-align:center;width:25%;"><?= $expense['sub_type'] != 'nothing' ? ucfirst(str_replace("_", " ", $expense['sub_type'])) : ''; ?></td>
                    <td style="text-align:center;width:25%;"><?= date('j F Y', strtotime($expense['date'])); ?></td>
                    <td style="text-align:center;width:25%;">
                        $<?= number_format($expense['expense'], 2, '.', ","); ?></td>
                </tr>
            <?php } ?>
        <?php } ?>
        <tr>
            <td colspan="4"></td>
        </tr>
        <tr>
            <td colspan="2"><span style="font-style: italic;font-weight: bold">Total</span></td>
            <td colspan="2"><span
                        style="font-style: italic;font-weight: bold">$<?= number_format($total_expense, 2, '.', ","); ?></span>
            </td>
        </tr>
        </tbody>
    </table>

    <br><br>
    <h3 style="font-style: italic">Balance:
        $<?= number_format($total_income, 2, '.', ","); ?> - $<?= number_format($total_expense, 2, '.', ","); ?>
        =
        $<?= number_format($total_income - $total_expense, 2, '.', ","); ?>
    </h3>

</div>
</body>
</html>