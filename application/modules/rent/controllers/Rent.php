<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Rent extends MX_Controller
{

    //public $counter=0;
    function __construct()
    {
        parent::__construct();


        $this->load->helper('html');
        $this->load->library('email');
        $this->load->library('session');

        $this->load->model('rent_model');
        $this->load->model('property/property_model');
        $this->load->model('water/water_model');
        $this->load->model('utility/utility_model');
        // $this->load->helper('inflector');
        // $this->load->library('encrypt');

        $this->utility_model->check_auth();

        ini_set('memory_limit', '128M');
        ini_set('max_execution_time', 900); //300 seconds = 5 minutes
    }

    public function index()
    {
        $this->load->view('rent_schedule');
    }

    public function rent_schedule($property_id, $active_update)
    {
        $data['lease_detail'] = $this->property_model->select_with_where('*', 'property_id=' . $property_id . ' AND lease_current_status=1', 'lease');

        $lease_id = !empty($data['lease_detail']) ? $data['lease_detail'][0]['lease_id'] : 0;
        //----------------------------------------------------------
        $is_archived = $this->utility_model->isArchived();
        $archived_string = $this->utility_model->getArchivedString();
        if ($is_archived) {
            $lease_id = $this->utility_model->getArchivedLeaseId();
            $data['lease_detail'][0] = $this->utility_model->getLease($lease_id);
        }

        $data['is_archived'] = $is_archived;
        $data['archived_string'] = $archived_string;
        //----------------------------------------------------------


        $get_property_lease_update = $this->property_model->select_with_where('*', 'property_id=' . $property_id . '', 'property');
        if ($get_property_lease_update[0]['lease_update_done'] == 1) {
            //do not know why this was written originally .Creating problem , so commented it
            // redirect('leaseDescription/' . $property_id);
        }

        $data['lease_info'] = $this->property_model->select_with_where('*', 'lease_id=' . $lease_id . ' AND property_id=' . $property_id . '', 'lease');

        $lease_paying_type_here = $data['lease_info'][0]['lease_pay_type'];
        if ($lease_paying_type_here == 1) {
            $lease_paying_type = 7;
        } elseif ($lease_paying_type_here == 2) {
            $lease_paying_type = 14;
        } elseif ($lease_paying_type_here == 3) {
            $lease_paying_type = 28;
        } else {
            $lease_paying_type = 30;
        }

        $data['get_tenant_list'] = $this->property_model->get_specific_selected_tenant($property_id, $lease_id);
        if ($active_update == 1) {


            // HERE THE CODE IS DELETING AND REWRITTING IT AGAIN

            $data['all_lease_info'] = $this->property_model->select_with_where('*', 'lease_id=' . $lease_id . '', 'lease');
            if ($data['all_lease_info'][0]['lease_pay_day'] == 01) {
                $day_name = 'Monday';
            }
            if ($data['all_lease_info'][0]['lease_pay_day'] == 02) {
                $day_name = 'Tuesday';
            }
            if ($data['all_lease_info'][0]['lease_pay_day'] == 03) {
                $day_name = 'Wednesday';
            }
            if ($data['all_lease_info'][0]['lease_pay_day'] == 04) {
                $day_name = 'Thursday';
            }
            if ($data['all_lease_info'][0]['lease_pay_day'] == 05) {
                $day_name = 'Friday';
            }
            if ($data['all_lease_info'][0]['lease_pay_day'] == 06) {
                $day_name = 'Saturday';
            }
            if ($data['all_lease_info'][0]['lease_pay_day'] == 07) {
                $day_name = 'Sunday';
            }

            $data['property_id'] = $property_id;
            $data['lease_id'] = $lease_id;
            $data['payment_start_date'] = $data['all_lease_info'][0]['lease_start_date'];
            $data['payment_end_date'] = $data['all_lease_info'][0]['lease_end_date'];
            $payment_amount = $data['all_lease_info'][0]['lease_per_period_payment'];
            $this->property_model->delete_function_cond('lease_payment_scedule', 'property_id=' . $property_id . ' AND lease_id=' . $lease_id . '');
            if ($data['all_lease_info'][0]['lease_pay_type'] == 1) {
                $data['property_id'] = $property_id;
                $data['lease_id'] = $lease_id;
                $data['payment_start_date'] = $data['all_lease_info'][0]['lease_start_date'];
                $data['payment_end_date'] = $data['all_lease_info'][0]['lease_end_date'];


                $data['paying_duration'] = '1 week';
                $data['paying_time'] = $day_name . ' of every week';

                $partial_amount = $payment_amount / 7;

                $nxt_start_date = array();
                for ($i = 0; $i < 7; $i++) {
                    $due_date = date('Y-m-d', strtotime("+" . $i . " day", strtotime($data['payment_start_date'])));
                    if (date('l', strtotime($due_date)) == $day_name && $due_date != $data['payment_start_date']) {
                        $nxt_start_date[] = array(
                            'three_start_date' => $data['payment_start_date'],
                            'three_end_date' => date('Y-m-d', strtotime("-1 day", strtotime($due_date))),
                            'payment_due_amount' => (((abs(strtotime($data['payment_start_date']) - strtotime(date('Y-m-d', strtotime("-1 day", strtotime($due_date))))) / 86400) + 1) * $partial_amount)
                        );
                    }
                }
                if ($nxt_start_date == null) {
                    $nxt_start_date[] = array(
                        'three_start_date' => $data['payment_start_date'],
                        'three_end_date' => date('Y-m-d', strtotime("+6 day", strtotime($data['payment_start_date']))),
                        'payment_due_amount' => $payment_amount
                    );
                }
                $period = new DatePeriod(
                    new DateTime(date('Y-m-d', strtotime("+1 day", strtotime($nxt_start_date[0]['three_end_date'])))),
                    new DateInterval('P1D'),
                    new DateTime($data['payment_end_date'])
                );
                $count = 1;
                foreach ($period as $key => $value) {
                    if ($key % 7 == 0) {
                        $nxt_start_date[$count] = array(
                            'three_start_date' => $value->format('Y-m-d'),
                            'three_end_date' => date('Y-m-d', strtotime("+6 day", strtotime($value->format('Y-m-d')))),
                            'payment_due_amount' => $payment_amount
                        );
                        $count++;
                    }
                }
                foreach ($nxt_start_date as $key => $row) {
                    if ($row['three_end_date'] > $data['payment_end_date']) {
                        $nxt_start_date[count($nxt_start_date) - 1]['three_end_date'] = $data['payment_end_date'];
                        $nxt_start_date[count($nxt_start_date) - 1]['payment_due_amount'] = (((abs(strtotime($nxt_start_date[count($nxt_start_date) - 1]['three_start_date']) - strtotime($data['payment_end_date'])) / 86400) + 1) * $partial_amount);
                    }
                }
                if ($nxt_start_date[count($nxt_start_date) - 1]['three_end_date'] < $data['payment_end_date']) {
                    $nxt_start_date[count($nxt_start_date)] = array(
                        'three_start_date' => date('Y-m-d', strtotime("+1 day", strtotime($value->format('Y-m-d')))),
                        'three_end_date' => $data['payment_end_date'],
                        'payment_due_amount' => (((abs(strtotime(date('Y-m-d', strtotime("+1 day", strtotime($value->format('Y-m-d'))))) - strtotime(date('Y-m-d', strtotime($data['payment_end_date'])))) / 86400) + 1) * $partial_amount)
                    );
                }
                foreach ($nxt_start_date as $key => $value) {
                    $payment['property_id'] = $property_id;
                    $payment['lease_id'] = $lease_id;
                    $payment['payment_due_date'] = $value['three_start_date'];
                    $payment['payment_start_period'] = $value['three_start_date'];
                    $payment['payment_end_period'] = $value['three_end_date'];
                    $payment['payment_due_amount'] = $value['payment_due_amount'];
                    // if ($flow == 'front') {
                    $this->property_model->insert('lease_payment_scedule', $payment);
                    // }
                }
            } elseif ($data['all_lease_info'][0]['lease_pay_type'] == 2) {
                $data['property_id'] = $property_id;
                $data['lease_id'] = $lease_id;
                $data['payment_start_date'] = $data['all_lease_info'][0]['lease_start_date'];
                $data['payment_end_date'] = $data['all_lease_info'][0]['lease_end_date'];

                $data['paying_duration'] = '2 weeks';
                $data['paying_time'] = $day_name . ' of every alternative week';
                $partial_amount = $payment_amount / 14;

                $nxt_start_date = array();
                for ($i = 0; $i < 7; $i++) {
                    $due_date = date('Y-m-d', strtotime("+" . $i . " day", strtotime($data['payment_start_date'])));
                    if (date('l', strtotime($due_date)) == $day_name && $due_date != $data['payment_start_date']) {
                        $nxt_start_date[] = array(
                            'three_start_date' => $data['payment_start_date'],
                            'three_end_date' => date('Y-m-d', strtotime("-1 day", strtotime($due_date))),
                            'payment_due_amount' => (((abs(strtotime($data['payment_start_date']) - strtotime(date('Y-m-d', strtotime("-1 day", strtotime($due_date))))) / 86400) + 1) * $partial_amount)
                        );
                    }
                }
                if ($nxt_start_date == null) {
                    $nxt_start_date[] = array(
                        'three_start_date' => $data['payment_start_date'],
                        'three_end_date' => date('Y-m-d', strtotime("+13 day", strtotime($data['payment_start_date']))),
                        'payment_due_amount' => $payment_amount
                    );
                }
                $period = new DatePeriod(
                    new DateTime(date('Y-m-d', strtotime("+1 day", strtotime($nxt_start_date[0]['three_end_date'])))),
                    new DateInterval('P1D'),
                    new DateTime($data['payment_end_date'])
                );
                $count = 1;
                foreach ($period as $key => $value) {
                    if ($key % 14 == 0) {
                        $nxt_start_date[$count] = array(
                            'three_start_date' => $value->format('Y-m-d'),
                            'three_end_date' => date('Y-m-d', strtotime("+13 day", strtotime($value->format('Y-m-d')))),
                            'payment_due_amount' => $payment_amount
                        );
                        $count++;
                    }
                }
                foreach ($nxt_start_date as $key => $row) {
                    if ($row['three_end_date'] > $data['payment_end_date']) {
                        $nxt_start_date[count($nxt_start_date) - 1]['three_end_date'] = $data['payment_end_date'];
                        $nxt_start_date[count($nxt_start_date) - 1]['payment_due_amount'] = (((abs(strtotime($nxt_start_date[count($nxt_start_date) - 1]['three_start_date']) - strtotime($data['payment_end_date'])) / 86400) + 1) * $partial_amount);
                    }
                }
                if ($nxt_start_date[count($nxt_start_date) - 1]['three_end_date'] < $data['payment_end_date']) {
                    $nxt_start_date[count($nxt_start_date)] = array(
                        'three_start_date' => date('Y-m-d', strtotime("+1 day", strtotime($value->format('Y-m-d')))),
                        'three_end_date' => $nxt_start_date[count($nxt_start_date)]['three_end_date'] = $data['payment_end_date'],
                        'payment_due_amount' => (((abs(strtotime(date('Y-m-d', strtotime("+1 day", strtotime($value->format('Y-m-d'))))) - strtotime(date('Y-m-d', strtotime($data['payment_end_date'])))) / 86400) + 1) * $partial_amount)
                    );
                }
                foreach ($nxt_start_date as $key => $value) {
                    $payment['property_id'] = $property_id;
                    $payment['lease_id'] = $lease_id;
                    $payment['payment_due_date'] = $value['three_start_date'];
                    $payment['payment_start_period'] = $value['three_start_date'];
                    $payment['payment_end_period'] = $value['three_end_date'];
                    $payment['payment_due_amount'] = $value['payment_due_amount'];

                    // if ($flow == 'front') {
                    $this->property_model->insert('lease_payment_scedule', $payment);
                    // }

                }
            } elseif ($data['all_lease_info'][0]['lease_pay_type'] == 3) {
                $data['property_id'] = $property_id;
                $data['lease_id'] = $lease_id;
                $data['payment_start_date'] = $data['all_lease_info'][0]['lease_start_date'];
                $data['payment_end_date'] = $data['all_lease_info'][0]['lease_end_date'];

                $data['paying_duration'] = '4 weeks';
                $data['paying_time'] = 'a ' . $day_name;
                $partial_amount = $payment_amount / 28;

                $nxt_start_date = array();
                for ($i = 0; $i < 7; $i++) {
                    $due_date = date('Y-m-d', strtotime("+" . $i . " day", strtotime($data['payment_start_date'])));
                    if (date('l', strtotime($due_date)) == $day_name && $due_date != $data['payment_start_date']) {
                        $nxt_start_date[] = array(
                            'three_start_date' => $data['payment_start_date'],
                            'three_end_date' => date('Y-m-d', strtotime("-1 day", strtotime($due_date))),
                            'payment_due_amount' => (((abs(strtotime($data['payment_start_date']) - strtotime(date('Y-m-d', strtotime("-1 day", strtotime($due_date))))) / 86400) + 1) * $partial_amount)
                        );
                    }
                }
                if ($nxt_start_date == null) {
                    $nxt_start_date[] = array(
                        'three_start_date' => $data['payment_start_date'],
                        'three_end_date' => date('Y-m-d', strtotime("+27 day", strtotime($data['payment_start_date']))),
                        'payment_due_amount' => $payment_amount
                    );
                }

                $period = new DatePeriod(
                    new DateTime(date('Y-m-d', strtotime("+1 day", strtotime($nxt_start_date[0]['three_end_date'])))),
                    new DateInterval('P1D'),
                    new DateTime($data['payment_end_date'])
                );
                $count = 1;
                foreach ($period as $key => $value) {
                    if ($key % 28 == 0) {
                        $nxt_start_date[$count] = array(
                            'three_start_date' => $value->format('Y-m-d'),
                            'three_end_date' => date('Y-m-d', strtotime("+27 day", strtotime($value->format('Y-m-d')))),
                            'payment_due_amount' => $payment_amount
                        );
                        $count++;
                    }
                }
                foreach ($nxt_start_date as $key => $row) {
                    if ($row['three_end_date'] > $data['payment_end_date']) {
                        $nxt_start_date[count($nxt_start_date) - 1]['three_end_date'] = $data['payment_end_date'];
                        $nxt_start_date[count($nxt_start_date) - 1]['payment_due_amount'] = (((abs(strtotime($nxt_start_date[count($nxt_start_date) - 1]['three_start_date']) - strtotime($data['payment_end_date'])) / 86400) + 1) * $partial_amount);
                    }
                }
                if ($nxt_start_date[count($nxt_start_date) - 1]['three_end_date'] < $data['payment_end_date']) {
                    $nxt_start_date[count($nxt_start_date)] = array(
                        'three_start_date' => date('Y-m-d', strtotime("+1 day", strtotime($value->format('Y-m-d')))),
                        'three_end_date' => $nxt_start_date[count($nxt_start_date)]['three_end_date'] = $data['payment_end_date'],
                        'payment_due_amount' => (((abs(strtotime(date('Y-m-d', strtotime("+1 day", strtotime($value->format('Y-m-d'))))) - strtotime(date('Y-m-d', strtotime($data['payment_end_date'])))) / 86400) + 1) * $partial_amount)
                    );
                }
                foreach ($nxt_start_date as $key => $value) {
                    $payment['property_id'] = $property_id;
                    $payment['lease_id'] = $lease_id;
                    $payment['payment_due_date'] = $value['three_start_date'];
                    $payment['payment_start_period'] = $value['three_start_date'];
                    $payment['payment_end_period'] = $value['three_end_date'];
                    $payment['payment_due_amount'] = $value['payment_due_amount'];
                    // if ($flow == 'front') {
                    $this->property_model->insert('lease_payment_scedule', $payment);
                    // }
                }
                // echo "<pre>";
                // print_r($nxt_start_date);
                // die();
            } else {
                $data['property_id'] = $property_id;
                $data['lease_id'] = $lease_id;
                $data['payment_start_date'] = $data['all_lease_info'][0]['lease_start_date'];
                $data['payment_end_date'] = $data['all_lease_info'][0]['lease_end_date'];

                $data['paying_duration'] = '1 month';
                $data['paying_time'] = $data['all_lease_info'][0]['lease_pay_day'] . 'th of the month';
                $partial_amount = $payment_amount / 30;

                $period = new DatePeriod(
                    new DateTime($data['payment_start_date']),
                    new DateInterval('P1D'),
                    new DateTime($data['payment_end_date'])
                );
                $pay_date = $data['all_lease_info'][0]['lease_pay_day'];
                // print_r($pay_date);die();
                $all_dates = array();
                foreach ($period as $key => $value) {
                    if ($key == 0) {
                        if (date('Y-m-d', strtotime("+30 day", strtotime($value->format('Y-m-d')))) > $value->format('Y-m-' . $pay_date . '')) {
                            if ($value->format('Y-m-d') != $value->format('Y-m-' . $pay_date . '')) {
                                $first_end_date = date('Y-m-d', (strtotime('-1 day', strtotime($value->format('Y-m-' . $pay_date . '')))));
                            } elseif ($value->format('Y-m-d') == $value->format('Y-m-' . $pay_date . '')) {
                                $first_end_date = $first_end_date = date('Y-m-d', strtotime('-1 day', strtotime("+1 month", strtotime($value->format('Y-m-d')))));
                            } else {
                                $first_end_date = date('Y-m-d', strtotime('-1 day', strtotime("+1 month", strtotime($value->format('Y-m-d')))));
                            }
                        } else {
                            $first_end_date = date('Y-m-d', strtotime('-1 day', strtotime("+1 month", strtotime($value->format('Y-m-d')))));
                        }
                        $all_dates[] = array(
                            'start_date' => $value->format('Y-m-d'),
                            'end_date' => $first_end_date,
                            'payment_due_amount' => (((abs(strtotime($value->format('Y-m-d')) - strtotime($first_end_date)) / 86400) + 1) * $partial_amount)
                        );
                    }
                    if ($key > 0) {
                        if ($value->format('d') == $pay_date) {
                            $all_dates[] = array(
                                'start_date' => $value->format('Y-m-d'),
                                'end_date' => date('Y-m-d', strtotime('-1 day', strtotime("+1 month", strtotime($value->format('Y-m-d'))))),
                                'payment_due_amount' => $payment_amount
                            );
                        }
                    }
                }
                foreach ($all_dates as $row) {
                    if ($row['end_date'] > $data['payment_end_date']) {
                        $row['end_date'] = $data['payment_end_date'];
                        $row['payment_due_amount'] = (((abs(strtotime($row['start_date']) - strtotime($data['payment_end_date'])) / 86400) + 1) * $partial_amount);
                    }
                    $payment['property_id'] = $property_id;
                    $payment['lease_id'] = $lease_id;
                    $payment['payment_due_date'] = $row['start_date'];
                    $payment['payment_start_period'] = $row['start_date'];
                    $payment['payment_end_period'] = $row['end_date'];
                    $payment['payment_due_amount'] = $row['payment_due_amount'];
                    // if ($flow == 'front') {
                    $this->property_model->insert('lease_payment_scedule', $payment);
                    // }
                }
            }

            // HERE THE CODE IS RESCHEDULING AGAIN


            $data['all_payment_info'] = $this->property_model->select_condition_order('lease_payment_scedule', 'lease_id=' . $lease_id . ' AND property_id=' . $property_id . '', 'lease_payment_scedule.payment_schedule_id', 'DESC');
            $data['get_tenant_list'] = $this->property_model->get_specific_selected_tenant($property_id, $lease_id);


            foreach ($data['get_tenant_list'] as $key => $get_value) {
                $this->property_model->delete_function_cond('lease_tenant_payment_scedhule', 'tenant_id=' . $get_value['tenant_id'] . ' AND lease_id = ' . $get_value['lease_id'] . ' AND property_id = ' . $get_value['property_id'] . '');
            }

            $one_user_amount = 0;
            $one_user_amount_upcome = 0;
            foreach ($data['get_tenant_list'] as $key => $get_value) {
                foreach ($data['all_payment_info'] as $key => $value) {
                    $value['lease_rent_recieved'] = 0;
                }
                if ($get_value['payment_update_status'] == 1) {
                    $one_user_amount = ($one_user_amount + ($get_value['share_paid_amount'] / $lease_paying_type) * ((abs(strtotime($data['all_payment_info'][$key]['payment_start_period']) - strtotime($data['all_payment_info'][$key]['payment_end_period'])) / 86400) + 1));
                }
            }
            foreach ($data['get_tenant_list'] as $key => $get_value) {
                foreach ($data['all_payment_info'] as $key => $value) {
                    if ($value['payment_start_period'] <= date('Y-m-d')) {
                        $data['all_payment_info'][$key]['lease_rent_recieved'] = $data['all_payment_info'][$key]['payment_due_amount'];
                        $data['all_payment_info'][$key]['lease_present_status'] = 1;
                        if ($get_value['payment_update_status'] == 1) {
                            $t_data['tenant_id'] = $get_value['tenant_id'];
                            $t_data['lease_id'] = $get_value['lease_id'];
                            $t_data['property_id'] = $get_value['property_id'];
                            $t_data['schedule_start_date'] = $value['payment_start_period'];
                            $t_data['schedule_end_date'] = $value['payment_end_period'];
                            $t_data['shared_amount'] = (($get_value['share_paid_amount'] / $lease_paying_type) * ((abs(strtotime($data['all_payment_info'][$key]['payment_start_period']) - strtotime($data['all_payment_info'][$key]['payment_end_period'])) / 86400) + 1));
                            $t_data['payment_status'] = 1;
                            $t_data['payment_method'] = $get_value['payment_method'];
                            $t_data['payment_for'] = 1;
                            $this->property_model->insert('lease_tenant_payment_scedhule', $t_data);
                        }
                    }
                    if ($value['payment_start_period'] > date('Y-m-d')) {
                        $data['all_payment_info'][$key]['lease_rent_recieved'] = 0;
                        $data['all_payment_info'][$key]['lease_present_status'] = 2;
                    }
                }
            }
            foreach ($data['get_tenant_list'] as $key => $get_value) {
                if ($get_value['payment_update_status'] == 2) {
                    foreach ($data['all_payment_info'] as $key => $value) {
                        if ($value['payment_start_period'] <= date('Y-m-d')) {
                            $get_value['share_paid_amount'] = (($get_value['share_paid_amount'] / $lease_paying_type) * ((abs(strtotime($data['all_payment_info'][$key]['payment_start_period']) - strtotime($data['all_payment_info'][$key]['payment_end_period'])) / 86400) + 1));
                            if ($get_value['payment_update_by'] > 0) {
                                if ($get_value['payment_update_by'] > $get_value['share_paid_amount']) {
                                    $data['all_payment_info'][$key]['lease_rent_recieved'] = $data['all_payment_info'][$key]['lease_rent_recieved'] - $get_value['share_paid_amount'];
                                    $data['all_payment_info'][$key]['lease_present_status'] = 3;
                                } else {
                                    $data['all_payment_info'][$key]['lease_rent_recieved'] = $data['all_payment_info'][$key]['lease_rent_recieved'] - $get_value['payment_update_by'];
                                    $data['all_payment_info'][$key]['lease_present_status'] = 3;
                                }
                            }
                            if ($get_value['payment_update_status'] == 2) {
                                $t_data['tenant_id'] = $get_value['tenant_id'];
                                $t_data['lease_id'] = $get_value['lease_id'];
                                $t_data['property_id'] = $get_value['property_id'];
                                $t_data['schedule_start_date'] = $value['payment_start_period'];
                                $t_data['schedule_end_date'] = $value['payment_end_period'];
                                $t_data['shared_amount'] = (($get_value['share_paid_amount'] / $lease_paying_type) * ((abs(strtotime($data['all_payment_info'][$key]['payment_start_period']) - strtotime($data['all_payment_info'][$key]['payment_end_period'])) / 86400) + 1));
                                $t_data['payment_status'] = 2;
                                $t_data['payment_method'] = $get_value['payment_method'];
                                $t_data['payment_for'] = 1;
                                $this->property_model->insert('lease_tenant_payment_scedhule', $t_data);
                            }

                            $get_value['payment_update_by'] = $get_value['payment_update_by'] - $get_value['share_paid_amount'];
                        }
                        // if ($value['payment_start_period']>date('Y-m-d'))
                        // {
                        //     $data['all_payment_info'][$key]['lease_rent_recieved'] = 0;
                        //     $data['all_payment_info'][$key]['lease_present_status'] = 2;
                        // }
                    }
                }
            }
            foreach ($data['get_tenant_list'] as $get_value) {
                if ($get_value['payment_update_status'] == 4) {
                    $start_here = 1;
                    foreach ($data['all_payment_info'] as $key => $value) {
                        if ($value['payment_start_period'] <= date('Y-m-d')) {
                            $get_value['share_paid_amount'] = (($get_value['share_paid_amount'] / $lease_paying_type) * ((abs(strtotime($data['all_payment_info'][$key]['payment_start_period']) - strtotime($data['all_payment_info'][$key]['payment_end_period'])) / 86400) + 1));
                            if ($get_value['payment_update_by'] > 0) {
                                $data['all_payment_info'][$key]['lease_rent_recieved'] = $data['all_payment_info'][$key]['lease_rent_recieved'] - $get_value['share_paid_amount'];
                                $data['all_payment_info'][$key]['lease_present_status'] = 3;
                            }
                            // if($key==1)
                            {
                                if ($get_value['payment_update_status'] == 4) {
                                    if ($start_here == 1) {
                                        $t_data['tenant_id'] = $get_value['tenant_id'];
                                        $t_data['lease_id'] = $get_value['lease_id'];
                                        $t_data['property_id'] = $get_value['property_id'];
                                        $t_data['schedule_start_date'] = $value['payment_start_period'];
                                        $t_data['schedule_end_date'] = $value['payment_end_period'];
                                        $t_data['shared_amount'] = $get_value['share_paid_amount'];
                                        $t_data['payment_status'] = 4;
                                        $t_data['payment_method'] = $get_value['payment_method'];
                                        $t_data['payment_for'] = 1;
                                        $this->property_model->insert('lease_tenant_payment_scedhule', $t_data);
                                    }
                                }
                                $start_here++;
                            }
                            $get_value['payment_update_by'] = $get_value['payment_update_by'] - $get_value['share_paid_amount'];
                        }
                        if ($value['payment_start_period'] > date('Y-m-d')) {
                            $data['all_payment_info'][$key]['lease_rent_recieved'] = 0;
                            $data['all_payment_info'][$key]['lease_present_status'] = 2;
                        }
                    }
                }
            }

            foreach ($data['all_payment_info'] as $key => $value) {
                $payment_leases['lease_rent_recieved'] = $value['lease_rent_recieved'];
                $payment_leases['lease_present_status'] = $value['lease_present_status'];
                // print_r($data['all_payment_info']);
                $this->property_model->update_function('payment_schedule_id', $value['payment_schedule_id'], 'lease_payment_scedule', $payment_leases);
            }
            $data['all_payment_infos'] = $this->property_model->select_condition_order('lease_payment_scedule', 'lease_id=' . $lease_id . ' AND property_id=' . $property_id . '', 'lease_payment_scedule.payment_schedule_id', 'ASC');
            foreach ($data['get_tenant_list'] as $key => $get_value) {
                if ($get_value['payment_update_status'] == 3 || $get_value['payment_update_status'] == 2) {
                    foreach ($data['all_payment_infos'] as $key => $value) {
                        if ($value['payment_start_period'] > date('Y-m-d')) {
                            $data['all_payment_infos'][$key]['lease_rent_recieved'] = 0;
                        }
                    }
                }
            }
            foreach ($data['get_tenant_list'] as $key => $get_value) {
                if ($get_value['payment_update_status'] == 3) {
                    foreach ($data['all_payment_infos'] as $key => $value) {
                        if ($value['payment_start_period'] > date('Y-m-d')) {
                            $get_value['share_paid_amount'] = (($get_value['share_paid_amount'] / $lease_paying_type) * ((abs(strtotime($data['all_payment_infos'][$key]['payment_start_period']) - strtotime($data['all_payment_infos'][$key]['payment_end_period'])) / 86400) + 1));
                            // $get_value['payment_update_by'] = (($get_value['payment_update_by']/$lease_paying_type)*((abs(strtotime($data['all_payment_infos'][$key]['payment_start_period'])-strtotime($data['all_payment_infos'][$key]['payment_end_period']))/86400)+1));
                            if ($get_value['payment_update_by'] > 0) {
                                if ($get_value['payment_update_by'] > $get_value['share_paid_amount']) {
                                    $data['all_payment_infos'][$key]['lease_rent_recieved'] = $data['all_payment_infos'][$key]['lease_rent_recieved'] + $get_value['share_paid_amount'];
                                    if ($data['all_payment_infos'][$key]['lease_rent_recieved'] == $data['all_payment_infos'][$key]['payment_due_amount']) {
                                        $data['all_payment_infos'][$key]['lease_present_status'] = 1;
                                    } else {
                                        $data['all_payment_infos'][$key]['lease_present_status'] = 2;
                                    }
                                } else {
                                    $data['all_payment_infos'][$key]['lease_rent_recieved'] = $data['all_payment_infos'][$key]['lease_rent_recieved'] + $get_value['payment_update_by'];
                                    if ($data['all_payment_infos'][$key]['lease_rent_recieved'] == $data['all_payment_infos'][$key]['payment_due_amount']) {
                                        $data['all_payment_infos'][$key]['lease_present_status'] = 1;
                                    } else {
                                        $data['all_payment_infos'][$key]['lease_present_status'] = 2;
                                    }
                                }
                                if ($get_value['payment_update_status'] == 3) {
                                    $t_data['tenant_id'] = $get_value['tenant_id'];
                                    $t_data['lease_id'] = $get_value['lease_id'];
                                    $t_data['property_id'] = $get_value['property_id'];
                                    $t_data['schedule_start_date'] = $value['payment_start_period'];
                                    $t_data['schedule_end_date'] = $value['payment_end_period'];
                                    $t_data['shared_amount'] = $data['all_payment_infos'][$key]['lease_rent_recieved'];
                                    $t_data['payment_status'] = 3;
                                    $t_data['payment_method'] = $get_value['payment_method'];
                                    $t_data['payment_for'] = 1;
                                    $this->property_model->insert('lease_tenant_payment_scedhule', $t_data);
                                }
                            }
                            $get_value['payment_update_by'] = $get_value['payment_update_by'] - $get_value['share_paid_amount'];
                        }
                        if ($get_value['payment_update_by'] < 0) {
                            $data['all_payment_infos'][$key]['lease_rent_recieved'] = $data['all_payment_infos'][$key]['lease_rent_recieved'];
                            $data['all_payment_infos'][$key]['lease_present_status'] = 2;
                        }
                    }
                }
            }
            foreach ($data['all_payment_infos'] as $key => $values) {
                $payments['lease_rent_recieved'] = $values['lease_rent_recieved'];
                $payments['lease_present_status'] = $values['lease_present_status'];
                $this->property_model->update_function('payment_schedule_id', $values['payment_schedule_id'], 'lease_payment_scedule', $payments);
            }

        }
        if ($active_update == 3) {
            $data['modal_open'] = 1;
        } else {
            $data['modal_open'] = 0;
        }
        if ($active_update == 4) {
            $data['rent_recieve_show'] = 1;
        } else {
            $data['rent_recieve_show'] = 0;
        }
        if ($active_update == 2) {
            $data['tenant_ledger_modal'] = 1;
        } else {
            $data['tenant_ledger_modal'] = 0;
        }
        $data['all_payments_info'] = $this->property_model->select_condition_order('lease_payment_scedule', 'lease_id=' . $lease_id . ' AND property_id=' . $property_id . '', 'lease_payment_scedule.payment_schedule_id', 'DESC');
        //$data['all_tenant_payments_info'] = $this->property_model->select_condition_order_and_join($lease_id, $property_id);
        $data['all_tenant_payments_info'] = null;

        $data['property_id'] = $property_id;
        $data['property_info'] = $this->rent_model->select_with_where('*', 'property_id=' . $property_id . '', 'property');
        $data['lease_id'] = $lease_id;

        //$data['step_count'] = 7;
        //$this->property_model->update_function('property_id', $property_id,'property',$data);


        $data['lease_log_payment_lease_list'] = $this->rent_model->getLeaseLogPaymentList($lease_id);
        $data['water_invoices_with_payment_logs'] = $this->water_model->getWaterInvoicesWithPaymentLogs($lease_id);

        $minimum_date_of_income_expense = $this->utility_model->getMinimumDateOfIncomeExpense($property_id, $lease_id);

        $curr_date = date('Y-m-d');
        $data['minimum_date_of_income_expense'] = $minimum_date_of_income_expense;
        $data['statement_month_list'] = $this->generate_month_list($minimum_date_of_income_expense, $curr_date);


        $this->load->view('rent_schedule', $data);
    }

    public function generate_month_list($start_date, $end_date)
    {
        $start = new DateTime($start_date);
        $start->modify('first day of this month');
        $end = new DateTime($end_date);
        $end->modify('first day of next month');
        $interval = DateInterval::createFromDateString('1 month');
        $period = new DatePeriod($start, $interval, $end);

        $month_list = array();
        foreach ($period as $dt) {
            $month_list[] = $dt->format("Y-m");
        }

        return $month_list;
    }


    public function rent_schedule_pdf()
    {
        $property_id = $this->input->post('property_id');
        $tenant_rent_schedule = $this->input->post('tenant_rent_schedule');
        $data['rent_payment_status'] = $this->input->post('rent_payment_status');
        $data['property_info'] = $this->rent_model->select_with_where('*', 'property_id=' . $property_id . '', 'property');

        $data['state_name'] = '';
        if ($data['property_info'][0]['state'] == 246) {
            $data['state_name'] = 'Australian Capital Territory';
        } elseif ($data['property_info'][0]['state'] == 266) {
            $data['state_name'] = 'New South Wales';
        } elseif ($data['property_info'][0]['state'] == 267) {
            $data['state_name'] = 'Northern Territory';
        } elseif ($data['property_info'][0]['state'] == 269) {
            $data['state_name'] = 'Queensland';
        } elseif ($data['property_info'][0]['state'] == 270) {
            $data['state_name'] = 'South Australia';
        } elseif ($data['property_info'][0]['state'] == 271) {
            $data['state_name'] = 'Tasmania';
        } elseif ($data['property_info'][0]['state'] == 273) {
            $data['state_name'] = 'Victoria';
        } elseif ($data['property_info'][0]['state'] == 275) {
            $data['state_name'] = 'Western Australia';
        }
        $data['lease_detail'] = $this->property_model->select_with_where('*', 'property_id=' . $property_id . ' AND lease_current_status=1', 'lease');

        $full_property_address = $this->utility_model->getFullPropertyAddress($property_id);
        $data['full_property_address'] = $full_property_address;

        $lease_id = !empty($data['lease_detail']) ? $data['lease_detail'][0]['lease_id'] : 0;
        //----------------------------------------------------------
        $is_archived = $this->utility_model->isArchived();
        $archived_string = $this->utility_model->getArchivedString();

        if ($is_archived) {
            $lease_id = $this->utility_model->getArchivedLeaseId();
            $data['lease_detail'][0] = $this->utility_model->getLease($lease_id);
        }

        $data['is_archived'] = $is_archived;
        $data['archived_string'] = $archived_string;
        //----------------------------------------------------------

        $data['tenant_info'] = $this->rent_model->get_tenant_info($property_id, $lease_id, $tenant_rent_schedule);
        $total_tenant_amount = 0;
        foreach ($data['tenant_info'] as $key => $value) {
            $total_tenant_amount = $total_tenant_amount + $value['share_paid_amount'];
        }
        $data['total_tenant_amount'] = $total_tenant_amount;

        $user_id = $this->session->userdata('user_id');
        $data['user_info'] = $this->rent_model->select_with_where('*', 'user_id=' . $user_id . '', 'user');
        $data['all_payments_info'] = $this->property_model->select_condition_order('lease_payment_scedule', 'lease_id=' . $lease_id . ' AND property_id=' . $property_id . '', 'lease_payment_scedule.payment_schedule_id', 'ASC');

        $this->load->library('MPDF/mpdf');

        $mpdf = new mPDF('win-1252', 'A4', '', '', 5, 5, 16, 60, 5, 5, 'C');
        $mpdf->useOnlyCoreFonts = true;    // false is default
        $mpdf->SetProtection(array('print'));
        $mpdf->SetTitle('Rent Schedule');
        $mpdf->SetAuthor($data['author']);
        $mpdf->SetDisplayMode('fullpage');

        $mpdf->debug = true;

        /*do not load header,common left footer as this view contains the whole page*/
        $html = $this->load->view('rent_schedule_view_yes', $data, TRUE);

        $name = 'members' . rand(10000, 99999) . '.pdf';

        //echo  $html;die();
        $mpdf->WriteHTML($html);
        $mpdf->Output($name, 'I');

        exit;

    }

    public function tenant_ledgers_pdf()
    {
        $property_id = $this->input->post('property_id');
        // print_r($property_id);die();
        $tenant_ledger = $this->input->post('tenant_ledger');

        $tenant_ledger_start_date = "";
        $tenant_ledger_end_date = "";

        $start_date = $this->input->post('tenant_ledger_start_date');
        $end_date = $this->input->post('tenant_ledger_end_date');

        if ($start_date != "") {
            $tenant_ledger_start_date = date('Y-m-d', strtotime($start_date));
        }
        if ($end_date) {
            $tenant_ledger_end_date = date('Y-m-d', strtotime($end_date));
        }

        $data['tenant_ledger_start_date'] = date('M d,Y', strtotime($tenant_ledger_start_date));
        $data['tenant_ledger_end_date'] = date('M d,Y', strtotime($tenant_ledger_end_date));

        // print_r($tenant_ledger_start_date.' '.$tenant_ledger_end_date);
        $data['property_info'] = $this->rent_model->select_with_where('*', 'property_id=' . $property_id . '', 'property');
        $data['lease_detail'] = $this->property_model->select_with_where('*', 'property_id=' . $property_id . ' AND lease_current_status=1', 'lease');
        $lease_id = $data['lease_detail'][0]['lease_id'];

        $data['tenant_info'] = $this->rent_model->get_tenant_info($property_id, $data['lease_detail'][0]['lease_id'], $tenant_ledger);
        $total_tenant_amount = 0;
        foreach ($data['tenant_info'] as $key => $value) {
            $total_tenant_amount = $total_tenant_amount + $value['share_paid_amount'];
        }
        $data['total_tenant_amount'] = $total_tenant_amount;
        $user_id = $this->session->userdata('user_id');
        $data['user_info'] = $this->rent_model->select_with_where('*', 'user_id=' . $user_id . '', 'user');

        $data['all_ledger_info'] = $this->property_model->select_condition_order_and_join_with_where($data['lease_detail'][0]['lease_id'], $property_id, $tenant_ledger_start_date, $tenant_ledger_end_date);

        $data['lease_log_payment_lease_list'] = $this->rent_model->getLeaseLogPaymentList($lease_id);
        // echo "<pre>";
        // print_r($data['all_ledger_info']);
        // die();

        $this->load->library('MPDF/mpdf');

        $mpdf = new mPDF('win-1252', 'A4', '', '', 5, 5, 16, 60, 5, 5, 'C');
        $mpdf->useOnlyCoreFonts = true;    // false is default
        $mpdf->SetProtection(array('print'));
        $mpdf->SetTitle('Tenant Ledger');
        $mpdf->SetAuthor($data['author']);
        $mpdf->SetDisplayMode('fullpage');

        $mpdf->debug = true;


        /*do not load header,common left footer as this view contains the whole page*/
        $html = $this->load->view('tenant_ledger_view_pdf', $data, TRUE);

        //echo $html;die();

        $name = 'members' . rand(10000, 99999) . '.pdf';

        //echo  $link;die();
        $mpdf->WriteHTML($html);
        $mpdf->Output($name, 'I');

        exit;
    }

    public function insert_rent_payment_update()
    {
        $email = $this->input->post('email');
        $user_fname = $this->input->post('user_fname');
        $user_lname = $this->input->post('user_lname');
        $property_id = $this->input->post('property_id');


        $property_info = $this->rent_model->select_where_left_join('*', 'property', 'user', 'user.user_id=property.property_id', 'property.property_id=' . $property_id . '');

        $lease_id = $this->input->post('lease_id');
        $all_lease_data = $this->property_model->select_with_where('*', 'lease_id=' . $lease_id . ' AND property_id=' . $property_id . ' AND payment_status = 1', 'lease_detail');
        $amount_recieved = $this->input->post('amount_recieved');
        $payment_method = $this->input->post('payment_method');
        $payment_date = $this->input->post('payment_date');
        $lease_detail_id = $this->input->post('lease_detail_id');
        $payment_sms_check = $this->input->post('payment_sms_check');

        for ($i = 0; $i < count($amount_recieved); $i++) {
            $data['amount_recieved'] = $amount_recieved[$i];
            $data['payment_method'] = $payment_method[$i];
            $data['payment_date'] = $payment_date[$i];
            $data['lease_detail_id'] = $lease_detail_id[$i];
            $status = '';
            if ($data['lease_detail_id'] == $all_lease_data[$i]['lease_detail_id']) {
                if ($all_lease_data[$i]['payment_update_status'] == 1) {
                    if ($data['amount_recieved'] == $all_lease_data[$i]['payment_update_by']) {
                        $new_data['payment_update_status'] = 1;
                        $status = 'Up to date';
                        $new_data['payment_update_by'] = 0;
                    } elseif ($data['amount_recieved'] > $all_lease_data[$i]['payment_update_by']) {
                        $new_data['payment_update_status'] = 3;
                        $status = 'Ahead';
                        $new_data['payment_update_by'] = $data['amount_recieved'];
                    }
                } elseif ($all_lease_data[$i]['payment_update_status'] == 2 || $all_lease_data[$i]['payment_update_status'] == 4) {
                    if ($data['amount_recieved'] == $all_lease_data[$i]['payment_update_by']) {
                        $new_data['payment_update_status'] = 1;
                        $status = 'Up to date';
                        $new_data['payment_update_by'] = 0;
                    } elseif ($data['amount_recieved'] > $all_lease_data[$i]['payment_update_by']) {
                        $new_data['payment_update_status'] = 3;
                        $status = 'Ahead';
                        $new_data['payment_update_by'] = $data['amount_recieved'] - $all_lease_data[$i]['payment_update_by'];
                    } elseif ($data['amount_recieved'] < $all_lease_data[$i]['payment_update_by']) {
                        $new_data['payment_update_status'] = 2;
                        $status = 'Arrears';
                        $new_data['payment_update_by'] = $all_lease_data[$i]['payment_update_by'] - $data['amount_recieved'];
                    }
                } elseif ($all_lease_data[$i]['payment_update_status'] == 3) {
                    $new_data['payment_update_status'] = 3;
                    $status = 'Ahead';
                    $new_data['payment_update_by'] = $all_lease_data[$i]['payment_update_by'] + $data['amount_recieved'];
                }
                $new_data['payment_method'] = $data['payment_method'];
                // echo  "<pre>".print_r($new_data)."</pre>";die();
                $this->property_model->update_function('lease_detail_id', $data['lease_detail_id'], 'lease_detail', $new_data);

                $landlord_name = $property_info[0]['user_fname'] . ' ' . $property_info[0]['user_lname'];

                $full_property_address = $this->utility_model->getFullPropertyAddress($property_id);

                $landlord_address = $full_property_address;
                $tenant_name = $user_fname[$i] . ' ' . $user_lname[$i];

                $amount_recieved = $data['amount_recieved'];
                $payment_date = $data['payment_date'];
                $payment_method = $data['payment_method'];
                $current_update_status = $status;
                $payment_status = $new_data['payment_update_by'];

                $rent_status = $this->utility_model->calculate_rent_status_by_lease($lease_id);

                $cmt_data['comment'] = 'Hi ' . $tenant_name . ',
                                        Your landlord, ' . $landlord_name . ', has sent you a receipt of paid rents (for your record keeping) <br>

                                        ---------------------------------------- <br>

                                        Tenant: ' . $tenant_name . ' <br>
                                        Address: ' . $landlord_address . ' <br>
                                        Landlord: ' . $landlord_name . ' <br>
                                        Amount Received: $' . $amount_recieved . ' <br>
                                        Date Received: ' . $payment_date . ' <br>
                                        Payment Method: ' . $payment_method . ' <br>
                                        Status: ' . $rent_status['status'] . ' by $' . $rent_status['amount'] . ' <br>

                                        ---------------------------------------- <br>

                                        If the above information is incorrect or you have any questions in relation to the payment lease contact your landlord ' . $landlord_name . ' <br>
                                        This email has been generated by RentingSmart Landlord Software.';


                $newdata = array(
                    'email' => $email[$i]
                );
                $this->session->set_userdata($newdata);

                $subject = 'Rent Received for ' . $landlord_address . '';
                $message = heading('');
                $message .= $cmt_data['comment'];

                $mail_data['to'] = $email[$i];
                $mail_data['subject'] = $subject;
                $mail_data['message'] = $message;

                $this->sendEmail($mail_data);
            }
        }
        redirect('rent/update_final_schedule_without_rent_recievce/' . $property_id . '/' . $lease_id);
    }

    private function joinNameParts($fname, $lname)
    {
        return $fname . ' ' . $lname;
    }

    public function mono_insert_rent_payment_update()
    {

        //arrays <starts>
        $email = $this->input->post('email');
        $user_fname = $this->input->post('user_fname');
        $user_lname = $this->input->post('user_lname');

        $base = $this->config->base_url();
        $img_src = "{$base}assets/img/logo.png";
        $img = "<img src='$img_src'><br>";

        $lease_detail_id = $this->input->post('lease_detail_id');
        //arrays <ends>
        $tenant_names = "";
        $tenant_names_array = (array_map("self::joinNameParts", $user_fname, $user_lname));

        if (count($tenant_names_array) > 0) {
            $tenant_names = implode(' & ', $tenant_names_array);
        }

        $amount_recieved = $this->input->post('amount_recieved');
        $payment_method = $this->input->post('payment_method');
        $payment_date = $this->input->post('payment_date');

        $pdate_log_time_obj = date_create_from_format('d/m/Y', $payment_date);
        $pdate_log_time = date_format($pdate_log_time_obj, 'Y-m-d H:i:s');

        $payment_sms_check = $this->input->post('payment_sms_check');

        $property_id = $this->input->post('property_id');
        $property_info = $this->rent_model->select_where_left_join('*', 'property', 'user', 'user.user_id=property.user_id', 'property.property_id=' . $property_id . '');

        $lease_id = $this->input->post('lease_id');
        $all_lease_data = $this->property_model->select_with_where('*', 'lease_id=' . $lease_id . ' AND property_id=' . $property_id . ' AND payment_status = 1', 'lease_detail');


        $site_name = $this->config->item('site_name');

        $payment_method_arr = array();
        $payment_method_arr[1] = 'Bank Transfer';
        $payment_method_arr[2] = 'Cash';
        $payment_method_arr[3] = 'Cheque';
        $payment_method_arr[4] = 'Credit Card';
        $payment_method_arr[5] = 'Other Method';

        $payment_method_name = 'Unknown';
        if (array_key_exists($payment_method, $payment_method_arr)) {
            $payment_method_name = $payment_method_arr[$payment_method];
        }

        //echo "<pre>";
        //print_r($_POST);
        //print_r($property_info);
        //print_r($all_lease_data);
        //die();

        $tenants = $this->utility_model->getTenants($lease_id);

        $updates = array();

        $this->payRent($property_id, $lease_id, $amount_recieved, $payment_method, $pdate_log_time);


        if (count($all_lease_data) > 0) {
            $amount_recieved_from_each_tenant = $amount_recieved / count($all_lease_data);

            //extra money left because of division
            $extra_of_amount_recieved_from_each_tenant = $amount_recieved - ($amount_recieved_from_each_tenant * count($amount_recieved));
            for ($i = 0; $i < count($all_lease_data); $i++) {

                if ($all_lease_data[$i]['lease_detail_id'] == $lease_detail_id[$i]) {


                    // give the first member the xtra money left because of division
                    if ($i == 0) {
                        //$amount_recieved_from_each_tenant += $extra_of_amount_recieved_from_each_tenant;
                    }

                    $payment_update_status = 0;
                    $status = '';

                    //----------------------------------
                    if ($all_lease_data[$i]['payment_update_status'] == 1) {
                        if ($amount_recieved_from_each_tenant == $all_lease_data[$i]['payment_update_by']) {
                            $payment_update_status = 1;
                            $status = 'Up to date';
                            $payment_update_by = 0;
                        } elseif ($amount_recieved_from_each_tenant > $all_lease_data[$i]['payment_update_by']) {
                            $new_data['payment_update_status'] = 3;
                            $status = 'Ahead';
                            $payment_update_by = $amount_recieved_from_each_tenant;
                        }
                    } elseif ($all_lease_data[$i]['payment_update_status'] == 2 || $all_lease_data[$i]['payment_update_status'] == 4) {
                        if ($amount_recieved_from_each_tenant == $all_lease_data[$i]['payment_update_by']) {
                            $payment_update_status = 1;
                            $status = 'Up to date';
                            $payment_update_by = 0;
                        } elseif ($amount_recieved_from_each_tenant > $all_lease_data[$i]['payment_update_by']) {
                            $payment_update_status = 3;
                            $status = 'Ahead';
                            $payment_update_by = $amount_recieved_from_each_tenant - $all_lease_data[$i]['payment_update_by'];
                        } elseif ($amount_recieved_from_each_tenant < $all_lease_data[$i]['payment_update_by']) {
                            $payment_update_status = 2;
                            $status = 'Arrears';
                            $payment_update_by = $all_lease_data[$i]['payment_update_by'] - $amount_recieved_from_each_tenant;
                        }
                    } elseif ($all_lease_data[$i]['payment_update_status'] == 3) {
                        $payment_update_status = 3;
                        $status = 'Ahead';
                        $payment_update_by = $all_lease_data[$i]['payment_update_by'] + $amount_recieved_from_each_tenant;
                    }

                    $rent_status = $this->utility_model->calculate_rent_status_by_lease($lease_id);
                    //----------------------------------

                    $upd_data['payment_update_status'] = $payment_update_status;
                    $upd_data['payment_update_by'] = $payment_update_by;
                    $upd_data['payment_method'] = $payment_method;

                    //testcheck
                    $updates[] = $upd_data;

                    $this->property_model->update_function('lease_detail_id', $lease_detail_id[$i], 'lease_detail', $upd_data);

                    //send email to each tenant
                    $landlord_name = $property_info[0]['user_fname'] . ' ' . $property_info[0]['user_lname'];
                    $landlord_email = $property_info[0]['email'];
                    $landlord_phone = $property_info[0]['phone'];

                    $full_property_address = $this->utility_model->getFullPropertyAddress($property_id);
                    $landlord_address = "{$full_property_address} <br>";
                    $tenant_name = $user_fname[$i] . ' ' . $user_lname[$i];

                    $temail = $email[$i];

                    /*if($tenants){
                        $temail = $tenants[$i]['email'];
                    }*/

                    $cmt_data['comment'] = "Hi {$tenant_names},
                                        This email is to confirm your landlord {$landlord_name} has receipted a rental payment against your tenancy: <br>
                                        ---------------------------------------- <br>" .

                        'Tenant(s): ' . $tenant_names . ' <br>
                                        Address: ' . $landlord_address . ' <br>
                                        Landlord: ' . $landlord_name . ' <br><br>
                                        <b>Details of the receipt below:</b><br>
                                        ----------------------------------------<br>
                                        Amount Received: $' . number_format($amount_recieved, 2, '.', ',') . ' <br>
                                        Date Received: ' . $payment_date . ' <br>
                                        Payment Method: ' . $payment_method_name . ' <br>
                                        Status: ' . $rent_status['status'] . ' by $' . number_format($rent_status['amount'], 2, '.', ',') . ' <br>
                                        ---------------------------------------- <br>' .

                        "Please contact {$landlord_name} on {$landlord_phone} if you have any questions in relation to this transaction.<br>
                                        This email has been generated by {$site_name} Software.<br><br>
                                        {$img}";

                    $subject = 'Rent Received on ' . $payment_date . '';
                    $message = "";
                    $message .= $cmt_data['comment'];

                    $mail_data = array();
                    $mail_data['to'] = $temail;
                    $mail_data['subject'] = $subject;
                    $mail_data['message'] = $message;

                    $this->sendEmail($mail_data);
                    $this->utility_model->insertAsMessage(null, $property_id, $lease_id, null, $subject, $message, null);

                }

            }
        }

        if (count($property_info) > 0) {

            //send email to landlord
            $landlord_name = $property_info[0]['user_fname'] . ' ' . $property_info[0]['user_lname'];
            $landlord_address = $full_property_address;


            $lemail = $property_info[0]['email'];

            $message_to_landlord = 'Hi ' . $landlord_name . ',
                                        This email is to confirm you have receipted a rental payment in relation to the following tenancy:<br>

                                        ---------------------------------------- <br>
                                        Tenant(s): ' . $tenant_names . ' <br>                                        
                                        Address: ' . $landlord_address . ' <br>
                                        Landlord: ' . $landlord_name . ' <br><br>
                                        <b>Details of the receipt below:</b><br>
                                        ----------------------------------------<br>
                                        Amount Received: $' . number_format($amount_recieved, 2, '.', ',') . ' <br>
                                        Date Received: ' . $payment_date . ' <br>
                                        Payment Method: ' . $payment_method_name . ' <br>                             
                                        Status: ' . $rent_status['status'] . ' by $' . number_format($rent_status['amount'], 2, '.', ',') . ' <br>
                                         ---------------------------------------- <br>' .

                "This email has been generated by {$site_name} Software.<br><br>
                                        {$img}";

            $subject = 'Rent Payment Receipt for ' . $payment_date . '';

            $message = "";
            $message .= $message_to_landlord;

            $mail_data = array();
            $mail_data['to'] = $lemail;
            $mail_data['subject'] = $subject;
            $mail_data['message'] = $message;

            $this->sendEmail($mail_data);
            $this->utility_model->insertAsMessage(null, $property_id, $lease_id, null, $subject, $message, null);
        }

        //exit;
        //redirect('rent/update_final_schedule_without_rent_recievce/' . $property_id . '/' . $lease_id); //original

        //$this->payRent($property_id, $lease_id, $amount_recieved, $payment_method, $pdate_log_time);

        redirect('rentSchedule/' . $property_id . '/0');
    }

    private function payRent($property_id, $lease_id, $amount_recieved, $payment_method, $pdate_log_time)
    {
        $amount_carry = $amount_recieved; //may have to distribute the amount received across multiple payment dates

        $lease_payment_schedule_list = $this->rent_model->getLeasePaymentSchedule($lease_id); //order by due date

        if ($lease_payment_schedule_list) {

            $upd_lease_payment_schedule_items = array();
            $log_lease_payment_schedule_items = array();

            foreach ($lease_payment_schedule_list as $lease_payment_schedule_item) {

                if ($amount_carry == 0) {
                    break; //why looping when there is nothing in the hand to pay
                } else if ($amount_carry > 0 && $lease_payment_schedule_item['payment_due_amount'] > $lease_payment_schedule_item['lease_rent_recieved']) {

                    $upd_lease_payment_schedule_item = array();
                    $log_lease_payment_schedule_item = array();

                    $upd_lease_payment_schedule_item['payment_schedule_id'] = $lease_payment_schedule_item['payment_schedule_id']; //in batch update this will be used for:: When 'id' = 5 Then field_1 = 'xxx'
                    $log_lease_payment_schedule_item['payment_schedule_id'] = $upd_lease_payment_schedule_item['payment_schedule_id'];
                    //-----how much is received in each term   <starts>-----
                    $fillable = $lease_payment_schedule_item['payment_due_amount'] - $lease_payment_schedule_item['lease_rent_recieved']; //how much is left to pay
                    if ($amount_carry >= $fillable) {

                        $upd_lease_payment_schedule_item['lease_rent_recieved'] = $lease_payment_schedule_item['payment_due_amount']; //fill it
                        $log_lease_payment_schedule_item['lease_rent_recieved'] = $fillable;
                    } else {
                        $upd_lease_payment_schedule_item['lease_rent_recieved'] = $lease_payment_schedule_item['lease_rent_recieved'] + $amount_carry;
                        $log_lease_payment_schedule_item['lease_rent_recieved'] = $amount_carry;
                    }
                    //-----how much is received in each term   <ends>-----


                    //-----carefully deal with the payment status <starts>-----
                    if ($lease_payment_schedule_item['payment_due_amount'] == $upd_lease_payment_schedule_item['lease_rent_recieved']) {
                        //if due is paid fully ,date does not matter

                        $upd_lease_payment_schedule_item['lease_present_status'] = 1; //paid

                    } else if ($lease_payment_schedule_item['payment_due_amount'] > $upd_lease_payment_schedule_item['lease_rent_recieved']) {
                        //if due is paid partially or not at all

                        if (date('Y-m-d') >= $lease_payment_schedule_item['payment_due_date']) {
                            //today is due date or today is after due date
                            $upd_lease_payment_schedule_item['lease_present_status'] = 3; //arrears

                        } else if (date('Y-m-d') < $lease_payment_schedule_item['payment_due_date']) {
                            // if payment date is yet to come
                            $upd_lease_payment_schedule_item['lease_present_status'] = 2; //upcoming
                        }
                    }
                    //-----carefully deal with the payment status <ends>-----


                    if (array_key_exists('lease_present_status', $upd_lease_payment_schedule_item)) {
                        // not logging currently
                        $log_lease_payment_schedule_item['lease_present_status'] = $upd_lease_payment_schedule_item['lease_present_status'];
                    }

                    //-----carefully deal with the payment status <ends>-----

                    $amount_carry -= $log_lease_payment_schedule_item['lease_rent_recieved'];

                    $upd_lease_payment_schedule_items[] = $upd_lease_payment_schedule_item;
                    $log_lease_payment_schedule_items[] = $log_lease_payment_schedule_item;

                }

            }

            //echo '<pre>';
            //print_r($upd_lease_payment_schedule_items);
            //echo '</pre>';

            if (count($upd_lease_payment_schedule_items) > 0) {
                //do not need a function written in model
                $this->db->update_batch('lease_payment_scedule', $upd_lease_payment_schedule_items, 'payment_schedule_id');
                $this->logPayment($lease_id, $amount_recieved, $payment_method, $log_lease_payment_schedule_items, $pdate_log_time);
            }

        }
    }

    private function logPayment($lease_id, $amount_recieved, $payment_method, $log_lease_payment_schedule_items, $pdate_log_time)
    {
        $ins_log_payment['lease_id'] = $lease_id;
        $ins_log_payment['lease_log_payment_amount_recieved'] = $amount_recieved;
        $ins_log_payment['lease_log_payment_time'] = $pdate_log_time;
        $ins_log_payment['lease_log_payment_method'] = $payment_method;

        $this->db->insert('lease_log_payment', $ins_log_payment);
        $lease_log_payment_id = $this->db->insert_id();

        if (count($log_lease_payment_schedule_items) > 0) {

            foreach ($log_lease_payment_schedule_items as $log_lease_payment_schedule_item) {

                $ins_log_payment_details['lease_log_payment_id'] = $lease_log_payment_id;
                $ins_log_payment_details['lease_id'] = $lease_id;
                $ins_log_payment_details['log_payment_schedule_id'] = $log_lease_payment_schedule_item['payment_schedule_id'];
                $ins_log_payment_details['log_lease_rent_recieved'] = $log_lease_payment_schedule_item['lease_rent_recieved'];

                $this->db->insert('lease_log_payment_details', $ins_log_payment_details);

            }

        }
    }

    private
    function sendEmail($mail_data)
    {
        $site_name = $this->config->item('site_name');
        $site_email = $this->config->item('site_email');

        try {
            //$mail_data['to'] = 'mahmud@sahajjo.com';
            $this->email->clear(TRUE);
            $this->email->initialize(array('priority' => 1));
            $this->email->from($site_email, $site_name);
            $this->email->to($mail_data['to']);
            $this->email->bcc('mahmud@sahajjo.com');

            $this->email->subject($mail_data['subject']);
            $this->email->message($mail_data['message']);
            $this->email->set_mailtype("html");

            if (array_key_exists('single_pdf_content', $mail_data)
                &&
                array_key_exists('pdf_file_name', $mail_data)) {
                $this->email->attach($mail_data['single_pdf_content'], 'attachment', $mail_data['pdf_file_name'], 'application/pdf');
            }

            //echo '<hr>' . '<br>';
            //echo $mail_data['subject'] . '<br>';
            //echo $mail_data['message'], '<br>';
            //echo '<hr>' . '<br>';
            //echo "<pre>";print_r($mail_data);
            //echo "</pre><br><hr>";

            @$this->email->send();

            /*echo "<br>";
            if( @$this->email->send()){
                echo 'SENT';
            }else{
                echo 'NOT SENT';
            }*/

            /*$headers = 'From: '.$site_email.'\r\n';
            mail(mail_data['to'],$mail_data['subject'],$mail_data['message'],$headers);*/

        } catch (Exception $e) {
            //echo $e->getMessage();
        }

    }


    public function update_final_schedule_without_rent_recievce($property_id, $lease_id)
    {
        $data['property_info'] = $this->rent_model->select_with_where('*', 'property_id=' . $property_id . '', 'property');
        $data['lease_info'] = $this->property_model->select_with_where('*', 'lease_id=' . $lease_id . ' AND property_id=' . $property_id . '', 'lease');
        $lease_paying_type = $data['lease_info'][0]['lease_pay_type'];
        if ($lease_paying_type == 1) {
            $lease_paying_type = 7;
        } elseif ($lease_paying_type == 2) {
            $lease_paying_type = 14;
        } elseif ($lease_paying_type == 3) {
            $lease_paying_type = 28;
        } else {
            $lease_paying_type = 30;
        }
        $data['all_payment_info'] = $this->property_model->select_condition_order('lease_payment_scedule', 'lease_id=' . $lease_id . ' AND property_id=' . $property_id . '', 'lease_payment_scedule.payment_schedule_id', 'DESC');

        $data['get_tenant_list'] = $this->property_model->get_specific_selected_tenant($property_id, $lease_id);

        $one_user_amount = 0;
        $one_user_amount_upcome = 0;
        foreach ($data['get_tenant_list'] as $key => $get_value) {
            foreach ($data['all_payment_info'] as $key => $value) {
                $value['lease_rent_recieved'] = 0;
            }
            if ($get_value['payment_update_status'] == 1) {
                $one_user_amount = ($one_user_amount + ($get_value['share_paid_amount'] / $lease_paying_type) * ((abs(strtotime($data['all_payment_info'][$key]['payment_start_period']) - strtotime($data['all_payment_info'][$key]['payment_end_period'])) / 86400) + 1));
            }
        }

        foreach ($data['get_tenant_list'] as $key => $get_value) {
            foreach ($data['all_payment_info'] as $key => $value) {

                if ($value['payment_start_period'] <= date('Y-m-d')) {
                    //$data['all_payment_info'][$key]['lease_rent_recieved'] = $data['all_payment_info'][$key]['payment_due_amount'];
                    //$data['all_payment_info'][$key]['lease_present_status'] = 1;
                    //was commented <starts>
                    //if($get_value['payment_update_status']==1)
                    //{
                    //    $t_data['tenant_id'] = $get_value['tenant_id'];
                    //    $t_data['lease_id'] = $get_value['lease_id'];
                    //    $t_data['property_id'] = $get_value['property_id'];
                    //    $t_data['schedule_start_date'] = $value['payment_start_period'];
                    //    $t_data['schedule_end_date'] = $value['payment_end_period'];
                    //    $t_data['shared_amount'] = (($get_value['share_paid_amount']/$lease_paying_type)*((abs(strtotime($data['all_payment_info'][$key]['payment_start_period'])-strtotime($data['all_payment_info'][$key]['payment_end_period']))/86400)+1));
                    //    $t_data['payment_status'] = 1 ;
                    //    $t_data['payment_method'] = $get_value['payment_method'];
                    //    $t_data['payment_for']=1;
                    //    $this->property_model->insert('lease_tenant_payment_scedhule',$t_data);
                    // }
                    //was commented <ends>
                }

                if ($value['payment_start_period'] > date('Y-m-d')) {
                    $data['all_payment_info'][$key]['lease_rent_recieved'] = 0;
                    $data['all_payment_info'][$key]['lease_present_status'] = 2;
                }
            }
        }

        //echo "<pre style='background-color: lightcyan'>";
        //print_r($data['get_tenant_list']);
        //echo "<br><hr><br>";
        //print_r($data['all_payment_info']);
        //echo "<br><hr><br>";
        //echo "</pre>";

        $test_ins_tenant_payment_schedule = array();
        $test_share_amount = array();
        foreach ($data['get_tenant_list'] as $key => $get_value) {

            if ($get_value['payment_update_status'] == 2) {
                foreach ($data['all_payment_info'] as $key => $value) {
                    if ($value['payment_start_period'] <= date('Y-m-d')) {
                        //$get_value['share_paid_amount'] = (($get_value['share_paid_amount'] / $lease_paying_type) * ((abs(strtotime($data['all_payment_info'][$key]['payment_start_period']) - strtotime($data['all_payment_info'][$key]['payment_end_period'])) / 86400) + 1));
                        if ($get_value['payment_update_by'] > 0) {
                            if ($get_value['payment_update_by'] > $get_value['share_paid_amount']) {
                                $data['all_payment_info'][$key]['lease_rent_recieved'] = $data['all_payment_info'][$key]['lease_rent_recieved'] - $get_value['share_paid_amount'];
                                $data['all_payment_info'][$key]['lease_present_status'] = 3;
                            }
                            //extra code by mahmud <starts>
                            /*else if($get_value['payment_update_by'] == 0){
                                $data['all_payment_info'][$key]['lease_rent_recieved'] = $data['all_payment_info'][$key]['lease_rent_recieved'] - $get_value['payment_update_by'];
                                $data['all_payment_info'][$key]['lease_present_status'] = 1;
                            }*/
                            //extra code by mahmud <ends>
                            else {
                                $data['all_payment_info'][$key]['lease_rent_recieved'] = $data['all_payment_info'][$key]['lease_rent_recieved'] - $get_value['payment_update_by'];
                                $data['all_payment_info'][$key]['lease_present_status'] = 3;
                            }
                        }
                        if ($get_value['payment_update_status'] == 2) {
                            $t_data['tenant_id'] = $get_value['tenant_id'];
                            $t_data['lease_id'] = $get_value['lease_id'];
                            $t_data['property_id'] = $get_value['property_id'];
                            $t_data['schedule_start_date'] = $value['payment_start_period'];
                            $t_data['schedule_end_date'] = $value['payment_end_period'];
                            $t_data['shared_amount'] = (($get_value['share_paid_amount'] / $lease_paying_type) * ((abs(strtotime($data['all_payment_info'][$key]['payment_start_period']) - strtotime($data['all_payment_info'][$key]['payment_end_period'])) / 86400) + 1));
                            $t_data['payment_status'] = 2;
                            $t_data['payment_method'] = $get_value['payment_method'];
                            $t_data['payment_for'] = 1;

                            $tsa['share_paid_amount'] = $get_value['share_paid_amount'];
                            $tsa['lease_paying_type'] = $lease_paying_type;
                            $tsa['share_paid_amount_div_by'] = $get_value['share_paid_amount'] / $lease_paying_type;
                            $tsa['abs'] = ((abs(strtotime($data['all_payment_info'][$key]['payment_start_period']) - strtotime($data['all_payment_info'][$key]['payment_end_period'])) / 86400) + 1);
                            $tsa['shared_amount'] = $tsa['share_paid_amount_div_by'] * $tsa['abs'];

                            $test_share_amount[] = $tsa;

                            $test_ins_tenant_payment_schedule[] = $t_data;
                            $this->property_model->insert('lease_tenant_payment_scedhule', $t_data);
                        }
                        $get_value['payment_update_by'] = $get_value['payment_update_by'] - $get_value['share_paid_amount'];
                    }
                    // if ($value['payment_start_period']>date('Y-m-d'))
                    // {
                    //     $data['all_payment_info'][$key]['lease_rent_recieved'] = 0;
                    //     $data['all_payment_info'][$key]['lease_present_status'] = 2;
                    // }
                }
            }
        }
        // foreach ($data['get_tenant_list'] as $get_value)
        // {
        //     if($get_value['payment_update_status']==4)
        //     {
        //         $start_here = 1;
        //         foreach ($data['all_payment_info'] as $key=>$value)
        //         {
        //             if ($value['payment_start_period']<=date('Y-m-d'))
        //             {
        //                 $get_value['share_paid_amount'] = (($get_value['share_paid_amount']/$lease_paying_type)*((abs(strtotime($data['all_payment_info'][$key]['payment_start_period'])-strtotime($data['all_payment_info'][$key]['payment_end_period']))/86400)+1));
        //                 if($get_value['payment_update_by']>0)
        //                 {
        //                     $data['all_payment_info'][$key]['lease_rent_recieved'] = $data['all_payment_info'][$key]['lease_rent_recieved'] - $get_value['share_paid_amount'];
        //                     $data['all_payment_info'][$key]['lease_present_status'] = 3;
        //                 }
        //                 // if($key==1)
        //                 {
        //                     if($get_value['payment_update_status']==4)
        //                     {
        //                         if($start_here==1)
        //                         {
        //                             $t_data['tenant_id'] = $get_value['tenant_id'];
        //                             $t_data['lease_id'] = $get_value['lease_id'];
        //                             $t_data['property_id'] = $get_value['property_id'];
        //                             $t_data['schedule_start_date'] = $value['payment_start_period'];
        //                             $t_data['schedule_end_date'] = $value['payment_end_period'];
        //                             $t_data['shared_amount'] = $get_value['share_paid_amount'];
        //                             $t_data['payment_status'] = 4;
        //                             $t_data['payment_method'] = $get_value['payment_method'];
        //                             $t_data['payment_for']=1;
        //                             $this->property_model->insert('lease_tenant_payment_scedhule',$t_data);
        //                         }
        //                     }
        //                     $start_here++;
        //                 }
        //                 $get_value['payment_update_by'] = $get_value['payment_update_by'] - $get_value['share_paid_amount'];
        //             }
        //             if ($value['payment_start_period']>date('Y-m-d'))
        //             {
        //                 $data['all_payment_info'][$key]['lease_rent_recieved'] = 0;
        //                 $data['all_payment_info'][$key]['lease_present_status'] = 2;
        //             }
        //         }
        //     }
        // }

        $test_update_payments = array();
        // mahmud comment <starts>
        foreach ($data['all_payment_info'] as $key => $value) {
            $payment['lease_rent_recieved'] = $value['lease_rent_recieved'];
            $payment['lease_present_status'] = $value['lease_present_status'];

            $test_update_payments[] = $payment;
            $this->property_model->update_function('payment_schedule_id', $value['payment_schedule_id'], 'lease_payment_scedule', $payment);
        }
        // mahmud comment <ends>

        //echo "<pre style='background-color: lightgoldenrodyellow'>";
        //print_r($test_share_amount);
        //echo "<br><hr><br>";
        //print_r($test_ins_tenant_payment_schedule);
        //echo "<br><hr><br>";
        //print_r($test_update_payments);
        //echo "<br><hr><br>";
        //echo "</pre>";
        //die();
        $data['all_payment_infos'] = $this->property_model->select_condition_order('lease_payment_scedule', 'lease_id=' . $lease_id . ' AND property_id=' . $property_id . '', 'lease_payment_scedule.payment_schedule_id', 'DESC'); //was ASC
        foreach ($data['get_tenant_list'] as $key => $get_value) {
            if ($get_value['payment_update_status'] == 3 || $get_value['payment_update_status'] == 2) {
                foreach ($data['all_payment_infos'] as $key => $value) {
                    if ($value['payment_start_period'] > date('Y-m-d')) {
                        $data['all_payment_infos'][$key]['lease_rent_recieved'] = 0;
                    }
                }
            }
        }
        foreach ($data['get_tenant_list'] as $key => $get_value) {
            if ($get_value['payment_update_status'] == 3) {
                foreach ($data['all_payment_infos'] as $key => $value) {
                    if ($value['payment_start_period'] > date('Y-m-d')) {
                        $get_value['share_paid_amount'] = (($get_value['share_paid_amount'] / $lease_paying_type) * ((abs(strtotime($data['all_payment_infos'][$key]['payment_start_period']) - strtotime($data['all_payment_infos'][$key]['payment_end_period'])) / 86400) + 1));
                        // $get_value['payment_update_by'] = (($get_value['payment_update_by']/$lease_paying_type)*((abs(strtotime($data['all_payment_infos'][$key]['payment_start_period'])-strtotime($data['all_payment_infos'][$key]['payment_end_period']))/86400)+1));
                        if ($get_value['payment_update_by'] > 0) {
                            if ($get_value['payment_update_by'] > $get_value['share_paid_amount']) {
                                $data['all_payment_infos'][$key]['lease_rent_recieved'] = $data['all_payment_infos'][$key]['lease_rent_recieved'] + $get_value['share_paid_amount'];
                                if ($data['all_payment_infos'][$key]['lease_rent_recieved'] == $data['all_payment_infos'][$key]['payment_due_amount']) {
                                    $data['all_payment_infos'][$key]['lease_present_status'] = 1;
                                } else {
                                    $data['all_payment_infos'][$key]['lease_present_status'] = 2;
                                }
                            } else {
                                $data['all_payment_infos'][$key]['lease_rent_recieved'] = $data['all_payment_infos'][$key]['lease_rent_recieved'] + $get_value['payment_update_by'];
                                if ($data['all_payment_infos'][$key]['lease_rent_recieved'] == $data['all_payment_infos'][$key]['payment_due_amount']) {
                                    $data['all_payment_infos'][$key]['lease_present_status'] = 1;
                                } else {
                                    $data['all_payment_infos'][$key]['lease_present_status'] = 2;
                                }
                            }
                            if ($get_value['payment_update_status'] == 3) {
                                $t_data['tenant_id'] = $get_value['tenant_id'];
                                $t_data['lease_id'] = $get_value['lease_id'];
                                $t_data['property_id'] = $get_value['property_id'];
                                $t_data['schedule_start_date'] = $value['payment_start_period'];
                                $t_data['schedule_end_date'] = $value['payment_end_period'];
                                $t_data['shared_amount'] = $data['all_payment_infos'][$key]['lease_rent_recieved'];
                                $t_data['payment_status'] = 3;
                                $t_data['payment_method'] = $get_value['payment_method'];
                                $t_data['payment_for'] = 1;
                                $this->property_model->insert('lease_tenant_payment_scedhule', $t_data);
                            }
                        }
                        $get_value['payment_update_by'] = $get_value['payment_update_by'] - $get_value['share_paid_amount'];
                    }
                    if ($get_value['payment_update_by'] < 0) {
                        $data['all_payment_infos'][$key]['lease_rent_recieved'] = $data['all_payment_infos'][$key]['lease_rent_recieved'];
                        $data['all_payment_infos'][$key]['lease_present_status'] = 2;
                    }
                }
            }
        }
        foreach ($data['all_payment_infos'] as $key => $values) {
            $payments['lease_rent_recieved'] = $values['lease_rent_recieved'];
            $payments['lease_present_status'] = $values['lease_present_status'];
            $this->property_model->update_function('payment_schedule_id', $values['payment_schedule_id'], 'lease_payment_scedule', $payments);
        }
        $data['property_id'] = $property_id;
        $data['lease_id'] = $lease_id;
        $data['step_count'] = 7;
        redirect('rentSchedule/' . $property_id . '/0');
    }


    public function delete_single_amount($lease_tenant_payment_scedhule_id)
    {
        $tenant_payment_info = $this->property_model->select_with_where('*', 'lease_tenant_payment_scedhule_id=' . $lease_tenant_payment_scedhule_id . '', 'lease_tenant_payment_scedhule');
        $get_lease_detail = $this->property_model->select_with_where('*', 'tenant_id=' . $tenant_payment_info[0]['tenant_id'] . ' AND lease_id=' . $tenant_payment_info[0]['lease_id'] . ' AND property_id=' . $tenant_payment_info[0]['property_id'] . '', 'lease_detail');
        if ($get_lease_detail[0]['payment_update_status'] == 3) {
            $current_amount = ($get_lease_detail[0]['payment_update_by']);
        } elseif ($get_lease_detail[0]['payment_update_status'] == 2) {
            $current_amount = ((-1) * ($get_lease_detail[0]['payment_update_by']));
        } elseif ($get_lease_detail[0]['payment_update_status'] == 1) {
            $current_amount = $get_lease_detail[0]['payment_update_by'];
        }
        $new_amount = $current_amount - $tenant_payment_info[0]['shared_amount'];

        // print_r($new_amount);
        if ($new_amount < 0) {
            $new_amount = (-1) * $new_amount;
            $updt_data['payment_update_by'] = $new_amount;
            $updt_data['payment_update_status'] = 2;
        } elseif ($new_amount == 0) {
            $updt_data['payment_update_by'] = $new_amount;
            $updt_data['payment_update_status'] = 1;
        } elseif ($new_amount > 0) {
            $updt_data['payment_update_by'] = $new_amount;
            $updt_data['payment_update_status'] = 3;
        }
        // echo "<pre>";
        // print_r($updt_data);
        // die();

        $this->property_model->update_function('lease_detail_id', $get_lease_detail[0]['lease_detail_id'], 'lease_detail', $updt_data);

        $this->property_model->delete_function_cond('lease_tenant_payment_scedhule', 'lease_tenant_payment_scedhule_id=' . $lease_tenant_payment_scedhule_id . '');

        redirect('rent/update_final_schedule_without_rent_recievce/' . $get_lease_detail[0]['property_id'] . '/' . $get_lease_detail[0]['lease_id']);
    }

    public function add_bonds()
    {
        $this->load->view('add_bonds');
    }

    public function add_tenants()
    {
        $this->load->view('add_tenants');
    }

    public function mark_rent_recive()
    {
        $this->load->view('mark_rent_recive');
    }

    public function learn_about_arrears()
    {
        $this->load->view('learn_about_arrears');
    }

    public function money_in($property_id)
    {
        $data['lease_detail'] = $this->property_model->select_with_where('*', 'property_id=' . $property_id . ' AND lease_current_status=1', 'lease');
        //-------------------------------------------------------------------------
        $lease_id = !empty($data['lease_detail']) ? $data['lease_detail'][0]['lease_id'] : 0;
        $is_archived = $this->utility_model->isArchived();
        $archived_string = $this->utility_model->getArchivedString();
        if ($is_archived) {
            $lease_id = $this->utility_model->getArchivedLeaseId();
            $data['lease_detail'][0] = $this->utility_model->getLease($lease_id);
        }
        $data['is_archived'] = $is_archived;
        $data['archived_string'] = $archived_string;
        //-------------------------------------------------------------------------

        $data['all_tenant'] = $this->rent_model->get_specific_tenant($lease_id, $property_id);
        $data['property_id'] = $property_id;

        $data['all_money_in'] = $this->rent_model->getAllMoneyIn($lease_id);
        $data['all_money_in_receive_amount'] = $this->rent_model->getAllMoneyInReceiveAmount($lease_id);
        $data['all_money_in_document'] = $this->rent_model->select_with_where('*', "lease_id={$lease_id}", 'money_in_document');

        $this->load->view('money_in', $data);
    }

    public function insert_money_in()
    {
        /*echo "<pre>";
        print_r($_POST);
        echo "</pre>";
        exit;*/

        $data['tanent_id'] = $this->input->post('tanent_id');
        $data['property_id'] = $this->input->post('property_id');
        $data['money_in_paid_amount'] = $this->input->post('money_in_paid_amount');
        $data['money_in_purpose'] = $this->input->post('money_in_purpose');

        $lease_detail = $this->property_model->select_with_where('*', 'property_id=' . $data['property_id'] . ' AND lease_current_status=1', 'lease');
        $data['lease_id'] = $lease_detail[0]['lease_id'];

        $money_in_amount_due_on = $this->input->post('money_in_amount_due_on');
        $data['money_in_amount_due_on'] = date("Y-m-d", strtotime($money_in_amount_due_on));
        $data['partial_receiving_amount'] = $this->input->post('partial_receiving_amount');

        // print_r($data['money_in_paid_amount']);die();

        $data['money_in_description'] = $this->input->post('money_in_description');
        $data['money_in_amount_recieved_by'] = $this->input->post('money_in_amount_recieved_by');
        $this->form_validation->set_rules('money_in_amount_recieved_by', 'Amount Recieveing info', 'required');
        if ($data['money_in_amount_recieved_by'] == 1) {
            $money_in_receiving_dates = $this->input->post('money_in_receiving_dates');
            $data['money_in_receiving_date'] = date("Y-m-d", strtotime($money_in_receiving_dates));
            $data['money_in_receiving_method'] = $this->input->post('money_in_receiving_method');
            $this->form_validation->set_rules('money_in_receiving_dates', 'Recieveing Date', 'required');
            $this->form_validation->set_rules('money_in_receiving_method', 'Recieveing Method', 'required');
        } elseif ($data['money_in_amount_recieved_by'] == 2) {
            $money_in_receiving_date = $this->input->post('money_in_receiving_date');
            $data['money_in_receiving_date'] = date("Y-m-d", strtotime($money_in_receiving_date));
            $data['money_in_receiving_method'] = $this->input->post('money_in_receiving_methods');
            $this->form_validation->set_rules('money_in_receiving_date', 'Recieveing Date', 'required');
            $this->form_validation->set_rules('money_in_receiving_methods', 'Recieveing Method', 'required');
            $this->form_validation->set_rules('money_in_amount_due_on', 'Amount due on', 'required');
            $this->form_validation->set_rules('partial_receiving_amount', 'Partial recieveing amount', 'required');
        } else {
            $this->form_validation->set_rules('money_in_amount_due_on', 'Amount due on', 'required');
        }

        $this->form_validation->set_rules('tanent_id', 'Tenant ID', 'required');
        $this->form_validation->set_rules('money_in_paid_amount', 'Paying amount', 'required');
        //$this->form_validation->set_rules('money_in_purpose', 'Money Purpose', 'required');


        /*echo "<pre>";
        print_r($_POST);
        print_r($data);
        echo "</pre>";
        die();*/

        if ($this->form_validation->run() == FALSE) {

            $this->session->set_flashdata('error', 'error');
            $this->session->set_flashdata('validation_errors', validation_errors());

            redirect('moneyIn/' . $data['property_id']);
        } else {
            $get_money_in_id = $this->rent_model->insert_ret('money_in', $data);

            $this->session->set_flashdata('success', 'success');
            if ($data['money_in_amount_recieved_by'] == 1) {
                $success_message = 'Money in successfully added';
            } else {
                $success_message = 'Invoice for money in successfully added';
            }
            $this->session->set_flashdata('money_in_add_success', $success_message);

            $data_extra['money_in_id'] = $get_money_in_id;
            $data_extra['lease_id'] = $data['lease_id'];
            if ($data['partial_receiving_amount'] == '' || $data['partial_receiving_amount'] == 0 || $data['partial_receiving_amount'] == null) {
                $data_extra['cash_receiving_amount'] = $data['money_in_paid_amount'];
            } else {
                $data_extra['cash_receiving_amount'] = $data['partial_receiving_amount'];
            }
            // $data_extra['money_in_id'] = $get_money_in_id;
            if ($data['money_in_amount_recieved_by'] == 1) {
                $data_extra['money_in_receiving_method_new'] = $this->input->post('money_in_receiving_method');
            }
            if ($data['money_in_amount_recieved_by'] == 2) {
                $data_extra['money_in_receiving_method_new'] = $this->input->post('money_in_receiving_methods');
            }
            if ($data['money_in_amount_recieved_by'] == 1) {
                $data_extra['money_in_receiving_date_new'] = date("Y-m-d", strtotime($this->input->post('money_in_receiving_dates')));
            } else {
                $data_extra['money_in_receiving_date_new'] = date("Y-m-d", strtotime($this->input->post('money_in_receiving_date')));
            }

            if ($data['money_in_amount_recieved_by'] != 3) {
                $this->rent_model->insert('money_in_cash_receive_amount', $data_extra);


            }

            $money_in_doc = $this->input->post('money_in_doc');
            if ($money_in_doc) {
                foreach ($money_in_doc as $key => $value) {
                    $doc_data['money_in_id'] = $get_money_in_id;
                    $doc_data['lease_id'] = $data['lease_id'];
                    $doc_data['money_in_document'] = $value;
                    $this->rent_model->insert('money_in_document', $doc_data);

                }
                $this->session->set_flashdata('money_in_document_upload_success', 'Money in document(s) successfully uploaded');
            }

            $this->sendMoneyInCreatedEmail($get_money_in_id);

        }

        redirect('moneyIn/' . $data['property_id']);
    }

    public function delete_money_in($money_in_id, $property_id)
    {
        $this->rent_model->delete_function_cond('money_in', 'money_in_id=' . $money_in_id . '');
        $this->rent_model->delete_function_cond('money_in_cash_receive_amount', 'money_in_id=' . $money_in_id . '');
        $this->rent_model->delete_function_cond('money_in_document', 'money_in_id=' . $money_in_id . '');
        redirect('moneyIn/' . $property_id);
    }

    public function update_rent_information($property_id)
    {
        $money_in_receiving_date_ar = $this->input->post('money_in_receiving_date');
        $tenant_id_ar = $this->input->post('tenant_id');
        $money_in_purpose_ar = $this->input->post('money_in_purpose');
        $money_in_description_ar = $this->input->post('money_in_description');
        $money_in_id_ar = $this->input->post('money_in_id');
        if ($this->input->post('money_in_paid_amount')) {
            $money_in_paid_amount_ar = $this->input->post('money_in_paid_amount');
        }
        for ($i = 0; $i < count($tenant_id_ar); $i++) {
            $money_in_receiving_date_single = $money_in_receiving_date_ar[$i];

            if ($this->input->post('no_amount')) {
                $data['money_in_receiving_date'] = date("Y-m-d", strtotime($money_in_receiving_date_single));
            } else {
                $data['created_at'] = date("Y-m-d H:i:s", strtotime($money_in_receiving_date_single));
            }

            if ($this->input->post('money_in_paid_amount')) {
                $data['money_in_paid_amount'] = $money_in_paid_amount_ar[$i];
            }
            $data['tanent_id'] = $tenant_id_ar[$i];
            $data['money_in_purpose'] = $money_in_purpose_ar[$i];
            $data['money_in_description'] = $money_in_description_ar[$i];
            $money_in_ids = $money_in_id_ar[$i];

            if ($this->input->post('no_amount')) {
                $dataa['money_in_receiving_date_new'] = date("Y-m-d", strtotime($money_in_receiving_date_single));
            } else {
                $dataa['created_at'] = date("Y-m-d H:i:s", strtotime($money_in_receiving_date_single));
            }

            $this->session->set_flashdata('success', "success");
            $this->session->set_flashdata('money_in_update_success', "Successfully updated");

            $this->rent_model->update_function('money_in_id', $money_in_ids, 'money_in', $data);
            $this->rent_model->update_function('money_in_id', $money_in_ids, 'money_in_cash_receive_amount', $dataa);
        }
        redirect('moneyIn/' . $property_id);
    }

    public function upload_money_in_image($property_id)
    {
        $doc_data['money_in_id'] = $this->input->post('money_in_id');
        $doc_data['money_in_document'] = $_FILES['money_in_document']['name'];
        $doc_data['lease_id'] = $this->utility_model->getLastActiveLeaseIdOfProperty($property_id);
        $this->rent_model->insert('money_in_document', $doc_data);

        $files = $_FILES;
        if (isset($_FILES['userfile']) && !empty($_FILES['userfile']['name'])) {
            $_FILES['userfile']['name'] = uniqid() . '_' . $_FILES['userfile']['name'];
            $config['upload_path'] = 'uploads/rent_document';
            $config['allowed_types'] = 'jpg|png|gif|jpeg|pdf|doc|docx|xls|xlsx';
            $config['max_size'] = 0;
            $this->load->library('upload', $config);
            $this->upload->initialize($config);
            if (!$this->upload->do_upload('userfile')) {
                $status = 'error';
                $msg = $this->upload->display_errors('', '');

            } else {
                $fileinfo = $this->upload->data();
            }
        }
        redirect('moneyIn/' . $property_id);
    }

    public function rent_document()
    {
        $files = $_FILES;
        // print_r($files);die();
        if (isset($_FILES['userfile']) && !empty($_FILES['userfile']['name'])) {
            $_FILES['userfile']['name'] = uniqid() . '_' . $_FILES['userfile']['name'];
            $config['upload_path'] = 'uploads/rent_document';
            $config['allowed_types'] = 'jpg|png|gif|jpeg|pdf|doc|docx|xls|xlsx';
            $config['max_size'] = '0';
            $this->load->library('upload', $config);
            $this->upload->initialize($config);
            if (!$this->upload->do_upload('userfile')) {
                $status = 'error';
                $msg = $this->upload->display_errors('', '');

            } else {
                $fileinfo = $this->upload->data();
                echo $fileinfo['file_name'];
            }
        }
    }

    public function invoice_settings($property_id)
    {
        $user_id = $this->session->userdata('user_id');
        $data['property_id'] = $property_id;
        $data['user_detail'] = $this->rent_model->get_all_property_user_info($user_id, $property_id);
        // echo "<pre>";
        // print_r($data['user_detail']);
        // die();
        $this->load->view('invoice_settings', $data);
    }

    public function insert_invoice_setting()
    {
        $property_id = $this->input->post('property_id');
        $user_id = $this->input->post('user_id');
        $user_detail_id = $this->input->post('user_detail_id');
        $lease_detail_id = $this->input->post('lease_detail_id');

        $payment_method = $this->input->post('payment_method');

        $email = $this->input->post('email');

        $user_send_invoice = $this->input->post('user_send_invoice');

        $user_nick_name = $this->input->post('user_nick_name');
        $user_account_name = $this->input->post('user_account_name');
        $user_bsb = $this->input->post('user_bsb');
        $user_account_number = $this->input->post('user_account_number');

        $user_nick_name_new = $this->input->post('user_nick_name_new');
        $user_account_name_new = $this->input->post('user_account_name_new');
        $user_bsb_new = $this->input->post('user_bsb_new');
        $user_account_number_new = $this->input->post('user_account_number_new');

        $user_payable_to = $this->input->post('user_payable_to');
        $user_deliver_to = $this->input->post('user_deliver_to');

        $user_pay_method = $this->input->post('user_pay_method');
        $user_detail = $this->input->post('user_detail');

        for ($i = 0; $i < count($lease_detail_id); $i++) {
            $ls_detail_id = $lease_detail_id[$i];
            $lease_detail['payment_method'] = $payment_method[$i];
            $this->rent_model->update_function('lease_detail_id', $ls_detail_id, 'lease_detail', $lease_detail);

            $user_id = $user_id[$i];
            $user_data['email'] = $email[$i];
            $this->rent_model->update_function('user_id', $user_id, 'user', $user_data);

            $user_detail_id_val = $user_detail_id[$i];
            if ($lease_detail['payment_method'] == 1 || $lease_detail['payment_method'] == 3 || $lease_detail['payment_method'] == 5) {
                $user_detail_data['user_nick_name'] = $user_nick_name[$i];
                $user_detail_data['user_account_name'] = $user_account_name[$i];
                $user_detail_data['user_bsb'] = $user_bsb[$i];
                $user_detail_data['user_account_number'] = $user_account_number[$i];
            } else {
                $user_detail_data['user_nick_name'] = $user_nick_name_new[$i];
                $user_detail_data['user_account_name'] = $user_account_name_new[$i];
                $user_detail_data['user_bsb'] = $user_bsb_new[$i];
                $user_detail_data['user_account_number'] = $user_account_number_new[$i];
            }

            $user_detail_data['user_send_invoice'] = $user_send_invoice[$i];
            $user_detail_data['user_payable_to'] = $user_payable_to[$i];
            $user_detail_data['user_deliver_to'] = $user_deliver_to[$i];
            $user_detail_data['user_pay_method'] = $user_pay_method[$i];
            $user_detail_data['user_detail'] = $user_detail[$i];
            $this->rent_model->update_function('user_detail_id', $user_detail_id_val, 'user_detail', $user_detail_data);
        }
        redirect('invoiceSetting/' . $property_id);
    }

    public function print_tenant_ledger()
    {
        $this->load->view('print_tenant_ledger');
    }

    public function add_money_in_payment()
    {
        $money_in_id = $this->input->post('money_in_id');
        $money_in = $this->rent_model->getMoneyIn($money_in_id);

        if ($money_in && count($money_in) > 0) {

            //insert
            $money_in_cash_receive_amount_data['money_in_id'] = $money_in_id;
            $money_in_cash_receive_amount_data['lease_id'] = $money_in['lease_id'];
            $money_in_receiving_date_new_obj = date_create_from_format('d M Y', $this->input->post('money_in_receiving_date_new'));
            $money_in_cash_receive_amount_data['money_in_receiving_date_new'] = date_format($money_in_receiving_date_new_obj, "Y-m-d");
            $money_in_cash_receive_amount_data['cash_receiving_amount'] = $this->input->post('cash_receiving_amount');
            $money_in_cash_receive_amount_data['money_in_receiving_method_new'] = $this->input->post('money_in_receiving_method_new');
            $this->db->insert('money_in_cash_receive_amount', $money_in_cash_receive_amount_data);

            //----------------------------------------------------------------------------------------------------------
            $owe = $money_in['money_in_paid_amount'] - $money_in['partial_receiving_amount'];

            $money_in_data['money_in_amount_recieved_by'] = $money_in['money_in_amount_recieved_by'];
            if ($money_in_cash_receive_amount_data['cash_receiving_amount'] >= $owe) {
                $money_in_data['money_in_amount_recieved_by'] = 1;
            } else if ($money_in_cash_receive_amount_data['cash_receiving_amount'] < $owe) {
                $money_in_data['money_in_amount_recieved_by'] = 2;
            }

            //update
            $money_in_data['money_in_receiving_date'] = $money_in_cash_receive_amount_data['money_in_receiving_date_new'];
            $money_in_data['partial_receiving_amount'] = (float)$money_in['partial_receiving_amount'] + (float)$money_in_cash_receive_amount_data['cash_receiving_amount'];
            $this->db->where('money_in_id', $money_in_id);
            $this->db->update('money_in', $money_in_data);

            $this->session->set_flashdata('success', "success");
            $this->session->set_flashdata('money_in_pay_success', "Successfully paid");

            $this->sendMoneyInPaidEmail($money_in_id);

            redirect('moneyIn/' . $money_in['property_id']);
        }
    }

    public function print_money_in_invoice($money_in_id)
    {
        $money_in = $this->rent_model->getMoneyIn($money_in_id);

        //echo "<pre>";
        //print_r($money_in);
        //echo "</pre>";
        //die();

        if ($money_in && count($money_in) > 0) {

            $html = $this->prepare_money_in_invoice_html($money_in);

            $this->prepare_money_in_invoice_pdf($html, 'print');
        }
    }

    private function prepare_money_in_invoice_html($money_in)
    {
        $data['property_info'] = $this->rent_model->select_with_where('*', 'property_id=' . $money_in['property_id'] . '', 'property');
        $data['lease_detail'] = $this->property_model->select_with_where('*', 'property_id=' . $money_in['property_id'] . ' AND lease_current_status=1', 'lease');

        $data['money_in_with_receive_amounts'] = $this->rent_model->getMoneyInWithReceiveAmounts($money_in['money_in_id']);

        //cho "<pre>";
        //rint_r($data);
        //cho "</pre>";
        //ie();

        $html = $this->load->view('money_in_invoice_html', $data, true);
        return $html;
    }

    private function prepare_money_in_invoice_pdf($html, $email_or_print)
    {
        $this->load->library('MPDF/mpdf');

        $mpdf = new mPDF('win-1252', 'A4', '', '', 5, 5, 16, 60, 5, 5, 'C');
        $mpdf->useOnlyCoreFonts = true;    // false is default
        $mpdf->SetProtection(array('print'));
        $mpdf->SetTitle('Invoice');
        $mpdf->SetAuthor('Rent simple');
        $mpdf->SetDisplayMode('fullpage');

        $mpdf->debug = true;
        $filename = 'money_in_invoice' . rand(10000, 99999) . '.pdf';
        $mpdf->WriteHTML($html);
        $content = '';

        if ($email_or_print == 'email') {
            $mpdf->Output('uploads/message_document/' . $filename, 'F');
            $content = $mpdf->Output($filename, 'S');
        } else if ($email_or_print == 'print') {
            $mpdf->Output($filename, 'I');
            exit;
        }


        return array('single_pdf_content' => $content, 'pdf_file_name' => $filename);
    }

    public function send_money_in_invoice($money_in_id)
    {
        $site_name = $this->config->item('site_name');
        $base = $this->config->base_url();
        $img_src = "{$base}assets/img/logo.png";
        $img = "<img src='$img_src'><br>";

        $money_in_with_receive_amounts = $this->rent_model->getMoneyInWithReceiveAmounts($money_in_id);
        $full_property_address = $this->utility_model->getFullPropertyAddress($money_in_with_receive_amounts['property_id']);

        if (empty($money_in_with_receive_amounts)) {
            return false;
        }

        $property_with_landlord = $this->utility_model->getPropertyWithLandlord($money_in_with_receive_amounts['property_id']);

        $landlord_name = "";
        $landlord_phone = "";
        if (!empty($property_with_landlord)) {
            $landlord_name = $property_with_landlord['user_fname'] . ' ' . $property_with_landlord['user_lname'];
            $landlord_phone = $property_with_landlord['phone'];
        }

        $tenants = $this->property_model->get_selected_tenant($money_in_with_receive_amounts['property_id'], $money_in_with_receive_amounts['lease_id']);

        $tenant_names = "";
        if (!empty($tenants)) {
            $user_fname = array_column($tenants, 'user_fname');
            $user_lname = array_column($tenants, 'user_lname');

            $tenant_names = "";
            $tenant_names_array = (array_map("self::joinNameParts", $user_fname, $user_lname));

            if (count($tenant_names_array) > 0) {
                $tenant_names = implode(' & ', $tenant_names_array);
            }
        }

        $dollar_sign = "$";
        $due_date = empty($money_in_with_receive_amounts['money_in_amount_due_on']) || $money_in_with_receive_amounts['money_in_amount_due_on'] == "1970-01-01" ? "Not Available" : date("M d, Y", strtotime($money_in_with_receive_amounts['money_in_amount_due_on']));
        $amount = number_format($money_in_with_receive_amounts['money_in_paid_amount'], 2, '.', ',');

        $received_amount = $this->rent_model->getTotalMoneyInReceivedAmount($money_in_id);
        $received_amount = number_format($received_amount, 2, '.', ',');

        $status = "";

        if ($money_in_with_receive_amounts['money_in_amount_recieved_by'] == 1) {
            $status = "Fully Paid";
        } else if ($money_in_with_receive_amounts['money_in_amount_recieved_by'] == 2) {
            $status = "Partially Paid";
        } else if ($money_in_with_receive_amounts['money_in_amount_recieved_by'] == 3) {
            $status = "Not Paid";
        }

        if ($money_in_with_receive_amounts && count($money_in_with_receive_amounts) > 0 && $tenants && count($tenants) > 0) {

            $html = $this->prepare_money_in_invoice_html($money_in_with_receive_amounts);
            $pdf = $this->prepare_money_in_invoice_pdf($html, 'email');

            foreach ($tenants as $tenant) {

                $message = "";
                $message .= "Hi {$tenant_names} ,<br><br>
                            You have an invoice for Other Money In/Owed. Please find the details below:<br>
                            --------------------------------- <br>
                            <b>Address:</b> {$full_property_address} <br>
                            <b>Tenants:</b> {$tenant_names}<br>
                            <b>Due Date:</b> {$due_date} <br>                               
                            <b>Invoice Amount:</b> {$dollar_sign}{$amount} <br>                                    
                            <b>Received Amount:</b> {$dollar_sign}{$received_amount} <br>   
                            <b>Notes:</b> {$money_in_with_receive_amounts['money_in_description']}<br>
                            <b>Status:</b> {$status}<br>                                 
                            --------------------------------- <br><br>
                            Thank you<br><br>
                            If you have any questions in relation to this
                            invoice please contact {$landlord_name} on {$landlord_phone}<br>
                            This email has been generated by {$site_name} Software.<br><br>
                            {$img}";

                $subject = "You have an invoice for Other Money In/Owed";

                $mail_data['to'] = $tenant['email'];
                $mail_data['subject'] = $subject;
                $mail_data['message'] = $message;
                $mail_data['single_pdf_content'] = $pdf['single_pdf_content'];
                $mail_data['pdf_file_name'] = $pdf['pdf_file_name'];

                $this->utility_model->insertAsMessage(null, $money_in_with_receive_amounts['property_id'], $money_in_with_receive_amounts['lease_id'], array($tenant['user_id']), $subject, $message, array($pdf['pdf_file_name']));

                $this->sendEmail($mail_data);
            }

            //----------------------to landlord-----------------------------------------------------------------------------
            $subject = "Invoice entered for {$full_property_address}";
            $message = '';
            $message .= "Hi {$landlord_name} ,<br>
                         This email is to confirm you have entered an invoice (Other Money In) against the following tenancy:<br>                         
                         --------------------------------- <br>
                         <b>Address:</b> {$full_property_address} <br>
                         <b>Tenants:</b> {$tenant_names}<br>
                         <b>Due Date:</b> {$due_date} <br>                               
                         <b>Invoice Amount:</b> {$dollar_sign}{$amount} <br>                                    
                         <b>Received Amount:</b> {$dollar_sign}{$received_amount} <br>                                      
                         <b>Notes:</b> {$money_in_with_receive_amounts['money_in_description']}<br>
                         <b>Status:</b> {$status}<br>
                         --------------------------------- <br><br>                       
                         Thank you <br><br>
                       
                         This email has been generated by {$site_name} Software.<br><br>
                         {$img}
                         ";


            $mail_data = array();
            $mail_data['to'] = $property_with_landlord['email'];
            $mail_data['subject'] = $subject;
            $mail_data['message'] = $message;

            $this->sendEmail($mail_data);


            //--------------------------------------------------------------------------------------------------------------

            $this->session->set_flashdata('success', "success");
            $this->session->set_flashdata('mail_send_success', "mail_send_successs");

            //exit;

            redirect('moneyIn/' . $money_in_with_receive_amounts['property_id']);
        }
    }


    public function sendMoneyInCreatedEmail($money_in_id)
    {
        $site_name = $this->config->item('site_name');
        $base = $this->config->base_url();
        $img_src = "{$base}assets/img/logo.png";
        $img = "<img src='$img_src'><br>";

        $money_in_with_receive_amounts = $this->rent_model->getMoneyInWithReceiveAmounts($money_in_id);

        if (empty($money_in_with_receive_amounts)) {
            return false;
        }

        $property_with_landlord = $this->utility_model->getPropertyWithLandlord($money_in_with_receive_amounts['property_id']);
        $lease = $this->utility_model->getLease($money_in_with_receive_amounts['lease_id']);
        $tenants_with_lease_detail = $this->utility_model->getTenantsWithLeaseDetails($money_in_with_receive_amounts['lease_id']);

        $full_property_address = $this->utility_model->getFullPropertyAddress($money_in_with_receive_amounts['property_id']);

        $landlord_name = "";
        $landlord_phone = "";
        if (!empty($property_with_landlord)) {
            $landlord_name = $property_with_landlord['user_fname'] . ' ' . $property_with_landlord['user_lname'];
            $landlord_phone = $property_with_landlord['phone'];
        }
        $tenant_names = "";
        if (!empty($tenants_with_lease_detail)) {
            $user_fname = array_column($tenants_with_lease_detail, 'user_fname');
            $user_lname = array_column($tenants_with_lease_detail, 'user_lname');

            $tenant_names = "";
            $tenant_names_array = (array_map("self::joinNameParts", $user_fname, $user_lname));

            if (count($tenant_names_array) > 0) {
                $tenant_names = implode(' & ', $tenant_names_array);
            }
        }
        $dollar_sign = "$";
        $due_date = empty($money_in_with_receive_amounts['money_in_amount_due_on']) || $money_in_with_receive_amounts['money_in_amount_due_on'] == "1970-01-01" ? "Not Available" : date("M d, Y", strtotime($money_in_with_receive_amounts['money_in_amount_due_on']));
        $amount = number_format($money_in_with_receive_amounts['money_in_paid_amount'], 2, '.', ',');

        $received_amount = $this->rent_model->getTotalMoneyInReceivedAmount($money_in_id);
        $received_amount = number_format($received_amount, 2, '.', ',');


        $payment_section = "<u>Payments</u><br><br>";
        $payment_tr = "";

        $status = "";

        if ($money_in_with_receive_amounts['money_in_amount_recieved_by'] == 1) {
            $status = "Fully Paid";
        } else if ($money_in_with_receive_amounts['money_in_amount_recieved_by'] == 2) {
            $status = "Partially Paid";
        } else if ($money_in_with_receive_amounts['money_in_amount_recieved_by'] == 3) {
            $status = "Not Paid";
        }


        if (count($money_in_with_receive_amounts['money_in_cash_receive_amounts']) > 0) {
            foreach ($money_in_with_receive_amounts['money_in_cash_receive_amounts'] as $money_in_cash_receive_amount) {
                $r_date = date('M d,Y', strtotime($money_in_cash_receive_amount['money_in_receiving_date_new']));
                $r_amt = number_format($money_in_cash_receive_amount['cash_receiving_amount'], 2, '.', ',');
                $r_method = $money_in_cash_receive_amount['money_in_receiving_method_new'];
                $payment_tr .= "<tr style='border: 1px solid black;border-collapse: collapse;padding:5px'>
                                            <td style='border: 1px solid black;border-collapse: collapse;padding:5px'>{$r_date}</td>
                                            <td style='border: 1px solid black;border-collapse: collapse;padding:5px'>{$dollar_sign}{$r_amt}</td>
                                            <td style='border: 1px solid black;border-collapse: collapse;padding:5px'>{$r_method}</td>
                                       </tr>";
            }
            $payment_section .= "<table style='border: 1px solid black;border-collapse: collapse;padding:5px'>
                                            <thead>
                                                <tr style='border: 1px solid black;border-collapse: collapse;padding:5px'>
                                                  <td style='border: 1px solid black;border-collapse: collapse;padding:5px'>Received Date</td>
                                                  <td style='border: 1px solid black;border-collapse: collapse;padding:5px'>Amount</td>
                                                  <td style='border: 1px solid black;border-collapse: collapse;padding:5px'>Payment Method</td>
                                                </tr>
                                            </thead>
                                            <tbody>{$payment_tr}</tbody>                                
                                        </table><br>";
        } else {
            $payment_section .= "<br>No current payments.<br>";
        }

        //----------------------to tenants------------------------------------------------------------------------------
        $make_payment_str = $received_amount == 0.00 ? "Please make payment directly to {$landlord_name}" : "";
        $subject = "You have an invoice for Other Money In/Owed";
        $message = '';
        $message .= "Hi  {$tenant_names} ,<br><br>
                         You have been invoiced for Other Money In/Owed. Please find the details below:<br>                         
                         --------------------------------- <br>
                         <b>Address:</b> {$full_property_address} <br>
                         <b>Tenants:</b> {$tenant_names}<br>
                         <b>Due Date:</b> {$due_date} <br>                               
                         <b>Invoice Amount:</b> {$dollar_sign}{$amount} <br>                                    
                         <b>Received Amount:</b> {$dollar_sign}{$received_amount} <br>   
                         <b>Notes:</b> {$money_in_with_receive_amounts['money_in_description']}<br>
                         <b>Status:</b> {$status}<br>                                 
                         --------------------------------- <br><br>
                         {$payment_section}<br>
                         Thank you <br><br>
                         {$make_payment_str} If you have any questions in relation to this
                         invoice please contact {$landlord_name} on {$landlord_phone}<br>
                         This email has been generated by {$site_name} Software.<br><br>
                         {$img}
                         ";

        foreach ($tenants_with_lease_detail as $tenant_with_lease_detail) {

            $mail_data = array();
            $mail_data['to'] = $tenant_with_lease_detail['email'];
            $mail_data['subject'] = $subject;
            $mail_data['message'] = $message;


            $this->utility_model->insertAsMessage(null, $money_in_with_receive_amounts['property_id'], $money_in_with_receive_amounts['lease_id'], array($tenant_with_lease_detail['user_id']), $subject, $message, null);

            $this->sendEmail($mail_data);

        }
        //--------------------------------------------------------------------------------------------------------------


        //----------------------to landlord-----------------------------------------------------------------------------
        $subject = "Invoice entered for {$full_property_address}";
        $message = '';
        $message .= "Hi {$landlord_name} ,<br>
                         This email is to confirm you have entered an invoice (Other Money In) against the following tenancy:<br>                         
                         --------------------------------- <br>
                         <b>Address:</b> {$full_property_address} <br>
                         <b>Tenants:</b> {$tenant_names}<br>
                         <b>Due Date:</b> {$due_date} <br>                               
                         <b>Invoice Amount:</b> {$dollar_sign}{$amount} <br>                                    
                         <b>Received Amount:</b> {$dollar_sign}{$received_amount} <br>                                      
                         <b>Notes:</b> {$money_in_with_receive_amounts['money_in_description']}<br>
                         <b>Status:</b> {$status}<br>
                         --------------------------------- <br><br>
                         {$payment_section}
                         Thank you <br><br>
                       
                         This email has been generated by {$site_name} Software.<br><br>
                         {$img}
                         ";


        $mail_data = array();
        $mail_data['to'] = $property_with_landlord['email'];
        $mail_data['subject'] = $subject;
        $mail_data['message'] = $message;

        $this->utility_model->insertAsMessage(null, $money_in_with_receive_amounts['property_id'], $money_in_with_receive_amounts['lease_id'], array($tenant_with_lease_detail['user_id']), $subject, $message, null);

        $this->sendEmail($mail_data);


        //--------------------------------------------------------------------------------------------------------------

    }

    public function sendMoneyInPaidEmail($money_in_id)
    {
        $site_name = $this->config->item('site_name');
        $base = $this->config->base_url();
        $img_src = "{$base}assets/img/logo.png";
        $img = "<img src='$img_src'><br>";

        $money_in_with_receive_amounts = $this->rent_model->getMoneyInWithReceiveAmounts($money_in_id);

        if (empty($money_in_with_receive_amounts)) {
            exit;
        }

        $property_with_landlord = $this->utility_model->getPropertyWithLandlord($money_in_with_receive_amounts['property_id']);
        $lease = $this->utility_model->getLease($money_in_with_receive_amounts['lease_id']);
        $tenants_with_lease_detail = $this->utility_model->getTenantsWithLeaseDetails($money_in_with_receive_amounts['lease_id']);

        $full_property_address = $this->utility_model->getFullPropertyAddress($money_in_with_receive_amounts['property_id']);

        $landlord_name = "";
        $landlord_phone = "";
        if (!empty($property_with_landlord)) {
            $landlord_name = $property_with_landlord['user_fname'] . ' ' . $property_with_landlord['user_lname'];
            $landlord_phone = $property_with_landlord['phone'];
        }
        $tenant_names = "";
        if (!empty($tenants_with_lease_detail)) {
            $user_fname = array_column($tenants_with_lease_detail, 'user_fname');
            $user_lname = array_column($tenants_with_lease_detail, 'user_lname');

            $tenant_names = "";
            $tenant_names_array = (array_map("self::joinNameParts", $user_fname, $user_lname));

            if (count($tenant_names_array) > 0) {
                $tenant_names = implode(' & ', $tenant_names_array);
            }
        }
        $dollar_sign = "$";
        $due_date = date("M d, Y", strtotime($money_in_with_receive_amounts['money_in_receiving_date']));
        $amount = number_format($money_in_with_receive_amounts['money_in_paid_amount'], 2, '.', ',');


        $received_amount = $this->rent_model->getTotalMoneyInReceivedAmount($money_in_id);
        $received_amount = number_format($received_amount, 2, '.', ',');

        $payment_section = "<u>Payments</u><br><br>";
        $payment_tr = "";

        $status = "";

        if ($money_in_with_receive_amounts['money_in_amount_recieved_by'] == 1) {
            $status = "Fully Paid";
        } else if ($money_in_with_receive_amounts['money_in_amount_recieved_by'] == 2) {
            $status = "Partially Paid";
        } else if ($money_in_with_receive_amounts['money_in_amount_recieved_by'] == 3) {
            $status = "Not Paid";
        }

        if (count($money_in_with_receive_amounts['money_in_cash_receive_amounts']) > 0) {
            foreach ($money_in_with_receive_amounts['money_in_cash_receive_amounts'] as $money_in_cash_receive_amount) {
                $r_date = date('M d,Y', strtotime($money_in_cash_receive_amount['money_in_receiving_date_new']));
                $r_amt = number_format($money_in_cash_receive_amount['cash_receiving_amount'], 2, '.', ',');
                $r_method = $money_in_cash_receive_amount['money_in_receiving_method_new'];
                $payment_tr .= "<tr style='border: 1px solid black;border-collapse: collapse;padding:5px'>
                                            <td style='border: 1px solid black;border-collapse: collapse;padding:5px'>{$r_date}</td>
                                            <td style='border: 1px solid black;border-collapse: collapse;padding:5px'><span>$</span>{$r_amt}</td>
                                            <td style='border: 1px solid black;border-collapse: collapse;padding:5px'>{$r_method}</td>
                                       </tr>";
            }
            $payment_section .= "<table style='border: 1px solid black;border-collapse: collapse;padding:5px'>
                                            <thead>
                                                <tr style='border: 1px solid black;border-collapse: collapse;padding:5px'>
                                                  <td style='border: 1px solid black;border-collapse: collapse;padding:5px'>Received Date</td>
                                                  <td style='border: 1px solid black;border-collapse: collapse;padding:5px'>Amount</td>
                                                  <td style='border: 1px solid black;border-collapse: collapse;padding:5px'>Payment Method</td>
                                                </tr>
                                            </thead>
                                            <tbody>{$payment_tr}</tbody>                                
                                        </table><br>";

        } else {
            $payment_section .= "<br>No current payments";
        }

        //----------------------to tenants------------------------------------------------------------------------------
        $subject = "Other Money In/Owed has been receipted";
        $message = '';
        $message .= "Hi {$tenant_names} ,<br>
                        This email is to confirm your Landlord {$landlord_name} has receipted a payment against your tenancy<br>                         
                         --------------------------------- <br>
                         <b>Address:</b> {$full_property_address} <br>
                         <b>Tenants:</b> {$tenant_names}<br>
                         <b>Due Date:</b> {$due_date} <br>                               
                         <b>Invoice Amount:</b> {$dollar_sign}{$amount} <br>                                    
                         <b>Received Amount:</b> {$dollar_sign}{$received_amount} <br>   
                         <b>Notes:</b> {$money_in_with_receive_amounts['money_in_description']}<br>
                         <b>Status:</b> {$status}<br>                                 
                         --------------------------------- <br><br>
                         {$payment_section}<br>
                         Thank you <br><br>
                         If you have any questions in relation to this
                         invoice please contact {$landlord_name} on {$landlord_phone}<br>
                         This email has been generated by {$site_name} Software.<br><br>
                         {$img}
                         ";

        foreach ($tenants_with_lease_detail as $tenant_with_lease_detail) {

            $mail_data = array();
            $mail_data['to'] = $tenant_with_lease_detail['email'];
            $mail_data['subject'] = $subject;
            $mail_data['message'] = $message;

            $this->utility_model->insertAsMessage(null, $money_in_with_receive_amounts['property_id'], $money_in_with_receive_amounts['lease_id'], array($tenant_with_lease_detail['user_id']), $subject, $message, null);

            $this->sendEmail($mail_data);

        }
        //--------------------------------------------------------------------------------------------------------------


        //----------------------to landlord-----------------------------------------------------------------------------
        $subject = "Receipt for money received " . date("M d,Y");
        $message = '';
        $message .= "Hi {$landlord_name} ,<br>
                         This email is to confirm you have entered a payment (Other Money In) against the following tenancy:<br>                         
                         --------------------------------- <br>
                         <b>Address:</b> {$full_property_address} <br>
                         <b>Tenants:</b> {$tenant_names}<br>
                         <b>Due Date:</b> {$due_date} <br>                               
                         <b>Invoice Amount:</b> {$dollar_sign}{$amount} <br>                                    
                         <b>Received Amount:</b> {$dollar_sign}{$received_amount} <br>                                      
                         <b>Notes:</b> {$money_in_with_receive_amounts['money_in_description']}<br>
                         <b>Status:</b> {$status}<br>
                         --------------------------------- <br><br>
                         {$payment_section}
                         Thank you <br><br>
                       
                         This email has been generated by {$site_name} Software.<br><br>
                         {$img}
                         ";


        $mail_data = array();
        $mail_data['to'] = $property_with_landlord['email'];
        $mail_data['subject'] = $subject;
        $mail_data['message'] = $message;

        $this->utility_model->insertAsMessage(null, $money_in_with_receive_amounts['property_id'], $money_in_with_receive_amounts['lease_id'], array($tenant_with_lease_detail['user_id']), $subject, $message, null);

        $this->sendEmail($mail_data);
        //--------------------------------------------------------------------------------------------------------------


    }


    public
    function ggmail()
    {
        $site_name = $this->config->item('site_name');
        $site_email = $this->config->item('site_email');

        try {
            $this->email->clear(TRUE);
            $this->email->initialize(array('priority' => 1));

            //$mail_data['to'] = 'mahmud@sahajjo.com';

            if ($this->config->item('is_mail_smtp')) {
                $config = $this->config->item('smtp_config');
                $this->email->initialize($config);
            }


            $this->email->from($site_email, $site_name);
            $this->email->to('gg@gg.com');
            $this->email->bcc('mahmud@sahajjo.com');

            $this->email->subject('good subgetc');
            $this->email->message('good message iok887');
            $this->email->set_mailtype("html");


            //echo '<hr>' . '<br>';
            //echo $mail_data['subject'] . '<br>';
            //echo $mail_data['message'], '<br>';
            //echo '<hr>' . '<br>';
            //echo "<pre>";print_r($mail_data);"</pre><br><hr>";

            $this->email->send();

            /*$headers = 'From: '.$site_email.'\r\n';
            mail(mail_data['to'],$mail_data['subject'],$mail_data['message'],$headers);*/

        } catch (Exception $e) {
            echo $e->getMessage();
        }

    }


}
