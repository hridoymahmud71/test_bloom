<?php $this->load->view('front/headlink'); ?>
<?php $this->load->view('front/top_menu'); ?>
<body>
<div class="setup_multistape">
    <div class="container">
        <?php $this->load->view('front/head_nav'); ?>
        <div class="ss_schedule">
            <h3 class="extra_heading">Rent Schedule
                <small> for the property at
                    <?= $this->utility_model->getFullPropertyAddress($property_info[0]['property_id'])?>
                </small>
            </h3>
            <?php $today = date('Y-m-d'); ?>
            <?php if ($this->session->flashdata('success')) { ?>
                <div class="panel panel-primary">
                    <div class="panel-heading">Successful!</div>
                    <div class="panel-body"><?= $this->session->flashdata('mail_send_success') ? "Email sent successfully" : ""; ?></div>
                </div>
            <?php } ?>



            <?php
            $status_array = array();
            $status_array[0] = '';
            $status_array[1] = 'up to date';
            $status_array[2] = 'arrears';
            $status_array[3] = 'ahead';
            $status_array[4] = 'arrears';

            $status_label_array = array();
            $status_label_array[0] = 'primary';
            $status_label_array[1] = 'success';
            $status_label_array[2] = 'danger';
            $status_label_array[3] = 'success';
            $status_label_array[4] = 'danger';

            $payment_method[1] = "Bank Transfer ";
            $payment_method[2] = "Cash";
            $payment_method[3] = "Cheque";
            $payment_method[4] = "Credit Card";
            $payment_method[4] = "Other method";

            ?>

            <?php
            function joinParts($fname, $lname)
            {
                return $fname . ' ' . $lname;
            }

            $first_names = array_column($get_tenant_list, 'user_fname');
            $last_names = array_column($get_tenant_list, 'user_lname');

            $names_array = (array_map("joinParts", $first_names, $last_names));
            $names = "";
            if (count($names_array) > 0) {
                $names = implode(' & ', $names_array);
            }
            $payment_update_by_sum = array_sum(array_column($get_tenant_list, 'payment_update_by'));
            ?>


            <div class="col-md-6">

                <div class="row">
                    <p class="tenant_box">
                    <div class="col-md-6">
                        <strong>
                            <?= $names ?>
                        </strong>
                    </div>
                    <div class="col-md-6">
                        <?php $rent_status = $this->utility_model->calculate_rent_status_by_lease($lease_id); ?>

                        <?php if (!empty($rent_status)) { ?>
                            <span id="tenant_payment_stats"
                                  class="label label-<?= $rent_status['label'] ?>">
                               <?= $rent_status['amount'] != 0 ? "$" . $rent_status['amount'] : "" ?>
                               <?= $rent_status['status'] ?>
                        </span>
                        <?php } ?>
                    </div>
                    </p>
                </div>


            </div>
            <div class="col-md-6">
                <div class="ss_enter_rent">
                    <!--<a data-toggle="modal" href="#recive_rent" id="mark_as_paid" class="btn btn-light btn-lg mark_paid">Enter Received Rent Now</a>-->
                    <?php if (!$is_archived) { ?>
                        <a data-toggle="modal" href="#mono_receive_rent" id="mono_mark_as_paid"
                           class="btn btn-light btn-lg">Enter Received Rent Now</a>
                    <?php } ?>
                    <a data-toggle="modal" data-target="#printshare" href="javascript:void(0);"
                       class="btn btn-light btn-lg">
                        Print Ledgers &amp; Schedules
                    </a>


                    <div class="row">
                        <div class="col-md-6">
                            <a download
                               href="statement/<?= $lease_id ?>?range=<?= "{$minimum_date_of_income_expense}to{$today}&mode=show" ?>"
                               class="btn btn-light " style="width: 100%">Show Statement</a>
                        </div>
                        <div class="col-md-6">
                            <a href="statement/<?= $lease_id ?>?range=<?= "{$minimum_date_of_income_expense}to{$today}&mode=email" ?>"
                               class="btn btn-light " style="width: 100%">Email Statement</a>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-6">
                            <div class="btn-group" role="group" style="width: 100%">
                                <button id="btnGroupDrop1 " type="button" class="btn btn-light dropdown-toggle"
                                        style="width: 100%"
                                        data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    Monthly Statements<br>Show &darr;
                                </button>

                                <div class="dropdown-menu" aria-labelledby="btnGroupDrop1" style="width: 100%">
                                    <?php if (!empty($statement_month_list)) { ?>
                                        <?php foreach ($statement_month_list as $statement_month_item) { ?>

                                            <a download
                                               href="statement/<?= $lease_id ?>?month=<?= $statement_month_item ?>&mode=show"
                                               class="dropdown-item btn btn-secondary month-list"><?= date('F, Y', strtotime($statement_month_item . "-01")) ?></a>

                                        <?php } ?>
                                    <?php } ?>

                                    <?php if (empty($statement_month_list)) { ?>
                                        <h4 style="font-style: italic">No Monthly Statements</h4>
                                    <?php } ?>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="btn-group" role="group" style="width: 100%">
                                <button id="btnGroupDrop2" type="button" class="btn btn-light dropdown-toggle"
                                        style="width: 100%"
                                        data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    Monthly Statements<br>Email &darr;
                                </button>

                                <div class="dropdown-menu" aria-labelledby="btnGroupDrop2" style="width: 100%">
                                    <?php if (!empty($statement_month_list)) { ?>
                                        <?php foreach ($statement_month_list as $statement_month_item) { ?>

                                            <a href="statement/<?= $lease_id ?>?month=<?= $statement_month_item ?>&mode=email"
                                               class="dropdown-item btn btn-secondary month-list"><?= date('F, Y', strtotime($statement_month_item . "-01")) ?></a>

                                        <?php } ?>
                                    <?php } ?>

                                    <?php if (empty($statement_month_list)) { ?>
                                        <h4 style="font-style: italic">No Monthly Statements</h4>
                                    <?php } ?>
                                </div>
                            </div>
                        </div>
                    </div>


                </div>
            </div>


            <div class="col-md-12">
                <div class="ss_views_by rs_views_by" style="margin-top: 10px">

                    <ul>

                        <li <?php if ($rent_recieve_show == 1) {
                            echo "class='active'";
                        } ?>><a href="#rentreceived" aria-controls="rentreceived" role="tab" data-toggle="tab">Rent
                                Received</a></li>
                        <li>
                            <a href="#waterreeived" aria-controls="waterreeived" role="tab" data-toggle="tab">
                                Water Received
                            </a>
                        </li>
                        <li>
                            <a href="#moneyinreceived" aria-controls="moneyinreceived" role="tab" data-toggle="tab">
                                Money In Received
                            </a>
                        </li>
                        <li <?php if ($rent_recieve_show == 0) {
                            echo "class='active'";
                        } ?>><a href="#periodschedule" aria-controls="periodschedule" role="tab" data-toggle="tab">Period
                                Schedule</a></li>
                    </ul>
                </div>
            </div>

            <div class="payment_table">
                <div class="tab-content">
                    <div role="tabpanel" class="tab-pane <?php if ($rent_recieve_show == 1) {
                        echo "active";
                    } ?>" id="rentreceived">

                        <!-- ---------- payment log <starts> ---------- -->
                        <?php if (count($lease_log_payment_lease_list) > 0) { ?>
                            <h2>Rent Received</h2>
                            <table class="table table-bordered table-hover" style="font-size: 15px;">
                                <thead style="background-color: white">
                                <tr>
                                    <td style="text-align:center;background-color: #0bafd2;font-weight:bold">Amount
                                        Received
                                    </td>
                                    <td style="text-align:center;background-color: #0bafd2;font-weight:bold">Date
                                        Received
                                    </td>
                                    <td style="text-align:center;background-color: #0bafd2;font-weight:bold">Payment
                                        Method
                                    </td>
                                    <td style="text-align:center;background-color: #0bafd2;font-weight:bold">Rental
                                        Periods
                                    </td>
                                </tr>
                                </thead>
                                <tbody>
                                <?php foreach ($lease_log_payment_lease_list as $lease_log_payment_lease_item) { ?>
                                    <tr style="border-bottom:2px solid black">
                                        <td style="text-align: center">
                                            <span>$</span><?= $lease_log_payment_lease_item['lease_log_payment_amount_recieved'] ?>
                                            <p style="word-break: break-all">
                                                ( <?= $this->utility_model->get_rent_payment_ref($lease_log_payment_lease_item['lease_log_payment_id']) ?>
                                                )</p></td>
                                        <td style="text-align: center"><?= date('d/m/y', strtotime($lease_log_payment_lease_item['lease_log_payment_time'])) ?></td>
                                        <td style="text-align: center">
                                            <?php if (array_key_exists($lease_log_payment_lease_item['lease_log_payment_method'], $payment_method)) {
                                                echo $payment_method[$lease_log_payment_lease_item['lease_log_payment_method']];
                                            } ?>
                                        </td>
                                        <td style="text-align: center">
                                            <?php if (count($lease_log_payment_lease_item['log_payment_details_with_schedule_list']) > 0) { ?>
                                                <table class="table table-bordered table-hover"
                                                       style="font-size: 15px;">
                                                    <thead style="background-color: white">
                                                    <tr>
                                                        <td style="text-align: center">Period
                                                        </td>
                                                        <td style="text-align: center">Amount received
                                                        </td>
                                                        <td style="text-align: center">Due
                                                        </td>
                                                    </tr>
                                                    </thead>
                                                    <tbody>
                                                    <?php foreach ($lease_log_payment_lease_item['log_payment_details_with_schedule_list'] as $log_payment_details_with_schedule_item) { ?>
                                                        <tr>
                                                            <td style="text-align: center">
                                                                <?= date('d/m/y', strtotime($log_payment_details_with_schedule_item['payment_start_period'])) ?>
                                                                <br>to<br>
                                                                <?= date('d/m/y', strtotime($log_payment_details_with_schedule_item['payment_end_period'])) ?>
                                                            </td>
                                                            <td style="text-align: center">
                                                                $<?= $log_payment_details_with_schedule_item['log_lease_rent_recieved'] ?>
                                                            </td>
                                                            <td style="text-align: center">
                                                                $<?= $log_payment_details_with_schedule_item['payment_due_amount'] ?>
                                                            </td>
                                                        </tr>
                                                    <?php } ?>
                                                    </tbody>
                                                </table>
                                            <?php } ?>
                                        </td>
                                    </tr>
                                <?php } ?>
                                </tbody>
                            </table>
                        <?php } ?>
                        <!-- ---------- payment log <ends> ---------- -->
                    </div>
                    <div role="tabpanel" class="tab-pane <?php if ($rent_recieve_show == 1) {
                        echo "active";
                    } ?>" id="waterreeived">

                        <!-- ---------- water <starts> ---------- -->
                        <?php if (count($water_invoices_with_payment_logs) > 0) { ?>
                            <h2>Water</h2>
                            <table class="table table-bordered table-hover" style="font-size: 15px;">
                                <thead style="background-color: white">
                                <tr>
                                    <th scope="col"
                                        style="text-align:center;background-color: #0bafd2;font-weight:bold">Period
                                    </th>
                                    <th scope="col"
                                        style="text-align:center;background-color: #0bafd2;font-weight:bold">Due Date
                                    </th>
                                    <th scope="col"
                                        style="text-align:center;background-color: #0bafd2;font-weight:bold">Amount
                                    </th>
                                    <th scope="col"
                                        style="text-align:center;background-color: #0bafd2;font-weight:bold">Paid /
                                        Outstanding
                                    </th>
                                    <td scope="col" style="text-align:center;background-color: #0bafd2;font-weight:bold"
                                        style="text-align:center;">Completed
                                    </td>
                                    <th scope="col"
                                        style="text-align:center;background-color: #0bafd2;font-weight:bold">Payments
                                    </th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php foreach ($water_invoices_with_payment_logs as $a_water_invoice_with_payment_logs) { ?>
                                    <tr style="border-bottom:2px solid black">
                                        <td style="text-align: center">
                                            <?= date("F, Y", strtotime($a_water_invoice_with_payment_logs['water_invoice_period_start'])); ?>
                                            <br>
                                            to
                                            <br>
                                            <?= date("F, Y", strtotime($a_water_invoice_with_payment_logs['water_invoice_period_end'])); ?>
                                        </td>
                                        <td style="text-align: center"><?= date("jS F, Y", strtotime($a_water_invoice_with_payment_logs['water_invoice_due_date'])); ?></td>
                                        <td style="text-align: center">
                                            $<?= $a_water_invoice_with_payment_logs['water_invoice_amount']; ?></td>
                                        <td style="text-align: center">
                                            $<?= $a_water_invoice_with_payment_logs['water_invoice_amount_paid']; ?> /
                                            $<?= $a_water_invoice_with_payment_logs['water_invoice_amount_due']; ?>
                                        </td>
                                        <td style="text-align: center">
                                            <?= $a_water_invoice_with_payment_logs['water_invoice_status'] == 1 ? "Yes" : "No"; ?>
                                        </td>
                                        <td>
                                            <?php if ($a_water_invoice_with_payment_logs['water_invoice_log']) { ?>

                                                <table class="table table-bordered table-hover">
                                                    <thead style="background-color: white">
                                                    <tr>
                                                        <th style="text-align: center" scope="col">Amount Paid</th>
                                                        <th style="text-align: center" scope="col">Date Paid</th>
                                                    </tr>
                                                    </thead>
                                                    <tbody>
                                                    <?php foreach ($a_water_invoice_with_payment_logs['water_invoice_log'] as $a_water_invoice_log) { ?>
                                                        <tr>
                                                            <td>
                                                                $<?= $a_water_invoice_log['water_invoice_log_amount']; ?>
                                                            </td>
                                                            <td>
                                                                <?= date("jS F, Y", strtotime($a_water_invoice_log['water_invoice_payment_date'])); ?>
                                                            </td>
                                                        </tr>
                                                    <?php } ?>
                                                    </tbody>
                                                </table>

                                            <?php } else { ?>
                                                <span style="text-align: center">No current payments</span>
                                            <?php } ?>
                                        </td>
                                    </tr>
                                <?php } ?>
                                </tbody>
                            </table>
                        <?php } ?>
                        <!-- ---------- water <ends> ---------- -->
                    </div>
                    <div role="tabpanel" class="tab-pane" id="moneyinreceived">

                        <?php if (!empty($all_tenant_payments_info)) { ?>
                            <br>
                            <h2>Money In</h2>
                            <div class="step_5_table">
                                <ul class="table_header top-header-div" style="background-color: #0bafd2!important;">
                                    <li style="text-align:center;font-weight:bold">Amount Received</li>
                                    <li style="text-align:center;font-weight:bold">Date Received</li>
                                    <li style="text-align:center;font-weight:bold">Payer</li>
                                    <li style="text-align:center;font-weight:bold">Paid by</li>
                                    <li style="text-align:center;font-weight:bold">Period(s)</li>
                                    <li style="text-align:center;font-weight:bold"> Actions</li>
                                </ul>
                                <?php foreach ($all_tenant_payments_info as $row) { ?>
                                    <ul class="table_row schedule_list">
                                        <li id="num">
                                        <span class="input_text" id="period_amount"
                                              style="display: inline;">$<?= $row['shared_amount'] ?></span>
                                            <input class="input-small form-control dollar_blue pay_period_amount"
                                                   type="Number" name="shared_amount[]"
                                                   value="<?= $row['shared_amount'] ?>"
                                                   style="display: none;">
                                        </li>
                                        <li>
                                            <h1>Recived Date</h1>
                                            <span class="input_text" id="due_date"
                                                  style="display: inline;"><?= date('M d', strtotime($row['schedule_start_date'])) ?></span>
                                            <div style="display:none" class="input-group input-small">
                                                <input class="form-control due_date" type="text"
                                                       name="schedule_start_date[]"
                                                       value="<?= date('M d', strtotime($row['schedule_start_date'])) ?>"
                                                       readonly>
                                                <span class="input-group-addon"><i
                                                            class="glyphicon glyphicon-calendar"></i></span>
                                            </div>
                                        </li>
                                        <li>
                                        <span class="input_text" id="Name_lastname"
                                              style="display: inline;"><?= $row['user_fname'] ?> <?= $row['user_lname'] ?></span>
                                            <select required class="input-small form-control tenant_id"
                                                    name="tenant_id[]"
                                                    style="display: none;">
                                                <option value="">Select Tenant</option>
                                                <?php foreach ($get_tenant_list as $row2) { ?>
                                                    <option <?php if ($row2['tenant_id'] == $row['tenant_id']) {
                                                        echo "selected";
                                                    } ?> value="<?= $row2['tenant_id'] ?>"><?= $row2['user_fname'] ?> <?= $row['user_lname'] ?></option>
                                                <?php } ?>
                                            </select>
                                            <!-- <input class="input-small dollar_blue pay_period_amount form-control" type="text"  value="<?= $row['user_fname'] ?> <?= $row['user_lname'] ?>" style="display: none;"> -->
                                        </li>
                                        <li>
                                        <span class="input_text" id="payment_mathod"
                                              style="display: inline;"><?php if ($row['payment_method'] == 1) {
                                                echo "Bank Transfer";
                                            } elseif ($row['payment_method'] == 2) {
                                                echo "cash";
                                            } elseif ($row['payment_method'] == 3) {
                                                echo "Cheque";
                                            } elseif ($row['payment_method'] == 4) {
                                                echo "credit card";
                                            } else {
                                                echo "Other method";
                                            }; ?></span>
                                            <select required class="input-small form-control payment_method"
                                                    name="payment_method[]" style="display: none;">
                                                <option value="">Select Method</option>
                                                <option <?php if ($row['payment_method'] == 1) {
                                                    echo "selected";
                                                } ?> value="1">Bank Transfer
                                                </option>
                                                <option <?php if ($row['payment_method'] == 2) {
                                                    echo "selected";
                                                } ?> value="2">Cash
                                                </option>
                                                <option <?php if ($row['payment_method'] == 3) {
                                                    echo "selected";
                                                } ?> value="3">Cheque
                                                </option>
                                                <option <?php if ($row['payment_method'] == 4) {
                                                    echo "selected";
                                                } ?> value="4">Credit Card
                                                </option>
                                                <option <?php if ($row['payment_method'] == 5) {
                                                    echo "selected";
                                                } ?> value="5">Other method
                                                </option>
                                            </select>
                                        </li>
                                        <li>
                                        <span class="" id="Name_lastname"
                                              style="display: inline;"><?= date('M d', strtotime($row['schedule_start_date'])) ?>
                                            - <?= date('M d', strtotime($row['schedule_end_date'])) ?></span>
                                        </li>
                                        <li>
                                            <!-- <span class="edit_lease_schedule" style="display: inline;"><a href="javascript:void(0);">edit |</a> -->
                                            </span> <a
                                                    href="rent/delete_single_amount/<?= $row['lease_tenant_payment_scedhule_id'] ?>"
                                                    style="position:absolute; margin-left: 5px;"> delete</a>
                                            <!-- <span class="save_lease_schedule" style="display: none;"><a href="javascript:void(0);" class="save_edits">save</a> <a href="javascript:void(0);" class="cencel_edit" style="position:absolute; margin-left: 5px;">| delete</a></span> -->
                                        </li>
                                    </ul>
                                    <input type="hidden" name="lease_tenant_payment_scedhule_id[]"
                                           value="<?= $row['lease_tenant_payment_scedhule_id']; ?>">
                                <?php } ?>
                            </div>
                        <?php } ?>
                    </div>
                    <div role="tabpanel" class="tab-pane <?php if ($rent_recieve_show == 0) {
                        echo "active";
                    } ?>" id="periodschedule">
                        <div class="ss_further">
                            <p class="text-center">
                                <span id="lease_dates_text">Lease dates: <?= date('F d, Y', strtotime($lease_info[0]['lease_start_date'])); ?>
                                    – <?= date('F d, Y', strtotime($lease_info[0]['lease_end_date'])); ?>.</span>
                                <a href="javascript:void(0);" class="furter_rent_show_hide">View Future Periods</a>
                            </p>
                            <ul class="table_header top-header-div">
                                <li><strong>Period</strong></li>
                                <li><strong>Due Date</strong></li>
                                <li><strong>Amount Due</strong></li>
                                <li><strong>Rent Received</strong></li>
                                <li><strong>Status</strong></li>
                            </ul>

                            <!-- style="display:none;" -->
                            <div id="furter_rent_list" style="display:none;">
                                <?php foreach ($all_payments_info as $row) { ?>
                                    <?php if ($row['payment_start_period'] > date('Y-m-d')) { ?>
                                        <ul class="table_row schedule_list">
                                            <li><strong><?= date('M d', strtotime($row['payment_start_period'])); ?>
                                                    - <?= date('M d', strtotime($row['payment_end_period'])); ?></strong>
                                            </li>
                                            <li><?= date('M d, Y', strtotime($row['payment_due_date'])); ?></li>
                                            <li><strong>$<?= $row['payment_due_amount'] ?></strong></li>
                                            <li><strong>$<?= $row['lease_rent_recieved'] ?></strong></li>
                                            <?php if ($row['lease_present_status'] == 1) { ?>
                                                <li><span id="lease_label" class="label label-paid">Paid</span></li>
                                            <?php } elseif ($row['lease_present_status'] == 2) { ?>
                                                <li><span class="label label-info">Upcoming</span></li>
                                            <?php } elseif ($row['lease_present_status'] == 3) { ?>
                                                <li><span class="label label-danger">Arrears</span></li>
                                            <?php } ?>
                                        </ul>
                                    <?php }
                                } ?>
                            </div>
                            <?php foreach ($all_payments_info as $row) { ?>
                                <?php if ($row['payment_start_period'] <= date('Y-m-d')) { ?>
                                    <ul class="table_row schedule_list <?php if ($row['payment_start_period'] <= date('Y-m-d') && $row['payment_end_period'] >= date('Y-m-d')) {
                                        echo 'paid_payment';
                                    } ?>">
                                    <li><strong><?= date('M d', strtotime($row['payment_start_period'])); ?>
                                            - <?= date('M d', strtotime($row['payment_end_period'])); ?></strong></li>
                                    <li><?= date('M d, Y', strtotime($row['payment_due_date'])); ?></li>
                                    <li><strong>$<?= $row['payment_due_amount'] ?></strong></li>
                                        <!-- <?php if ($row['payment_start_period'] < date('Y-m-d')) { ?> -->
                                        <li><strong>$<?= $row['lease_rent_recieved'] ?></strong></li>
                                        <!-- <?php } else { ?> -->
                                        <li><strong>$<?= $row['lease_rent_recieved'] ?></strong></li>
                                        <!-- <?php } ?> -->
                                    <?php if ($row['lease_present_status'] == 1) { ?>
                                        <li><span id="lease_label" class="label label-paid">Paid</span></li>
                                    <?php } elseif ($row['lease_present_status'] == 2) { ?>
                                        <li><span class="label label-info">Upcoming</span></li>
                                    <?php } elseif ($row['lease_present_status'] == 3) { ?>
                                        <li><span class="label label-danger">Arrears</span></li>
                                    <?php }
                                } ?>
                                </ul>
                            <?php } ?>
                        </div>
                    </div>
                </div>
            </div>

            <div class="text-center extar_p">
                <a href="Dashboard/<?= $property_id; ?>" class="btn btn-light btn-md">Back to Dashboard</a>
            </div>
        </div>
        <div style="clear:both"></div>
    </div>

</div>
<!-- Modal -->
<div class="modal fade" id="recive_rent" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span>
                </button>
                <h2 class="modal-title" id="myModalLabel">Enter Rent Received</h2>
            </div>
            <form action="rent/insert_rent_payment_update" onsubmit="return chk_payment_update()" method="post">
                <div class="modal-body remove_wrap" id="cbcheck">
                    <ul class="table_header top-header-div">
                        <li>Payer Name</li>
                        <li>Amount Received</li>
                        <li>Payment Method</li>
                        <li>Date Paid</li>
                        <li>Send Receipt?</li>
                    </ul>
                    <?php foreach ($get_tenant_list as $row) { ?>
                        <ul class="table_row schedule_list">
                            <li class="payer_name"><label id="tenant_name"
                                                          for="t-payment"><?= $row['user_fname'] ?> <?= $row['user_lname'] ?></label>
                            </li>
                            <li class="how_much">
                                <h1>Amount Received</h1>
                                <input type="text" class="input-small p_amount form-control"
                                       id="p_amount<?= $row['lease_detail_id']; ?>" name="amount_recieved[]"
                                       value="<?php if ($row['payment_update_status'] == 2 || $row['payment_update_status'] == 4) {
                                           echo $row['payment_update_by'];
                                       } else {
                                           echo "0.00";
                                       } ?>">
                            </li>
                            <li class="payment_method">
                                <h1>Payment Method</h1>
                                <select class="input-small form-control p_method"
                                        id="p_method<?= $row['lease_detail_id']; ?>" name="payment_method[]">
                                    <option value="">Select Method</option>
                                    <option value="1">Bank Transfer</option>
                                    <option value="2">Cash</option>
                                    <option value="3">Cheque</option>
                                    <option value="4">Credit Card</option>
                                    <option value="5">Other method</option>
                                </select>
                            </li>
                            <li><h1>Date Paid</h1>
                                <div class="input-group">
                                    <input class="form-control date_hover" name="payment_date[]"
                                           value="<?= date('d/m/Y'); ?>" id="date_hover<?= $row['lease_detail_id']; ?>"
                                           type="text" readonly/>
                                    <span class="input-group-addon"><i class="glyphicon glyphicon-calendar"></i></span>
                                </div>
                            </li>
                            <li>
                                <h1>Send Receipt?</h1>
                                <input class="form-control p_checkbox"
                                       value="<?= $row['lease_detail_id']; ?>" <?php if ($row['payment_update_status'] == 2 || $row['payment_update_status'] == 4) {
                                    echo 'checked';
                                } ?> style="width:26%;float:left;margin-right:10%;margin-top:0%;" type="checkbox">
                                <input type="hidden" name="payment_sms_check[]" value="0">
                                <label class="send_receipt" id="send_receipt_label" for="send_receipt_5a7ef7fcdffaf">
                                </label>
                                <span class="left-text"><b>Send</b></span>
                            </li>
                            <li>
                                <button type="button" class="close remove_payment">×</button>
                            </li>
                        </ul>
                        <input type="hidden" name="email[]" value="<?= $row['email']; ?>">
                        <input type="hidden" name="user_fname[]" value="<?= $row['user_fname']; ?>">
                        <input type="hidden" name="user_lname[]" value="<?= $row['user_lname']; ?>">
                        <input type="hidden" name="lease_detail_id[]" value="<?= $row['lease_detail_id']; ?>">
                    <?php } ?>
                </div>
                <div class="modal-footer">
                    <input type="hidden" name="property_id" value="<?= $property_id; ?>">
                    <input type="hidden" name="lease_id" value="<?= $lease_id; ?>">
                    <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
                    <input type="submit" class="btn btn-primary  pull-right" value="Mark as paid">
                </div>
            </form>
        </div>
    </div>
</div>
<!-- Modal -->
<div class="modal fade" id="mono_receive_rent" tabindex="-1" role="dialog" aria-labelledby="myMonoModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close mono_receive_rent_close_btn" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h2 class="modal-title" id="myMonoModalLabel">Enter Rent Received</h2>
            </div>
            <form id="mono_insert_rent_payment_update_form" action="rent/mono_insert_rent_payment_update" method="post">
                <div class="modal-body remove_wrap" id="mono_cbcheck">
                    <ul class="table_header top-header-div">
                        <li>Payer Name</li>
                        <li>Amount Received</li>
                        <li>Payment Method</li>
                        <li>Date Paid</li>
                        <li style="display: none">Send Receipt?</li>
                    </ul>
                    <? $names = array(); ?>
                    <?php foreach ($get_tenant_list as $row) { ?>

                        <? $names[] = $row['user_fname'] . ' ' . $row['user_lname'] ?>

                        <input type="hidden" name="email[]" value="<?= $row['email']; ?>">
                        <input type="hidden" name="user_fname[]" value="<?= $row['user_fname']; ?>">
                        <input type="hidden" name="user_lname[]" value="<?= $row['user_lname']; ?>">
                        <input type="hidden" name="lease_detail_id[]" value="<?= $row['lease_detail_id']; ?>">

                    <?php } ?>

                    <!-- mono starts  -->
                    <ul class="table_row schedule_list">
                        <li class="payer_name"><label id="tenant_name"
                                                      for="t-payment"><?= implode(" & ", $names) ?></label>
                        </li>
                        <li class="how_much">
                            <h1>Amount Received</h1>
                            <input type="text" class="input-small p_amount form-control"
                                   id="p_amount" name="amount_recieved" value="">
                        </li>
                        <li class="payment_method">
                            <h1>Payment Method</h1>
                            <select class="input-small form-control p_method"
                                    id="p_method" name="payment_method">
                                <option value="">Select Method</option>
                                <option value="1">Bank Transfer</option>
                                <option value="2">Cash</option>
                                <option value="3">Cheque</option>
                                <option value="4">Credit Card</option>
                                <option value="5">Other method</option>
                            </select>
                        </li>
                        <li><h1>Date Paid</h1>
                            <div class="input-group">
                                <input class="form-control date_hover" name="payment_date"
                                       value="<?= date('d/m/Y'); ?>" id="date_hover"
                                       type="text" readonly/>
                                <span class="input-group-addon"><i class="glyphicon glyphicon-calendar"></i></span>
                            </div>
                        </li>
                        <li style="display: none">
                            <h1>Send Receipt?</h1>
                            <input class="form-control p_checkbox"
                                   value="" style="width:26%;float:left;margin-right:10%;margin-top:0%;"
                                   type="checkbox">
                            <input type="hidden" name="payment_sms_check" value="0">
                            <label class="send_receipt" id="send_receipt_label" for="send_receipt_5a7ef7fcdffaf">
                            </label>
                            <span class="left-text"><b>Send</b></span>
                        </li>
                    </ul>
                    <!-- mono ends -->
                </div>
                <div class="modal-footer">
                    <input type="hidden" name="property_id" value="<?= $property_id; ?>">
                    <input type="hidden" name="lease_id" value="<?= $lease_id; ?>">
                    <button type="button" class="btn btn-default pull-left mono_receive_rent_close_btn"
                            data-dismiss="modal">Close
                    </button>
                    <input type="submit" id="mono_insert_rent_payment_update_form_submit_btn"
                           class="btn btn-primary  pull-right" value="Mark as paid">
                </div>
            </form>
        </div>
    </div>
</div>
<!-- Modal -->
<div class="modal fade" id="complete_wizard" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content text-center">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title" id="myModalLabel">Wizard Complete</h4>
            </div>
            <div class="modal-body">
                <h4>Next up is your personal dashboard<br/> and property control center.</h4>
                <p> From there you can add maintenance issues,<br/> expenses, mark rents received,<br/> set up invoices
                    and more!</p>
                <br/><br/><br/>
                <a href="property" class="btn btn btn-primary btn-lg">Continue to get organised</a>
                <br/><br/><br/><br/><br/><br/>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="print_ledger" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">×</button>
        <div class="modal-title">
            <h4 class="print_ledger_title">Print and share with your tenants</h4>
        </div>
        <div class="form-chooser row-fluid">
            <div class="span6">
                <h2>
                    <span class="custom-radio" data-show-form="print-rent-form"> </span>
                    Rent Schedules
                </h2>
                <p>All rent payments, lease periods<br>and due dates for this lease</p>
            </div>
            <div class="span6">
                <h2">
                <span class="custom-radio custom-radio-checked" data-show-form="print-ledger-form"> </span>
                Tenant Ledgers
                </h2>
                <p>Historical rent payments paid<br>by individual tenant or all</p>
            </div>
        </div>
    </div>
    <div class="modal-body print-ledger-form">
        <div class="row-fluid print-rent-form">
            <div class="span12">
                <form id="print_rent_schedule_form"
                      action="?r=RentingSmartTenantLedger&amp;print_ledger=1&amp;property_id=NDA2MA" method="post"
                      name="print_rent_schedule_form">
                    <input id="print_rent_schedule" type="hidden" name="print_rent_schedule" value="1">
                    <div class="row-fluid group-row">
                        <div class="span6 title-column">
                            <p>View rent schedule for all<br> tenants or by individual tenant?</p>
                        </div>
                        <div class="span6 rent_schedule_tenants_name_section">
                            <div class="customize-radio clearfix radio-text">
                                <div>
                                    <input id="tenant_name_radio_0" type="radio" name="rent_schedule_tenant_id"
                                           class="Select_tenant_radio all_tenants" value="4937" checked="checked">
                                    <label for="tenant_name_radio_0">Show All Tenants</label>
                                </div>
                                <div id="rep_radio">
                                    <input id="tenant_name_radio_4937" type="radio" name="rent_schedule_tenant_id"
                                           class="Select_tenant_radio tenant_name_redio" value="4937">
                                    <label for="tenant_name_radio_4937" class="tenant_name_label">Aaaaaaaaaa
                                        Bbbbbbbbb</label>
                                </div>
                                <div style="display: none;" class="stop_radio"></div>
                            </div>
                            <div class="span6 schedule_tenant_name_error"
                                 style="display: none; width:100%;  margin: 0px 0px 0px -28px;">
                                <p class="help-block error">
                                    <i class="fa fa-warning"> </i> Please select one tenant
                                </p>
                            </div>
                        </div>
                    </div>
                    <div class="row-fluid group-row">
                        <div class="span6 title-column">
                            <p>Show payment status<br>(paid, arrears, upcoming)?</p>
                        </div>
                        <div class="span6 status_section customize-radio customize-radio-inline radio-text">
                            <div>
                                <input id="ledger_tenant_status_no" type="radio" name="payment_status"
                                       class="payment_status Select_tenant_radio" value="No" checked="checked">
                                <label for="ledger_tenant_status_no">No</label>
                            </div>
                            <div>
                                <input id="ledger_tenant_status_yes" type="radio" name="payment_status"
                                       class="payment_status Select_tenant_radio" value="Yes">
                                <label for="ledger_tenant_status_yes">Yes</label>
                            </div>
                        </div>
                    </div>
                    <div class="row-fluid">
                        <div class="span3 rent_schedule_error ledger_msgs" style="display: none;">
                            <p class="help-block error">
                                <i class="fa fa-warning"> </i> Something went wrong please try again later...
                            </p>
                        </div>
                    </div>
                    <div class="do-it">
                        <a href="javascript:void(0);" class="print_rent_schedule">Generate &amp; Print</a>
                    </div>
                </form>
            </div>
        </div>
        <div class="row-fluid print-ledger-form">
            <div class="span12">
                <form id="print_ledger_form"
                      action="?r=RentingSmartTenantLedger&amp;print_ledger=1&amp;property_id=NDA2MA" method="post"
                      name="print_ledger_form">
                    <input id="print_tenant_ledgers" type="hidden" name="print_tenant_ledgers" value="1">
                    <div class="print_date row-fluid group-row">
                        <p class="period-from">Select period:</p>
                        <div class="input-box">
                            <input id="ledger_start_date" type="text" name="ledger_start_date" placeholder="Start date"
                                   class="ledger_dates hasDatepicker" value="Feb 11, 2018">
                        </div>
                        <p class="period-to">to</p>
                        <div class="input-box">
                            <input id="ledger_end_date" type="text" name="ledger_end_date" placeholder="End date"
                                   class="ledger_dates hasDatepicker" value="Feb 11, 2018">
                        </div>
                    </div>
                    <div class="row-fluid group-row">
                        <div class="span6 title-column">
                            <p>Ledger for which tenants?</p>
                        </div>
                        <div class="span6 rent_schedule_tenants_name_section">
                            <div class="customize-radio clearfix tenants_name_section">
                                <div id="show_all">
                                    <input id="ledger_tenant_name_radio_0" type="radio" name="ledger_tenant_id"
                                           class="Select_tenant_radio all_tenants_ids" value="4937"
                                           style="display: none;">
                                    <label for="ledger_tenant_name_radio_0">Show All Tenants</label>
                                </div>
                                <div class="rep_tenants_radio">
                                    <input id="ledger_tenant_name_radio_4937" type="radio" name="ledger_tenant_id"
                                           class="Select_tenant_radio ledger_tenant_name_radio" value="4937">
                                    <label for="ledger_tenant_name_radio_4937" class="ledger_tenant_name_label">Aaaaaaaaaa
                                        Bbbbbbbbb</label>
                                </div>
                                <div class="stop_tenants_radio"></div>
                            </div>
                            <div class="span3 tenant_name_error"
                                 style="width: 100%; margin: 0px 0px -30px -25px; display: none;">
                                <p class="help-block error">
                                    <i class="fa fa-warning"> </i> Please select one tenant
                                </p>
                            </div>
                        </div>
                    </div>
                    <div class="span6 ledger_error_msg ledger_msgs" style="margin: 0px 0px 10px 70px; display: none;">
                        <p class="help-block error">
                            <i class="fa fa-warning"> </i> Something went wrong please try again later...
                        </p>
                    </div>
                    <div class="do-it">
                        <a href="javascript:void(0);" class="print_ledger">Generate &amp; Print</a>
                    </div>
                </form>
            </div>
        </div> <!-- print-ledger-form -->
    </div>
    <div class="modal-footer">
        <img src="https://app.rentingsmart.com//RentingSmartHtml/schedule_wizard/../common/css/img/wizard_complete_bg_transparent.png"
             alt="Landlord of the year">
    </div>
</div>
<div class="modal fade" id="printshare" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">

    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title" id="myModalLabel">Print</h4>
            </div>
            <div class="modal-body">
                <br/><br/>
                <!-- Nav tabs -->
                <ul class="nav nav-tabs" role="tablist">
                    <li role="presentation" class="active">
                        <a class="text-center" href="#psrentschedules" aria-controls="home" role="tab"
                           data-toggle="tab">
                            <h2>Rent Schedules</h2>
                            <p>All rent payments, lease periods and due dates for this lease</p>
                        </a>
                    </li>
                    <li role="presentation">
                        <a class="text-center" href="#pstenantledgers" aria-controls="profile" role="tab"
                           data-toggle="tab">
                            <h2>Tenant Ledgers</h2>
                            <p>Complete ledger of payments received by the tenant(s)</p>
                        </a>
                    </li>
                </ul>
                <!-- Tab panes -->
                <div class="tab-content">
                    <div role="tabpanel" class="tab-pane active" id="psrentschedules">
                        <form target="_blank" action="rent/rent_schedule_pdf<?= $archived_string ?>" method="post">
                            <div class="form-group" style="display: none;">
                                <label class="col-md-5 text-right" for="exampleInputName2 ">View rent schedule for all
                                    tenants or by individual tenant?</label>
                                <div class="checkbox col-md-7">
                                    <label class="radio-inline">
                                        <input type="radio" name="tenant_rent_schedule" checked id="inlineRadio1"
                                               value="0"> Show All Tenants
                                    </label>
                                    <!-- <?php foreach ($get_tenant_list as $key => $row) { ?>
                                        <br/>
                                        <label class="radio-inline">
                                            <input type="radio" name="tenant_rent_schedule" id="inlineRadio2" value="<?= $row['tenant_id'] ?>"><?= $row['user_fname'] ?> <?= $row['user_lname'] ?>
                                        </label>
                                    <?php } ?> -->
                                </div>
                            </div>
                            <div class="form-group" style="display: none;">
                                <label class="col-md-5 text-right" for="exampleInputName2">Show payment status (paid,
                                    arrears, upcoming)?</label>
                                <div class="checkbox col-md-7">
                                    <label class="radio-inline">
                                        <input type="radio" name="rent_payment_status" id="inlineRadio1" value="0"> No
                                    </label>
                                    <br/>
                                    <label class="radio-inline">
                                        <input type="radio" checked name="rent_payment_status" id="inlineRadio2"
                                               value="1"> Yes
                                    </label>
                                </div>
                            </div>
                            <div class="text-center">
                                <br/><br/>
                                <input type="hidden" name="property_id" value="<?= $property_id; ?>">
                                <input type="submit" class=" btn btn-primary btn-lg" value="Generate & Print">
                                <br/>
                                <br/>
                            </div>
                        </form>
                    </div>
                    <div role="tabpanel" class="tab-pane" id="pstenantledgers">
                        <form action="property/tenant_ledgers_pdf<?= $archived_string ?>" method="post" target="_blank">
                            <div style="display: none" class="form-group">
                                <label class="col-md-3 text-right" for="exampleInputName2 ">Select period:</label>
                                <div class="input-group date pull-left col-md-4">
                                    <input class="form-control tenant_ledger_start_date" name="tenant_ledger_start_date"
                                           type="text" readonly required/>
                                    <span class="input-group-addon"><i class="glyphicon glyphicon-calendar"></i></span>
                                </div>
                                <div class=" col-md-1 text-center" style="line-height:35px;">To</div>
                                <div class="input-group col-md-4">
                                    <input class="form-control tenant_ledger_end_date" name="tenant_ledger_end_date"
                                           type="text" readonly required/>
                                    <span class="input-group-addon"><i class="glyphicon glyphicon-calendar"></i></span>
                                </div>
                            </div>
                            <div class="form-group" style="display: none;">
                                <label class="col-md-5 text-right" for="exampleInputName2 ">Ledger for which
                                    tenants?</label>
                                <div class="checkbox col-md-7">
                                    <label class="radio-inline">
                                        <input type="radio" checked name="tenant_ledger" id="inlineRadio1" value="0">
                                        Show All Tenants
                                    </label>
                                    <?php foreach ($get_tenant_list as $key => $row) { ?>
                                        <br/>
                                        <label class="radio-inline">
                                            <input type="radio" name="tenant_ledger" id="inlineRadio2"
                                                   value="<?= $row['tenant_id'] ?>"><?= $row['user_fname'] ?> <?= $row['user_lname'] ?>
                                        </label>
                                    <?php } ?>
                                </div>
                            </div>
                            <div class="text-center">
                                <br/><br/>
                                <input type="hidden" name="property_id" value="<?= $property_id; ?>">
                                <input type="submit" class=" btn btn-primary btn-lg" value="Generate & Print">
                                <br/>
                                <br/>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<style>
    .ui-datepicker {
        position: relative;
        z-index: 10000 !important;
    }
</style>

<?= $this->load->view('front/footerlink'); ?>
<script type="text/javascript">
    $(function () {
        $('.start_date, .end_date, .tenant_ledger_start_date, .tenant_ledger_end_date').datepicker({
            dateFormat: 'd M, yy'
        });
    });
</script>
<script type="text/javascript">
    <?php if($modal_open == 1){?>
    //$('#recive_rent').modal('show');
    $('#mono_receive_rent').modal('show');
    <?php }?>
</script>

<script>
    $('#mono_receive_rent').on('show.bs.modal', function (e) {
        //alert('modal show');
    });
    $('#mono_receive_rent').on('hide.bs.modal', function (e) {
        console.log(e);
        //e.preventDefault()
        //alert('modal hide');
    });
    $("#mono_receive_rent_close_btn").on("click", function (e) {
        //e.preventDefault();
        //$('#mono_receive_rent').hide();
    })
</script>

<script>
    $("#mono_mark_as_paid").on("click", function () {
        // $('#mono_receive_rent').modal('show');
    })
</script>

<script type="text/javascript">
    <?php if($tenant_ledger_modal == 1){?>
    $('#printshare').modal('show');
    <?php }?>
</script>
<script type="text/javascript">
    $("#cbcheck,#mono_cbcheck").on("change", ".p_checkbox", function () {
        if ($(this).is(":checked")) {
            $(this).next('input').val("1");
        }
        else {
            $(this).next('input').val("0");
        }
    });
    $(document).ready(function () {

        $('.remove_wrap').on("click", ".remove_payment", function (e) {
            e.preventDefault();
            $(this).closest('ul').remove();
        });
        $('.date_hover').each(function () {
            var date_hover = $(this);
            date_hover.datepicker({
                dateFormat: 'd/m/yy'
            });
        });
        $('.p_checkbox').each(function () {
            if ($(this).is(":checked")) {
                $(this).next('input').val("1");
            }
            else {
                $(this).next('input').val("0");
            }
        });
    });

    function chk_payment_update() {
        var ok = true;
        $('.p_method').each(function () {
            var p_method = $(this);
            var p_method_closest_li = p_method.closest('li');
            if (p_method.val() == '') {
                p_method_closest_li.addClass("has-error");
                ok = false;
            }
        });
        $('.p_amount').each(function () {
            var p_amount = $(this);
            // console.log(p_amount);
            var p_amount_closest_li = p_amount.closest('li');
            if (p_amount.val() == '') {
                p_amount_closest_li.addClass("has-error");
                ok = false;
            }
        });
        return ok;
    }
</script>

<script>

    $("#mono_insert_rent_payment_update_form_submit_btn").on("click", function (e) {
        e.preventDefault();

        if (mono_chk_payment_update() == true) {
            $("#mono_insert_rent_payment_update_form").submit();
        }
    });


    function mono_chk_payment_update() {
        var ok = true;

        var p_method = $("#p_method");
        var p_method_closest_li = p_method.closest('li');
        if (p_method.val() == '') {
            p_method_closest_li.addClass("has-error");
            ok = false;
        } else {
            p_method_closest_li.removeClass("has-error");
        }


        var p_amount = $("#p_amount");

        var p_amount_closest_li = p_amount.closest('li');
        if (p_amount.val() == '' || isNaN(p_amount.val())) {
            p_amount_closest_li.addClass("has-error");
            ok = false;
        } else {
            p_amount_closest_li.removeClass("has-error");
        }

        return ok;
    }
</script>

<script type="text/javascript"></script>
</body>
</html>