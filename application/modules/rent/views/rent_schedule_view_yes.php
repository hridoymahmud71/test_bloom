<!DOCTYPE html>
<html>
<head>
	<link href="assets/css/bootstrap.min.css" rel="stylesheet">
    <link href="assets/css/style.css" rel="stylesheet">
</head>
<style>
    body {
        background: none;
    }
</style>
<body>
	<div class="container">
		<?php
			if($lease_detail[0]['lease_pay_type']==1)
			{
				$pay_type_duration = "week";
				$lease_paying_type = 7;
			}
			elseif($lease_detail[0]['lease_pay_type']==2)
			{
				$pay_type_duration = "Fortnight";
				$lease_paying_type = 14;
			}
			elseif($lease_detail[0]['lease_pay_type']==3)
			{
				$pay_type_duration = "Four week";
				$lease_paying_type = 28;
			}
			else
			{
				$pay_type_duration = "Month";
				$lease_paying_type = 30;
			}
		?>

        <?php
            $pay_day = "";

            if($lease_detail[0]['lease_pay_type'] != 4){
                $pay_day = $this->utility_model->getWeekDay($lease_detail[0]['lease_pay_day']);
            }else if($lease_detail[0]['lease_pay_type'] == 4){
                $pay_day = $this->utility_model->addOrdinalNumberSuffix($lease_detail[0]['lease_pay_day']);
            }

        ?>


        <div class="col-md-12">
            <div class="text-center">
                <h3><img style="max-width: 200px;" src="assets/img/logo.png"></h3>
            </div>
        </div>
        <div class="col-md-12">
            <h3 class="text-center">Rent Schedule</h3>
        </div>
        <br>


		<div>
			<p><span style="font-weight: bold">Property :</span> <?= $full_property_address?> </p>
			<p><span style="font-weight: bold">Tenants :</span> <?php foreach($tenant_info as $key=>$row){ if($key!=0){echo ' & ';}echo  $row['user_fname']. ' '.$row['user_lname'];}?></p>
            <p><span style="font-weight: bold">Lease Period :</span> <?= date('d/m/Y',strtotime($lease_detail[0]['lease_start_date']));?> to <?= date('d/m/Y',strtotime($lease_detail[0]['lease_end_date']));?></p>
			<p><span style="font-weight: bold">Total Rent :</span> $<?=$total_tenant_amount;?>  on <?= $pay_day?> every <?=$pay_type_duration;?></p>
			<p><span style="font-weight: bold">Landlord :</span> <?=$user_info[0]['user_fname']?> <?=$user_info[0]['user_lname']?></p>
			<p><span style="font-weight: bold">Date Created :</span> <?=date('d/m/Y');?></p>
		</div>
		<br>
		<br>
		<br>
		<table class="table table-bordered table-hover" style="font-size: 15px;">
			<thead>
				<tr>
					<td style="text-align:center;background-color: #0bafd2;font-weight:bold" ><h4>Due Date</h4></td>
					<td style="text-align:center;background-color: #0bafd2;font-weight:bold" ><h4>Period</h4></td>
					<td style="text-align:center;background-color: #0bafd2;font-weight:bold" ><h4>Amount</h4></td>
					<?php if($rent_payment_status==1) { ?>
						<td style="text-align:center;background-color: #0bafd2;font-weight:bold" ><h4>Rent Received</h4></td>
						<td style="text-align:center;background-color: #0bafd2;font-weight:bold" ><h4>Status</h4></td>
					<?php } ?>
				</tr>
			</thead>
			<tbody>
				<?php foreach($all_payments_info as $row) { ?>
				<tr>
					<td style="text-align:center;width:20%;"><?=date('d,M Y', strtotime($row['payment_start_period']));?></td>
					<td style="text-align:center;width:20%;"><?=date('d M', strtotime($row['payment_start_period']));?> - <?=date('d M', strtotime($row['payment_end_period']));?></td>
					<td style="text-align:center;width:20%;">$<?= $row['payment_due_amount'] ?></td>
					<?php if($rent_payment_status==1) { ?>
						<td style="text-align:center;width:20%;"><?php if($row['lease_rent_recieved']!=0){?>$<?=$row['lease_rent_recieved'];?><?php }else{echo "-";}?></td>
						<td style="text-align:center;width:20%;"><?php if($row['lease_present_status']==1){echo "Paid";}elseif($row['lease_present_status']==2){echo "Upcoming";}else{echo "Arrears";}?></td>
					<?php } ?>
				</tr>
				<?php }?>
			</tbody>
		</table>
	</div>
</body>
</html>