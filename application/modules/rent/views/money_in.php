<?php $this->load->view('front/headlink'); ?>
<?php $this->load->view('front/top_menu'); ?>
<div class="container">
    <?php $this->load->view('front/head_nav'); ?>

    <style>
        .left_to_pay_warning {
            display: none;
        }
    </style>

    <div class="row">
        <div class="ss_container">

            <?php if ($this->session->flashdata('success') ) { ?>
                <br>
                <div class="row">
                    <div class="col-md-3"></div>
                    <div class="col-md-6">
                        <div class="panel panel-primary">

                            <?php if ($this->session->flashdata('money_in_add_success')) { ?>
                                <div class="panel-heading">Successful!</div>
                                <div class="panel-body"><?= $this->session->flashdata('money_in_add_success')?></div>
                            <?php } ?>
                            <?php if ($this->session->flashdata('money_in_pay_success')) { ?>
                                <div class="panel-heading">Successful!</div>
                                <div class="panel-body"><?= $this->session->flashdata('money_in_pay_success')?></div>
                            <?php } ?>

                            <?php if ($this->session->flashdata('money_in_update_success')) { ?>
                                <div class="panel-heading">Successful!</div>
                                <div class="panel-body"><?= $this->session->flashdata('money_in_update_success')?></div>
                            <?php } ?>
                            <?php if ($this->session->flashdata('mail_send_success')) { ?>
                                <div class="panel-heading">Successful!</div>
                                <div class="panel-body">Invoice sent</div>
                            <?php } ?>
                        </div>
                    </div>
                    <div class="col-md-3"></div>
                </div>
                <br>
            <?php } ?>

            <?php if ($this->session->flashdata('error') ) { ?>
                <br>
                <div class="row">
                    <div class="col-md-3"></div>
                    <div class="col-md-6">
                        <div class="panel panel-danger">
                            <?php if ($this->session->flashdata('validation_errors')) { ?>
                                <div class="panel-heading">Error!</div>
                                <div class="panel-body"><?= $this->session->flashdata('validation_errors')?></div>
                            <?php } ?>
                        </div>
                    </div>
                    <div class="col-md-3"></div>
                </div>
                <br>
            <?php } ?>

            <h3 class="extra_heading">Other Money In
                <small>for the property at <?= $this->utility_model->getFullPropertyAddress($property_id)?></small>
            </h3>
            <p>
                Here you can enter in other monies received from the tenant.<br> This can be in the way of compensation for damage to the property as an example.
            </p>
            <div>
                <?php if(!$is_archived) { ?>
                <a data-toggle="modal" data-target="#moneyincreateinvoice"
                   class="pull-right btn btn-primary">
                    <span class="glyphicon glyphicon-plus"   aria-hidden="true"></span>
                    Money Received or Owed
                </a>
                <?php } ?>
            </div>
            <div class="ss_bound_content" style="display: none">
                <br/><br/><br/>
                <!-- Nav tabs -->
                <div class="ss_expensesinvoices">
                    <div class="ss_expensesinvoices_top">
                        <div class="col-md-2"><h2>Period</h2></div>
                        <div class="col-md-4">
                            <div class="input-group">
                                <input id="money_in_start" class="form-control" type="text" readonly/>
                                <span class="input-group-addon"><i class="glyphicon glyphicon-calendar"></i></span>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="input-group">
                                <input id="money_in_end" class="form-control" type="text" readonly/>
                                <span class="input-group-addon"><i class="glyphicon glyphicon-calendar"></i></span>
                            </div>
                        </div>
                        <div class="col-md-2"></div>
                    </div>
                </div>
            </div>
            <div class="ss_expensesinvoices_content">
                <?php
                $full_recieve = 0;
                $part_recieve = 0;
                $not_recieve = 0;
                ?>
                <?php foreach ($all_money_in as $row) {
                    if ($row['money_in_amount_recieved_by'] == 1) {
                        $full_recieve = $full_recieve + 1;
                    }
                    if ($row['money_in_amount_recieved_by'] == 2) {
                        $part_recieve = $part_recieve + 1;
                    }
                    if ($row['money_in_amount_recieved_by'] == 3) {
                        $not_recieve = $not_recieve + 1;
                    }
                } ?>
                <?php if ($full_recieve > 0) { ?>
                    <h3>Money in - Fully received</h3>
                    <div class="expense_title_bar">
                        <div class="row-fluid top-table-name">
                            <div class="col-md-2">
                                <p>Received Date</p>
                            </div>
                            <div class="col-md-3">
                                <p>Who owned you?</p>
                            </div>
                            <div class="col-md-2">
                                <p>Amount Paid</p>
                            </div>
                            <div class="col-md-2">
                                <p>Pay Method</p>
                            </div>
                            <div class="col-md-2">
                                <p>Owning for?</p>
                            </div>
                            <div class="col-md-1">
                            </div>
                        </div>
                    </div>
                    <form action="rent/update_rent_information/<?= $property_id; ?>" method="post">
                        <?php foreach ($all_money_in as $row) { ?>
                            <?php if ($row['money_in_amount_recieved_by'] == 1) { ?>
                                <div class="ss_expensesinvoices_c_view col-md-12">

                                    <div class="row-fluid row-inner ss_expanse_top_h">
                                        <div class="col-md-2">
                                            <h4 class="table-name">Due Date</h4>
                                            <p class="unpaid_expense_date money_in_receiving_date">
                                                <?= date("d/m/Y", strtotime($row['money_in_receiving_date'])); ?>
                                            </p>
                                        </div>
                                        <div class="col-md-3">
                                            <h4 class="table-name">Creditor</h4>
                                            <p class="unpaid_paying_who"><?= $row['user_fname']; ?> <?= $row['user_lname']; ?></p>
                                        </div>
                                        <div class="col-md-2">
                                            <h4 class="table-name">Amount Due</h4>
                                            <p class="unpaid_amountdue expense_amountdue">
                                                $<?= $row['money_in_paid_amount']; ?></p>
                                        </div>
                                        <div class="col-md-2">
                                            <h4 class="table-name">Tax Code</h4>
                                            <p class="unpaid_taxcode"><?= $row['money_in_receiving_method']; ?></p>
                                        </div>
                                        <div class="col-md-2">
                                            <p class="money_in_purpose_view"><?= $row['money_in_purpose']; ?></p>
                                        </div>
                                    </div>

                                    <div class="row-fluid row-inner ss_expanse_top_h_edit" style="display: none;">
                                        <div class="col-md-2">
                                            <h4 class="table-name">Due Date</h4>
                                            <p>
                                                <input type="text" class="form-control money_in_receiving_date_date" readonly
                                                       name="money_in_receiving_date[]"
                                                       value="<?= $row['money_in_receiving_date'] != '0000-00-00' ? date("M d, Y", strtotime($row['money_in_receiving_date'])) : ''; ?>">
                                            </p>
                                        </div>
                                        <div class="col-md-3">
                                            <h4 class="table-name">Creditor</h4>
                                            <p>
                                                <select name="tenant_id[]" class="form-control">
                                                    <?php foreach ($all_tenant as $rows) { ?>
                                                        <option value="<?= $rows['user_id']; ?>" <?php if ($row['user_id'] == $rows['tenant_id']) {
                                                            echo "selected";
                                                        } ?>><?= $rows['user_fname']; ?></option>
                                                    <?php } ?>
                                                </select>
                                            </p>
                                        </div>
                                        <div class="col-md-2">
                                            <h4 class="table-name">Amount Due</h4>
                                            <p>$<?= $row['money_in_paid_amount']; ?></p>
                                        </div>
                                        <div class="col-md-2">
                                            <h4 class="table-name">Tax Code</h4>
                                            <p class="unpaid_taxcode"><?= $row['money_in_receiving_method']; ?></p>
                                        </div>
                                        <div class="col-md-2">
                                            <p>
                                                <input type="text" class="form-control" name="money_in_purpose[]"
                                                       value="<?= $row['money_in_purpose']; ?>">
                                            </p>
                                        </div>
                                    </div>

                                    <div class="row ss_wxpance_ifo" style="display: none;">
                                        <ul class="payment-info">
                                            <div class="row-fluid">
                                                <div class="col-md-8">
                                                    <div class="row-fluid">
                                                        <div class="col-md-3">
                                                            <p class="bottom-text">Payments:</p>
                                                        </div>
                                                        <div class="col-md-9">
                                                            <p class="paid_payments">
                                <span>
                                  <?php foreach ($all_money_in_receive_amount as $key => $value) { ?>
                                      <?php if ($value['money_in_id'] == $row['money_in_id']) { ?>
                                          $<?= $value['cash_receiving_amount']; ?> on <?= date("d/m/Y", strtotime($value['money_in_receiving_date_new'])); ?> paid via <?= $value['money_in_receiving_method_new']; ?>
                                          <br>
                                      <?php } ?>
                                  <?php } ?>
                                </span>
                                                            </p>
                                                        </div>
                                                    </div>
                                                    <div class="row-fluid">
                                                        <div class="col-md-3">
                                                            <p class="bottom-text">Attachments:</p>
                                                        </div>
                                                        <div class="col-md-9">
                                                            <p class="attachments_links">
                                                                <?php foreach ($all_money_in_document as $row2) { ?>
                                                                    <?php if ($row2['money_in_id'] == $row['money_in_id']) { ?>
                                                                        <a class="paid_attachments" download
                                                                           href="uploads/rent_document/<?= $row2['money_in_document']; ?>">
                                                                            <span class="paid_attachment"><?= $row2['money_in_document']; ?></span>
                                                                        </a>&nbsp
                                                                    <?php } ?>
                                                                <?php } ?>
                                                                <a class="alast_flag" id="last_flag10" href="#"
                                                                   style="display:none;">I am not visible</a>
                                                            </p>
                                                        </div>
                                                    </div>
                                                    <div class="row-fluid">
                                                        <div class="col-md-3">
                                                            <p class="bottom-text">Comments:</p>
                                                        </div>
                                                        <div class="col-md-9">
                                                            <p class="paid_comments"><?= $row['money_in_description']; ?></p>
                                                            <p class="paid_comments_chng" style="display: none;">
                                                                <textarea cols="60" rows="5"
                                                                          name="money_in_description[]"><?= $row['money_in_description']; ?></textarea>
                                                            </p>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-1"></div>
                                                <div class="col-md-3">
                                                    <div class="bottom-link submit_btn">
                                                        <?php if(!$is_archived) { ?>
                                                        <p><a style="cursor:pointer;" class="paid_expense_edit">Edit
                                                                'Money in'</a></p>
                                                        <p>
                                                            <a href="javascript:void(0);"
                                                               class="add_attachment OpenImgUpload">Add attachment</a>
                                                            <input type="hidden" class="money_in_close_id"
                                                                   name="money_in_id[]"
                                                                   value="<?= $row['money_in_id'] ?>">
                                                        </p>
                                                        <p><a class="delete_expense"
                                                              href="rent/delete_money_in/<?= $row['money_in_id'] ?>/<?= $property_id; ?>">Delete
                                                                this Money in</a></p>
                                                        <?php } ?>
                                                        <p><a class="delete_expense" download
                                                              href="rent/print_money_in_invoice/<?= $row['money_in_id'] ?>">Print
                                                                Invoice</a></p>
                                                        <?php if(!$is_archived) { ?>
                                                        <p><a class="delete_expense"
                                                              href="rent/send_money_in_invoice/<?= $row['money_in_id'] ?>">Send
                                                                Invoice</a></p>
                                                        <?php } ?>
                                                    </div>
                                                    <div class="span3 text-right submit_btn_chng"
                                                         style="display: none;">
                                                        <p><input type="submit"
                                                                  class="btn btn-primary paid_expense_edit_save"
                                                                  value="Save changes"></p>
                                                        <p><a class="btn btn-light paid_expense_edit_cancel">Cancel
                                                                changes</a></p>
                                                    </div>
                                                </div>
                                            </div>
                                        </ul>
                                    </div>
                                </div>
                            <?php } ?>
                        <?php } ?>
                    </form>
                <?php } ?>


                <?php if ($part_recieve > 0) { ?>
                    <h3>Money in - Part Received</h3>
                    <div class="expense_title_bar">
                        <div class="row-fluid top-table-name">
                            <div class="col-md-2">
                                <p>Due Date</p>
                            </div>
                            <div class="col-md-3">
                                <p>Who owes you?</p>
                            </div>
                            <div class="col-md-2">
                                <p>Total Amount</p>
                            </div>
                            <div class="col-md-2">
                                <p>Amount Owing</p>
                            </div>
                            <div class="col-md-2">
                                <p>Owning for?</p>
                            </div>
                            <div class="col-md-1">
                            </div>
                        </div>
                    </div>
                    <form action="rent/update_rent_information/<?= $property_id; ?>" method="post">
                        <?php foreach ($all_money_in as $row) { ?>
                            <?php if ($row['money_in_amount_recieved_by'] == 2) { ?>
                                <div class="ss_expensesinvoices_c_view col-md-12">
                                    <div class="row-fluid row-inner ss_expanse_top_h">
                                        <div class="col-md-2">
                                            <h4 class="table-name">Due Date</h4>
                                            <p class="unpaid_expense_date money_in_receiving_date">
                                                <?= date("d/m/Y", strtotime($row['money_in_amount_due_on'])); ?>
                                            </p>
                                        </div>
                                        <div class="col-md-3">
                                            <h4 class="table-name">Creditor</h4>
                                            <p class="unpaid_paying_who"><?= $row['user_fname']; ?> <?= $row['user_lname']; ?></p>
                                        </div>
                                        <div class="col-md-2">
                                            <h4 class="table-name">Amount Due</h4>
                                            <p class="unpaid_amountdue expense_amountdue">
                                                $<?= $row['money_in_paid_amount']; ?></p>
                                        </div>
                                        <div class="col-md-2">
                                            <?php $count_val = 0;
                                            foreach ($all_money_in_receive_amount as $key => $value) { ?>
                                                <?php if ($value['money_in_id'] == $row['money_in_id']) { ?>
                                                    <?php $count_val = $count_val + $value['cash_receiving_amount']; ?>
                                                <?php } ?>
                                            <?php } ?>
                                            <p class="unpaid_taxcode">$<?php echo $row['money_in_paid_amount'] - $count_val; ?></p>
                                        </div>
                                        <div class="col-md-2">
                                            <p class="money_in_purpose_view"><?= $row['money_in_purpose']; ?></p>
                                        </div>
                                    </div>


                                    <div class="row-fluid row-inner ss_expanse_top_h_edit" style="display:none;">
                                        <div class="col-md-2">
                                            <h4 class="table-name">Due Date</h4>
                                            <p>
                                                <input type="text" class="form-control money_in_receiving_date_date" readonly
                                                       name="money_in_receiving_date[]"
                                                       value="<?= $row['money_in_receiving_date'] != '0000-00-00' ? date("M d, Y", strtotime($row['money_in_receiving_date'])) : ''; ?>">
                                            </p>
                                        </div>
                                        <div class="col-md-3">
                                            <h4 class="table-name">Creditor</h4>
                                            <p>
                                                <select name="tenant_id[]" class="form-control">
                                                    <?php foreach ($all_tenant as $rows) { ?>
                                                        <option value="<?= $rows['user_id']; ?>" <?php if ($row['user_id'] == $rows['tenant_id']) {
                                                            echo "selected";
                                                        } ?>><?= $rows['user_fname']; ?></option>
                                                    <?php } ?>
                                                </select>
                                            </p>
                                        </div>
                                        <div class="col-md-2">
                                            <h4 class="table-name">Amount Due</h4>
                                            <p>
                                                <input type="text" class="form-control" name="money_in_paid_amount[]"
                                                       value="<?= $row['money_in_paid_amount'] ?>">
                                            </p>
                                        </div>
                                        <div class="col-md-2">
                                            <p><?php echo $row['money_in_paid_amount'] - $count_val; ?></p>
                                        </div>
                                        <div class="col-md-2">
                                            <p>
                                                <input type="text" class="form-control" name="money_in_purpose[]"
                                                       value="<?= $row['money_in_purpose']; ?>">
                                            </p>
                                        </div>
                                    </div>


                                    <div class="row ss_wxpance_ifo" style="display: none;">
                                        <ul class="payment-info">
                                            <div class="row-fluid">
                                                <div class="col-md-8">
                                                    <div class="row-fluid">
                                                        <div class="col-md-3">
                                                            <p class="bottom-text">Payments:</p>
                                                        </div>
                                                        <div class="col-md-9">
                                                            <p class="paid_payments">
                                <span>
                                  <?php foreach ($all_money_in_receive_amount as $key => $value) { ?>
                                      <?php if ($value['money_in_id'] == $row['money_in_id']) { ?>
                                          $<?= $value['cash_receiving_amount']; ?> on <?= date("M d, Y", strtotime($value['money_in_receiving_date_new'])); ?> paid via <?= $value['money_in_receiving_method_new']; ?>
                                          <br>
                                      <?php } ?>
                                  <?php } ?>
                                </span>
                                                            </p>
                                                        </div>
                                                    </div>
                                                    <div class="row-fluid">
                                                        <div class="col-md-3">
                                                            <p class="bottom-text">Attachments:</p>
                                                        </div>
                                                        <div class="col-md-9">
                                                            <p class="attachments_links">
                                                                <?php foreach ($all_money_in_document as $row2) { ?>
                                                                    <?php if ($row2['money_in_id'] == $row['money_in_id']) { ?>
                                                                        <a class="paid_attachments" download
                                                                           href="uploads/rent_document/<?= $row2['money_in_document']; ?>">
                                                                            <span class="paid_attachment"><?= $row2['money_in_document']; ?></span>
                                                                        </a>&nbsp
                                                                    <?php } ?>
                                                                <?php } ?>
                                                                <a class="alast_flag" id="last_flag10" href="#"
                                                                   style="display:none;">I am not visible</a>
                                                            </p>
                                                        </div>
                                                    </div>
                                                    <div class="row-fluid">
                                                        <div class="col-md-3">
                                                            <p class="bottom-text">Comments:</p>
                                                        </div>
                                                        <div class="col-md-9">
                                                            <p class="paid_comments"><?= $row['money_in_description']; ?></p>
                                                            <p class="paid_comments_chng" style="display: none;">
                                                                <textarea cols="60" rows="5"
                                                                          name="money_in_description[]"><?= $row['money_in_description']; ?></textarea>
                                                            </p>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-1"></div>
                                                <div class="col-md-3">
                                                    <?php if(!$is_archived) { ?>
                                                    <div class="add_payment_wrapper">
                                                        <button type="button"
                                                                class="btn btn-default btn-sm add_payment_btn"
                                                                data-toggle="modal"
                                                                attr-money-in-id="<?= $row['money_in_id'] ?>"
                                                                attr-max-payable="<?= $row['money_in_paid_amount'] - $row['partial_receiving_amount'] ?>"
                                                        >
                                                            Add Payment
                                                        </button>
                                                    </div>
                                                    <br>
                                                    <?php } ?>

                                                    <div class="bottom-link submit_btn">
                                                        <?php if(!$is_archived) { ?>
                                                        <p><a style="cursor:pointer;" class="paid_expense_edit">Edit
                                                                'Money in'</a></p>
                                                        <p>
                                                            <a href="javascript:void(0);"
                                                               class="add_attachment OpenImgUpload">Add attachment</a>
                                                            <input type="hidden" class="money_in_close_id"
                                                                   name="money_in_id[]"
                                                                   value="<?= $row['money_in_id'] ?>">
                                                        </p>
                                                        <p><a class="delete_expense"
                                                              href="rent/delete_money_in/<?= $row['money_in_id'] ?>/<?= $property_id; ?>">Delete
                                                                this Money in</a></p>
                                                        <?php } ?>
                                                        <p><a class="delete_expense" download
                                                              href="rent/print_money_in_invoice/<?= $row['money_in_id'] ?>">Print
                                                                Invoice</a></p>
                                                        <?php if(!$is_archived) { ?>
                                                        <p><a class="delete_expense"
                                                              href="rent/send_money_in_invoice/<?= $row['money_in_id'] ?>">Send
                                                                Invoice</a></p>
                                                        <?php } ?>
                                                    </div>
                                                    <div class="span3 text-right submit_btn_chng"
                                                         style="display: none;">
                                                        <p><input type="submit" class="btn btn-primary"
                                                                  value="Save changes" id="paid_expense_edit_save"></p>
                                                        <p><a class="btn btn-light paid_expense_edit_cancel">Cancel
                                                                changes</a></p>
                                                    </div>
                                                </div>
                                            </div>
                                        </ul>
                                    </div>
                                </div>
                            <?php } ?>
                        <?php } ?>
                    </form>
                <?php } ?>

                <?php if ($not_recieve > 0) { ?>
                    <h3>Money in - Not yet received</h3>
                    <div class="expense_title_bar">
                        <div class="row-fluid top-table-name">
                            <div class="col-md-2">
                                <p>Due Date</p>
                            </div>
                            <div class="col-md-3">
                                <p>Who owes you?</p>
                            </div>
                            <div class="col-md-2">
                                <p>Amount Due</p>
                            </div>
                            <div class="col-md-2">
                                <p>Due Date</p>
                            </div>
                            <div class="col-md-2">
                                <p>Owning for?</p>
                            </div>
                            <div class="col-md-1">
                            </div>
                        </div>
                    </div>
                    <form action="rent/update_rent_information/<?= $property_id; ?>" method="post">
                        <?php foreach ($all_money_in as $row) { ?>
                            <?php if ($row['money_in_amount_recieved_by'] == 3) { ?>
                                <div class="ss_expensesinvoices_c_view col-md-12">

                                    <div class="row-fluid row-inner ss_expanse_top_h">
                                        <div class="col-md-2">
                                            <h4 class="table-name">Due Date</h4>
                                            <p class="unpaid_expense_date money_in_receiving_date">
                                                <?= date("d/m/Y", strtotime($row['money_in_amount_due_on'])); ?>
                                            </p>
                                        </div>
                                        <div class="col-md-3">
                                            <h4 class="table-name">Creditor</h4>
                                            <p class="unpaid_paying_who"><?= $row['user_fname']; ?> <?= $row['user_lname']; ?></p>
                                        </div>
                                        <div class="col-md-2">
                                            <h4 class="table-name">Amount Due</h4>
                                            <p class="unpaid_amountdue expense_amountdue">
                                                $<?= $row['money_in_paid_amount']; ?></p>
                                        </div>
                                        <div class="col-md-2">
                                            <h4 class="table-name">Tax Code</h4>
                                            <p class="unpaid_taxcode">
                                                <?php if ($row['money_in_amount_due_on'] <= date('Y-m-d')) { ?>
                                                    <?php echo (round((abs(strtotime(date('Y-m-d', strtotime($row['money_in_amount_due_on']))) - strtotime(date('Y-m-d'))) / 86400))) . ' days ago'; ?>
                                                <?php } else { ?>
                                                    <?php echo 'in ' . (round((abs(strtotime(date('Y-m-d', strtotime($row['money_in_amount_due_on']))) - strtotime(date('Y-m-d'))) / 86400))) . ' days'; ?>
                                                <?php } ?>
                                            </p>
                                        </div>
                                        <div class="col-md-2">
                                            <p class="money_in_purpose_view"><?= $row['money_in_purpose']; ?></p>
                                        </div>
                                        <?php if ($row['money_in_amount_due_on'] <= date('Y-m-d')) { ?>
                                            <div class="col-md-1">
                                                <p style="color:red;">overdue</p>
                                            </div>
                                        <?php } ?>
                                    </div>


                                    <div class="row-fluid row-inner ss_expanse_top_h_edit" style="display: none;">
                                        <div class="col-md-2">
                                            <h4 class="table-name">Due Date</h4>
                                            <p>
                                                <input type="text" class="form-control money_in_receiving_date_date" readonly
                                                       name="money_in_receiving_date[]"
                                                       value="<?= $row['money_in_receiving_date'] != '0000-00-00' ? date("M d, Y", strtotime($row['money_in_receiving_date'])) : ''; ?>">
                                            </p>
                                        </div>
                                        <div class="col-md-3">
                                            <h4 class="table-name">Creditor</h4>
                                            <p>
                                                <select name="tenant_id[]" class="form-control">
                                                    <?php foreach ($all_tenant as $rows) { ?>
                                                        <option value="<?= $rows['user_id']; ?>" <?php if ($row['user_id'] == $rows['tenant_id']) {
                                                            echo "selected";
                                                        } ?>><?= $rows['user_fname']; ?></option>
                                                    <?php } ?>
                                                </select>
                                            </p>
                                        </div>
                                        <div class="col-md-2">
                                            <h4 class="table-name">Amount Due</h4>
                                            <p>
                                                <input type="text" class="form-control" name="money_in_paid_amount[]"
                                                       value="<?= $row['money_in_paid_amount']; ?>">
                                            </p>
                                        </div>
                                        <div class="col-md-2">
                                            <?php if ($row['money_in_amount_due_on'] <= date('Y-m-d')) { ?>
                                                <?php echo (round((abs(strtotime(date('Y-m-d', strtotime($row['money_in_amount_due_on']))) - strtotime(date('Y-m-d'))) / 86400))) . ' days ago'; ?>
                                            <?php } else { ?>
                                                <?php echo 'in ' . (round((abs(strtotime(date('Y-m-d', strtotime($row['money_in_amount_due_on']))) - strtotime(date('Y-m-d'))) / 86400))) . ' days'; ?>
                                            <?php } ?>
                                        </div>
                                        <div class="col-md-2">
                                            <p>
                                                <input type="text" class="form-control" name="money_in_purpose[]"
                                                       value="<?= $row['money_in_purpose']; ?>">
                                            </p>
                                        </div>
                                    </div>


                                    <div class="row ss_wxpance_ifo" style="display: none;">
                                        <ul class="payment-info">
                                            <div class="row-fluid">
                                                <div class="col-md-8">
                                                    <div class="row-fluid">
                                                        <div class="col-md-3">
                                                            <p class="bottom-text">Payments:</p>
                                                        </div>
                                                        <div class="col-md-9">
                                                            <p class="paid_payments">-</p>
                                                        </div>
                                                    </div>
                                                    <div class="row-fluid">
                                                        <div class="col-md-3">
                                                            <p class="bottom-text">Attachments:</p>
                                                        </div>
                                                        <div class="col-md-9">
                                                            <p class="attachments_links">
                                                                <?php foreach ($all_money_in_document as $row2) { ?>
                                                                    <?php if ($row2['money_in_id'] == $row['money_in_id']) { ?>
                                                                        <a class="paid_attachments" download
                                                                           href="uploads/rent_document/<?= $row2['money_in_document']; ?>">
                                                                            <span class="paid_attachment"><?= $row2['money_in_document']; ?></span>
                                                                        </a>&nbsp
                                                                    <?php } ?>
                                                                <?php } ?>
                                                                <a class="alast_flag" id="last_flag10" href="#"
                                                                   style="display:none;">I am not visible</a>
                                                            </p>
                                                        </div>
                                                    </div>
                                                    <div class="row-fluid">
                                                        <div class="col-md-3">
                                                            <p class="bottom-text">Comments:</p>
                                                        </div>
                                                        <div class="col-md-9">
                                                            <p class="paid_comments"><?= $row['money_in_description']; ?></p>
                                                            <p class="paid_comments_chng" style="display: none;">
                                                                <textarea cols="60" rows="5"
                                                                          name="money_in_description[]"><?= $row['money_in_description']; ?></textarea>
                                                            </p>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-1"></div>
                                                <div class="col-md-3">
                                                    <?php if(!$is_archived) { ?>
                                                    <div class="add_payment_wrapper">
                                                        <button type="button"
                                                                class="btn btn-default btn-sm add_payment_btn"
                                                                data-toggle="modal"
                                                                attr-money-in-id="<?= $row['money_in_id'] ?>"
                                                                attr-max-payable="<?= $row['money_in_paid_amount'] - $row['partial_receiving_amount'] ?>"
                                                        >
                                                            Add Payment
                                                        </button>
                                                    </div>
                                                    <br>
                                                    <?php } ?>

                                                    <div class="bottom-link submit_btn">
                                                        <?php if(!$is_archived) { ?>
                                                        <p><a style="cursor:pointer;" class="paid_expense_edit">Edit
                                                                'Money in'</a></p>
                                                        <p>
                                                            <a href="javascript:void(0);"
                                                               class="add_attachment OpenImgUpload">Add attachment</a>
                                                            <input type="hidden" class="money_in_close_id"
                                                                   name="money_in_id[]"
                                                                   value="<?= $row['money_in_id'] ?>">
                                                            <input type="hidden" name="no_amount" value="">
                                                        </p>
                                                        <p><a class="delete_expense"
                                                              href="rent/delete_money_in/<?= $row['money_in_id'] ?>/<?= $property_id; ?>">Delete
                                                                this Money in</a></p>
                                                        <?php } ?>
                                                        <p><a class="delete_expense" download
                                                              href="rent/print_money_in_invoice/<?= $row['money_in_id'] ?>">Print
                                                                Invoice</a></p>
                                                        <?php if(!$is_archived) { ?>
                                                        <p><a class="delete_expense"
                                                              href="rent/send_money_in_invoice/<?= $row['money_in_id'] ?>">Send
                                                                Invoice</a></p>
                                                        <?php } ?>
                                                    </div>
                                                    <div class="span3 text-right submit_btn_chng"
                                                         style="display: none;">
                                                        <p><input type="submit" class="btn btn-primary"
                                                                  value="Save changes" id="paid_expense_edit_save"></p>
                                                        <p><a class="btn btn-light paid_expense_edit_cancel">Cancel
                                                                changes</a></p>
                                                    </div>
                                                </div>
                                            </div>
                                        </ul>
                                    </div>
                                </div>
                            <?php } ?>
                        <?php } ?>
                    </form>
                <?php } ?>
            </div>
            <div class="clear extra_padding">
                <form id="form_submit_trigger" action="rent/upload_money_in_image/<?= $property_id; ?>" method="post"
                      enctype="multipart/form-data">
                    <input type="file" id="imgupload" name="money_in_document" style="display:none;">
                    <input type="text" name="money_in_id" id="money_in_id_paste" style="display:none;">
                </form>
                <a class="btn btn-light pull-right" href="Dashboard/<?=$property_id?>">Back To Dashboard</a>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="moneyincreateinvoice" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title" id="myModalLabel">Money In (+ Create Invoices)</h4>
            </div>

            <div class="modal-body">
                <div class="stepwizard">
                    <div class="rate-updates">
                        <div class="tab-content margintop0" style="border:none !important;">
                            <div class="">
                                <div class="step001">
                                    <form onsubmit="return check_money_in()" action="rent/insert_money_in"
                                          method="post">
                                        <div class="form-group" style="display: none">
                                            <div class="col-md-5"><label>Which tenant owes (or has paid) you?*<br/>
                                                </label></div>
                                            <div class="col-md-7">
                                                <select name="tanent_id" id="get_tenant_id" class="form-control">
                                                    <option value="">Select a tenant</option>
                                                    <?php foreach ($all_tenant as $row) { ?>
                                                        <option value="<?= $row['user_id']; ?>"  <?php if($row == $all_tenant[0]){ echo " selected ";} ?>><?= $row['user_fname'] ?> <?= $row['user_lname'] ?></option>
                                                    <?php } ?>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="col-md-5"><label>Total amount owed (or paid)?*</label></div>
                                            <div class="col-md-7">
                                                <div class="col-md-12">
                                                    <input type="number" id="money_in_paid_amount"
                                                           name="money_in_paid_amount" class="form-control"
                                                           placeholder="$0.00">
                                                </div>
                                                <div class="col-md-6" style="display: none">for</div>
                                                <div class="col-md-6" style="display: none">
                                                    <input type="text" id="money_in_purpose" name="money_in_purpose"
                                                           class="form-control" placeholder="Short Desc.">
                                                </div>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <div class="col-md-5"><label>This bill has been *</label></div>
                                            <div class="col-md-7">
                                                <label class="radio-inline">
                                                    <input type="radio" name="money_in_amount_recieved_by"
                                                           id="inlineRadio1" value="1"> Received in full
                                                </label>
                                                <label class="radio-inline">
                                                    <input type="radio" name="money_in_amount_recieved_by"
                                                           id="inlineRadio2" value="2"> Part received
                                                </label>
                                                <label class="radio-inline">
                                                    <input type="radio" name="money_in_amount_recieved_by"
                                                           id="inlineRadio3" value="3"> Still owing
                                                </label>
                                            </div>
                                        </div>
                                        <div id="full_div_view" style="display: none;">
                                            <div id="full_recieve">
                                                <div class="form-group">
                                                    <div class="col-md-5"><label>Date received and payment
                                                            method?* </label></div>
                                                    <div class="col-md-7">
                                                        <div class="row">
                                                            <div class="col-md-6"><input type="text" readonly
                                                                                         name="money_in_receiving_dates"
                                                                                         id="fromDatepick"
                                                                                         class="form-control"
                                                                                         placeholder="Nov 09, 2013">
                                                            </div>
                                                            <div class="col-md-6"><input type="text"
                                                                                         name="money_in_receiving_method"
                                                                                         id="money_in_receiving_method"
                                                                                         class="col-md-4  form-control"
                                                                                         placeholder="e.g. Cash"></div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                            <div id="amount_due_date" style="display: none">
                                                <div class="form-group">
                                                    <div class="col-md-5"><label>The full amount is due on* </label>
                                                    </div>
                                                    <div class="col-md-7">
                                                        <div class="row">
                                                            <div class="col-md-6"><input type="text" readonly
                                                                                         id="money_in_amount_due_on"
                                                                                         name="money_in_amount_due_on"
                                                                                         class="form-control"
                                                                                         placeholder="Nov 09, 2013">
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div id="part_recieve" style="display: none">
                                                <div class="form-group">
                                                    <div class="col-md-5"><label>Received details* (amount, date,
                                                            how) </label></div>
                                                    <div class="col-md-7">
                                                        <div class="row">
                                                            <div class="col-md-3"><input type="text"
                                                                                         id="partial_receiving_amount"
                                                                                         name="partial_receiving_amount"
                                                                                         class="form-control"
                                                                                         placeholder="0.00"></div>
                                                            <div class="col-md-5"><input type="text" readonly
                                                                                         id="money_in_receiving_date"
                                                                                         name="money_in_receiving_date"
                                                                                         class="form-control"
                                                                                         placeholder="Nov 09, 2013">
                                                            </div>
                                                            <div class="col-md-4"><input type="text"
                                                                                         id="money_in_receiving_methods"
                                                                                         name="money_in_receiving_methods"
                                                                                         class="col-md-4 form-control"
                                                                                         placeholder="e.g. Cash"></div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <div class="col-md-5"><label>Add notes (incl. payment details)<br/>
                                                        <small> Your tenants will see those notes if
                                                            you create and send an invoice/receipt
                                                        </small>
                                                    </label>
                                                </div>
                                                <div class="col-md-7">
                                                    <textarea name="money_in_description"
                                                              class="form-control"></textarea>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <div class="col-md-5"><label>Upload supporting docs
                                                        <small> (These will be shared with your
                                                            tenants if you send an invoice/receipt)
                                                        </small>
                                                    </label></div>
                                                <div class="col-md-7">
                                                    <div class="form-group inputDnD">
                                                        <div class="dropzone">
                                   <span style="display: none" class="my-dz-message pull-right">
                                        <h3>Click Here to upload another file</h3>
                                    </span>
                                                            <div class="dz-message">
                                                                <h3> Click Here to upload your files</h3>
                                                            </div>
                                                        </div>
                                                        <div class="previews" id="preview"></div>
                                                    </div>
                                                    <div>* Max 20 MB per file</div>
                                                </div>
                                            </div>
                                            <div class="clear"></div>
                                            <div class="text-center">
                                                <input type="hidden" name="property_id" value="<?= $property_id; ?>">
                                                <input class="print_ledgers btn btn-primary btn-lg" type="submit"
                                                       value="Save details">
                                                <button type="button" class="btn btn-light btn-lg mark_paid"
                                                        data-dismiss="modal">Cancel
                                                </button>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


<!-- Modal -->
<div class="modal fade "
     id="payment_modal" tabindex="-1"
     role="dialog" aria-labelledby="exampleModalCenterTitle"
     aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered"
         role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="">Add Payment</h5>
                <button type="button" class="close"
                        data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form class="add_money_in_payment_form"
                      action="rent/add_money_in_payment"
                      method="post"
                      enctype="multipart/form-data">
                    <input type="hidden" name="money_in_id" class="money_in_id"
                           value="">
                    <input type="hidden" name="max_payable" class="max_payable"
                           value="">
                    <div class="form-group">
                        <label for="">Amount Received</label>
                        <input type="text"
                               class="form-control cash_receiving_amount"
                               name="cash_receiving_amount"
                               placeholder="">
                        <small class="form-text left_to_pay_warning"
                               style="color: red;">
                            Only <span class="left_to_pay"></span>
                            is left to pay
                        </small>
                    </div>
                    <div class="form-group">
                        <label for="">Payment Method</label>
                        <input type="text"
                               class="form-control money_in_receiving_method_new"
                               name="money_in_receiving_method_new"
                               placeholder="e.g. Cash">
                    </div>
                    <div class="form-group">
                        <label for="">Date Received</label>
                        <input type="text"
                               class="form-control money_in_receiving_date_new"
                               name="money_in_receiving_date_new" readonly
                               placeholder="">
                    </div>


                    <button type="submit"
                            class="btn btn-primary add_money_in_payment_form_submit_btn">
                        Submit
                    </button>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary"
                        data-dismiss="modal">Close
                </button>
            </div>
        </div>
    </div>
</div>

<div id="taxreport" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title" id="myModalLabel">Let's create a Tax Report</h4>
            </div>
            <div class="modal-body">
                <p>P.S Have you entered all your income + expenses ?</p>
                <ul>
                    <li>interest payments</li>
                    <li> rental income</li>
                    <li>advertising expenses</li>
                </ul>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-light" data-dismiss="modal">Close</button>
                <a href="yourcashflowreports.html" type="button" class="btn btn-primary">Yes? ... Let's Generate Your
                    Report</a>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    $('.OpenImgUpload').click(function () {
        var money_in_id = $(this).closest('.ss_expensesinvoices_c_view').find('.money_in_close_id').val();
        $("#money_in_id_paste").val(money_in_id);
        $('#imgupload').trigger('click');
    });
    $('#imgupload').on('change', function () {
        $("#form_submit_trigger").trigger("submit");
    })
</script>
<script type="text/javascript">
    $('#inlineRadio1').click(function () {
        $('#full_recieve').show();
        $('#amount_due_date').hide();
        $('#part_recieve').hide();
        $('#full_div_view').show();
    });
    $('#inlineRadio2').click(function () {
        $('#full_recieve').hide();
        $('#amount_due_date').show();
        $('#part_recieve').show();
        $('#full_div_view').show();
    });
    $('#inlineRadio3').click(function () {
        $('#full_recieve').hide();
        $('#amount_due_date').show();
        $('#part_recieve').hide();
        $('#full_div_view').show();
    });
</script>

<script type="text/javascript">
    function check_money_in() {
        var ok = true;
        var tenant_id = $("#get_tenant_id").val();
        var money_in_paid_amount = $("#money_in_paid_amount").val();
        var money_in_purpose = $("#money_in_purpose").val();

        var dot = '';

        (tenant_id == '') ? $('#get_tenant_id').closest('div').addClass("has-error") : $('#get_tenant_id').closest('div').removeClass("has-error");
        (money_in_paid_amount == '') ? $('#money_in_paid_amount').closest('div').addClass("has-error") : $('#money_in_paid_amount').closest('div').removeClass("has-error");
        if (tenant_id == '' || money_in_paid_amount == '') {
            ok = false;
            dot += '-0.1-';
        }

        if ($("#inlineRadio1").is(":checked")) {
            var fromDatepick = $("#fromDatepick").val();
            var money_in_receiving_method = $("#money_in_receiving_method").val();

            (fromDatepick == '') ? $('#fromDatepick').closest('div').addClass("has-error") : $('#fromDatepick').closest('div').removeClass("has-error");
            (money_in_receiving_method == '') ? $('#money_in_receiving_method').closest('div').addClass("has-error") : $('#money_in_receiving_method').closest('div').removeClass("has-error");

            if (fromDatepick == '' || money_in_receiving_method == '') {
                ok = false;
                dot += '-1.1-';
            }
        }

        else if ($("#inlineRadio2").is(":checked")) {
            var money_in_amount_due_on = $("#money_in_amount_due_on").val();
            var partial_receiving_amount = $("#partial_receiving_amount").val();
            var money_in_receiving_date = $("#money_in_receiving_date").val();
            var money_in_receiving_methods = $("#money_in_receiving_methods").val();

            if (parseFloat(partial_receiving_amount) >= parseFloat(money_in_paid_amount)) {
                $('#partial_receiving_amount').closest('div').addClass("has-error");
                ok = false;
                dot += '-2.1-';
            }
            else {
                $('#partial_receiving_amount').closest('div').removeClass("has-error");
            }

            if (money_in_amount_due_on == '' || partial_receiving_amount == '' || money_in_receiving_date == '' || money_in_receiving_methods == '') {
                ok = false;
                dot += '-2.2-';
            }

            (money_in_amount_due_on == '') ? $('#money_in_amount_due_on').closest('div').addClass("has-error") : $('#money_in_amount_due_on').closest('div').removeClass("has-error");
            (partial_receiving_amount == '') ? $('#partial_receiving_amount').closest('div').addClass("has-error") : $('#partial_receiving_amount').closest('div').removeClass("has-error");
            (money_in_receiving_date == '') ? $('#money_in_receiving_date').closest('div').addClass("has-error") : $('#money_in_receiving_date').closest('div').removeClass("has-error");
            (money_in_receiving_methods == '') ? $('#money_in_receiving_methods').closest('div').addClass("has-error") : $('#money_in_receiving_methods').closest('div').removeClass("has-error");
        }

        else if ($("#inlineRadio3").is(":checked")) {
            var money_in_amount_due_on = $("#money_in_amount_due_on").val();

            if (money_in_amount_due_on == '') {
                ok = false;
                dot += '-2.3-';
            }

            (money_in_amount_due_on == '') ? $('#money_in_amount_due_on').closest('div').addClass("has-error") : $('#money_in_amount_due_on').closest('div').removeClass("has-error");
        }

        console.log(dot);
        console.log(ok);


        return ok;
    }
</script>

<!-- <script src="//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script> -->
<?php $this->load->view('front/footerlink'); ?>
<script type="text/javascript">
    $(function () {
        $('#money_in_start, #fromDatepick, #money_in_amount_due_on, #money_in_receiving_date, .money_in_receiving_date_date, .money_in_receiving_date_new').datepicker({
            dateFormat: 'd M yy'
        });
    });
</script>
<script type="text/javascript">
    Dropzone.autoDiscover = false;
    var foto_upload = new Dropzone(".dropzone", {
        url: "<?php echo base_url('rent/rent_document') ?>",
        maxFilesize: 20,
        method: "post",
        acceptedFiles: ".jpg,.jpeg,.png,.gif,.doc,docx,.pdf,.xls,.xlsx, application/msword, \n" +
        "    application/vnd.openxmlformats-officedocument.wordprocessingml.document,\n" +
        "    application/vnd.ms-excel,\n" +
        "    application/vnd.openxmlformats-officedocument.spreadsheetml.sheet,\n" +
        "    application/pdf'",
        paramName: "userfile",
        dictInvalidFileType: "This File Type Not Supported",
        addRemoveLinks: true,
        init: function () {
            var count = 0;
            thisDropzone = this;
            this.on("success", function (file, json) {
                var obj = json;
                $('.previews').append(
                    "<input type='hidden' name='money_in_doc[]' value='" + obj + "'>\n\
            <input type='hidden' name='money_in_width[]' value='" + file.width + "'>\n\
            <input type='hidden' name='money_in_height[]' value='" + file.height + "'>"
                );
                $(".my-dz-message").show();
            });
        }
    });
</script>
<script>
    $(".ss_expanse_top_h").click(function () {
        $(this).closest('.ss_expensesinvoices_c_view').find('.ss_wxpance_ifo').toggle("slow");
    });
    $(".paid_expense_edit").click(function () {

        $(this).closest('.ss_expensesinvoices_c_view').find('.ss_expanse_top_h').hide();
        $(this).closest('.ss_expensesinvoices_c_view').find('.ss_expanse_top_h_edit').show();

        $(this).closest('.ss_expensesinvoices_c_view').find('.unpaid_paying_who').hide();
        $(this).closest('.ss_expensesinvoices_c_view').find('.unpaid_paying_who_chng').show();

        $(this).closest('.ss_expensesinvoices_c_view').find('.money_in_purpose_view').hide();
        $(this).closest('.ss_expensesinvoices_c_view').find('.money_in_purpose_view_chng').show();

        $(this).closest('.ss_expensesinvoices_c_view').find('.paid_comments').hide();
        $(this).closest('.ss_expensesinvoices_c_view').find('.paid_comments_chng').show();

        $(this).closest('.ss_expensesinvoices_c_view').find('.submit_btn').hide();
        $(this).closest('.ss_expensesinvoices_c_view').find('.submit_btn_chng').show();
    });


    $(".paid_expense_edit_cancel").click(function () {
        $(this).closest('.ss_expensesinvoices_c_view').find('.ss_expanse_top_h').show();
        $(this).closest('.ss_expensesinvoices_c_view').find('.ss_expanse_top_h_edit').hide();

        // $(this).closest('.ss_expensesinvoices_c_view').find('.money_in_receiving_date').show();
        // $(this).closest('.ss_expensesinvoices_c_view').find('.money_in_receiving_date_chng').hide();

        $(this).closest('.ss_expensesinvoices_c_view').find('.unpaid_paying_who').show();
        $(this).closest('.ss_expensesinvoices_c_view').find('.unpaid_paying_who_chng').hide();

        $(this).closest('.ss_expensesinvoices_c_view').find('.money_in_purpose_view').show();
        $(this).closest('.ss_expensesinvoices_c_view').find('.money_in_purpose_view_chng').hide();

        $(this).closest('.ss_expensesinvoices_c_view').find('.paid_comments').show();
        $(this).closest('.ss_expensesinvoices_c_view').find('.paid_comments_chng').hide();

        $(this).closest('.ss_expensesinvoices_c_view').find('.submit_btn').show();
        $(this).closest('.ss_expensesinvoices_c_view').find('.submit_btn_chng').hide();

    });


</script>

<script>
    $(function () {

        $('.add_payment_btn').on('click', function (e) {
            var add_payment_btn = $(this);
            var payment_modal = $('#payment_modal');

            init_payment_modal(payment_modal, add_payment_btn);

            payment_modal.modal('show');
        })

        function init_payment_modal(payment_modal, add_payment_btn) {
            var form = payment_modal.find('.add_money_in_payment_form');

            var max_payable = form.find('.max_payable');
            var cash_receiving_amount = form.find('.cash_receiving_amount');
            var money_in_receiving_date_new = form.find('.money_in_receiving_date_new');
            var left_to_pay_warning = form.find('.left_to_pay_warning');
            var money_in_receiving_method_new = form.find('.money_in_receiving_method_new');

            var money_in_id_attr = add_payment_btn.attr('attr-money-in-id');
            var max_payable_attr = add_payment_btn.attr('attr-max-payable');

            var input_money_in_id = form.find('.money_in_id');
            var input_max_payable = form.find('.max_payable');
            var span_left_to_pay = form.find('.left_to_pay');

            input_money_in_id.val(money_in_id_attr);
            input_max_payable.val(max_payable_attr);
            span_left_to_pay.html(max_payable_attr);

            cash_receiving_amount.val('');
            money_in_receiving_method_new.val('');
            money_in_receiving_date_new.val('');

            cash_receiving_amount.closest('.form-group').removeClass('has-error');
            money_in_receiving_date_new.closest('.form-group').removeClass('has-error');
            left_to_pay_warning.hide();
        }

        $('.add_money_in_payment_form_submit_btn').on('click', function (e) {
            e.preventDefault();

            var form = $(this).parents('.add_money_in_payment_form');

            console.log(form);
            //form.css("background-color", "yellow");

            if (paymentFormValidate(form)) {
                form.submit();
                //location.reload();
            }

        })
    })

    function paymentFormValidate(form) {
        var ret = false;
        var err_cnt = 0;

        var max_payable = form.find('.max_payable');
        var cash_receiving_amount = form.find('.cash_receiving_amount');
        var money_in_receiving_date_new = form.find('.money_in_receiving_date_new');
        var left_to_pay_warning = form.find('.left_to_pay_warning');

        if (cash_receiving_amount.val() == '' || isNaN(cash_receiving_amount.val()) || parseFloat(cash_receiving_amount.val()) < 0) {
            err_cnt++;
            console.log('a');
            cash_receiving_amount.closest('.form-group').addClass('has-error');
        } else {
            cash_receiving_amount.closest('.form-group').removeClass('has-error');
        }

        if (money_in_receiving_date_new.val() == '') {
            err_cnt++;
            console.log('b');
            money_in_receiving_date_new.closest('.form-group').addClass('has-error');
        } else {
            money_in_receiving_date_new.closest('.form-group').removeClass('has-error');
        }

        if (parseFloat(cash_receiving_amount.val()) > parseFloat(max_payable.val())) {
            err_cnt++;
            console.log('c');
            left_to_pay_warning.show();
        } else {
            left_to_pay_warning.hide();
        }

        if (err_cnt == 0) {
            ret = true;
        }

        return ret;

    }
</script>


</body>
</html>