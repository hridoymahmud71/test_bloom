<?php $this->load->view('front/headlink');?>
<?php $this->load->view('front/top_menu');?>
<div class="container">
    <?php $this->load->view('front/head_nav'); ?>
    <div class="row">  
        <div class="ss_container">
            <h2 class="text-center">Would you like your tenants to pay their rent, on time, more often? </h2>
            <div class="ss_bound_content">
                <div class="row ss_invice_to">
                    <div class="col-md-1"></div>  
                    <div class="col-md-5 ">
                        <br/>
                        <ul>
                            <li><span class="glyphicon glyphicon-check" aria-hidden="true"></span> Send professional invoices to your tenants</li>
                            <li><span class="glyphicon glyphicon-check" aria-hidden="true"></span> Automatically emailed 3 days prior to due date</li>
                            <li><span class="glyphicon glyphicon-check" aria-hidden="true"></span> Be copied in on invoice (be certain it�s sent)</li>
                        </ul>
                    </div>
                    <div class="col-md-4  ">
                        <br/> 
                        What would your invoices look like? <br/>
                        Examples when paying via
                    </div>
                    <div class="col-md-2"></div> 
                    <br/>     
                </div>   
                <div class="ss_invice_list">
                    <form action="rent/insert_invoice_setting" method="post">
                        <div class="row ss_invice_h_top">    
                            <div class="col-md-2">Tenant </div>
                            <div class="col-md-3">Email <small>(please confirm, or enter)</small></div>
                            <div class="col-md-3">Payment method</div>
                            <div class="col-md-2">Settings</div>
                            <div class="col-md-2">Send invoices?</div>
                        </div>
                        <?php foreach($user_detail as $row){?>
                            <div class="row ss_invice_list_con">    
                                <div class="col-md-2"><?=$row['user_fname'];?> <?=$row['user_lname'];?></div>
                                <input type="hidden" name="user_id[]" value="<?=$row['user_id'];?>">
                                <input type="hidden" name="user_detail_id[]" value="<?=$row['user_detail_id'];?>">
                                <input type="hidden" name="lease_detail_id[]" value="<?=$row['lease_detail_id'];?>">
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <input type="text" name="email[]" class="form-control" value="<?=$row['email'];?>" placeholder="yourname@gmail.com">
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <select class="form-control setting_tenant_info" name="payment_method[]">
                                        <option <?php if($row['payment_method']==1){echo "selected";}?> value="1">Bank Transfer</option>
                                        <option <?php if($row['payment_method']==3){echo "selected";}?> value="3">Cheque</option>
                                        <option <?php if($row['payment_method']==5){echo "selected";}?> value="5">Other method</option>
                                        <!-- <option>DEFT</option> -->
                                    </select>
                                </div>
                                <div class="col-md-2 ggg">
                                    <span id="invoice_check_bank" <?php if($row['payment_method']!=1){echo 'style="display: none;"';}?> class="glyphicon glyphicon-cog "  data-toggle="modal" data-target="#ss_invoice_check_bank" aria-hidden="true"></span>
                                    <span id="invoice_check_cheque" <?php if($row['payment_method']!=3){echo 'style="display: none;"';}?> class="glyphicon glyphicon-cog "  data-toggle="modal" data-target="#ss_invoice_check_cheque" aria-hidden="true"></span>
                                    <span id="invoice_check_other" <?php if($row['payment_method']!=5){echo 'style="display: none;"';}?> class="glyphicon glyphicon-cog "  data-toggle="modal" data-target="#ss_invoice_check_other" aria-hidden="true"></span>
                                    <span id="invoice_check_bank_neutral" <?php if($row['payment_method']==1 || $row['payment_method']==3 || $row['payment_method']==5){echo 'style="display: none;"';}?> class="glyphicon glyphicon-cog "  data-toggle="modal" data-target="#ss_invoice_check_bank_neutral" aria-hidden="true"></span>
                                    
                                </div>
                                <div class="col-md-2">
                                    <div class="checkbox">
                                        <label>
                                            <input class="form-control invoice_checkbox" style="width: 22%;" <?php if($row['user_send_invoice']==1){echo "checked";}?> type="checkbox"> 
                                            <input class="checkbox_hidden_input" type="hidden" name="user_send_invoice[]" value="<?=$row['user_send_invoice'];?>">
                                        </label>
                                    </div>
                                </div>
                                <div>
                                    <!-- Modal one -->
                                    <div class="modal fade" id="ss_invoice_check_bank"  tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                                        <div class="modal-dialog" role="document">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                                    <h4 class="modal-title" id="myModalLabel">Add a Bank Account</h4>
                                                </div>
                                                <div class="modal-body">
                                                    <div class="row">
                                                        <div class="col-md-3">
                                                            <p>Nickname</p>
                                                        </div>
                                                        <div class="col-md-9">
                                                            <input type="text" class="form-control" placeholder="My Westpac Account" value="<?=$row['user_nick_name'];?>" name="user_nick_name[]">
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <br/>
                                                        <div class="col-md-3">
                                                            <p>Account name*</p>
                                                        </div>
                                                        <div class="col-md-9">
                                                            <input type="text" class="form-control" placeholder="Benjamin levi" value="<?=$row['user_account_name'];?>" name="user_account_name[]">
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <br/>
                                                        <div class="col-md-3">
                                                            <p>BSB*</p>
                                                        </div>
                                                        <div class="col-md-9">
                                                            <input type="text" class="form-control" placeholder="XXX-XXX" value="<?=$row['user_bsb']?>" name="user_bsb[]">
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <br/>
                                                        <div class="col-md-3">
                                                            <p>Account Number*</p>
                                                        </div>
                                                        <div class="col-md-9">
                                                            <input type="text" class="form-control" placeholder="123456789" value="<?=$row['user_account_number']?>" name="user_account_number[]">
                                                        </div>
                                                    </div>
                                                    <p> <br/><br/> View an example invoice with these payment details  <br/><br/></p>
                                                </div>
                                                <div class="modal-footer">
                                                    <button type="button" class="btn btn-light" data-dismiss="modal">Close</button>
                                                    <button type="button" class="btn btn-primary" data-dismiss="modal">Save Settings</button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- Modal two -->
                                    <div class="modal fade" id="ss_invoice_check_cheque"  tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                                        <div class="modal-dialog" role="document">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                                    <h4 class="modal-title" id="myModalLabel">Pay by Cheque</h4>
                                                </div>
                                                <div class="modal-body">
                                                    <div class="row">
                                                        <div class="col-md-3">
                                                            <p>Payable to:</p>
                                                        </div>
                                                        <div class="col-md-9">
                                                            <input type="text" class="form-control" placeholder="Mr John Landlord" value="<?=$row['user_payable_to']?>" name="user_payable_to[]">
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <br/>
                                                        <div class="col-md-3"><p>Deliver to:</p></div>
                                                        <div class="col-md-9">
                                                            <textarea rows="3" placeholder="Enter your mailing address (or type 'Landlord to pickup from residence')" name="user_deliver_to[]" class="form-control"><?=$row['user_deliver_to']?></textarea>
                                                        </div>
                                                    </div>
                                                    <p> <br/><br/> View an example invoice with these payment details  <br/><br/></p>
                                                </div>
                                                <div class="modal-footer">
                                                    <button type="button" class="btn btn-light" data-dismiss="modal">Close</button>
                                                    <button type="button" class="btn btn-primary" data-dismiss="modal">Save Settings</button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- Modal three -->
                                    <div class="modal fade" id="ss_invoice_check_other"  tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                                        <div class="modal-dialog" role="document">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                                    <h4 class="modal-title" id="myModalLabel">Pay by Other Method</h4>
                                                </div>
                                                <div class="modal-body">
                                                    <div class="row">
                                                        <div class="col-md-3">
                                                            <p>Pay method:</p>
                                                        </div>
                                                        <div class="col-md-9">
                                                            <input type="text" class="form-control" placeholder="Cash in Brown paper bag" value="<?=$row['user_pay_method']?>" name="user_pay_method[]">
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <br/><br/>
                                                        <div class="col-md-3"><p>Details:</p></div>
                                                        <div class="col-md-9">
                                                            <textarea rows="3" placeholder="Optional description" name="user_detail[]" class="form-control"><?=$row['user_detail']?></textarea>
                                                        </div>
                                                    </div>
                                                    <p> <br/><br/> View an example invoice with these payment details  <br/><br/></p>
                                                </div>
                                                <div class="modal-footer">
                                                    <button type="reset" class="btn btn-light" data-dismiss="modal">Close</button>
                                                    <button type="button" class="btn btn-primary" data-dismiss="modal">Save Settings</button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- Modal blank -->
                                    <div class="modal fade" id="ss_invoice_check_bank_neutral"  tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                                        <div class="modal-dialog" role="document">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                                    <h4 class="modal-title" id="myModalLabel">Add a Bank Account</h4>
                                                </div>
                                                <div class="modal-body">
                                                    <div class="row">
                                                        <div class="col-md-3">
                                                            <p>Nickname</p>
                                                        </div>
                                                        <div class="col-md-9">
                                                            <input type="text" class="form-control" placeholder="My Westpac Account" value="" name="user_nick_name_new[]">
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <br/>
                                                        <div class="col-md-3">
                                                            <p>Account name*</p>
                                                        </div>
                                                        <div class="col-md-9">
                                                            <input type="text" class="form-control" placeholder="Benjamin levi" value="" name="user_account_name_new[]">
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <br/>
                                                        <div class="col-md-3">
                                                            <p>BSB*</p>
                                                        </div>
                                                        <div class="col-md-9">
                                                            <input type="text" class="form-control" placeholder="XXX-XXX" value="" name="user_bsb_new[]">
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <br/>
                                                        <div class="col-md-3">
                                                            <p>Account Number*</p>
                                                        </div>
                                                        <div class="col-md-9">
                                                            <input type="text" class="form-control" placeholder="123456789" value="" name="user_account_number_new[]">
                                                        </div>
                                                    </div>
                                                    <p> <br/><br/> View an example invoice with these payment details  <br/><br/></p>
                                                </div>
                                                <div class="modal-footer">
                                                    <button type="button" class="btn btn-light" data-dismiss="modal">Close</button>
                                                    <button type="button" class="btn btn-primary" data-dismiss="modal">Save Settings</button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        <?php }?>
                        <div class="clear">
                            <br/><br/>
                            <p><strong> ready to set up and send invoices to your tenants? </strong></p>
                            <p> No problem... Make sure all are unticked, and whenever you�re ready, you can come back here to start!</p>
                            <br/><br/>
                        </div>
                        <div class="clear text-center">
                            <input type="hidden" name="property_id" value="<?=$row['property_id'];?>">
                            <input type="submit" value="Save Invoice Settings" class="print_ledgers btn btn-primary btn-lg">
                            <a href="" class="btn btn-light btn-lg mark_paid">
                            Cancel</a>
                            <br>
                            <br>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<?php $this->load->view('front/footerlink'); ?>
<script type="text/javascript">
    $(document).on("change", ".setting_tenant_info", function () {
        var select_method = $(this).closest('.ss_invice_list_con').find('.setting_tenant_info').val();
        if(select_method==1)
        {
            $(this).closest('.ss_invice_list_con').find('#invoice_check_bank').show();
            $(this).closest('.ss_invice_list_con').find('#invoice_check_cheque').hide();
            $(this).closest('.ss_invice_list_con').find('#invoice_check_other').hide();
            $(this).closest('.ss_invice_list_con').find('#invoice_check_bank_neutral').hide();
        }
        else if(select_method==3)
        {
            $(this).closest('.ss_invice_list_con').find('#invoice_check_bank').hide();
            $(this).closest('.ss_invice_list_con').find('#invoice_check_cheque').show();
            $(this).closest('.ss_invice_list_con').find('#invoice_check_other').hide();
            $(this).closest('.ss_invice_list_con').find('#invoice_check_bank_neutral').hide();
        }
        else if(select_method==5)
        {
            $(this).closest('.ss_invice_list_con').find('#invoice_check_bank').hide();
            $(this).closest('.ss_invice_list_con').find('#invoice_check_cheque').hide();
            $(this).closest('.ss_invice_list_con').find('#invoice_check_other').show();
            $(this).closest('.ss_invice_list_con').find('#invoice_check_bank_neutral').hide();
        }
        else
        {
            $(this).closest('.ss_invice_list_con').find('#invoice_check_bank').hide();
            $(this).closest('.ss_invice_list_con').find('#invoice_check_cheque').hide();
            $(this).closest('.ss_invice_list_con').find('#invoice_check_other').hide();
            $(this).closest('.ss_invice_list_con').find('#invoice_check_bank_neutral').show();
        }
    });
    $(document).on("change", ".invoice_checkbox", function () {
        if($(this).is(":checked"))
        {
            $(this).closest('.ss_invice_list_con').find('.checkbox_hidden_input').val('1');
        }
        else
        {
            $(this).closest('.ss_invice_list_con').find('.checkbox_hidden_input').val('0');
        }
    });
</script>
</body>
</html>
