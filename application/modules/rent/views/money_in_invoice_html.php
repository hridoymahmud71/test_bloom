<!DOCTYPE html>
<html>
<head>
    <link href="assets/css/bootstrap.min.css" rel="stylesheet">
    <link href="assets/css/style.css" rel="stylesheet">
    <style>
        body {
            background: none;
        }
    </style>
</head>
<body>
<div class="container">


    <div class="col-md-12 text-center">
        <div >
            <h3><img style="height: 50px;width: 100px;" src="assets/img/logo.png"></h3>
        </div>
        <div>
            <h3>Money In Invoice</h3>
        </div>

    </div>
    <br>
    <br>

    <div>
        <p>
            <span style="font-weight: bold">Property:</span> <?= $this->utility_model->getFullPropertyAddress($lease_detail[0]['property_id']) ?>
        </p>
        <p><span style="font-weight: bold">Lease Name:</span> <?= $lease_detail[0]['lease_name']; ?></p>
        <p>
            <span style="font-weight: bold">Lease Period:</span> <?= date('d/m/Y', strtotime($lease_detail[0]['lease_start_date'])); ?>
            to <?= date('d/m/Y', strtotime($lease_detail[0]['lease_end_date'])); ?></p>
    </div>
    <br>
    <br>

    <?php if ($money_in_with_receive_amounts && count($money_in_with_receive_amounts) > 0) { ?>

        <?php if (!in_array($money_in_with_receive_amounts['money_in_amount_due_on'], array("", false, null, "1970-01-01"))) { ?>
            <p>
                <span style="font-weight: bold">Due Date:</span> <?= date('M d,Y', strtotime($money_in_with_receive_amounts['money_in_amount_due_on'])); ?>
            </p>
        <?php } ?>
        <p><span style="font-weight: bold">Amount:</span>
            $<?= number_format($money_in_with_receive_amounts['money_in_paid_amount'], 2, '.', ',') ?> </p>
        <p>
            <span style="font-weight: bold">Partially Received:</span> $<?= number_format($money_in_with_receive_amounts['partial_receiving_amount'], 2, '.', ',') ?>
        </p>

        <br>
        <table class="table table-bordered table-hover" style="font-size: 15px;">
            <thead>
            <tr>
                <td scope="row" style="text-align:center;background-color: #0bafd2;font-weight:bold"><h4>Received
                        Date</h4></td>
                <td scope="row" style="text-align:center;background-color: #0bafd2;font-weight:bold"><h4>Amount</h4>
                </td>
                <td scope="row" style="text-align:center;background-color: #0bafd2;font-weight:bold"><h4>Payment
                        Method</h4></td>
            </tr>
            </thead>
            <tbody>

            <?php if ($money_in_with_receive_amounts['money_in_cash_receive_amounts'] && count($money_in_with_receive_amounts['money_in_cash_receive_amounts']) > 0) { ?>
                <?php foreach ($money_in_with_receive_amounts['money_in_cash_receive_amounts'] as $money_in_cash_receive_amount) { ?>
                    <tr>
                        <td style="text-align:center;"><?= date('M d,Y', strtotime($money_in_cash_receive_amount['money_in_receiving_date_new'])); ?></td>
                        <td style="text-align:center;">
                            $<?= number_format($money_in_cash_receive_amount['cash_receiving_amount'], 2, '.', ',') ?></td>
                        <td style="text-align:center;"><?= $money_in_cash_receive_amount['money_in_receiving_method_new'] ?></td>
                    </tr>
                <?php } ?>
            <?php } ?>

            </tbody>
        </table>
    <?php } ?>
</div>
</body>
</html>