	<?php $this->load->view('front/headlinkk'); ?>

			<h4>Current lease: dfd (365 days to go) <a href="#" data-toggle="modal" data-target="#myModal"><span class="glyphicon glyphicon-cog" aria-hidden="true"></span></a></h4>
        
            <div class="ss_schedule">
                <h3>Rent Schedule for the property at <span>Name</span> </h3>
                <div class="col-md-4">
                    <div class="ss_views_by">
                            View by:
                            <ul>
                                <li  class="active"><a href="#rentreceived" aria-controls="rentreceived" role="tab" data-toggle="tab">Rent Received</a></li>
                                <li><a href="#periodschedule" aria-controls="periodschedule" role="tab" data-toggle="tab">Period Schedule</a></li>
                            </ul>
                    </div>
                </div>
                <div class="col-md-4">
                        <p class="tenant_box"><span id="tenant_name">Name lastname..</span> <span id="tenant_payment_stats" class="label label-info">$ 37.71 ahead</span> </p>

                </div>
                <div class="col-md-4">
                        <div class="ss_enter_rent">
                                <a data-toggle="modal" href="#recive_rent" id="mark_as_paid" class="btn btn-light btn-lg mark_paid">Enter Received Rent Now</a>
                             
                                <a  data-toggle="modal" data-target="#print_ledger" href="javascript:void(0);" id="print_tenant_ledger " class="print_ledgers btn btn-primary btn-lg">
                                    Print Ledgers &amp; Schedules
                                </a>
                            </div>

                </div>
                <div class="payment_table">
                <div class="tab-content">
                     
                        <div role="tabpanel" class="tab-pane active" id="rentreceived">
                                <div class="step_5_table">
                                        <ul class="table_header top-header-div">
                                            <li>Amount Received</li>
                                            <li>Date Received</li>
                                            <li>Payer</li>
                                            <li>Paid by</li>
                                            <li>Period(s)</li>
                                            <li> Actions</li>
                                        </ul>
                                        <ul class="table_row schedule_list">
                                            <li id="num">
                                                    <span class="input_text" id="period_amount" style="display: inline;">$50.29</span> 
                                                    <input class="input-small form-control dollar_blue pay_period_amount" type="Number" name="amount_due[]" value="50.29" style="display: none;">
                                            </li>
                                            <li>
                                            <h1>Recived Date</h1>
                                                <span class="input_text" id="due_date" style="display: inline;">Feb 28, 2018</span>
                                                  <div id="fromDate1" style="display:none" class="input-group input-small date" data-date-format="mm-dd-yyyy" name="due_date[]">
                                                        <input class="form-control" type="text" readonly="">
                                                        <span class="input-group-addon"><i class="glyphicon glyphicon-calendar"></i></span>
                                                    </div>
                                            </li>
                                            <li>
                                                    <span class="input_text" id="Name_lastname" style="display: inline;">Name lastname</span> 
                                                    <input class="input-small dollar_blue pay_period_amount form-control" type="text"  value="Name lastname" style="display: none;">
                                            </li>
                                            <li>
                                                    <span class="input_text" id="payment_mathod" style="display: inline;">Cash</span> 
                                                     
                                                    <select class="input-small form-control payment_method" style="display: none;">
                                                            <option value="Select Method">Select Method</option>
                                                            <option value="Bank Transfer">Bank Transfer</option>
                                                            <option value="Cash" selected="selected">Cash</option>
                                                            <option value="Cheque">Cheque</option>
                                                            <option value="Credit Card">Credit Card</option>
                                                            <option value="Other method">Other method</option>
                                                        </select>
                                            
                                            </li>
                                            <li>
                                                    <span class="" id="Name_lastname" style="display: inline;">Feb 17 - Feb 18 (part)</span> 
                                             </li>
                                            
                                            <li>
                                                <span class="edit_lease_schedule" style="display: inline;"><a href="javascript:void(0);">edit</a></span>
                                                <span class="save_lease_schedule" style="display: none;"><a href="javascript:void(0);" class="save_edits">save</a> <a href="javascript:void(0);" class="cencel_edit" style="position:absolute; margin-left: 5px;">| delete</a></span>
                                            </li>
                                        </ul>
                                        
                              
                                    </div>
                        </div>
                        <div role="tabpanel" class="tab-pane" id="periodschedule"> 
                            
                            <div class="ss_further">
                                    <ul class="table_header top-header-div">
                                            <li><strong>Period</strong> </li>
                                            <li>Due Date</li>
                                            <li>Amount Due</li>
                                            <li>Rent Received</li>
                                            <li>Status</li>
                                          
                                    
                                    </ul>
                                    <p class="text-center">
                                            <span id="lease_dates_text">Lease dates:  February 10, 2018 � February 10, 2019.</span>
                                            <a href="javascript:void(0);" class="furter_rent_show_hide" >View more Future Periods</a>
                                        </p>
                                      <div id="furter_rent_list" style="display:none;">
                                            <ul class="table_row schedule_list">
                                                    <li><strong>12 - Feb 18</strong> </li> 
                                                    <li>Feb 12, 2018 </li>
                                                    <li><strong>$22.00</strong> </li>
                                                    <li><strong>$0.00</strong> </li>
                                                    <li><span class="label label-info">Upcoming</span> <span data-toggle="modal" data-target="#ss_schedule_edit"  class="glyphicon glyphicon-edit" aria-hidden="true"></span></li>
                                            </ul>
                                            <ul class="table_row schedule_list">
                                                    <li><strong>12 - Feb 18</strong> </li> 
                                                    <li>Feb 12, 2018 </li>
                                                    <li><strong>$22.00</strong> </li>
                                                    <li><strong>$0.00</strong> </li>
                                                    <li><span class="label label-info">Upcoming</span> </li>
                                            </ul>
                                            <ul class="table_row schedule_list">
                                                    <li><strong>12 - Feb 18</strong> </li> 
                                                    <li>Feb 12, 2018 </li>
                                                    <li><strong>$22.00</strong> </li>
                                                    <li><strong>$0.00</strong> </li>
                                                    <li><span class="label label-info">Upcoming</span> </li>
                                            </ul>
                                            <ul class="table_row schedule_list">
                                                    <li><strong>12 - Feb 18</strong> </li> 
                                                    <li>Feb 12, 2018 </li>
                                                    <li><strong>$22.00</strong> </li>
                                                    <li><strong>$0.00</strong> </li>
                                                    <li><span class="label label-info">Upcoming</span> </li>
                                            </ul>
                                            <ul class="table_row schedule_list">
                                                    <li><strong>12 - Feb 18</strong> </li> 
                                                    <li>Feb 12, 2018 </li>
                                                    <li><strong>$22.00</strong> </li>
                                                    <li><strong>$0.00</strong> </li>
                                                    <li><span class="label label-info">Upcoming</span> </li>
                                            </ul>
                                            <ul class="table_row schedule_list">
                                                    <li><strong>12 - Feb 18</strong> </li> 
                                                    <li>Feb 12, 2018 </li>
                                                    <li><strong>$22.00</strong> </li>
                                                    <li><strong>$0.00</strong> </li>
                                                    <li><span class="label label-info">Upcoming</span> </li>
                                            </ul>
                                    </div>  
                                    <ul class="table_row schedule_list">
                                        <li><strong>12 - Feb 18</strong> </li> 
                                        <li>Feb 12, 2018 </li>
                                        <li><strong>$22.00</strong> </li>
                                        <li><strong>$22.00</strong> </li>
                                        <li><span id="lease_label" class="label label-paid">Paid</span> <span data-toggle="modal" data-target="#ss_schedule_edit" class="glyphicon glyphicon-edit" aria-hidden="true"></span> </li>
                                    </ul>
                                    <ul class="table_row schedule_list paid_payment">
                                            <li><strong>12 - Feb 18</strong> </li> 
                                            <li>Feb 12, 2018 </li>
                                            <li><strong>$22.00</strong> </li>
                                            <li><strong>$22.00</strong> </li>
                                            <li><span id="lease_label" class="label label-paid">Paid</span> <span data-toggle="modal" data-target="#ss_schedule_edit" class="glyphicon glyphicon-edit" aria-hidden="true"></span></li>
                                    </ul>
                                    <ul class="table_row schedule_list">
                                                <li><strong>12 - Feb 18</strong> </li> 
                                                <li>Feb 12, 2018 </li>
                                                <li><strong>$22.00</strong> </li>
                                                <li><strong>$22.00</strong> </li>
                                                <li><span id="lease_label" class="label label-paid">Paid</span> <span class="glyphicon glyphicon-edit" data-toggle="modal" data-target="#ss_schedule_edit" aria-hidden="true"></span></li>
                                    </ul>
                            </div>

                        </div>
                      </div>
                    
                    </div>
                    <div class="text-center extar_p">
                            <a href="dashboard_poperty.html" class="btn btn-light btn-md">Back to Dashboard</a>
 
                    </div>
            </div>
        </div>
        </div>	
    
       
    
    </div>
 

        <!-- Modal -->
<div class="modal fade" id="recive_rent" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
          <div class="modal-content">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
              <h2 class="modal-title" id="myModalLabel">You've received some money</h2>
            </div>
            <div class="modal-body">
                    <ul class="table_header top-header-div">
                            <li>Payer Name</li>
                            <li>Amount Received</li>
                            <li>Payment Method</li>
                            <li>Date Paid</li>
                            <li>Send Receipt?</li>                 
                            <li> </li>
                        
                        </ul>
                        <ul class="table_row schedule_list"> 
                                <li class="payer_name"><label id="tenant_name" for="t-payment">aaaaaaaaaa bbbbbbbbb</label></li>
                                <li class="how_much">
								<h1>Amount Received</h1>
                                    <input type="text" class="input-small form-control" name="" id="t-payment" value="0.00">
                                 
                                </li>
                                <li class="payment_method">
								<h1>Payment Method</h1>
                                  
                                    <select class="input-small form-control" id="p-method" name="">
                                        <option value="Select Method" selected="">Select Method</option>
                                        <option id="Bank Transfer" value="Bank Transfer">Bank Transfer</option>
                                        <option id="Cash" value="Cash">Cash</option>
                                        <option id="Cheque" value="Cheque">Cheque</option>
                                        <option id="Credit Card" value="Credit Card">Credit Card</option>
                                        <option id="Other method" value="Other method">Other method</option>
                                    </select>
                                </li>
                                <li><h1>Date Paid</h1>  
                                    <div id="fromDate2" class="input-group date" data-date-format="mm-dd-yyyy">
                                            <input class="form-control" type="text" readonly />
                                            <span class="input-group-addon"><i class="glyphicon glyphicon-calendar"></i></span>
                                        </div>
                                </li>
                                <li><h1>Send Receipt?</h1><input type="checkbox" id="send_receipt_5a7ef7fcdffaf" name="send_receipt[7277]">
                                    <label class="send_receipt" id="send_receipt_label" for="send_receipt_5a7ef7fcdffaf">
                                    </label>
                                    <span class="left-text">Send</span>
                                </li>
                                <li><button type="button" class="close remove_payment">�</button></li>
                            </ul>
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-default pull-left" data-dismiss="modal" >Close</button>
              <button type="button" class="btn btn-primary  pull-right"> Mark as paid</button>
            </div>
          </div>
        </div>
      </div>
      <!-- Modal -->
<div class="modal fade" id="complete_wizard" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
          <div class="modal-content text-center">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
              <h4 class="modal-title" id="myModalLabel">Wizard Complete</h4>
            </div>
            <div class="modal-body">
                   <h4>Next up is your personal dashboard<br/> and property control center.</h4> 
                    
                   <p> From there you can add maintenance issues,<br/> expenses, mark rents received,<br/> set up invoices and more!</p>
                   <br/><br/><br/>
                   <a href="dashboard_poperty.html" class="btn btn btn-primary btn-lg">Continue to get organised</a>
                   <br/><br/><br/><br/><br/><br/>
                </div>
          
          </div>
        </div>
      </div>



 
        </div>
 
      
      <!-- Modal -->
      <div class="modal fade" id="ss_schedule_edit" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
          <div class="modal-content">
            <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
              <h4 class="modal-title" id="myModalLabel">Edit an individual period</h4>
            </div>
            <div class="modal-body">
              <p>You can change the dates, due date or amount due. If it is non-rent money, you can add this in separately under �Money In�</p> 
                <form>
                    <div class="form-group">
                            <label for="exampleInputName2">Dates</label>
                            <div id="fromDate3" class="input-group date" data-date-format="mm-dd-yyyy">
                                    <input class="form-control" type="text" readonly />
                                    <span class="input-group-addon"><i class="glyphicon glyphicon-calendar"></i></span>
                                </div>
                    </div>
                    <div class="form-group">
                            <label for="exampleInputName2">Due Date</label>
                            <div id="fromDate4" class="input-group date" data-date-format="mm-dd-yyyy">
                                    <input class="form-control" type="text" readonly />
                                    <span class="input-group-addon"><i class="glyphicon glyphicon-calendar"></i></span>
                                </div>
                    </div>
                    <div class="form-group">
                            <label for="exampleInputName2">Amount Due</label>
                            <input class="form-control" type="number" placeholder="$0.00" />
                    </div>
            </form>
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-light" data-dismiss="modal">Cancel</button>
              <button type="button" class="btn btn-primary">changes</button>
            </div>
          </div>
        </div>
      </div> 

 
  <?php $this->load->view('front/footerlinkk'); ?>



 
<script type="text/javascript">
    $(window).on('load',function(){
        $('#recive_rent').modal('show');
    });
</script>