<?php $this->load->view('front/headlink'); ?>
<?php $this->load->view('front/top_menu'); ?>
<style>
    #ui-datepicker-div{
        z-index: 99999 !important;
    }
</style>
<div class="container">
    <?php $this->load->view('front/head_nav'); ?>
    <div class="row">
        <div class="ss_container">
            <h2><?= ucwords(str_replace("_", "", $which_form)) ?> Invoice </h2>
            <div class="ss_bound_content">
                <form onsubmit="return chk_document_upload()" action="<?= $form_action ?>" method="post"
                      enctype="multipart/form-data">
                    <div class="row extra_padding">
                        <div class="col-md-3">
                        </div>
                        <div class="col-md-6">
                            <?php if ($this->session->flashdata('validation_errors')) { ?>
                                <div class="panel panel-danger">
                                    <div class="panel-heading">Error!</div>
                                    <div class="panel-body"><?php echo $this->session->flashdata('validation_errors'); ?></div>
                                </div>
                            <?php } ?>
                            <?php if ($this->session->flashdata('payment_success')) { ?>
                                <div class="panel panel-primary">
                                    <div class="panel-heading">Success!</div>
                                    <div class="panel-body">
                                        Successfully added payment
                                        &nbsp;
                                        <a class="btn btn-primary btn-sm"
                                           href="waterInvoiceList/<?= $property_id ?>">View Invoice List
                                        </a>
                                    </div>
                                </div>
                            <?php } ?>
                        </div>
                    </div>
                    <div class="col-md-3">
                    </div>
            </div>

            <div class="tab-content">
                <div class="row-fluid document-list-header top-table-name">
                    <!--<div class="col-md-2">Label</div>-->
                    <div class="col-md-4">Invoice Number</div>
                   <!-- <div class="col-md-2">Invoice Description</div>-->
                    <div class="col-md-2">Invoice Due date</div>
                    <div class="col-md-2">Total Invoice Amount</div>
                    <div class="col-md-2">Amount Paid</div>
                    <div class="col-md-2">Amount Outstanding</div>
                </div>
                <div role="tabpanel" class="tab-pane active" id="dall">

                    <?php if ($water_invoice) { ?>
                        <div class="row-fluid documents-list_area">
                            <a class="rep_href">
                                <div class="col-md-4">
                                    <h4 class="table-name">Document Title</h4>
                                    <h5 style="word-break: break-all;"
                                        id="document_title"><?= $water_invoice['water_invoice_number'] ?></h5>
                                </div>
                                <!--<div class="col-md-2">
                                    <h4 class="table-name">Document Description</h4>
                                    <h5 id="document_title"><?/*= $water_invoice['water_invoice_description'] */?></h5>
                                </div>-->
                                <div class="col-md-2 uploaded-date"><?= date("jS F, Y", strtotime($water_invoice['water_invoice_due_date'])); ?></div>
                                <div class="col-md-2 uploaded-date">
                                    $<?= $water_invoice['water_invoice_amount']; ?></div>
                                <div class="col-md-2 uploaded-date">
                                    $<?= $water_invoice['water_invoice_amount_paid']; ?></div>
                                <div class="col-md-2 uploaded-date">
                                    $<?= $water_invoice['water_invoice_amount_due']; ?></div>
                            </a>
                        </div>
                    <?php } ?>

                </div>
            </div>


            <br>
            <div id="payment_error_div" class="row" style="display: none">
                <div class="col-md-3">
                </div>
                <div class="col-md-6">
                    <div class="panel panel-danger">
                        <div class="panel-heading">Error!</div>
                        <div id="payment_error_text" class="panel-body"></div>
                    </div>
                </div>
                <div class="col-md-3">
                </div>
            </div>
            <br>
            <div class="row">
                <div class="col-md-3">
                    Payment amount
                </div>
                <div class="col-md-6">
                    <div class="input-group">
						<span class="input-group-addon">
							$
						</span>
                        <input id="water_invoice_log_amount" type="text" name="water_invoice_log_amount"
                               class="form-control"
                               value="">
                    </div>

                    <input type="hidden" id="water_invoice_amount" name="water_invoice_amount"
                           value="<?= $water_invoice['water_invoice_amount']; ?>">
                    <input type="hidden" id="water_invoice_amount_paid" name="water_invoice_amount_paid"
                           value="<?= $water_invoice['water_invoice_amount_paid']; ?>">
                    <input type="hidden" id="water_invoice_amount_due" name="water_invoice_amount_due"
                           value="<?= $water_invoice['water_invoice_amount_due']; ?>">
                </div>
                <div class="col-md-3">
                </div>
            </div>
            <br>
            <div class="row">
                <div class="col-md-3">
                    Payment date
                </div>
                <div class="col-md-6">
                    <input id="water_invoice_payment_date" readonly type="text" name="water_invoice_payment_date"
                           class="form-control"
                           value=""
                    >
                </div>
                <div class="col-md-3">
                </div>
            </div>
            <div class="row extra_padding">
                <div class="col-md-3">
                    Note
                    (Any notes written here will be visible to the tenant)
                    <br>
                </div>
                <div class="col-md-6">
                            <textarea name="water_invoice_payment_note" id="water_invoice_payment_note"
                                      rows="6"
                                      class="form-control"></textarea>
                </div>
                <div class="col-md-3">
                </div>
            </div>

            <div class="col-md-3"></div>
            <div class="col-md-6">
                <div class="modal-footer clear">
                    <br><br>
                    <input type="hidden" name="water_invoice_id"
                           value="<?= ($water_invoice) ? $water_invoice['water_invoice_id'] : ''; ?>">
                    <input type="hidden" name="property_id" value="<?= $property_id; ?>">
                    <input type="hidden" name="lease_id" value="<?= $lease_id; ?>">
                    <input type="submit" class="btn btn-primary" value="Save">
                    <br><br>
                </div>
            </div>
            <div class="col-md-3"></div>
            </form>
        </div>
    </div>
</div>
</div>
<script src="//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
<?php $this->load->view('front/footerlink'); ?>


<script type="text/javascript">

    function chk_document_upload() {
        var err = 0;
        var ret = false;

        var payment_empty = false;

        var payment_error_div = $('#payment_error_div');
        var payment_error_text = $('#payment_error_text');

        var water_invoice_log_amount = $('#water_invoice_log_amount');
        var water_invoice_payment_date = $('#water_invoice_payment_date');

        var water_invoice_amount = $('#water_invoice_amount');
        var water_invoice_amount_paid = $('#water_invoice_amount_paid');
        var water_invoice_amount_due = $('#water_invoice_amount_due');


        if (water_invoice_log_amount.val() == '' || isNaN(water_invoice_log_amount.val()) || parseFloat(water_invoice_log_amount.val()) <= 0) {
            water_invoice_log_amount.closest('div').addClass("has-error");
            err = err + 1;
            payment_empty = true;
        }
        else {
            water_invoice_log_amount.closest('div').removeClass("has-error");
        }

        if (water_invoice_payment_date.val() == '') {
            water_invoice_payment_date.closest('div').addClass("has-error");
            err = err + 1;
        }
        else {
            water_invoice_payment_date.closest('div').removeClass("has-error");
        }

        if (!payment_empty) {
            if (parseFloat(water_invoice_log_amount.val()) > parseFloat(water_invoice_amount.val())) {
                water_invoice_log_amount.closest('div').addClass("has-error");
                err = err + 1;

                payment_error_text.html('You cannot pay more than the invoice amount');
                payment_error_div.show();
            }

            else {
                water_invoice_log_amount.closest('div').removeClass("has-error");

                payment_error_text.html('');
                payment_error_div.hide();
            }

            if (parseFloat(water_invoice_log_amount.val()) > parseFloat(water_invoice_amount_due.val())) {
                water_invoice_log_amount.closest('div').addClass("has-error");
                err = err + 1;

                payment_error_text.html('You cannot pay more than the due amount');
                payment_error_div.show();
            }

            else {
                water_invoice_log_amount.closest('div').removeClass("has-error");

                payment_error_text.html('');
                payment_error_div.hide();
            }
        }


        if (err == 0) {
            ret = true;
        }
        return ret;
    }
</script>

<script type="text/javascript">
    $(function () {
        $('#water_invoice_payment_date').datepicker({
            required: true,
            dateFormat: 'd M, yy'
        });
    });
</script>


</body>
</html>