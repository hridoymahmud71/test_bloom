<?php $this->load->view('front/headlink'); ?>
<?php $this->load->view('front/top_menu'); ?>
<div class="container">
    <?php $this->load->view('front/head_nav'); ?>
    <div class="row">
        <div class="ss_container">
            <div class=" text-center">
<h2 class="extra_heading"><?= ucwords(str_replace("_", "", $which_form)) ?> Water Invoice  </h2>
			
            <form class="container form-horizontal" onsubmit="return chk_document_upload()" action="<?= $form_action ?>" method="post"
                  enctype="multipart/form-data">
            <div class="ss_bound_content">
                    <div class="row extra_padding">
                        <div class="col-sm-4">
                        </div>
                        <div class="col-sm-8">
                            <?php if ($this->session->flashdata('validation_errors')) { ?>
                                <div class="panel panel-danger">
                                    <div class="panel-heading">Error!</div>
                                    <div class="panel-body"><?php echo $this->session->flashdata('validation_errors'); ?></div>
                                </div>
                            <?php } ?>
                            <?php if ($this->session->flashdata('warning')) { ?>
                                <div class="panel panel-warning">
                                    <div class="panel-heading">Warning!</div>
                                    <div class="panel-body"><?php echo $this->session->flashdata('warning'); ?></div>
                                </div>
                            <?php } ?>
                            <?php if ($this->session->flashdata('add_success')) { ?>
                                <div class="panel panel-primary">
                                    <div class="panel-heading">Success!</div>
                                    <div class="panel-body">
                                        Successfully added Invoice
                                        &nbsp;
                                        <a class="btn btn-primary btn-sm"
                                           href="waterInvoiceList/<?= $property_id ?>">View Invoice List
                                        </a>
                                    </div>
                                </div>
                            <?php } ?>
                            <?php if ($this->session->flashdata('update_success')) { ?>
                                <div class="panel panel-primary">
                                    <div class="panel-heading">Success!</div>
                                    <div class="panel-body">
                                        Successfully updated invoice
                                        &nbsp;
                                        <a class="btn btn-primary btn-sm"
                                           href="waterInvoiceList/<?= $property_id ?>">View Invoice List
                                        </a>
                                    </div>
                                </div>
                            <?php } ?>
                        </div>
                    </div>
                    <div class="col-md-3">
                    </div>
            </div>
            <?php if ($water_invoice) { ?>
                <?php if ($water_invoice['water_invoice_bill_copy'] != '' && $water_invoice['water_invoice_bill_copy'] != null) { ?>
                    <div class="row extra_padding">
                        <div class="col-md-3">
                            Files
                            <br>
                        </div>
                        <div class="col-md-6">
                            <ul class="list-group">
                                <li class="list-group-item">
                                    <a download
                                       href="uploads/water_bill/<?= $water_invoice['water_invoice_bill_copy'] ?>"><?= $water_invoice['water_invoice_bill_copy'] ?></a>
                                </li>
                            </ul>
                        </div>
                        <div class="col-md-3">
                        </div>
                    </div>
                <?php } ?>
            <?php } ?>

            <div class="row">

                <label class="col-sm-4 control-label" for="">Upload Bill Copy<span>
                </label>
                <div class="col-sm-8">
                    <div>*File is optional</div>
                    <div class="form-group inputDnD">
                        <div class="dropzone">
                         <span style="display: none" class="my-dz-message pull-right">
                             <!-- <h3>upload more</h3>-->
                          </span>
                            <div class="dz-message">
                                <h3> Click Here to upload your file</h3>
                            </div>

                        </div>
                        <div class="previews" id="preview"></div>
                    </div>
                    <div>* Max 20 MB per file</div>
                </div>
            </div>
            <br>
            <div class="row extra_padding" style="display: none">
                <label class="col-sm-4 control-label">
                    Description
                    <br>
                </label>
                <div class="col-sm-8">
                            <textarea name="water_invoice_description" id="water_invoice_description" rows="6"
                                      class="form-control"><?= ($water_invoice) ? $water_invoice['water_invoice_description'] : ''; ?></textarea>
                </div>
               
            </div>
            <div class="row">
                <label class="col-sm-4 control-label">
                    Invoice amount
                </label>
                <div class="col-sm-8">
                    <div class="input-group">
						<span class="input-group-addon">
							$
						</span>
                        <input id="water_invoice_amount" type="text" name="water_invoice_amount"
                               class="form-control"
                               value="<?= ($water_invoice) ? $water_invoice['water_invoice_amount'] : ''; ?>"
                        >
                    </div>
                </div>
            </div>
            <br>
            <div class="row">
                <label class="col-sm-4 control-label">
                    Invoice due date
                </label>
                <div class="col-sm-8">
                    <input id="water_invoice_due_date" readonly type="text" name="water_invoice_due_date"
                           class="form-control"
                           value="<?= ($water_invoice) ? date('jS F, Y', strtotime($water_invoice['water_invoice_due_date'])) : ''; ?>"
                    >
                </div>
            </div>
            <br>
            <div class="row">
                <label class="col-sm-4 control-label">
                    Water Metre Reading
                </label>
                <div class="col-sm-8">
                    <input id="water_invoice_water_metre" type="text" name="water_invoice_water_metre"
                           class="form-control"
                           value="<?= ($water_invoice) ? $water_invoice['water_invoice_water_metre'] : ''; ?>"
                    >
                </div>
            </div>
            <br>
            <div class="row">
                <label class="col-sm-4 control-label">
                    Water Usage Period
                    <br>
                    <small>You can find out what period this charge is by looking at the back page of your water bill
                    </small>
                </label>
                <div class="col-sm-8">
                    <div class="row">
                        <div class="col-md-5">
                            <input id="water_invoice_period_start" readonly type="text"
                                   name="water_invoice_period_start"
                                   class="form-control form-inine"
                                   value="<?= ($water_invoice) ? date('F, Y', strtotime($water_invoice['water_invoice_period_start'])) : ''; ?>"
                            >
                        </div>
                        <div class="col-md-2 text-center">to</div>
                        <div class="col-md-5">
                            <input id="water_invoice_period_end" readonly type="text" name="water_invoice_period_end"
                                   class="form-control form-inine"
                                   value="<?= ($water_invoice) ? date('F, Y', strtotime($water_invoice['water_invoice_period_end'])) : ''; ?>"
                            >
                        </div>
                    </div>


                </div>
            </div>

            <div class="col-sm-4 control-label"></div>
            <div class="col-sm-8">
                <div class="modal-footer clear">
                    <br><br>
                    <input type="hidden" name="water_invoice_id"
                           value="<?= ($water_invoice) ? $water_invoice['water_invoice_id'] : ''; ?>">
                    <input type="hidden" name="property_id" value="<?= $property_id; ?>">
                    <input type="hidden" name="lease_id" value="<?= $lease_id?>">
                    <input type="submit" class="btn btn-primary" value="Save">
                    <br><br>
                </div>
            </div>
            <div class="col-md-3"></div>
            </form>
			
        </div>
        </div>
    </div>
</div>
</div>
<script src="//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
<?php $this->load->view('front/footerlink'); ?>

<style>
    .ui-datepicker {
        z-index: 99999 !important;
    }
</style>

<script type="text/javascript">
    Dropzone.autoDiscover = false;
    var foto_upload = new Dropzone(".dropzone", {
        url: "<?php echo base_url('waterBill') ?>",
        maxFiles: 1,
        maxFilesize: 20,
        method: "post",
        acceptedFiles: ".jpg,.jpeg,.png,.gif,.pdf,.doc,.docx,.xls,.xlsx",
        paramName: "userfile",
        dictInvalidFileType: "This File Type Not Supported",
        addRemoveLinks: true,
        init: function () {
            this.on("maxfilesexceeded", function (file) {
                alert("No more files please!");
            });
            var count = 0;
            thisDropzone = this;
            this.on("success", function (file, json) {
                var obj = json;
                $('.previews').append(
                    "<input type='hidden' name='water_bills[]' value='" + obj + "'>\n\
            <input type='hidden' name='file_width[]' value='" + file.width + "'>\n\
            <input type='hidden' name='file_height[]' value='" + file.height + "'>"
                );
                $(".my-dz-message").show();
            });
        }
    });

    /*$("#submit_btn").on("click",function(){
        e.preventDefault();
    });*/

</script>

<script type="text/javascript">

    function chk_document_upload() {
        var err = 0;
        var ret = false;
        var water_invoice_description = $('#water_invoice_description');
        var water_invoice_amount = $('#water_invoice_amount');
        var water_invoice_due_date = $('#water_invoice_due_date');
        var water_invoice_water_metre = $('#water_invoice_water_metre');
        var wate_invoice_period_start = $('#water_invoice_period_start');
        var water_invoice_period_end = $('#water_invoice_period_end');

        // if (water_invoice_description.val() == '') {
        //     water_invoice_description.closest('div').addClass("has-error");
        //     err = err + 1;
        // }
        // else {
        //     water_invoice_description.closest('div').removeClass("has-error");
        // }

        if (water_invoice_amount.val() == '' || isNaN(water_invoice_amount.val()) || parseFloat(water_invoice_amount.val()) <= 0) {
            water_invoice_amount.closest('div').addClass("has-error");
            err = err + 1;
        }
        else {
            water_invoice_amount.closest('div').removeClass("has-error");
        }


        if (water_invoice_due_date.val() == '') {
            water_invoice_due_date.closest('div').addClass("has-error");
            err = err + 1;
        }
        else {
            water_invoice_due_date.closest('div').removeClass("has-error");
        }

        if (water_invoice_water_metre.val() == '' || isNaN(water_invoice_water_metre.val()) || parseFloat(water_invoice_water_metre.val()) <= 0) {
            water_invoice_water_metre.closest('div').addClass("has-error");
            err = err + 1;
        }
        else {
            water_invoice_water_metre.closest('div').removeClass("has-error");
        }

        if (wate_invoice_period_start.val() == '') {
            wate_invoice_period_start.closest('div').addClass("has-error");
            err = err + 1;
        }
        else {
            wate_invoice_period_start.closest('div').removeClass("has-error");
        }
        if (water_invoice_period_end.val() == '') {
            water_invoice_period_end.closest('div').addClass("has-error");
            err = err + 1;
        }
        else {
            water_invoice_period_end.closest('div').removeClass("has-error");
        }

        if (err == 0) {
            ret = true;
        }
        return ret;
    }
</script>
<style>
    .ui-monthpicker {
        margin-top: 18px;
    }

    .ui-monthpicker .ui-datepicker-month {
        display: none;
    }

    .ui-monthpicker td span {
        padding: 5px;
        cursor: pointer;
        text-align: center;
    }
</style>
<script type="text/javascript">
    $(function () {
        $('#water_invoice_due_date').datepicker({
            required: true,
            dateFormat: 'd M, yy'
        });
        $('#water_invoice_period_start').monthpicker({
            dateFormat: 'M, yy'
        });
        $('#water_invoice_period_end').monthpicker({
            dateFormat: 'M, yy'
        });
    });
</script>


</body>
</html>