<?php $this->load->view('front/headlink'); ?>
<?php $this->load->view('front/top_menu'); ?>
<div class="container">
    <?php $this->load->view('front/head_nav'); ?>
    <div class="row">
        <div class="ss_container">
            <h3 class="extra_heading"> Water Invoices
                <small>for the property at <?= $this->utility_model->getFullPropertyAddress($property_id)?></small>

            </h3>
            <div class="row">
                <?php if ($property_id > 0) { ?>
                <?php if (!$is_archived) { ?>
                    <a href="addWaterInvoice/<?= $property_id; ?>" class="pull-right btn btn-primary"
                       style="    margin-left: 10px;">
                        <span class="glyphicon glyphicon-plus" aria-hidden="true"></span>
                        Add new Invoice
                    </a>
                <?php } ?>
                <?php } ?>
            </div>
            <?php if ($this->session->flashdata('success')) { ?>
                <br>
                <div class="row">
                    <div class="col-md-3"></div>
                    <div class="col-md-6">
                        <div class="panel panel-primary">
                            <?php if ($this->session->flashdata('mail_send_success')) { ?>
                                <div class="panel-heading">Success!</div>
                                <div class="panel-body">Invoice sent to Tenants</div>
                            <?php } ?>

                            <?php if ($this->session->flashdata('cancel_success')) { ?>
                                <div class="panel-heading">Success!</div>
                                <div class="panel-body">Invoice cancelled successfully</div>
                            <?php } ?>

                            <?php if ($this->session->flashdata('mark_complete_success')) { ?>
                                <div class="panel-heading">Success!</div>
                                <div class="panel-body">Invoice Marked as completed</div>
                            <?php } ?>
                        </div>
                    </div>
                    <div class="col-md-3"></div>
                </div>
                <br>
            <?php } ?>


            <div class="ss_bound_content">

                <!-- Tab panes -->
                <div class="tab-content">
                    <div class="row-fluid document-list-header top-table-name">
                        <!--<div class="col-md-2">Label</div>-->
                        <div class="col-md-2">Invoice Number</div>
                      <!--  <div class="col-md-2">Invoice Description</div>-->
                        <div class="col-md-2">Invoice Due Date</div>
                        <div class="col-md-2">Period</div>
                        <div class="col-md-1">Total Invoice Amount</div>
                        <div class="col-md-1">Metre</div>
                        <div class="col-md-1">Amount Paid</div>
                        <div class="col-md-1">Outstanding</div>
                        <div class="col-md-2">Action</div>
                    </div>
                    <div role="tabpanel" class="tab-pane active" id="dall">
                        <?php if (count($water_invoices) == 0) { ?>
                            <h4 style="font-style: italic;color:grey">No invoices for water have been created </h4>
                        <?php } ?>
                        <form id="all_submit_form" action="lease/doc_download" method="post">
                            <?php if (count($water_invoices) > 0) { ?>
                                <?php foreach ($water_invoices as $water_invoice) { ?>
                                    <div class="row-fluid documents-list_area">
                                        <a class="rep_href">
                                            <div class="col-md-2">
                                                <h4 class="table-name">Document Title</h4>
                                                <h5 style="word-break: break-all;"
                                                    id="document_title"><?= $water_invoice['water_invoice_number'] ?></h5>
                                            </div>
                                            <!--<div class="col-md-2">
                                                <h4 class="table-name">Document Description</h4>
                                                <h5 id="document_title"><?/*= $water_invoice['water_invoice_description'] */?></h5>
                                            </div>-->
                                            <div class="col-md-2 uploaded-date"><?= date("jS F, Y", strtotime($water_invoice['water_invoice_due_date'])); ?></div>
                                            <div class="col-md-2 uploaded-date"><?= date("F, Y", strtotime($water_invoice['water_invoice_period_start'])); ?>
                                                to <?= date("F, Y", strtotime($water_invoice['water_invoice_period_end'])); ?></div>
                                            <div class="col-md-1 uploaded-date">
                                                $<?= $water_invoice['water_invoice_amount']; ?></div>
                                            <div class="col-md-1 uploaded-date"><?= $water_invoice['water_invoice_water_metre']; ?></div>
                                            <div class="col-md-1 uploaded-date">
                                                $<?= $water_invoice['water_invoice_amount_paid']; ?></div>
                                            <div class="col-md-1 uploaded-date">
                                                $<?= $water_invoice['water_invoice_amount_due']; ?></div>
                                            <div class="col-md-2 ">

                                                <?php if(!$is_archived) { ?>
                                                <a class="btn btn-sm btn-default btn-block"
                                                   href="editWaterInvoice/<?= $water_invoice['water_invoice_id'] ?>">Edit</a>
                                                <?php } ?>
                                                <a download class="btn btn-sm btn-default btn-block"
                                                   href="waterInvoicePdf/<?= $water_invoice['water_invoice_id'] ?>">Download PDF Copy</a>
                                                <?php if(!$is_archived) { ?>
                                                <a class="btn btn-sm btn-default btn-block"
                                                   href="sendInvoiceToTenants/<?= $water_invoice['water_invoice_id'] ?>">Send
                                                    Invoice</a>
                                                <?php } ?>
                                                <?php if ($water_invoice['water_invoice_status'] == 0) { ?>
                                                    <?php if(!$is_archived) { ?>
                                                    <a class="btn btn-sm btn-default btn-block" style="color: white ;background-color: #337ab7;"
                                                       href="payWaterInvoice/<?= $water_invoice['water_invoice_id'] ?>">Pay</a>
                                                    <a class="btn btn-sm btn-default btn-block complete_anchor"
                                                       href="markCompleteWaterInvoice/<?= $water_invoice['water_invoice_id'] ?>">Mark
                                                        Complete</a>
                                                    <?php } ?>
                                                <?php } ?>

                                                <?php if ($water_invoice['water_invoice_status'] == 1) { ?>
                                                    <a class="btn btn-sm btn-success btn-block" style="color: white;cursor:auto"
                                                       href="javascript:void(0);" >
                                                        Paid
                                                    </a>
                                                <?php } ?>
                                                <?php if(!$is_archived) { ?>
                                                <a class="btn btn-sm btn-default btn-block cancel_anchor"
                                                   href="cancelWaterInvoice/<?= $water_invoice['water_invoice_id'] ?>">Cancel</a>
                                                <?php } ?>
                                            </div>
                                        </a>
                                    </div>
                                <?php } ?>
                            <?php } ?>
                            <div id="get_val_here"></div>
                        </form>
                    </div>
                    <div style="margin-top: 50px">
                        <a class="btn btn-light pull-right" href="Dashboard/<?= $property_id ?>">Back To Dashboard</a>
                    </div>


                </div>
            </div>
        </div>
    </div>
</div>
<script src="//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>

<script type="text/javascript">
    $(function () {

        $('.cancel_anchor').on('click', function (e) {

            return confirm('Cancelling the invoice will permanently remove this invoice with its related payment.\n Are you sure?');
        });
        $('.complete_anchor').on('click', function (e) {

            return confirm('The total invoice amount will be changed according to amount paid and no amount will be outstanding.\n Are you sure to mark this invoice as completely paid?');
        });

    });

</script>
<?php $this->load->view('front/footerlink'); ?>
</body>
</html>