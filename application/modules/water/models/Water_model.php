<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Water_model extends CI_Model
{

    public function __construct()
    {
        parent::__construct();
    }

    public function getActiveLease($property_id)
    {
        $this->db->select('*');
        $this->db->from('lease');
        $this->db->where('property_id', $property_id);
        $this->db->where('lease_current_status', 1);

        $query = $this->db->get();
        $row = $query->row_array();
        return $row;
    }

    public function getLease($lease_id)
    {
        $this->db->select('*');
        $this->db->from('lease');
        $this->db->where('lease_id', $lease_id);

        $query = $this->db->get();
        $row = $query->row_array();
        return $row;
    }

    public function getUser($user_id)
    {
        $this->db->select('*');
        $this->db->from('user');
        $this->db->where('user_id', $user_id);

        $query = $this->db->get();
        $row = $query->row_array();
        return $row;
    }

    public function getWaterInvoiceList($lease_id)
    {
        $this->db->select('*');
        $this->db->from('water_invoice');
        $this->db->where('lease_id', $lease_id);
        $this->db->order_by('water_invoice_created_at', 'desc');

        $query = $this->db->get();
        $result = $query->result_array();
        return $result;
    }

    public function getWaterInvoice($water_invoice_id)
    {
        $this->db->select('*');
        $this->db->from('water_invoice');
        $this->db->where('water_invoice_id', $water_invoice_id);

        $query = $this->db->get();
        $row = $query->row_array();
        return $row;
    }

    public function getWaterInvoiceLogs($water_invoice_id)
    {
        $this->db->select('*');
        $this->db->from('water_invoice_log');
        $this->db->where('water_invoice_id', $water_invoice_id);

        $query = $this->db->get();
        $result = $query->result_array();
        return $result;
    }

    public function insert($table_name,$data)
    {
        $this->db->insert($table_name, $data);
        return TRUE;
    }

    public function insert_ret($tablename, $tabledata)
    {
        $this->db->insert($tablename, $tabledata);
        return $this->db->insert_id();
    }

    public function delete_function($tableName, $columnName, $columnVal)
    {
        $this->db->where($columnName, $columnVal);
        $this->db->delete($tableName);
    }

    public function update_function($columnName, $columnVal, $tableName, $data)
    {
        $this->db->where($columnName, $columnVal);
        $this->db->update($tableName, $data);
    }

    public function getTotalWaterInvoiceLogAmount($water_invoice_id)
    {
        $this->db->select('SUM(water_invoice_log_amount) AS total_water_invoice_log_amount', FALSE);
        $this->db->from('water_invoice_log');
        $this->db->where('water_invoice_id', $water_invoice_id);

        $query = $this->db->get();
        $row = $query->row_array();

        $ret = 0.00;

        if($row){
            $ret = $row['total_water_invoice_log_amount']?$row['total_water_invoice_log_amount']:0.00;
        }

        return $ret;
    }

    public function getTenantsWithLeaseDetails($lease_id)
    {
        $this->db->select('*');
        $this->db->from('lease_detail');
        $this->db->where('lease_id', $lease_id);
        $this->db->join('user','lease_detail.tenant_id=user.user_id');

        $query = $this->db->get();
        $result = $query->result_array();

        return $result;
    }

    public function getProperty($property_id)
    {
        $this->db->select('*');
        $this->db->from('property');
        $this->db->where('property_id', $property_id);

        $query = $this->db->get();
        $row = $query->row_array();
        return $row;
    }

    public function getWaterInvoicesWithPaymentLogs($lease_id)
    {
        $ret = null;

        $water_invoices = $this->getWaterInvoiceList($lease_id);

        if($water_invoices){

            for ($i = 0; $i < count($water_invoices);$i++){

                $water_invoices[$i]['water_invoice_log'] = $this->getWaterInvoiceLogs( $water_invoices[$i]['water_invoice_id']);

            }

            $ret = $water_invoices;

        }

        return $ret;
    }


}
