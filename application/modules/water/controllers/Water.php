<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Water extends MX_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model('water_model');
        $this->load->model('utility/utility_model');

        $this->utility_model->check_auth();

        ini_set('memory_limit', '128M');
        ini_set('max_execution_time', 900); //300 seconds = 5 minutes
    }

    private function generateInvoiceNumber()
    {
        return mt_rand(100000, 999999) . date('YmdHisu');
    }


    public function water_invoice_list($property_id)
    {
        $data['property_id'] = $property_id;
        $data['water_invoices'] = null;
        $data['lease'] = null;

        $lease = $this->water_model->getActiveLease($property_id);
        //-------------------------------------------------------------------------
        $lease_id = !empty($lease) ? $lease['lease_id'] : 0;
        $is_archived = $this->utility_model->isArchived();
        $archived_string = $this->utility_model->getArchivedString();
        if ($is_archived) {
            $lease_id = $this->utility_model->getArchivedLeaseId();
        }
        $data['is_archived'] = $is_archived;
        $data['archived_string'] = $archived_string;
        //-------------------------------------------------------------------------

        $water_invoices = null;
        $water_invoices = $this->water_model->getWaterInvoiceList($lease_id);

        $data['lease_id'] = $lease_id;
        $data['water_invoices'] = $water_invoices;

        $this->load->view('water_invoice_list', $data);
    }

    public function add_water_invoice($property_id)
    {
        $data['which_form'] = 'add';
        $data['property_id'] = $property_id;
        $data['form_action'] = 'insertWaterInvoice';

        $lease_id = 0;
        $active_lease = $this->water_model->getActiveLease($property_id);

        $water_invoices = null;
        if ($active_lease) {
            $lease_id = $active_lease['lease_id'];
        }
        $data['lease_id'] = $lease_id;

        $data['water_invoice'] = null;
        $data['water_invoice_logs'] = null;

        $this->load->view('water_invoice_form', $data);
    }

    public function edit_water_invoice($water_invoice_id)
    {
        $water_invoice = $this->water_model->getWaterInvoice($water_invoice_id);

        $data['which_form'] = 'edit';
        $data['property_id'] = 0;
        $data['lease_id'] = 0;
        if ($water_invoice) {
            $data['property_id'] = $water_invoice['property_id'];
        }

        $data['form_action'] = 'updateWaterInvoice';
        $data['water_invoice'] = $water_invoice;

        $this->load->view('water_invoice_form', $data);
    }

    public function insert_water_invoice()
    {
        /*echo "<pre>";
        print_r($_POST);
        echo "</pre>";
        die();*/

        $water_invoice_data['water_invoice_number'] = $this->generateInvoiceNumber();
        $water_invoice_data['water_invoice_description'] = $this->input->post('water_invoice_description');
        $water_invoice_data['water_invoice_amount'] = $this->input->post('water_invoice_amount');
        $water_invoice_data['water_invoice_amount_due'] = $this->input->post('water_invoice_amount');
        $water_invoice_data['water_invoice_water_metre'] = $this->input->post('water_invoice_water_metre');
        $water_invoice_data['water_invoice_due_date'] = date('Y-m-d', strtotime($this->input->post('water_invoice_due_date')));

        $water_invoice_period_start_obj = date_create_from_format('F, Y', $this->input->post('water_invoice_period_start'));
        $water_invoice_data['water_invoice_period_start'] = date_format($water_invoice_period_start_obj, "Y-m");

        $water_invoice_period_end_obj = date_create_from_format('F, Y', $this->input->post('water_invoice_period_end'));
        $water_invoice_data['water_invoice_period_end'] = date_format($water_invoice_period_end_obj, "Y-m");

        $water_invoice_data['property_id'] = $this->input->post('property_id');
        $water_invoice_data['lease_id'] = $this->input->post('lease_id');
        $water_invoice_data['water_invoice_created_at'] = date('Y-m-d H:i:s');
        $water_invoice_data['water_invoice_updated_at'] = date('Y-m-d H:i:s');

        $water_bills = $this->input->post('water_bills');

        //$this->form_validation->set_rules('water_invoice_description', 'Water Invoice Description', 'required');
        $this->form_validation->set_rules('water_invoice_amount', 'Water Invoice Amount', 'required|greater_than[0]');
        $this->form_validation->set_rules('water_invoice_water_metre', 'Water Metre', 'required|greater_than[0]');
        $this->form_validation->set_rules('water_invoice_due_date', 'Water Invoice Due Date', 'required');
        $this->form_validation->set_rules('water_invoice_period_start', 'Water Invoice Start Period', 'required');
        $this->form_validation->set_rules('water_invoice_period_end', 'Water Invoice End Period', 'required');

        if ($water_invoice_data['water_invoice_period_start'] >= $water_invoice_data['water_invoice_period_end']) {
            $errors = "<p> Period end time must be lower that the start time</p>";
            $this->session->set_flashdata('validation_errors', $errors);
            redirect('addWaterInvoice/' . $water_invoice_data['property_id']);
        }

        if ($this->form_validation->run() == false) {
            $this->session->set_flashdata('validation_errors', validation_errors());
            redirect('addWaterInvoice/' . $water_invoice_data['property_id']);
        }

        $water_invoice_id = $this->water_model->insert_ret('water_invoice', $water_invoice_data);

        if ($water_bills) {
            foreach ($water_bills as $water_bill) {
                $upd_data['water_invoice_bill_copy'] = $water_bill;
                $this->db->where('water_invoice_id', $water_invoice_id);
                $this->db->update('water_invoice', $upd_data);
            }
        }

        $this->sndInvToTnts($water_invoice_id, null);

        $this->session->set_flashdata('add_success', 'add_success');
        redirect('addWaterInvoice/' . $water_invoice_data['property_id']);

    }

    public function update_water_invoice()
    {

        /* echo "<pre>";
         print_r($_POST);
         echo "</pre>";
         die();*/

        $paid = null;

        $water_invoice_id = $this->input->post('water_invoice_id');
        $water_invoice_data['water_invoice_description'] = $this->input->post('water_invoice_description');
        $water_invoice_data['water_invoice_amount'] = $this->input->post('water_invoice_amount');
        $water_invoice_data['water_invoice_water_metre'] = $this->input->post('water_invoice_water_metre');
        $water_invoice_data['water_invoice_due_date'] = date('Y-m-d', strtotime($this->input->post('water_invoice_due_date')));

        $water_invoice_period_start_obj = date_create_from_format('F, Y', $this->input->post('water_invoice_period_start'));
        $water_invoice_data['water_invoice_period_start'] = date_format($water_invoice_period_start_obj, "Y-m");

        $water_invoice_period_end_obj = date_create_from_format('F, Y', $this->input->post('water_invoice_period_end'));
        $water_invoice_data['water_invoice_period_end'] = date_format($water_invoice_period_end_obj, "Y-m");

        $water_invoice_data['water_invoice_updated_at'] = date('Y-m-d H:i:s');
        $water_bills = $this->input->post('water_bills');

        if ($water_invoice_data['water_invoice_period_start'] >= $water_invoice_data['water_invoice_period_end']) {
            $errors = "<p> Period end time must be lower that the start time</p>";
            $this->session->set_flashdata('validation_errors', $errors);
            redirect('editWaterInvoice/' . $water_invoice_id);
        }

        //already existing invoice
        $water_invoice = $this->water_model->getWaterInvoice($water_invoice_id);

        if ($water_invoice) {

            if ($water_invoice['water_invoice_status'] != 1) {

                //nothing paid yet
                if ($water_invoice['water_invoice_amount_paid'] == 0) {
                    $water_invoice_data['water_invoice_amount_due'] = $water_invoice_data['water_invoice_amount'];
                }

                //invoice amount is set equal to already paid amount, set invoice as paid and clear dues
                if ($water_invoice['water_invoice_amount_paid'] == $water_invoice_data['water_invoice_amount']) {
                    $water_invoice_data['water_invoice_amount_due'] = 0;
                    $water_invoice['water_invoice_status'] = 1;
                } else if ($water_invoice['water_invoice_amount_paid'] > $water_invoice_data['water_invoice_amount']) {
                    $warning_text = 'The invoice amount must be greater than the amount already paid';
                    unset($water_invoice_data['water_invoice_amount']);

                    $this->session->set_flashdata('warning', $warning_text);
                } else if ($water_invoice['water_invoice_amount_paid'] < $water_invoice_data['water_invoice_amount']) {
                    $water_invoice_data['water_invoice_amount_due'] = $water_invoice_data['water_invoice_amount'] - $water_invoice['water_invoice_amount_paid'];
                }
            }


            //keep this at last position
            if ($water_invoice['water_invoice_status'] == 1) {
                $warning_text = 'This invoice is already completed and amount cannot be updated';
                unset($water_invoice_data['water_invoice_amount']);
                $this->session->set_flashdata('warning', $warning_text);
            }
        }

        if ($water_bills) {
            foreach ($water_bills as $water_bill) {
                $upd_data['water_invoice_bill_copy'] = $water_bill;
                $this->db->where('water_invoice_id', $water_invoice_id);
                $this->db->update('water_invoice', $upd_data);
            }
        }


        //$this->form_validation->set_rules('water_invoice_description', 'Water Invoice Description', 'required');
        $this->form_validation->set_rules('water_invoice_amount', 'Water Invoice Amount', 'required|greater_than[0]');
        $this->form_validation->set_rules('water_invoice_water_metre', 'Water Metre', 'required|greater_than[0]');
        $this->form_validation->set_rules('water_invoice_due_date', 'Water Invoice Due Date', 'required');
        $this->form_validation->set_rules('water_invoice_period_start', 'Water Invoice Start Period', 'required');
        $this->form_validation->set_rules('water_invoice_period_end', 'Water Invoice End Period', 'required');


        if ($this->form_validation->run() == false) {
            $this->session->set_flashdata('validation_errors', validation_errors());
            redirect('editWaterInvoice/' . $water_invoice_id);
        }

        $this->water_model->update_function('water_invoice_id', $water_invoice_id, 'water_invoice', $water_invoice_data);
        $this->sndInvToTnts($water_invoice_id, $paid);
        $this->session->set_flashdata('update_success', 'update_success');
        redirect('editWaterInvoice/' . $water_invoice_id);
    }


    public function pay_water_invoice($water_invoice_id)
    {
        $water_invoice = $this->water_model->getWaterInvoice($water_invoice_id);

        $data['which_form'] = 'pay';
        $data['property_id'] = 0;
        $data['lease_id'] = 0;
        if ($water_invoice) {
            $data['property_id'] = $water_invoice['property_id'];
        }

        $data['form_action'] = 'insertWaterInvoiceLog';
        $data['water_invoice'] = $water_invoice;

        $this->load->view('water_invoice_payment_form', $data);
    }

    public function cancel_water_invoice($water_invoice_id)
    {
        $water_invoice = $this->water_model->getWaterInvoice($water_invoice_id);

        if ($water_invoice && !empty($water_invoice)) {

            $top_html = $this->waterInvoiceLogTopHtml($water_invoice_id);
            $bottom_html = $this->waterInvoiceLogBottomHtml($water_invoice_id);
            $subject = "Water invoice is cancelled";

            $message = "{$top_html} {$bottom_html}";

            $tenants_with_lease_detail = $this->water_model->getTenantsWithLeaseDetails($water_invoice['lease_id']);

            if ($tenants_with_lease_detail) {

                foreach ($tenants_with_lease_detail as $tenant_with_lease_detail) {
                    $greet = "Dear {$tenant_with_lease_detail['user_fname']} {$tenant_with_lease_detail['user_fname']}<br>";
                    $message = "{$greet} A water invoice is cancelled.<br> {$message}";
                    $mail_data['to'] = $tenant_with_lease_detail['email'];
                    $mail_data['subject'] = $subject;
                    $mail_data['message'] = $message;
                    $this->sendEmail($mail_data);

                    $this->utility_model->insertAsMessage(null, $water_invoice['property_id'], $water_invoice['lease_id'], array($tenant_with_lease_detail['user_id']), $subject, $message, null);
                }

            }


            $this->cncl_wtr_invc($water_invoice_id);

            $this->session->set_flashdata('success', "success");
            $this->session->set_flashdata('cancel_success', 'cancel_success');
            redirect('waterInvoiceList/' . $water_invoice['property_id']);
        }

    }

    private function cncl_wtr_invc($water_invoice_id)
    {
        //remove from logs
        $this->db->where('water_invoice_id', $water_invoice_id);
        $this->db->delete('water_invoice_log');

        //then remove from main table
        $this->db->where('water_invoice_id', $water_invoice_id);
        $this->db->delete('water_invoice');
    }

    public function mark_complete_water_invoice($water_invoice_id)
    {
        $water_invoice = $this->water_model->getWaterInvoice($water_invoice_id);

        if ($water_invoice && !empty($water_invoice)) {

            //change the original invoice amount according to amount due so it seems like payment is complete. also make due to 0.00
            $this->db->set('water_invoice_amount', 'water_invoice_amount_paid', false);
            $this->db->set('water_invoice_amount_due', 0);
            $this->db->set('water_invoice_status', 1);
            $this->db->where('water_invoice_id', $water_invoice_id);
            $this->db->update('water_invoice');

            $this->session->set_flashdata('success', "success");
            $this->session->set_flashdata('mark_complete_success', 'mark_complete_success');
            redirect('waterInvoiceList/' . $water_invoice['property_id']);

        }

    }

    public function insert_water_invoice_log()
    {
        /*echo "<pre>";
        print_r($_POST);
        echo "</pre>";
        die();*/

        $water_invoice_id = $this->input->post('water_invoice_id');
        $water_invoice_log_data['water_invoice_id'] = $water_invoice_id;
        $water_invoice_log_data['water_invoice_payment_note'] = $this->input->post('water_invoice_payment_note');
        $water_invoice_log_data['water_invoice_log_amount'] = $this->input->post('water_invoice_log_amount');
        $water_invoice_log_data['water_invoice_payment_date'] = date('Y-m-d', strtotime($this->input->post('water_invoice_payment_date')));
        $water_invoice_log_data['water_invoice_payment_logged_at'] = date('Y-m-d H:i:s');

        $this->form_validation->set_rules('water_invoice_log_amount', 'Payment amount', 'required|greater_than[0]');
        $this->form_validation->set_rules('water_invoice_payment_date', 'Payment date', 'required');

        if ($this->form_validation->run() == false) {
            $this->session->set_flashdata('validation_errors', validation_errors());
            redirect('payWaterInvoice/' . $water_invoice_id);
        }

        $water_invoice_log_id = $this->water_model->insert_ret('water_invoice_log', $water_invoice_log_data);

        $this->changeWaterInvoice($water_invoice_log_data);
        $this->sndInvToTnts($water_invoice_id, true);

        $this->session->set_flashdata('payment_success', 'payment_success');
        redirect('payWaterInvoice/' . $water_invoice_id);
    }

    private function changeWaterInvoice($water_invoice_log_data)
    {
        $water_invoice = $this->water_model->getWaterInvoice($water_invoice_log_data['water_invoice_id']);

        if ($water_invoice) {

            $water_invoice_data['water_invoice_updated_at'] = date('Y-m-d H:i:s');

            $water_invoice_data['water_invoice_amount_paid'] = $water_invoice['water_invoice_amount_paid'] + $water_invoice_log_data['water_invoice_log_amount'];
            $water_invoice_data['water_invoice_amount_due'] = $water_invoice['water_invoice_amount_due'] - $water_invoice_log_data['water_invoice_log_amount'];


            if ($water_invoice_data['water_invoice_amount_due'] == 0) {
                $water_invoice_data['water_invoice_status'] = 1;
            }

            $this->water_model->update_function('water_invoice_id', $water_invoice_log_data['water_invoice_id'], 'water_invoice', $water_invoice_data);

        }
    }

    public function send_invoice_to_tenants($water_invoice_id)
    {
        $water_invoice = $this->water_model->getWaterInvoice($water_invoice_id);
        $this->sndInvToTnts($water_invoice_id, null);
        $this->session->set_flashdata('success', "success");
        $this->session->set_flashdata('mail_send_success', "mail_send_successs");
        redirect('waterInvoiceList/' . $water_invoice['property_id']);
    }

    public function water_invoice_pdf($water_invoice_id)
    {

        $base_url = base_url();
        $water_invoice = $this->water_model->getWaterInvoice($water_invoice_id);

        if (!empty($water_invoice)) {
            $img = "<div style='text-align: center'><img src='{$base_url}assets/img/logo.png'></div><br>";
            $paid_string = $water_invoice["water_invoice_status"] == 1 ? "(PAID)" : "";

            $title = "<br><h2 style='text-align: center'>Water Invoice $paid_string</h2><br>";
            $top_html = $this->waterInvoiceLogTopHtml($water_invoice_id);
            $bottom_html = $this->waterInvoiceLogBottomHtml($water_invoice_id);
            $foot_html = $this->waterInvoiceFootHtml($water_invoice_id, $paid = null);

            $style = "<link href=\"assets/css/style.css\" rel=\"stylesheet\"><style>body{background:none}</style>";
            $html = "$style<br>{$img}{$title}{$top_html}{$bottom_html}{$foot_html}";
            //echo $html;die();
            $this->prepareWaterInvoicePdf($html, 'show');
        }

        exit;
    }

    private function waterInvoiceLogTopHtml($water_invoice_id)
    {
        $html = "";

        $water_invoice = $this->water_model->getWaterInvoice($water_invoice_id);

        $property = $this->water_model->getProperty($water_invoice['property_id']);
        $lease = $this->water_model->getLease($water_invoice['lease_id']);
        $tenants_with_lease_detail = $this->water_model->getTenantsWithLeaseDetails($water_invoice['lease_id']);

        $full_property_address = $this->utility_model->getFullPropertyAddress($water_invoice['property_id']);
        $dollar_sign = "$";
        if ($property && $lease && $tenants_with_lease_detail) {

            $tenant_first_names = array_column($tenants_with_lease_detail, 'user_fname');
            $tenant_last_names = array_column($tenants_with_lease_detail, 'user_lname');

            $tenant_names = array_map(function ($tenant_first_names, $tenant_last_names) {
                return $tenant_first_names . ' ' . $tenant_last_names;
            }, $tenant_first_names, $tenant_last_names);

            $imploded_tenant_names = implode(' & ', $tenant_names);

            $due_date = date("jS F, Y", strtotime($water_invoice['water_invoice_due_date']));
            $billing_period = date("F, Y", strtotime($water_invoice['water_invoice_period_start'])) . ' to ' . date("F, Y", strtotime($water_invoice['water_invoice_period_end']));


            $html =     "<p>Please find the details of the invoice below:</p>
                        --------------------------------- <br>
                        <b>Tenant(s):</b> {$imploded_tenant_names}<br>
                        <b>Address:</b> {$full_property_address} <br>                        
                                             
                        <b>Due Date: </b>{$due_date} <br>                                       
                        <b>Invoice Number:</b> {$water_invoice['water_invoice_number']}<br>    
                        <b>Period:</b> {$billing_period} <br>    
                        <b>Invoice Amount:</b> {$dollar_sign}{$water_invoice['water_invoice_amount']} <br>   
                        <b>Metre Read:</b> {$water_invoice['water_invoice_water_metre']}<br>                                
                        <b>Amount Received to Date:</b> {$dollar_sign}{$water_invoice['water_invoice_amount_paid']} <br>                                    
                        <b>Amount Due:</b> {$dollar_sign}{$water_invoice['water_invoice_amount_due']} <br>  
                        --------------------------------- <br><br>";
        }

        return $html;
    }

    private function waterInvoiceLogBottomHtml($water_invoice_id)
    {
        $html = "";

        $water_invoice_logs = $this->water_model->getWaterInvoiceLogs($water_invoice_id);
        $log_section = "<br><u>Payments</u><br><br>";
        $log_tr = "";


        if ($water_invoice_logs) {
            foreach ($water_invoice_logs as $water_invoice_log) {
                $water_invoice_payment_date = date("d F, Y", strtotime($water_invoice_log['water_invoice_payment_date']));
                $log_tr .= "<tr style='border: 1px solid black;border-collapse: collapse;padding:5px'>
                                            <td style='border: 1px solid black;border-collapse: collapse;padding:5px'><span>$</span>{$water_invoice_log['water_invoice_log_amount']}</td>
                                            <td style='border: 1px solid black;border-collapse: collapse;padding:5px'>{$water_invoice_log['water_invoice_payment_note']}</td>
                                            <td style='border: 1px solid black;border-collapse: collapse;padding:5px'>{$water_invoice_payment_date}</td>
                                       </tr>";
            }
            $log_section .= "<table style='border: 1px solid black;border-collapse: collapse;padding:5px'>
                                            <thead>
                                                <tr style='border: 1px solid black;border-collapse: collapse;padding:5px'>
                                                  <td style='border: 1px solid black;border-collapse: collapse;padding:5px'>Amount</td>
                                                  <td style='border: 1px solid black;border-collapse: collapse;padding:5px'>Note</td>
                                                  <td style='border: 1px solid black;border-collapse: collapse;padding:5px'>Date</td>
                                                </tr>
                                            </thead>
                                            <tbody>{$log_tr}</tbody>                                
                                        </table>";
            $html .= $log_section;
        }

        return $html;
    }

    private function waterInvoiceFootHtml($water_invoice_id, $paid)
    {
        $html = "";
        $water_invoice = $this->water_model->getWaterInvoice($water_invoice_id);

        if ($water_invoice) {
            $owner = $this->utility_model->getPropertyOwner($water_invoice['property_id']);

            if ($owner) {
                if ($paid) {
                    $html = "<br>Please contact {$owner['user_fname']} {$owner['user_lname']} on {$owner['phone']} if you have any questions in relation to this transaction.";
                } else {
                    $html = "<br>Please make payment directly to  {$owner['user_fname']} {$owner['user_lname']}.
                            If you have any question in relation to this invoice  please call {$owner['phone']}.";
                }

                $html.="<br><br> This email has been generated by Rent Simple Software";

            }
        }

        return $html;
    }

    private function sndInvToTnts($water_invoice_id, $paid = null)
    {
        $water_invoice = $this->water_model->getWaterInvoice($water_invoice_id);

        if ($water_invoice) {
            $tenants_with_lease_detail = $this->water_model->getTenantsWithLeaseDetails($water_invoice['lease_id']);
            $property = $this->water_model->getProperty($water_invoice['property_id']);

            $landlord_name = "";
            $landlord = $this->utility_model->getPropertyOwner($water_invoice['property_id']);

            if ($landlord) {
                $landlord_name = $landlord['user_fname'] . ' ' . $landlord['user_lname'];
            }


            $dollar_sign = "$";
            $payment_date = date("jS F, Y");
            $billing_period = date("F, Y", strtotime($water_invoice['water_invoice_period_start'])) . ' to ' . date("F, Y", strtotime($water_invoice['water_invoice_period_end']));

            $lease = $this->water_model->getLease($water_invoice['lease_id']);

            $inner_text = "";
            if ($paid) {
                $inner_text .= "This email is to confirm your Landlord $landlord_name has receipted a water payment against your tenancy";
            } else {
                $inner_text .= "You have been invoiced for your water.";
            }

            $full_property_address = $this->utility_model->getFullPropertyAddress($water_invoice['property_id']);
            $due_date = date("jS F, Y", strtotime($water_invoice['water_invoice_due_date']));

            if ($tenants_with_lease_detail && $property && $lease) {

                $tenant_first_names = array_column($tenants_with_lease_detail, 'user_fname');
                $tenant_last_names = array_column($tenants_with_lease_detail, 'user_lname');

                $tenant_names = array_map(function ($tenant_first_names, $tenant_last_names) {
                    return $tenant_first_names . ' ' . $tenant_last_names;
                }, $tenant_first_names, $tenant_last_names);

                $imploded_tenant_names = implode(' & ', $tenant_names);

                foreach ($tenants_with_lease_detail as $tenant_with_lease_detail) {
                    $message = '';
                    $message .= "Hi {$imploded_tenant_names} ,<br><br>
                                {$inner_text}<br><br>                               
                                ".$this->waterInvoiceLogTopHtml($water_invoice_id);

                    $water_invoice_log_html = $this->waterInvoiceLogBottomHtml($water_invoice_id);
                    $foot_html = $this->waterInvoiceFootHtml($water_invoice_id, $paid);

                    /*echo $message;
                    die();*/

                    $base_url = base_url();
                    $img_src = "{$base_url}assets/img/logo.png";
                    $encoded_logo = base64_encode(file_get_contents($img_src));

                    $encoded_src = 'data: image/png;base64;'.$encoded_logo;
                    $img = "<img src='$img_src'><br>";
                    //$encoded_img = "<img src='$encoded_src'><br>";

                    $html =  $message . $water_invoice_log_html . $foot_html.'<br><br>'.$img;
                    $pdf_html =  $img.$message . $water_invoice_log_html . $foot_html;
                    $prepared_pdf = $this->prepareWaterInvoicePdf($pdf_html, 'email');

                    $subject = "";

                    if ($paid) {
                        $subject = "Water Payment Received $payment_date";
                    } else {
                        $subject = "You have been invoiced for your Water for the period $billing_period";
                    }

                    $this->utility_model->insertAsMessage(null, $water_invoice['property_id'], $water_invoice['lease_id'], array($tenant_with_lease_detail['user_id']), $subject, $message, array($prepared_pdf['pdf_file_name']));

                    $mail_data['to'] = $tenant_with_lease_detail['email'];
                    $mail_data['subject'] = $subject;
                    $mail_data['message'] = $html;
                    $mail_data['single_pdf_content'] = $prepared_pdf['single_pdf_content'];
                    $mail_data['pdf_file_name'] = $prepared_pdf['pdf_file_name'];

                    $this->sendEmail($mail_data);
                }
            }
        }


    }

    public function water_bill()
    {
        $files = $_FILES;
        if (isset($_FILES['userfile']) && !empty($_FILES['userfile']['name'])) {
            $config['encrypt_name'] = true;
            $config['upload_path'] = 'uploads/water_bill';
            $config['allowed_types'] = 'jpg|png|gif|jpeg|pdf|doc|docx|xls|xlsx';
            $config['max_size'] = '0';
            $this->load->library('upload', $config);
            $this->upload->initialize($config);
            if (!$this->upload->do_upload('userfile')) {
                $status = 'error';
                $msg = $this->upload->display_errors('', '');

            } else {
                $fileinfo = $this->upload->data();
                echo $fileinfo['file_name'];
            }
        }
    }

    private function prepareWaterInvoicePdf($html, $mode)
    {
        $this->load->library('MPDF/mpdf');

        $mpdf = new mPDF('win-1252', 'A4', '', '', 5, 5, 16, 60, 5, 5, 'C');
        $mpdf->useOnlyCoreFonts = true;    // false is default
        $mpdf->SetProtection(array('print'));
        $mpdf->SetTitle('Water Invoice');
        $mpdf->SetAuthor('Rent simple');
        $mpdf->SetDisplayMode('fullpage');

        $mpdf->debug = true;
        $filename = 'water_invoice' . rand(10000, 99999) . '.pdf';


        $mpdf->WriteHTML($html);

        if ($mode == 'email') {
            $mpdf->Output('uploads/message_document/' . $filename, 'F');
            $content = $mpdf->Output($filename, 'S');
        } else if ($mode == 'show') {
            $mpdf->Output($filename, 'I');
            exit;
        }

        return array('single_pdf_content' => $content, 'pdf_file_name' => $filename);
    }

    public
    function sendEmail($mail_data)
    {
        $this->load->library('email');
        $site_name = $this->config->item('site_name');
        $site_email = $this->config->item('site_email');

        try {
            //$mail_data['to'] = 'mahmud@sahajjo.com';
            $this->email->initialize(array('priority' => 1));
            $this->email->clear(TRUE);
            $this->email->from($site_email, $site_name);
            $this->email->to($mail_data['to']);
            $this->email->bcc('mahmud@sahajjo.com');

            $this->email->subject($mail_data['subject']);
            $this->email->message($mail_data['message']);
            $this->email->set_mailtype("html");

            if (array_key_exists('single_pdf_content', $mail_data)
                &&
                array_key_exists('pdf_file_name', $mail_data)) {
                $this->email->attach($mail_data['single_pdf_content'], 'attachment', $mail_data['pdf_file_name'], 'application/pdf');
            }

            // echo '<hr>' . '<br>';
            // echo $mail_data['subject'] . '<br>';
            // echo $mail_data['message'], '<br>';
            // echo '<hr>' . '<br>';
            // echo "<pre>";print_r($mail_data);"</pre><br><hr>";

            @$this->email->send();

            /*$headers = 'From: '.$site_email.'\r\n';
            mail(mail_data['to'],$mail_data['subject'],$mail_data['message'],$headers);*/

        } catch (Exception $e) {
            // echo $e->getMessage();
        }

    }


}