<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Login extends MX_Controller
{

    //public $counter=0;
    function __construct()
    {
        parent::__construct();
        $this->load->model('login_model');
        $this->load->model('home/home_model');
        $this->load->model('utility/utility_model');
        $this->load->helper('inflector');
        $this->load->library('encrypt');

        if ($this->session->userdata('user_id') && !$this->utility_model->is_user_inactive($this->session->userdata('user_id'))) {
            redirect('property', 'refresh');
        }

        require_once(APPPATH . 'libraries/stripe-php/init.php');
    }

    public function index()
    {
        // $data['company_info']=$this->home_model->select_with_where('*', 'id=1', 'company');
        $this->load->view('index');
    }

    public function login_check()
    {
        $email = $this->input->post('email');
        $password = $this->encryptIt($this->input->post('password'));
        $res = $this->login_model->check_login($email, $password);
        // echo "<pre>";print_r($res);die();
        if (count($res) > 0) {
            $this->session->set_userdata('user_id', $res[0]['user_id']);
            $this->session->set_userdata('email', $res[0]['email']);
            $this->session->set_userdata('role_type', $res[0]['role_type']);
            $this->session->set_userdata('user_fname', $res[0]['user_fname']);
            $this->session->set_userdata('user_lname', $res[0]['user_lname']);
            $this->session->set_userdata('login_scc', 'Welcome to Admin Dashboard.');

            if (!$this->utility_model->get_software_validity($this->session->userdata('user_id'))) {
                redirect('subscribe', 'refresh');
            }
            /*$gg = $this->utility_model->is_user_inactive($this->session->userdata('user_id'));
            echo "[$gg]";exit;*/


            if ($this->utility_model->is_user_inactive($this->session->userdata('user_id'))) {
                $this->session->set_flashdata('deactivated', 'User is deactivated');
                redirect('login');
            }

            redirect('property', 'refresh');
        } else {
            $this->session->set_userdata('log_err', 'Email or Password is Incorrect.');
            redirect('login', 'refresh');
        }

    }


    function decryptIt($q)
    {
        $cryptKey = 'Lf6Q5htqdgnSn0AABqlsSddj1QNu0fJs';
        $qDecoded = rtrim(mcrypt_decrypt(MCRYPT_RIJNDAEL_256, md5($cryptKey), base64_decode($q), MCRYPT_MODE_CBC, md5(md5($cryptKey))), "\0");
        return ($qDecoded);
    }

    function encryptIt($q)
    {
        $cryptKey = 'Lf6Q5htqdgnSn0AABqlsSddj1QNu0fJs';
        $qEncoded = base64_encode(mcrypt_encrypt(MCRYPT_RIJNDAEL_256, md5($cryptKey), $q, MCRYPT_MODE_CBC, md5(md5($cryptKey))));
        return ($qEncoded);
    }

}
