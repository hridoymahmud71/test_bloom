<!DOCTYPE html>
<base href="<?php echo base_url(); ?>">
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Rent Simple</title>
    <link href="assets/css/bootstrap.min.css" rel="stylesheet">
    <link href="assets/css/style.css" rel="stylesheet">

    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body>
<div class="login">
    <div class="container">
        <div class="row">
            <div class="main">
                <div class="logo"><img src="assets/img/logo.png"/></div>
                <h3>Welcome to Rent Simple! <br>
                    <!-- <a href="registration">Sign Up</a> -->
                </h3>
                <div class="row" id="login_div">
                    <form role="form" name="login_submit" onsubmit="return check_login()" action="login/login_check"
                          method="post">
                        <?php if ($this->session->flashdata('subscription_incomplete')) { ?>
                            <div class="alert alert-danger">
                                <button class="close" data-close="alert"></button>
                                <span><?= $this->session->flashdata('subscription_complete'); ?></span>
                            </div>
                        <?php } ?>
                        <?php if ($this->session->flashdata('subscription_complete')) { ?>
                            <div class="alert alert-success">
                                <button class="close" data-close="alert"></button>
                                <span><?= $this->session->flashdata('subscription_incomplete'); ?></span>
                            </div>
                        <?php } ?>
                        <?php if ($this->session->flashdata('deactivated')) { ?>
                            <div class="alert alert-danger">
                                <button class="close" data-close="alert"></button>
                                <span><?= $this->session->flashdata('deactivated'); ?></span>
                            </div>
                        <?php } ?>
                        <?php if ($this->session->userdata('log_err')) { ?>
                            <div class="alert alert-danger">
                                <button class="close" data-close="alert"></button>
                                <span><?= $this->session->userdata('log_err'); ?></span>
                            </div>
                        <?php }
                        $this->session->unset_userdata('log_err'); ?>

                        <?php if ($this->session->userdata('log_scc')) { ?>
                            <div class="alert alert-success">
                                <button class="close" data-close="alert"></button>
                                <span><?= $this->session->userdata('log_scc'); ?></span>
                            </div>
                        <?php }
                        $this->session->unset_userdata('log_scc'); ?>

                        <div class="alert alert-danger" id="err_div" style="display: none">
                            <strong id="err_msg"></strong>
                        </div>
                        <div class="form-group">
                            <label for="inputEmail">Email Address</label>
                            <input type="text" name="email" class="form-control" id="inputEmail">
                        </div>
                        <div class="form-group">
                            <a class="pull-right" href="" onclick="return get_forgot_pass_section()">Forgot
                                password?</a>
                            <label for="inputPassword">Password</label>
                            <input type="password" name="password" class="form-control" id="inputPassword">
                        </div>
                        <!-- <div class="checkbox pull-right">
                            <label>
                                <input type="checkbox">
                            Remember me </label>
                        </div> -->
                        <button type="submit" class="btn btn btn-primary ">
                            Log Into Your Account
                        </button>
                    </form>
                    <!--<div class="login-or">
                        <hr class="hr-or">
                        <span class="span-or">or</span>
                    </div>-->
                    <!-- <div class="col-xs-6 col-sm-6 col-md-6">
                            <a href="<?php echo $this->facebook->login_url(); ?>" class="btn btn-lg btn-primary btn-block ss_btn_facebook">Facebook</a>
                        </div>
                        <div class="col-xs-6 col-sm-6 col-md-6">
                            <a href="#" class="btn btn-lg btn-info ss_btn_google btn-block">Google</a>
                        </div> -->
                </div>
                <!--<div class="brand" id="brand_div">
                    <h4> As seen on:</h4>
                    <img src="assets/img/logo6.jpg" alt="logo"/>
                    <img src="assets/img/logo7.jpg" alt="logo"/>
                </div>-->
                <div style="display: none;" class="row" id="forget_div">
                    <form role="form" name="login_submit" onsubmit="return check_forgot_pass()" action="forget"
                          method="post">
                        <div class="alert alert-danger" id="err_forgot_div" style="display: none">
                            <strong id="err_forgot_msg"></strong>
                        </div>
                        <div class="form-group">
                            <label for="inputEmail">Email Address</label>
                            <input type="email" name="email" class="form-control" id="inputforgotEmail">
                        </div>
                        <div class="text-center"><input type="submit" class="btn btn-primary" name=""></div>
                    </form>
                </div>
            </div>

        </div>
    </div>
</div>


<script>
    function check_forgot_pass() {
        $("#err_forgot_div").hide();
        //var regex = new RegExp("^(?:\\+?88)?8801[15-9]\\d{8}$");
        var ok = true;
        var email = $("#inputforgotEmail").val();
        if (email == '') {
            $("#err_forgot_div").show();
            $("#err_forgot_msg").html('* Email fields must be filled out');
            ok = false;
        }
        else {
            $.ajax({
                type: "POST",
                url: "forget/ajax_check_email",
                data: 'email=' + email,
                success: function (data) {
                    if (data == 1) {
                        ok = true;
                    }
                    else {
                        $("#err_forgot_div").show();
                        $("#err_forgot_msg").html('* Email address is not valid');
                        ok = false;
                    }
                }
            });
        }
        return ok;
    }
</script>

<script>
    function check_login() {
        $("#err_div").hide();
        //var regex = new RegExp("^(?:\\+?88)?8801[15-9]\\d{8}$");
        var ok = true;
        var email = $("#inputEmail").val();
        var password = $("#inputPassword").val();
        if (email == '' || password == "") {
            $("#err_div").show();
            $("#err_msg").html('* All fields must be filled out');
            ok = false;
        }
        return ok;
    }
</script>

<script type="text/javascript">
    function get_forgot_pass_section() {
        $('#login_div').hide();
        $('#brand_div').hide();
        $('#forget_div').show();
        return false;
    }
</script>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<script src="assets/js/bootstrap.min.js"></script>
<!-- <script src="assets/js/custom.js"></script> -->
</body>
</html>
