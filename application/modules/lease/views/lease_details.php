<?php $this->load->view('front/headlink'); ?>
<style>
    #signed_lease_agreement {
        display: none;
    }

    #uploaded_file {
        display: none;
    }

    #remove_file {
        display: none;
    }

    .error {
        border: 1px solid crimson;
    }
</style>
<?php $this->load->view('front/top_menu'); ?>
<div class="container">
    <div class="ss_schedule">

        <form class="container form-horizontal" onsubmit="return chk_lease_detail()" action="lease/update_lease"
              method="post"

              enctype="multipart/form-data">
            <div class="row">

                <?php $this->load->view('front/head_nav'); ?>
                <h4 class="rm_top_btn row">
                    <div class="col-sm-7">Current Lease: <?= $lease_detail[0]['lease_name'] ?>
                        <?php
                        $lease_time_diff = (int)((strtotime($lease_detail[0]['lease_end_date']) - strtotime(date('Y-m-d'))) / 86400);
                        $lease_time_diff_string = sprintf("(%d days to go)", $lease_time_diff);

                        if ($lease_time_diff < 0) {
                            $lease_time_diff_string = sprintf("(ended %d day(s) ago)", abs($lease_time_diff));
                        }
                        ?>
                        <strong><?= $lease_time_diff_string ?></strong>
                        <a href="#" data-toggle="modal" style="display: none" id="settings_btn"><span
                                    class="glyphicon glyphicon-cog"
                                    aria-hidden="true"></span></a>
                    </div>

                    <div class="col-sm-5">

                        <?php if ($lease_detail[0]['lease_end_requested'] == 0 && $lease_time_diff >= 0){ ?>
                        <div class="row">
                            <?php if(!$is_archived) { ?>
                            <div class="col-md-4">
                                <button type="button" class="btn btn-info pull-right" data-toggle="modal"
                                        data-target="#extend_lease_modal">
                                    Extend Lease
                                </button>
                            </div>
                            <div class="col-md-4">
                                <button type="button" class="btn btn-info pull-right" data-toggle="modal"
                                        data-target="#change_rent_modal">
                                    Change Rent
                                </button>
                            </div>
                            <div class="col-md-4">
                                <button type="button" class="btn btn-info pull-right" data-toggle="modal"
                                        data-target="#end_lease_modal">
                                    End Lease
                                </button>
                            </div>
                                <?php } ?>

                            <?php } else { ?>
                                <div class="row">
                                    Refund amount : $<?= $lease_detail[0]['lease_refundable'] ?>
                                </div>
                            <?php } ?>

                        </div>


                    </div>

                    <div style="clear: both;"></div>
                </h4>
                <?php if ($this->session->flashdata('success')) { ?>
                    <div class="row ">
                        <div class="col-md-3">

                        </div>
                        <div class="col-md-6">
                            <div class="panel panel-primary">
                                <div class="panel-heading">Success!</div>
                                <?php if ($this->session->flashdata('lease_extend_success') && !$this->session->flashdata('rent_change_success')) { ?>
                                    <div class="panel-body">
                                        <p>Successfully extended the lease up
                                            to <?= $this->session->flashdata('flash_new_lease_end_date') ?></p>
                                        <p>Upload a copy of the lease renewal <a
                                                    href="leaseDocumentAdd/<?= $property_id ?>">here</a></p>
                                    </div>
                                <?php } ?>
                                <?php if ($this->session->flashdata('rent_change_success')) { ?>
                                    <div class="panel-body">
                                        <p>Successfully changed the rental amount effective from <?= $this->session->flashdata('rent_effective_date') ?></p>
                                    </div>
                                <?php } ?>
                            </div>
                        </div>
                    </div>
                <?php } ?>

                <?php if ($this->session->flashdata('error')) { ?>
                    <div class="row ">
                        <div class="col-md-3">

                        </div>
                        <div class="col-md-6">
                            <div class="panel panel-danger">
                                <div class="panel-heading">Unsuccessful!</div>
                                <?php if ($this->session->flashdata('validation_errors')) { ?>
                                    <div class="panel-body">
                                        <?= $this->session->flashdata('validation_errors') ?>
                                    </div>
                                <?php } ?>
                                <?php if ($this->session->flashdata('vacate_lease_end_date_conflict')) { ?>
                                    <div class="panel-body">
                                        <p>Vacate date should be lower than lease end date</p>
                                    </div>
                                <?php } ?>
                                <?php if ($this->session->flashdata('lease_end_unsuccessful')) { ?>
                                    <div class="panel-body">
                                        <p>Lease could not be ended</p>
                                    </div>
                                <?php } ?>
                            </div>
                        </div>
                    </div>
                <?php } ?>

                <div class="row">
                    <div class="col-xs-12">
                        <div class="col-md-12 mrwell text-center">
                            <h3 class="extra_heading">Lease & Rent Details <small>for the property at
                                    <?= $this->utility_model->getFullPropertyAddress($property_info[0]['property_id'])?></small>
                            </h3>
                            <div class="row form-group">
                                <br/>
                                <label for="inputLeaseName" class="col-sm-4 control-label">Lease Name</label>
                                <div class="col-sm-8">
                                    <input readonly type="text" name="lease_name" value="<?= $lease_detail[0]['lease_name']; ?>"
                                           class="form-control" id="inputLeaseName" placeholder="Lease Name">
                                </div>
                            </div>

                            <h4 class="form_heading">Lease dates</h4>
                            <br/>
                            <div class=" form-group row">
                                <label class="col-sm-4 control-label">from:</label>
                                <div class="col-sm-8">
                                    <input class="form-control" readonly type="text" id="rangeBa"
                                           name="lease_start_date"
                                           value="<?= date("jS F, Y", strtotime($lease_detail[0]['lease_start_date'])); ?>">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-4 control-label">to:</label>
                                <div class="col-sm-8">
                                    <input class="form-control" readonly type="text" id="rangeBb" name="lease_end_date"
                                           value="<?= date("jS F, Y", strtotime($lease_detail[0]['lease_end_date'])); ?>">
                                </div>
                            </div>


                            <br/> <br/>


                            <?php
                            $uploaded_file_exists = false;
                            $uploaded_file_url = false;
                            $uploaded_file_name = false;
                            $uploaded_file_href = ' href="#" ';
                            if (($lease_detail)) {

                                if ($lease_detail[0]['lease_file'] != null && $lease_detail[0]['lease_file'] != false && trim($lease_detail[0]['lease_file']) != '') {
                                    $uploaded_file_exists = true;
                                    $uploaded_file_name = $lease_detail[0]['lease_file'];
                                    $uploaded_file_href = ' href="' . base_url() . '/uploads/image_and_file/file/' . $lease_detail[0]['lease_file'] . '" ';
                                }
                            } ?>

                            <?php if ($uploaded_file_exists) { ?>
                                <div class="form-group row text-left">
                                    <label class="col-sm-4 control-label">Signed lease agreement</label>
                                    <div class="col-sm-8" style="line-height: 35px">
                                        <span class="file_upload_wrapper" style="display: none;">
                                            <input id="signed_lease_agreement" name="lease_file" type="file"
                                                   style="position: relative; left: 0px;">
                                        </span>
                                        <span>
                                            <a download="download" id="uploaded_file" <?= $uploaded_file_href ?> >
                                                <?= $uploaded_file_name ? $uploaded_file_name : 'no-file' ?>
                                            </a>
                                            <i id="remove_file"  class="glyphicon glyphicon-remove"></i>
                                        </span>
                                    </div>
                                </div>
                            <?php } ?>

                            <?php if (!empty($bond_img_recieve_info)) { ?>
                                <div class="form-group row text-left" >
                                    <label class="col-sm-4 control-label">Other documents</label>
                                    <div class="col-sm-8">

                                        <ul class="list-group">
                                            <?php foreach ($bond_img_recieve_info as $row) { ?>
                                                <?php if (!empty($row['recieve_image'])) { ?>
                                                    <li class="list-group-item"><a download
                                                           href="uploads/lease/<?= $row['recieve_image'] ?>"><?= $row['recieve_image'] ?></a>
                                                    </li>
                                                <?php } ?>
                                            <?php } ?>
                                        </ul>

                                    </div>
                                </div>
                            <?php } ?>

                            <!--<pre>
                                <?php /*print_r($lease_documents)*/ ?>
                            </pre>-->

                            <?php if (!empty($lease_documents)) { ?>
                                <div class="form-group row text-left" >
                                    <label class="col-sm-4 control-label">Lease documents</label>
                                    <div class="col-sm-8">
                                        <ul class="list-group">
                                            <?php foreach ($lease_documents as $lease_document) { ?>
                                                <?php if (!empty($lease_document['document_detail'])) { ?>
                                                    <li class="list-group-item">
                                                        <a  download
                                                           href="uploads/lease/<?= $lease_document['document_detail'] ?>">
                                                            <?= $lease_document['document_detail'] ?>
                                                        </a>
                                                    </li>
                                                <?php } ?>
                                            <?php } ?>
                                        </ul>
                                    </div>
                                </div>

                            <?php } ?>


                            <br/>

                            <div style="clear: both;"></div>
                            <div class="text-left">

                                <h4 class="form_heading">Rent payments</h4>

                                <div class="form-group row">
                                    <label class="col-sm-4 control-label">How often?</label>
                                    <div class="col-sm-8">
                                        <select class="form-control" disabled="disabled" id="inputday"
                                                name="lease_pay_type">
                                            <option <?php if ($lease_detail[0]['lease_pay_type'] == 1) {
                                                echo "selected";
                                            } ?> id="Weekly" value="1" selected="selected">Weekly
                                            </option>
                                            <option <?php if ($lease_detail[0]['lease_pay_type'] == 2) {
                                                echo "selected";
                                            } ?> id="Fortnightly" value="2">Fortnightly
                                            </option>
                                            <option <?php if ($lease_detail[0]['lease_pay_type'] == 3) {
                                                echo "selected";
                                            } ?> id="4Weekly" value="3">4 Weekly
                                            </option>
                                            <option <?php if ($lease_detail[0]['lease_pay_type'] == 4) {
                                                echo "selected";
                                            } ?> id="Monthly" value="4">Monthly
                                            </option>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-4 control-label"> How much per period?</label>
                                    <div class="col-sm-8">
                                        <input readonly type="text" class="form-control" name="lease_per_period_payment"
                                               value="<?= $lease_detail[0]['lease_per_period_payment']; ?>"
                                               id="howmuchperperiod" placeholder="$0">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-4 control-label">Payday:</label>
                                    <div class="col-sm-8">
                                        <select class="form-control" disabled="disabled"
                                                id="inputhowOftenday" <?php if (($lease_detail)) {
                                            if ($lease_detail[0]['lease_pay_type'] == 4) {
                                                echo "style='display: none;'";
                                            } else {
                                                echo "name='lease_pay_day'";
                                            }
                                        } else {
                                            echo "name='lease_pay_day'";
                                        } ?>>
                                            <option <?php if ($lease_detail[0]['lease_pay_day'] == 1) {
                                                echo "selected";
                                            } ?> id="Monday" value="1" selected="selected">Monday
                                            </option>
                                            <option <?php if ($lease_detail[0]['lease_pay_day'] == 2) {
                                                echo "selected";
                                            } ?> id="Tuesday" value="2">Tuesday
                                            </option>
                                            <option <?php if ($lease_detail[0]['lease_pay_day'] == 3) {
                                                echo "selected";
                                            } ?> id="Wednesday" value="3">Wednesday
                                            </option>
                                            <option <?php if ($lease_detail[0]['lease_pay_day'] == 4) {
                                                echo "selected";
                                            } ?> id="Thursday" value="4">Thursday
                                            </option>
                                            <option <?php if ($lease_detail[0]['lease_pay_day'] == 5) {
                                                echo "selected";
                                            } ?> id="Friday" value="5">Friday
                                            </option>
                                            <option <?php if ($lease_detail[0]['lease_pay_day'] == 6) {
                                                echo "selected";
                                            } ?> id="Saturday" value="6">Saturday
                                            </option>
                                            <option <?php if ($lease_detail[0]['lease_pay_day'] == 7) {
                                                echo "selected";
                                            } ?> id="Sunday" value="7">Sunday
                                            </option>
                                        </select>
                                        <select class="form-control" disabled="disabled"
                                                id="inputmonthDetail" <?php if (($lease_detail)) {
                                            if ($lease_detail[0]['lease_pay_type'] != 4) {
                                                echo "style='display: none;'";
                                            } else {
                                                echo "name='lease_pay_day'";
                                            }
                                        } else {
                                            echo "style='display: none;'";
                                        } ?>>
                                            <?php for ($i = 1; $i <= 28; $i++) { ?>
                                                <option <?php if ($lease_detail[0]['lease_pay_day'] == $i) {
                                                    echo "selected";
                                                } ?> value="<?= $i; ?>">
                                                    <?= $i; ?>
                                                    <?php if ($i == 1) {
                                                        echo 'st';
                                                    } elseif ($i == 2) {
                                                        echo 'nd';
                                                    } elseif ($i == 3) {
                                                        echo 'rd';
                                                    } else {
                                                        echo "th";
                                                    } ?> of the month
                                                </option>
                                            <?php } ?>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-12" style="display:none">
                                    <h3>Which tenant(s) actually pay you the rent?</h3>
                                    <table class="table p-summary" border="0" cellspacing="0" cellpadding="4">
                                        <tbody>
                                        <tr class="table_tenants_title">
                                            <th scope="col" class="pull-right">Name</th>
                                            <th scope="col"></th>
                                            <th scope="col">Share Paid</th>
                                            <th scope="col">Pay Method</th>
                                        </tr>
                                        <?php foreach ($all_tenant as $key => $row) { ?>
                                            <tr class="tenant_payment_detail">
                                                <td class="pull-right">
                                                    <input class="form-control first_name_checkbox custom-checkbox"
                                                           style="margin-bottom: 9px;width:40px;" type="checkbox"
                                                           id="payment_status<?= $key; ?>" name="payment_status[]"
                                                           value="<?= $key; ?>" <?php if ($row['payment_status'] == 1) {
                                                        echo 'checked="checked"';
                                                    } ?>>
                                                    <input type="hidden" name="payment_status[]" value="0"/>
                                                </td>
                                                <td>
                                                    <p style="display:inline-block; margin: 0px;"
                                                       class="first_name_text"><?= $row['user_fname'] ?> <?= $row['user_lname'] ?></p>
                                                </td>
                                                <td>
                                                    <input class="form-control shared_amount_cls" type="text"
                                                           id="rent_amount_<?= $key; ?>" name="share_paid_amount[]"
                                                           value="<?= $row['share_paid_amount'] ?>">
                                                </td>
                                                <td>
                                                    <select class="form-control" disabled="disabled"
                                                            id="payment_method_<?= $key; ?>"
                                                            name="payment_method[]">
                                                        <option <?php if ($row['payment_method'] == 1) {
                                                            echo "selected";
                                                        } ?> value="1">Bank Transfer (EFT)
                                                        </option>
                                                        <option <?php if ($row['payment_method'] == 2) {
                                                            echo "selected";
                                                        } ?> value="2">Cash
                                                        </option>
                                                        <option <?php if ($row['payment_method'] == 3) {
                                                            echo "selected";
                                                        } ?> value="3">Cheque
                                                        </option>
                                                        <option <?php if ($row['payment_method'] == 4) {
                                                            echo "selected";
                                                        } ?> value="4">Credit Card
                                                        </option>
                                                        <option <?php if ($row['payment_method'] == 5) {
                                                            echo "selected";
                                                        } ?> value="5">Other method
                                                        </option>
                                                    </select>
                                                </td>
                                                <input type="hidden" name="tenant_id[]"
                                                       value="<?= $row['tenant_id']; ?>">
                                            </tr>
                                        <?php } ?>
                                        </tbody>
                                    </table>
                                </div>

                            </div>
                            <div>
                                <div class="row">
                                    <div class="col-sm-4"></div>
                                    <div class="col-sm-8 text-left" style="display: none"><h4>Total paid per period:
                                            $<span id="total_amount"
                                                   class="ss_price_text"><?= $lease_detail[0]['lease_per_period_payment']; ?></span>
                                        </h4>
                                    </div>
                                </div>
                            </div>
                            <br/>
                            <div class="row">
                                <div class="col-sm-4"></div>
                                <div class="col-sm-8 text-left">
                                    <input type="hidden" name="property_id" value="<?= $property_id; ?>">
                                    <input type="hidden" name="lease_id" value="<?= $lease_detail[0]['lease_id']; ?>">
                                    <input style="display: none;" type="submit" id="sub_btn" value="Save Changes"
                                           class="print_ledgers btn btn-primary btn-lg">
                                    <a href="Dashboard/<?= $property_id; ?>" class="btn btn-light btn-lg mark_paid">
                                        Back to Dashboard</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </form>

    </div>
</div>
<!-- Modal -->
<div class="modal fade" id="extend_lease_modal" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Extend the current lease agreement. Enter the new lease end date below:</h4>
            </div>
            <form action="leaseExtend" method="post" id="lease_extend">
                <div class="modal-body">
                    <input type="hidden" name="lease_id" value="<?= $lease_detail[0]['lease_id'] ?>">
                    <p>
                        <span style="color: red">
                            * by default the end date is set to 1 years time. You may edit this by selecting the correct date below
                        </span>
                    </p>
                    <div class="form-group">
                        <label for="new_lease_end_date">New lease end date:</label>
                        <input class="form-control" type="text" id="new_lease_end_date" readonly
                               lease-end-date="<?= date('Y,m,d', strtotime($lease_detail[0]['lease_end_date'])) ?>"
                               name="new_lease_end_date"
                               value="<?= date('j F, Y', strtotime("+1 year", strtotime($lease_detail[0]['lease_end_date']))); ?>">
                    </div>
                    <div class="form-group">
                        <label for="date">Will the rent be increased/decreased?</label>
                        <select name="rent_increment_check" class="form-control" id="rent_increment_check">
                            <option value="no" selected="selected">No</option>
                            <option value="yes">Yes</option>
                        </select>
                    </div>
                    <div id="rent_increment_wrapper" style="display: none;">
                        <div class="form-group">
                            <label for="new_lease_end_date">New rental amount:</label>
                            <input class="form-control" type="text" id="new_rent" name="new_rent" value="">
                        </div>
                        <div class="form-group col-md-6">
                            <select class="form-control" name="new_lease_pay_type" id="input_day">
                                <option id="weekly" value="1">Weekly</option>
                                <option id="fortnightly" value="2">Fortnightly</option>
                                <option id="monthly" value="4">Monthly</option>
                            </select>
                        </div>
                        <div class="form-group col-md-6" id="new_lease_pay_day_week_wrapper">
                            <select class="form-control" name="new_lease_pay_day_week">
                                <option value="1">Monday</option>
                                <option value="2">Tuesday</option>
                                <option value="3">Wednesday</option>
                                <option value="4">Thursday</option>
                                <option value="5">Friday</option>
                                <option value="6">Saturday</option>
                                <option value="7">Sunday</option>
                            </select>
                        </div>
                        <div class="form-group col-md-6" id="new_lease_pay_day_month_wrapper" style="display: none">
                            <select class="form-control" name="new_lease_pay_day_month">
                                <?php for ($i = 1; $i <= 28; $i++) { ?>
                                    <option value="<?= $i ?>">
                                        <?= $this->utility_model->addOrdinalNumberSuffix($i) ?> of the month
                                    </option>
                                <?php } ?>
                            </select>
                        </div>
                    </div>

                </div>
                <div class="modal-footer">
                    <button id="leaseExtendSubmitBtn" type="button" class="btn btn-success" >Confirm</button>
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </form>
        </div>

    </div>
</div>

<!-- Modal -->
<div class="modal fade" id="change_rent_modal" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Change the amount and frequency of rent</h4>
            </div>
            <form action="changeRent" method="post" id="change_rent">
                <div class="modal-body">
                    <input type="hidden" name="lease_id" value="<?= $lease_detail[0]['lease_id'] ?>">
                    <div class="form-group">
                        <label for="new_lease_end_date">Rent effective date</label>
                        <input class="form-control" type="text" name="rent_effective_date" id="rent_effective_date" readonly

                               min-rent-effective-date="<?php if (!empty($next_of_current_lease_rent_period_single)) {
                                   echo date('Y,m,d', strtotime($next_of_current_lease_rent_period_single['payment_start_period']));
                               } else {
                                   echo 'no_date';
                               } ?>"
                               max-rent-effective-date="<?php if (!empty($next_of_current_lease_rent_period_list_last_item)) {
                                   echo date('Y,m,d', strtotime($next_of_current_lease_rent_period_list_last_item['payment_end_period']));
                               } else {
                                   echo 'no_date';
                               } ?>"
                               name="rent_effective_date"
                               value="">
                    </div>

                    <div class="form-group">
                        <label for="new_rent">New rental amount:</label>
                        <input class="form-control" type="text" id="new_rent_for_rent_change" name="new_rent" value="">
                    </div>
                    <div class="form-group col-md-6">
                        <select class="form-control" name="new_lease_pay_type_" id="input_day_for_rent_change">
                            <option id="weekly" value="1">
                                Weekly
                            </option>
                            <option id="fortnightly" value="2">
                                Fortnightly
                            </option>
                            <option id="monthly" value="4">
                                Monthly
                            </option>
                        </select>
                    </div>
                    <div class="form-group col-md-6" id="new_lease_pay_day_week_wrapper_for_rent_change">
                        <select class="form-control" name="new_lease_pay_day_week">
                            <option value="1">
                                Monday
                            </option>
                            <option value="2">
                                Tuesday
                            </option>
                            <option value="3">
                                Wednesday
                            </option>
                            <option value="4">
                                Thursday
                            </option>
                            <option value="5">
                                Friday
                            </option>
                            <option value="6">
                                Saturday
                            </option>
                            <option value="7">
                                Sunday
                            </option>
                        </select>
                    </div>
                    <div class="form-group col-md-6" id="new_lease_pay_day_month_wrapper_for_rent_change"
                         style="display: none">
                        <select class="form-control" name="new_lease_pay_day_month">
                            <?php for ($i = 1; $i <= 28; $i++) { ?>
                                <option value="<?= $i ?>">
                                    <?= $this->utility_model->addOrdinalNumberSuffix($i) ?> of the month
                                </option>
                            <?php } ?>
                        </select>
                    </div>


                </div>
                <div class="modal-footer">
                    <button id="rentChangeSubmitBtn" type="button" class="btn btn-success">Confirm</button>
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </form>
        </div>

    </div>
</div>

<!-- Modal -->
<div class="modal fade" id="end_lease_modal" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">End the current lease agreement. Enter the new lease end date and vacate date
                    below:</h4>
            </div>
            <form action="leaseEnd" method="post" id="lease_end">
                <div class="modal-body">
                    <input type="hidden" name="lease_id" value="<?= $lease_detail[0]['lease_id'] ?>">
                    <div class="form-group">
                        <label for="vacate_date_to_end">Vacate Date</label>
                        <input class="form-control" type="text" id="vacate_date_to_end" name="vacate_date_to_end"
                               value="" readonly required="required">
                    </div>
                    <div class="form-group">
                        <label for="new_lease_end_date">New Lease End Date</label>
                        <input class="form-control" type="text" id="new_lease_end_date_to_end" readonly
                               required="required"
                               lease-start-date="<?= date('Y,m,d', strtotime($lease_detail[0]['lease_start_date'])) ?>"
                               lease-end-date="<?= date('Y,m,d', strtotime($lease_detail[0]['lease_end_date'])) ?>"
                               name="new_lease_end_date_to_end"
                               value="">
                    </div>

                </div>
                <div class="modal-footer">
                    <button type="submit" id="leaseEndSubmitBtn" class="btn btn-success">Confirm</button>
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </form>
        </div>

    </div>
</div>

<div class="container">
    <div class="modal fade" id="leaseModal" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Select a Lease or add a new lease</h4>
                </div>
                <form action="property/property_details/<?= $property_id; ?>" method="post">
                    <div class="modal-body">
                        <h4>Lease Option</h4>
                        <select class="form-control" name="lease_id" id="lease">
                            <option value="new_lease">Add a new lease</option>
                            <?php foreach ($all_lease as $row) { ?>
                                <option <?php if ($row['lease_id'] == $lease_detail[0]['lease_id']) {
                                    echo "selected";
                                } ?> value="<?= $row['lease_id']; ?>"
                                     disabled><?= $row['lease_name']; ?></option>
                            <?php } ?>
                        </select>
                    </div>
                    <div class="modal-footer">
                        <input type="submit" class="btn btn-success" value="Confirm" name="">
                    </div>
                </form>
            </div>
        </div>
    </div>
    <div class="modal fade" id="vacateModal" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Vacate Tenants For
                        Lease: <?= $lease_detail[0]['lease_name'] ?></h4>

                </div>
                <?php
                $vacatable = false;
                $current_date = date('Y-m-d');

                if (strtotime($lease_detail[0]['lease_end_date']) <= strtotime(date($current_date))) {
                    $vacatable = true;
                }
                ?>
                <form id="vacate_form" vacate_form_success="0"
                      lease_vacated="<?= $lease_detail[0]['lease_vacated'] ?>"
                      action="property/vacate_tenants/<?= $lease_detail[0]['lease_id'] ?>"
                      method="post">
                    <div class="modal-body">
                        <h5><strong>Lease
                                Period</strong>: <?= date('jS F, Y', strtotime($lease_detail[0]['lease_start_date'])); ?>
                            to <?= date('jS F, Y', strtotime($lease_detail[0]['lease_end_date'])); ?></h5>

                        <div class="form-group" <?= $vacatable ? " style='display:none' " : ""; ?> >
                            <br>
                            <h5 style="color: darkred">**You cannot vacant tenants.Lease is not complete
                                yet</h5>
                        </div>
                        <div class="form-group" <?= !$vacatable ? " style='display:none' " : ""; ?>>
                            <label for="vacate_date">Vacate date</label>
                            <input class="form-control" type="text" id="vacate_date"
                                   min_vacate_date="<?= date('Y,m,d', strtotime($lease_detail[0]['lease_end_date'])); ?>"
                                   value=""
                                   name="vacate_date" readonly>
                            <small id="vacate_date_help" class="form-text text-muted">
                                Enter a vacate date to mark this tenancy as completed
                            </small>
                        </div>

                    </div>
                    <div class="modal-footer" <?= !$vacatable ? " style='display:none' " : ""; ?> >
                        <button type="submit" id="vacate_tenant_form_submit_btn" class="btn btn-success">
                            Confirm
                        </button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<script src="//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>

<style>
    .red {
        border: 2px solid red;
    }
</style>

<script>
    $(function () {
        change_rent();

        function change_rent() {
            var rent_effective_date = $('#rent_effective_date');
            var min_rent_effective_date = rent_effective_date.attr('min-rent-effective-date');
            var max_rent_effective_date = rent_effective_date.attr('max-rent-effective-date');

            var min_date = null;
            var max_date = null;

            if (min_rent_effective_date != "no_date") {
                min_date = new Date(min_rent_effective_date);
            }
            if (max_rent_effective_date != "no_date") {
                max_date = new Date(max_rent_effective_date);
            }


            rent_effective_date.datepicker({
                changeMonth: true,
                changeYear: true,
                minDate: min_date,
                maxDate: max_date,
                dateFormat: 'd MM, yy',
            })
        }

        $("#input_day_for_rent_change").on("change", function () {
            var input_day = $(this);

            if (input_day.val() == "4") {
                $("#new_lease_pay_day_week_wrapper_for_rent_change").hide();
                $("#new_lease_pay_day_month_wrapper_for_rent_change").show();
            } else {
                $("#new_lease_pay_day_month_wrapper_for_rent_change").hide();
                $("#new_lease_pay_day_week_wrapper_for_rent_change").show();
            }
        });

        $("#change_rent").on("submit", function () {

            var ret = true;
            var err = 0;

            var new_rent = $("#new_rent_for_rent_change");
            var rent_effective_date = $("#rent_effective_date");


            if (!validate_new_rent_for_rent_change(new_rent.val())) {
                err++;
                new_rent.addClass("error");
            } else {
                new_rent.removeClass("error");
            }

            if (rent_effective_date.val() == "" || rent_effective_date.val() == null) {
                err++;
                rent_effective_date.addClass("error");
            } else {
                rent_effective_date.removeClass("error");
            }

            if (err > 0) {
                ret = false;
            }

            return ret;

        });

        function validate_new_rent_for_rent_change(rent) {
            var currencyFormat = /^\d+(\.\d{1,2})?$/;
            var ret = false;
            if (currencyFormat.test(rent)) {
                ret = true;
            }

            console.log(rent);
            console.log(ret);

            return ret;
        }
    });
</script>

<script>
    $(function () {
        extend_lease();

        var rent_increment_check = $("#rent_increment_check");
        rent_increment_check.on("change", function () {
            var rent_increment_check = $(this);

            if (rent_increment_check.val() == "yes") {
                $("#rent_increment_wrapper").show();
            } else {
                $("#rent_increment_wrapper").hide();
            }

        });

        $("#input_day").on("change", function () {
            var input_day = $(this);

            if (input_day.val() == "4") {
                $("#new_lease_pay_day_week_wrapper").hide();
                $("#new_lease_pay_day_month_wrapper").show();
            } else {
                $("#new_lease_pay_day_month_wrapper").hide();
                $("#new_lease_pay_day_week_wrapper").show();
            }

        });

        function extend_lease() {
            var new_lease_end_date = $('#new_lease_end_date');
            var lease_end_date_attr = new_lease_end_date.attr('lease-end-date');
            var lease_end_date = new Date(lease_end_date_attr);
            var min_date = new Date(lease_end_date);
            var min_gap = 1; //how many days after lease ends? set at least 1
            var year = 365;
            var year_after_lease_end_date = new Date(lease_end_date);

            min_date.setDate(lease_end_date.getDate() + min_gap);
            year_after_lease_end_date.setDate(lease_end_date.getDate() + year);

            new_lease_end_date.datepicker({
                changeMonth: true,
                changeYear: true,
                minDate: min_date,
                dateFormat: 'd MM, yy',
            })
        }

        $("#lease_extend").on("submit", function () {

            var ret = true;
            if (rent_increment_check.val() == "yes") {
                var new_rent = $("#new_rent");

                if (!validate_new_rent(new_rent.val())) {
                    ret = false;
                    new_rent.addClass("error");
                } else {
                    new_rent.removeClass("error");
                }
            }

            return ret;

        });

        function validate_new_rent(rent) {
            var currencyFormat = /^\d+(\.\d{1,2})?$/;
            var ret = false;
            if (currencyFormat.test(rent)) {
                ret = true;
            }

            console.log(rent);
            console.log(ret);

            return ret;
        }
    })
</script>

<script>
    $(function () {

        end_lease();

        function end_lease() {
            var vacate_date_to_end = $('#vacate_date_to_end');
            var new_lease_end_date_to_end = $('#new_lease_end_date_to_end');
            var lease_start_date_attr = new_lease_end_date_to_end.attr('lease-start-date');
            var lease_end_date_attr = new_lease_end_date_to_end.attr('lease-end-date');

            var lease_start_date = new Date(lease_start_date_attr);
            var lease_end_date = new Date(lease_end_date_attr);

            //--------------------------------------------------
            var today = new Date();
            //--------------------------------------------------

            var min_date = today;
            min_date.setDate(today.getDate() + 1);

            var max_date = new Date(lease_end_date);

            //window.prompt(min_date);
            //window.prompt(max_date);

            vacate_date_to_end.datepicker({
                changeMonth: true,
                changeYear: true,
                minDate: min_date,
                maxDate: max_date,
                dateFormat: 'd MM, yy',
            })

            new_lease_end_date_to_end.datepicker({
                changeMonth: true,
                changeYear: true,
                minDate: min_date,
                maxDate: max_date,
                dateFormat: 'd MM, yy',
            })
        }


        $('#leaseEndSubmitBtn').on('click', function (e) {
            e.preventDefault();

            swal({
                    title: "Are you sure?",
                    text: "Once you end this lease it cannot be reverted & the profile will be archived",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: "#DD6B55",
                    confirmButtonText: "I want to end this lease",
                    cancelButtonText: "No",
                    closeOnConfirm: true,
                    closeOnCancel: true
                },
                function (isConfirm) {
                    if (isConfirm) {
                        $("#lease_end").submit();
                    }
                });

        })

        $('#leaseExtendSubmitBtn').on('click', function (e) {
            e.preventDefault();

            swal({
                    title: "Are you sure?",
                    text: "Once you do this it cannot be reverted!",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: "#DD6B55",
                    confirmButtonText: "I want to do this",
                    cancelButtonText: "No",
                    closeOnConfirm: true,
                    closeOnCancel: true
                },
                function (isConfirm) {
                    if (isConfirm) {
                        $("#lease_extend").submit();
                    }
                });

        })

        $('#rentChangeSubmitBtn').on('click', function (e) {
            e.preventDefault();

            swal({
                    title: "Are you sure?",
                    text: "Are you sure you want to change the current rent?",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: "#DD6B55",
                    confirmButtonText: "Yes, I do",
                    cancelButtonText: "No",
                    closeOnConfirm: true,
                    closeOnCancel: true
                },
                function (isConfirm) {
                    if (isConfirm) {
                        $("#change_rent").submit();
                    }
                });

        })


    })
</script>


<script>
    $(function () {

        var settings_btn = $("#settings_btn");
        var leaseModal = $("#leaseModal");
        var vacateModal = $("#vacateModal");
        var lease = $("#lease");


        var vacate_form = $("#vacate_form");
        var vacate_tenant_form_submit_btn = $("#vacate_tenant_form_submit_btn");
        var vacate_date = $("#vacate_date");
        var min_vacate_date = new Date(vacate_date.attr('min_vacate_date'));
        var vacate_date_help = $("#vacate_date_help");
        var vacate_form_success = vacate_form.attr('vacate_form_success');
        var lease_vacated = vacate_form.attr('lease_vacated');

        var vacate_form_url = vacate_form.attr('action');
        var vacate_form_method = vacate_form.attr('method');

        vacate_date.datepicker({
            dateFormat: 'd M, yy',
            minDate: min_vacate_date
        });


        settings_btn.on("click", function () {
            toggleModal();
        });

        lease.on("change", function () {
            toggleModal();
        });

        function toggleModal() {
            if (lease.val() == 'new_lease' && vacate_form_success == "0" && lease_vacated == "0") {
                leaseModal.modal('hide');
                vacateModal.modal('show');
            } else {
                leaseModal.modal('show');
                vacateModal.modal('hide');
            }
        }

        vacate_tenant_form_submit_btn.on("click", function (e) {

            e.preventDefault();

            if (vacate_date.val() == '') {
                vacate_date.addClass('red');
            } else {
                vacate_date.removeClass('red');
                submitVacateForm();
            }

        });

        function submitVacateForm() {
            $.ajax({
                url: vacate_form_url,
                type: vacate_form_method,
                data: vacate_form.serialize(),
                success: function (response) {
                    console.log(response);
                    var parsed_response = JSON.parse(response);

                    if (parsed_response.success == 'true') {
                        vacate_form_success = "1";
                        toggleModal();
                    } else {
                        alert("Something Went Wrong!");
                    }

                },
                error: function (jqXHR, textStatus, errorThrown) {
                    alert('ERROR!');
                    console.log(textStatus, errorThrown);
                }
            });
        }


    });
</script>


<script>
    $(document).ready(function () {
        var uploaded_file_exist = <?php if($uploaded_file_exists) { ?> true <?php } else { ?> false <?php } ?> ;
        if (uploaded_file_exist) {
            show_file_anchor_not_input();
        } else {
            show_file_input_not_anchor();
        }
        $('#remove_file').on('click', function () {
            show_file_input_not_anchor();
        });
    });

    function show_file_input_not_anchor() {
        $('#signed_lease_agreement').show();
        $('#uploaded_file').hide();
        $('#remove_file').hide();
    }

    function show_file_anchor_not_input() {
        $('#signed_lease_agreement').hide();
        $('#uploaded_file').show();
        //$('#remove_file').show();
    }
</script>

<script type="text/javascript">
    $(document).ready(function () {
        var selectedtime = "<?=$lease_detail[0]['lease_pay_type'];?>";
        if (selectedtime == 4) {
            $('#inputhowOftenday').hide();
            $('#inputhowOftenday').removeAttr('name');
            $('#inputmonthDetail').show();
            $('#inputmonthDetail').attr('name', 'lease_pay_day');
        }
        else {
            $('#inputhowOftenday').show();
            $('#inputhowOftenday').attr('name', 'lease_pay_day');
            $('#inputmonthDetail').hide();
            $('#inputmonthDetail').removeAttr('name');
        }
    });
    $("#inputday").change(function () {

        var selectedtime = $("#inputday").val();
        if (selectedtime == 4) {
            $('#inputhowOftenday').hide();
            $('#inputhowOftenday').removeAttr('name');
            $('#inputmonthDetail').show();
            $('#inputmonthDetail').attr('name', 'lease_pay_day');
        }
        else {
            $('#inputhowOftenday').show();
            $('#inputhowOftenday').attr('name', 'lease_pay_day');
            $('#inputmonthDetail').hide();
            $('#inputmonthDetail').removeAttr('name');
        }
    });
</script>
<script type="text/javascript">
    var how_much_per_period = $('#howmuchperperiod').val();
    $(document).on("keyup", "#howmuchperperiod", function () {
        $('#total_amount').html($('#howmuchperperiod').val());
    });
    $(document).on("change", ".first_name_checkbox", function () {
        var how_much_per_period = $('#howmuchperperiod').val();
        var all_sum = 0;
        $(".first_name_checkbox:checked").each(function () {
            var curr_amount = $(this).closest('.tenant_payment_detail').find('.shared_amount_cls').val();
            all_sum = parseInt(all_sum) + parseInt(curr_amount);
        });
        /*if(all_sum !=$('#howmuchperperiod').val())
        {
            $('.ss_price_text').closest('div').addClass('ss_price_text');
            $('#total_amount').css("color", "red");
            $("#sub_btn").attr("disabled", true);
        }
        else
        {
            $('.ss_price_text').closest('div').removeClass('ss_price_text');
            $('#total_amount').css("color", "green");
            $("#sub_btn").attr("disabled", false);
        }*/

        /*if($(this).is(":checked"))
        {
            $(this).next('input').val("1");
        }
        else
        {
            $(this).next('input').val("0");
        }*/
    });
    $(document).on("keyup", ".shared_amount_cls", function () {
        var all_sum = 0;
        $(".first_name_checkbox:checked").each(function () {
            var curr_amount = $(this).closest('.tenant_payment_detail').find('.shared_amount_cls').val();
            all_sum = parseInt(all_sum) + parseInt(curr_amount);
            if (all_sum == NaN) {
                all_sum = 0;
            }

        });
        // $('#total_amount').text(all_sum);
        /*if(all_sum !=$('#howmuchperperiod').val())
        {
            $('.ss_price_text').closest('div').addClass('ss_price_text');
            $('#total_amount').css("color", "red");
            $("#sub_btn").attr("disabled", true);
        }
        else
        {
            $('.ss_price_text').closest('div').removeClass('ss_price_text');
            $('#total_amount').css("color", "green");
            $("#sub_btn").attr("disabled", false);
        }*/

        /*if($(this).is(":checked"))
        {
            $(this).next('input').val("1");
        }
        else
        {
            $(this).next('input').val("0");
        }*/
    });
    $(document).ready(function () {
        var all_sum = 0;
        /*$(".first_name_checkbox:checked").each(function () {
            var curr_amount = $(this).closest('.tenant_payment_detail').find('.shared_amount_cls').val();
            all_sum = parseInt(all_sum) + parseInt(curr_amount);
            if(all_sum==NaN)
            {
                all_sum = 0;
            }

        });*/
        // $('#total_amount').text(all_sum);
        /*if(all_sum !=$('#howmuchperperiod').val())
        {
            $('.ss_price_text').closest('div').addClass('ss_price_text');
            $('#total_amount').css("color", "red");
            $("#sub_btn").attr("disabled", true);
        }
        else
        {
            $('.ss_price_text').closest('div').removeClass('ss_price_text');
            $('#total_amount').css("color", "green");
            $("#sub_btn").attr("disabled", false);
        }*/
        $('#total_amount').css("color", "green");
        /*$('.first_name_checkbox').each(function(){
            if($(this).is(":checked"))
            {
                $(this).next('input').val("1");
            }
            else
            {
                $(this).next('input').val("0");
            }
        });*/
    });

    function chk_lease_detail() {
        var ok = true;
        var input_Lease_Name = $('#inputLeaseName').val();
        var how_much_per_period = $('#howmuchperperiod').val();
        var payment_total_amount = $('#total_amount').text();
        if (how_much_per_period == '' || how_much_per_period == null) {
            $('#howmuchperperiod').closest('div').addClass('has-error');
            ok = false;
        }
        else {
            $('#howmuchperperiod').closest('div').removeClass('has-error');
            ok = true;
        }
        if (input_Lease_Name == '' || input_Lease_Name == null) {
            $('#inputLeaseName').closest('div').addClass('has-error');
            ok = false;
        }
        else {
            $('#inputLeaseName').closest('div').removeClass('has-error');
            ok = true;
        }
        /*if(how_much_per_period != payment_total_amount)
        {
            alert('Per period amount and Total paid per period not matched');
            $('.ss_price_text').closest('div').addClass('ss_price_text');
            $('#total_amount').css("color", "red");
            ok = false;
        }
        else
        {
            $('.ss_price_text').closest('div').removeClass('ss_price_text');
            $('#total_amount').css("color", "green");
            ok = true;
        }*/
        var all_sum = 0;
        /*$(".first_name_checkbox:checked").each(function () {
            var curr_amount = $(this).closest('.tenant_payment_detail').find('.shared_amount_cls').val();
            all_sum = parseInt(all_sum) + parseInt(curr_amount);
        });*/
        /*if(all_sum !=$('#howmuchperperiod').val())
        {
            $('.ss_price_text').closest('div').addClass('ss_price_text');
            $('#total_amount').css("color", "red");
            $("#sub_btn").attr("disabled", true);
            ok = false;
        }
        else
        {
            $('.ss_price_text').closest('div').removeClass('ss_price_text');
            $('#total_amount').css("color", "green");
            $("#sub_btn").attr("disabled", false);
            ok = true;
        }*/

        /*$(".first_name_checkbox:checked").each(function () {
            var sharing_amount = $(this).closest('.tenant_payment_detail').find('.shared_amount_cls').val();
            if(sharing_amount==0 || sharing_amount=='')
            {
                $(this).closest('.tenant_payment_detail').find('.shared_amount_cls').closest('td').addClass('has-error');
                ok = false;
            }
            else
            {
                $(this).closest('.tenant_payment_detail').find('.shared_amount_cls').closest('td').removeClass('has-error');
                ok = true;
            }
        });*/
        return ok;
    }

</script>
<script type="text/javascript" src="https://code.jquery.com/ui/1.12.0/jquery-ui.min.js"></script>
<script type="text/javascript" src="assets/js/daterangepicker.jQuery.js"></script>
<script type="text/javascript">
    $(function () {
        $('#rangeBa, #rangeBb').daterangepicker();
    });
</script>
<script src="assets/js/bootstrap.min.js"></script>

<!-- <script src="assets/js/custom.js"></script> -->
<style>
    /*.ui-widget-header {
        color: #fff;
    }*/

    .ui-daterangepickercontain {
        top: 511px;
    }

    .ui-widget-header {
        background: #337ab7;
        border: #1f496d;
    }

    .ui-state-default, .ui-widget-content .ui-state-default, .ui-widget-header .ui-state-default {
        color: #337ab7;
    }

    .ui-state-active, .ui-widget-content .ui-state-active, .ui-widget-header .ui-state-active {
        border: 1px solid #1f496d;
    }
</style>
</body>
</html>