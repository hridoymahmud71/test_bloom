<?php $this->load->view('front/headlink'); ?>
<?php $this->load->view('front/top_menu'); ?>
<div class="container">
    <div class="logo text-left">
        <?php $this->load->view('front/head_nav'); ?>

        <div class="container">
            <div class="modal fade" id="leaseModal" role="dialog">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                            <h4 class="modal-title">Select a Lease or add a new lease</h4>
                        </div>
                        <form action="property/property_details/<?= $property_id; ?>" method="post">
                            <div class="modal-body">
                                <h4>Lease Option</h4>
                                <select class="form-control" name="lease_id" id="lease">
                                    <option value="new_lease">Add a new lease</option>
                                    <?php foreach ($all_lease as $row) { ?>
                                        <option <?php if ($row['lease_id'] == $lease_detail[0]['lease_id']) {
                                            echo "selected";
                                        } ?> value="<?= $row['lease_id']; ?>"
                                             disabled><?= $row['lease_name']; ?>
                                        </option>
                                    <?php } ?>
                                </select>
                            </div>
                            <div class="modal-footer">
                                <input type="submit" class="btn btn-success" value="Confirm" name="">
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <div class="modal fade" id="vacateModal" role="dialog">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                            <h4 class="modal-title">Vacate Tenants For
                                Lease: <?= $lease_detail[0]['lease_name'] ?></h4>

                        </div>
                        <?php
                        $vacatable = false;
                        $current_date = date('Y-m-d');

                        if (strtotime($lease_detail[0]['lease_end_date']) <= strtotime(date($current_date))) {
                            $vacatable = true;
                        }
                        ?>
                        <form id="vacate_form" vacate_form_success="0"
                              lease_vacated="<?= $lease_detail[0]['lease_vacated'] ?>"
                              action="property/vacate_tenants/<?= $lease_detail[0]['lease_id'] ?>"
                              method="post">
                            <div class="modal-body">
                                <h5><strong>Lease
                                        Period</strong>: <?= date('jS F, Y', strtotime($lease_detail[0]['lease_start_date'])); ?>
                                    to <?= date('jS F, Y', strtotime($lease_detail[0]['lease_end_date'])); ?></h5>

                                <div class="form-group" <?= $vacatable ? " style='display:none' " : ""; ?> >
                                    <br>
                                    <h5 style="color: darkred">**You cannot vacant tenants.Lease is not complete
                                        yet</h5>
                                </div>
                                <div class="form-group" <?= !$vacatable ? " style='display:none' " : ""; ?>>
                                    <label for="vacate_date">Vacate date</label>
                                    <input class="form-control" type="text" id="vacate_date"
                                           min_vacate_date="<?= date('Y,m,d', strtotime($lease_detail[0]['lease_end_date'])); ?>"
                                           value=""
                                           name="vacate_date" readonly>
                                    <small id="vacate_date_help" class="form-text text-muted">
                                        Enter a vacate date to mark this tenancy as completed
                                    </small>
                                </div>

                            </div>
                            <div class="modal-footer" <?= !$vacatable ? " style='display:none' " : ""; ?> >
                                <button type="submit" id="vacate_tenant_form_submit_btn" class="btn btn-success">
                                    Confirm
                                </button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <h4>Current Lease: <?= $lease_detail[0]['lease_name'] ?>
            <?php
            $lease_time_diff = (int)((strtotime($lease_detail[0]['lease_end_date']) - strtotime(date('Y-m-d'))) / 86400);
            $lease_time_diff_string = sprintf("(%d days to go)", $lease_time_diff);

            if ($lease_time_diff < 0) {
                $lease_time_diff_string = sprintf("(ended %d day(s) ago)", abs($lease_time_diff));
            }
            ?>
            <?= $lease_time_diff_string ?>
            <a href="#" data-toggle="modal" id="settings_btn"><span class="glyphicon glyphicon-cog"
                                                                    aria-hidden="true"></span></a>
        </h4>
        <div class="ss_container well ">
            <h3 class="extra_heading">Bond Information</h3>

            <div class="ss_bound_content">
                <!-- Nav tabs -->
                <ul class="nav nav-tabs" role="tablist">
                    <li role="presentation" class="active"><a href="#startlease" aria-controls="startlease" role="tab"
                                                              data-toggle="tab">Start of lease</a></li>
                    <li role="presentation"><a href="#endlease" aria-controls="profile" role="tab" data-toggle="tab">End
                            of lease</a></li>
                </ul>
                <!-- Tab panes -->
                <form class="form-horizontal" onsubmit="return check_bonds()" method="post" action="lease/save_bonds"
                      enctype="multipart/form-data">
                    <div class="tab-content">
                        <div role="tabpanel" class="tab-pane active" id="startlease">
                            <h3 class="form_heading">Bond Received</h3>
                            <div class="form-group row">
                                <label class="col-sm-4 control-label" for="bondamount">Bond amount</label>
                                <div class=" col-sm-8"><input <?= $is_archived?" readonly ":"" ?> type="text" name="bond_amount"
                                                              value="<?php if ($bond_info) {
                                                                  echo $bond_info[0]['bond_amount'];
                                                              } ?>" class="form-control wheelable" id="bondamount"
                                                              placeholder="$0.00"></div>
                            </div>
                            <div class="form-group row" style="display: none">
                                <label class="col-sm-4 control-label" for="bondamount">Date lodged</label>
                                <div class="col-sm-8">
                                    <div class="input-group">
                                        <input <?= $is_archived?" readonly ":"" ?> class="form-control" type="text" id="bond_start_date"
                                               name="lodge_start_date"
                                               value="<?php if ($bond_info && $bond_info[0]['lodge_start_date'] != null) {
                                                   echo date("M d, Y", strtotime($bond_info[0]['lodge_start_date']));
                                               } else {
                                                   echo date("M d, Y");
                                               } ?>" readonly/>
                                        <span class="input-group-addon"><i
                                                    class="glyphicon glyphicon-calendar"></i></span>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-4 control-label" for="bondamount">Lodged with</label>
                                <div class=" col-sm-8"><input <?= $is_archived?" readonly ":"" ?> type="text" name="lodged_with"
                                                              value="<?php if ($bond_info) {
                                                                  echo $bond_info[0]['lodged_with'];
                                                              } ?>" class="form-control" id="lodgedwith" placeholder="">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-4 control-label" for="bondamount">Additional notes</label>
                                <div class=" col-sm-8"><textarea <?= $is_archived?" readonly ":"" ?>  class="form-control col-md-12"
                                                                 name="bond_start_add_note"
                                                                 rows="3"><?php if ($bond_info) {
                                            echo $bond_info[0]['bond_start_add_note'];
                                        } ?></textarea></div>
                            </div>
                            <?php if(!$is_archived) { ?>
                            <div class="form-group row">
                                <label class="col-sm-4 control-label" for="bondamount">Upload bond documents</label>
                                <div class="col-sm-8 ">
                                    <div class="inputDnD">
                                        <div class="dropzone">
                                               <span style="display: none" class="my-dz-message1 pull-right">
                                                    <h3>Click Here to upload another file</h3>
                                                </span>
                                            <div class="dz-message">
                                                <h3> Click Here to upload your files</h3>
                                            </div>
                                        </div>
                                        <div class="previews" id="preview"></div>
                                    </div>
                                    <div>* Max 20 MB per file</div>
                                </div>
                            </div>
                            <?php } ?>
                            <br>
                            <?php if (!empty($bond_img_recieve_info)) { ?>
                                <div class="form-group row">
                                    <div class="col-sm-4"></div>
                                    <div class="col-sm-8">
                                        <ul class="list-group">
                                            <?php foreach ($bond_img_recieve_info as $row) { ?>
                                                <?php if (!empty($row['recieve_image'])) { ?>
                                                    <li class="list-group-item">
                                                        <a download
                                                           href="uploads/lease/<?= $row['recieve_image'] ?>"><?= $row['recieve_image'] ?></a>
                                                    </li>
                                                <?php } ?>
                                            <?php } ?>
                                        </ul>
                                    </div>

                                </div>

                            <?php } ?>
                        </div>
                        <div role="tabpanel" class="tab-pane" id="endlease">
                            <h3 class="form_heading">Bond allocation at termination of the tenancy</h3>
                            <div class="form-group row">
                                <label class="col-sm-4 control-label" for="bondamount">Amount to be refunded to
                                    tenant</label>
                                <div class=" col-sm-8"><input <?= $is_archived?" readonly ":"" ?> type="number" name="bond_tenant_return"
                                                              value="<?php if ($bond_info) {
                                                                  echo $bond_info[0]['bond_tenant_return'];
                                                              } ?>" class="form-control" id="bondamount"
                                                              placeholder="$0.00"></div>
                            </div>
                            <div class="form-group row">
                                <label for="bondamount" class="col-sm-4 control-label">Amount to be refunded to
                                    landlord</label>
                                <div class=" col-sm-8"><input <?= $is_archived?" readonly ":"" ?> type="number" name="bond_lanlord_keep"
                                                              value="<?php if ($bond_info) {
                                                                  echo $bond_info[0]['bond_lanlord_keep'];
                                                              } ?>" class="form-control" id="bondamount"
                                                              placeholder="$0.00"></div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-4 control-label" for="bondamount">Date lodged</label>
                                <div class=" col-sm-8">
                                    <div class="input-group date">
                                        <input <?= $is_archived?" readonly ":"" ?> class="form-control" id="bond_end_date" name="lodge_return_date"
                                               value="<?php if ($bond_info && $bond_info[0]['lodge_return_date'] != null) {
                                                   echo date("M d, Y", strtotime($bond_info[0]['lodge_return_date']));
                                               } else {
                                                   echo date("M d, Y");
                                               } ?>" type="text" readonly/>
                                        <span class="input-group-addon"><i
                                                    class="glyphicon glyphicon-calendar"></i></span>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-4 control-label" for="bondamount">Expenses landlord is
                                    claiming</label>
                                <div class=" col-sm-8"><textarea <?= $is_archived?" readonly ":"" ?> class="form-control col-md-12" rows="3"
                                                                 name="bond_return_add_note"><?php if ($bond_info) {
                                            echo $bond_info[0]['bond_return_add_note'];
                                        } ?></textarea></div>
                            </div>
                            <?php if(!$is_archived) {?>
                            <div class="form-group row">
                                <label class="col-sm-4 control-label" for="bondamount">Evidence of bond refund</label>
                                <div class="col-sm-8 ">
                                    <div class="inputDnD">
                                        <div class="dropzone dropzone_2">
                                               <span style="display: none" class="my-dz-message2 pull-right">
                                                    <h3>Click Here to upload another file</h3>
                                               </span>
                                            <div class="dz-message">
                                                <h3> Click Here to upload your files</h3>
                                            </div>
                                        </div>
                                        <div class="preview_2" id="preview_1"></div>
                                    </div>
                                    <div>* Max 20 MB per file</div>

                                </div>
                            </div>
                            <?php } ?>
                            <?php if (!empty($bond_img_return_info)) { ?>
                                <div class="form-group row">
                                    <div class="col-md-4"></div>
                                    <div class="col-md-8">
                                        <ul class="list-group"">
                                        <?php foreach ($bond_img_return_info as $row) { ?>
                                            <?php if (!empty($row['return_image'])) { ?>
                                                <li class="list-group-item">
                                                    <a download href="uploads/lease/<?= $row['return_image'] ?>"><?= $row['return_image'] ?></a>
                                                </li>
                                            <?php } ?>
                                        <?php } ?>
                                        </ul>
                                    </div>
                                </div>

                            <?php } ?>
                        </div>
                        <? if(!$is_archived) { ?>
                        <div class="modal-footer clear">
                            <br/><br/>
                            <input type="hidden" name="property_id" value="<?= $property_id; ?>">
                            <input type="hidden" name="lease_id" value="<?= $lease_detail[0]['lease_id']; ?>">
                            <input type="hidden" name="bond_id" value="<?php if ($bond_info) {
                                echo $bond_info[0]['bond_id'];
                            } ?>">
                            <button type="submit" class="btn btn-primary"> Save changes</button>
                            <br/><br/>
                        </div>
                        <?php } ?>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
</div>
</div>
</div>

<script src="//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
<script type="text/javascript" src="assets/js/dropzone.min.js"></script>

<style>
    .red {
        border: 2px solid red;
    }
</style>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-mousewheel/3.1.13/jquery.mousewheel.js"></script>
<script>
    $(function () {
        var BondAmountIcr = 50.00;

        $("#bondamount").bind("mousewheel", function (event, delta) {
            alert('dd');

            if (this.value == '' || isNaN(this.value)) {
                this.value = 0;
            }

            if (delta > 0) {
                this.value = parseInt(this.value) + BondAmountIcr;
            } else {
                if (parseInt(this.value) >= 0) {
                    this.value = parseInt(this.value) - BondAmountIcr;
                } else {
                    this.value = 0.00;
                    alert('Bond Amount Cannot be negative');
                }
            }
            return false;
        });

    });
</script>
<script>
    $(function () {

        //------------------------------
        var settings_btn = $("#settings_btn");
        var leaseModal = $("#leaseModal");
        var vacateModal = $("#vacateModal");
        var lease = $("#lease");
        //--------------------------------

        var vacate_form = $("#vacate_form");
        var vacate_tenant_form_submit_btn = $("#vacate_tenant_form_submit_btn");
        var vacate_date = $("#vacate_date");
        var min_vacate_date = new Date(vacate_date.attr('min_vacate_date'));
        var vacate_date_help = $("#vacate_date_help");
        var vacate_form_success = vacate_form.attr('vacate_form_success');
        var lease_vacated = vacate_form.attr('lease_vacated');

        var vacate_form_url = vacate_form.attr('action');
        var vacate_form_method = vacate_form.attr('method');

        vacate_date.datepicker({
            dateFormat: 'd M, yy',
            minDate: min_vacate_date
        });


        settings_btn.on("click", function () {
            toggleModal();
        });

        lease.on("change", function () {
            toggleModal();
        });

        function toggleModal() {
            if (lease.val() == 'new_lease' && vacate_form_success == "0" && lease_vacated == "0") {
                leaseModal.modal('hide');
                vacateModal.modal('show');
            } else {
                leaseModal.modal('show');
                vacateModal.modal('hide');
            }
        }

        vacate_tenant_form_submit_btn.on("click", function (e) {

            e.preventDefault();

            if (vacate_date.val() == '') {
                vacate_date.addClass('red');
            } else {
                vacate_date.removeClass('red');
                submitVacateForm();
            }

        });

        function submitVacateForm() {
            $.ajax({
                url: vacate_form_url,
                type: vacate_form_method,
                data: vacate_form.serialize(),
                success: function (response) {
                    console.log(response);
                    var parsed_response = JSON.parse(response);

                    if (parsed_response.success == 'true') {
                        vacate_form_success = "1";
                        toggleModal();
                    } else {
                        alert("Something Went Wrong!");
                    }

                },
                error: function (jqXHR, textStatus, errorThrown) {
                    alert('ERROR!');
                    console.log(textStatus, errorThrown);
                }
            });
        }


    });
</script>


<script type="text/javascript">
    Dropzone.autoDiscover = false;
    var foto_upload = new Dropzone(".dropzone", {
        url: "<?php echo base_url('lease/property_photo') ?>",
        maxFilesize: 20,
        method: "post",
        acceptedFiles: ".jpg,.jpeg,.png,.gif,.doc,.docx,.xls,.pdf",
        paramName: "userfile",
        dictInvalidFileType: "This File Type Not Supported",
        addRemoveLinks: true,
        init: function () {
            var count = 0;
            thisDropzone = this;
            this.on("success", function (file, json) {
                var obj = json;
                $('.previews').append(
                    "<input type='hidden' name='recieve_image[]' value='" + obj + "'>\n\
            <input type='hidden' name='recieve_width[]' value='" + file.width + "'>\n\
            <input type='hidden' name='recieve_height[]' value='" + file.height + "'>"
                );
                $(".my-dz-message1").show();
            });
        }
    });
    var foto_upload_new = new Dropzone(".dropzone_2", {
        url: "<?php echo base_url('lease/property_photo') ?>",
        maxFilesize: 20,
        method: "post",
        acceptedFiles: ".jpg,.jpeg,.png,.gif,.doc,.docx,.xls,.pdf",
        paramName: "userfile",
        dictInvalidFileType: "This File Type Not Supported",
        addRemoveLinks: true,
        init: function () {
            var count = 0;
            thisDropzone = this;
            this.on("success", function (file, json) {
                var obj = json;
                $('.preview_2').append(
                    "<input type='hidden' name='return_image[]' value='" + obj + "'>\n\
            <input type='hidden' name='return_width[]' value='" + file.width + "'>\n\
            <input type='hidden' name='return_height[]' value='" + file.height + "'>"
                );
                $(".my-dz-message2").show();
            });
        }
    });
</script>
<script type="text/javascript">
    function check_bonds() {
        var ok = true;
        var start_date = $('#bond_start_date').val();
        var end_date = $('#bond_end_date').val();
        if (new Date(start_date) > new Date(end_date)) {
            alert('Date lodged for Bonds Receivig should be less than Bonds Returning...');
            $('#bond_start_date').closest('div').addClass("has-error");
            $('#bond_end_date').closest('div').addClass("has-error");
            ok = false;
        }
        else {
            ok = true;
        }
        return ok;
    }
</script>


<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<script type="text/javascript" src="https://code.jquery.com/ui/1.12.0/jquery-ui.min.js"></script>
<script type="text/javascript" src="assets/js/daterangepicker.jQuery.js"></script>
<script type="text/javascript">
    $(function () {
        $('#bond_start_date, #bond_end_date').datepicker({
            dateFormat: 'M d, yy'
        });
    });
</script>
<script src="assets/js/bootstrap.min.js"></script>
<!-- <script src="assets/js/custom.js"></script> -->
<style>
    .ui-widget-header {
        color: #fff;
    }

    .ui-daterangepickercontain {
        top: 511px;
    }

    .ui-widget-header {
        background: #337ab7;
        border: #1f496d;
    }

    .ui-state-default, .ui-widget-content .ui-state-default, .ui-widget-header .ui-state-default {
        color: #337ab7;
    }

    .ui-state-active, .ui-widget-content .ui-state-active, .ui-widget-header .ui-state-active {
        border: 1px solid #1f496d;
    }
</style>
</body>
</html>