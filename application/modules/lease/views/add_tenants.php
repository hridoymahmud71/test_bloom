<?php $this->load->view('front/headlink'); ?>
<?php $this->load->view('front/top_menu'); ?>
<div class="container">
<?php $this->load->view('front/head_nav'); ?>
    <div>
        <h4>Current Lease: <?= $lease_detail[0]['lease_name'] ?>
            <?php
            $lease_time_diff = (int)((strtotime($lease_detail[0]['lease_end_date']) - strtotime(date('Y-m-d'))) / 86400);
            $lease_time_diff_string = sprintf("(%d days to go)", $lease_time_diff);

            if ($lease_time_diff < 0) {
                $lease_time_diff_string = sprintf("(ended %d day(s) ago)", abs($lease_time_diff));
            }
            ?>
            <?= $lease_time_diff_string ?>
            <a href="#" data-toggle="modal" id="settings_btn"><span class="glyphicon glyphicon-cog"
                                                                    aria-hidden="true"></span></a>
        </h4>

        <div class="container">
            <div class="modal fade" id="leaseModal" role="dialog">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                            <h4 class="modal-title">Select a Lease or add a new lease</h4>
                        </div>
                        <form action="property/property_details/<?= $property_id; ?>" method="post">
                            <div class="modal-body">
                                <h4>Lease Option</h4>
                                <select class="form-control" name="lease_id" id="lease">
                                    <option value="new_lease">Add a new lease</option>
                                    <?php foreach ($all_lease as $row) { ?>
                                        <option <?php if ($row['lease_id'] == $lease_detail[0]['lease_id']) {
                                            echo "selected";
                                        } ?> value="<?= $row['lease_id']; ?>"
                                             disabled><?= $row['lease_name']; ?></option>
                                    <?php } ?>
                                </select>
                            </div>
                            <div class="modal-footer">
                                <input type="submit" class="btn btn-success" value="Confirm" name="">
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <div class="modal fade" id="vacateModal" role="dialog">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                            <h4 class="modal-title">Vacate Tenants For
                                Lease: <?= $lease_detail[0]['lease_name'] ?></h4>

                        </div>
                        <?php
                        $vacatable = false;
                        $current_date = date('Y-m-d');

                        if (strtotime($lease_detail[0]['lease_end_date']) <= strtotime(date($current_date))) {
                            $vacatable = true;
                        }
                        ?>
                        <form id="vacate_form" vacate_form_success="0"
                              lease_vacated="<?= $lease_detail[0]['lease_vacated'] ?>"
                              action="property/vacate_tenants/<?= $lease_detail[0]['lease_id'] ?>"
                              method="post">
                            <div class="modal-body">
                                <h5><strong>Lease
                                        Period</strong>: <?= date('jS F, Y', strtotime($lease_detail[0]['lease_start_date'])); ?>
                                    to <?= date('jS F, Y', strtotime($lease_detail[0]['lease_end_date'])); ?></h5>

                                <div class="form-group" <?= $vacatable ? " style='display:none' " : ""; ?> >
                                    <br>
                                    <h5 style="color: darkred">**You cannot vacant tenants.Lease is not complete
                                        yet</h5>
                                </div>
                                <div class="form-group" <?= !$vacatable ? " style='display:none' " : ""; ?>>
                                    <label for="vacate_date">Vacate date</label>
                                    <input class="form-control" type="text" id="vacate_date"
                                           min_vacate_date="<?= date('Y,m,d', strtotime($lease_detail[0]['lease_end_date'])); ?>"
                                           value=""
                                           name="vacate_date" readonly>
                                    <small id="vacate_date_help" class="form-text text-muted">
                                        Enter a vacate date to mark this tenancy as completed
                                    </small>
                                </div>

                            </div>
                            <div class="modal-footer" <?= !$vacatable ? " style='display:none' " : ""; ?> >
                                <button type="submit" id="vacate_tenant_form_submit_btn" class="btn btn-success">
                                    Confirm
                                </button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <form onsubmit="return check_step_three_tenant()" action="lease/insert_tanent_management" method="post">
                <div class="ss_container">
                    <h3 class="extra_heading">Tenant Details <small>for the property at <span>
                                <?= $this->utility_model->getFullPropertyAddress($property_info[0]['property_id'])?>
                            </span></small></h3>
                    <div class="well well-small" style="display: none">
                        <button type="button" class="close" data-dismiss="alert" onclick="parentNode.remove()">�</button>
                        <p><a target="_blank" href="https://www.bloompropertygroup.com.au/" class="btn btn-black">Click Here</a> to learn about finding great tenants.</p>
                    </div>
                    <?php if ($this->session->flashdata('success')) { ?>
                        <div class="panel panel-primary">
                            <div class="panel-heading">Success!</div>
                            <div class="panel-body">
                                <?php if ($this->session->flashdata('save_success')) { ?>
                                    Successfully Saved
                                <?php } ?>
                            </div>
                        </div>
                    <?php } ?>
                        <div class="container">
                            <div class="row clearfix">
                                <div class="col-md-12 column">
                                    <table class="table table-bordered table-hover" id="tab_logic">
                                        <thead>
                                            <tr>
                                                <th class="text-left">Name *</th>
                                                <th class="text-left">Surname *</th>
                                                <th class="text-left">Telephone</th>
                                                <th class="text-left">Email</th>
                                                <th class="text-left">Recieve Correspondence</th>
                                                <!--<th></th>-->
                                            </tr>
                                        </thead>
                                        <tbody class="tenant_input_fields_wrap">
                                            <?php foreach($all_tenant as $key=>$row){?>
                                            <tr>
                                                <td id="user_fname<?=$key;?>">
                                                    <input <?= $is_archived?" readonly ":"" ?> type="text" name='user_fname[]' value="<?=$row['user_fname'];?>"  placeholder='First Name' class="form-control"/>
                                                </td>
                                                <td id="user_lname<?=$key;?>">
                                                    <input <?= $is_archived?" readonly ":"" ?> type="text" name='user_lname[]' value="<?=$row['user_lname'];?>" placeholder='Last Name' class="form-control"/>
                                                </td>
                                                <td id="phone<?=$key;?>">
                                                    <input <?= $is_archived?" readonly ":"" ?> type="tel" name='phone[]' value="<?=$row['phone'];?>" placeholder='Telephone' class="form-control"/>
                                                </td>
                                                <td id="email<?=$key;?>">
                                                    <input <?= $is_archived?" readonly ":"" ?> type="text" name='email[]' value="<?=$row['email'];?>" placeholder='Email' class="form-control"/>
                                                </td>
                                                <td id="correspondence<?=$key;?>">
                                                    <div class="radio">
                                                      <label><input <?= $is_archived?" disabled ":"" ?> type="radio" value="1"  name="correspondence[<?=$key;?>]" <?php if($row['correspondence']==1){echo "checked";}?>> Both&nbsp&nbsp</label>
                                                    </div>
                                                    <div class="radio">
                                                      <label><input <?= $is_archived?" disabled ":"" ?> type="radio" value="2" name="correspondence[<?=$key;?>]" <?php if($row['correspondence']==2){echo "checked";}?>>Phone</label>
                                                    </div>
                                                    <div class="radio">
                                                      <label><input <?= $is_archived?" disabled ":"" ?> type="radio" value="3" name="correspondence[<?=$key;?>]" <?php if($row['correspondence']==3){echo "checked";}?>>Email</label>
                                                    </div>
                                                </td>
                                                <td  style="display: none">
                                                    <a class="btn btn-danger pull-right tenant_remove_field">Delete Row</a>
                                                    <input type="hidden" name="tenant_id[]" value="<?=$row['user_id'];?>">
                                                </td>
                                            </tr>
                                            <?php }?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            <a class="btn btn-success pull-left tenant_add_field_button" style="display: none;">Add Another Tenant</a><!--<a id='delete_row' class="btn btn-danger pull-right">Delete Row</a>-->
                        </div>
                        <div class="clear"></div>
                        <br/><br/>
                        <input type="hidden" name="lease_id" value="<?=$lease_detail[0]['lease_id'];?>">
                        <input type="hidden" name="property_id" value="<?=$property_id;?>">
                        <?php if(!$is_archived) { ?>
                        <input type="submit"  value="Save Changes" class="print_ledgers btn btn-primary btn-lg">
                        <a href="Dashboard/<?=$property_id?>" class="btn btn-light btn-lg mark_paid">
                        Back to Dashboard</a>
                        <?php } ?>
                        <br/>
                        <br/>
                </div>
            </form>
        </div>
    </div>
</div>
<?php $this->load->view('front/footerlink');?>
<!-- <script src="//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script> -->
<style>
    .red {
        border: 2px solid red;
    }
</style>

<script>
    $(function () {

        //------------------------------
        var settings_btn = $("#settings_btn");
        var leaseModal = $("#leaseModal");
        var vacateModal = $("#vacateModal");
        var lease = $("#lease");
        //--------------------------------

        var vacate_form = $("#vacate_form");
        var vacate_tenant_form_submit_btn = $("#vacate_tenant_form_submit_btn");
        var vacate_date = $("#vacate_date");
        var min_vacate_date = new Date(vacate_date.attr('min_vacate_date'));
        var vacate_date_help = $("#vacate_date_help");
        var vacate_form_success = vacate_form.attr('vacate_form_success');
        var lease_vacated = vacate_form.attr('lease_vacated');

        var vacate_form_url = vacate_form.attr('action');
        var vacate_form_method = vacate_form.attr('method');

        vacate_date.datepicker({
            dateFormat: 'd M, yy',
            minDate: min_vacate_date
        });


        settings_btn.on("click", function () {
            toggleModal();
        });

        lease.on("change", function () {
            toggleModal();
        });

        function toggleModal() {
            if (lease.val() == 'new_lease' && vacate_form_success == "0" && lease_vacated == "0") {
                leaseModal.modal('hide');
                vacateModal.modal('show');
            } else {
                leaseModal.modal('show');
                vacateModal.modal('hide');
            }
        }

        vacate_tenant_form_submit_btn.on("click", function (e) {

            e.preventDefault();

            if (vacate_date.val() == '') {
                vacate_date.addClass('red');
            } else {
                vacate_date.removeClass('red');
                submitVacateForm();
            }

        });

        function submitVacateForm() {
            $.ajax({
                url: vacate_form_url,
                type: vacate_form_method,
                data: vacate_form.serialize(),
                success: function (response) {
                    console.log(response);
                    var parsed_response = JSON.parse(response);

                    if (parsed_response.success == 'true') {
                        vacate_form_success = "1";
                        toggleModal();
                    } else {
                        alert("Something Went Wrong!");
                    }

                },
                error: function (jqXHR, textStatus, errorThrown) {
                    alert('ERROR!');
                    console.log(textStatus, errorThrown);
                }
            });
        }


    });
</script>



<script type="text/javascript">
    $(document).ready(function() {
        var max_fields      = 100; //maximum input boxes allowed
        var wrapper         = $(".tenant_input_fields_wrap"); //Fields wrapper
        var add_button      = $(".tenant_add_field_button"); //Add button ID
        var x = <?=count($all_tenant)-1;?>; //initlal text box count

        $(add_button).click(function(e){ //on add input button click
            e.preventDefault();
        if(x < max_fields){ //max input box allowed
        x++; //text box increment
        $(wrapper).append('<tr><td id="user_fname'+x+'"><input type="text"  name="user_fname[]"  placeholder="First Name" class="form-control"/></td><td id="user_lname'+x+'"><input type="text" name="user_lname[]" placeholder="Last Name" class="form-control"/></td><td id="phone'+x+'"><input type="tel" name="phone[]" placeholder="Telephone" class="form-control"/></td><td id="email'+x+'"><input type="text" name="email[]" placeholder="Email" class="form-control"/></td><td id="correspondence'+x+'"><div class="radio"><label><input type="radio" value="1" checked name="correspondence['+x+']"> Both&nbsp&nbsp</label></div><div class="radio"><label><input type="radio" value="2" name="correspondence['+x+']">Phone</label></div><div class="radio"><label><input type="radio" value="3" name="correspondence['+x+']">Email</label></div></td><td><a class="btn btn-danger pull-right tenant_remove_field">Delete Row</a><input type="hidden" name="tenant_id[]" value="0"></td></tr>'); //add input box
        }
        });

        $(wrapper).on("click",".tenant_remove_field", function(e){ //user click on remove text
            if(x>0)
            {
                e.preventDefault(); $(this).closest('tr').remove(); x--;
            }
        })
    });
</script>
<script>
    function check_step_three_tenant() 
    {
        var flag=true;
        var fname_ok = 0;
        var lname_ok = 0;
        var phone_ok = 0;
        var email_ok = 0;
        var first_name = document.getElementsByName('user_fname[]');
        var last_name = document.getElementsByName('user_lname[]');
        var phone = document.getElementsByName('phone[]');
        var email = document.getElementsByName('email[]');

        for (i=0; i<first_name.length; i++)
        {
            if (first_name[i].value == 0)
            {
                $('#user_fname'+i).addClass("has-error");
                fname_ok = fname_ok +1;
            }
            else
            {
                $('#user_fname'+i).removeClass("has-error");
            }
        }
        for (i=0; i<last_name.length; i++)
        {
            if (last_name[i].value == '')
            {
                $('#user_lname'+i).addClass("has-error");
                lname_ok = lname_ok +1;
            }
            else
            {
                $('#user_lname'+i).removeClass("has-error");
            }
        }
        for (i=0; i<phone.length; i++)
        {
            if (phone[i].value != '')
            {
                if(!$.isNumeric(phone[i].value))
                {
                    $('#phone'+i).addClass("has-error");
                    phone_ok = phone_ok +1;
                }
                else
                {
                    $('#phone'+i).removeClass("has-error");
                }
            }
        }
        for (i=0; i<email.length; i++)
        {
            if (email[i].value != '')
            {
                if( /(.+)@(.+){2,}\.(.+){2,}/.test(email[i].value) )
                {
                    $('#email'+i).removeClass("has-error");
                }
                else
                {
                    $('#email'+i).addClass("has-error");
                    email_ok = email_ok +1;
                }
            }
        }
        if(fname_ok <=0 && lname_ok <= 0 && phone_ok <=0 && email_ok <= 0)
        {
            flag = true;
        }
        else
        {
            flag = false;
        }
        return flag;
    }
</script>
</body>
</html>
