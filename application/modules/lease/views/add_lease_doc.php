<?php $this->load->view('front/headlink'); ?>
<?php $this->load->view('front/top_menu'); ?>
  <div class="container">
    <?php $this->load->view('front/head_nav');?>
    <div class="row">  
 
      <div class="ss_container">

        <h3 class="extra_heading">Add New Files </h3>
        <div class="ss_bound_content">
          <form class="form-horizontal" onsubmit="return chk_document_upload()" action="lease/insert_lease_doc" method="post" enctype="multipart/form-data">
            <div class="row-fluid document-actions">
              <div class="col-md-8  text-left">
                <p><strong> Upload your file</strong></p>
                <p>
                    If you are uploading multiple files you can save them under the one title.
                </p>
              </div>
                <div class="col-md-4  text-left">
                    <ol><strong>Instructions:</strong>
                        <li>Upload the file/s</li>
                        <li>Add a title & description.</li>
                        <li>Click save!</li>
                    </ol>
                </div>
            </div>
            <div class="">
              <div class="form-group row">
                <label class="col-sm-4 control-label" for="">Upload File(s)</label>
                <div class="col-sm-8">
                    <div>* At least one file upload  needed</div>
                  <div class="inputDnD">
                     <div class="dropzone" >
                         <span style="display: none" class="my-dz-message pull-right">
                              <h3>Click Here to upload another file</h3>
                          </span>
                      <div class="dz-message" >
                        <h3> Click Here to upload your files</h3>
                      </div>

                    </div>
                    <div class="previews" id="preview"></div>
                  </div>
                    <div>* Max 20 MB per file</div>
                </div>
                <div>
                  <h5 class="image_display" style="display:none;color:red;">** At lease one image upload needed.</h5>
                </div>
              </div>
            </div>
            <div style="display:none" class="row-fluid document-actions">
              <div class="col-md-4  text-left">
                <p> tenants selected  will receive notifications via email and will have access to these files.</p>
              </div>
              <div class="form-group row text-left">
                <label class="col-sm-4 control-label"> To </label>
                <div class="col-sm-8">
                  <?php foreach($all_tenant as $key=>$row){?>
                  <div class="checkbox">
                    <label>
                      <!--<input name="user_id[]" value="<?/*=$row['user_id'];*/?>" type="checkbox"> <?/*=$row['user_fname'];*/?> --><?/*=$row['user_lname'];*/?>
                        <input name="user_id[]" value="<?=$row['user_id'];?>" type="checkbox" checked> <?=$row['user_fname'];?> <?=$row['user_lname'];?>
                    </label>
                  </div>
                  <?php }?>
                </div>
              </div>
            </div>
            <div class="row form-group">
              <label class="col-sm-4 control-label" >Title</label> 
                
               
              <div class="col-sm-8">
                <input type="text" name="doc_title" id="inpu_title" class="form-control">
              </div>
               
            </div>
            <div style="display:none" class="row form-group extra_padding">
              <label class="col-sm-4 control-label" >
                Category
              </label>
              <div class="col-md-8">
                <select id="select_title_option" class="form-control" name="doc_cat">
                  <option value="">Select an option</option>
                  <!--<option value="1">Receipts - Maintenance &amp; Repairs</option>
                  <option value="2">Receipts - Strata</option>
                  <option value="3">Lease &amp; Bond</option>
                  <option value="4">Inspection - Entry</option>
                  <option value="5">Inspection - Routine</option>
                  <option value="6">Inspection - Exit</option>
                  <option value="7">Maintenance</option>
                  <option value="8">Other</option>
                  <option value="9">Receipts - Accounting &amp; Bookkeeping</option>
                  <option value="10">Receipts - Bank fees</option>
                  <option value="11">Receipts - Council rates</option>
                  <option value="12">Receipts - GST</option>
                  <option value="13">Receipts - Real estate commission</option>
                  <option value="14">Receipts - Insurance</option>
                  <option value="15">Receipts - Land tax</option>
                  <option value="16">Receipts - Legal fees</option>
                  <option value="17">Receipts - Loan interest repayments</option>
                  <option value="18">Receipts - Management &amp; admin fees</option>
                  <option value="19">Receipts - Property advertising</option>
                  <option value="20">Receipts - Property cleaning</option>
                  <option value="21">Receipts - Real estate agent fees</option>
                  <option value="22">Receipts - Stationary and postage</option>
                  <option value="23">Receipts - Strata and body corporate</option>
                  <option value="24">Receipts - Utilities (electricity, gas, water)</option>
                  <option value="25">Receipts - Building inspection</option>
                  <option value="26">Receipts - Legal costs</option>
                  <option value="27">Receipts - Stamp duty</option>
                  <option value="28">Receipts - Pest control</option>
                  <option value="29">Receipts - Other purchase costs</option>
                  <option value="31">Receipts - Other sales expenses</option>-->
                </select>
              </div>
              <div class="col-md-3">
              </div>
            </div>
            <div class="row  form-group extra_padding">
              <label class="col-sm-4 control-label" >
                Description
                  <br>
                  <small>(you may want to note the date or time photos were taken or what the documents include and when they were received)</small>
              </label>
              <div class="col-sm-8">
                <textarea name="doc_desc" rows="6" class="form-control"></textarea>
              </div>
               
            </div>

            <label class="col-sm-4 control-label" ></label>
            <div class="col-sm-8">
              <h5 class="image_display text-right" style="display:none;color:red;">** At lease one image upload needed.</h5>
              <div class="modal-footer clear">
                <br><br>
                <!--<button type="button" class="btn btn-light" data-dismiss="modal">Close</button>-->
                <input type="hidden" name="property_id" value="<?=$property_id;?>">
                <input type="submit" class="btn btn-primary" value="Save Upload">
                  <!--<button type="submit" id="submit_btn" class="btn btn-primary">Save Upload</button>-->
                <br><br>
              </div>
            </div>
             <div style="clear: both;"></div>
          </form>
        </div>
      </div>
    </div>    
  </div>
  <script src="//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
  <?php $this->load->view('front/footerlink');?>
  <script type="text/javascript">
    Dropzone.autoDiscover = false;
    var foto_upload = new Dropzone(".dropzone", {
      url: "<?php echo base_url('lease/lease_document') ?>",
      maxFilesize: 20,
      method: "post",
      acceptedFiles: ".jpg,.jpeg,.png,.gif,.doc,.docx,.pdf,.xls,.xlsx",
      paramName: "userfile",
      dictInvalidFileType: "This File Type Not Supported",
      addRemoveLinks: true,
      init: function () {
        var count = 0;
        thisDropzone = this;
        this.on("success", function (file, json) {
          var obj = json;
          $('.previews').
          append(
            "<input type='hidden' name='lease_doc[]' value='" + obj + "'>\n\
            <input type='hidden' name='lease_width[]' value='" + file.width + "'>\n\
            <input type='hidden' name='lease_height[]' value='" + file.height + "'>"
            );
          $(".my-dz-message").show();
        });
      }
    });

    /*$("#submit_btn").on("click",function(){
        e.preventDefault();
    });*/

  </script>
  <script type="text/javascript">
    function chk_document_upload()
    {
      var ok = 0;
      var inpu_title = $('#inpu_title').val();
      var select_title_option = $('#select_title_option').val();
      if(inpu_title=='')
      {
        $('#inpu_title').closest('div').addClass("has-error");
        ok = ok +1;
      }
      else
      {
        $('#inpu_title').closest('div').removeClass("has-error");
      }
      /*if(select_title_option == '')
      {
        $('#select_title_option').closest('div').addClass("has-error");
        ok = ok +1;
      }
      else
      {
        $('#select_title_option').closest('div').removeClass("has-error");
      }*/
      var count= foto_upload.files;
      if(count.length=='' || count.length == 0)
      {
        $('.image_display').show();
        ok = ok +1;
      }
      else
      {
        $('.image_display').hide();
      }

      if(ok == 0)
      {
        ok = true;
      }
      else
      {
        ok = false;
      }
      return ok;
    }
  </script>
</body>
</html>