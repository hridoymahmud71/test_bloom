<?php $this->load->view('front/headlink'); ?>
<?php $this->load->view('front/top_menu'); ?>
<div class="container">
    <?php $this->load->view('front/head_nav'); ?>
    <div class="row">
     
        <div class="ss_container">
            <h3 class="extra_heading">My Documents and Files

            </h3>
            <style>
                .force_blue{
                    color: white;
                    background-color: #14b8dd;
                }
            </style>
            <div>
                <?php if(!$is_archived) { ?>
                <a href="leaseDocumentAdd/<?= $property_id; ?>" class="pull-right btn btn-primary"  style="    margin-left: 10px;"><span class="glyphicon glyphicon-plus"   aria-hidden="true"></span>Upload a
                    Document or File
                </a>
                <?php } ?>
            </div>
            <div class="ss_bound_content" style="background: transparent">
                <div id="all" class="row-fluid document-actions" style="display: none">
                    <div class="col-md-12">
                        <div class="top-btn">
                            <a style="display: none" href="javascript:void(0);" id="all_submit" class="btn btn-light" id="btn-download">Download
                                Files</a>
                            <a href="javascript:void(0);" id="all_delete" class="btn btn-light" id="btn-delete">Delete
                                Selected</a>
                        </div>
                    </div>
                </div>
                <div id="each" class="row-fluid document-actions" style="display: none;">
                    <div class="col-md-12">
                        <div class="top-btn">
                            <a href="javascript:void(0);" id="each_submit" class="btn btn-light" id="btn-download">Download
                                Files</a>
                            <a href="javascript:void(0);" id="each_delete" class="btn btn-light" id="btn-delete">Delete
                                Selected</a>
                        </div>
                    </div>
                </div>
                <div id="arch" class="row-fluid document-actions" style="display: none;">
                    <div class="col-md-12">
                        <div class="top-btn">
                            <a href="javascript:void(0);" id="arch_submit" class="btn btn-light" id="btn-download">Download
                                Files</a>
                            <a href="javascript:void(0);" id="arch_delete" class="btn btn-light" id="btn-delete">Unarchive
                                Selected</a>
                        </div>
                    </div>
                </div>
                <br/>
                <!-- Nav tabs -->
                <ul style="display:none" class="nav nav-tabs" role="tablist">
                    <li>View:</li>
                    <li role="presentation" class="active"><a href="#dall" class="all_list" aria-controls="All"
                                                              role="tab" data-toggle="tab">All</a></li>
                    <?php foreach ($lease_category as $row) { ?>
                        <li role="presentation"><a class="each_list" href="#<?= $row['lease_doc_cat_id']; ?>"
                                                   aria-controls="All" role="tab"
                                                   data-toggle="tab"><?= $row['lease_cat_name']; ?></a></li>
                    <?php } ?>
                    <li role="presentation"><a href="#darchive" class="arch_list" aria-controls="Closed" role="tab"
                                               data-toggle="tab">Archive</a></li>
                </ul>

                <?php if (!empty($property_leases)) { ?>
                    <br>
                <ul class="list-group">
                    <h4>Lease File(s):</h4>
                        <?php foreach ($property_leases as $property_lease) { ?>

                            <?php if(!empty($property_lease['lease_file'])) { ?>
                            &nbsp;<li class="list-group-item">
                                <a download
                                   href="<?= base_url() ?>uploads/image_and_file/file/<?= $property_lease['lease_file'] ?>">
                                    <?= $property_lease['lease_file'] ?>
                                </a>
                            </li>
                            <?php } ?>

                        <?php } ?>
                </ul>
                <?php } ?>
                <br>

                <?php if(!empty($bond_recieve_images)) { ?>
                    <br>
                <ul class="list-group">
                    <h4>Bond File(s)</h4>
                    <?php foreach($bond_recieve_images as $bond_recieve_image){?>
                        <li class="list-group-item"><a download href="uploads/lease/<?=$bond_recieve_image['recieve_image']?>"><?=$bond_recieve_image['recieve_image']?></a></li>
                    <?php }?>
                </ul>
                <?php } ?>

                <br>
                <h4 class="form_heading no_margin">Other Documents & Files</h4><br>
                <!-- Tab panes -->
                <div class="tab-content">
                    <div class="row-fluid document-list-header top-table-name force_blue">
                        <!--<div class="col-md-2">Label</div>-->
                        <div class="col-md-4">Document File</div>
                        <div class="col-md-6">Description </div>
                        <!--<div class="col-md-2">Shared</div>-->
                        <div class="col-md-2">Date Uploaded</div>
                    </div>
                    <div role="tabpanel" class="tab-pane active" id="dall">
                        <form id="all_submit_form" action="lease/doc_download" method="post">
                            <?php foreach ($all_lease_doc_info as $row) { ?>
                                <?php if ($row['archive_status'] == 0) { ?>
                                    <div class="row-fluid documents-list_area">
                                        <a href="leaseDocumentEdit/<?= $property_id ?>/<?= $row['doc_detail_id'] ?><?= $archived_string?>"
                                           class="rep_href">

                                            <div class="col-md-4">
                                                <h4 class="table-name">Document Title</h4>
                                                <h5 id="document_title"><?= $row['doc_title'] ?></h5>
                                            </div>
                                            <div class="col-md-6">
                                                <h4 class="table-name">Document Description</h4>
                                                <h5 id="document_title"><?= $row['doc_desc'] ?></h5>
                                            </div>
                                           <!-- <div class="col-md-2">
                                                <h4 class="table-name">Sharing &amp; Docs</h4>
                                                <div class="sharing-count">
                                                    <?php /*$user_count = 0;
                                                    foreach ($all_lease_doc_tenant as $user_row) { */?>
                                                        <?php /*if ($row['doc_detail_id'] == $user_row['doc_detail_id']) {
                                                            $user_count++;
                                                        }
                                                    } */?>
                                                    <span><?/*= $user_count; */?></span>
                                                    <span class="glyphicon glyphicon-user" aria-hidden="true"></span>
                                                </div>
                                                <div class="doc-count">
                                                    <?php /*$user_count = 0;
                                                    foreach ($all_lease_doc as $doc_row) { */?>
                                                        <?php /*if ($row['doc_detail_id'] == $doc_row['doc_detail_id']) {
                                                            $user_count++;
                                                        }
                                                    } */?>
                                                    <span><?/*= $user_count; */?></span>
                                                    <span class="glyphicon glyphicon-paperclip"
                                                          aria-hidden="true"></span>
                                                </div>
                                            </div>-->
                                            <div class="col-md-2 uploaded-date"><?= date("jS \of F, Y", strtotime($row['created_at'])); ?></div>
                                        </a>
                                    </div>
                                <?php } ?>
                            <?php } ?>
                            <div id="get_val_here"></div>
                        </form>
                    </div>
                    <div>
                        <form id="each_submit_form" action="lease/each_doc_download" method="post">
                            <div id="get_each_here"></div>
                    </div>
                    <style>
                        .documents-list_area h5,.uploaded-date{
                            color: rgb(51, 122, 183) !important;
                            
                        }
                        .documents-list_area h5:hover,.uploaded-date:hover{
                            text-decoration: underline ;
                            color: black !important;

                        }
                    </style>

                        <div role="tabpanel" class="tab-pane" id="">
                            <?php foreach ($all_lease_doc_info as $row) { ?>
                                <?php if ($row['lease_cat_name'] == $lease_cat['lease_cat_name'] && $row['archive_status'] == 0) { ?>
                                    <div class="row-fluid documents-list_area" >
                                        <a href="leaseDocumentEdit/<?= $property_id ?>/<?= $row['doc_detail_id'] ?>"
                                           class="rep_href">
                                            <div class="col-md-4">
                                                <h4 class="table-name">Label</h4>
                                                <div class="col-md-2">
                                                    <input type="checkbox" class="form-control document_id"
                                                           name="eachdocument[]" value="<?= $row['doc_detail_id'] ?>">
                                                </div>
                                                <label for="" id="document_label"
                                                       class="ss_lbond"><?= $row['lease_cat_name'] ?></label>
                                            </div>
                                            <div class="col-md-4">
                                                <h4 class="table-name">Document Title</h4>
                                                <h5 id="document_title"><?= $row['doc_title'] ?></h5>
                                            </div>
                                            <div class="col-md-2">
                                                <h4 class="table-name">Sharing &amp; Docs</h4>
                                                <div class="sharing-count">
                                                    <?php $user_count = 0;
                                                    foreach ($all_lease_doc_tenant as $user_row) { ?>
                                                        <?php if ($row['doc_detail_id'] == $user_row['doc_detail_id']) {
                                                            $user_count++;
                                                        }
                                                    } ?>
                                                    <span><?= $user_count; ?></span>
                                                    <span class="glyphicon glyphicon-user" aria-hidden="true"></span>
                                                </div>
                                                <div class="doc-count">
                                                    <?php $user_count = 0;
                                                    foreach ($all_lease_doc as $doc_row) { ?>
                                                        <?php if ($row['doc_detail_id'] == $doc_row['doc_detail_id']) {
                                                            $user_count++;
                                                        }
                                                    } ?>
                                                    <span><?= $user_count; ?></span>
                                                    <span class="glyphicon glyphicon-paperclip"
                                                          aria-hidden="true"></span>
                                                </div>
                                            </div>
                                            <div class="col-md-2 uploaded-date"><?= date("jS \of F, Y", strtotime($row['created_at'])); ?></div>
                                        </a>
                                    </div>
                                <?php } ?>
                            <?php } ?>
                        </div>

                    <div>
                        </form>
                    </div>
                    <div role="tabpanel" class="tab-pane" id="darchive">
                        <form id="arch_submit_form" action="lease/arch_doc_download" method="post">
                            <?php foreach ($all_lease_doc_info as $row) { ?>
                                <?php if ($row['archive_status'] == 1) { ?>
                                    <div class="row-fluid documents-list_area">
                                        <a href="leaseDocumentEdit/<?= $property_id ?>/<?= $row['doc_detail_id'] ?>"
                                           class="rep_href">
                                            <div class="col-md-4">
                                                <h4 class="table-name">Label</h4>
                                                <div class="col-md-2">
                                                    <input type="checkbox" class="form-control document_id"
                                                           name="archdocument[]" value="<?= $row['doc_detail_id'] ?>">
                                                </div>
                                                <label for="" id="document_label"
                                                       class="ss_aarchive">Zip</label>
                                            </div>
                                            <div class="col-md-4">
                                                <h4 class="table-name">Document Title</h4>
                                                <h5 id="document_title"><?= $row['doc_title'] ?></h5>
                                            </div>
                                            <div class="col-md-2">
                                                <h4 class="table-name">Sharing &amp; Docs</h4>
                                                <div class="sharing-count">
                                                    <?php $user_count = 0;
                                                    foreach ($all_lease_doc_tenant as $user_row) { ?>
                                                        <?php if ($row['doc_detail_id'] == $user_row['doc_detail_id']) {
                                                            $user_count++;
                                                        }
                                                    } ?>
                                                    <span><?= $user_count; ?></span>
                                                    <span class="glyphicon glyphicon-user" aria-hidden="true"></span>
                                                </div>
                                                <div class="doc-count">
                                                    <?php $user_count = 0;
                                                    foreach ($all_lease_doc as $doc_row) { ?>
                                                        <?php if ($row['doc_detail_id'] == $doc_row['doc_detail_id']) {
                                                            $user_count++;
                                                        }
                                                    } ?>
                                                    <span><?= $user_count; ?></span>
                                                    <span class="glyphicon glyphicon-paperclip"
                                                          aria-hidden="true"></span>
                                                </div>
                                            </div>
                                            <div class="col-md-2 uploaded-date"><?= date("jS F, Y", strtotime($row['created_at'])); ?></div>
                                        </a>
                                    </div>
                                <?php } ?>
                            <?php } ?>
                            <div id="get_arch_here"></div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script src="//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>


<script type="text/javascript">
    $('.each_list').click(function () {
        $('#all').hide();
        $('#each').show();
        $('#arch').hide();
    });

    $('.all_list').click(function () {
        $('#all').show();
        $('#each').hide();
        $('#arch').hide();
    });

    $('.arch_list').click(function () {
        $('#all').hide();
        $('#each').hide();
        $('#arch').show();
    });
</script>
<script type="text/javascript">
    $('#all_submit').click(function () {
        $('#get_val_here').append('<input type="hidden" name="delete_status" value="0"><input type="hidden" name="property_id" value="<?=$property_id?>">');
        $('#all_submit_form').trigger('submit');
    });
    $('#all_delete').click(function () {
        $('#get_val_here').append('<input type="hidden" name="delete_status" value="1"><input type="hidden" name="property_id" value="<?=$property_id?>">');
        $('#all_submit_form').trigger('submit');
    });
    $('#each_submit').click(function () {
        $('#get_each_here').append('<input type="hidden" name="delete_each_status" value="0"><input type="hidden" name="property_id" value="<?=$property_id?>">');
        $('#each_submit_form').trigger('submit');
    });
    $('#each_delete').click(function () {
        $('#get_each_here').append('<input type="hidden" name="delete_each_status" value="1"><input type="hidden" name="property_id" value="<?=$property_id?>">');
        $('#each_submit_form').trigger('submit');
    });
    $('#arch_submit').click(function () {
        $('#get_arch_here').append('<input type="hidden" name="delete_arch_status" value="0"><input type="hidden" name="property_id" value="<?=$property_id?>">');
        $('#arch_submit_form').trigger('submit');
    });
    $('#arch_delete').click(function () {
        $('#get_arch_here').append('<input type="hidden" name="delete_arch_status" value="1"><input type="hidden" name="property_id" value="<?=$property_id?>">');
        $('#arch_submit_form').trigger('submit');
    });
</script>
<?php $this->load->view('front/footerlink'); ?>
</body>
</html>