<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Lease extends MX_Controller
{

    //public $counter=0;
    function __construct()
    {
        parent::__construct();
        $this->load->model('lease_model');
        $this->load->model('property/property_model');
        $this->load->model('utility/utility_model');
        // $this->load->helper('inflector');
        // $this->load->library('encrypt');

        $this->utility_model->check_auth();

    }

    public function index()
    {
        $this->load->view('lease_details');
    }

    public function lease_details($property_id)
    {
        if ($this->input->post('lease_id')) {
            if (($this->input->post('lease_id')) == 'new_lease') {
                redirect('property/step_three/' . $property_id);
            } else {
                $this->lease_model->update_with_set_value('lease_current_status', '0', 'property_id', $property_id, 'lease');
                $lease_id = $this->input->post('lease_id');
                $data['lease_current_status'] = 1;
                $this->lease_model->update_active_lease($property_id, $lease_id, $data);
            }
        }


        $data['property_id'] = $property_id;
        $data['property_info'] = $this->lease_model->select_with_where('*', 'property_id=' . $property_id . '', 'property');
        $data['lease_detail'] = $this->lease_model->select_with_where('*', 'property_id=' . $property_id . ' AND lease_current_status=1', 'lease');
        $data['all_lease'] = $this->lease_model->select_with_where('*', 'property_id=' . $property_id . '', 'lease');

        $lease_id = !empty($data['lease_detail']) ? $data['lease_detail'][0]['lease_id'] : 0;
        //----------------------------------------------------------
        $is_archived = $this->utility_model->isArchived();
        $archived_string = $this->utility_model->getArchivedString();
        if ($is_archived) {
            $lease_id = $this->utility_model->getArchivedLeaseId();
            $data['lease_detail'][0] = $this->utility_model->getLease($lease_id);
        }

        $data['is_archived'] = $is_archived;
        $data['archived_string'] = $archived_string;
        //----------------------------------------------------------


        $data['all_tenant'] = $this->lease_model->get_specific_tenant($lease_id, $property_id);

        $data['bond_info'] = $this->lease_model->select_with_where('*', 'property_id=' . $property_id . ' AND lease_id=' . $lease_id . '', 'bond');

        if ($data['bond_info']) {
            $data['bond_img_recieve_info'] = $this->lease_model->select_with_where('*', 'bond_id=' . $data['bond_info'][0]['bond_id'] . ' AND status=1', 'bond_recieve_image');
            $data['bond_img_return_info'] = $this->lease_model->select_with_where('*', 'bond_id=' . $data['bond_info'][0]['bond_id'] . ' AND status=1', 'bond_return_image');

        } else {
            $data['bond_img_recieve_info'] = $this->lease_model->select_with_where('*', 'status=1', 'bond_recieve_image');
            $data['bond_img_return_info'] = $this->lease_model->select_with_where('*', 'status=1', 'bond_return_image');
        }

        $data['lease_documents'] = $this->lease_model->getLeaseDocuments($property_id);

        $data['current_lease_rent_period'] = null;
        $data['next_of_current_lease_rent_period_single'] = null;
        $data['next_of_current_lease_rent_period_list'] = null;
        $data['next_of_current_lease_rent_period_list_last_item'] = null;

        if (!empty($data['lease_detail'])) {
            $data['current_lease_rent_period'] = $this->utility_model->getCurrentLeaseRentPeriod($lease_id);
            $data['next_of_current_lease_rent_period_single'] = $this->utility_model->getNextOfCurrentLeaseRentPeriod($lease_id, 'single');
            $data['next_of_current_lease_rent_period_list'] = $this->utility_model->getNextOfCurrentLeaseRentPeriod($lease_id, 'list');
        }

        if (!empty($data['next_of_current_lease_rent_period_list'])) {
            $data['next_of_current_lease_rent_period_list_last_item'] = end($data['next_of_current_lease_rent_period_list']);
        }

        /*echo "<pre>";
        print_r($data);
        echo "</pre>";
        die();*/

        $this->load->view('lease_details', $data);
    }

    public function update_lease()
    {
        $lease_id = $this->input->post('lease_id');
        $data['property_id'] = $this->input->post('property_id');
        $data['lease_name'] = $this->input->post('lease_name');
        $lease_start_date = $this->input->post('lease_start_date');
        $lease_end_date = $this->input->post('lease_end_date');
        $data['lease_start_date'] = date("Y-m-d", strtotime($lease_start_date));
        $data['lease_end_date'] = date("Y-m-d", strtotime($lease_end_date));
        // $data['lease_file'] = $this->input->post('lease_file');
        $data['lease_pay_type'] = $this->input->post('lease_pay_type');
        $data['lease_per_period_payment'] = $this->input->post('lease_per_period_payment');
        $data['lease_pay_day'] = $this->input->post('lease_pay_day');

        $this->form_validation->set_rules('lease_name', 'Lease Name', 'required');
        $this->form_validation->set_rules('lease_start_date', 'Lease Start Date', 'required');
        $this->form_validation->set_rules('lease_end_date', 'Lease End Date', 'required');
        $this->form_validation->set_rules('lease_pay_type', 'Lease Pay Type', 'required');
        $this->form_validation->set_rules('lease_per_period_payment', 'Lease Per Period', 'required');
        $this->form_validation->set_rules('lease_pay_day', 'Lease Pay Day', 'required');
        if ($this->form_validation->run() == false) {
            $this->session->set_userdata('insert_stp_four_err', validation_errors());
            redirect('lease/lease_details/' . $data['property_id']);
        } else {
            $this->lease_model->update_function('lease_id', $lease_id, 'lease', $data);
        }

        $payment_status = $this->input->post('payment_status');
        $share_paid_amount = $this->input->post('share_paid_amount');
        $payment_method = $this->input->post('payment_method');
        // echo "<pre>";print_r($payment_method);die();
        $tenant_id = $this->input->post('tenant_id');
//        for($i=0;$i<count($share_paid_amount);$i++)
//        {
//            $datas['payment_status'] = in_array($i, $_POST['payment_status']) ? '1' : '0';
//            // $datas['payment_status'] = $payment_status[$i];
//            $datas['share_paid_amount'] = $share_paid_amount[$i];
//            $datas['payment_method'] = $payment_method[$i];
//            $this->lease_model->update_lease_details_function($lease_id,$tenant_id[$i],$data['property_id'],$datas);
//        }
        $active_update['lease_update_done'] = 0;
        $this->lease_model->update_function('property_id', $data['property_id'], 'property', $active_update);

        if ($_FILES) {
            $files = $_FILES;
            if ($_FILES['lease_file']['name'] != '') {
                $_FILES['userfile']['name'] = uniqid() . '_' . underscore($files['lease_file']['name']);
                $_FILES['userfile']['type'] = $files['lease_file']['type'];
                $_FILES['userfile']['tmp_name'] = $files['lease_file']['tmp_name'];
                $_FILES['userfile']['error'] = $files['lease_file']['error'];
                $_FILES['userfile']['size'] = $files['lease_file']['size'];

                $oldFileName = $_FILES['userfile']['name'];
                $_FILES['userfile']['name'] = str_replace("'", "", $oldFileName);
                $this->upload->initialize($this->set_upload_options($_FILES['userfile']['name'], 'image_and_file/file'));
                $this->upload->do_upload();

                $data_image['lease_file'] = $_FILES['userfile']['name'];
                $this->lease_model->update_function('lease_id', $get_lease_details_id, 'lease', $data_image);
                redirect('rentSchedule/' . $data['property_id'] . '/1');
            }
        }
        redirect('rentSchedule/' . $data['property_id'] . '/1');
    }

    private function set_upload_options($file_name, $folder_name)
    {
        //upload an image options
        $url = base_url();

        $config = array();
        $config['file_name'] = $file_name;
        $config['upload_path'] = 'uploads/' . $folder_name;
        $config['allowed_types'] = 'jpg|png|gif|jpeg|xls|doc|docx|pdf|csv|mp3|mp4';
        $config['max_size'] = '10000';
        $config['overwrite'] = TRUE;

        return $config;
    }

    public function all_bonds($property_id)
    {
        if ($this->input->post('lease_id')) {
            if (($this->input->post('lease_id')) == 'new_lease') {
                redirect('property/step_three/' . $property_id);
            } else {
                $this->lease_model->update_with_set_value('lease_current_status', '0', 'property_id', $property_id, 'lease');
                $lease_id = $this->input->post('lease_id');
                $data['lease_current_status'] = 1;
                $this->lease_model->update_active_lease($property_id, $lease_id, $data);
            }
        }
        $data['property_id'] = $property_id;
        $data['lease_detail'] = $this->lease_model->select_with_where('*', 'property_id=' . $property_id . ' AND lease_current_status=1', 'lease');
        $lease_id = !empty($data['lease_detail']) ? $data['lease_detail'][0]['lease_id'] : 0;
        //----------------------------------------------------------
        $is_archived = $this->utility_model->isArchived();
        $archived_string = $this->utility_model->getArchivedString();
        if ($is_archived) {
            $lease_id = $this->utility_model->getArchivedLeaseId();
            $data['lease_detail'][0] = $this->utility_model->getLease($lease_id);
        }

        $data['is_archived'] = $is_archived;
        $data['archived_string'] = $archived_string;
        //----------------------------------------------------------

        $data['all_lease'] = $this->lease_model->select_with_where('*', 'property_id=' . $property_id . '', 'lease');
        $data['bond_info'] = $this->lease_model->select_with_where('*', 'property_id=' . $property_id . ' AND lease_id=' . $lease_id . '', 'bond');

        $data['bond_img_recieve_info'] = array();
        $data['bond_img_return_info'] = array();

        if ($data['bond_info']) {
            $data['bond_img_recieve_info'] = $this->lease_model->select_with_where('*', 'bond_id=' . $data['bond_info'][0]['bond_id'] . ' AND status=1', 'bond_recieve_image');
            $data['bond_img_return_info'] = $this->lease_model->select_with_where('*', 'bond_id=' . $data['bond_info'][0]['bond_id'] . ' AND status=1', 'bond_return_image');
        } else {
            $data['bond_img_recieve_info'][] = $this->lease_model->select_with_where('*', 'status=1', 'bond_recieve_image');
            $data['bond_img_return_info'][] = $this->lease_model->select_with_where('*', 'status=1', 'bond_return_image');
        }

        $data['all_tenant'] = $this->lease_model->get_specific_tenant($lease_id, $property_id);
        $this->load->view('add_bonds', $data);
    }

    public function property_photo()
    {
        $files = $_FILES;
        if (isset($_FILES['userfile']) && !empty($_FILES['userfile']['name'])) {
            $_FILES['userfile']['name'] = uniqid() . '_' . $_FILES['userfile']['name'];
            $config['upload_path'] = 'uploads/lease';
            $config['allowed_types'] = 'jpg|png|gif|jpeg|pdf|doc|docx|xls';
            $config['max_size'] = '0';
            $this->load->library('upload', $config);
            $this->upload->initialize($config);
            if (!$this->upload->do_upload('userfile')) {
                $status = 'error';
                $msg = $this->upload->display_errors('', '');

            } else {
                $fileinfo = $this->upload->data();
                echo $fileinfo['file_name'];
            }
        }
    }

    public function save_bonds()
    {
        $property_id = $this->input->post('property_id');
        $lease_id = $this->input->post('lease_id');
        $bond_id = $this->input->post('bond_id');

        $chk_bond_exist = $this->lease_model->count_with_where('*', 'property_id=' . $property_id . ' AND lease_id=' . $lease_id . '', 'bond');

        //$data['user_id'] = 1; //this property_id will be dynamic value //
        $data['user_id'] = $this->session->userdata('user_id');

        $data['property_id'] = $property_id;
        $data['lease_id'] = $lease_id;
        $data['bond_amount'] = $this->input->post('bond_amount');

        $lodge_start_date = $this->input->post('lodge_start_date');
        $data['lodge_start_date'] = date("Y-m-d", strtotime($lodge_start_date));

        $data['lodged_with'] = $this->input->post('lodged_with');
        $data['bond_start_add_note'] = $this->input->post('bond_start_add_note');
        //$data['bond_start_file'] = $this->input->post('bond_start_file');
        $data['bond_tenant_return'] = $this->input->post('bond_tenant_return');
        $data['bond_lanlord_keep'] = $this->input->post('bond_lanlord_keep');
        $lodge_return_date = $this->input->post('lodge_return_date');
        $data['lodge_return_date'] = date("Y-m-d", strtotime($lodge_return_date));
        $data['bond_return_add_note'] = $this->input->post('bond_return_add_note');

        $recieve_image = $this->input->post('recieve_image');
        $return_image = $this->input->post('return_image');

        if ($chk_bond_exist == 0) {
            $insert_id = $this->lease_model->insert('bond', $data);
        } else {
            $this->lease_model->update_bonds($property_id, $lease_id, $data);
        }

        if ($recieve_image) {
            foreach ($recieve_image as $key => $value) {
                $image_recieve_data['recieve_image'] = $value;
                if ($chk_bond_exist == 0) {
                    $image_recieve_data['bond_id'] = $insert_id;
                    $this->lease_model->insert('bond_recieve_image', $image_recieve_data);
                } else {
                    $image_recieve_data['bond_id'] = $bond_id;
                    $this->lease_model->insert('bond_recieve_image', $image_recieve_data);
                }
            }
        }
        if ($return_image) {
            foreach ($return_image as $key => $value) {
                $image_return_data['return_image'] = $value;
                if ($chk_bond_exist == 0) {
                    $image_return_data['bond_id'] = $insert_id;
                    $this->lease_model->insert('bond_return_image', $image_return_data);
                } else {
                    $image_return_data['bond_id'] = $bond_id;
                    $this->lease_model->insert('bond_return_image', $image_return_data);
                }
                // $this->lease_model->insert('property_photos',$image_return_data);
            }
        }

        redirect('leaseBonds/' . $property_id);
    }


    public function add_tenants($property_id)
    {
        if ($this->input->post('lease_id')) {
            if (($this->input->post('lease_id')) == 'new_lease') {
                redirect('property/step_three/' . $property_id);
            } else {
                $this->lease_model->update_with_set_value('lease_current_status', '0', 'property_id', $property_id, 'lease');
                $lease_id = $this->input->post('lease_id');
                $data['lease_current_status'] = 1;
                $this->lease_model->update_active_lease($property_id, $lease_id, $data);
            }
        }
        $data['property_id'] = $property_id;
        $data['property_info'] = $this->lease_model->select_with_where('*', 'property_id=' . $property_id . '', 'property');
        $data['all_lease'] = $this->lease_model->select_with_where('*', 'property_id=' . $property_id . '', 'lease');
        $data['lease_detail'] = $this->lease_model->select_with_where('*', 'property_id=' . $property_id . ' AND lease_current_status=1', 'lease');

        $lease_id = !empty($data['lease_detail']) ? $data['lease_detail'][0]['lease_id'] : 0;
        //----------------------------------------------------------
        $is_archived = $this->utility_model->isArchived();
        $archived_string = $this->utility_model->getArchivedString();
        if ($is_archived) {
            $lease_id = $this->utility_model->getArchivedLeaseId();
            $data['lease_detail'][0] = $this->utility_model->getLease($lease_id);
        }

        $data['is_archived'] = $is_archived;
        $data['archived_string'] = $archived_string;
        //----------------------------------------------------------

        $data['all_tenant'] = $this->lease_model->get_specific_tenant($data['lease_detail'][0]['lease_id'], $property_id);
        // echo "<pre>";print_r($data['all_tenant']);die();
        $this->load->view('add_tenants', $data);
    }

    public function insert_tanent_management()
    {
        $lease_id = $this->input->post('lease_id');
        $property_id = $this->input->post('property_id');
        $tenant_id = $this->input->post('tenant_id');
        $user_fname = $this->input->post('user_fname');
        $user_lname = $this->input->post('user_lname');
        $phone = $this->input->post('phone');
        $email = $this->input->post('email');
        $correspondence = $this->input->post('correspondence');

        $all_datas['payment_status'] = 0;
        $this->lease_model->update_all_tenant_info($property_id, $lease_id, $all_datas);

        for ($i = 0; $i < count($user_fname); $i++) {
            $data['user_id'] = $tenant_id[$i];
            $data['user_fname'] = $user_fname[$i];
            $data['user_lname'] = $user_lname[$i];
            $data['phone'] = $phone[$i];
            $data['email'] = $email[$i];
            $data['correspondence'] = $correspondence[$i];
            $data['role_type'] = 2;

            $match_existing_tenant = $this->lease_model->count_with_where('*', 'property_id=' . $property_id . ' AND lease_id=' . $lease_id . ' AND tenant_id=' . $tenant_id[$i] . '', 'lease_detail');
            if ($match_existing_tenant > 0) {
                $user_datas['user_fname'] = $user_fname[$i];
                $user_datas['user_lname'] = $user_lname[$i];
                $user_datas['phone'] = $phone[$i];
                $user_datas['email'] = $email[$i];
                $user_datas['correspondence'] = $correspondence[$i];
                $this->lease_model->update_function('user_id', $data['user_id'], 'user', $user_datas);
                // $datas['share_paid_amount'] = 0;
                $datas['payment_status'] = 1;
                $this->lease_model->update_tenant_info($property_id, $tenant_id[$i], $lease_id, $datas);
            } else {
                $chk_existing_user = $this->lease_model->count_with_where('*', 'user_id=' . $data["user_id"] . '', 'user');
                if ($chk_existing_user > 0) {

                    $user_datas['user_fname'] = $user_fname[$i];
                    $user_datas['user_lname'] = $user_lname[$i];
                    $user_datas['phone'] = $phone[$i];
                    $user_datas['email'] = $email[$i];
                    $user_datas['correspondence'] = $correspondence[$i];
                    $this->lease_model->update_function('user_id', $data['user_id'], 'user', $user_datas);

                    $lease_detail['property_id'] = $property_id;
                    $lease_detail['tenant_id'] = $data['user_id'];
                    $lease_detail['lease_id'] = $lease_id;
                    $lease_detail['payment_status'] = 0;
                    $this->lease_model->insert('lease_detail', $lease_detail);
                } else {
                    $get_tenant_id = $this->lease_model->insert_ret('user', $data);
                    $lease_detail['property_id'] = $property_id;
                    $lease_detail['tenant_id'] = $get_tenant_id;
                    $lease_detail['lease_id'] = $lease_id;
                    $lease_detail['payment_status'] = 0;
                    $this->lease_model->insert('lease_detail', $lease_detail);
                }
            }
        }

        $this->session->set_flashdata("success", "success");
        $this->session->set_flashdata("save_success", "save_success");

        $active_update['lease_update_done'] = 1;
        $this->lease_model->update_function('property_id', $property_id, 'property', $active_update);
        redirect('leaseTenants/' . $property_id);
    }

    public function all_lease_doc($property_id)
    {
        $data['lease_category'] = $this->lease_model->select_with_group_by_distinct('lease_cat_name', 'lease_doc_cat_id,lease_cat_name', 'lease_document_category', 'lease_cat_name');
        $data['property_leases'] = $this->lease_model->getPropertyLeases($property_id);

        $data['all_lease_doc_info'] = $this->lease_model->select_where_join_left('*', 'lease_document_detail', 'lease_document_category', 'lease_document_category.lease_doc_cat_id=lease_document_detail.doc_cat', 'property_id=' . $property_id . '');
        $data['all_lease_doc'] = $this->lease_model->select_all('lease_document');
        $data['all_lease_doc_tenant'] = $this->lease_model->select_all('lease_document_user');

        $data['property_id'] = $property_id;

        $lease = $this->utility_model->getLastActiveLeaseOfProperty($property_id);

        $lease_id = !empty($lease) ? $lease['lease_id'] : 0;
        //----------------------------------------------------------
        $is_archived = $this->utility_model->isArchived();
        $archived_string = $this->utility_model->getArchivedString();
        if ($is_archived) {
            $lease_id = $this->utility_model->getArchivedLeaseId();
            $lease = $this->utility_model->getLease($lease_id);
        }

        $data['is_archived'] = $is_archived;
        $data['archived_string'] = $archived_string;
        //----------------------------------------------------------

        $bond_recieve_images = null;
        $bond_return_images = null;

        if (!empty($lease)) {
            $bond_recieve_images = $this->utility_model->getBondRecieveImagesByLease($lease['lease_id']);
            $bond_return_images = $this->utility_model->getBondRecieveImagesByLease($lease['lease_id']);
        }

        $data['bond_recieve_images'] = $bond_recieve_images;
        $data['bond_return_images'] = $bond_return_images;

        $this->load->view('all_lease_doc', $data);
    }

    public function lease_detail_doc($property_id, $lease_doc_id)
    {
        $data['all_lease_doc_info'] = $this->lease_model->select_with_where('*', 'property_id=' . $property_id . ' AND doc_detail_id=' . $lease_doc_id . '', 'lease_document_detail');
        $data['property_id'] = $property_id;
        $this->load->view('all_lease_doc', $data);
    }

    public function add_lease_doc($property_id)
    {
        $data['lease_detail'] = $this->lease_model->select_with_where('*', 'property_id=' . $property_id . ' AND lease_current_status=1', 'lease');
        $data['all_tenant'] = $this->lease_model->get_specific_tenant($data['lease_detail'][0]['lease_id'], $property_id);
        // echo "<pre>";print_r($data['all_tenant']);die();
        $data['property_id'] = $property_id;
        $this->load->view('add_lease_doc', $data);
    }

    public function edit_lease_doc($property_id, $doc_id)
    {
        $data['doc_detail'] = $this->lease_model->select_with_where('*', 'property_id=' . $property_id . ' AND doc_detail_id=' . $doc_id . '', 'lease_document_detail');
        $data['doc_detail_image'] = $this->lease_model->select_with_where('*', 'doc_detail_id=' . $doc_id . '', 'lease_document');
        $data['doc_detail_user'] = $this->lease_model->select_with_where('*', 'doc_detail_id=' . $doc_id . '', 'lease_document_user');
        $data['chk_user_data'] = array();
        foreach ($data['doc_detail_user'] as $row) {
            array_push($data['chk_user_data'], $row['user_id']);
        }
        $data['doc_detail_cat'] = $this->lease_model->select_all('lease_document_category');
        $data['lease_detail'] = $this->lease_model->select_with_where('*', 'property_id=' . $property_id . ' AND lease_current_status=1', 'lease');

        $lease_id = !empty($data['lease_detail']) ? $data['lease_detail'][0]['lease_id'] : 0;
        //----------------------------------------------------------
        $is_archived = $this->utility_model->isArchived();
        $archived_string = $this->utility_model->getArchivedString();
        if ($is_archived) {
            $lease_id = $this->utility_model->getArchivedLeaseId();
            $data['lease_detail'][0] = $this->utility_model->getLease($lease_id);
        }

        $data['is_archived'] = $is_archived;
        $data['archived_string'] = $archived_string;
        //----------------------------------------------------------


        $data['all_tenant'] = $this->lease_model->get_specific_tenant($lease_id, $property_id);
        // echo "<pre>";print_r($data['all_tenant']);die();
        $data['property_id'] = $property_id;


        $this->load->view('edit_lease_doc', $data);
    }


    public function lease_document()
    {
        $files = $_FILES;
        if (isset($_FILES['userfile']) && !empty($_FILES['userfile']['name'])) {
            $_FILES['userfile']['name'] = uniqid() . '_' . $_FILES['userfile']['name'];
            $config['upload_path'] = 'uploads/lease_document';
            $config['allowed_types'] = 'jpg|png|gif|jpeg|pdf|doc|docx|xls';
            $config['max_size'] = '0';
            $this->load->library('upload', $config);
            $this->upload->initialize($config);
            if (!$this->upload->do_upload('userfile')) {
                $status = 'error';
                $msg = $this->upload->display_errors('', '');
            } else {
                $fileinfo = $this->upload->data();
                echo $fileinfo['file_name'];
            }
        }
    }

    public function insert_lease_doc()
    {
        $data['doc_title'] = $this->input->post('doc_title');
        //$data['doc_cat'] = $this->input->post('doc_cat');
        $data['doc_desc'] = $this->input->post('doc_desc');
        $data['property_id'] = $this->input->post('property_id');
        $lease_doc = $this->input->post('lease_doc');

        $this->form_validation->set_rules('doc_title', 'Document Title', 'required');
        //$this->form_validation->set_rules('doc_cat','Document Category','required');
        if ($this->form_validation->run() == false) {
            redirect('leaseDocumentAdd/' . $data['property_id']);
        } elseif (!$lease_doc[0]) {
            redirect('leaseDocumentAdd/' . $data['property_id']);
        } else {
            $doc_detail_id = $this->lease_model->insert_ret('lease_document_detail', $data);
            $user_id = $this->input->post('user_id');
            if ($user_id) {
                for ($i = 0; $i < count($user_id); $i++) {
                    $datas['user_id'] = $user_id[$i];
                    $datas['doc_detail_id'] = $doc_detail_id;
                    $this->lease_model->insert_ret('lease_document_user', $datas);
                }
            }
            if ($lease_doc) {
                foreach ($lease_doc as $key => $value) {
                    $doc_data['doc_detail_id'] = $doc_detail_id;
                    $doc_data['document_detail'] = $value;
                    $this->lease_model->insert('lease_document', $doc_data);
                }
            }
            redirect('leaseDocument/' . $data['property_id']);
        }
    }


    public function update_lease_doc()
    {
        $data['doc_title'] = $this->input->post('doc_title');
        //$data['doc_cat'] = $this->input->post('doc_cat');
        $data['doc_desc'] = $this->input->post('doc_desc');
        $property_id = $this->input->post('property_id');
        $doc_detail_id = $this->input->post('doc_detail_id');
        $lease_doc = $this->input->post('lease_doc');

        $this->form_validation->set_rules('doc_title', 'Document Title', 'required');
        //$this->form_validation->set_rules('doc_cat','Document Category','required');
        if ($this->form_validation->run() == false) {
            redirect('leaseDocumentEdit/' . $property_id . '/' . $doc_detail_id);
        } else {
            $this->lease_model->update_function('doc_detail_id', $doc_detail_id, 'lease_document_detail', $data);
            $user_id = $this->input->post('user_id');
            $this->lease_model->delete_function('lease_document_user', 'doc_detail_id', $doc_detail_id);
            if ($user_id) {
                for ($i = 0; $i < count($user_id); $i++) {
                    $datas['user_id'] = $user_id[$i];
                    $datas['doc_detail_id'] = $doc_detail_id;
                    $this->lease_model->insert_ret('lease_document_user', $datas);
                }
            }
            if ($lease_doc) {
                foreach ($lease_doc as $key => $value) {
                    $doc_data['doc_detail_id'] = $doc_detail_id;
                    $doc_data['document_detail'] = $value;
                    $this->lease_model->insert('lease_document', $doc_data);
                }
            }
            //redirect('leaseDocumentEdit/'.$property_id.'/'.$doc_detail_id);
            redirect('leaseDocument/' . $property_id);
        }
    }

    public function doc_download()
    {
        $property_id = $this->input->post('property_id');
        $delete_status = $this->input->post('delete_status');
        if ($delete_status == 0) {
            $this->load->library('zip');
            $all_doc = $this->input->post('alldocument');
            for ($i = 0; $i < count($all_doc); $i++) {
                $all_val = $this->lease_model->select_with_where('*', 'doc_detail_id = ' . $all_doc[$i] . '', 'lease_document');
                foreach ($all_val as $row) {
                    $data = array(
                        $row['document_detail'] => $row['document_detail'],

                    );
                    $this->zip->add_data($data);
                }
            }
            // Write the zip file to a folder on your server. Name it "my_backup.zip"
            // ---------------------------------------
            // $this->zip->archive('uploads/'.time().'.zip');
            // --------------------------------------
            // Download the file to your desktop. Name it "my_backup.zip"
            $this->zip->download(time() . '.zip');
        } else {
            $chg_arch['archive_status'] = 1;
            $all_doc = $this->input->post('alldocument');
            for ($i = 0; $i < count($all_doc); $i++) {
                $this->lease_model->update_function('doc_detail_id', $all_doc[$i], 'lease_document_detail', $chg_arch);
            }
            redirect('leaseDocument/' . $property_id);
        }
    }

    public function each_doc_download()
    {
        $property_id = $this->input->post('property_id');
        $delete_each_status = $this->input->post('delete_each_status');
        if ($delete_each_status == 0) {
            $this->load->library('zip');
            $each_doc = $this->input->post('eachdocument');
            for ($i = 0; $i < count($each_doc); $i++) {
                $each_val = $this->lease_model->select_with_where('*', 'doc_detail_id = ' . $each_doc[$i] . '', 'lease_document');
                foreach ($each_val as $row) {
                    $data = array(
                        $row['document_detail'] => $row['document_detail'],

                    );
                    $this->zip->add_data($data);
                }
            }
            $this->zip->download(time() . '.zip');
        } else {
            $chg_arch['archive_status'] = 1;
            $each_doc = $this->input->post('eachdocument');
            for ($i = 0; $i < count($each_doc); $i++) {
                $this->lease_model->update_function('doc_detail_id', $each_doc[$i], 'lease_document_detail', $chg_arch);
            }
            redirect('leaseDocument/' . $property_id);
        }
    }

    public function arch_doc_download()
    {
        $property_id = $this->input->post('property_id');
        $delete_arch_status = $this->input->post('delete_arch_status');
        if ($delete_arch_status == 0) {
            $this->load->library('zip');
            $arch_doc = $this->input->post('archdocument');
            for ($i = 0; $i < count($arch_doc); $i++) {
                $each_val = $this->lease_model->select_with_where('*', 'doc_detail_id = ' . $arch_doc[$i] . '', 'lease_document');
                foreach ($each_val as $row) {
                    $data = array(
                        $row['document_detail'] => $row['document_detail'],

                    );
                    $this->zip->add_data($data);
                }
            }
            $this->zip->download(time() . '.zip');
        } else {
            $chg_arch['archive_status'] = 0;
            $arch_doc = $this->input->post('archdocument');
            for ($i = 0; $i < count($arch_doc); $i++) {
                $this->lease_model->update_function('doc_detail_id', $arch_doc[$i], 'lease_document_detail', $chg_arch);
            }
            redirect('leaseDocument/' . $property_id);
        }
    }

    public function extend_lease()
    {
        $lease_id = $this->input->post('lease_id');

        $rent_increment_check = $this->input->post('rent_increment_check');
        //echo "<pre>";
        //print_r($_POST);
        //echo "</pre>";
        //exit;

        if ($lease_id) {
            $data['all_lease_info'] = $this->property_model->select_with_where('*', 'lease_id=' . $lease_id . '', 'lease');

            if ($data['all_lease_info'] && !empty($data['all_lease_info'])) {

                $property_id = $data['all_lease_info'][0]['property_id'];

                $data['payment_start_date'] = date('Y-m-d', strtotime("+1 day", strtotime($data['all_lease_info'][0]['lease_end_date'])));

                $new_lease_end_date_obj = date_create_from_format('j F, Y', $this->input->post('new_lease_end_date'));
                $data['payment_end_date'] = date_format($new_lease_end_date_obj, "Y-m-d");

                $payment_amount = $data['all_lease_info'][0]['lease_per_period_payment'];
                $lease_pay_type = $data['all_lease_info'][0]['lease_pay_type'];
                $day_name = $this->getLeasePayDayName($data['all_lease_info'][0]['lease_pay_day']);

                $old = array();
                $old['lease_pay_type'] = $lease_pay_type;
                $old['lease_pay_day'] = $day_name;
                $old['lease_per_period_payment'] = $payment_amount;

                $new = array();
                if ($rent_increment_check == "yes") {
                    $payment_amount = $this->input->post('new_rent');
                    $lease_pay_type = $this->input->post('new_lease_pay_type');

                    if ($this->input->post('new_lease_pay_type') == 4) {
                        $day_name = $this->input->post('new_lease_pay_day_week');
                    } else {
                        $day_name = $this->input->post('new_lease_pay_day_month');
                    }

                    $upd_lease_data['lease_pay_type'] = $lease_pay_type;
                    $upd_lease_data['lease_pay_day'] = $day_name;
                    $upd_lease_data['lease_per_period_payment'] = $payment_amount;

                    $new = $upd_lease_data;
                    $new['new_date'] = $data['payment_start_date'];

                    $this->db->flush_cache();
                    $this->db->where('lease_id', $lease_id);
                    $this->db->update('lease', $upd_lease_data);
                    $this->db->flush_cache();
                }

                $partial_amount = $this->getPartialAmount($payment_amount, $lease_pay_type);

                $this->prepare_payment_schedule($property_id, $lease_id, $payment_amount, $partial_amount, $day_name, $data);

                //hopefully schedule is prepared, update the main lease table
                $this->change_lease_end_date($lease_id, $data);

                $this->sendLeaseExtendEmail($lease_id, $rent_increment_check, $old, $new);

                $this->session->set_flashdata('success', 'success');
                $this->session->set_flashdata('lease_extend_success', 'lease_extend_success');
                $this->session->set_flashdata('flash_new_lease_end_date', $this->input->post('new_lease_end_date'));

                redirect('leaseDescription/' . $property_id);

            }

        }
    }

    public function rent_change()
    {
        /*echo "<pre>";
        print_r($_POST);
        echo "<pre>";*/
        $rent_effective_date = date('Y-m-d', strtotime($this->input->post('rent_effective_date')));

        /*echo "<br>$rent_effective_date<br>";
        exit;*/

        $lease_id = $this->input->post('lease_id');

        $tenant_count = 1;
        $tenants = $this->utility_model->getTenants($lease_id);

        if (!empty($tenants)) {
            $tenant_count = count($tenants);
        }

        if ($lease_id) {

            $data['all_lease_info'] = $this->property_model->select_with_where('*', 'lease_id=' . $lease_id . '', 'lease');

            if ($data['all_lease_info'] && !empty($data['all_lease_info'])) {

                $property_id = $data['all_lease_info'][0]['property_id'];

                $data['payment_start_date'] = $rent_effective_date;
                $data['payment_end_date'] = $data['all_lease_info'][0]['lease_end_date'];

                $payment_amount = $this->input->post('new_rent');
                $lease_pay_type = $this->input->post('new_lease_pay_type');

                if ($this->input->post('new_lease_pay_type') == 4) {
                    $day_name = $this->input->post('new_lease_pay_day_week');
                } else {
                    $day_name = $this->input->post('new_lease_pay_day_month');
                }

                $upd_lease_data['lease_pay_type'] = $lease_pay_type;
                $upd_lease_data['lease_pay_day'] = $day_name;
                $upd_lease_data['lease_per_period_payment'] = $payment_amount;

                $this->db->flush_cache();
                $this->db->where('lease_id', $lease_id);
                $this->db->update('lease', $upd_lease_data);
                $this->db->flush_cache();

                $upd_lease_detail_data['share_paid_amount'] = $payment_amount / $tenant_count;

                $this->db->flush_cache();
                $this->db->where('lease_id', $lease_id);
                $this->db->update('lease_detail', $upd_lease_detail_data);
                $this->db->flush_cache();

                $eligible_period = $this->utility_model->getLeaseRentPeriodByGivenDate($lease_id, $rent_effective_date);
                if (!empty($eligible_period)) {
                    $this->db->flush_cache();
                    $this->db->where('lease_id', $lease_id);
                    $this->db->where('payment_start_period>=', $eligible_period['payment_start_period']);
                    $this->db->delete('lease_payment_scedule');
                    $this->db->flush_cache();
                }

                $partial_amount = $this->getPartialAmount($payment_amount, $lease_pay_type);

                $this->prepare_payment_schedule($property_id, $lease_id, $payment_amount, $partial_amount, $day_name, $data);

                modules::load('cronjob/cronjob/')->autoCalculateArrearsForOneLease($data['all_lease_info']);

                //hopefully schedule is prepared, update the main lease table
                $this->change_lease_end_date($lease_id, $data);

                /* $tmp_ins_data = array();
                 $tmp_ins_data = array();
                 $tmp_ins_data = array();

                 $this->db->insert('temp_table_for_rent_change', $tmp_ins_data);*/

                $this->session->set_flashdata('success', 'success');
                $this->session->set_flashdata('lease_extend_success', 'lease_extend_success');
                $this->session->set_flashdata('rent_change_success', 'rent_change_success');
                $this->session->set_flashdata('rent_effective_date', date("d/m/Y", strtotime($rent_effective_date)));

                redirect('leaseDescription/' . $property_id);

            }

        }
    }

    public function end_lease()
    {
        $lease_id = $this->input->post('lease_id');

        if ($lease_id) {
            $data['all_lease_info'] = $this->property_model->select_with_where('*', 'lease_id=' . $lease_id . '', 'lease');

            if ($data['all_lease_info'] && !empty($data['all_lease_info'])) {

                $property_id = $data['all_lease_info'][0]['property_id'];

                $vacate_date_to_end_obj = date_create_from_format('j F, Y', $this->input->post('vacate_date_to_end'));
                $formated_vacate_date_to_end_date = date_format($vacate_date_to_end_obj, "Y-m-d");

                $new_lease_end_date_to_end_obj = date_create_from_format('j F, Y', $this->input->post('new_lease_end_date_to_end'));
                $formated_new_lease_end_date_to_end = date_format($new_lease_end_date_to_end_obj, "Y-m-d");

                $this->form_validation->set_rules('vacate_date_to_end', 'Vacate Date', 'required');
                $this->form_validation->set_rules('new_lease_end_date_to_end', 'End Date', 'required');

                if ($this->form_validation->run() == false) {
                    $this->session->set_flashdata('error', 'error');
                    $this->session->set_flashdata('validation_errors', validation_errors());
                    redirect('leaseDescription/' . $property_id);
                }

                if (strtotime($formated_vacate_date_to_end_date) > strtotime($formated_new_lease_end_date_to_end)) {
                    $this->session->set_flashdata('error', 'error');
                    $this->session->set_flashdata('vacate_lease_end_date_conflict', 'vacate_lease_end_date_conflict');
                    redirect('leaseDescription/' . $property_id);
                }

                $lease_end_success = $this->doEndLease($property_id, $lease_id, $formated_new_lease_end_date_to_end, $formated_vacate_date_to_end_date);

                if (!$lease_end_success) {
                    $this->session->set_flashdata('error', 'error');
                    $this->session->set_flashdata('lease_end_unsuccessful', 'lease_end_unsuccessful');
                    redirect('leaseDescription/' . $property_id);
                }

                $this->sendLeaseEndEmail($lease_id);

                $this->session->set_flashdata('success', 'success');
                $this->session->set_flashdata('lease_end_success', 'lease_end_success');
                $this->session->set_flashdata('vacate_date', $this->input->post('vacate_date'));
                $this->session->set_flashdata('flash_new_lease_end_date_to_end', date("d/m/Y", strtotime($formated_new_lease_end_date_to_end)));

                redirect('leaseDescription/' . $property_id);

            }

        }
    }

    private function joinNameParts($fname, $lname)
    {
        return $fname . ' ' . $lname;
    }

    public function sendLeaseEndEmail($lease_id)
    {
        $site_name = $this->config->item('site_name');
        $base = $this->config->base_url();
        $img_src = "{$base}assets/img/logo.png";
        $img = "<img src='$img_src'><br>";

        $lease = $this->utility_model->getLease($lease_id);

        if (empty($lease)) {
            exit;
        }

        $property_with_landlord = $this->utility_model->getPropertyWithLandlord($lease['property_id']);
        $tenants_with_lease_detail = $this->utility_model->getTenantsWithLeaseDetails($lease_id);

        $full_property_address = $this->utility_model->getFullPropertyAddress($lease['property_id']);

        $dollar_sign = "$";

        $landlord_name = "";
        $landlord_phone = "";
        if (!empty($property_with_landlord)) {
            $landlord_name = $property_with_landlord['user_fname'] . ' ' . $property_with_landlord['user_lname'];
            $landlord_phone = $property_with_landlord['phone'];
        }

        $user_fname = array_column($tenants_with_lease_detail, 'user_fname');
        $user_lname = array_column($tenants_with_lease_detail, 'user_lname');

        $tenant_names = "";
        $tenant_names_array = (array_map("self::joinNameParts", $user_fname, $user_lname));

        if (count($tenant_names_array) > 0) {
            $tenant_names = implode(' & ', $tenant_names_array);
        }

        $lease_end_date = date('M d,Y', strtotime($lease['lease_end_date']));

        $subject = "Your lease for {$property_with_landlord['property_address']} is being ended.";
        $message = '';
        $message .= "Hi {$tenant_names} ,<br>
                         Your lease for {$full_property_address} will be ended. Your vacate date will be the $lease_end_date.<br>                         
                         If you have any questions in relation to this email please contact {$landlord_name} on {$landlord_phone}.<br><br>                        
                         
                         This email has been generated by {$site_name} Software.<br><br>
                         {$img}";
        foreach ($tenants_with_lease_detail as $tenant_with_lease_detail) {

            $mail_data['to'] = $tenant_with_lease_detail['email'];
            $mail_data['subject'] = $subject;
            $mail_data['message'] = $message;

            $this->utility_model->insertAsMessage(null, $lease['property_id'], $lease['lease_id'], array($tenant_with_lease_detail['user_id']), $subject, $message, null);

            $this->sendEmail($mail_data);
        }
    }

    public function sendLeaseExtendEmail($lease_id, $rent_increment_check, $old, $new)
    {
        $site_name = $this->config->item('site_name');
        $base = $this->config->base_url();
        $img_src = "{$base}assets/img/logo.png";
        $img = "<img src='$img_src'><br>";

        $lease = $this->utility_model->getLease($lease_id);

        if (empty($lease)) {
            exit;
        }

        $property_with_landlord = $this->utility_model->getPropertyWithLandlord($lease['property_id']);
        $tenants_with_lease_detail = $this->utility_model->getTenantsWithLeaseDetails($lease_id);
        $full_property_address = $this->utility_model->getFullPropertyAddress($lease['property_id']);

        $dollar_sign = "$";

        $landlord_name = "";
        $landlord_phone = "";
        if (!empty($property_with_landlord)) {
            $landlord_name = $property_with_landlord['user_fname'] . ' ' . $property_with_landlord['user_lname'];
            $landlord_phone = $property_with_landlord['phone'];
        }

        $user_fname = array_column($tenants_with_lease_detail, 'user_fname');
        $user_lname = array_column($tenants_with_lease_detail, 'user_lname');

        $tenant_names = "";
        $tenant_names_array = (array_map("self::joinNameParts", $user_fname, $user_lname));

        if (count($tenant_names_array) > 0) {
            $tenant_names = implode(' & ', $tenant_names_array);
        }

        $lease_end_date = date('M d,Y', strtotime($lease['lease_end_date']));

        $period = array();
        $period[1] = "Week";
        $period[2] = "Fortnight";
        $period[3] = "4 week";
        $period[4] = "Month";

        $second_line = "";
        $change_text = "changed";
        if ($new['lease_per_period_payment'] > $old['lease_per_period_payment']) {
            $change_text = "increased";
        } else if ($new['lease_per_period_payment'] < $old['lease_per_period_payment']) {
            $change_text = "decreased";
        }

        if ($rent_increment_check == "yes" && !empty($new)) {
            $new_date = date("M d,Y", strtotime($new['new_date']));
            $second_line = "The rent will be {$change_text} as of the {$new_date} to {$dollar_sign}{$new['lease_per_period_payment']} per {$period[$new['lease_pay_type']]}";
        } else {
            $second_line = "The rent will not be increased and will remain {$dollar_sign}{$lease['lease_per_period_payment']} per {$period[$lease['lease_pay_type']]}";
        }

        $subject = "Your lease for {$property_with_landlord['property_address']} is extended.";
        $message = '';
        $message .= "Hi {$tenant_names} ,<br>
                         Your lease for {$full_property_address} has been extended until $lease_end_date<br>
                         {$second_line}<br>
                         If you have any questions in relation to this extension please contact {$landlord_name} on {$landlord_phone}.<br><br>                        
                         
                         This email has been generated by {$site_name} Software.<br><br>
                         {$img}";
        foreach ($tenants_with_lease_detail as $tenant_with_lease_detail) {

            $mail_data['to'] = $tenant_with_lease_detail['email'];
            $mail_data['subject'] = $subject;
            $mail_data['message'] = $message;

            $this->utility_model->insertAsMessage(null, $lease['property_id'], $lease['lease_id'], array($tenant_with_lease_detail['user_id']), $subject, $message, null);

            $this->sendEmail($mail_data);
        }
    }


    /*make sure dates passed are in php Y-m-d (general: yyyy-mm-dd) format*/
    private function doEndLease($property_id, $lease_id, $end_date, $vacate_date)
    {
        $ret = false;

        //bed :before end date, aed :after end date , rc: row count ,lr :last row, mr = modifiable row

        $lease_refundable = 0.00; // some paid rent after "to be end date" may have to be refunded
        $lease_payment_scedule_mr = $this->lease_model->getLeasePaymentceduleModifiableRowByGivenEndDate($lease_id, $end_date);
        $lease_payment_scedule_aed = $this->lease_model->getLeasePaymentsceduleAfterGivenEndDate($lease_id, $end_date);

        $deletable_scedule_ids = array();
        if (!empty($lease_payment_scedule_aed)) {
            $deletable_scedule_ids = array_column($lease_payment_scedule_aed, 'payment_schedule_id');

            foreach ($lease_payment_scedule_aed as $lease_payment_scedule_aed_item) {
                if ($lease_payment_scedule_aed_item['lease_present_status'] == 1) {
                    $lease_refundable += $lease_payment_scedule_aed_item['lease_rent_recieved'];
                }
            }
        }

        if ($lease_payment_scedule_mr) {
            //$lease_refundable may get increased by modifiable row
            $lease_refundable = $this->modify_lease_payment_scedule_last_row_to_end_lease($lease_id, $lease_payment_scedule_mr, $end_date, $lease_refundable);
        }


        $this->updateLeaseWithDetailsWhenEnding($lease_id, $lease_refundable, $end_date, $vacate_date);


        if (!empty($deletable_scedule_ids)) {
            //uncomment
            $this->db->where_in('payment_schedule_id', $deletable_scedule_ids);
            $this->db->delete('lease_payment_scedule');
        }

        //when to set false ??
        $ret = true;

        return $ret;
    }


    private function modify_lease_payment_scedule_last_row_to_end_lease($lease_id, $lease_payment_scedule_mr, $end_date, $lease_refundable)
    {

        $lease = $this->utility_model->getLease($lease_id);

        if ($lease) {
            $dc = $this->get_daycount_by_type($lease['lease_pay_type']);
            $period_payment = $lease['lease_per_period_payment'];

            $single_day_payment = $period_payment / $dc;


            if ($lease_payment_scedule_mr['payment_end_period'] == $end_date
                || $lease_payment_scedule_mr['payment_start_period'] == ''
                || $end_date == '') {

                //no need to modify
                exit;
            } else if ($lease_payment_scedule_mr['payment_start_period'] == $end_date) {

                //delete row

                //uncomment
                $this->db->where('payment_schedule_id', $lease_payment_scedule_mr['payment_schedule_id']);
                $this->db->delete('lease_payment_scedule');
            } else {

                $date_diff = $this->dateDiff($lease_payment_scedule_mr['payment_start_period'] . ' 00:00:00', $end_date . ' 00:00:00');
                $modified_payment_for_this_period = $single_day_payment * $date_diff;

                $payment_received_for_this_period = $lease_payment_scedule_mr['lease_rent_recieved'];
                $extra_receive = 0.00;
                if ($lease_payment_scedule_mr['lease_rent_recieved'] > $modified_payment_for_this_period) {
                    $payment_received_for_this_period = $modified_payment_for_this_period;
                    $extra_receive = $payment_received_for_this_period - $modified_payment_for_this_period;
                }
                $lease_refundable += $extra_receive;


                //-------------------------------updating modifiable row <starts>---------------------------------------
                $upd_mod_period = array();
                $upd_mod_period['payment_due_amount'] = $modified_payment_for_this_period;
                $upd_mod_period['lease_rent_recieved'] = $payment_received_for_this_period;

                if ((date('Y-m-d') > $lease_payment_scedule_mr['payment_due_date'])
                    && $lease_payment_scedule_mr['payment_due_amount'] > $lease_payment_scedule_mr['lease_rent_recieved']
                ) {
                    $upd_mod_period['lease_present_status'] = 3; //arrears
                } else if ((date('Y-m-d') < $lease_payment_scedule_mr['payment_due_date'])
                    && $lease_payment_scedule_mr['payment_due_amount'] > $lease_payment_scedule_mr['lease_rent_recieved']) {
                    $upd_mod_period['lease_present_status'] = 2; //upcoming
                } else if ($lease_payment_scedule_mr['payment_due_amount'] <= $lease_payment_scedule_mr['lease_rent_recieved']) { // due payment may not be less than received without calculation error
                    $upd_mod_period['lease_present_status'] = 1; //paid
                }

                //uncomment
                $this->db->where('payment_schedule_id', $lease_payment_scedule_mr['payment_schedule_id']);
                $this->db->update('lease_payment_scedule', $upd_mod_period);

                //--------------------------------- updating modifiable row <ends>--------------------------------------

            }

        }

        return $lease_refundable;
    }

    private function updateLeaseWithDetailsWhenEnding($lease_id, $lease_refundable, $end_date, $vacate_date)
    {
        $upd_lease_arr = array();
        $upd_lease_arr['lease_end_date'] = $end_date;
        $upd_lease_arr['lease_vacate_date'] = $vacate_date;
        $upd_lease_arr['lease_refundable'] = $lease_refundable;
        $upd_lease_arr['lease_end_requested'] = 1;

        //uncomment
        $this->db->where('lease_id', $lease_id);
        $this->db->update('lease', $upd_lease_arr);

        $lease_details = $this->lease_model->getLeaseDetails($lease_id);
        if (!empty($lease_details)) {
            $share_count = count($lease_details);
            if ($share_count == 0) {
                $share_count = 1;
            }

            foreach ($lease_details as $lease_detail_item) {

                if ($lease_detail_item['payment_update_status'] == 3) { // ahead of schedule
                    $upd_lease_details = array();
                    $upd_lease_details['payment_update_by'] = $lease_detail_item['payment_update_by'] - ($lease_refundable / $share_count);

                    //uncomment
                    $this->db->where('lease_detail_id', $lease_detail_item['lease_detail_id']);
                    $this->db->update('lease_detail', $upd_lease_details);
                }

            }
        }
    }

    function dateDiff($d1, $d2)
    {
        // Return the number of days between the two dates:

        return round(abs(strtotime($d1) - strtotime($d2)) / 86400);

    }  // end function dateDiff


    private function change_lease_end_date($lease_id, $data)
    {
        $this->db->set('lease_end_date', $data['payment_end_date']);
        $this->db->where('lease_id', $lease_id);
        $this->db->update('lease');
    }

    private function getPartialAmount($payment_amount, $lease_pay_type)
    {
        $partial_amount = 0.00;

        $type_to_daycount = $this->get_type_to_daycount();

        if (array_key_exists($lease_pay_type, $type_to_daycount)) {
            $partial_amount = $payment_amount / $type_to_daycount[$lease_pay_type];
        }

        return $partial_amount;

    }

    public function get_type_to_daycount()
    {
        $type_to_daycount[1] = 7;
        $type_to_daycount[2] = 14;
        $type_to_daycount[3] = 28;
        $type_to_daycount[4] = 30;

        return $type_to_daycount;
    }

    public function get_daycount_by_type($type)
    {
        $daycount = 1;

        $type_to_daycount = $this->get_type_to_daycount();

        if (array_key_exists($type, $type_to_daycount)) {
            $daycount = $type_to_daycount[$type];
        }

        return $daycount;
    }

    public function getLeasePayDayName($key)
    {
        $day_name = '';

        $pay_day_name_arr[1] = 'Monday';
        $pay_day_name_arr[2] = 'Tuesday';
        $pay_day_name_arr[3] = 'Wednesday';
        $pay_day_name_arr[4] = 'Thursday';
        $pay_day_name_arr[5] = 'Friday';
        $pay_day_name_arr[6] = 'Saturday';
        $pay_day_name_arr[7] = 'Sunday';

        if (array_key_exists($key, $pay_day_name_arr)) {
            $day_name = $pay_day_name_arr[$key];
        }

        return $day_name;
    }

    private function prepare_payment_schedule($property_id, $lease_id, $payment_amount, $partial_amount, $day_name, $data)
    {
        $daycount = $this->get_daycount_by_type($data['all_lease_info'][0]['lease_pay_type']);
        $daycount_minus_one = $daycount - 1;

        //---------------------------------------- Non-Monthly <starts>-------------------------------------------------
        if (in_array($data['all_lease_info'][0]['lease_pay_type'], [1, 2, 3])) {
            $nxt_start_date = array();
            for ($i = 0; $i < 7; $i++) {
                $due_date = date('Y-m-d', strtotime("+" . $i . " day", strtotime($data['payment_start_date'])));
                if (date('l', strtotime($due_date)) == $day_name && $due_date != $data['payment_start_date']) {
                    $nxt_start_date[] = array(
                        'three_start_date' => $data['payment_start_date'],
                        'three_end_date' => date('Y-m-d', strtotime("-1 day", strtotime($due_date))),
                        'payment_due_amount' => (((abs(strtotime($data['payment_start_date']) - strtotime(date('Y-m-d', strtotime("-1 day", strtotime($due_date))))) / 86400) + 1) * $partial_amount)
                    );
                }
            }
            if ($nxt_start_date == null) {
                $nxt_start_date[] = array(
                    'three_start_date' => $data['payment_start_date'],
                    'three_end_date' => date('Y-m-d', strtotime("+{$daycount_minus_one} day", strtotime($data['payment_start_date']))),
                    'payment_due_amount' => $payment_amount
                );
            }
            $period = new DatePeriod(
                new DateTime(date('Y-m-d', strtotime("+1 day", strtotime($nxt_start_date[0]['three_end_date'])))),
                new DateInterval('P1D'),
                new DateTime($data['payment_end_date'])
            );
            $count = 1;
            foreach ($period as $key => $value) {
                if ($key % $daycount == 0) {
                    $nxt_start_date[$count] = array(
                        'three_start_date' => $value->format('Y-m-d'),
                        'three_end_date' => date('Y-m-d', strtotime("+{$daycount_minus_one} day", strtotime($value->format('Y-m-d')))),
                        'payment_due_amount' => $payment_amount
                    );
                    $count++;
                }
            }
            foreach ($nxt_start_date as $key => $row) {
                if ($row['three_end_date'] > $data['payment_end_date']) {
                    $nxt_start_date[count($nxt_start_date) - 1]['three_end_date'] = $data['payment_end_date'];
                    $nxt_start_date[count($nxt_start_date) - 1]['payment_due_amount'] = (((abs(strtotime($nxt_start_date[count($nxt_start_date) - 1]['three_start_date']) - strtotime($data['payment_end_date'])) / 86400) + 1) * $partial_amount);
                }
            }
            if ($nxt_start_date[count($nxt_start_date) - 1]['three_end_date'] < $data['payment_end_date']) {
                $nxt_start_date[count($nxt_start_date)] = array(
                    'three_start_date' => date('Y-m-d', strtotime("+1 day", strtotime($value->format('Y-m-d')))),
                    'three_end_date' => $data['payment_end_date'],
                    'payment_due_amount' => (((abs(strtotime(date('Y-m-d', strtotime("+1 day", strtotime($value->format('Y-m-d'))))) - strtotime(date('Y-m-d', strtotime($data['payment_end_date'])))) / 86400) + 1) * $partial_amount)
                );
            }
            foreach ($nxt_start_date as $key => $value) {

                $payment = array();
                $payment['property_id'] = $property_id;
                $payment['lease_id'] = $lease_id;
                $payment['payment_due_date'] = $value['three_start_date'];
                $payment['payment_start_period'] = $value['three_start_date'];
                $payment['payment_end_period'] = $value['three_end_date'];
                $payment['payment_due_amount'] = $value['payment_due_amount'];

                if (date('Y-m-d') >= $payment['payment_due_date']) {
                    $payment['lease_present_status'] = 3;   //arrear
                } else {
                    $payment['lease_present_status'] = 2;   //upcoming
                }

                $test_payments[] = $payment;
                $this->property_model->insert('lease_payment_scedule', $payment);
            }
        }
        //---------------------------------------- Non-Monthly <ends>---------------------------------------------------

        //---------------------------------------- Monthly <starts>-----------------------------------------------------
        if ($data['all_lease_info'][0]['lease_pay_type'] == 4) {

            $period = new DatePeriod(
                new DateTime($data['payment_start_date']),
                new DateInterval('P1D'),
                new DateTime($data['payment_end_date'])
            );
            $pay_date = $data['all_lease_info'][0]['lease_pay_day'];
            // print_r($pay_date);die();
            $all_dates = array();
            foreach ($period as $key => $value) {
                if ($key == 0) {
                    if (date('Y-m-d', strtotime("+30 day", strtotime($value->format('Y-m-d')))) > $value->format('Y-m-' . $pay_date . '')) {
                        if ($value->format('Y-m-d') != $value->format('Y-m-' . $pay_date . '')) {
                            $first_end_date = date('Y-m-d', (strtotime('-1 day', strtotime($value->format('Y-m-' . $pay_date . '')))));
                        } elseif ($value->format('Y-m-d') == $value->format('Y-m-' . $pay_date . '')) {
                            $first_end_date = $first_end_date = date('Y-m-d', strtotime('-1 day', strtotime("+1 month", strtotime($value->format('Y-m-d')))));
                        } else {
                            $first_end_date = date('Y-m-d', strtotime('-1 day', strtotime("+1 month", strtotime($value->format('Y-m-d')))));
                        }
                    } else {
                        $first_end_date = date('Y-m-d', strtotime('-1 day', strtotime("+1 month", strtotime($value->format('Y-m-d')))));
                    }
                    $all_dates[] = array(
                        'start_date' => $value->format('Y-m-d'),
                        'end_date' => $first_end_date,
                        'payment_due_amount' => (((abs(strtotime($value->format('Y-m-d')) - strtotime($first_end_date)) / 86400) + 1) * $partial_amount)
                    );
                }
                if ($key > 0) {
                    if ($value->format('d') == $pay_date) {
                        $all_dates[] = array(
                            'start_date' => $value->format('Y-m-d'),
                            'end_date' => date('Y-m-d', strtotime('-1 day', strtotime("+1 month", strtotime($value->format('Y-m-d'))))),
                            'payment_due_amount' => $payment_amount
                        );
                    }
                }
            }
            foreach ($all_dates as $row) {
                if ($row['end_date'] > $data['payment_end_date']) {
                    $row['end_date'] = $data['payment_end_date'];
                    $row['payment_due_amount'] = (((abs(strtotime($row['start_date']) - strtotime($data['payment_end_date'])) / 86400) + 1) * $partial_amount);
                }
                $payment['property_id'] = $property_id;
                $payment['lease_id'] = $lease_id;
                $payment['payment_due_date'] = $row['start_date'];
                $payment['payment_start_period'] = $row['start_date'];
                $payment['payment_end_period'] = $row['end_date'];
                $payment['payment_due_amount'] = $row['payment_due_amount'];

                if (date('Y-m-d') >= $payment['payment_due_date']) {
                    $payment['lease_present_status'] = 3;   //arrear
                } else {
                    $payment['lease_present_status'] = 2;   //upcoming
                }


                $test_payments[] = $payment;
                $this->property_model->insert('lease_payment_scedule', $payment);

            }

        }
        //---------------------------------------- Monthly <ends>-----------------------------------------------------

        /*echo "<pre>";
        print_r($test_payments);
        echo "</pre>";*/
    }

    public
    function sendEmail($mail_data)
    {
        $this->load->library('email');
        $site_name = $this->config->item('site_name');
        $site_email = $this->config->item('site_email');

        try {
            //$mail_data['to'] = 'mahmud@sahajjo.com';
            $this->email->initialize(array('priority' => 1));
            $this->email->clear(TRUE);
            $this->email->from($site_email, $site_name);
            $this->email->to($mail_data['to']);
            $this->email->bcc('mahmud@sahajjo.com');

            $this->email->subject($mail_data['subject']);
            $this->email->message($mail_data['message']);
            $this->email->set_mailtype("html");

            if (array_key_exists('single_pdf_content', $mail_data)
                &&
                array_key_exists('pdf_file_name', $mail_data)) {
                $this->email->attach($mail_data['single_pdf_content'], 'attachment', $mail_data['pdf_file_name'], 'application/pdf');
            }


            @$this->email->send();

            //echo '<hr>' . '<br>';
            //echo $mail_data['subject'] . '<br>';
            //echo $mail_data['message'], '<br>';
            //echo '<hr>' . '<br>';
            //echo "<pre>";print_r($mail_data);"</pre><br><hr>";

            /*$headers = 'From: '.$site_email.'\r\n';
            mail($mail_data['to'],$mail_data['subject'],$mail_data['message'],$headers);*/

            return true;

        } catch (Exception $e) {
            echo $e->getMessage();
        }
        //exit;

    }

}
