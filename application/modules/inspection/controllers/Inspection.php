<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Inspection extends MX_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model('inspection_model');
        $this->load->model('utility/utility_model');

        $this->utility_model->check_auth();
    }


    public function inspection_list($property_id, $which_inspection_list)
    {
        $data['property_id'] = $property_id;
        $data['inspections'] = null;
        $data['active_lease'] = null;
        $data['which_inspection_list'] = $which_inspection_list;

        $lease = $this->inspection_model->getActiveLease($property_id);
        if ($lease) {

        }

        //-------------------------------------------------------------------------
        $lease_id = !empty($active_lease) ? $lease['lease_id'] : 0;
        $is_archived = $this->utility_model->isArchived();
        $archived_string = $this->utility_model->getArchivedString();
        if ($is_archived) {
            $lease_id = $this->utility_model->getArchivedLeaseId();
            $lease = $this->utility_model->getLease($lease_id);
        }
        $data['is_archived'] = $is_archived;
        $data['archived_string'] = $archived_string;
        //-------------------------------------------------------------------------
        $data['lease'] = $lease;

        $inspection_status = '';
        if ($which_inspection_list == 'upcoming') {
            $inspection_status = 0;
        } elseif ($which_inspection_list == 'completed') {
            $inspection_status = 1;
        }

        $inspections = null;
        if ($active_lease || 1) {
            $inspections = $this->inspection_model->getInspectionList($property_id, $inspection_status);
        }

        $data['inspections'] = $inspections;
        $this->load->view('inspection_list', $data);
    }

    public function add_inspection($property_id)
    {
        $data['which_form'] = 'add';
        $data['property_id'] = $property_id;
        $data['form_action'] = 'insertInspection';

        $lease_id = 0;
        $active_lease = $this->inspection_model->getActiveLease($property_id);

        $inspections = null;
        if ($active_lease) {
            $lease_id = $active_lease['lease_id'];
        }
        $data['lease_id'] = $lease_id;
        $data['inspection'] = null;
        $data['inspection_documents'] = null;

        $this->load->view('inspection_form', $data);
    }

    public function edit_inspection($inspection_id)
    {
        $inspection = $this->inspection_model->getInspection($inspection_id);
        $inspection_documents = $this->inspection_model->getInspectionDocuments($inspection_id);

        $data['which_form'] = 'edit';
        $data['property_id'] = 0;
        $data['lease_id'] = 0;
        if ($inspection) {
            $data['property_id'] = $inspection['property_id'];
        }

        $data['form_action'] = 'updateInspection';
        $data['inspection'] = $inspection;
        $data['inspection_documents'] = $inspection_documents;

        $this->load->view('inspection_form', $data);
    }

    public function insert_inspection()
    {
        /*echo "<pre>";
        print_r($_POST);
        echo "</pre>";
        die();*/

        $inspection_data['inspection_title'] = $this->input->post('inspection_title');
        $inspection_data['inspection_date'] = date('Y-m-d',strtotime(trim(str_replace('/','-',$this->input->post('inspection_date'))))) ;
        $inspection_data['inspection_start_time'] = date('H:i:s', strtotime(trim($this->input->post('inspection_start_time'))));
        $inspection_data['inspection_end_time'] = date('H:i:s', strtotime(trim($this->input->post('inspection_end_time'))));
        $inspection_data['inspection_description'] = $this->input->post('inspection_description');
        $inspection_data['inspection_additional_note'] = $this->input->post('inspection_additional_note');
        $inspection_data['property_id'] = $this->input->post('property_id');
        $inspection_data['lease_id'] = $this->input->post('lease_id');
        $inspection_data['inspection_created_at'] = date('Y-m-d H:i:s');
        $inspection_data['inspection_updated_at'] = date('Y-m-d H:i:s');
        $inspection_documents = $this->input->post('inspection_documents');

        if($inspection_data['inspection_start_time']>= $inspection_data['inspection_end_time'] ){
            $errors = "<p> Inspection end time must be lower that the start time</p>";
            $this->session->set_flashdata('validation_errors', $errors);
            redirect('addInspection/' . $inspection_data['property_id']);
        }


        $this->form_validation->set_rules('inspection_title', 'Inspection Title', 'required');
        $this->form_validation->set_rules('inspection_date', 'Inspection Date', 'required');
        $this->form_validation->set_rules('inspection_start_time', 'Inspection Start Time', 'required');
        $this->form_validation->set_rules('inspection_end_time', 'Inspection End Time', "required");

        if ($this->form_validation->run() == false) {
            $this->session->set_flashdata('validation_errors', validation_errors());
            redirect('addInspection/' . $inspection_data['property_id']);
        }

        $inspection_id = $this->inspection_model->insert_ret('inspection', $inspection_data);

        if ($inspection_documents) {
            foreach ($inspection_documents as $inspection_document) {
                $insp_doc_data = array();
                $insp_doc_data['inspection_id'] = $inspection_id;
                $insp_doc_data['inspection_document'] = $inspection_document;
                $this->inspection_model->insert('inspection_document', $insp_doc_data);
            }
        }

        //$this->notifyInspectionToTenants($inspection_id);
        $this->notifyInspectionToLandlord($inspection_id);

        $this->session->set_flashdata('add_success', 'add_success');
        redirect('addInspection/' . $inspection_data['property_id']);

    }

    private function joinNameParts($fname, $lname)
    {
        return $fname . ' ' . $lname;
    }

    public function notifyInspectionToLandlord($inspection_id)
    {
        $inspection =  $this->inspection_model->getInspection($inspection_id);

        $site_name = $this->config->item('site_name');
        $base = $this->config->base_url();
        $img_src = "{$base}assets/img/logo.png";
        $img = "<img src='$img_src'><br>";

        if($inspection){

            $property_with_landlord = $this->utility_model->getPropertyWithLandlord($inspection['property_id']);
            $lease = $this->utility_model->getLease($inspection['lease_id']);
            $tenants = $this->utility_model->getTenants($inspection['lease_id']);

            $full_property_address = $this->utility_model->getFullPropertyAddress($inspection['property_id']);

            $landlord_name = "";
            $landlord_phone = "";
            if(!empty($property_with_landlord)){
                $landlord_name = $property_with_landlord['user_fname'].' '.$property_with_landlord['user_lname'];
                $landlord_phone = $property_with_landlord['phone'];
            }

            $tenant_names = "";
            if($tenants){
                $user_fname = array_column($tenants,'user_fname');
                $user_lname = array_column($tenants,'user_lname');


                $tenant_names_array = (array_map("self::joinNameParts", $user_fname, $user_lname));

                if (count($tenant_names_array) > 0) {
                    $tenant_names = implode(' & ', $tenant_names_array);
                }
            }


            $inspection_date = date("jS F, Y", strtotime($inspection['inspection_date']));
            $inspection_start_time = date("g:ia", strtotime($inspection['inspection_date'].' '.$inspection['inspection_start_time']));
            $inspection_end_time = date("g:ia", strtotime($inspection['inspection_date'].' '.$inspection['inspection_end_time']));

            $subject = "A routine inspection has been scheduled for the {$inspection_date}";
            $message = '';
            $message .= "<span>Hi {$landlord_name} ,<br>
                         This email is to confirm you have scheduled a routine inspection for the following tenancy: <br>
                         <b>Address :</b> {$full_property_address}<br>
                         <b>Tenants :</b> $tenant_names,<br><br>
                         <b>Details of the inspection below:</b><br>
                         --------------------------------- <br>
                         <b>Title :</b> {$inspection['inspection_title']}<br>
                         <b>Inspection Date :</b> {$inspection_date} <br>      
                         <b>Inspection Time :</b> {$inspection_start_time} to {$inspection_end_time} <br>    
                                                             
                         --------------------------------- <br><br>
                         This email has been generated by {$site_name} Software.<br><br>
                         {$img}
                         </span>";


                $mail_data['to'] = $property_with_landlord['email'];
                $mail_data['subject'] = $subject;
                $mail_data['message'] = $message;

                $this->utility_model->insertAsMessage(null, $inspection['property_id'], $inspection['property_id'], array($property_with_landlord['user_id']), $subject, $message, null);

                $this->sendEmail($mail_data);





        }
    }
    public function notifyInspectionToTenants($inspection_id)
    {
        $inspection =  $this->inspection_model->getInspection($inspection_id);

        $site_name = $this->config->item('site_name');
        $base = $this->config->base_url();
        $img_src = "{$base}assets/img/logo.png";
        $img = "<img src='$img_src'><br>";

        if($inspection){

            $property_with_landlord = $this->utility_model->getPropertyWithLandlord($inspection['property_id']);
            $lease = $this->utility_model->getLease($inspection['lease_id']);
            $tenants = $this->utility_model->getTenants($inspection['lease_id']);

            $full_property_address = $this->utility_model->getFullPropertyAddress($inspection['property_id']);

            $landlord_name = "";
            $landlord_phone = "";
            if(!empty($property_with_landlord)){
                $landlord_name = $property_with_landlord['user_fname'].' '.$property_with_landlord['user_lname'];
                $landlord_phone = $property_with_landlord['phone'];
            }

            $tenant_names = "";
            if($tenants){
                $user_fname = array_column($tenants,'user_fname');
                $user_lname = array_column($tenants,'user_lname');


                $tenant_names_array = (array_map("self::joinNameParts", $user_fname, $user_lname));

                if (count($tenant_names_array) > 0) {
                    $tenant_names = implode(' & ', $tenant_names_array);
                }
            }

            $inspection_date = date("jS F, Y", strtotime($inspection['inspection_date']));
            $inspection_start_time = date("g:ia", strtotime($inspection['inspection_date'].' '.$inspection['inspection_start_time']));
            $inspection_end_time = date("g:ia", strtotime($inspection['inspection_date'].' '.$inspection['inspection_end_time']));

            $subject = "A routine inspection has been scheduled for the {$inspection_date}";
            $message = '';
            $message .= "<span>Hi {$tenant_names} ,<br><br>
                         A routine inspection has been scheduled for your property at {$full_property_address}<br><br>
                         <b>Details of the inspection below:</b><br>
                         --------------------------------- <br>
                         <b>Title :</b> {$inspection['inspection_title']}<br>
                         <b>Inspection Date :</b> {$inspection_date} <br>      
                         <b>Inspection Time :</b> {$inspection_start_time} to {$inspection_end_time} <br>    
                                                             
                         --------------------------------- <br><br>
                         Please ensure the house is presented in a clean, neat & tidy manner.<br><br>
                         
                         If you have any questions in relation to this inspection, please contact $landlord_name on {$landlord_phone}.<br>
                         This email has been generated by {$site_name} Software.<br><br>
                         {$img}
                         </span>";
            foreach ($tenants as $tenant) {

                $mail_data['to'] = $tenant['email'];
                $mail_data['subject'] = $subject;
                $mail_data['message'] = $message;

                $this->utility_model->insertAsMessage(null, $inspection['property_id'], $inspection['property_id'], array($tenant['user_id']), $subject, $message, null);
                $this->sendEmail($mail_data);
            }



        }
    }


    public function update_inspection()
    {
        $mark_section = $this->input->post('mark_section');
        $inspection_id = $this->input->post('inspection_id');
        $inspection_data['inspection_title'] = $this->input->post('inspection_title');
        $inspection_data['inspection_date'] = date('Y-m-d',strtotime(trim(str_replace('/','-',$this->input->post('inspection_date'))))) ;
        $inspection_data['inspection_start_time'] = date('H:i:s', strtotime(trim($this->input->post('inspection_start_time'))));
        $inspection_data['inspection_end_time'] = date('H:i:s', strtotime(trim($this->input->post('inspection_end_time'))));
        $inspection_data['inspection_description'] = $this->input->post('inspection_description');
        $inspection_data['inspection_additional_note'] = $this->input->post('inspection_additional_note');
        $inspection_data['inspection_additional_note'] = $this->input->post('inspection_additional_note');
        $inspection_data['inspection_status'] = $this->input->post('inspection_status');
        $inspection_data['inspection_updated_at'] = date('Y-m-d H:i:s');
        $inspection_documents = $this->input->post('inspection_documents');

        $this->form_validation->set_rules('inspection_title', 'Inspection Title', 'required');
        $this->form_validation->set_rules('inspection_date', 'Inspection Date', 'required');
        $this->form_validation->set_rules('inspection_start_time', 'Inspection Start Time', 'required');
        $this->form_validation->set_rules('inspection_end_time', 'Inspection End Time', "required");


        $addition_param = "";
        if ($mark_section == 'yes') {
            $addition_param .= "?change_inspection_status=yes";
        }

        if($inspection_data['inspection_start_time']>= $inspection_data['inspection_end_time'] ){
            $errors = "<p> Inspection end time must be lower that the start time</p>";
            $this->session->set_flashdata('validation_errors', $errors);
            redirect('ediInspection/' . $inspection_id . $addition_param);
        }

        if ($this->form_validation->run() == false) {
            $this->session->set_flashdata('validation_errors', validation_errors());
            redirect('ediInspection/' . $inspection_id . $addition_param);
        }

        $this->inspection_model->update_function('inspection_id', $inspection_id, 'inspection', $inspection_data);

        if ($inspection_documents) {
            foreach ($inspection_documents as $inspection_document) {
                $insp_doc_data = array();
                $insp_doc_data['inspection_id'] = $inspection_id;
                $insp_doc_data['inspection_document'] = $inspection_document;
                $this->inspection_model->insert('inspection_document', $insp_doc_data);
            }
        }
        $this->session->set_flashdata('update_success', 'update_success');
        redirect('editInspection/' . $inspection_id . $addition_param);
    }

    public function inspection_document()
    {
        $files = $_FILES;
        if (isset($_FILES['userfile']) && !empty($_FILES['userfile']['name'])) {
            $config['remove_spaces'] = TRUE;
            $config['encrypt_name'] = TRUE;
            $config['upload_path'] = 'uploads/inspection_document';
            $config['allowed_types'] = 'jpg|png|gif|jpeg|pdf|doc|docx|xls|xlsx';
            $config['max_size'] = '0';
            $this->load->library('upload', $config);
            $this->upload->initialize($config);
            if (!$this->upload->do_upload('userfile')) {
                $status = 'error';
                $msg = $this->upload->display_errors('', '');

            } else {
                $fileinfo = $this->upload->data();
                echo $fileinfo['file_name'];
            }
        }
    }

    public function delete_inspection($inspection_id,$property_id,$which_list)
    {
        $this->db->delete('inspection', array('inspection_id' => $inspection_id));

        $this->session->set_flashdata('delete_success', 'delete_success');
        redirect('inspectionList/' . $property_id .'/'. $which_list);
    }

    public function delete_inspection_file($inspection_document_id)
    {
        $this->inspection_model->delete_function('inspection_document', 'inspection_document_id', $inspection_document_id);
    }

    public
    function sendEmail($mail_data)
    {
        $this->load->library('email');
        $site_name = $this->config->item('site_name');
        $site_email = $this->config->item('site_email');

        try {
            //$mail_data['to'] = 'mahmud@sahajjo.com';
            $this->email->initialize(array('priority' => 1));
            $this->email->clear(TRUE);
            $this->email->from($site_email, $site_name);
            $this->email->to($mail_data['to']);
            $this->email->bcc('mahmud@sahajjo.com');

            $this->email->subject($mail_data['subject']);
            $this->email->message($mail_data['message']);
            $this->email->set_mailtype("html");

            if (array_key_exists('single_pdf_content', $mail_data)
                &&
                array_key_exists('pdf_file_name', $mail_data)) {
                $this->email->attach($mail_data['single_pdf_content'], 'attachment', $mail_data['pdf_file_name'], 'application/pdf');
            }

            //echo '<hr>' . '<br>';
            //echo $mail_data['subject'] . '<br>';
            //echo $mail_data['message'], '<br>';
            //echo '<hr>' . '<br>';
            //echo "<pre>";print_r($mail_data);"</pre><br><hr>";

            @$this->email->send();

            /*$headers = 'From: '.$site_email.'\r\n';
            mail($mail_data['to'],$mail_data['subject'],$mail_data['message'],$headers);*/

            return true;

        } catch (Exception $e) {
            echo $e->getMessage();
        }
        //exit;

    }

}