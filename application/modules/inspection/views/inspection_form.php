<?php $this->load->view('front/headlink'); ?>
<?php $this->load->view('front/top_menu'); ?>
<div class="container">

    <style>
        .talk-bubble {
            display: inline-block;
            position: relative;
            width: 150px;
            height: auto;
            background-color: lightyellow;
        }

        /* Right triangle, left side slightly down */
        .tri-right.border.left-in:before {
            content: ' ';
            position: absolute;
            width: 0;
            height: 0;
            left: -40px;
            right: auto;
            top: 24px;
            bottom: auto;
            border: 20px solid;
            border-color: #666 #666 transparent transparent;
        }
        .tri-right.left-in:after{
            content: ' ';
            position: absolute;
            width: 0;
            height: 0;
            left: -20px;
            right: auto;
            top: 24px;
            bottom: auto;
            border: 12px solid;
            border-color: lightyellow lightyellow transparent transparent;
        }

        /* talk bubble contents */
        .talktext{
            padding: 1em;
            line-height: 1.5em;
            text-justify: inter-word;
        }
        .talktext p{
            /* remove webkit p margins */
            -webkit-margin-before: 0em;
            -webkit-margin-after: 0em;
        }

    </style>

    <?php $this->load->view('front/head_nav'); ?>
    <div class="row">
        <?php
        $display_inspection_mark_section = false;
        if (isset($_REQUEST['change_inspection_status'])) {
            if ($_REQUEST['change_inspection_status'] == 'yes') {
                $display_inspection_mark_section = true;
            }
        } ?>
        <div class="ss_container">
		<div class="text-center">
            <h2 class="extra_heading"><?= ucwords(str_replace("_", "", $which_form)) ?> Inspection </h2>
           
                <form class="container form-horizontal" onsubmit="return chk_document_upload()" action="<?= $form_action ?>" method="post"
                      enctype="multipart/form-data">
                       <div class="ss_bound_content">
                    <div class="row extra_padding">
                        <div class="col-md-3">
                        </div>
                        <div class="col-md-6">
                            <?php if ($this->session->flashdata('validation_errors')) { ?>
                                <div class="panel panel-danger">
                                    <div class="panel-heading">Error!</div>
                                    <div class="panel-body"><?php echo $this->session->flashdata('validation_errors'); ?></div>
                                </div>
                            <?php } ?>
                            <?php if ($this->session->flashdata('add_success')) { ?>
                                <div class="panel panel-primary">
                                    <div class="panel-heading">Success!</div>
                                    <div class="panel-body">
                                        Successfully added inspection
                                        &nbsp;
                                        <a class="btn btn-primary btn-sm"
                                           href="inspectionList/<?= $property_id ?>/upcoming">View upcoming
                                            inspections</a>
                                    </div>
                                </div>
                            <?php } ?>
                            <?php if ($this->session->flashdata('update_success')) { ?>
                                <div class="panel panel-primary">
                                    <div class="panel-heading">Success!</div>
                                    <div class="panel-body">
                                        Successfully updated inspection
                                        <br><br>
                                        <div class="row" style="margin: 5px">
                                            <a class="btn-sm"
                                               href="inspectionList/<?= $property_id ?>/upcoming">View upcoming
                                                inspections</a>
                                        </div>
                                        <div class="row" style="margin: 5px">
                                            <a class="btn-sm"
                                               href="inspectionList/<?= $property_id ?>/all">View all
                                                inspections</a>
                                        </div>
                                        <div class="row" style="margin: 5px">
                                            <a class="btn-sm"
                                               href="inspectionList/<?= $property_id ?>/completed">View completed
                                                inspections</a>
                                        </div>
                                    </div>
                                </div>
                            <?php } ?>
                        </div>
                        <div class="col-md-3">
                        </div>
                    </div>

                    <?php if ($inspection_documents) { ?>
                        <div class="row ">
                            <div class="col-md-12 form-group">
                            <label class="col-sm-4 control-label">
                               Files 
                              
                            </label>
                            <div class="col-md-8">
                                <ul class="list-group">
                                    <?php foreach ($inspection_documents as $inspection_document) { ?>
                                        <li class="list-group-item">
                                            <span inspection-document-id="<?= $inspection_document['inspection_document_id'] ?>"
                                                  style="cursor: pointer" class="badge delete_file">X</span>
                                            <a download="download" target="_blank"
                                               href="uploads/inspection_document/<?= $inspection_document['inspection_document'] ?>"><?= $inspection_document['inspection_document'] ?></a>
                                        </li>
                                    <?php } ?>
                                </ul>
                            </div>
                            <div class="col-md-3">
                            </div>
                            </div>
                        </div>
                    <?php } ?>
                    <div class="row ">
                        <div class="form-group col-md-12">
                            <label class="col-sm-4 control-label" for="">Upload Files
                                <span>
                                    <br>
                                    <small>
                                        (You may want to upload a copy of the routine inspection letter sent here
                                        <br>
                                        or
                                        <br>upload a copy of the routine conducted here)
                                    </small>
                                </span>
                            </label>
                            <div class="col-md-8">
                                <div>*Image is optional</div>
                                <div class="form-group inputDnD">
                                    <div class="dropzone" style="margin: 30px">
                         <span style="display: none" class="my-dz-message pull-right">
                              <h3>Click Here to upload another file</h3>
                          </span>
                                        <div class="dz-message">
                                            <h3> Click Here to upload your files</h3>
                                        </div>

                                    </div>
                                    <div class="previews" id="preview"></div>
                                </div>
                                <div>* Max 20 MB per file</div>
                            </div>
                            
                        </div>
                    </div>

                    <div class="row extra_padding">
                        <div class="col-md-12">
						
                        <label class="col-sm-4 control-label">
                            Title
                        </label>
                        <div class="col-md-8">
                            <input type="text" name="inspection_title" id="inspection_title" class="form-control"
                                   value="<?= ($inspection) ? $inspection['inspection_title'] : ''; ?>"
                            >
                        </div>
                    </div>
                    </div>
                    <div class="row extra_padding">
                        <div class="col-md-12">
						<label class="col-sm-4 control-label">
                            Description
                        </label>
                        <div class="col-md-8">
                            <textarea name="inspection_description" id="inspection_description" rows="6"
                                      class="form-control"><?= ($inspection) ? $inspection['inspection_description'] : ''; ?></textarea>
                        </div>
                    </div>
                    </div>
                    <div class="row extra_padding">
                        <div class="col-md-12">
						<label class="col-sm-4 control-label">
                            Inspection Date
                        </label>
                        <div class="col-md-8">
                            <input id="inspection_date" type="text" name="inspection_date" readonly
                                   class="form-control"
                                   value="<?= ($inspection) ? date('d/m/Y', strtotime($inspection['inspection_date'])) : ''; ?>"
                            >
                        </div>
                        <div class="col-md-3">
                        </div>
                    </div>
                    </div>
                    <div class="row extra_padding" >
                        <div class="col-md-12">
						<label class="col-sm-4 control-label">
                             Inspection Time
                        </label>
                        <div class="col-md-8" >
                            <p id="timeOnly" class="input-group">
                                <input id="inspection_start_time" type="text" name="inspection_start_time"
                                       class="form-control form-inline time"
                                       value="<?= ($inspection) ? date('g:i:a', strtotime($inspection['inspection_start_time'])) : ''; ?>"
                                >
                                &nbsp;to&nbsp;
                                <input id="inspection_end_time" type="text" name="inspection_end_time"
                                        class="form-control form-inline time"
                                        value="<?= ($inspection) ? date('g:i:a', strtotime($inspection['inspection_end_time'])) : ''; ?>"
                                >
                            </p>

                        </div>
                    </div>
                  </div>
<div style="clear: both;"></div>
                    <div id="inspection_status_div" class="row"
                         style="display:<?= $display_inspection_mark_section ? 'block' : 'none' ?>;margin-top: 5px">
                         <div class="col-md-12">
						 <label class="col-sm-4 control-label">
                             Inspection Status
                        </label>
                        <div class="col-sm-8">
                            <select name="inspection_status" id="" style="float: left;margin-right: 5px;">
                                <option value="0" <?= ($inspection) ? $inspection['inspection_status'] == 0 ? 'selected' : '' : ''; ?> >
                                    Incomplete
                                </option>
                                <option value="1" <?= ($inspection) ? $inspection['inspection_status'] == 1 ? 'selected' : '' : ''; ?> >
                                    Complete
                                </option>
                            </select>
                            <div class="talk-bubble tri-right left-in">
                                <div class="talktext">
                                    <p>You can mark the routine inspection as completed by changing the inspection status here</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    </div>
                    <div class="row extra_padding">
                        <div class="col-md-12">
						<label class="col-sm-4 control-label">
                              Notes <br>
                            <?php if ($display_inspection_mark_section) {?>
                            <small>
                                (Here you may want to note anything you were not satisfied with at the inspection and any maintenance you have reported you need to action)
                            </small>
                            <?php } ?>
                            <?php if (!$display_inspection_mark_section) {?>
                                <small>
                                    (Here you may want to note what you need to look out for at the inspection or some maintenance reported previously you may want to investigate)
                                </small>
                            <?php } ?>
                            <br>
                        </label>
                       
                        <div class="col-md-8">
                            <textarea name="inspection_additional_note" id="" rows="6"
                                      class="form-control"><?= ($inspection) ? $inspection['inspection_additional_note'] : ''; ?></textarea>
                        </div>
                        
                    </div>
                    </div>


                    <div class="col-md-4"></div>
                    <div class="col-md-8">
                        <div class="modal-footer clear">
                            <br><br>
                            <input type="hidden" name="mark_section"
                                   value="<?= $display_inspection_mark_section ? 'yes' : 'no'; ?>">
                            <input type="hidden" name="inspection_id"
                                   value="<?= ($inspection) ? $inspection['inspection_id'] : ''; ?>">
                            <input type="hidden" name="property_id" value="<?= $property_id; ?>">
                            <input type="hidden" name="lease_id" value="<?= $lease_id; ?>">
                            <input type="submit" class="btn btn-primary" value="Save">
                            <br><br>
                        </div>
                    </div>
                    <div class="col-md-3"></div>
</div>
                </form>

            </div>
            </div>
        </div>
    </div>
</div>
<script src="//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
<?php $this->load->view('front/footerlink'); ?>
<script type="text/javascript">
    Dropzone.autoDiscover = false;
    var foto_upload = new Dropzone(".dropzone", {
        url: "<?php echo base_url('inspectionDocument') ?>",
        maxFilesize: 20,
        method: "post",
        acceptedFiles: ".jpg,.jpeg,.png,.gif,.pdf,.doc,.docx,.xls,.xlsx",
        paramName: "userfile",
        dictInvalidFileType: "This File Type Not Supported",
        addRemoveLinks: true,
        init: function () {
            var count = 0;
            thisDropzone = this;
            this.on("success", function (file, json) {
                var obj = json;
                $('.previews').append(
                    "<input type='hidden' name='inspection_documents[]' value='" + obj + "'>\n\
            <input type='hidden' name='file_width[]' value='" + file.width + "'>\n\
            <input type='hidden' name='file_height[]' value='" + file.height + "'>"
                );
                $(".my-dz-message").show();
            });
        }
    });

    /*$("#submit_btn").on("click",function(){
        e.preventDefault();
    });*/

</script>

<script type="text/javascript">
    document.addEventListener("DOMContentLoaded", function (event) {
        $('#inspection_date').datepicker({
            dateFormat: "dd/mm/yy"
        });

        $('#timeOnly .time').timepicker({
            'step': 15,
            'showDuration': true,
            'timeFormat': 'g:ia'
        });

        $('#timeOnly').datepair();
    });
</script>

<style>
    .ui-datepicker{
        z-index:99999 !important;
    }
</style>

<script type="text/javascript">

    function chk_document_upload() {
        var err = 0;
        var ret = false;
        var inspection_title = $('#inspection_title');
        var inspection_date = $('#inspection_date');
        var inspection_start_time = $('#inspection_start_time');
        var inspection_end_time = $('#inspection_end_time');
        if (inspection_title.val() == '') {
            inspection_title.closest('div').addClass("has-error");
            err = err + 1;
        }
        else {
            inspection_title.closest('div').removeClass("has-error");
        }

        if (inspection_date.val() == '') {
            inspection_date.closest('div').addClass("has-error");
            err = err + 1;
        }
        else {
            inspection_date.closest('div').removeClass("has-error");
        }

        if (inspection_start_time.val() == '') {
            inspection_start_time.closest('div').addClass("has-error");
            err = err + 1;
        }
        else {
            inspection_start_time.closest('div').removeClass("has-error");
        }

        if (inspection_end_time.val() == '') {
            inspection_end_time.closest('div').addClass("has-error");
            err = err + 1;
        }
        else {
            inspection_end_time.closest('div').removeClass("has-error");
        }

        var count = foto_upload.files;

        if (err == 0) {
            ret = true;
        }
        return ret;
    }
</script>

<script>
    $(function () {
        $('.delete_file').on('click', function (e) {
            var inspection_document_id = $(this).attr('inspection-document-id');
            console.log(inspection_document_id);
            if (confirm("Are you sure to delete this file?")) {
                deleteFile(inspection_document_id, $(this).closest('li'))
            }
        });
    })

    function deleteFile(inspection_document_id, item) {
        var url = "deleteInspectionFile/" + inspection_document_id;
        var jqxhr = $.get(url, function () {
            alert("success");
        })
            .done(function () {
                item.remove();
            })
            .fail(function () {
                alert("Error! Deletion Unsuccessful");
            })
            .always(function () {
                console.log("finished");
            });

    }

</script>
<?php if ($display_inspection_mark_section) { ?>
    <script>
        $(function () {

        });
        <?php if(!$this->session->flashdata('update_success')) { ?>
        $('html, body').animate({
            scrollTop: $("#inspection_status_div").offset().top
        }, 2000);
        <?php } ?>

    </script>

<?php } ?>


</body>
</html>