<?php $this->load->view('front/headlink'); ?>
<?php $this->load->view('front/top_menu'); ?>
<div class="container">
    <?php $this->load->view('front/head_nav'); ?>
    <div class="row">
        <div class="ss_container">
            <h3 class="extra_heading"> <?= ucfirst($which_inspection_list) ?> Inspections
                <small>for the property at <?= $this->utility_model->getFullPropertyAddress($property_id)?></small>
            </h3>
            <div class="row">
                <?php if(!$is_archived) { ?>
                <a href="addInspection/<?= $property_id; ?>" class="pull-right btn btn-primary">
                    <span class="glyphicon glyphicon-plus" aria-hidden="true"></span>
                    Add new Inspection
                </a>
                <?php } ?>
            </div>
            <?php if ($this->session->flashdata('delete_success')) { ?>
                <div class="panel panel-primary">
                    <div class="panel-heading">Success!</div>
                    <div class="panel-body">
                        Successfully deleted
                    </div>
                </div>
            <?php } ?>
                <!-- Tab panes -->
                <div class="tab-content">
                    <div class="row-fluid document-list-header top-table-name">
                        <!--<div class="col-md-2">Label</div>-->
                        <div class="col-md-2">Inspection Title</div>
                        <div class="col-md-2">Inspection Description</div>
                        <div class="col-md-2">Inspection Date</div>
                        <div class="col-md-2">Status</div>
                        <div class="col-md-2">Date Created</div>
                        <div class="col-md-2">Action</div>
                    </div>
                    <?php if (count($inspections) == 0) { ?>
                        <h4 style="font-style: italic;color:grey">There are currently no routine inspections scheduled</h4>
                    <?php } ?>
                    <div role="tabpanel" class="tab-pane active" id="dall">
                        <form id="all_submit_form" action="lease/doc_download" method="post">

                            <?php if (count($inspections) > 0) { ?>
                                <?php foreach ($inspections as $inspection) { ?>
                                    <div class="row-fluid documents-list_area">
                                        <a class="rep_href">
                                            <div class="col-md-2">
                                                <h4 class="table-name">Document Title</h4>
                                                <h5 id="document_title"><?= $inspection['inspection_title'] ?></h5>
                                            </div>
                                            <div class="col-md-2">
                                                <h4 class="table-name">Document Description</h4>
                                                <h5 id="document_title"><?= $inspection['inspection_description'] ?></h5>
                                            </div>
                                            <div class="col-md-2 uploaded-date">
                                                <span class="glyphicon glyphicon-calendar"></span>&nbsp;<?= date("jS F, Y", strtotime($inspection['inspection_date'])); ?>
                                                <br>
                                                <span class="glyphicon glyphicon-time"></span>&nbsp;<?= date("g:i a", strtotime($inspection['inspection_start_time'])); ?> to <?= date("g:i a", strtotime($inspection['inspection_end_time'])); ?>
                                            </div>
                                            <?php
                                            $status_html = '';
                                            $status_html = $inspection['inspection_status'] == 1 ? "<span class='label label-success'>Inspected</span>" : "<span class='label label-danger'>Not inspected</span>"
                                            ?>
                                            <div class="col-md-2 uploaded-date"><?= $status_html ?></div>
                                            <div class="col-md-2 uploaded-date"><?= date("jS F, Y", strtotime($inspection['inspection_created_at'])); ?></div>
                                            <div class="col-md-2 ">
                                                <?php if(!$is_archived) { ?>
                                                <a class="btn btn-sm btn-default btn-block"
                                                   href="editInspection/<?= $inspection['inspection_id'] ?>">Edit</a>
                                                <a class="btn btn-sm btn-danger btn-block deleteBtn"
                                                   href="deleteInspection/<?= $inspection['inspection_id'] ?>/<?=$property_id?>/<?= $which_inspection_list?>">Delete</a>
                                                <a class="btn btn-sm btn-default btn-block"
                                                   href="editInspection/<?= $inspection['inspection_id'] ?>?change_inspection_status=yes">
                                                    <?= $inspection['inspection_status'] == 1 ? "Mark Uninspected" : "Mark Inspected" ?>
                                                </a>
                                                <?php } ?>
                                            </div>
                                        </a>
                                    </div>
                                <?php } ?>
                            <?php } ?>
                            <div id="get_val_here"></div>
                        </form>
                        <div style="margin-top: 50px">
                            <a class="btn btn-light pull-right" href="Dashboard/<?= $property_id ?>">Back To Dashboard</a>
                        </div>
                    </div>



                </div>

        </div>
    </div>
    <script>
        $('.deleteBtn').on('click', function (e) {
            e.preventDefault();

            var href = $(this).attr("href");

            console.log(href);

            swal({
                    title: "Are you sure to delete?",
                    text: "This inspection will be permanently deleted",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: "#DD6B55",
                    confirmButtonText: "Yes",
                    cancelButtonText: "No",
                    closeOnConfirm: true,
                    closeOnCancel: true
                },
                function (isConfirm) {
                    if (isConfirm) {
                        window.location.href = href;
                    }
                });


        })
    </script>
</div>
<script src="//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
<?php $this->load->view('front/footerlink'); ?>
</body>
</html>