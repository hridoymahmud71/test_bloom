<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Inspection_model extends CI_Model
{

    public function __construct()
    {
        parent::__construct();
    }

    public function getActiveLease($property_id)
    {
        $this->db->select('*');
        $this->db->from('lease');
        $this->db->where('property_id', $property_id);
        $this->db->where('lease_current_status', 1);

        $query = $this->db->get();
        $row = $query->row_array();
        return $row;
    }

    public function getLease($lease_id)
    {
        $this->db->select('*');
        $this->db->from('lease');
        $this->db->where('lease_id', $lease_id);

        $query = $this->db->get();
        $row = $query->row_array();
        return $row;
    }

    public function getInspectionList($property_id, $inspection_status,$limit=false)
    {
        $this->db->select('*');
        $this->db->from('inspection');
        $this->db->where('property_id', $property_id);

        if ($inspection_status !== '') {
            $this->db->where('inspection_status', $inspection_status);
        }

        $this->db->order_by('inspection_date','asc');

        $query = $this->db->get();
        $result = $query->result_array();
        return $result;
    }

    public function getNextInspection($property_id, $inspection_status)
    {
        $this->db->select('*');
        $this->db->from('inspection');
        $this->db->where('property_id', $property_id);

        if ($inspection_status !== '') {
            $this->db->where('inspection_status', $inspection_status);
        }

        $this->db->order_by('inspection_date','asc');

        $query = $this->db->get();
        $result = $query->row_array();
        return $result;
    }

    public function getInspection($inspection_id)
    {
        $this->db->select('*');
        $this->db->from('inspection');
        $this->db->where('inspection_id', $inspection_id);

        $query = $this->db->get();
        $row = $query->row_array();
        return $row;
    }

    public function getInspectionDocuments($inspection_id)
    {
        $this->db->select('*');
        $this->db->from('inspection_document');
        $this->db->where('inspection_id', $inspection_id);

        $query = $this->db->get();
        $result = $query->result_array();
        return $result;
    }

    public function insert($table_name,$data)
    {
        $this->db->insert($table_name, $data);
        return TRUE;
    }

    public function insert_ret($tablename, $tabledata)
    {
        $this->db->insert($tablename, $tabledata);
        return $this->db->insert_id();
    }

    public function delete_function($tableName, $columnName, $columnVal)
    {
        $this->db->where($columnName, $columnVal);
        $this->db->delete($tableName);
    }

    public function update_function($columnName, $columnVal, $tableName, $data)
    {
        $this->db->where($columnName, $columnVal);
        $this->db->update($tableName, $data);
    }

}
