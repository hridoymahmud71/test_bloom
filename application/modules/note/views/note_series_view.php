<?php $this->load->view('front/headlink'); ?>

<div class="container">

    <style>
        body {
            background: white !important;
        }


        .single-item-cont-row {
            padding: 15px;
            background-color: #f9f9f9;
            margin-bottom:10px ;
        }

    </style>


    <div class="row" style="padding-top: 10px">
        <div class="text-center">
            <img class="" src="assets/img/logo.png">
            <h4>
                Notes(s) for tenancy
            </h4>
        </div>
        <h3>
           <?= $this->utility_model->getFullPropertyAddress($property_id); ?>
        </h3>
    </div>


    <div class="row">
        <div class="ss_container" style="margin-top: 0">
            <div class="ss_bound_content" style="margin-top: 0;padding-top: 0">
                <!-- Nav tabs -->
                <div class="ss_expensesinvoices">
                    <div class="ss_expensesinvoices_top">
                        <div class="container">
                            <?php if (!empty($notes)) { ?>
                                <?php foreach ($notes as $note) { ?>
                                    <div class="row single-item-cont-row">
                                        <div class="row">
                                            <div class="col-md-12">
                                                <h3><?= $note['note_title']; ?></h3>
                                                <div>Created
                                                    at:<?= date("d/m/Y H:i:s A", strtotime($note['note_created_at'])); ?></div>
                                                <div>Updated
                                                    at:<?= date("d/m/Y H:i:s A", strtotime($note['note_updated_at'])); ?></div>
                                            </div>
                                        </div>
                                        <br>
                                        <hr>
                                        <br>
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div style="font-size: larger"><?= $note['note_description']; ?></div>
                                            </div>
                                        </div>
                                        <br>
                                        <hr>
                                        <br>
                                        <div class="row">
                                            <div class="col-md-12">
                                                <?php if ($note['note_documents']) { ?>
                                                    <h4>Attachment(s):</h4>
                                                    <?php foreach ($note['note_documents'] as $note_document) { ?>
                                                        <?php $ext = '';
                                                        $ext = strtolower(pathinfo($note_document['note_document'], PATHINFO_EXTENSION));
                                                        $image_file_types = ['jpg', 'jpeg', 'png', 'gif'];
                                                        ?>

                                                        <?php /*if (in_array($ext, $image_file_types)) { */?><!--
                                                            <img style="height:100px;width:100px;"
                                                                 src="uploads/note_document/<?/*= $note_document['note_document']; */?>">
                                                        --><?php /*} */?>

                                                        <?php if (!in_array($ext, $image_file_types) || 1) { ?>
                                                            <a download
                                                               href="uploads/note_document/<?= $note_document['note_document']; ?>"><?= $note_document['note_document']; ?></a>
                                                        <?php } ?>
                                                    <?php } ?>
                                                <?php } ?>
                                            </div>
                                        </div>
                                    </div>
                                <?php } ?>
                            <?php } ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


<?php $this->load->view('front/footerlink'); ?>
