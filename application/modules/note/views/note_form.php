<?php $this->load->view('front/headlink'); ?>
<?php $this->load->view('front/top_menu'); ?>
<div class="container">

    <style>
        .talk-bubble {
            display: inline-block;
            position: relative;
            width: 150px;
            height: auto;
            background-color: lightyellow;
        }

        /* Right triangle, left side slightly down */
        .tri-right.border.left-in:before {
            content: ' ';
            position: absolute;
            width: 0;
            height: 0;
            left: -40px;
            right: auto;
            top: 24px;
            bottom: auto;
            border: 20px solid;
            border-color: #666 #666 transparent transparent;
        }

        .tri-right.left-in:after {
            content: ' ';
            position: absolute;
            width: 0;
            height: 0;
            left: -20px;
            right: auto;
            top: 24px;
            bottom: auto;
            border: 12px solid;
            border-color: lightyellow lightyellow transparent transparent;
        }

        /* talk bubble contents */
        .talktext {
            padding: 1em;
            line-height: 1.5em;
            text-align: justify;
            text-justify: inter-word;
        }

        .talktext p {
            /* remove webkit p margins */
            -webkit-margin-before: 0em;
            -webkit-margin-after: 0em;
        }

    </style>

    <?php $this->load->view('front/head_nav'); ?>
    <div class="row">

        <div class="ss_container">
            <div class=" well text-center"><h2 class="extra_heading"><?= ucwords(str_replace("_", "", $which_form)) ?> Note </h2>
            <div class="ss_bound_content">
                <form class="container form-horizontal" onsubmit="return chk_document_upload()" action="<?= $form_action ?>" method="post"
                      enctype="multipart/form-data">
                    <div class="row extra_padding">
                        <div class="col-sm-4">
                        </div>
                        <div class="col-sm-8">
                            <?php if ($this->session->flashdata('validation_errors')) { ?>
                                <div class="panel panel-danger">
                                    <div class="panel-heading">Error!</div>
                                    <div class="panel-body"><?php echo $this->session->flashdata('validation_errors'); ?></div>
                                </div>
                            <?php } ?>
                            <?php if ($this->session->flashdata('add_success')) { ?>
                                <div class="panel panel-primary">
                                    <div class="panel-heading">Success!</div>
                                    <div class="panel-body">
                                        Successfully added note
                                        &nbsp;
                                        <a class="btn btn-primary btn-sm"
                                           href="noteList/<?= $property_id ?>">View Notes
                                        </a>
                                    </div>
                                </div>
                            <?php } ?>
                            <?php if ($this->session->flashdata('update_success')) { ?>
                                <div class="panel panel-primary">
                                    <div class="panel-heading">Success!</div>
                                    <div class="panel-body">
                                        Successfully updated note
                                        &nbsp;
                                        &nbsp;
                                        <a class="btn btn-primary btn-sm"
                                           href="noteList/<?= $property_id ?>">View Notes
                                        </a>
                                    </div>
                                </div>
                            <?php } ?>
                        </div>
                        <div class="col-md-3">
                        </div>
                    </div>

                    <?php if ($note_documents) { ?>
                        <div class="row extra_padding">
                            <div class="col-md-4">
                                Files
                                <br>
                            </div>
                            <div class="col-md-8">
                                <ul class="list-group">
                                    <?php foreach ($note_documents as $note_document) { ?>
                                        <li class="list-group-item">
                                            <span note-document-id="<?= $note_document['note_document_id'] ?>"
                                                  style="cursor: pointer" class="badge delete_file">X</span>
                                            <a download="download" download
                                               href="uploads/note_document/<?= $note_document['note_document'] ?>"><?= $note_document['note_document'] ?></a>
                                        </li>
                                    <?php } ?>
                                </ul>
                            </div>
                            <div class="col-md-3">
                            </div>
                        </div>
                    <?php } ?>
                    <div class="row ">
                        <div class="form-group col-md-12">
                            <label class="col-sm-4 control-label" for="">Upload File(s) </label>
                            <div class="col-sm-8">
                                <div>*Image is optional</div>
                                <div class="form-group inputDnD">
                                    <div class="dropzone">
                         <span style="display: none" class="my-dz-message pull-right">
                              <h3>Click Here to upload another file</h3>
                          </span>
                                        <div class="dz-message">
                                            <h3> Click Here to upload your files</h3>
                                        </div>

                                    </div>
                                    <div class="previews" id="preview"></div>
                                </div>
                                <div>* Max 20 MB per file</div>
                            </div>
                            <div class="col-md-3">
                            </div>
                        </div>
                    </div>

                    <div class="row extra_padding">
                        <label class="col-sm-4 control-label">
                            Title
                        </label>
                        <div class="col-sm-8">
                            <input type="text" name="note_title" id="note_title" class="form-control"
                                   value="<?= ($note) ? $note['note_title'] : ''; ?>"
                            >
                        </div>
                    </div>
                    <div class="row extra_padding">
                        <label class="col-sm-4 control-label">
                            Description

                        </label>
                        <div class="col-sm-8">
                            <textarea name="note_description" id="note_description" rows="6"
                                      class="form-control"><?= ($note) ? $note['note_description'] : ''; ?></textarea>
                        </div>
                    </div>


                    <div class="col-sm-4"></div>
                    <div class="col-sm-8">
                        <div class="modal-footer clear">
                            <br><br>
                            <input type="hidden" name="note_id"
                                   value="<?= ($note) ? $note['note_id'] : ''; ?>">
                            <input type="hidden" name="property_id" value="<?= $property_id; ?>">
                            <input type="hidden" name="lease_id" value="<?= $lease_id; ?>">
                            <input type="submit" class="btn btn-primary" value="Save">
                            <br><br>
                        </div>
                    </div>
                    <div style="clear:both"></div>
                </form>
            </div>
		</div>
        </div>
    </div>
</div>
<script src="//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
<?php $this->load->view('front/footerlink'); ?>
<script type="text/javascript">
    Dropzone.autoDiscover = false;
    var foto_upload = new Dropzone(".dropzone", {
        url: "<?php echo base_url('noteDocument') ?>",
        maxFilesize: 20,
        method: "post",
        acceptedFiles: ".jpg,.jpeg,.png,.gif,.pdf,.doc,.docx,.xls,.xlsx",
        paramName: "userfile",
        dictInvalidFileType: "This File Type Not Supported",
        addRemoveLinks: true,
        init: function () {
            var count = 0;
            thisDropzone = this;
            this.on("success", function (file, json) {
                var obj = json;
                $('.previews').append(
                    "<input type='hidden' name='note_documents[]' value='" + obj + "'>\n\
            <input type='hidden' name='file_width[]' value='" + file.width + "'>\n\
            <input type='hidden' name='file_height[]' value='" + file.height + "'>"
                );
                $(".my-dz-message").show();
            });
        }
    });

    /*$("#submit_btn").on("click",function(){
        e.preventDefault();
    });*/

</script>


<script type="text/javascript">

    function chk_document_upload() {
        var err = 0;
        var ret = false;
        var note_title = $('#note_title');
        if (note_title.val() == '') {
            note_title.closest('div').addClass("has-error");
            err = err + 1;
        }
        else {
            note_title.closest('div').removeClass("has-error");
        }

        var count = foto_upload.files;

        if (err == 0) {
            ret = true;
        }
        return ret;
    }
</script>

<script>
    $(function () {
        $('.delete_file').on('click', function (e) {
            var note_document_id = $(this).attr('note-document-id');
            console.log(note_document_id);
            if (confirm("Are you sure to delete this file?")) {
                deleteFile(note_document_id, $(this).closest('li'))
            }
        });
    })

    function deleteFile(note_document_id, item) {
        var url = "deleteNoteFile/" + note_document_id;
        var jqxhr = $.get(url, function () {
            alert("success");
        })
            .done(function () {
                item.remove();
            })
            .fail(function () {
                alert("Error! Deletion Unsuccessful");
            })
            .always(function () {
                console.log("finished");
            });

    }

</script>


</body>
</html>