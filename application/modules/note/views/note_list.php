<?php $this->load->view('front/headlink'); ?>
<?php $this->load->view('front/top_menu'); ?>
<div class="container">
    <?php $this->load->view('front/head_nav'); ?>
    <div class="row">
        <div class="ss_container">
            <h3 class="extra_heading"> Notes for the Tenancy <small>for the property at <?= $this->utility_model->getFullPropertyAddress($property_id)?></small>
            </h3>
            <div>
                <?php if (!$is_archived) { ?>
                <a href="addNote/<?= $property_id; ?>" class="pull-right btn btn-primary"
                   style="margin-left: 10px;">
                    <span class="glyphicon glyphicon-plus" aria-hidden="true"></span>
                    Add new Note
                </a>
                <?php } ?>
            </div>
            <?php if ($lease) { ?>
                <div>
                    <a target="_blank" href="noteSeriesView/<?= $property_id; ?>?pdf=ok"
                       class="pull-right btn btn-primary"
                       style="  margin-left: 10px;">
                        <span class="glyphicon glyphicon-pdf" aria-hidden="true"></span>
                        Download notes as PDF
                    </a>
                </div>
            <?php } ?>

            <?php if ($this->session->flashdata('delete_success')) { ?>
                <div class="panel panel-primary" style="margin-top: 100px">
                    <div class="panel-heading">Success!</div>
                    <div class="panel-body">
                        Successfully deleted
                    </div>
                </div>
            <?php } ?>
            <div class="ss_bound_content">
                <?php if (count($notes) == 0) { ?>
                    <h4 style="font-style: italic;color:grey">There are currently no notes for this tenancy.</h4>
                <?php } ?>


                <!-- Tab panes -->
                <div class="tab-content">
                    <div class="row-fluid document-list-header top-table-name">
                        <!--<div class="col-md-2">Label</div>-->
                        <div class="col-md-2">Note Title</div>
                        <div class="col-md-4">Note Description</div>
                        <div class="col-md-2">Date Created</div>
                        <div class="col-md-2">Date Updated</div>
                        <div class="col-md-2">Action</div>
                    </div>

                    <div role="tabpanel" class="tab-pane active" id="dall">
                        <form id="all_submit_form" action="lease/doc_download" method="post">
                            <?php if (count($notes) > 0) { ?>
                                <?php foreach ($notes as $note) { ?>
                                    <div class="row-fluid documents-list_area">
                                        <a class="rep_href">
                                            <div class="col-md-2">
                                                <h4 class="table-name">Document Title</h4>
                                                <h5 id="document_title"><?= $note['note_title'] ?></h5>
                                            </div>
                                            <div class="col-md-4">
                                                <h4 class="table-name">Document Description</h4>
                                                <h5 id="document_title"><?= $note['note_description'] ?></h5>
                                            </div>
                                            <div class="col-md-2 uploaded-date"><?= date("jS F, Y", strtotime($note['note_created_at'])); ?></div>
                                            <div class="col-md-2 uploaded-date"><?= date("jS F, Y", strtotime($note['note_updated_at'])); ?></div>
                                            <div class="col-md-2 ">
                                                <?php if (!$is_archived) { ?>
                                                <a class="btn btn-sm btn-default btn-block"
                                                   href="editNote/<?= $note['note_id'] ?>">Edit
                                                </a>
                                                <?php } ?>
                                                <a target="_blank" class="btn btn-sm btn-default btn-block"
                                                   href="noteView/<?= $note['note_id'] ?>?pdf=ok">PDF
                                                </a>
                                                <?php if (!$is_archived) { ?>
                                                <a class="btn btn-sm btn-danger btn-block deleteBtn"
                                                   href="deleteNote/<?= $note['note_id'] ?>/<?= $property_id ?>">Delete</a>
                                                <?php } ?>
                                            </div>
                                        </a>
                                    </div>
                                <?php } ?>
                            <?php } ?>
                            <div id="get_val_here"></div>
                        </form>
                    </div>
                    <div style="margin-top: 50px">
                        <a class="btn btn-light pull-right" href="Dashboard/<?= $property_id ?>">Back To Dashboard</a>
                    </div>


                </div>
            </div>
        </div>
    </div>
</div>

<script>
    $('.deleteBtn').on('click', function (e) {
        e.preventDefault();

        var href = $(this).attr("href");

        console.log(href);

        swal({
                title: "Are you sure to delete?",
                text: "This inspection will be permanently deleted",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "Yes",
                cancelButtonText: "No",
                closeOnConfirm: true,
                closeOnCancel: true
            },
            function (isConfirm) {
                if (isConfirm) {
                    window.location.href = href;
                }
            });


    })
</script>

<script src="//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
<?php $this->load->view('front/footerlink'); ?>
</body>
</html>