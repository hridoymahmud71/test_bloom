<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Note extends MX_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model('note_model');
        $this->load->model('utility/utility_model');

        $this->utility_model->check_auth();
    }


    public function note_list($property_id)
    {
        $data['property_id'] = $property_id;
        $data['notes'] = null;
        $data['lease'] = null;

        $lease = $this->note_model->getActiveLease($property_id);
        //-------------------------------------------------------------------------
        $lease_id = !empty($lease) ? $lease['lease_id'] : 0;
        $is_archived = $this->utility_model->isArchived();
        $archived_string = $this->utility_model->getArchivedString();
        if ($is_archived) {
            $lease_id = $this->utility_model->getArchivedLeaseId();
            $lease = $this->utility_model->getLease($lease_id);
        }
        $data['is_archived'] = $is_archived;
        $data['archived_string'] = $archived_string;
        //-------------------------------------------------------------------------
        $data['lease'] = $lease;
        $notes = $this->note_model->getNoteList($property_id);
        $data['notes'] = $notes;


        $this->load->view('note_list', $data);
    }

    public function add_note($property_id)
    {
        $data['which_form'] = 'add';
        $data['property_id'] = $property_id;
        $data['form_action'] = 'insertNote';

        $lease_id = 0;
        $active_lease = $this->note_model->getActiveLease($property_id);

        $notes = null;
        if ($active_lease) {
            $lease_id = $active_lease['lease_id'];
        }
        $data['lease_id'] = $lease_id;
        $data['note'] = null;
        $data['note_documents'] = null;

        $this->load->view('note_form', $data);
    }

    public function delete_note($note_id, $property_id)
    {
        $this->db->delete('note', array('note_id' => $note_id));
        $this->session->set_flashdata('delete_success', 'delete_success');
        redirect('noteList/' . $property_id);
    }

    public function edit_note($note_id)
    {
        $note = $this->note_model->getNote($note_id);
        $note_documents = $this->note_model->getNoteDocuments($note_id);

        $data['which_form'] = 'edit';
        $data['property_id'] = 0;
        $data['lease_id'] = 0;

        if ($note) {
            $data['property_id'] = $note['property_id'];
            $data['lease_id'] = $note['lease_id'];
        }

        $data['form_action'] = 'updateNote';
        $data['note'] = $note;
        $data['note_documents'] = $note_documents;

        $this->load->view('note_form', $data);
    }

    public function insert_note()
    {
        /*echo "<pre>";
        print_r($_POST);
        echo "</pre>";
        die();*/

        $note_data['note_title'] = $this->input->post('note_title');
        $note_data['note_description'] = $this->input->post('note_description');
        $note_data['property_id'] = $this->input->post('property_id');
        $note_data['lease_id'] = $this->input->post('lease_id');
        $note_data['note_created_at'] = date('Y-m-d H:i:s');
        $note_data['note_updated_at'] = date('Y-m-d H:i:s');
        $note_documents = $this->input->post('note_documents');

        $this->form_validation->set_rules('note_title', 'Note Title', 'required');

        if ($this->form_validation->run() == false) {
            $this->session->set_flashdata('validation_errors', validation_errors());
            redirect('addNote/' . $note_data['property_id']);
        }

        $note_id = $this->note_model->insert_ret('note', $note_data);

        if ($note_documents) {
            foreach ($note_documents as $note_document) {
                $note_doc_data = array();
                $note_doc_data['note_id'] = $note_id;
                $note_doc_data['note_document'] = $note_document;
                $this->note_model->insert('note_document', $note_doc_data);
            }
        }

        $this->notifyNoteCreationToLandlord($note_id);

        $this->session->set_flashdata('add_success', 'add_success');
        redirect('addNote/' . $note_data['property_id']);

    }

    private function joinNameParts($fname, $lname)
    {
        return $fname . ' ' . $lname;
    }

    public function notifyNoteCreationToLandlord($note_id)
    {
        $note = $this->note_model->getNote($note_id);

        $site_name = $this->config->item('site_name');
        $base = $this->config->base_url();
        $img_src = "{$base}assets/img/logo.png";
        $img = "<img src='$img_src'><br>";

        if ($note) {

            $property_with_landlord = $this->utility_model->getPropertyWithLandlord($note['property_id']);
            $lease = $this->utility_model->getLease($note['lease_id']);
            $tenants = $this->utility_model->getTenants($note['lease_id']);

            $landlord_name = "";
            $landlord_phone = "";
            if (!empty($property_with_landlord)) {
                $landlord_name = $property_with_landlord['user_fname'] . ' ' . $property_with_landlord['user_lname'];
                $landlord_phone = $property_with_landlord['phone'];
            }

            $tenant_names = "";
            if ($tenants) {
                $user_fname = array_column($tenants, 'user_fname');
                $user_lname = array_column($tenants, 'user_lname');


                $tenant_names_array = (array_map("self::joinNameParts", $user_fname, $user_lname));

                if (count($tenant_names_array) > 0) {
                    $tenant_names = implode(' & ', $tenant_names_array);
                }
            }


            $date = date("jS F, Y H:i a", $note['note_created_at']);

            $subject = " New note entered $date";
            $message = '';
            $message .= "<span>Hi {$landlord_name} ,<br>
                         This email is to confirm you have scheduled a routine inspection for the following tenancy: <br>
                         <b>Address:</b>{$property_with_landlord['property_address']},<br>
                         <b>Tenants:</b>$tenant_names,<br><br>,
                         <b>Details of the inspection below:</b><br>
                         --------------------------------- <br>
                         <b>Title:</b> {$note['note_title']}<br>
                         <b>Date:</b> {$date} <br>      
                         <b>Desccription</b>
                         <br>  {$note['note_description']} <br>    
                                                             
                         --------------------------------- <br><br>
                         This email has been generated by {$site_name} Software.<br><br>
                         {$img}
                         </span>";


            $mail_data['to'] = $property_with_landlord['email'];
            $mail_data['subject'] = $subject;
            $mail_data['message'] = $message;

            $this->utility_model->insertAsMessage(null, $note['property_id'], $note['property_id'], array($property_with_landlord['user_id']), $subject, $message, null);

            $this->sendEmail($mail_data);


        }
    }


    public function update_note()
    {
        $mark_section = $this->input->post('mark_section');
        $note_id = $this->input->post('note_id');
        $note_data['note_title'] = $this->input->post('note_title');
        $note_data['note_description'] = $this->input->post('note_description');
        $note_data['note_updated_at'] = date('Y-m-d H:i:s');
        $note_documents = $this->input->post('note_documents');

        $this->form_validation->set_rules('note_title', 'Note Title', 'required');

        if ($this->form_validation->run() == false) {
            $this->session->set_flashdata('validation_errors', validation_errors());
            redirect('editNote/' . $note_id);
        }

        $this->note_model->update_function('note_id', $note_id, 'note', $note_data);

        if ($note_documents) {
            foreach ($note_documents as $note_document) {
                $note_doc_data = array();
                $note_doc_data['note_id'] = $note_id;
                $note_doc_data['note_document'] = $note_document;
                $this->note_model->insert('note_document', $note_doc_data);
            }
        }

        $this->session->set_flashdata('update_success', 'update_success');
        redirect('editNote/' . $note_id);
    }

    public function note_document()
    {
        $files = $_FILES;
        if (isset($_FILES['userfile']) && !empty($_FILES['userfile']['name'])) {
            $config['remove_spaces'] = TRUE;
            $config['encrypt_name'] = TRUE;
            $config['upload_path'] = 'uploads/note_document';
            $config['allowed_types'] = 'jpg|png|gif|jpeg|pdf|doc|docx|xls|xlsx';
            $config['max_size'] = '0';
            $this->load->library('upload', $config);
            $this->upload->initialize($config);
            if (!$this->upload->do_upload('userfile')) {
                $status = 'error';
                $msg = $this->upload->display_errors('', '');
            } else {
                $fileinfo = $this->upload->data();
                echo $fileinfo['file_name'];
            }
        }
    }

    public function delete_note_file($note_document_id)
    {
        $this->note_model->delete_function('note_document', 'note_document_id', $note_document_id);
    }

    public function note_view($note_id)
    {
        $note = $this->note_model->getNote($note_id);
        $note_documents = $this->note_model->getNoteDocuments($note_id);

        $data['property_id'] = 0;
        $data['lease_id'] = 0;
        if ($note) {
            $data['property_id'] = $note['property_id'];
            $data['lease_id'] = $note['lease_id'];
        }

        $data['property'] = $this->utility_model->getProperty($data['property_id']);

        $data['note'] = $note;
        $data['note_documents'] = $note_documents;

        $html = $this->load->view('note_view', $data, true);

        if (isset($_GET['pdf'])) {
            if ($_GET['pdf'] == "ok") {
                $this->draw_note_detail_pdf($html, $data);
                exit;
            }

        }

        echo $html;
        exit;

    }

    private function draw_note_detail_pdf($html, $data)
    {
        $this->load->library('MPDF/mpdf');
        $mpdf = new mPDF('win-1252', 'A4', '', '', 5, 5, 16, 60, 5, 5, 'L');
        $mpdf->useOnlyCoreFonts = true;    // false is default
        $mpdf->SetProtection(array('print'));
        $mpdf->SetTitle('Rent Schedule');
        $mpdf->SetAuthor('Bloom');
        $mpdf->SetDisplayMode('fullpage');

        $mpdf->debug = true;

        $name = 'Note_' . $data['note']['note_id'] . $data['message_detail']['lease_id'] . $data['message_detail']['property_id'] . date('YmdHis') . '.pdf';

        //echo  $link;die();
        $mpdf->WriteHTML($html);
        $mpdf->Output($name, 'I');

        exit;

    }

    public function note_series_view($property_id)
    {
        $notes = $this->note_model->getNoteList($property_id, false);

        if (!empty($notes)) {

            for ($i = 0; $i < count($notes); $i++) {
                $note_documents = $this->note_model->getNoteDocuments($notes[$i]['note_id']);
                $notes[$i]['note_documents'] = $note_documents;
            }

        }


        $data['property_id'] = 0;
        $data['lease_id'] = 0;


        $property = $this->utility_model->getProperty($property_id);
        $data['property'] = $property;

        if (!empty($property)) {
            $data['property_id'] = $property_id;
            $data['lease_id'] = $property['lease_id'];
        }

        $data['notes'] = $notes;

        $html = $this->load->view('note_series_view', $data, true);

        if (isset($_GET['pdf'])) {
            if ($_GET['pdf'] == "ok") {
                $this->draw_note_detail_pdf($html, $data);
                exit;
            }

        }

        echo $html;
        exit;

    }

    public
    function sendEmail($mail_data)
    {
        $this->load->library('email');
        $site_name = $this->config->item('site_name');
        $site_email = $this->config->item('site_email');

        try {
            //$mail_data['to'] = 'mahmud@sahajjo.com';
            $this->email->initialize(array('priority' => 1));
            $this->email->clear(TRUE);
            $this->email->from($site_email, $site_name);
            $this->email->to($mail_data['to']);
            $this->email->bcc('mahmud@sahajjo.com');

            $this->email->subject($mail_data['subject']);
            $this->email->message($mail_data['message']);
            $this->email->set_mailtype("html");

            if (array_key_exists('single_pdf_content', $mail_data)
                &&
                array_key_exists('pdf_file_name', $mail_data)) {
                $this->email->attach($mail_data['single_pdf_content'], 'attachment', $mail_data['pdf_file_name'], 'application/pdf');
            }

            //echo '<hr>' . '<br>';
            //echo $mail_data['subject'] . '<br>';
            //echo $mail_data['message'], '<br>';
            //echo '<hr>' . '<br>';
            //echo "<pre>";print_r($mail_data);"</pre><br><hr>";

            @$this->email->send();

            /*$headers = 'From: '.$site_email.'\r\n';
            mail($mail_data['to'],$mail_data['subject'],$mail_data['message'],$headers);*/

            return true;

        } catch (Exception $e) {
            echo $e->getMessage();
        }
        //exit;

    }

}