<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Property_model extends CI_Model
{
    ////// Basic Model Function Starts ///////
    public function insert($table_name, $data)
    {
        $this->db->insert($table_name, $data);
    }

    public function joinOneTableOneCondition($selector, $fromTable, $fromTableCol, $joinTable, $joinTableCol, $whereCondition)
    {
        $this->db->select($selector);
        $this->db->from($fromTable);

        $this->db->join($joinTable, $joinTable . '.' . $joinTableCol . ' = ' . $fromTable . '.' . $fromTableCol, 'left');
        $this->db->where($whereCondition);

        $result = $this->db->get();
//        echo $this->db->last_query();
        return $result->result_array();
    }

    public function selectOneCondition($select, $table, $whereCondition1)
    {
        $this->db->select($select);
        $this->db->from($table);

        $this->db->where($whereCondition1);

        $result = $this->db->get();
        return $result->result_array();
    }


    public function update_lease_detail($property_id, $lease_detail_data)
    {
        $this->db->where('property_id', $property_id);
        $this->db->where('lease_id', 0);
        $this->db->update('lease_detail', $lease_detail_data);
    }

    public function get_tenant_info_first_three($property_id)
    {
        $this->db->select('*');
        $this->db->from('lease_detail');
        $this->db->join('user', 'user.user_id=lease_detail.tenant_id');
        $this->db->where('lease_detail.property_id', $property_id);
        $this->db->where('lease_detail.lease_id', 0);
        $this->db->limit(3);

        $result = $this->db->get();
        return $result->result_array();
    }

    public function update_existing_lease_detail($property_id, $lease_id, $data)
    {
        $this->db->where('property_id', $property_id);
        $this->db->where('lease_id', $lease_id);
        $this->db->update('lease', $data);
    }

    public function insert_ret($tablename, $tabledata)
    {
        $this->db->insert($tablename, $tabledata);
        return $this->db->insert_id();
    }

    public function update_function($columnName, $columnVal, $tableName, $data)
    {
        $this->db->where($columnName, $columnVal);
        $this->db->update($tableName, $data);
    }


    public function delete_function_cond($tableName, $cond)
    {
        $where = '( ' . $cond . ' )';
        $this->db->where($where);
        $this->db->delete($tableName);
    }

    public function delete_function($tableName, $columnName, $columnVal)
    {
        $this->db->where($columnName, $columnVal);
        $this->db->delete($tableName);
    }

    public function select_all($table_name)
    {
        $this->db->select('*');
        $this->db->from($table_name);

        $query = $this->db->get();
        $result = $query->result_array();

        return $result;
    }

    public function select_all_decending($table_name)
    {
        $this->db->select('*');
        $this->db->from($table_name);
        $this->db->order_by('created_at', 'DESC');
        $query = $this->db->get();
        $result = $query->result_array();
        return $result;
    }

    public function select_all_acending($table_name, $col_name)
    {
        $this->db->select('*');
        $this->db->from($table_name);
        $this->db->order_by($col_name, 'ASC');
        $query = $this->db->get();
        $result = $query->result_array();
        return $result;
    }

    public function select_condition_decending($table_name, $condition)
    {
        $this->db->select('*');
        $this->db->from($table_name);
        $where = '(' . $condition . ')';
        $this->db->where($where);
        $this->db->order_by('created_at', 'DESC');
        $query = $this->db->get();
        $result = $query->result_array();
        return $result;
    }

    public function select_condition_order($table_name, $condition, $order_col, $order_type)
    {
        $this->db->select('*');
        $this->db->from($table_name);
        $where = '(' . $condition . ')';
        $this->db->where($where);
        $this->db->order_by($order_col, $order_type);
        $query = $this->db->get();
        $result = $query->result_array();
        return $result;
    }


    public function select_with_where($selector, $condition, $tablename)
    {
        $this->db->select($selector);
        $this->db->from($tablename);
        $where = '(' . $condition . ')';
        $this->db->where($where);
        $result = $this->db->get();
        return $result->result_array();
    }

    public function update_with_set_value($field, $field_value, $columnName, $columnVal, $tablename)
    {
        $this->db->set($field, $field_value);
        $this->db->where($columnName, $columnVal);
        $this->db->update($tablename);
    }

    public function select_where_join($selector, $table_name, $join_table, $join_condition, $condition)
    {
        $this->db->select($selector);
        $this->db->from($table_name);
        $this->db->join($join_table, $join_condition);
        $where = '(' . $condition . ')';
        $this->db->where($where);
        //$this->db->order_by($order_col,$order_action);
        $result = $this->db->get();
        return $result->result_array();
    }

    public function select_where_left_join($selector, $table_name, $join_table, $join_condition, $condition)
    {
        $this->db->select($selector);
        $this->db->from($table_name);
        $this->db->join($join_table, $join_condition, 'left');
        $where = '(' . $condition . ')';
        $this->db->where($where);
        //$this->db->order_by($order_col,$order_action);
        $result = $this->db->get();
        return $result->result_array();
    }

    public function select_where_left_join_two($selector, $table_name, $join_table, $join_condition, $join_table2, $join_condition2, $condition)
    {
        $this->db->select($selector);
        $this->db->from($table_name);
        $this->db->join($join_table, $join_condition, 'left');
        $this->db->join($join_table2, $join_condition2, 'left');
        $where = '(' . $condition . ')';
        $this->db->where($where);
        //$this->db->order_by($order_col,$order_action);
        $result = $this->db->get();
        return $result->result_array();
    }

    public function count_result_row($table_name, $condition)
    {
        $this->db->select('*');
        $this->db->from($table_name);
        $where = '(' . $condition . ')';
        $this->db->where($where);
        $result = $this->db->get();
        return $result->num_rows();
    }

    ////// Basic Model Function End ///////


    public function select_where_two_join_order($selector, $table_name, $join_table, $join_condition, $join_table2, $join_condition2, $condition, $order_col, $order_action, $limit)
    {
        $this->db->select($selector);
        $this->db->from($table_name);
        $this->db->join($join_table, $join_condition);
        $this->db->join($join_table2, $join_condition2);
        $where = '(' . $condition . ')';
        $this->db->where($where);
        $this->db->order_by($order_col, $order_action);
        $this->db->limit($limit);
        $result = $this->db->get();
        return $result->result_array();
    }


    public function select_order_limit_condition($selector, $table_name, $condition, $order_col, $order_action, $limit)
    {
        $this->db->select($selector);
        $this->db->from($table_name);
        $where = '(' . $condition . ')';
        $this->db->where($where);
        $this->db->order_by($order_col, $order_action);
        $this->db->limit($limit);
        $result = $this->db->get();
        return $result->result_array();
    }


    public function get_selected_tenant($property_id, $lease_id)
    {
        $this->db->select('lease_detail.*,user.user_id,user.user_fname,user.user_lname,lease.lease_per_period_payment,user.email');
        $this->db->from('lease_detail');
        $this->db->join('user', 'user.user_id=lease_detail.tenant_id');
        $this->db->join('lease', 'lease.lease_id=lease_detail.lease_id');
        $this->db->where('lease_detail.property_id', $property_id);
        $this->db->where('lease_detail.lease_id', $lease_id);

        $result = $this->db->get();
        return $result->result_array();
    }


    public function get_lease_payment_schedule($property_id, $lease_id)
    {
        $this->db->select('*');
        $this->db->from('lease_payment_scedule');
        $this->db->where('property_id', $property_id);
        $this->db->where('lease_id', $lease_id);

        $result = $this->db->get();
        return $result->result_array();
    }


    public function check_user_property($user_id, $property_id)
    {
        $this->db->select('*');
        $this->db->from('property');
        $this->db->where('property_id', $property_id);
        $this->db->where('user_id', $user_id);

        $result = $this->db->get();
        return $result->num_rows();
    }

    public function update_active_lease($property_id, $lease_id, $data)
    {
        $this->db->where('property_id', $property_id);
        $this->db->where('lease_id', $lease_id);
        $this->db->update('lease', $data);
    }

    public function update_lease( $lease_id, $data)
    {
        $this->db->where('lease_id', $lease_id);
        $this->db->update('lease', $data);

    }

    public function get_specific_tenant($lease_id)
    {
        $this->db->select('*');
        $this->db->from('lease_detail');
        $this->db->join('user', 'user.user_id=lease_detail.tenant_id');
        $this->db->where('lease_detail.lease_id', $lease_id);

        $result = $this->db->get();
        return $result->result_array();
    }

    public function update_tenant_infos($lease_detail_id, $property_id, $lease_id, $data)
    {
        $this->db->where('lease_detail_id', $lease_detail_id);
        $this->db->where('property_id', $property_id);
        $this->db->where('lease_id', $lease_id);
        $this->db->update('lease_detail', $data);
    }

    /*--------------------------------------------------*/

    public function getProperty($property_id)
    {
        $this->db->select('*');
        $this->db->from('property');
        $this->db->where('property_id', $property_id);

        $query = $this->db->get();
        return $query->row_array();
    }

    public function getLease($lease_id)
    {
        $this->db->select('*');
        $this->db->from('lease');
        $this->db->where('lease_id', $lease_id);

        $query = $this->db->get();
        return $query->row_array();
    }

    public function insertPaymentScheduleInBatch($data)
    {
        $this->db->insert_batch('lease_payment_scedule', $data);
    }

    public function removeLeasePaymentSchedule($property_id, $lease_id)
    {
        $this->db->where('property_id', $property_id);
        $this->db->where('lease_id', $lease_id);
        $this->db->delete('lease_payment_scedule');
    }

    /*----------------------------------------------------*/


    public function get_specific_selected_tenant($property_id, $lease_id)
    {
        $this->db->select('lease_detail.*,user.user_fname,user.user_lname,user.phone,user.email,lease.lease_per_period_payment');
        $this->db->from('lease_detail');
        $this->db->join('user', 'user.user_id=lease_detail.tenant_id');
        $this->db->join('lease', 'lease.lease_id=lease_detail.lease_id');
        $this->db->where('lease_detail.property_id', $property_id);
        $this->db->where('lease_detail.lease_id', $lease_id);
        $this->db->where('lease_detail.payment_status', 1);

        $result = $this->db->get();
        return $result->result_array();
    }

    public function get_active_property_lease($user_id)
    {
        $this->db->select('*');
        $this->db->from('property');
        $this->db->join('lease', 'lease.property_id=property.property_id', 'left');
        $this->db->where('property.user_id', $user_id);
        $this->db->where('lease.lease_current_status', 1);

        $result = $this->db->get();
        return $result->result_array();
    }

    public function get_property_lease($user_id)
    {
        $this->db->select('*');
        $this->db->from('property');
        $this->db->join('lease', 'lease.property_id=property.property_id', 'left');
        $this->db->where('property.user_id', $user_id);

        $result = $this->db->get();
        return $result->result_array();
    }

    public function get_properties($user_id)
    {
        $this->db->select('*');
        $this->db->from('property');
        $this->db->where('property.user_id', $user_id);

        $result = $this->db->get();
        return $result->result_array();
    }

    public function get_all_paid_amount($property_id)
    {
        $this->db->select('sum(total_invoice_amount) as total_paid');
        $this->db->from('expense');
        $this->db->where('property_id', $property_id);
        $this->db->where('expense_paying_status', 1);
        $this->db->where('expense_payment_due >=', date('Y-m-01'));
        $this->db->where('expense_payment_due <=', date('Y-m-31'));

        $result = $this->db->get();
        return $result->result_array();
    }

    public function get_all_partial_paid_amount($property_id)
    {
        $this->db->select('sum(expense_partial_amount_paid) as total_partial_paid');
        $this->db->from('expense');
        $this->db->where('property_id', $property_id);
        $this->db->where('expense_paying_status', 2);
        $this->db->where('expense_payment_due >=', date('Y-m-01'));
        $this->db->where('expense_payment_due <=', date('Y-m-31'));

        $result = $this->db->get();
        return $result->result_array();
    }

    public function get_all_due_amount($property_id, $start_date, $end_date)
    {
        $this->db->select('sum(total_invoice_amount) as total_due');
        $this->db->from('expense');
        $this->db->where('property_id', $property_id);
        $this->db->where('expense_paying_status', 3);
        $this->db->where('expense_payment_due >=', $start_date);
        $this->db->where('expense_payment_due <=', $end_date);

        $result = $this->db->get();
        return $result->result_array();
    }

    public function get_all_overdue_amount($property_id)
    {
        $this->db->select('sum(total_invoice_amount) as total_overdue');
        $this->db->from('expense');
        $this->db->where('property_id', $property_id);
        $this->db->where('expense_paying_status', 3);
        $this->db->where('expense_payment_due <', date('Y-m-d'));

        $result = $this->db->get();
        return $result->result_array();
    }

    public function get_all_new_maintanance($property_id)
    {
        $this->db->select('*');
        $this->db->from('maintenance');
        $this->db->where('maintenance_status', 1);

        $result = $this->db->get();
        return $result->num_rows();
    }

    public function get_all_active_maintanance($property_id)
    {
        $this->db->select('*');
        $this->db->from('maintenance');
        $this->db->where('maintenance_status', 2);
        $this->db->or_where('maintenance_status', 3);

        $result = $this->db->get();
        return $result->num_rows();
    }

    public function get_all_new_maintanance_of_current_lease($property_id)
    {
        $this->db->select('*');
        $this->db->from('maintenance');
        $this->db->where('maintenance_status', 1);

        $this->db->join('user', 'maintenance.created_by = user.user_id', 'LEFT');
        $this->db->where('maintenance.property_id', $property_id);

        $this->db->join('lease', 'maintenance.lease_id = lease.lease_id', 'LEFT');
        $this->db->where('lease.lease_current_status', 1);

        $result = $this->db->get();
        return $result->num_rows();
    }

    public function get_all_active_maintanance_of_current_lease($property_id)
    {
        $this->db->select('*');
        $this->db->from('maintenance');
        $this->db->group_start();
        $this->db->where('maintenance_status', 2);
        $this->db->or_where('maintenance_status', 3);
        $this->db->group_end();

        $this->db->join('user', 'maintenance.created_by = user.user_id', 'LEFT');
        $this->db->where('maintenance.property_id', $property_id);

        $this->db->join('lease', 'maintenance.lease_id = lease.lease_id', 'LEFT');
        $this->db->where('lease.lease_current_status', 1);

        $result = $this->db->get();
        return $result->num_rows();
    }

    public function count_maintanance_of_current_lease_by_status($property_id,$status)
    {
        $this->db->select('*');
        $this->db->from('maintenance');

        $st_arr = array();
        $st_arr['new'] = 1;
        $st_arr['open'] = 2;
        $st_arr['to_do_later'] = 3;
        $st_arr['closed'] = 4;
        $st_arr['archived'] = 5;


        if(!empty($status) && array_key_exists($status,$st_arr)){
            $this->db->where('maintenance_status',$st_arr[$status]);
        }



        $this->db->join('user', 'maintenance.created_by = user.user_id', 'LEFT');
        $this->db->where('maintenance.property_id', $property_id);

        $this->db->join('lease', 'maintenance.lease_id = lease.lease_id', 'LEFT');
        $this->db->where('lease.lease_current_status', 1);

        $result = $this->db->get();
        return $result->num_rows();
    }

    public function select_condition_order_and_join($lease_id, $property_id)
    {
        $this->db->select('*');
        $this->db->from('lease_tenant_payment_scedhule');
        $this->db->join('user', 'user.user_id=lease_tenant_payment_scedhule.tenant_id', 'LEFT');
        $this->db->where('lease_tenant_payment_scedhule.lease_id', $lease_id);
        $this->db->where('lease_tenant_payment_scedhule.property_id', $property_id);
        $this->db->order_by('lease_tenant_payment_scedhule.schedule_start_date', 'DESC');

        $result = $this->db->get();
        return $result->result_array();
    }

    public function select_row_with_where($selector, $condition, $tablename)
    {
        $this->db->select($selector);
        $this->db->from($tablename);
        $where = '(' . $condition . ')';
        $this->db->where($where);
        $result = $this->db->get();
        return $result->row_array();
    }

    public function select_condition_order_and_join_with_where($lease_id, $property_id, $tenant_ledger_start_date, $tenant_ledger_end_date)
    {
        $this->db->select('*');
        $this->db->from('lease_tenant_payment_scedhule');
        $this->db->join('user', 'user.user_id=lease_tenant_payment_scedhule.tenant_id', 'LEFT');
        $this->db->where('lease_tenant_payment_scedhule.lease_id', $lease_id);
        $this->db->where('lease_tenant_payment_scedhule.property_id', $property_id);
        if ($tenant_ledger_start_date != "") {
            $this->db->where('lease_tenant_payment_scedhule.schedule_start_date >=', $tenant_ledger_start_date);
        }
        if ($tenant_ledger_end_date != "") {
            $this->db->where('lease_tenant_payment_scedhule.schedule_start_date <=', $tenant_ledger_end_date);
        }

        $this->db->order_by('lease_tenant_payment_scedhule.lease_tenant_payment_scedhule_id', 'DESC');

        $result = $this->db->get();

        //echo $this->db->last_query();die();
        return $result->result_array();
    }

    public function getWaterInvoiceList($lease_id)
    {
        $this->db->select('*');
        $this->db->from('water_invoice');
        $this->db->where('lease_id', $lease_id);

        $query = $this->db->get();
        $result = $query->result_array();
        return $result;
    }



}

?>