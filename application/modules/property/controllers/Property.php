<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Property extends MX_Controller
{

    function __construct()
    {
        $this->load->model('property_model');
        $this->load->model('rent/rent_model');
        $this->load->model('other/other_model');
        $this->load->model('water/water_model');
        $this->load->model('inspection/inspection_model');
        $this->load->model('utility/utility_model');


        //date_default_timezone_set('Asia/Dhaka');
        if ($this->session->userdata('language_select') == 'bangla') {
            $this->lang->load('front', 'bangla');
        } else {
            $this->lang->load('front', 'english');
        }

        $user_id = $this->session->userdata('user_id');
        $email_id = $this->session->userdata('email');
        $name = $this->session->userdata('name');
        $type = $this->session->userdata('role_type');
        $user_lname = $this->session->userdata('user_fname');
        $user_lname = $this->session->userdata('user_lname');

        if ($user_id == '' || $email_id = '') {
            $this->session->unset_userdata('user_id');
            $this->session->unset_userdata('email');
            $this->session->unset_userdata('name');
            $this->session->unset_userdata('role_type');
            $this->session->unset_userdata('user_fname');
            $this->session->set_userdata('log_err', 'Login First');
            redirect('login', 'refresh');
        }

        $this->utility_model->check_auth();

        ini_set('memory_limit', '128M');
        ini_set('max_execution_time', 900); //300 seconds = 5 minutes
    }

    public function index()
    {
        $user_id = $this->session->userdata('user_id');



        $data['all_property'] = $this->property_model->get_properties($user_id);
        $all_property_active_lease = $this->property_model->get_active_property_lease($user_id);

        foreach ($all_property_active_lease as $key => $value) {
            $all_property_active_lease[$key]['property_payment_update'] = $this->get_current_financial_condition($value['property_id'], $value['lease_id']);
        }


        $i = 0;
        foreach ($data['all_property'] as $property) {
            $data['all_property'][$i]['all_property_active_lease'] = null;
            $j = 0;
            foreach ($all_property_active_lease as $a_property_active_lease) {
                if ($property['property_id'] == $a_property_active_lease['property_id']) {
                    $data['all_property'][$i]['all_property_active_lease'][$j] = $a_property_active_lease;
                }
                $j++;
            }
            $i++;
        }

        /*echo "<pre>";
        print_r($data['all_property']);
        //print_r($all_property_active_lease);
        echo "</pre>";
        die();*/

        $this->load->view('property_all', $data);
    }

    public function gg()
    {
        $data['x'] = 'Y';
        $this->load->view('cc', $data);
    }

    public function property_details($property_id, $link = NULL)
    {
        if ($link != null) {
            $data['info_open'] = 1;
        } else {
            $data['info_open'] = 0;
        }
        $property_idd = $property_id;
        $user_fname = $this->session->userdata('user_fname');
        $user_id = $this->session->userdata('user_id');
        $data['property_id'] = $property_id;
        $data['get_state'] = $this->property_model->select_with_where('*', 'country_id=13 AND show_status = 1', 'state');
        $data['result'] = $this->property_model->select_with_where('*', "property_id=" . $property_idd . "", 'property');
        $data['result2'] = $this->property_model->select_with_where('*', "property_id=" . $property_idd . "", 'property_photos');
        $data['all_country'] = $this->other_model->select_all_acending('country', 'country_name');
        $data['property_note'] = $this->property_model->select_with_where('*', "property_id=" . $property_idd . "", 'property_notes');
        $get_property_lease_update = $this->property_model->select_with_where('*', 'property_id=' . $property_id . '', 'property');
        if ($get_property_lease_update[0]['lease_update_done'] == 1) {
            //do not know why this was written originally .Creating problem , so commented it
            //redirect('leaseDescription/' . $property_id);
        }
        if ($this->input->post('lease_id')) {
            if (($this->input->post('lease_id')) == 'new_lease') {
                $add_another_lease_param = "?add_another_lease=yes";
                redirect('stepThree/' . $property_id . $add_another_lease_param);
            } else {
                $this->property_model->update_with_set_value('lease_current_status', '0', 'property_id', $property_id, 'lease');
                $lease_id = $this->input->post('lease_id');
                $upd_data['lease_current_status'] = 1;
                $this->property_model->update_active_lease($property_id, $lease_id, $upd_data);
            }
        }
        $data['property_id'] = $property_id;

        $data['lease_detail'] = $this->property_model->select_with_where('*', 'property_id=' . $property_id . ' AND lease_current_status=1', 'lease');

        //-------------------------------------------------------------------------
        $lease_id = !empty($data['lease_detail']) ? $data['lease_detail'][0]['lease_id'] : 0;
        $is_archived = $this->utility_model->isArchived();
        $archived_string = $this->utility_model->getArchivedString();
        if ($is_archived) {
            $lease_id = $this->utility_model->getArchivedLeaseId();
            $data['lease_detail'][0] = $this->utility_model->getLease($lease_id);
        }
        $data['is_archived'] = $is_archived;
        $data['archived_string'] = $archived_string;
        //-------------------------------------------------------------------------

        //if there is no current lease
        /* if (!$data['lease_detail']) {
             redirect('ArchivedLeases/' . $property_id);
         }*/

        $data['property_info'] = $this->rent_model->select_with_where('*', 'property_id=' . $property_id . '', 'property');
        $data['all_lease'] = $this->property_model->select_with_where('*', 'property_id=' . $property_id . '', 'lease');
        $data['property_financial_condition'] = $this->get_current_financial_condition($property_id, $lease_id);
        $data['lease_detail_tenant'] = $this->property_model->select_with_where('*', 'lease_id=' . $lease_id . '', 'lease_detail');
        $data['expense_paid_amount'] = $this->property_model->get_all_paid_amount($property_id);
        $data['expense_partial_paid_amount'] = $this->property_model->get_all_partial_paid_amount($property_id);
        $start_date = date('Y-m-d');
        $end_date = date('Y-m-d', strtotime("+30 day", strtotime($start_date)));
        $data['expense_due_amount'] = $this->property_model->get_all_due_amount($property_id, $start_date, $end_date);
        $data['expense_overdue_amount'] = $this->property_model->get_all_overdue_amount($property_id, $start_date, $end_date);
        $data['total_maintanance_new'] = $this->property_model->get_all_new_maintanance_of_current_lease($property_id);
        $data['total_maintanance_active'] = $this->property_model->get_all_active_maintanance_of_current_lease($property_id);

        $data['count_new_maintanance'] = $this->property_model->count_maintanance_of_current_lease_by_status($property_id, 'new');
        $data['count_open_maintanance'] = $this->property_model->count_maintanance_of_current_lease_by_status($property_id, 'open');
        $data['count_closed_maintanance'] = $this->property_model->count_maintanance_of_current_lease_by_status($property_id, 'closed');
        $data['count_archived_maintanance'] = $this->property_model->count_maintanance_of_current_lease_by_status($property_id, 'archived');

        $data['get_all_tenant'] = $this->property_model->get_specific_selected_tenant($property_id, $lease_id);
        $data['all_tenant_payment_info'] = $this->property_model->select_condition_order('lease_tenant_payment_scedhule', 'property_id=' . $property_id . ' AND lease_id=' . $lease_id . ' AND payment_status=2 OR payment_status=4', 'schedule_start_date', 'ASC');

        $data['water_invoices'] = $this->property_model->getWaterinvoiceList($lease_id);


        $water_invoice_count = 0;
        $paid_water_invoice_count = 0;
        $due_water_invoice_count = 0;
        $water_invoice_total = 0.00;
        $paid_water_invoice_total = 0.00;
        $due_water_invoice_total = 0.00;

        if ($data['water_invoices']) {
            foreach ($data['water_invoices'] as $water_invoice) {
                $water_invoice_count++;
                $water_invoice_total += $water_invoice['water_invoice_amount'];
                $paid_water_invoice_total += $water_invoice['water_invoice_amount_paid'];
                $due_water_invoice_total += $water_invoice['water_invoice_amount_due'];

                if ($water_invoice['water_invoice_status'] == 1) {
                    $paid_water_invoice_count++;
                } else if ($water_invoice['water_invoice_status'] == 0) {
                    $due_water_invoice_count++;
                }
            }
        }

        $data['water_invoice_count'] = $water_invoice_count;
        $data['paid_water_invoice_count'] = $paid_water_invoice_count;
        $data['due_water_invoice_count'] = $due_water_invoice_count;
        $data['water_invoice_total'] = $water_invoice_total;
        $data['paid_water_invoice_total'] = $paid_water_invoice_total;
        $data['due_water_invoice_total'] = $due_water_invoice_total;

        $data['income_expense_graph_json'] = $this->getIncomeExpenseGraphJson($property_id, $lease_id);
        $data['next_inspection'] = $this->inspection_model->getNextInspection($property_id, 0);
        $data['upcoming_inspections'] = $this->inspection_model->getInspectionList($property_id, 0, 5);

        $pid = $this->input->get('id', TRUE);
        $this->session->set_userdata('property_id', $pid);
        $this->load->view('property_details', $data);
    }

    private function getIncomeExpenseGraphJson($property_id, $lease_id)
    {
        $data = array();
        $prepare_data_by_loop_or_sql_view = 2; //set 1 for loop, set 2 for sql_view

        //not really, but in case
        $fiscal_year_starts = date('Y-01-01');
        $fiscal_year_ends = date('Y-12-31');

        $y1 = (int)date('Y');
        $y2 = (int)date('Y');
        if (strtotime(date('Y-m-d')) <= strtotime(date('Y-06-30'))) {
            $y1 = (int)date('Y') - 1;
        } else {
            $y2 = (int)date('Y') + 1;
        }

        $fiscal_year_starts = date("$y1-07-01");
        $fiscal_year_ends = date("$y2-06-30");


        $arrs = array();
        $arrs['rent_income'] = $this->utility_model->rentIncomeDatesWithAmountByLease($lease_id, $fiscal_year_starts, $fiscal_year_ends);
        $arrs['money_in_income'] = $this->utility_model->moneyInIncomeDatesWithAmountByLease($lease_id, $fiscal_year_starts, $fiscal_year_ends);
        $arrs['water_income'] = $this->utility_model->waterIncomeDatesWithAmountByLease($lease_id, $fiscal_year_starts, $fiscal_year_ends);
        $arrs['full_main_expense'] = $this->utility_model->mainExpenseDatesWithAmountByProperty($property_id, 'full', $fiscal_year_starts, $fiscal_year_ends);
        $arrs['partial_main_expense'] = $this->utility_model->mainExpenseDatesWithAmountByProperty($property_id, 'partial', $fiscal_year_starts, $fiscal_year_ends);
        //$arrs['maintenance_expense'] = $this->utility_model->maintenanceExpenseDatesWithAmountByLease($lease_id); //covered in normal expense

        $merged_arr = array();
        foreach ($arrs as $arr_key => $arr_val) {
            if (!empty($arr_val)) {
                $merged_arr = array_merge($merged_arr, $arr_val);
            }
        }


        if (!empty($merged_arr)) {
            uasort($merged_arr, "self::date_cmp"); //sort by date
            sort($merged_arr); //reorder keys

            if ($prepare_data_by_loop_or_sql_view == 2) {
                $data = $this->prepareIncomeExpenseGraphBySql($merged_arr);
            }

        }


        $json_data = array();

        $month_names[1] = 'January';
        $month_names[2] = 'February';
        $month_names[3] = 'March';
        $month_names[4] = 'April';
        $month_names[5] = 'May';
        $month_names[6] = 'June';
        $month_names[7] = 'July';
        $month_names[8] = 'August';
        $month_names[9] = 'September';
        $month_names[10] = 'October';
        $month_names[11] = 'November';
        $month_names[12] = 'December';

        for ($i = 1; $i <= 12; $i++) {
            $marr = array();
            $marr['mon'] = $month_names[$i];
            $marr['income'] = 0.00;
            $marr['expense'] = 0.00;

            if (!empty($data)) {

                for ($j = 0; $j < count($data); $j++) {
                    if ($data[$j]['mon'] == $i) {
                        $marr['mon'] = $month_names[$data[$j]['mon']];
                        $marr['income'] = floatval($data[$j]['income']);
                        $marr['expense'] = floatval($data[$j]['expense']);
                    }
                }

            }

            $json_data[] = $marr;
        }

        if (!empty($json_data)) {


            $len = count($json_data);
            $firsthalf = array_slice($json_data, 0, $len / 2);
            $secondhalf = array_slice($json_data, $len / 2);
            $json_data = array_merge($secondhalf, $firsthalf);
        }

        return json_encode($json_data);

    }

    function date_cmp($a, $b)
    {
        return strcmp($a["date"], $b["date"]);
    }

    private function prepareIncomeExpenseGraphBySql($merged_arr)
    {
        //$this->db->query('DROP TEMPORARY TABLE IF EXISTS temp_income_expense');//precaution
        $this->db->query('CREATE TEMPORARY TABLE temp_income_expense 
          ( date DATE, income DECIMAL(20,2) , expense DECIMAL(20,2))');

        $this->db->insert_batch('temp_income_expense', $merged_arr);

        $rows = $this->db->query("SELECT SUM(income) as income,SUM(expense) as expense, MONTH(date) as mon FROM temp_income_expense GROUP BY mon")->result_array();

        return $rows;
    }

    public function ajax_get_state()
    {
        $country_id = $this->input->post('country_id');
        $data['get_state'] = $this->other_model->select_with_where('*', 'country_id=' . $country_id, 'state');
        $state_data_json = array();
        $state_data_json['stateDiv'] = $this->load->view('state_div', $data, TRUE);
        echo json_encode($state_data_json);
    }

    public function ajax_get_state2()
    {
        $country_id = $this->input->post('country_idd');
        $data['get_state'] = $this->other_model->select_with_where('*', 'country_id=' . $country_id, 'state');


        $data['property_state'] = $this->other_model->select_with_where('*', 'country=' . $country_id, 'property');

        $state_data_json = array();
        $state_data_json['stateDiv'] = $this->load->view('state_div2', $data, TRUE);
        echo json_encode($state_data_json);
    }

    public function delete_property_note()
    {
        $id = $this->input->get('id');
        $result = $this->other_model->delete_property_note($id);
    }

    public function update_property_photos_info($property_id)
    {
        $data['property_id'] = $property_id;
        $data3['property_note'] = $this->input->post('property_notess');
        $data3['property_note_id'] = $this->input->post('property_note_id');
        $countNote = count($data3['property_note_id']);
        //echo $countNote; die;
        if ($countNote > 0) {
            $data4['property_id'] = $property_id;
            for ($j = 0; $j < $countNote; $j++) {
                $data4['property_id'] = $property_id;

                $data4['property_note'] = $data3['property_note'][$j];
                $data5['property_note_id'] = $data3['property_note_id'][$j];

                if ($data5['property_note_id'] == 0) {
                    $result_set = $this->other_model->insert('property_notes', $data4);
                } else {
                    $result_set = $this->other_model->note_update($data4, $data5);
                }
            }
        }

        $image = $this->input->post('image');
        if ($image) {
            foreach ($image as $key => $value) {
                $image_data['property_photo'] = $value;
                $image_data['property_id'] = $property_id;
                $result_set = $this->other_model->insert('property_photos', $image_data);
            }
        }

        $property_data['property_address'] = $this->input->post('property_address');
        $property_data['city'] = $this->input->post('city');
        $property_data['zip_code'] = $this->input->post('zip_code');
        $property_data['country'] = $this->input->post('country');
        $property_data['state'] = $this->input->post('state');
        $property_data['bedrooms'] = $this->input->post('bedrooms');
        $property_data['bathrooms'] = $this->input->post('bathrooms');
        $property_data['carspaces'] = $this->input->post('carspaces');
        $property_data['purchase_details'] = $this->input->post('purchase_details');

        // echo "<pre>";
        // print_r($property_data);
        // die();
        $this->form_validation->set_rules('property_address', 'Property Address', 'required');
        $this->form_validation->set_rules('city', 'City', 'required');
        $this->form_validation->set_rules('zip_code', 'Zip Code', 'required');
        $this->form_validation->set_rules('country', 'Country', 'required');
        $this->form_validation->set_rules('state', 'State', 'required');
        if ($this->form_validation->run() == FALSE) {
            redirect('Dashboard/' . $property_id . '/1');
        } else {
            $this->property_model->update_function('property_id', $property_id, 'property', $property_data);
            redirect('Dashboard/' . $property_id . '/1');
        }
    }

    private function set_upload_options_multiple()
    {
        //upload an image options
        $config = array();
        $config['upload_path'] = './uploads/property_file/';
        $config['allowed_types'] = 'jpg|png|gif|jpeg';
        $config['max_size'] = '10000';
        $config['remove_spaces'] = TRUE;
        $config['encrypt_name'] = TRUE;
        $config['overwrite'] = FALSE;

        return $config;
    }

    public function delete_property_image()
    {
        $id = $this->input->get('id');
        $result = $this->other_model->delete_property_photo($id);
    }

    public function delete_property()
    {
        $property_id = $this->input->get('pid');
        $result = 1;
        if ($result) {
            redirect('property', 'refresh');
        }
    }

    public function property_photo()
    {
        $files = $_FILES;
        if (isset($_FILES['userfile']) && !empty($_FILES['userfile']['name'])) {
            $config['remove_spaces'] = TRUE;
            $config['encrypt_name'] = TRUE;
            $config['upload_path'] = 'uploads/property_file';
            $config['allowed_types'] = 'jpg|png|gif|jpeg';
            $config['max_size'] = '0';
            $this->load->library('upload', $config);
            $this->upload->initialize($config);
            if (!$this->upload->do_upload('userfile')) {
                $status = 'error';
                $msg = $this->upload->display_errors('', '');

            } else {
                $fileinfo = $this->upload->data();
                echo $fileinfo['file_name'];
            }
        }
    }


    public function get_current_financial_condition($property_id, $lease_id)
    {
        $get_tenant_info = $this->property_model->get_specific_selected_tenant($property_id, $lease_id);
        $count = 0;
        foreach ($get_tenant_info as $key => $row) {
            if ($row['payment_update_status'] == 1) {
                $count = $count + 0;
            } elseif ($row['payment_update_status'] == 2) {
                $count = $count - $row['payment_update_by'];
            } elseif ($row['payment_update_status'] == 3) {
                $count = $count + $row['payment_update_by'];
            } elseif ($row['payment_update_status'] == 4) {
                $count = $count - $row['payment_update_by'];
            }
        }
        return $count;
    }

    public function property_dashboard($property_id)
    {
        if ($this->input->post('lease_id')) {
            if (($this->input->post('lease_id')) == 'new_lease') {
                redirect('property/step_three/' . $property_id);
            } else {
                $this->property_model->update_with_set_value('lease_current_status', '0', 'property_id', $property_id, 'lease');
                $lease_id = $this->input->post('lease_id');
                $data['lease_current_status'] = 1;
                $this->property_model->update_active_lease($property_id, $lease_id, $data);
            }
        }
        $data['property_id'] = $property_id;
        $data['all_lease'] = $this->property_model->select_with_where('*', 'property_id=' . $property_id . '', 'lease');

        $data['lease_detail'] = $this->property_model->select_with_where('*', 'property_id=' . $property_id . ' AND lease_current_status=1', 'lease');
        // echo "<pre>";print_r($data['all_lease']);die();
        $data['all_tenant'] = $this->property_model->get_specific_tenant($data['lease_detail'][0]['lease_id']);

        $user_id = $this->session->userdata('user_id');
        $chk_prprty_exist = $this->property_model->check_user_property($user_id, $property_id);
        if ($chk_prprty_exist == 1) {
            $this->load->view('property_dashboard', $data);
        } else {
            redirect('property');
        }
    }

    public function steps($property_id, $step_count, $flow = false)
    {
        $data['lease_detail'] = $this->property_model->select_with_where('*', 'property_id=' . $property_id . ' AND lease_current_status=1', 'lease');

        if ($step_count == 2) {
            redirect('stepTwo/' . $property_id);
        } else if ($step_count == 3) {
            redirect('stepThree/' . $property_id);
        } else if ($step_count == 4) {
            redirect('stepFour/' . $property_id);
        } else if ($step_count == 5) {
            redirect('stepFive/' . $property_id . '/' . $data['lease_detail'][0]['lease_id'] . '/' . $flow);
        } else if ($step_count == 6) {
            redirect('stepSix/' . $property_id . '/' . $data['lease_detail'][0]['lease_id']);
        }
    }

    public function step_one()
    {
        $data['step_num'] = 'one';
        $this->load->view('step_one_view', $data);
    }

    // public function ajax_get_state()
    // {
    //     $country_id = $this->input->post('country_id');
    //     $data['get_state'] = $this->property_model->select_with_where('*', 'country_id=' . $country_id.' AND show_status = 1', 'state');

    //     $state_data_json = array();
    //     $state_data_json['stateDiv'] = $this->load->view('state_div', $data, TRUE);
    //     echo json_encode($state_data_json);
    // }

    // public function ajax_get_state2()
    // {
    //     $country_id = $this->input->post('country_idd');
    //     $data['get_state'] = $this->property_model->select_with_where('*', 'country_id=' . $country_id, 'state');
    //     $data['property_state'] = $this->property_model->select_with_where('*', 'country=' . $country_id, 'property');
    //     $state_data_json = array();
    //     $state_data_json['stateDiv'] = $this->load->view('state_div2', $data, TRUE);
    //     echo json_encode($state_data_json);
    // }


    public function step_two($property_id = NULL)
    {


        $data['property_info'] = '';
        if ($property_id != NULL) {
            $data['property_id'] = $property_id;
            $condition = 'property_id' . '=' . $property_id;
            $join_condition = 'country' . ',' . 'country.country_id' . '=' . 'property.country';
            $join_condition2 = 'state' . ',' . 'state.state_id' . '=' . 'property.state';
            $data['property_info'] = $this->property_model->select_where_left_join_two('*', 'property', 'country', $join_condition, 'state', $join_condition2, $condition);
        }
        $data['get_state'] = $this->property_model->select_with_where('*', 'country_id=13 AND show_status = 1', 'state');
        $data['all_country'] = $this->property_model->select_all_acending('country', 'country_name');
//        echo '<pre>';print_r($data['property_info']);die;
        $data['step_num'] = 'two';
        $this->load->view('step_two_view', $data);
    }

    public function insert_property_step_two()
    {
        $data['property_id'] = $this->input->post('property_id');
        // print_r($data['property_id']);die();
        $data['property_address'] = $this->input->post('property_address');
        $data['city'] = $this->input->post('city');
        $data['zip_code'] = $this->input->post('zip_code');
        $data['country'] = $this->input->post('country');
        $data['state'] = $this->input->post('state');
        $data['user_id'] = $this->session->userdata('user_id');
        $data['step_count'] = 2;
        //$data['user_id'] = 1;

        $this->form_validation->set_rules('property_address', 'Property Address', 'required');
        $this->form_validation->set_rules('city', 'City', 'required');
        $this->form_validation->set_rules('zip_code', 'ZIP/Postal Code', 'required');
        $this->form_validation->set_rules('country', 'Country', 'required');
        $this->form_validation->set_rules('state', 'State', 'required');

        if ($this->form_validation->run() == false) {
            $this->session->set_userdata('insert_stp_two_err', validation_errors());
            redirect('stepTwo/' . $data['property_id']);
        } else {
            if ($data['property_id'] != '') {
                $this->property_model->update_function('property_id', $data['property_id'], 'property', $data);
            } else {

                $data['step_count'] = 3;
                $data['property_id'] = $this->property_model->insert_ret('property', $data);
            }
            redirect('stepThree/' . $data['property_id']);
        }
    }

    public function step_three($property_id)
    {
        $whereCondition = 'lease_detail.' . 'property_id' . '=' . $property_id;
        $data['all_tenant'] = $this->property_model->joinOneTableOneCondition('*', 'user', 'user_id', 'lease_detail', 'tenant_id', $whereCondition);
        $data['property_id'] = $property_id;
        $data['property_detail'] = $this->property_model->select_with_where('*', 'property_id=' . $property_id . '', 'property');
        $data['step_num'] = 'three';

        $data['add_another_lease'] = 'no';
        if (isset($_REQUEST['add_another_lease'])) {
            $data['add_another_lease'] = $_REQUEST['add_another_lease'];
        }

        if ($data['add_another_lease'] == 'yes') {
            $data['all_tenant'] = null;
        }

        $this->load->view('step_three_view', $data);
    }

    public function insert_tanent()
    {
        //echo '<pre>';print_r($_POST);die();
        $arr = array();
        $property_id = $this->input->post('property_id');
        $user_fname = $this->input->post('user_fname');
        $user_id = $this->input->post('user_id');
        $user_lname = $this->input->post('user_lname');
        $phone = $this->input->post('phone');
        $email = $this->input->post('email');
        $correspondence = $this->input->post('correspondence');
        // echo '<pre>';print_r($correspondence);die();

        $whereCondition1 = 'property_id' . '=' . $property_id;
        $lease_info = $this->property_model->selectOneCondition('*', 'lease_detail', $whereCondition1);
        $tenant_id = array();
        foreach ($lease_info as $row) {
            $tenant_id[] = $row['tenant_id'];
        }

        if (isset($user_id) && is_array($user_id)) {
            $arr = array_diff($tenant_id, $user_id);
            foreach ($arr as $ar) {
                $this->property_model->delete_function('user', 'user_id', $ar);
            }
        }

        for ($i = 0; $i < count($user_fname); $i++) {
            $data['user_fname'] = $user_fname[$i];
            $data['user_lname'] = $user_lname[$i];
            $data['phone'] = $phone[$i];
            $data['email'] = $email[$i];
            $data['correspondence'] = $correspondence[$i];
            $data['role_type'] = 2;

            if (is_array($user_id) && !empty($user_id)) {
                if ($user_id[$i] == 0) {
                    $get_tenant_id = $this->property_model->insert_ret('user', $data);
                    $lease_detail['property_id'] = $property_id;
                    $lease_detail['tenant_id'] = $get_tenant_id;

                    $this->property_model->insert('lease_detail', $lease_detail);
                } else {
                    if (in_array($user_id[$i], $tenant_id)) {
                        $this->property_model->update_function('user_id', $user_id[$i], 'user', $data);
                    }
                }
            }


        }
        $property['step_count'] = 4;
        $this->property_model->update_function('property_id', $property_id, 'property', $property);

        $add_another_lease = 'no';
        if (isset($_REQUEST['add_another_lease'])) {
            $add_another_lease = $_REQUEST['add_another_lease'];
        }

        $add_another_lease_param = "?add_another_lease={$add_another_lease}";

        redirect('property/step_four/' . $property_id . $add_another_lease_param);
    }

    public function step_four($property_id)
    {
        $data['property_id'] = $property_id;
        $lease_new_name = '';
        $data['tenant_info'] = $this->property_model->get_tenant_info_first_three($property_id);
        if (count($data['tenant_info']) > 0) {
            foreach ($data['tenant_info'] as $key => $value) {
                if ($key == 0) {
                    $lease_new_name = $lease_new_name . $value['user_fname'] . ' ' . $value['user_lname'];
                } else {
                    $lease_new_name = $lease_new_name . ' And ' . $value['user_fname'] . ' ' . $value['user_lname'];
                }
            }
        }
        $data['lease_new_name'] = $lease_new_name;
        $data['property_detail'] = $this->property_model->select_with_where('*', 'property_id=' . $property_id . '', 'property');
        $data['lease_info'] = $this->property_model->select_with_where('*', 'property_id=' . $property_id . ' AND lease_current_status=1', 'lease');
        if ($data['lease_info']) {
            $data['bond_info'] = $this->property_model->select_with_where('*', 'property_id=' . $property_id . ' AND lease_id=' . $data['lease_info'][0]['lease_id'] . '', 'bond');
        } else {
            $data['bond_info'][0]['bond_amount'] = '';
            $data['bond_info'][0]['lodge_start_date'] = '';
            $data['bond_info'][0]['lodged_with'] = '';
            $data['bond_info'][0]['bond_id'] = 0;
        }
        // echo "<pre>";
        // print_r($data['bond_info']);
        // die();
        $data['step_num'] = 'four';

        $add_another_lease = 'no';
        if (isset($_REQUEST['add_another_lease'])) {
            $add_another_lease = $_REQUEST['add_another_lease'];
        }
        $data['add_another_lease'] = $add_another_lease;

        if ($data['add_another_lease'] == 'yes') {
            $data['bond_info'] = null;
            $data['lease_info'] = null;

        }

        $this->load->view('step_four_view', $data);
    }

    public function insert_lease()
    {
        $property_id = $this->input->post('property_id');
        $lease_id = $this->input->post('lease_id');

        $add_another_lease = 'no';
        if (isset($_REQUEST['add_another_lease'])) {
            $add_another_lease = $_REQUEST['add_another_lease'];
        }

        $data['property_id'] = $this->input->post('property_id');
        $data['lease_name'] = $this->input->post('lease_name');

        $lease_start_date = $this->input->post('lease_start_date');
        $data['lease_start_date'] = date("Y-m-d", strtotime($lease_start_date));

        $lease_end_date = $this->input->post('lease_end_date');
        $data['lease_end_date'] = date("Y-m-d", strtotime($lease_end_date));

        // $data['lease_file'] = $this->input->post('lease_file');
        $data['lease_pay_type'] = $this->input->post('lease_pay_type');
        $data['lease_per_period_payment'] = $this->input->post('lease_per_period_payment');
        $data['lease_pay_day'] = $this->input->post('lease_pay_day');
        $data['lease_current_status'] = 1;

        $get_match_data = $this->property_model->count_result_row('lease', 'property_id=' . $property_id . ' AND lease_id=' . $lease_id . '');

        $this->form_validation->set_rules('lease_name', 'Lease Name', 'required');
        $this->form_validation->set_rules('lease_start_date', 'Lease Start Date', 'required');
        $this->form_validation->set_rules('lease_end_date', 'Lease End Date', 'required');
        $this->form_validation->set_rules('lease_pay_type', 'Lease Pay Type', 'required');
        $this->form_validation->set_rules('lease_per_period_payment', 'Lease Per Period', 'required');
        $this->form_validation->set_rules('lease_pay_day', 'Lease Pay Day', 'required');
        if ($this->form_validation->run() == false) {
            $this->session->set_userdata('insert_stp_four_err', validation_errors());
            redirect('property/step_four/' . $property_id);
        } else {
            if ($get_match_data == 0 && $add_another_lease == 'no') {
                $get_lease_details_id = $this->property_model->insert_ret('lease', $data);

            } else if ($add_another_lease == 'yes') {
                //first deactivate property leases
                $this->property_model->update_with_set_value('lease_current_status', '0', 'property_id', $property_id, 'lease');

                //then insert the new lease
                $get_lease_details_id = $this->property_model->insert_ret('lease', $data);

                //update the new lease as active
                $upd_data['lease_current_status'] = 1;
                $this->property_model->update_active_lease($property_id, $get_lease_details_id, $upd_data);

            } else {
                $this->property_model->update_existing_lease_detail($property_id, $lease_id, $data);
                $get_lease_details_id = $lease_id;
            }
        }

        $lease_detail_data['lease_id'] = $get_lease_details_id;
        $this->property_model->update_lease_detail($property_id, $lease_detail_data);

        if ($_FILES) {
            $files = $_FILES;
            if ($_FILES['lease_file']['name'] != '') {
                $_FILES['userfile']['name'] = uniqid() . '_' . underscore($files['lease_file']['name']);
                $_FILES['userfile']['type'] = $files['lease_file']['type'];
                $_FILES['userfile']['tmp_name'] = $files['lease_file']['tmp_name'];
                $_FILES['userfile']['error'] = $files['lease_file']['error'];
                $_FILES['userfile']['size'] = $files['lease_file']['size'];

                $oldFileName = $_FILES['userfile']['name'];
                $_FILES['userfile']['name'] = str_replace("'", "", $oldFileName);
                $this->upload->initialize($this->set_upload_options($_FILES['userfile']['name'], 'image_and_file/file'));

                if ($this->upload->do_upload()) {
                    $data_image['lease_file'] = $_FILES['userfile']['name'];
                    $this->property_model->update_function('lease_id', $get_lease_details_id, 'lease', $data_image);
                }
            }
        }

        $bond_id = $this->input->post('bond_id');
        $bond_data['bond_amount'] = $this->input->post('bond_amount');
        $lodge_start_date = $this->input->post('lodge_start_date');
        $bond_data['lodge_start_date'] = date("Y-m-d", strtotime($lodge_start_date));
        $bond_data['lodged_with'] = $this->input->post('lodged_with');
        $bond_data['lease_id'] = $get_lease_details_id;
        $bond_data['property_id'] = $property_id;
        $bond_data['user_id'] = $this->session->userdata('user_id');
        if ($bond_id == 0) {
            $this->property_model->insert('bond', $bond_data);
        } else {
            $this->property_model->update_function('bond_id', $bond_id, 'bond', $bond_data);
        }

        $property['step_count'] = 5;
        $this->property_model->update_function('property_id', $property_id, 'property', $property);
        redirect('stepFive/' . $property_id . '/' . $get_lease_details_id . '/front');
    }

    public function step_five($property_id, $lease_id, $flow)
    {
        $data['ul_starting_point'] = 1;
        $data['all_lease_info'] = $this->property_model->select_with_where('*', 'lease_id=' . $lease_id . '', 'lease');
        // echo "<pre>";print_r($data['all_lease_info']);

        if ($data['all_lease_info'][0]['lease_pay_day'] == 01) {
            $day_name = 'Monday';
        }
        if ($data['all_lease_info'][0]['lease_pay_day'] == 02) {
            $day_name = 'Tuesday';
        }
        if ($data['all_lease_info'][0]['lease_pay_day'] == 03) {
            $day_name = 'Wednesday';
        }
        if ($data['all_lease_info'][0]['lease_pay_day'] == 04) {
            $day_name = 'Thursday';
        }
        if ($data['all_lease_info'][0]['lease_pay_day'] == 05) {
            $day_name = 'Friday';
        }
        if ($data['all_lease_info'][0]['lease_pay_day'] == 06) {
            $day_name = 'Saturday';
        }
        if ($data['all_lease_info'][0]['lease_pay_day'] == 07) {
            $day_name = 'Sunday';
        }

        $data['property_id'] = $property_id;
        $data['lease_id'] = $lease_id;
        $data['payment_start_date'] = $data['all_lease_info'][0]['lease_start_date'];
        $data['payment_end_date'] = $data['all_lease_info'][0]['lease_end_date'];
        $payment_amount = $data['all_lease_info'][0]['lease_per_period_payment'];
        if ($flow == 'front') {
            $this->property_model->delete_function_cond('lease_payment_scedule', 'property_id=' . $property_id . ' AND lease_id=' . $lease_id . '');
        }
        if ($data['all_lease_info'][0]['lease_pay_type'] == 1) {
            $data['property_id'] = $property_id;
            $data['lease_id'] = $lease_id;
            $data['payment_start_date'] = $data['all_lease_info'][0]['lease_start_date'];
            $data['payment_end_date'] = $data['all_lease_info'][0]['lease_end_date'];


            $data['paying_duration'] = '';
            $data['paying_time'] = 'every week on a ' . $day_name;

            $partial_amount = $payment_amount / 7;

            $nxt_start_date = array();
            for ($i = 0; $i < 7; $i++) {
                $due_date = date('Y-m-d', strtotime("+" . $i . " day", strtotime($data['payment_start_date'])));
                if (date('l', strtotime($due_date)) == $day_name && $due_date != $data['payment_start_date']) {
                    $nxt_start_date[] = array(
                        'three_start_date' => $data['payment_start_date'],
                        'three_end_date' => date('Y-m-d', strtotime("-1 day", strtotime($due_date))),
                        'payment_due_amount' => (((abs(strtotime($data['payment_start_date']) - strtotime(date('Y-m-d', strtotime("-1 day", strtotime($due_date))))) / 86400) + 1) * $partial_amount)
                    );
                }
            }
            if ($nxt_start_date == null) {
                $nxt_start_date[] = array(
                    'three_start_date' => $data['payment_start_date'],
                    'three_end_date' => date('Y-m-d', strtotime("+6 day", strtotime($data['payment_start_date']))),
                    'payment_due_amount' => $payment_amount
                );
            }
            $period = new DatePeriod(
                new DateTime(date('Y-m-d', strtotime("+1 day", strtotime($nxt_start_date[0]['three_end_date'])))),
                new DateInterval('P1D'),
                new DateTime($data['payment_end_date'])
            );
            $count = 1;
            foreach ($period as $key => $value) {
                if ($key % 7 == 0) {
                    $nxt_start_date[$count] = array(
                        'three_start_date' => $value->format('Y-m-d'),
                        'three_end_date' => date('Y-m-d', strtotime("+6 day", strtotime($value->format('Y-m-d')))),
                        'payment_due_amount' => $payment_amount
                    );
                    $count++;
                }
            }
            foreach ($nxt_start_date as $key => $row) {
                if ($row['three_end_date'] > $data['payment_end_date']) {
                    $nxt_start_date[count($nxt_start_date) - 1]['three_end_date'] = $data['payment_end_date'];
                    $nxt_start_date[count($nxt_start_date) - 1]['payment_due_amount'] = (((abs(strtotime($nxt_start_date[count($nxt_start_date) - 1]['three_start_date']) - strtotime($data['payment_end_date'])) / 86400) + 1) * $partial_amount);
                }
            }
            if ($nxt_start_date[count($nxt_start_date) - 1]['three_end_date'] < $data['payment_end_date']) {
                $nxt_start_date[count($nxt_start_date)] = array(
                    'three_start_date' => date('Y-m-d', strtotime("+1 day", strtotime($value->format('Y-m-d')))),
                    'three_end_date' => $data['payment_end_date'],
                    'payment_due_amount' => (((abs(strtotime(date('Y-m-d', strtotime("+1 day", strtotime($value->format('Y-m-d'))))) - strtotime(date('Y-m-d', strtotime($data['payment_end_date'])))) / 86400) + 1) * $partial_amount)
                );
            }
            foreach ($nxt_start_date as $key => $value) {
                $payment['property_id'] = $property_id;
                $payment['lease_id'] = $lease_id;
                $payment['payment_due_date'] = $value['three_start_date'];
                $payment['payment_start_period'] = $value['three_start_date'];
                $payment['payment_end_period'] = $value['three_end_date'];
                $payment['payment_due_amount'] = $value['payment_due_amount'];
                if ($flow == 'front') {
                    $this->property_model->insert('lease_payment_scedule', $payment);
                }
            }
        } elseif ($data['all_lease_info'][0]['lease_pay_type'] == 2) {
            $data['property_id'] = $property_id;
            $data['lease_id'] = $lease_id;
            $data['payment_start_date'] = $data['all_lease_info'][0]['lease_start_date'];
            $data['payment_end_date'] = $data['all_lease_info'][0]['lease_end_date'];

            $data['paying_duration'] = '';
            $data['paying_time'] = 'every 2 weeks on a ' . $day_name;
            $partial_amount = $payment_amount / 14;

            $nxt_start_date = array();
            for ($i = 0; $i < 7; $i++) {
                $due_date = date('Y-m-d', strtotime("+" . $i . " day", strtotime($data['payment_start_date'])));
                if (date('l', strtotime($due_date)) == $day_name && $due_date != $data['payment_start_date']) {
                    $nxt_start_date[] = array(
                        'three_start_date' => $data['payment_start_date'],
                        'three_end_date' => date('Y-m-d', strtotime("-1 day", strtotime($due_date))),
                        'payment_due_amount' => (((abs(strtotime($data['payment_start_date']) - strtotime(date('Y-m-d', strtotime("-1 day", strtotime($due_date))))) / 86400) + 1) * $partial_amount)
                    );
                }
            }
            if ($nxt_start_date == null) {
                $nxt_start_date[] = array(
                    'three_start_date' => $data['payment_start_date'],
                    'three_end_date' => date('Y-m-d', strtotime("+13 day", strtotime($data['payment_start_date']))),
                    'payment_due_amount' => $payment_amount
                );
            }
            $period = new DatePeriod(
                new DateTime(date('Y-m-d', strtotime("+1 day", strtotime($nxt_start_date[0]['three_end_date'])))),
                new DateInterval('P1D'),
                new DateTime($data['payment_end_date'])
            );
            $count = 1;
            foreach ($period as $key => $value) {
                if ($key % 14 == 0) {
                    $nxt_start_date[$count] = array(
                        'three_start_date' => $value->format('Y-m-d'),
                        'three_end_date' => date('Y-m-d', strtotime("+13 day", strtotime($value->format('Y-m-d')))),
                        'payment_due_amount' => $payment_amount
                    );
                    $count++;
                }
            }
            foreach ($nxt_start_date as $key => $row) {
                if ($row['three_end_date'] > $data['payment_end_date']) {
                    $nxt_start_date[count($nxt_start_date) - 1]['three_end_date'] = $data['payment_end_date'];
                    $nxt_start_date[count($nxt_start_date) - 1]['payment_due_amount'] = (((abs(strtotime($nxt_start_date[count($nxt_start_date) - 1]['three_start_date']) - strtotime($data['payment_end_date'])) / 86400) + 1) * $partial_amount);
                }
            }
            if ($nxt_start_date[count($nxt_start_date) - 1]['three_end_date'] < $data['payment_end_date']) {
                $nxt_start_date[count($nxt_start_date)] = array(
                    'three_start_date' => date('Y-m-d', strtotime("+1 day", strtotime($value->format('Y-m-d')))),
                    'three_end_date' => $nxt_start_date[count($nxt_start_date)]['three_end_date'] = $data['payment_end_date'],
                    'payment_due_amount' => (((abs(strtotime(date('Y-m-d', strtotime("+1 day", strtotime($value->format('Y-m-d'))))) - strtotime(date('Y-m-d', strtotime($data['payment_end_date'])))) / 86400) + 1) * $partial_amount)
                );
            }
            foreach ($nxt_start_date as $key => $value) {
                $payment['property_id'] = $property_id;
                $payment['lease_id'] = $lease_id;
                $payment['payment_due_date'] = $value['three_start_date'];
                $payment['payment_start_period'] = $value['three_start_date'];
                $payment['payment_end_period'] = $value['three_end_date'];
                $payment['payment_due_amount'] = $value['payment_due_amount'];

                if ($flow == 'front') {
                    $this->property_model->insert('lease_payment_scedule', $payment);
                }

            }
        } elseif ($data['all_lease_info'][0]['lease_pay_type'] == 3) {
            $data['property_id'] = $property_id;
            $data['lease_id'] = $lease_id;
            $data['payment_start_date'] = $data['all_lease_info'][0]['lease_start_date'];
            $data['payment_end_date'] = $data['all_lease_info'][0]['lease_end_date'];

            $data['paying_duration'] = '';
            $data['paying_time'] = 'every 4 weeks on a ' . $day_name;
            $partial_amount = $payment_amount / 28;

            $nxt_start_date = array();
            for ($i = 0; $i < 7; $i++) {
                $due_date = date('Y-m-d', strtotime("+" . $i . " day", strtotime($data['payment_start_date'])));
                if (date('l', strtotime($due_date)) == $day_name && $due_date != $data['payment_start_date']) {
                    $nxt_start_date[] = array(
                        'three_start_date' => $data['payment_start_date'],
                        'three_end_date' => date('Y-m-d', strtotime("-1 day", strtotime($due_date))),
                        'payment_due_amount' => (((abs(strtotime($data['payment_start_date']) - strtotime(date('Y-m-d', strtotime("-1 day", strtotime($due_date))))) / 86400) + 1) * $partial_amount)
                    );
                }
            }
            if ($nxt_start_date == null) {
                $nxt_start_date[] = array(
                    'three_start_date' => $data['payment_start_date'],
                    'three_end_date' => date('Y-m-d', strtotime("+27 day", strtotime($data['payment_start_date']))),
                    'payment_due_amount' => $payment_amount
                );
            }

            $period = new DatePeriod(
                new DateTime(date('Y-m-d', strtotime("+1 day", strtotime($nxt_start_date[0]['three_end_date'])))),
                new DateInterval('P1D'),
                new DateTime($data['payment_end_date'])
            );
            $count = 1;
            foreach ($period as $key => $value) {
                if ($key % 28 == 0) {
                    $nxt_start_date[$count] = array(
                        'three_start_date' => $value->format('Y-m-d'),
                        'three_end_date' => date('Y-m-d', strtotime("+27 day", strtotime($value->format('Y-m-d')))),
                        'payment_due_amount' => $payment_amount
                    );
                    $count++;
                }
            }
            foreach ($nxt_start_date as $key => $row) {
                if ($row['three_end_date'] > $data['payment_end_date']) {
                    $nxt_start_date[count($nxt_start_date) - 1]['three_end_date'] = $data['payment_end_date'];
                    $nxt_start_date[count($nxt_start_date) - 1]['payment_due_amount'] = (((abs(strtotime($nxt_start_date[count($nxt_start_date) - 1]['three_start_date']) - strtotime($data['payment_end_date'])) / 86400) + 1) * $partial_amount);
                }
            }
            if ($nxt_start_date[count($nxt_start_date) - 1]['three_end_date'] < $data['payment_end_date']) {
                $nxt_start_date[count($nxt_start_date)] = array(
                    'three_start_date' => date('Y-m-d', strtotime("+1 day", strtotime($value->format('Y-m-d')))),
                    'three_end_date' => $nxt_start_date[count($nxt_start_date)]['three_end_date'] = $data['payment_end_date'],
                    'payment_due_amount' => (((abs(strtotime(date('Y-m-d', strtotime("+1 day", strtotime($value->format('Y-m-d'))))) - strtotime(date('Y-m-d', strtotime($data['payment_end_date'])))) / 86400) + 1) * $partial_amount)
                );
            }
            foreach ($nxt_start_date as $key => $value) {
                $payment['property_id'] = $property_id;
                $payment['lease_id'] = $lease_id;
                $payment['payment_due_date'] = $value['three_start_date'];
                $payment['payment_start_period'] = $value['three_start_date'];
                $payment['payment_end_period'] = $value['three_end_date'];
                $payment['payment_due_amount'] = $value['payment_due_amount'];
                if ($flow == 'front') {
                    $this->property_model->insert('lease_payment_scedule', $payment);
                }
            }
            // echo "<pre>";
            // print_r($nxt_start_date);
            // die();
        } else {
            $data['property_id'] = $property_id;
            $data['lease_id'] = $lease_id;
            $data['payment_start_date'] = $data['all_lease_info'][0]['lease_start_date'];
            $data['payment_end_date'] = $data['all_lease_info'][0]['lease_end_date'];

            $data['paying_duration'] = '';
            $data['paying_time'] = 'every ' . $data['all_lease_info'][0]['lease_pay_day'] . 'th of the month';
            $partial_amount = $payment_amount / 30;

            $period = new DatePeriod(
                new DateTime($data['payment_start_date']),
                new DateInterval('P1D'),
                new DateTime($data['payment_end_date'])
            );
            $pay_date = $data['all_lease_info'][0]['lease_pay_day'];
            // print_r($pay_date);die();
            $all_dates = array();
            foreach ($period as $key => $value) {
                if ($key == 0) {
                    if (date('Y-m-d', strtotime("+30 day", strtotime($value->format('Y-m-d')))) > $value->format('Y-m-' . $pay_date . '')) {
                        if ($value->format('Y-m-d') != $value->format('Y-m-' . $pay_date . '')) {
                            $first_end_date = date('Y-m-d', (strtotime('-1 day', strtotime($value->format('Y-m-' . $pay_date . '')))));
                        } elseif ($value->format('Y-m-d') == $value->format('Y-m-' . $pay_date . '')) {
                            $first_end_date = $first_end_date = date('Y-m-d', strtotime('-1 day', strtotime("+1 month", strtotime($value->format('Y-m-d')))));
                        } else {
                            $first_end_date = date('Y-m-d', strtotime('-1 day', strtotime("+1 month", strtotime($value->format('Y-m-d')))));
                        }
                    } else {
                        $first_end_date = date('Y-m-d', strtotime('-1 day', strtotime("+1 month", strtotime($value->format('Y-m-d')))));
                    }
                    $all_dates[] = array(
                        'start_date' => $value->format('Y-m-d'),
                        'end_date' => $first_end_date,
                        'payment_due_amount' => (((abs(strtotime($value->format('Y-m-d')) - strtotime($first_end_date)) / 86400) + 1) * $partial_amount)
                    );
                }
                if ($key > 0) {
                    if ($value->format('d') == $pay_date) {
                        $all_dates[] = array(
                            'start_date' => $value->format('Y-m-d'),
                            'end_date' => date('Y-m-d', strtotime('-1 day', strtotime("+1 month", strtotime($value->format('Y-m-d'))))),
                            'payment_due_amount' => $payment_amount
                        );
                    }
                }
            }
            foreach ($all_dates as $row) {
                if ($row['end_date'] > $data['payment_end_date']) {
                    $row['end_date'] = $data['payment_end_date'];
                    $row['payment_due_amount'] = (((abs(strtotime($row['start_date']) - strtotime($data['payment_end_date'])) / 86400) + 1) * $partial_amount);
                }
                $payment['property_id'] = $property_id;
                $payment['lease_id'] = $lease_id;
                $payment['payment_due_date'] = $row['start_date'];
                $payment['payment_start_period'] = $row['start_date'];
                $payment['payment_end_period'] = $row['end_date'];
                $payment['payment_due_amount'] = $row['payment_due_amount'];
                if ($flow == 'front') {
                    $this->property_model->insert('lease_payment_scedule', $payment);
                }
            }
        }
        $data['property_id'] = $property_id;
        $data['lease_id'] = $lease_id;

        $data['property'] = $this->property_model->getProperty($property_id);
        $data['lease'] = $this->property_model->getLease($lease_id);

        $data['step_num'] = 'five';
        $data['lease_payment_schedule'] = $this->property_model->get_lease_payment_schedule($property_id, $lease_id);
        $data['bond_info'] = $this->property_model->select_with_where('*', 'property_id=' . $property_id . ' AND lease_id=' . $lease_id . '', 'bond');

        $data['step_five_ul_view'] = $this->load->view('step_five_ul_view', $data, true);
        $this->load->view('step_five_view', $data);
    }

    public function insert_lease_and_rent_details()
    {
        $rows = array();

        $arr_payment_due_date = $this->input->post('input_payment_due_date');
        $arr_lease_payment_period = $this->input->post('input_lease_payment_period');
        $arr_amount = $this->input->post('input_amount');

        $property_id = $this->input->post('property_id');
        $lease_id = $this->input->post('lease_id');

        if (
            $property_id && $lease_id
            &&
            $arr_payment_due_date && $arr_lease_payment_period && $arr_amount
            &&
            (count($arr_payment_due_date) == count($arr_lease_payment_period))
            &&
            count($arr_lease_payment_period) == count($arr_amount)
        ) {

            for ($i = 0; $i < count($arr_payment_due_date); $i++) {
                $row = array();

                $exploded_lease_payment_period = explode("-", $arr_lease_payment_period[$i]);
                $payment_start_period = trim($exploded_lease_payment_period[0]);
                $payment_end_period = trim($exploded_lease_payment_period[1]);

                $row['property_id'] = $property_id;
                $row['lease_id'] = $lease_id;
                $row['payment_due_date'] = date("Y-m-d", strtotime(trim($arr_payment_due_date[$i])));
                $row['payment_start_period'] = date("Y-m-d", strtotime($payment_start_period));
                $row['payment_end_period'] = date("Y-m-d", strtotime($payment_end_period));
                $row['payment_due_amount'] = trim($arr_amount[$i]);

                if (count($row) > 0) {
                    $rows[] = $row;
                }
            }
        }

        if (count($rows) > 0) {
            //first delete, then insert
            $this->property_model->removeLeasePaymentSchedule($property_id, $lease_id);
            $this->property_model->insertPaymentScheduleInBatch($rows);
        }
        redirect('stepSix/' . $property_id . '/' . $lease_id);
    }

    private function getColumnSum($array, $key)
    {
        $sum = array_sum(array_column($array, $key));

        return $sum;
    }

    public function step_six($property_id, $lease_id)
    {
        $data['lease_info'] = $this->property_model->select_with_where('*', 'lease_id=' . $lease_id . ' AND property_id=' . $property_id . '', 'lease');
        $data['get_tenant_list'] = $this->property_model->get_selected_tenant($property_id, $lease_id);

        $current_date = date('Y-m-d');
        $data['arrears_lease_payment_scedule'] = $this->property_model->select_with_where('*', "lease_id={$lease_id}  AND property_id={$property_id} AND payment_due_date<'{$current_date}'  AND (lease_present_status=3 OR lease_present_status=0) ", 'lease_payment_scedule');
        //echo $this->db->last_query();
        $total_arrears = $this->getColumnSum($data['arrears_lease_payment_scedule'], 'payment_due_amount');
        $shared_arrears = $total_arrears / count($data['get_tenant_list']);
        //echo $total_arrears;die();

        if (count($data['get_tenant_list']) == 0) {
            $data['tenant_sharing_amount'] = 0;
        } else {
            $data['tenant_sharing_amount'] = ($data['lease_info'][0]['lease_per_period_payment'] / count($data['get_tenant_list']));
        }
        $data['max_advance_amount'] = $this->calculate_max_advance_amount($property_id, $lease_id);
        $data['max_arrear_amount'] = 1000.00; // calculate this;
        $data['property_id'] = $property_id;
        $data['step_num'] = 'six';
        // echo "<pre>";
        // print_r($data['get_tenant_list']);
        // die();
        //unset( $_SESSION['all_session_data']);
        foreach ($data['get_tenant_list'] as $key => $val) {
            $all_session_data['lease_detail_id'][$key] = $val['lease_detail_id'];
            $all_session_data['payment_status'][$key] = $val['payment_status'];
            $all_session_data['share_paid_amount'][$key] = $data['tenant_sharing_amount'] . '.00';
            $all_session_data['payment_method'][$key] = 2;
            $all_session_data['payment_update_status'][$key] = 4;
            //$all_session_data['payment_update_by'][$key] = $data['tenant_sharing_amount'] . '.00';
            $all_session_data['payment_update_by'][$key] = $shared_arrears;
        }
        $all_session_data['property_id'] = $property_id;
        $all_session_data['lease_id'] = $lease_id;
        $_SESSION['all_session_data'] = $all_session_data;
        // echo "<pre>";
        // print_r($_SESSION);
        // die();
        redirect('property/insert_step_six');

        // $this->load->view('step_six_view', $data);
    }

    public function change_lease_schedule_by_ajax()
    {
        /*echo '<pre>';
        print_r($_POST);
        echo '</pre>';
        //die();*/

        $lease_payment_schedule = array();

        $start_point = date('Y-m-d', strtotime("+1 day", strtotime((string)$this->input->post('start_point'))));
        $end_point = date('Y-m-d', strtotime((string)$this->input->post('end_point')));

        $data['ul_starting_point'] = (int)$this->input->post('ul_starting_point');
        $property_id = (int)$this->input->post('property_id');
        $lease_id = (int)$this->input->post('lease_id');

        $flow = 'ajax';

        $data['all_lease_info'] = $this->property_model->select_with_where('*', 'lease_id=' . $lease_id . '', 'lease');
        // echo "<pre>";print_r($data['all_lease_info']);
        if ($data['all_lease_info'][0]['lease_pay_day'] == 01) {
            $day_name = 'Monday';
        }
        if ($data['all_lease_info'][0]['lease_pay_day'] == 02) {
            $day_name = 'Tuesday';
        }
        if ($data['all_lease_info'][0]['lease_pay_day'] == 03) {
            $day_name = 'Wednesday';
        }
        if ($data['all_lease_info'][0]['lease_pay_day'] == 04) {
            $day_name = 'Thursday';
        }
        if ($data['all_lease_info'][0]['lease_pay_day'] == 05) {
            $day_name = 'Friday';
        }
        if ($data['all_lease_info'][0]['lease_pay_day'] == 06) {
            $day_name = 'Saturday';
        }
        if ($data['all_lease_info'][0]['lease_pay_day'] == 07) {
            $day_name = 'Sunday';
        }

        $data['property_id'] = $property_id;
        $data['lease_id'] = $lease_id;
        $data['payment_start_date'] = $start_point;
        $data['payment_end_date'] = $end_point;
        $payment_amount = $data['all_lease_info'][0]['lease_per_period_payment'];
        /*if ($flow == 'front') {
            $this->property_model->delete_function_cond('lease_payment_scedule', 'property_id=' . $property_id . ' AND lease_id=' . $lease_id . '');
        }*/
        if ($data['all_lease_info'][0]['lease_pay_type'] == 1) {
            $data['property_id'] = $property_id;
            $data['lease_id'] = $lease_id;
            $data['payment_start_date'] = $start_point;
            $data['payment_end_date'] = $end_point;


            $data['paying_duration'] = '1 week';
            $data['paying_time'] = $day_name;

            $partial_amount = $payment_amount / 7;

            $nxt_start_date = array();
            /*for ($i = 0; $i < 7; $i++) {
                $due_date = date('Y-m-d', strtotime("+" . $i . " day", strtotime($data['payment_start_date'])));
                if (date('l', strtotime($due_date)) == $day_name && $due_date != $data['payment_start_date']) {
                    $nxt_start_date[] = array(
                        'three_start_date' => $data['payment_start_date'],
                        'three_end_date' => date('Y-m-d', strtotime("-1 day", strtotime($due_date))),
                        'payment_due_amount' => (((abs(strtotime($data['payment_start_date']) - strtotime(date('Y-m-d', strtotime("-1 day", strtotime($due_date))))) / 86400) + 1) * $partial_amount)
                    );
                }
            }*/
            if ($nxt_start_date == null) {
                $nxt_start_date[] = array(
                    'three_start_date' => $data['payment_start_date'],
                    'three_end_date' => date('Y-m-d', strtotime("+6 day", strtotime($data['payment_start_date']))),
                    'payment_due_amount' => $payment_amount
                );
            }
            $period = new DatePeriod(
                new DateTime(date('Y-m-d', strtotime("+1 day", strtotime($nxt_start_date[0]['three_end_date'])))),
                new DateInterval('P1D'),
                new DateTime($data['payment_end_date'])
            );
            $count = 1;
            foreach ($period as $key => $value) {
                if ($key % 7 == 0) {
                    $nxt_start_date[$count] = array(
                        'three_start_date' => $value->format('Y-m-d'),
                        'three_end_date' => date('Y-m-d', strtotime("+6 day", strtotime($value->format('Y-m-d')))),
                        'payment_due_amount' => $payment_amount
                    );
                    $count++;
                }
            }
            foreach ($nxt_start_date as $key => $row) {
                if ($row['three_end_date'] > $data['payment_end_date']) {
                    $nxt_start_date[count($nxt_start_date) - 1]['three_end_date'] = $data['payment_end_date'];
                    $nxt_start_date[count($nxt_start_date) - 1]['payment_due_amount'] = (((abs(strtotime($nxt_start_date[count($nxt_start_date) - 1]['three_start_date']) - strtotime($data['payment_end_date'])) / 86400) + 1) * $partial_amount);
                }
            }
            if ($nxt_start_date[count($nxt_start_date) - 1]['three_end_date'] < $data['payment_end_date']) {
                $nxt_start_date[count($nxt_start_date)] = array(
                    'three_start_date' => date('Y-m-d', strtotime("+1 day", strtotime($value->format('Y-m-d')))),
                    'three_end_date' => $data['payment_end_date'],
                    'payment_due_amount' => (((abs(strtotime(date('Y-m-d', strtotime("+1 day", strtotime($value->format('Y-m-d'))))) - strtotime(date('Y-m-d', strtotime($data['payment_end_date'])))) / 86400) + 1) * $partial_amount)
                );
            }
            foreach ($nxt_start_date as $key => $value) {
                $payment['property_id'] = $property_id;
                $payment['lease_id'] = $lease_id;
                $payment['payment_due_date'] = $value['three_start_date'];
                $payment['payment_start_period'] = $value['three_start_date'];
                $payment['payment_end_period'] = $value['three_end_date'];
                $payment['payment_due_amount'] = number_format($value['payment_due_amount'], 2, '.', '');
                if ($flow == 'ajax') {
                    //$this->property_model->insert('lease_payment_scedule', $payment);
                    $lease_payment_schedule[] = $payment;
                }
            }
        } elseif ($data['all_lease_info'][0]['lease_pay_type'] == 2) {
            $data['property_id'] = $property_id;
            $data['lease_id'] = $lease_id;
            $data['payment_start_date'] = $start_point;
            $data['payment_end_date'] = $end_point;

            $data['paying_duration'] = '2 weeks';
            $data['paying_time'] = $day_name . ' of every alternative week';
            $partial_amount = $payment_amount / 14;

            $nxt_start_date = array();
            /*for ($i = 0; $i < 7; $i++) {
                $due_date = date('Y-m-d', strtotime("+" . $i . " day", strtotime($data['payment_start_date'])));
                if (date('l', strtotime($due_date)) == $day_name && $due_date != $data['payment_start_date']) {
                    $nxt_start_date[] = array(
                        'three_start_date' => $data['payment_start_date'],
                        'three_end_date' => date('Y-m-d', strtotime("-1 day", strtotime($due_date))),
                        'payment_due_amount' => (((abs(strtotime($data['payment_start_date']) - strtotime(date('Y-m-d', strtotime("-1 day", strtotime($due_date))))) / 86400) + 1) * $partial_amount)
                    );
                }
            }*/
            if ($nxt_start_date == null) {
                $nxt_start_date[] = array(
                    'three_start_date' => $data['payment_start_date'],
                    'three_end_date' => date('Y-m-d', strtotime("+13 day", strtotime($data['payment_start_date']))),
                    'payment_due_amount' => $payment_amount
                );
            }
            $period = new DatePeriod(
                new DateTime(date('Y-m-d', strtotime("+1 day", strtotime($nxt_start_date[0]['three_end_date'])))),
                new DateInterval('P1D'),
                new DateTime($data['payment_end_date'])
            );
            $count = 1;
            foreach ($period as $key => $value) {
                if ($key % 14 == 0) {
                    $nxt_start_date[$count] = array(
                        'three_start_date' => $value->format('Y-m-d'),
                        'three_end_date' => date('Y-m-d', strtotime("+13 day", strtotime($value->format('Y-m-d')))),
                        'payment_due_amount' => $payment_amount
                    );
                    $count++;
                }
            }
            foreach ($nxt_start_date as $key => $row) {
                if ($row['three_end_date'] > $data['payment_end_date']) {
                    $nxt_start_date[count($nxt_start_date) - 1]['three_end_date'] = $data['payment_end_date'];
                    $nxt_start_date[count($nxt_start_date) - 1]['payment_due_amount'] = (((abs(strtotime($nxt_start_date[count($nxt_start_date) - 1]['three_start_date']) - strtotime($data['payment_end_date'])) / 86400) + 1) * $partial_amount);
                }
            }
            if ($nxt_start_date[count($nxt_start_date) - 1]['three_end_date'] < $data['payment_end_date']) {
                $nxt_start_date[count($nxt_start_date)] = array(
                    'three_start_date' => date('Y-m-d', strtotime("+1 day", strtotime($value->format('Y-m-d')))),
                    'three_end_date' => $nxt_start_date[count($nxt_start_date)]['three_end_date'] = $data['payment_end_date'],
                    'payment_due_amount' => (((abs(strtotime(date('Y-m-d', strtotime("+1 day", strtotime($value->format('Y-m-d'))))) - strtotime(date('Y-m-d', strtotime($data['payment_end_date'])))) / 86400) + 1) * $partial_amount)
                );
            }
            foreach ($nxt_start_date as $key => $value) {
                $payment['property_id'] = $property_id;
                $payment['lease_id'] = $lease_id;
                $payment['payment_due_date'] = $value['three_start_date'];
                $payment['payment_start_period'] = $value['three_start_date'];
                $payment['payment_end_period'] = $value['three_end_date'];
                $payment['payment_due_amount'] = number_format($value['payment_due_amount'], 2, '.', '');
                if ($flow == 'ajax') {
                    //$this->property_model->insert('lease_payment_scedule', $payment);
                    $lease_payment_schedule[] = $payment;
                }
            }
        } elseif ($data['all_lease_info'][0]['lease_pay_type'] == 3) {
            $data['property_id'] = $property_id;
            $data['lease_id'] = $lease_id;
            $data['payment_start_date'] = $start_point;
            $data['payment_end_date'] = $end_point;

            $data['paying_duration'] = '4 weeks';
            $data['paying_time'] = 'a ' . $day_name;
            $partial_amount = $payment_amount / 28;

            $nxt_start_date = array();
            /*for ($i = 0; $i < 7; $i++) {
                $due_date = date('Y-m-d', strtotime("+" . $i . " day", strtotime($data['payment_start_date'])));
                if (date('l', strtotime($due_date)) == $day_name && $due_date != $data['payment_start_date']) {
                    $nxt_start_date[] = array(
                        'three_start_date' => $data['payment_start_date'],
                        'three_end_date' => date('Y-m-d', strtotime("-1 day", strtotime($due_date))),
                        'payment_due_amount' => (((abs(strtotime($data['payment_start_date']) - strtotime(date('Y-m-d', strtotime("-1 day", strtotime($due_date))))) / 86400) + 1) * $partial_amount)
                    );
                }
            }*/
            if ($nxt_start_date == null) {
                $nxt_start_date[] = array(
                    'three_start_date' => $data['payment_start_date'],
                    'three_end_date' => date('Y-m-d', strtotime("+27 day", strtotime($data['payment_start_date']))),
                    'payment_due_amount' => $payment_amount
                );
            }

            $period = new DatePeriod(
                new DateTime(date('Y-m-d', strtotime("+1 day", strtotime($nxt_start_date[0]['three_end_date'])))),
                new DateInterval('P1D'),
                new DateTime($data['payment_end_date'])
            );
            $count = 1;
            foreach ($period as $key => $value) {
                if ($key % 28 == 0) {
                    $nxt_start_date[$count] = array(
                        'three_start_date' => $value->format('Y-m-d'),
                        'three_end_date' => date('Y-m-d', strtotime("+27 day", strtotime($value->format('Y-m-d')))),
                        'payment_due_amount' => $payment_amount
                    );
                    $count++;
                }
            }
            foreach ($nxt_start_date as $key => $row) {
                if ($row['three_end_date'] > $data['payment_end_date']) {
                    $nxt_start_date[count($nxt_start_date) - 1]['three_end_date'] = $data['payment_end_date'];
                    $nxt_start_date[count($nxt_start_date) - 1]['payment_due_amount'] = (((abs(strtotime($nxt_start_date[count($nxt_start_date) - 1]['three_start_date']) - strtotime($data['payment_end_date'])) / 86400) + 1) * $partial_amount);
                }
            }
            if ($nxt_start_date[count($nxt_start_date) - 1]['three_end_date'] < $data['payment_end_date']) {
                $nxt_start_date[count($nxt_start_date)] = array(
                    'three_start_date' => date('Y-m-d', strtotime("+1 day", strtotime($value->format('Y-m-d')))),
                    'three_end_date' => $nxt_start_date[count($nxt_start_date)]['three_end_date'] = $data['payment_end_date'],
                    'payment_due_amount' => (((abs(strtotime(date('Y-m-d', strtotime("+1 day", strtotime($value->format('Y-m-d'))))) - strtotime(date('Y-m-d', strtotime($data['payment_end_date'])))) / 86400) + 1) * $partial_amount)
                );
            }
            foreach ($nxt_start_date as $key => $value) {
                $payment['property_id'] = $property_id;
                $payment['lease_id'] = $lease_id;
                $payment['payment_due_date'] = $value['three_start_date'];
                $payment['payment_start_period'] = $value['three_start_date'];
                $payment['payment_end_period'] = $value['three_end_date'];
                $payment['payment_due_amount'] = number_format($value['payment_due_amount'], 2, '.', '');
                if ($flow == 'ajax') {
                    //$this->property_model->insert('lease_payment_scedule', $payment);
                    $lease_payment_schedule[] = $payment;
                }
            }
            // echo "<pre>";
            // print_r($nxt_start_date);
            // die();
        } else {
            $data['property_id'] = $property_id;
            $data['lease_id'] = $lease_id;
            $data['payment_start_date'] = $start_point;
            $data['payment_end_date'] = $end_point;

            $data['paying_duration'] = '1 month';
            $data['paying_time'] = $data['all_lease_info'][0]['lease_pay_day'] . 'th of the month';
            $partial_amount = $payment_amount / 30;

            $period = new DatePeriod(
                new DateTime($data['payment_start_date']),
                new DateInterval('P1D'),
                new DateTime($data['payment_end_date'])
            );
            $pay_date = $data['all_lease_info'][0]['lease_pay_day'];
            // print_r($pay_date);die();
            $all_dates = array();
            foreach ($period as $key => $value) {
                /*if ($key == 0) {
                    if (date('Y-m-d', strtotime("+30 day", strtotime($value->format('Y-m-d')))) > $value->format('Y-m-' . $pay_date . '')) {
                        if ($value->format('Y-m-d') != $value->format('Y-m-' . $pay_date . '')) {
                            $first_end_date = date('Y-m-d', (strtotime('-1 day', strtotime($value->format('Y-m-' . $pay_date . '')))));
                        } elseif ($value->format('Y-m-d') == $value->format('Y-m-' . $pay_date . '')) {
                            $first_end_date = $first_end_date = date('Y-m-d', strtotime('-1 day', strtotime("+1 month", strtotime($value->format('Y-m-d')))));
                        } else {
                            $first_end_date = date('Y-m-d', strtotime('-1 day', strtotime("+1 month", strtotime($value->format('Y-m-d')))));
                        }
                    } else {
                        $first_end_date = date('Y-m-d', strtotime('-1 day', strtotime("+1 month", strtotime($value->format('Y-m-d')))));
                    }
                    $all_dates[] = array(
                        'start_date' => $value->format('Y-m-d'),
                        'end_date' => $first_end_date,
                        'payment_due_amount' => (((abs(strtotime($value->format('Y-m-d')) - strtotime($first_end_date)) / 86400) + 1) * $partial_amount)
                    );
                }*/
                if ($key > 0) { //$key > 0
                    if ($value->format('d') == $pay_date) {
                        $all_dates[] = array(
                            'start_date' => $value->format('Y-m-d'),
                            'end_date' => date('Y-m-d', strtotime('-1 day', strtotime("+1 month", strtotime($value->format('Y-m-d'))))),
                            'payment_due_amount' => $payment_amount
                        );
                    }
                }
            }
            foreach ($all_dates as $row) {
                if ($row['end_date'] > $data['payment_end_date']) {
                    $row['end_date'] = $data['payment_end_date'];
                    $row['payment_due_amount'] = (((abs(strtotime($row['start_date']) - strtotime($data['payment_end_date'])) / 86400) + 1) * $partial_amount);
                }
                $payment['property_id'] = $property_id;
                $payment['lease_id'] = $lease_id;
                $payment['payment_due_date'] = $row['start_date'];
                $payment['payment_start_period'] = $row['start_date'];
                $payment['payment_end_period'] = $row['end_date'];
                $payment['payment_due_amount'] = number_format($value['payment_due_amount'], 2, '.', '');
                if ($flow == 'ajax') {
                    //$this->property_model->insert('lease_payment_scedule', $payment);
                    $lease_payment_schedule[] = $payment;
                }
            }
        }
        $data['property_id'] = $property_id;
        $data['lease_id'] = $lease_id;

        $data['property'] = $this->property_model->getProperty($property_id);
        $data['lease'] = $this->property_model->getLease($lease_id);

        $data['step_num'] = 'five';
        //$data['lease_payment_schedule'] = $this->property_model->get_lease_payment_schedule($property_id, $lease_id);

        $data['lease_payment_schedule'] = $lease_payment_schedule;
        $html = $this->load->view('step_five_ul_view', $data, true);
        echo $html;
        die();
    }

    private function calculate_max_advance_amount($property_id, $lease_id)
    {
        $max_advance_amount = 0.00;
        $current_date = date('Y-m-d');
        $payment_schedule_list = $this->property_model->select_with_where('*', 'lease_id=' . $lease_id . ' AND property_id=' . $property_id . '', 'lease_payment_scedule');

        if ($payment_schedule_list) {
            foreach ($payment_schedule_list as $payment) {

                if (strtotime($payment['payment_end_period']) > strtotime($current_date)) {
                    if (is_numeric($payment['payment_due_amount'])) {
                        $max_advance_amount += $payment['payment_due_amount'];
                    }
                }
            }
        }

        return $max_advance_amount;
    }

    public function insert_step_six()
    {
        /*echo "<pre>";
        print_r($_SESSION);
        echo "<pre>";
        die();*/

        $property_id = $_SESSION['all_session_data']['property_id'];
        $lease_id = $_SESSION['all_session_data']['lease_id'];
        $lease_detail_id = $_SESSION['all_session_data']['lease_detail_id'];
        $payment_status = $_SESSION['all_session_data']['payment_status'];
        $share_paid_amount = $_SESSION['all_session_data']['share_paid_amount'];
        $payment_method = $_SESSION['all_session_data']['payment_method'];
        $payment_update_status = $_SESSION['all_session_data']['payment_update_status'];
        $payment_update_by = $_SESSION['all_session_data']['payment_update_by'];

        for ($i = 0; $i < count($payment_status); $i++) {
            $payment_status[$i] = 1;
            if ($payment_status[$i] == 1) {
                $data = array();
                $data['payment_status'] = 1;
                $data['share_paid_amount'] = $share_paid_amount[$i];
                $data['payment_method'] = $payment_method[$i];
                $data['payment_update_status'] = $payment_update_status[$i];
                // $data['payment_update_status'] = $payment_update_status[$i];
                if ($payment_update_status[$i] == 2 || $payment_update_status[$i] == 3) {
                    $data['payment_update_by'] = $payment_update_by[$i];
                } elseif ($payment_update_status[$i] == 4) {
                    //$data['payment_update_by'] = $share_paid_amount[$i];
                    $data['payment_update_by'] = $payment_update_by[$i];
                } else {
                    $data['payment_update_by'] = 0;
                }
                $this->property_model->update_tenant_infos($lease_detail_id[$i], $property_id, $lease_id, $data);
            } else {
                $data = array();
                $data['payment_status'] = 0;
                $this->property_model->update_tenant_infos($lease_detail_id[$i], $property_id, $lease_id, $data);
            }
        }

        $property_step['step_count'] = 7;
        $this->property_model->update_function('property_id', $property_id, 'property', $property_step);
        // die();
        // unset($_SESSION['all_session_data']);
        redirect('property/update_final_schedule/' . $property_id . '/' . $lease_id);
    }

    public function rent_schedule($property_id)
    {
        $data['property_id'] = $property_id;
        $property['step_count'] = 6;
        $this->property_model->update_function('property_id', $property_id, 'property', $property);
        $this->load->view('rent_schedule_view', $data);
    }

    public function update_final_schedule($property_id, $lease_id)
    {

        $data['property_info'] = $this->rent_model->select_with_where('*', 'property_id=' . $property_id . '', 'property');
        $data['lease_info'] = $this->property_model->select_with_where('*', 'lease_id=' . $lease_id . ' AND property_id=' . $property_id . '', 'lease');
        $lease_paying_type = $data['lease_info'][0]['lease_pay_type'];
        if ($lease_paying_type == 1) {
            $lease_paying_type = 7;
        } elseif ($lease_paying_type == 2) {
            $lease_paying_type = 14;
        } elseif ($lease_paying_type == 3) {
            $lease_paying_type = 28;
        } else {
            $lease_paying_type = 30;
        }
        $data['all_payment_info'] = $this->property_model->select_condition_order('lease_payment_scedule', 'lease_id=' . $lease_id . ' AND property_id=' . $property_id . '', 'lease_payment_scedule.payment_schedule_id', 'DESC');

        $data['get_tenant_list'] = $this->property_model->get_specific_selected_tenant($property_id, $lease_id);
        foreach ($data['get_tenant_list'] as $key => $get_value) {
            $this->property_model->delete_function_cond('lease_tenant_payment_scedhule', 'tenant_id=' . $get_value['tenant_id'] . ' AND lease_id = ' . $get_value['lease_id'] . ' AND property_id = ' . $get_value['property_id'] . '');
        }
        $one_user_amount = 0;
        $one_user_amount_upcome = 0;
        foreach ($data['get_tenant_list'] as $key => $get_value) {
            foreach ($data['all_payment_info'] as $key => $value) {
                $value['lease_rent_recieved'] = 0;
            }
            if ($get_value['payment_update_status'] == 1) {
                $one_user_amount = ($one_user_amount + ($get_value['share_paid_amount'] / $lease_paying_type) * ((abs(strtotime($data['all_payment_info'][$key]['payment_start_period']) - strtotime($data['all_payment_info'][$key]['payment_end_period'])) / 86400) + 1));
            }
        }
        foreach ($data['get_tenant_list'] as $key => $get_value) {
            foreach ($data['all_payment_info'] as $key => $value) {
                if ($value['payment_start_period'] <= date('Y-m-d')) {
                    //$data['all_payment_info'][$key]['lease_rent_recieved'] = $data['all_payment_info'][$key]['payment_due_amount'];  //it was $data['all_payment_info'][$key]['payment_due_amount'] before, Then made 0 ;
                    //$data['all_payment_info'][$key]['lease_present_status'] = 1; //it was 1 before, Then made 3

                    $data['all_payment_info'][$key]['lease_rent_recieved'] = 0;  //it was $data['all_payment_info'][$key]['payment_due_amount'] before, Then made 0 ;
                    $data['all_payment_info'][$key]['lease_present_status'] = 3; //it was 1 before, Then made 3
                    if ($get_value['payment_update_status'] == 1) {
                        $t_data['tenant_id'] = $get_value['tenant_id'];
                        $t_data['lease_id'] = $get_value['lease_id'];
                        $t_data['property_id'] = $get_value['property_id'];
                        $t_data['schedule_start_date'] = $value['payment_start_period'];
                        $t_data['schedule_end_date'] = $value['payment_end_period'];
                        $t_data['shared_amount'] = (($get_value['share_paid_amount'] / $lease_paying_type) * ((abs(strtotime($data['all_payment_info'][$key]['payment_start_period']) - strtotime($data['all_payment_info'][$key]['payment_end_period'])) / 86400) + 1));
                        $t_data['payment_status'] = 1;
                        $t_data['payment_method'] = $get_value['payment_method'];
                        $t_data['payment_for'] = 1;
                        $this->property_model->insert('lease_tenant_payment_scedhule', $t_data);
                    }
                }
                if ($value['payment_start_period'] > date('Y-m-d')) {
                    $data['all_payment_info'][$key]['lease_rent_recieved'] = 0;
                    $data['all_payment_info'][$key]['lease_present_status'] = 2;
                }
            }
        }
        //die();
        foreach ($data['get_tenant_list'] as $key => $get_value) {
            if ($get_value['payment_update_status'] == 2) {
                foreach ($data['all_payment_info'] as $key => $value) {
                    if ($value['payment_start_period'] <= date('Y-m-d')) {
                        $get_value['share_paid_amount'] = (($get_value['share_paid_amount'] / $lease_paying_type) * ((abs(strtotime($data['all_payment_info'][$key]['payment_start_period']) - strtotime($data['all_payment_info'][$key]['payment_end_period'])) / 86400) + 1));
                        if ($get_value['payment_update_by'] > 0) {
                            if ($get_value['payment_update_by'] > $get_value['share_paid_amount']) {
                                //$data['all_payment_info'][$key]['lease_rent_recieved'] = $data['all_payment_info'][$key]['lease_rent_recieved'] - $get_value['share_paid_amount'];
                                $data['all_payment_info'][$key]['lease_rent_recieved'] = 0;
                                $data['all_payment_info'][$key]['lease_present_status'] = 3;
                            } else {
                                //$data['all_payment_info'][$key]['lease_rent_recieved'] = $data['all_payment_info'][$key]['lease_rent_recieved'] - $get_value['payment_update_by'];
                                $data['all_payment_info'][$key]['lease_rent_recieved'] = 0;
                                $data['all_payment_info'][$key]['lease_present_status'] = 3;
                            }
                        }
                        if ($get_value['payment_update_status'] == 2) {
                            $t_data['tenant_id'] = $get_value['tenant_id'];
                            $t_data['lease_id'] = $get_value['lease_id'];
                            $t_data['property_id'] = $get_value['property_id'];
                            $t_data['schedule_start_date'] = $value['payment_start_period'];
                            $t_data['schedule_end_date'] = $value['payment_end_period'];
                            $t_data['shared_amount'] = (($get_value['share_paid_amount'] / $lease_paying_type) * ((abs(strtotime($data['all_payment_info'][$key]['payment_start_period']) - strtotime($data['all_payment_info'][$key]['payment_end_period'])) / 86400) + 1));
                            $t_data['payment_status'] = 2;
                            $t_data['payment_method'] = $get_value['payment_method'];
                            $t_data['payment_for'] = 1;
                            $this->property_model->insert('lease_tenant_payment_scedhule', $t_data);
                        }

                        $get_value['payment_update_by'] = $get_value['payment_update_by'] - $get_value['share_paid_amount'];

                    }
                    // if ($value['payment_start_period']>date('Y-m-d'))
                    // {
                    //     $data['all_payment_info'][$key]['lease_rent_recieved'] = 0;
                    //     $data['all_payment_info'][$key]['lease_present_status'] = 2;
                    // }
                }
            }
        }
        //die();
        foreach ($data['get_tenant_list'] as $get_value) {
            if ($get_value['payment_update_status'] == 4) {
                $start_here = 1;
                foreach ($data['all_payment_info'] as $key => $value) {
                    if ($value['payment_start_period'] <= date('Y-m-d')) {
                        $get_value['share_paid_amount'] = (($get_value['share_paid_amount'] / $lease_paying_type) * ((abs(strtotime($data['all_payment_info'][$key]['payment_start_period']) - strtotime($data['all_payment_info'][$key]['payment_end_period'])) / 86400) + 1));
                        if ($get_value['payment_update_by'] > 0) {

                            // if condition is mahmud-added , segment inside else were original
                            if ($value['payment_end_period'] > date('Y-m-d')) {
                                $data['all_payment_info'][$key]['lease_rent_recieved'] = 0;
                                $data['all_payment_info'][$key]['lease_present_status'] = 3;
                            } else {
                                //$data['all_payment_info'][$key]['lease_rent_recieved'] = $data['all_payment_info'][$key]['lease_rent_recieved'] - $get_value['share_paid_amount']; //problem here ?
                                $data['all_payment_info'][$key]['lease_rent_recieved'] = 0; //problem here ?
                                $data['all_payment_info'][$key]['lease_present_status'] = 3;
                            }

                        }
                        if ($key == 1) {
                            if ($get_value['payment_update_status'] == 4) {
                                if ($start_here == 1) {
                                    $t_data['tenant_id'] = $get_value['tenant_id'];
                                    $t_data['lease_id'] = $get_value['lease_id'];
                                    $t_data['property_id'] = $get_value['property_id'];
                                    $t_data['schedule_start_date'] = $value['payment_start_period'];
                                    $t_data['schedule_end_date'] = $value['payment_end_period'];
                                    $t_data['shared_amount'] = $get_value['share_paid_amount'];
                                    $t_data['payment_status'] = 4;
                                    $t_data['payment_method'] = $get_value['payment_method'];
                                    $t_data['payment_for'] = 1;
                                    $this->property_model->insert('lease_tenant_payment_scedhule', $t_data);
                                }
                            }
                            $start_here++;
                        }
                        $get_value['payment_update_by'] = $get_value['payment_update_by'] - $get_value['share_paid_amount'];
                        //echo "**{$get_value['payment_update_by']}**";
                    }
                    if ($value['payment_start_period'] > date('Y-m-d')) {
                        $data['all_payment_info'][$key]['lease_rent_recieved'] = 0;
                        $data['all_payment_info'][$key]['lease_present_status'] = 2;
                    }
                }
            }
        }
        foreach ($data['all_payment_info'] as $key => $value) {
            $payment['lease_rent_recieved'] = $value['lease_rent_recieved'];
            $payment['lease_present_status'] = $value['lease_present_status'];
            $this->property_model->update_function('payment_schedule_id', $value['payment_schedule_id'], 'lease_payment_scedule', $payment);
        }
        $data['all_payment_infos'] = $this->property_model->select_condition_order('lease_payment_scedule', 'lease_id=' . $lease_id . ' AND property_id=' . $property_id . '', 'lease_payment_scedule.payment_schedule_id', 'ASC');

        foreach ($data['get_tenant_list'] as $key => $get_value) {
            if ($get_value['payment_update_status'] == 3 || $get_value['payment_update_status'] == 2) {
                foreach ($data['all_payment_infos'] as $key => $value) {
                    if ($value['payment_start_period'] > date('Y-m-d')) {
                        $data['all_payment_infos'][$key]['lease_rent_recieved'] = 0;
                    }
                }
            }
        }
        foreach ($data['get_tenant_list'] as $key => $get_value) {
            if ($get_value['payment_update_status'] == 3) {
                foreach ($data['all_payment_infos'] as $key => $value) {
                    if ($value['payment_start_period'] > date('Y-m-d')) {
                        $get_value['share_paid_amount'] = (($get_value['share_paid_amount'] / $lease_paying_type) * ((abs(strtotime($data['all_payment_infos'][$key]['payment_start_period']) - strtotime($data['all_payment_infos'][$key]['payment_end_period'])) / 86400) + 1));
                        // $get_value['payment_update_by'] = (($get_value['payment_update_by']/$lease_paying_type)*((abs(strtotime($data['all_payment_infos'][$key]['payment_start_period'])-strtotime($data['all_payment_infos'][$key]['payment_end_period']))/86400)+1));
                        if ($get_value['payment_update_by'] > 0) {
                            if ($get_value['payment_update_by'] > $get_value['share_paid_amount']) {
                                $data['all_payment_infos'][$key]['lease_rent_recieved'] = $data['all_payment_infos'][$key]['lease_rent_recieved'] + $get_value['share_paid_amount'];
                                if ($data['all_payment_infos'][$key]['lease_rent_recieved'] == $data['all_payment_infos'][$key]['payment_due_amount']) {
                                    $data['all_payment_infos'][$key]['lease_present_status'] = 1;
                                } else {
                                    $data['all_payment_infos'][$key]['lease_present_status'] = 2;
                                }
                            } else {
                                $data['all_payment_infos'][$key]['lease_rent_recieved'] = $data['all_payment_infos'][$key]['lease_rent_recieved'] + $get_value['payment_update_by'];
                                if ($data['all_payment_infos'][$key]['lease_rent_recieved'] == $data['all_payment_infos'][$key]['payment_due_amount']) {
                                    $data['all_payment_infos'][$key]['lease_present_status'] = 1;
                                } else {
                                    $data['all_payment_infos'][$key]['lease_present_status'] = 2;
                                }
                            }
                            if ($get_value['payment_update_status'] == 3) {
                                $t_data['tenant_id'] = $get_value['tenant_id'];
                                $t_data['lease_id'] = $get_value['lease_id'];
                                $t_data['property_id'] = $get_value['property_id'];
                                $t_data['schedule_start_date'] = $value['payment_start_period'];
                                $t_data['schedule_end_date'] = $value['payment_end_period'];
                                $t_data['shared_amount'] = $data['all_payment_infos'][$key]['lease_rent_recieved'];
                                $t_data['payment_status'] = 3;
                                $t_data['payment_method'] = $get_value['payment_method'];
                                $t_data['payment_for'] = 1;
                                $this->property_model->insert('lease_tenant_payment_scedhule', $t_data);
                            }
                        }
                        $get_value['payment_update_by'] = $get_value['payment_update_by'] - $get_value['share_paid_amount'];
                    }
                    if ($get_value['payment_update_by'] < 0) {
                        $data['all_payment_infos'][$key]['lease_rent_recieved'] = $data['all_payment_infos'][$key]['lease_rent_recieved'];
                        $data['all_payment_infos'][$key]['lease_present_status'] = 2;
                    }
                }
            }
        }
        foreach ($data['all_payment_infos'] as $key => $values) {
            $payments['lease_rent_recieved'] = $values['lease_rent_recieved'];
            $payments['lease_present_status'] = $values['lease_present_status'];
            $this->property_model->update_function('payment_schedule_id', $values['payment_schedule_id'], 'lease_payment_scedule', $payments);
        }

        //die();
        /*echo "<pre>";
        print_r($data);
        echo "</pre>";
        die();*/
        $data['property_id'] = $property_id;
        $data['lease_id'] = $lease_id;
        $data['step_count'] = 7;
        redirect('confirmSchedule/' . $property_id . '/' . $lease_id);
    }

    public function confirmSchedule($property_id, $lease_id)
    {
        $data['property_info'] = $this->rent_model->select_with_where('*', 'property_id=' . $property_id . '', 'property');
        $data['lease_info'] = $this->property_model->select_with_where('*', 'lease_id=' . $lease_id . ' AND property_id=' . $property_id . '', 'lease');
        $data['get_tenant_list'] = $this->property_model->get_specific_selected_tenant($property_id, $lease_id);

        $data['all_payments_info'] = $this->property_model->select_condition_order('lease_payment_scedule', 'lease_id=' . $lease_id . ' AND property_id=' . $property_id . '', 'lease_payment_scedule.payment_schedule_id', 'DESC');
        $data['all_tenant_payments_info'] = $this->property_model->select_condition_order_and_join($lease_id, $property_id);
        $data['property_id'] = $property_id;
        $data['lease_id'] = $lease_id;
        $data['step_count'] = 7;

        //$this->insertFirstInspection($property_id, $lease_id);

        redirect('Dashboard/' . $property_id); //mahmud edited here
        $this->load->view('confirm_payment', $data);
    }

    private function insertFirstInspection($property_id, $lease_id)
    {
        $lease = $this->property_model->select_row_with_where('*', "lease_id={$lease_id}", 'lease');
        $inspection_insertable = false;

        $gap = '90'; //3 months

        $gap_after_lease_started = '';
        if ($lease) {
            $gap_after_lease_started = date('Y-m-d', strtotime("+{$gap} days", strtotime(($lease['lease_start_date']))));
            if ($gap_after_lease_started > date('Y-m-d')
                &&
                $gap_after_lease_started < $lease['lease_end_date']) {
                $inspection_insertable = true;
            }
        }

        if ($inspection_insertable) {

            $inspection_data['inspection_title'] = 'Your first inspection';
            $inspection_data['inspection_date'] = $gap_after_lease_started;
            $inspection_data['inspection_start_time'] = '09:00:00';
            $inspection_data['inspection_end_time'] = '11:00:00';
            $inspection_data['inspection_description'] = 'This is your first inspection';
            $inspection_data['property_id'] = $property_id;
            $inspection_data['lease_id'] = $lease_id;
            $inspection_data['inspection_created_at'] = date('Y-m-d H:i:s');
            $inspection_data['inspection_updated_at'] = date('Y-m-d H:i:s');

            $inspection_id = $this->inspection_model->insert_ret('inspection', $inspection_data);
        }


    }



    // public function update_final_schedule_except_rent_recieve($property_id,$lease_id)
    // {
    //     $data['property_info'] = $this->rent_model->select_with_where('*', 'property_id=' . $property_id . '', 'property');
    //     $data['lease_info'] = $this->property_model->select_with_where('*', 'lease_id='.$lease_id.' AND property_id='.$property_id.'', 'lease');
    //     $lease_paying_type = $data['lease_info'][0]['lease_pay_type'];
    //     if($lease_paying_type==1)
    //     {
    //         $lease_paying_type = 7;
    //     }
    //     elseif($lease_paying_type == 2)
    //     {
    //         $lease_paying_type =14;
    //     }
    //     elseif($lease_paying_type == 3)
    //     {
    //         $lease_paying_type = 28;
    //     }
    //     else
    //     {
    //         $lease_paying_type = 30;
    //     }
    //     $data['all_payment_info'] = $this->property_model->select_condition_order('lease_payment_scedule', 'lease_id='.$lease_id.' AND property_id='.$property_id.'', 'lease_payment_scedule.payment_schedule_id','DESC');

    //     $data['get_tenant_list'] = $this->property_model->get_specific_selected_tenant($property_id, $lease_id);
    //     // foreach ($data['get_tenant_list'] as $key => $get_value)
    //     // {
    //         // $this->property_model->delete_function_cond('lease_tenant_payment_scedhule', 'tenant_id='.$get_value['tenant_id'].' AND lease_id = '.$get_value['lease_id'].' AND property_id = '.$get_value['property_id'].'');
    //     // }
    //     $one_user_amount = 0;
    //     $one_user_amount_upcome = 0;
    //     foreach ($data['get_tenant_list'] as $key => $get_value)
    //     {
    //         foreach ($data['all_payment_info'] as $key=>$value)
    //         {
    //             $value['lease_rent_recieved'] = 0;
    //         }
    //         if($get_value['payment_update_status']==1)
    //         {
    //             $one_user_amount = ($one_user_amount+($get_value['share_paid_amount']/$lease_paying_type)*((abs(strtotime($data['all_payment_info'][$key]['payment_start_period'])-strtotime($data['all_payment_info'][$key]['payment_end_period']))/86400)+1));
    //         }
    //     }
    //     foreach ($data['get_tenant_list'] as $key => $get_value)
    //     {
    //         foreach ($data['all_payment_info'] as $key=>$value)
    //         {
    //             if ($value['payment_start_period']<=date('Y-m-d'))
    //             {
    //                 $data['all_payment_info'][$key]['lease_rent_recieved'] = $data['all_payment_info'][$key]['payment_due_amount'];
    //                 $data['all_payment_info'][$key]['lease_present_status'] = 1;
    //                 // if($get_value['payment_update_status']==1)
    //                 // {
    //                     // $t_data['tenant_id'] = $get_value['tenant_id'];
    //                     // $t_data['lease_id'] = $get_value['lease_id'];
    //                     // $t_data['property_id'] = $get_value['property_id'];
    //                     // $t_data['schedule_start_date'] = $value['payment_start_period'];
    //                     // $t_data['schedule_end_date'] = $value['payment_end_period'];
    //                     // $t_data['shared_amount'] = (($get_value['share_paid_amount']/$lease_paying_type)*((abs(strtotime($data['all_payment_info'][$key]['payment_start_period'])-strtotime($data['all_payment_info'][$key]['payment_end_period']))/86400)+1));
    //                     // $t_data['payment_status'] = 1 ;
    //                     // $t_data['payment_method'] = $get_value['payment_method'];
    //                     // $t_data['payment_for']=1;
    //                     // $this->property_model->insert('lease_tenant_payment_scedhule',$t_data);
    //                 // }
    //             }
    //             // if ($value['payment_start_period']>date('Y-m-d'))
    //             // {
    //             //     $data['all_payment_info'][$key]['lease_rent_recieved'] = 0;
    //             //     $data['all_payment_info'][$key]['lease_present_status'] = 2;
    //             // }
    //         }
    //     }
    //     foreach ($data['get_tenant_list'] as $key => $get_value)
    //     {
    //         if($get_value['payment_update_status']==2)
    //         {
    //             foreach ($data['all_payment_info'] as $key=>$value)
    //             {
    //                 if ($value['payment_start_period']<=date('Y-m-d'))
    //                 {
    //                     $get_value['share_paid_amount'] = (($get_value['share_paid_amount']/$lease_paying_type)*((abs(strtotime($data['all_payment_info'][$key]['payment_start_period'])-strtotime($data['all_payment_info'][$key]['payment_end_period']))/86400)+1));
    //                     if($get_value['payment_update_by']>0)
    //                     {
    //                         if($get_value['payment_update_by']>$get_value['share_paid_amount'])
    //                         {
    //                             $data['all_payment_info'][$key]['lease_rent_recieved'] = $data['all_payment_info'][$key]['lease_rent_recieved'] - $get_value['share_paid_amount'];
    //                             $data['all_payment_info'][$key]['lease_present_status'] = 3;
    //                         }
    //                         else
    //                         {
    //                             $data['all_payment_info'][$key]['lease_rent_recieved'] = $data['all_payment_info'][$key]['lease_rent_recieved'] - $get_value['payment_update_by'];
    //                             $data['all_payment_info'][$key]['lease_present_status'] = 3;
    //                         }
    //                     }
    //                     // if($get_value['payment_update_status']==2)
    //                     // {
    //                         // $t_data['tenant_id'] = $get_value['tenant_id'];
    //                         // $t_data['lease_id'] = $get_value['lease_id'];
    //                         // $t_data['property_id'] = $get_value['property_id'];
    //                         // $t_data['schedule_start_date'] = $value['payment_start_period'];
    //                         // $t_data['schedule_end_date'] = $value['payment_end_period'];
    //                         // $t_data['shared_amount'] = (($get_value['share_paid_amount']/$lease_paying_type)*((abs(strtotime($data['all_payment_info'][$key]['payment_start_period'])-strtotime($data['all_payment_info'][$key]['payment_end_period']))/86400)+1));
    //                         // $t_data['payment_status'] = 2 ;
    //                         // $t_data['payment_method'] = $get_value['payment_method'];
    //                         // $t_data['payment_for']=1;
    //                         // $this->property_model->insert('lease_tenant_payment_scedhule',$t_data);
    //                     // }

    //                     $get_value['payment_update_by'] = $get_value['payment_update_by'] - $get_value['share_paid_amount'];
    //                 }
    //                 // if ($value['payment_start_period']>date('Y-m-d'))
    //                 // {
    //                 //     $data['all_payment_info'][$key]['lease_rent_recieved'] = 0;
    //                 //     $data['all_payment_info'][$key]['lease_present_status'] = 2;
    //                 // }
    //             }
    //         }
    //     }
    //     foreach ($data['get_tenant_list'] as $get_value)
    //     {
    //         if($get_value['payment_update_status']==4)
    //         {
    //             // $start_here = 1;
    //             foreach ($data['all_payment_info'] as $key=>$value)
    //             {
    //                 if ($value['payment_start_period']<=date('Y-m-d'))
    //                 {
    //                     $get_value['share_paid_amount'] = (($get_value['share_paid_amount']/$lease_paying_type)*((abs(strtotime($data['all_payment_info'][$key]['payment_start_period'])-strtotime($data['all_payment_info'][$key]['payment_end_period']))/86400)+1));
    //                     if($get_value['payment_update_by']>0)
    //                     {
    //                         $data['all_payment_info'][$key]['lease_rent_recieved'] = $data['all_payment_info'][$key]['lease_rent_recieved'] - $get_value['share_paid_amount'];
    //                         $data['all_payment_info'][$key]['lease_present_status'] = 3;
    //                     }
    //                     // if($key==1)
    //                     // {
    //                         // if($get_value['payment_update_status']==4)
    //                         // {
    //                             // if($start_here==1)
    //                             // {
    //                                 // $t_data['tenant_id'] = $get_value['tenant_id'];
    //                                 // $t_data['lease_id'] = $get_value['lease_id'];
    //                                 // $t_data['property_id'] = $get_value['property_id'];
    //                                 // $t_data['schedule_start_date'] = $value['payment_start_period'];
    //                                 // $t_data['schedule_end_date'] = $value['payment_end_period'];
    //                                 // $t_data['shared_amount'] = $get_value['share_paid_amount'];
    //                                 // $t_data['payment_status'] = 4;
    //                                 // $t_data['payment_method'] = $get_value['payment_method'];
    //                                 // $t_data['payment_for']=1;
    //                                 // $this->property_model->insert('lease_tenant_payment_scedhule',$t_data);
    //                             // }
    //                         // }
    //                         // $start_here++;
    //                     // }
    //                     $get_value['payment_update_by'] = $get_value['payment_update_by'] - $get_value['share_paid_amount'];
    //                 }
    //                 if ($value['payment_start_period']>date('Y-m-d'))
    //                 {
    //                     $data['all_payment_info'][$key]['lease_rent_recieved'] = 0;
    //                     $data['all_payment_info'][$key]['lease_present_status'] = 2;
    //                 }
    //             }
    //         }
    //     }
    //     foreach ($data['all_payment_info'] as $key => $value)
    //     {
    //         $payment['lease_rent_recieved'] = $value['lease_rent_recieved'];
    //         $payment['lease_present_status'] = $value['lease_present_status'];
    //         $this->property_model->update_function('payment_schedule_id', $value['payment_schedule_id'], 'lease_payment_scedule', $payment);
    //     }
    //     $data['all_payment_infos'] = $this->property_model->select_condition_order('lease_payment_scedule', 'lease_id='.$lease_id.' AND property_id='.$property_id.'', 'lease_payment_scedule.payment_schedule_id','ASC');
    //     foreach ($data['get_tenant_list'] as $key => $get_value)
    //     {
    //         if($get_value['payment_update_status']==3 || $get_value['payment_update_status']==2)
    //         {
    //             foreach ($data['all_payment_infos'] as $key=>$value)
    //             {
    //                 if ($value['payment_start_period']>date('Y-m-d'))
    //                 {
    //                     $data['all_payment_infos'][$key]['lease_rent_recieved'] = 0;
    //                 }
    //             }
    //         }
    //     }
    //     foreach ($data['get_tenant_list'] as $key => $get_value)
    //     {
    //         if($get_value['payment_update_status']==3)
    //         {
    //             foreach ($data['all_payment_infos'] as $key=>$value)
    //             {
    //                 if ($value['payment_start_period']>date('Y-m-d'))
    //                 {
    //                     $get_value['share_paid_amount'] = (($get_value['share_paid_amount']/$lease_paying_type)*((abs(strtotime($data['all_payment_infos'][$key]['payment_start_period'])-strtotime($data['all_payment_infos'][$key]['payment_end_period']))/86400)+1));
    //                     // $get_value['payment_update_by'] = (($get_value['payment_update_by']/$lease_paying_type)*((abs(strtotime($data['all_payment_infos'][$key]['payment_start_period'])-strtotime($data['all_payment_infos'][$key]['payment_end_period']))/86400)+1));
    //                     if($get_value['payment_update_by']>0)
    //                     {
    //                         if($get_value['payment_update_by']>$get_value['share_paid_amount'])
    //                         {
    //                             $data['all_payment_infos'][$key]['lease_rent_recieved'] = $data['all_payment_infos'][$key]['lease_rent_recieved'] + $get_value['share_paid_amount'];
    //                             if($data['all_payment_infos'][$key]['lease_rent_recieved']==$data['all_payment_infos'][$key]['payment_due_amount'])
    //                             {
    //                                 $data['all_payment_infos'][$key]['lease_present_status'] = 1;
    //                             }
    //                             else
    //                             {
    //                                 $data['all_payment_infos'][$key]['lease_present_status'] = 2;
    //                             }
    //                         }
    //                         else
    //                         {
    //                             $data['all_payment_infos'][$key]['lease_rent_recieved'] = $data['all_payment_infos'][$key]['lease_rent_recieved'] + $get_value['payment_update_by'];
    //                             if($data['all_payment_infos'][$key]['lease_rent_recieved']==$data['all_payment_infos'][$key]['payment_due_amount'])
    //                             {
    //                                 $data['all_payment_infos'][$key]['lease_present_status'] = 1;
    //                             }
    //                             else
    //                             {
    //                                 $data['all_payment_infos'][$key]['lease_present_status'] = 2;
    //                             }
    //                         }
    //                         // if($get_value['payment_update_status']==3)
    //                         // {
    //                             // $t_data['tenant_id'] = $get_value['tenant_id'];
    //                             // $t_data['lease_id'] = $get_value['lease_id'];
    //                             // $t_data['property_id'] = $get_value['property_id'];
    //                             // $t_data['schedule_start_date'] = $value['payment_start_period'];
    //                             // $t_data['schedule_end_date'] = $value['payment_end_period'];
    //                             // $t_data['shared_amount'] = $data['all_payment_infos'][$key]['lease_rent_recieved'];
    //                             // $t_data['payment_status'] = 3;
    //                             // $t_data['payment_method'] = $get_value['payment_method'];
    //                             // $t_data['payment_for']=1;
    //                             // $this->property_model->insert('lease_tenant_payment_scedhule',$t_data);
    //                         // }
    //                     }
    //                     $get_value['payment_update_by'] = $get_value['payment_update_by'] - $get_value['share_paid_amount'];
    //                 }
    //                 if($get_value['payment_update_by']<0)
    //                 {
    //                     $data['all_payment_infos'][$key]['lease_rent_recieved'] = $data['all_payment_infos'][$key]['lease_rent_recieved'];
    //                     $data['all_payment_infos'][$key]['lease_present_status'] = 2;
    //                 }
    //             }
    //         }
    //     }
    //     foreach ($data['all_payment_infos'] as $key => $values)
    //     {
    //         $payments['lease_rent_recieved'] = $values['lease_rent_recieved'];
    //         $payments['lease_present_status'] = $values['lease_present_status'];
    //         $this->property_model->update_function('payment_schedule_id', $values['payment_schedule_id'], 'lease_payment_scedule', $payments);
    //     }
    //     $data['property_id'] = $property_id;
    //     $data['lease_id'] = $lease_id;
    //     $data['step_count'] = 7;
    //     redirect('confirmSchedule/' . $property_id . '/' . $lease_id);
    // }

    public function delete_single_amount($lease_tenant_payment_scedhule_id)
    {
        $tenant_payment_info = $this->property_model->select_with_where('*', 'lease_tenant_payment_scedhule_id=' . $lease_tenant_payment_scedhule_id . '', 'lease_tenant_payment_scedhule');
        $get_lease_detail = $this->property_model->select_with_where('*', 'tenant_id=' . $tenant_payment_info[0]['tenant_id'] . ' AND lease_id=' . $tenant_payment_info[0]['lease_id'] . ' AND property_id=' . $tenant_payment_info[0]['property_id'] . '', 'lease_detail');

        if ($get_lease_detail[0]['payment_update_status'] == 3) {
            $current_amount = ($get_lease_detail[0]['payment_update_by']);
        } elseif ($get_lease_detail[0]['payment_update_status'] == 2) {
            $current_amount = ((-1) * ($get_lease_detail[0]['payment_update_by']));
        } elseif ($get_lease_detail[0]['payment_update_status'] == 1) {
            $current_amount = $get_lease_detail[0]['payment_update_by'];
        }
        $new_amount = $current_amount - $tenant_payment_info[0]['shared_amount'];
        if ($new_amount < 0) {
            $new_amount = (-1) * $new_amount;
            $updt_data['payment_update_by'] = $new_amount;
            $updt_data['payment_update_status'] = 2;
        }
        if ($new_amount == 0) {
            $updt_data['payment_update_by'] = $new_amount;
            $updt_data['payment_update_status'] = 1;
        }
        if ($new_amount > 0) {
            $updt_data['payment_update_by'] = $new_amount;
            $updt_data['payment_update_status'] = 3;
        }

        $this->property_model->update_function('lease_detail_id', $get_lease_detail[0]['lease_detail_id'], 'lease_detail', $updt_data);
        $this->property_model->delete_function_cond('lease_tenant_payment_scedhule', 'lease_tenant_payment_scedhule_id=' . $lease_tenant_payment_scedhule_id . '');
        redirect('property/update_final_schedule_without_rent_recievce/' . $get_lease_detail[0]['property_id'] . '/' . $get_lease_detail[0]['lease_id']);
    }

    public function rent_schedule_pdf()
    {
        $property_id = $this->input->post('property_id');
        $tenant_rent_schedule = $this->input->post('tenant_rent_schedule');
        $data['rent_payment_status'] = $this->input->post('rent_payment_status');
        $data['property_info'] = $this->rent_model->select_with_where('*', 'property_id=' . $property_id . '', 'property');
        $data['lease_detail'] = $this->property_model->select_with_where('*', 'property_id=' . $property_id . ' AND lease_current_status=1', 'lease');
        $data['tenant_info'] = $this->rent_model->get_tenant_info($property_id, $data['lease_detail'][0]['lease_id'], $tenant_rent_schedule);


        $data['state_name'] = '';
        if ($data['property_info'][0]['state'] == 246) {
            $data['state_name'] = 'Australian Capital Territory';
        } elseif ($data['property_info'][0]['state'] == 266) {
            $data['state_name'] = 'New South Wales';
        } elseif ($data['property_info'][0]['state'] == 267) {
            $data['state_name'] = 'Northern Territory';
        } elseif ($data['property_info'][0]['state'] == 269) {
            $data['state_name'] = 'Queensland';
        } elseif ($data['property_info'][0]['state'] == 270) {
            $data['state_name'] = 'South Australia';
        } elseif ($data['property_info'][0]['state'] == 271) {
            $data['state_name'] = 'Tasmania';
        } elseif ($data['property_info'][0]['state'] == 273) {
            $data['state_name'] = 'Victoria';
        } elseif ($data['property_info'][0]['state'] == 275) {
            $data['state_name'] = 'Western Australia';
        }


        $total_tenant_amount = 0;
        foreach ($data['tenant_info'] as $key => $value) {
            $total_tenant_amount = $total_tenant_amount + $value['share_paid_amount'];
        }
        $data['total_tenant_amount'] = $total_tenant_amount;
        // echo "<pre>";print_r($data['$total_tenant_amount']);die();
        $user_id = $this->session->userdata('user_id');
        $data['user_info'] = $this->rent_model->select_with_where('*', 'user_id=' . $user_id . '', 'user');
        $data['all_payments_info'] = $this->property_model->select_condition_order('lease_payment_scedule', 'lease_id=' . $data['lease_detail'][0]['lease_id'] . ' AND property_id=' . $property_id . '', 'lease_payment_scedule.payment_schedule_id', 'ASC');

        $this->load->library('MPDF/mpdf');

        $mpdf = new mPDF('win-1252', 'A4', '', '', 5, 5, 16, 60, 5, 5, 'C');
        $mpdf->useOnlyCoreFonts = true;    // false is default
        $mpdf->SetProtection(array('print'));
        $mpdf->SetTitle('Rent Schedule');
        $mpdf->SetAuthor($data['author']);
        $mpdf->SetDisplayMode('fullpage');

        $mpdf->debug = true;


        /*do not load header,common left footer as this view contains the whole page*/
        $html = $this->load->view('rent_schedule_view_yes', $data, TRUE);

        //echo $html;die();

        $name = 'members' . rand(10000, 99999) . '.pdf';

        //echo  $link;die();
        $mpdf->WriteHTML($html);
        $mpdf->Output($name, 'I');

        exit;
    }

    public function tenant_ledgers_pdf()
    {
        $property_id = $this->input->post('property_id');

        $tenant_ledger = $this->input->post('tenant_ledger');
        $tenant_ledger_start_date = "";
        $tenant_ledger_end_date = "";

        $start_date = $this->input->post('tenant_ledger_start_date');
        $end_date = $this->input->post('tenant_ledger_end_date');

        if ($start_date != "") {
            $tenant_ledger_start_date = date('Y-m-d', strtotime($start_date));
        }
        if ($end_date) {
            $tenant_ledger_end_date = date('Y-m-d', strtotime($end_date));
        }

        $full_property_address = $this->utility_model->getFullPropertyAddress($property_id);
        $data['full_property_address'] = $full_property_address;
        $data['property_info'] = $this->rent_model->select_with_where('*', 'property_id=' . $property_id . '', 'property');
        $data['lease_detail'] = $this->property_model->select_with_where('*', 'property_id=' . $property_id . ' AND lease_current_status=1', 'lease');

        $lease_id = !empty($data['lease_detail']) ? $data['lease_detail'][0]['lease_id'] : 0;
        //----------------------------------------------------------
        $is_archived = $this->utility_model->isArchived();
        $archived_string = $this->utility_model->getArchivedString();
        if ($is_archived) {
            $lease_id = $this->utility_model->getArchivedLeaseId();
            $data['lease_detail'][0] = $this->utility_model->getLease($lease_id);
        }

        $data['is_archived'] = $is_archived;
        $data['archived_string'] = $archived_string;
        //----------------------------------------------------------

        $data['tenant_info'] = $this->rent_model->get_tenant_info($property_id, $data['lease_detail'][0]['lease_id'], $tenant_ledger);

        $data['state_name'] = '';
        if ($data['property_info'][0]['state'] == 246) {
            $data['state_name'] = 'Australian Capital Territory';
        } elseif ($data['property_info'][0]['state'] == 266) {
            $data['state_name'] = 'New South Wales';
        } elseif ($data['property_info'][0]['state'] == 267) {
            $data['state_name'] = 'Northern Territory';
        } elseif ($data['property_info'][0]['state'] == 269) {
            $data['state_name'] = 'Queensland';
        } elseif ($data['property_info'][0]['state'] == 270) {
            $data['state_name'] = 'South Australia';
        } elseif ($data['property_info'][0]['state'] == 271) {
            $data['state_name'] = 'Tasmania';
        } elseif ($data['property_info'][0]['state'] == 273) {
            $data['state_name'] = 'Victoria';
        } elseif ($data['property_info'][0]['state'] == 275) {
            $data['state_name'] = 'Western Australia';
        }

        $total_tenant_amount = 0;
        foreach ($data['tenant_info'] as $key => $value) {
            $total_tenant_amount = $total_tenant_amount + $value['share_paid_amount'];
        }
        $data['total_tenant_amount'] = $total_tenant_amount;
        $user_id = $this->session->userdata('user_id');
        $data['user_info'] = $this->rent_model->select_with_where('*', 'user_id=' . $user_id . '', 'user');
        $data['all_ledger_info'] = $this->property_model->select_condition_order_and_join_with_where($lease_id, $property_id, $tenant_ledger_start_date, $tenant_ledger_end_date);

        $data['all_ledger_info'] = null;
        /*echo "<pre>";
        print_r($data['lease_detail']);
        die();*/

        $data['tenant_ledger_start_date'] = "Unknown Date";
        $data['tenant_ledger_end_date'] = "Unknown Date";

        if ($start_date && $end_date) {
            $data['tenant_ledger_start_date'] = date('d/m/Y', strtotime($start_date));
            $data['tenant_ledger_end_date'] = date('d/m/Y', strtotime($end_date));
        } else if ($data['lease_detail'] != null && !empty($data['lease_detail'])) {
            $data['tenant_ledger_start_date'] = date('d/m/Y', strtotime($data['lease_detail'][0]['lease_start_date']));
            $data['tenant_ledger_end_date'] = date('d/m/Y', strtotime($data['lease_detail'][0]['lease_end_date']));
        }

        $data['lease_log_payment_lease_list'] = $this->rent_model->getLeaseLogPaymentList($lease_id);
        $data['water_invoices_with_payment_logs'] = $this->water_model->getWaterInvoicesWithPaymentLogs($lease_id);


        $this->load->library('MPDF/mpdf');

        $mpdf = new mPDF('win-1252', 'A4', '', '', 5, 5, 16, 60, 5, 5, 'C');
        $mpdf->useOnlyCoreFonts = true;    // false is default
        $mpdf->SetProtection(array('print'));
        $mpdf->SetTitle('Tenant Legder');
        $mpdf->SetAuthor($data['author']);
        $mpdf->SetDisplayMode('fullpage');

        $mpdf->debug = true;


        /*do not load header,common left footer as this view contains the whole page*/
        $html = $this->load->view('tenant_ledger_view_pdf', $data, TRUE);

        //echo $html;die();

        $name = 'members' . rand(10000, 99999) . '.pdf';

        //echo  $link;die();
        $mpdf->WriteHTML($html);
        $mpdf->Output($name, 'I');

        exit;
    }


    public function insert_payment_update()
    {
        $property_id = $this->input->post('property_id');
        $lease_id = $this->input->post('lease_id');
        $all_lease_data = $this->property_model->select_with_where('*', 'lease_id=' . $lease_id . ' AND property_id=' . $property_id . ' AND payment_status = 1', 'lease_detail');
        $amount_recieved = $this->input->post('amount_recieved');
        $payment_method = $this->input->post('payment_method');
        $payment_date = $this->input->post('payment_date');
        $lease_detail_id = $this->input->post('lease_detail_id');
        $payment_sms_check = $this->input->post('payment_sms_check');

        for ($i = 0; $i < count($amount_recieved); $i++) {
            $data['amount_recieved'] = $amount_recieved[$i];
            $data['payment_method'] = $payment_method[$i];
            $data['payment_date'] = $payment_date[$i];
            $data['lease_detail_id'] = $lease_detail_id[$i];
            if ($data['lease_detail_id'] == $all_lease_data[$i]['lease_detail_id']) {
                if ($all_lease_data[$i]['payment_update_status'] == 1) {
                    if ($data['amount_recieved'] == $all_lease_data[$i]['payment_update_by']) {
                        $new_data['payment_update_status'] = 1;
                        $new_data['payment_update_by'] = 0;
                    } elseif ($data['amount_recieved'] > $all_lease_data[$i]['payment_update_by']) {
                        $new_data['payment_update_status'] = 3;
                        $new_data['payment_update_by'] = $data['amount_recieved'];
                    }
                } elseif ($all_lease_data[$i]['payment_update_status'] == 2 || $all_lease_data[$i]['payment_update_status'] == 4) {
                    if ($data['amount_recieved'] == $all_lease_data[$i]['payment_update_by']) {
                        $new_data['payment_update_status'] = 1;
                        $new_data['payment_update_by'] = 0;
                    } elseif ($data['amount_recieved'] > $all_lease_data[$i]['payment_update_by']) {
                        $new_data['payment_update_status'] = 3;
                        $new_data['payment_update_by'] = $data['amount_recieved'] - $all_lease_data[$i]['payment_update_by'];
                    } elseif ($data['amount_recieved'] < $all_lease_data[$i]['payment_update_by']) {
                        $new_data['payment_update_status'] = 2;
                        $new_data['payment_update_by'] = $all_lease_data[$i]['payment_update_by'] - $data['amount_recieved'];
                    }
                } elseif ($all_lease_data[$i]['payment_update_status'] == 3) {
                    $new_data['payment_update_status'] = 3;
                    $new_data['payment_update_by'] = $all_lease_data[$i]['payment_update_by'] + $data['amount_recieved'];
                }
                $new_data['payment_method'] = $data['payment_method'];
                $this->property_model->update_function('lease_detail_id', $data['lease_detail_id'], 'lease_detail', $new_data);
            }
        }
        redirect('property/update_final_schedule_without_rent_recievce/' . $property_id . '/' . $lease_id);
    }

    public function update_final_schedule_without_rent_recievce($property_id, $lease_id)
    {
        $data['property_info'] = $this->rent_model->select_with_where('*', 'property_id=' . $property_id . '', 'property');
        $data['lease_info'] = $this->property_model->select_with_where('*', 'lease_id=' . $lease_id . ' AND property_id=' . $property_id . '', 'lease');
        $lease_paying_type = $data['lease_info'][0]['lease_pay_type'];
        if ($lease_paying_type == 1) {
            $lease_paying_type = 7;
        } elseif ($lease_paying_type == 2) {
            $lease_paying_type = 14;
        } elseif ($lease_paying_type == 3) {
            $lease_paying_type = 28;
        } else {
            $lease_paying_type = 30;
        }
        $data['all_payment_info'] = $this->property_model->select_condition_order('lease_payment_scedule', 'lease_id=' . $lease_id . ' AND property_id=' . $property_id . '', 'lease_payment_scedule.payment_schedule_id', 'DESC');

        $data['get_tenant_list'] = $this->property_model->get_specific_selected_tenant($property_id, $lease_id);

        $one_user_amount = 0;
        $one_user_amount_upcome = 0;
        foreach ($data['get_tenant_list'] as $key => $get_value) {
            foreach ($data['all_payment_info'] as $key => $value) {
                $value['lease_rent_recieved'] = 0;
            }
            if ($get_value['payment_update_status'] == 1) {
                $one_user_amount = ($one_user_amount + ($get_value['share_paid_amount'] / $lease_paying_type) * ((abs(strtotime($data['all_payment_info'][$key]['payment_start_period']) - strtotime($data['all_payment_info'][$key]['payment_end_period'])) / 86400) + 1));
            }
        }
        foreach ($data['get_tenant_list'] as $key => $get_value) {
            foreach ($data['all_payment_info'] as $key => $value) {
                if ($value['payment_start_period'] <= date('Y-m-d')) {
                    $data['all_payment_info'][$key]['lease_rent_recieved'] = $data['all_payment_info'][$key]['payment_due_amount'];
                    $data['all_payment_info'][$key]['lease_present_status'] = 1;
                    // if($get_value['payment_update_status']==1)
                    // {
                    //     $t_data['tenant_id'] = $get_value['tenant_id'];
                    //     $t_data['lease_id'] = $get_value['lease_id'];
                    //     $t_data['property_id'] = $get_value['property_id'];
                    //     $t_data['schedule_start_date'] = $value['payment_start_period'];
                    //     $t_data['schedule_end_date'] = $value['payment_end_period'];
                    //     $t_data['shared_amount'] = (($get_value['share_paid_amount']/$lease_paying_type)*((abs(strtotime($data['all_payment_info'][$key]['payment_start_period'])-strtotime($data['all_payment_info'][$key]['payment_end_period']))/86400)+1));
                    //     $t_data['payment_status'] = 1 ;
                    //     $t_data['payment_method'] = $get_value['payment_method'];
                    //     $t_data['payment_for']=1;
                    //     $this->property_model->insert('lease_tenant_payment_scedhule',$t_data);
                    // }
                }
                if ($value['payment_start_period'] > date('Y-m-d')) {
                    $data['all_payment_info'][$key]['lease_rent_recieved'] = 0;
                    $data['all_payment_info'][$key]['lease_present_status'] = 2;
                }
            }
        }
        foreach ($data['get_tenant_list'] as $key => $get_value) {
            if ($get_value['payment_update_status'] == 2) {
                foreach ($data['all_payment_info'] as $key => $value) {
                    if ($value['payment_start_period'] <= date('Y-m-d')) {
                        $get_value['share_paid_amount'] = (($get_value['share_paid_amount'] / $lease_paying_type) * ((abs(strtotime($data['all_payment_info'][$key]['payment_start_period']) - strtotime($data['all_payment_info'][$key]['payment_end_period'])) / 86400) + 1));
                        if ($get_value['payment_update_by'] > 0) {
                            if ($get_value['payment_update_by'] > $get_value['share_paid_amount']) {
                                $data['all_payment_info'][$key]['lease_rent_recieved'] = $data['all_payment_info'][$key]['lease_rent_recieved'] - $get_value['share_paid_amount'];
                                $data['all_payment_info'][$key]['lease_present_status'] = 3;
                            } else {
                                $data['all_payment_info'][$key]['lease_rent_recieved'] = $data['all_payment_info'][$key]['lease_rent_recieved'] - $get_value['payment_update_by'];
                                $data['all_payment_info'][$key]['lease_present_status'] = 3;
                            }
                        }
                        if ($get_value['payment_update_status'] == 2) {
                            $t_data['tenant_id'] = $get_value['tenant_id'];
                            $t_data['lease_id'] = $get_value['lease_id'];
                            $t_data['property_id'] = $get_value['property_id'];
                            $t_data['schedule_start_date'] = $value['payment_start_period'];
                            $t_data['schedule_end_date'] = $value['payment_end_period'];
                            $t_data['shared_amount'] = (($get_value['share_paid_amount'] / $lease_paying_type) * ((abs(strtotime($data['all_payment_info'][$key]['payment_start_period']) - strtotime($data['all_payment_info'][$key]['payment_end_period'])) / 86400) + 1));
                            $t_data['payment_status'] = 2;
                            $t_data['payment_method'] = $get_value['payment_method'];
                            $t_data['payment_for'] = 1;
                            $this->property_model->insert('lease_tenant_payment_scedhule', $t_data);
                        }

                        $get_value['payment_update_by'] = $get_value['payment_update_by'] - $get_value['share_paid_amount'];
                    }
                    // if ($value['payment_start_period']>date('Y-m-d'))
                    // {
                    //     $data['all_payment_info'][$key]['lease_rent_recieved'] = 0;
                    //     $data['all_payment_info'][$key]['lease_present_status'] = 2;
                    // }
                }
            }
        }
        // foreach ($data['get_tenant_list'] as $get_value)
        // {
        //     if($get_value['payment_update_status']==4)
        //     {
        //         $start_here = 1;
        //         foreach ($data['all_payment_info'] as $key=>$value)
        //         {
        //             if ($value['payment_start_period']<=date('Y-m-d'))
        //             {
        //                 $get_value['share_paid_amount'] = (($get_value['share_paid_amount']/$lease_paying_type)*((abs(strtotime($data['all_payment_info'][$key]['payment_start_period'])-strtotime($data['all_payment_info'][$key]['payment_end_period']))/86400)+1));
        //                 if($get_value['payment_update_by']>0)
        //                 {
        //                     $data['all_payment_info'][$key]['lease_rent_recieved'] = $data['all_payment_info'][$key]['lease_rent_recieved'] - $get_value['share_paid_amount'];
        //                     $data['all_payment_info'][$key]['lease_present_status'] = 3;
        //                 }
        //                 // if($key==1)
        //                 {
        //                     if($get_value['payment_update_status']==4)
        //                     {
        //                         if($start_here==1)
        //                         {
        //                             $t_data['tenant_id'] = $get_value['tenant_id'];
        //                             $t_data['lease_id'] = $get_value['lease_id'];
        //                             $t_data['property_id'] = $get_value['property_id'];
        //                             $t_data['schedule_start_date'] = $value['payment_start_period'];
        //                             $t_data['schedule_end_date'] = $value['payment_end_period'];
        //                             $t_data['shared_amount'] = $get_value['share_paid_amount'];
        //                             $t_data['payment_status'] = 4;
        //                             $t_data['payment_method'] = $get_value['payment_method'];
        //                             $t_data['payment_for']=1;
        //                             $this->property_model->insert('lease_tenant_payment_scedhule',$t_data);
        //                         }
        //                     }
        //                     $start_here++;
        //                 }
        //                 $get_value['payment_update_by'] = $get_value['payment_update_by'] - $get_value['share_paid_amount'];
        //             }
        //             if ($value['payment_start_period']>date('Y-m-d'))
        //             {
        //                 $data['all_payment_info'][$key]['lease_rent_recieved'] = 0;
        //                 $data['all_payment_info'][$key]['lease_present_status'] = 2;
        //             }
        //         }
        //     }
        // }
        foreach ($data['all_payment_info'] as $key => $value) {
            $payment['lease_rent_recieved'] = $value['lease_rent_recieved'];
            $payment['lease_present_status'] = $value['lease_present_status'];
            $this->property_model->update_function('payment_schedule_id', $value['payment_schedule_id'], 'lease_payment_scedule', $payment);
        }
        $data['all_payment_infos'] = $this->property_model->select_condition_order('lease_payment_scedule', 'lease_id=' . $lease_id . ' AND property_id=' . $property_id . '', 'lease_payment_scedule.payment_schedule_id', 'ASC');
        foreach ($data['get_tenant_list'] as $key => $get_value) {
            if ($get_value['payment_update_status'] == 3 || $get_value['payment_update_status'] == 2) {
                foreach ($data['all_payment_infos'] as $key => $value) {
                    if ($value['payment_start_period'] > date('Y-m-d')) {
                        $data['all_payment_infos'][$key]['lease_rent_recieved'] = 0;
                    }
                }
            }
        }
        foreach ($data['get_tenant_list'] as $key => $get_value) {
            if ($get_value['payment_update_status'] == 3) {
                foreach ($data['all_payment_infos'] as $key => $value) {
                    if ($value['payment_start_period'] > date('Y-m-d')) {
                        $get_value['share_paid_amount'] = (($get_value['share_paid_amount'] / $lease_paying_type) * ((abs(strtotime($data['all_payment_infos'][$key]['payment_start_period']) - strtotime($data['all_payment_infos'][$key]['payment_end_period'])) / 86400) + 1));
                        // $get_value['payment_update_by'] = (($get_value['payment_update_by']/$lease_paying_type)*((abs(strtotime($data['all_payment_infos'][$key]['payment_start_period'])-strtotime($data['all_payment_infos'][$key]['payment_end_period']))/86400)+1));
                        if ($get_value['payment_update_by'] > 0) {
                            if ($get_value['payment_update_by'] > $get_value['share_paid_amount']) {
                                $data['all_payment_infos'][$key]['lease_rent_recieved'] = $data['all_payment_infos'][$key]['lease_rent_recieved'] + $get_value['share_paid_amount'];
                                if ($data['all_payment_infos'][$key]['lease_rent_recieved'] == $data['all_payment_infos'][$key]['payment_due_amount']) {
                                    $data['all_payment_infos'][$key]['lease_present_status'] = 1;
                                } else {
                                    $data['all_payment_infos'][$key]['lease_present_status'] = 2;
                                }
                            } else {
                                $data['all_payment_infos'][$key]['lease_rent_recieved'] = $data['all_payment_infos'][$key]['lease_rent_recieved'] + $get_value['payment_update_by'];
                                if ($data['all_payment_infos'][$key]['lease_rent_recieved'] == $data['all_payment_infos'][$key]['payment_due_amount']) {
                                    $data['all_payment_infos'][$key]['lease_present_status'] = 1;
                                } else {
                                    $data['all_payment_infos'][$key]['lease_present_status'] = 2;
                                }
                            }
                            if ($get_value['payment_update_status'] == 3) {
                                $t_data['tenant_id'] = $get_value['tenant_id'];
                                $t_data['lease_id'] = $get_value['lease_id'];
                                $t_data['property_id'] = $get_value['property_id'];
                                $t_data['schedule_start_date'] = $value['payment_start_period'];
                                $t_data['schedule_end_date'] = $value['payment_end_period'];
                                $t_data['shared_amount'] = $data['all_payment_infos'][$key]['lease_rent_recieved'];
                                $t_data['payment_status'] = 3;
                                $t_data['payment_method'] = $get_value['payment_method'];
                                $t_data['payment_for'] = 1;
                                $this->property_model->insert('lease_tenant_payment_scedhule', $t_data);
                            }
                        }
                        $get_value['payment_update_by'] = $get_value['payment_update_by'] - $get_value['share_paid_amount'];
                    }
                    if ($get_value['payment_update_by'] < 0) {
                        $data['all_payment_infos'][$key]['lease_rent_recieved'] = $data['all_payment_infos'][$key]['lease_rent_recieved'];
                        $data['all_payment_infos'][$key]['lease_present_status'] = 2;
                    }
                }
            }
        }
        foreach ($data['all_payment_infos'] as $key => $values) {
            $payments['lease_rent_recieved'] = $values['lease_rent_recieved'];
            $payments['lease_present_status'] = $values['lease_present_status'];
            $this->property_model->update_function('payment_schedule_id', $values['payment_schedule_id'], 'lease_payment_scedule', $payments);
        }
        $data['property_id'] = $property_id;
        $data['lease_id'] = $lease_id;
        $data['step_count'] = 7;

        redirect('confirmSchedule/' . $property_id . '/' . $lease_id);
    }

    public function update_rent_recieved()
    {
        $lease_tenant_payment_scedhule_id = $this->input->post('lease_tenant_payment_scedhule_id');
        $shared_amount = $this->input->post('lease_tenant_payment_scedhule_id');
        $start_date = $this->input->post('schedule_start_date');
        $tenant_id = $this->input->post('tenant_id');
        $payment_method = $this->input->post('payment_method');
        for ($i = 0; $i < count($lease_tenant_payment_scedhule_id); $i++) {
            $data['shared_amount'] = $shared_amount[$i];
            $data['schedule_start_date'] = date('Y-m-d', strtotime($start_date[$i]));
            $data['tenant_id'] = $tenant_id[$i];
            $data['payment_method'] = $payment_method[$i];
            $payment_scedhule_id = $lease_tenant_payment_scedhule_id[$i];
            $this->property_model->update_function('lease_tenant_payment_scedhule_id', $payment_scedhule_id, 'lease_tenant_payment_scedhule', $data);
        }
        redirect('property');
    }

    private function set_upload_options($file_name, $folder_name)
    {
        //upload an image options
        $url = base_url();

        $config = array();
        $config['file_name'] = $file_name;
        $config['upload_path'] = 'uploads/' . $folder_name;
        $config['allowed_types'] = 'jpg|png|gif|jpeg|xls|doc|docx|pdf|csv|mp3|mp4';
        $config['max_size'] = '10000';
        $config['overwrite'] = TRUE;

        return $config;
    }

    public function inactive_lease_list($property_id)
    {
        $data['property_id'] = $property_id;
        $data['property'] = $this->property_model->select_with_where('*', "property_id={$property_id} ", 'property');
        $data['all_leases'] = $this->property_model->select_with_where('*', "property_id={$property_id}", 'lease');
        $data['inactive_leases'] = $this->property_model->select_with_where('*', "property_id={$property_id} AND lease_current_status=0 ", 'lease');

        $per_array = array(
            1 => 'week',
            2 => 'fortnight',
            3 => '4 week',
            4 => 'month',
        );

        $data['tenants_with_lease_details'] = null;

        $sqls = '';
        if ($data['inactive_leases']) {

            for ($i = 0; $i < count($data['inactive_leases']); $i++) {

                $tenants_with_lease_details = $this->property_model->select_where_join('*', 'lease_detail', 'user', 'lease_detail.tenant_id=user.user_id', "lease_detail.lease_id={$data['inactive_leases'][$i]['lease_id']}");

                $data['inactive_leases'][$i]['tenants_with_lease_details'] = $tenants_with_lease_details;

                //$sqls.='<br> <qst> '.$this->db->last_query().' </qst> <br>';
            }
        }

        $data['per_array'] = $per_array;

        /*echo "<pre>";
        echo $sqls;
        print_r($data['inactive_leases']);
        echo "</pre>";
        die();*/

        $this->load->view('inactive_lease_list', $data);
    }

    public function vacate_tenants($lease_id)
    {
        $response['success'] = 'false';

        $vacate_date = trim($this->input->post('vacate_date'));

        $lease_vacate_date = '0000-00-00';
        if ($vacate_date) {
            $lease_vacate_date = date('Y-m-d', strtotime($vacate_date));
        }

        //$upd_data['lease_current_status'] = 0;
        $upd_data['lease_vacated'] = 1;
        $upd_data['lease_vacate_date'] = $lease_vacate_date;

        $this->property_model->update_lease($lease_id, $upd_data);

        $response['success'] = 'true';

        echo json_encode($response);
        exit;
    }

    public function dashboard_modal_open($lease_id)
    {
        $lease = $this->utility_model->getLease($lease_id);

        /*echo "<pre>";
        print_r($lease);
        echo "</pre>";*/

        $flag = 0;

        if ($lease) {

            if ($lease['dashboard_modal_open'] == 0) {
                $flag = 1;
            }
        }

        echo $flag;
        exit;
    }

    public function dashboard_modal_shown($lease_id)
    {
        $this->db->set('dashboard_modal_open', 1);
        $this->db->where('lease_id', $lease_id);
        $this->db->update('lease');

        echo 1;
        exit;
    }

    public function deactivate_lease($lease_id)
    {
        $lease = $this->utility_model->getLease($lease_id);

        $upd_data = array();
        $upd_data['lease_current_status'] = 0;

        if ($lease) {
            $this->db->where('lease_id', $lease_id);
            $this->db->update('lease', $upd_data);

            redirect("ArchivedLeases/{$lease['property_id']}");
        }
    }

    public function notification_list()
    {
        $notifications = $this->utility_model->getNotificationListByUser($this->session->userdata('user_id'), $seen = 0, $limit = 10);


        $data['notifications'] = $notifications;
        $html = $this->load->view("notification_list_view", $data, true);

        echo $html;
        exit;

    }

    public function mark_notification_seen()
    {
        $notification_id = $this->uri->segment(3);

        try {
            $data = array();
            $data["seen"] = 1;

            $this->db->where('notification_id', $notification_id);
            $this->db->update('notification', $data);

            echo "success";

        } catch (Exception $e) {
            echo "error";
        }

        exit;
    }

}
