<?php $this->load->view('front/headlink'); ?>
<body>
<style>
    #signed_lease_agreement {
        display: none;
    }

    #uploaded_file {
        display: none;
    }

    #remove_file {
        display: none;
    }
</style>
<div class="setup_multistape">
    <div class="container">
        <?php $this->load->view('front/head_nav'); ?>
        <h3></h3>
        <div class="row form-group">
            <?= $this->load->view('front/step_div'); ?>
        </div>
    </div>
    <form class="container form-horizontal" onsubmit="return check_step_four_lease()" action="property/insert_lease"
          method="post"
          enctype="multipart/form-data">
        <div class="row setup-content" id="step-4">
            <div class="col-xs-12">
                <div class="col-md-12 well text-center">
                    <div class="alert alert-danger" id="err_div" style="display: none">
                        <strong id="err_msg"></strong>
                    </div>
                    <h1 class="extra_heading"> Lease & Rent Details
                        <small>for the property at <?= $this->utility_model->getFullPropertyAddress($property_id)?></small>
                    </h1>
                    <div class="form-group row">
                        <label for="inputLeaseName" class="col-sm-4 control-label">Lease Name</label>
                        <div class="col-sm-8">
                            <input id="inputLeaseName" type="text" name="lease_name" value="<?php if (($lease_info)) {
                                echo $lease_info[0]['lease_name'];
                            } else {
                                echo $lease_new_name;
                            } ?>" class="form-control" id="inputLeaseName" placeholder="Lease Name">
                        </div>
                    </div>

                    <div class="row">
                           <h4 style="color: red">If you have a current tenant in place, please ensure the lease start date reflects the same date the next rental payment is due.</h4>
                    </div>

                    <h4 class="form_heading no_margin">Lease Period</h4>
                    <div class="col-sm-12">
                        <div class="form-group ">
                            <label class="col-sm-4 control-label">from:</label>
                            <div class="col-sm-8 input-group">
                                <input class="form-control" value="<?php if (($lease_info)) {
                                    echo date("M d, Y", strtotime($lease_info[0]['lease_start_date']));
                                } ?>" name="lease_start_date" type="text" id="StartLeaseDate" readonly/>
                                <span class="input-group-addon"><i class="glyphicon glyphicon-calendar"></i></span>
                            </div>
                        </div>
                        <div class="form-group ">
                            <label class="col-sm-4 control-label">to:</label>
                            <div class="col-sm-8 input-group">
                                <input class="form-control" value="<?php if (($lease_info)) {
                                    echo date("M d, Y", strtotime($lease_info[0]['lease_end_date']));
                                } ?>" name="lease_end_date" id="EndLeaseDate" type="text" readonly/>
                                <span class="input-group-addon"><i class="glyphicon glyphicon-calendar"></i></span>
                            </div>
                        </div>
                    </div>
                    <div class="form-group row">
                        <div style=" text-align: left;">
                            <label class="col-sm-4 control-label">Add signed lease agreement</label>
                            <div class="col-sm-8">
                        <span class="file_upload_wrapper" style="display: inline; text-align: left;">
                            <input id="signed_lease_agreement" name="lease_file" type="file"
                                   style="position: relative; left: 0px; ">
                            <?php
                            $uploaded_file_exists = false;
                            $uploaded_file_url = false;
                            $uploaded_file_name = false;
                            $uploaded_file_href = ' href="#" ';
                            if (($lease_info)) {
                                $uploaded_file_exists = true;
                                if ($lease_info[0]['lease_file'] != null && $lease_info[0]['lease_file'] != false && trim($lease_info[0]['lease_file']) != '') {
                                    $uploaded_file_name = $lease_info[0]['lease_file'];
                                    $uploaded_file_href = ' href="' . '/uploads/image_and_file/file/' . $lease_info[0]['lease_file'] . '" ';
                                }
                            } ?>
                        </span>
                                <span>
                                <a download="download"
                                   id="uploaded_file" <?= $uploaded_file_href ?> ><?= $uploaded_file_name ? $uploaded_file_name : '' ?>
                            </a>
                            <i id="remove_file" class="glyphicon glyphicon-remove"></i>
                        </span>
                            </div>
                        </div>
                    </div>
                    <div class="">
                        <h4 class="form_heading no_margin">Bond Information</h4>
                        <div class="row">
                            <div class="col-md-4">
                                <label class="col-md-5 control-label">Bond Amount</label>
                                <div class="col-md-7">
                                    <input type="text"   name="bond_amount" value="<?php if ($bond_info) {
                                        echo $bond_info[0]['bond_amount'];
                                    } ?>" class="form-control wheelable" id="bondamount" placeholder="$0.00">
                                </div>
                            </div>
                            <div class="col-md-4">
                                <label class="col-md-5 control-label">Date lodged</label>
                                <div class="col-md-7">
                                    <input class="form-control" type="text" id="bond_start_date" name="lodge_start_date"
                                           value="<?php if ($bond_info && $bond_info[0]['lodge_start_date'] != null) {
                                               echo date("M d, Y", strtotime($bond_info[0]['lodge_start_date']));
                                           } else {
                                               echo date("M d, Y");
                                           } ?>" readonly/>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <label class="col-md-5 control-label">Lodged with</label>
                                <div class="col-md-7">
                                    <input type="text" name="lodged_with" value="<?php if ($bond_info) {
                                        echo $bond_info[0]['lodged_with'];
                                    } ?>" class="form-control" id="lodgedwith" placeholder="">
                                </div>
                                <input type="hidden" name="bond_id" value="<?php if ($bond_info) {
                                    echo $bond_info[0]['bond_id'];
                                } else {
                                    echo 0;
                                } ?>">
                            </div>
                        </div>
                    </div>
                    <div class="text-left">
                        <br/>
                        <h4 class="form_heading no_margin">Rent Payments</h4>
                        <div class="row">
                            <div class="col-md-4">
                                <label class="col-md-5 control-label">How often?</label>
                                <div class="col-md-7">
                                    <select class="form-control" name="lease_pay_type" id="inputday">
                                        <option <?php if (($lease_info)) {
                                            if ($lease_info[0]['lease_pay_type'] == 1) {
                                                echo 'selected';
                                            }
                                        } ?> id="Weekly" value="1" selected="selected">Weekly
                                        </option>
                                        <option <?php if (($lease_info)) {
                                            if ($lease_info[0]['lease_pay_type'] == 2) {
                                                echo 'selected';
                                            }
                                        } ?> id="Fortnightly" value="2">Fortnightly
                                        </option>
                                        <!-- <option <?php if (($lease_info)) {
                                            if ($lease_info[0]['lease_pay_type'] == 3) {
                                                echo 'selected';
                                            }
                                        } ?> id="4Weekly" value="3">4 Weekly
</option> -->
                                        <option <?php if (($lease_info)) {
                                            if ($lease_info[0]['lease_pay_type'] == 4) {
                                                echo 'selected';
                                            }
                                        } ?> id="Monthly" value="4">Monthly
                                        </option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <label class="col-md-7 control-label">How much per period?</label>
                                <div class="col-md-5">
                                    <input type="text" value="<?php if (($lease_info)) {
                                        echo $lease_info[0]['lease_per_period_payment'];
                                    } ?>" class="form-control" name="lease_per_period_payment"
                                           id="inputHowmuchperperiod" placeholder="$0">
                                </div>
                            </div>
                            <div class="col-md-4">
                                <label class="col-md-5 control-label">Day Rent Due:</label>
                                <div class="col-md-7">
                                    <select class="form-control" id="inputhowOftenday" <?php if (($lease_info)) {
                                        if ($lease_info[0]['lease_pay_type'] == 4) {
                                            echo "style='display: none;'";
                                        } else {
                                            echo "name='lease_pay_day'";
                                        }
                                    } else {
                                        echo "name='lease_pay_day'";
                                    } ?>>
                                        <option <?php if (($lease_info)) {
                                            if ($lease_info[0]['lease_pay_day'] == 1) {
                                                echo 'selected';
                                            }
                                        } ?> id="Monday" value="1" selected="selected">Monday
                                        </option>
                                        <option <?php if (($lease_info)) {
                                            if ($lease_info[0]['lease_pay_day'] == 2) {
                                                echo 'selected';
                                            }
                                        } ?> id="Tuesday" value="2">Tuesday
                                        </option>
                                        <option <?php if (($lease_info)) {
                                            if ($lease_info[0]['lease_pay_day'] == 3) {
                                                echo 'selected';
                                            }
                                        } ?> id="Wednesday" value="3">Wednesday
                                        </option>
                                        <option <?php if (($lease_info)) {
                                            if ($lease_info[0]['lease_pay_day'] == 4) {
                                                echo 'selected';
                                            }
                                        } ?> id="Thursday" value="4">Thursday
                                        </option>
                                        <option <?php if (($lease_info)) {
                                            if ($lease_info[0]['lease_pay_day'] == 5) {
                                                echo 'selected';
                                            }
                                        } ?> id="Friday" value="5">Friday
                                        </option>
                                        <option <?php if (($lease_info)) {
                                            if ($lease_info[0]['lease_pay_day'] == 6) {
                                                echo 'selected';
                                            }
                                        } ?> id="Saturday" value="6">Saturday
                                        </option>
                                        <option <?php if (($lease_info)) {
                                            if ($lease_info[0]['lease_pay_day'] == 7) {
                                                echo 'selected';
                                            }
                                        } ?> id="Sunday" value="7">Sunday
                                        </option>
                                    </select>
                                    <select class="form-control" id="inputmonthDetail" <?php if (($lease_info)) {
                                        if ($lease_info[0]['lease_pay_type'] != 4) {
                                            echo "style='display: none;'";
                                        } else {
                                            echo "name='lease_pay_day'";
                                        }
                                    } else {
                                        echo "style='display: none;'";
                                    } ?>>
                                        <?php for ($i = 1; $i <= 28; $i++) { ?>
                                            <option <?php if (($lease_info)) {
                                                if ($lease_info[0]['lease_pay_day'] == $i) {
                                                    echo 'selected';
                                                }
                                            } ?> value="<?= $i; ?>">
                                                <?= $i; ?>
                                                <?php if ($i == 1) {
                                                    echo 'st';
                                                } elseif ($i == 2) {
                                                    echo 'nd';
                                                } elseif ($i == 3) {
                                                    echo 'rd';
                                                } else {
                                                    echo "th";
                                                } ?> of the month
                                            </option>
                                        <?php } ?>
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="text-center extar_p">
                        <a href="stepThree/<?= $property_id . "?add_another_lease={$add_another_lease}"; ?>"
                           class="btn btn-light btn-md">Back</a>
                        <input type="hidden" name="add_another_lease" value="<?= $add_another_lease; ?>">
                        <input type="hidden" name="property_id" value="<?= $property_id; ?>">
                        <input type="hidden" name="lease_id" value="<?php if (($lease_info)) {
                            echo $lease_info[0]['lease_id'];
                        } else {
                            echo 0;
                        } ?>">
                        <input class="btn btn-primary btn-md" type="submit" value="Generate Rent Schedule">
                    </div>
                </div>
            </div>
        </div>
    </form>
</div>
<?php $this->load->view('front/footerlink'); ?>
<script type="text/javascript">
    $("#inputday").change(function () {
        var selectedtime = $("#inputday").val();
        if (selectedtime == 4) {
            $('#inputhowOftenday').hide();
            $('#inputhowOftenday').removeAttr('name');
            $('#inputmonthDetail').show();
            $('#inputmonthDetail').attr('name', 'lease_pay_day');
        }
        else {
            $('#inputhowOftenday').show();
            $('#inputhowOftenday').attr('name', 'lease_pay_day');
            $('#inputmonthDetail').hide();
            $('#inputmonthDetail').removeAttr('name');
        }
    });
</script>

<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-mousewheel/3.1.13/jquery.mousewheel.js" ></script>
<script>
    $(function() {
        var BondAmountIcr = 50.00;

        $("#bondamount").bind("mousewheel", function(event, delta) {

            if(this.value == '' || isNaN(this.value)){
                this.value = 0;
            }

            if (delta > 0) {
                this.value = parseInt(this.value) + BondAmountIcr;
            } else {
                if (parseInt(this.value) >= 0) {
                    this.value = parseInt(this.value) - BondAmountIcr;
                }else{
                    this.value = 0.00;
                    alert('Bond Amount Cannot be negative');
                }
            }
            return false;
        });

    });
</script>

<script type="text/javascript">

    function check_step_four_lease() {

        var BondAmount = $('#bondamount').val();
        var flag = true;
        var count = 0;
        var LeaseName = $('#inputLeaseName').val();
        var StartDate = $('#StartLeaseDate').val();
        var EndDate = $('#EndLeaseDate').val();
        var PerPeriod = $('#inputHowmuchperperiod').val();
        if (LeaseName == '' || StartDate == "" || EndDate == "" || PerPeriod == "") {
            $("#err_div").show();
            $("#err_msg").html('* All fields must be filled out');
            flag = false;
        }
        else {
            $("#err_div").hide();
        }
        if (LeaseName == '') {
            count = count + 1;
            $('#inputLeaseName').closest('div').addClass("has-error");
        }
        else {
            $('#inputLeaseName').closest('div').removeClass("has-error");
        }
        if (StartDate == '') {
            count = count + 1;
            $('#StartLeaseDate').closest('div').addClass("has-error");
        }
        else {
            $('#StartLeaseDate').closest('div').removeClass("has-error");
        }
        if (EndDate == '') {
            count = count + 1;
            $('#EndLeaseDate').closest('div').addClass("has-error");
        }
        else {
            $('#EndLeaseDate').closest('div').removeClass("has-error");
        }
        if (PerPeriod == '' || isNaN(PerPeriod) || parseFloat(PerPeriod) <= 0) {
            count = count + 1;
            $('#inputHowmuchperperiod').closest('div').addClass("has-error");
        }
        else {
            $('#inputHowmuchperperiod').closest('div').removeClass("has-error");
        }

        if (StartDate >= EndDate) {
            /*alert(StartDate);
            alert(EndDate);
            $('#StartLeaseDate').closest('div').addClass("has-error");
            $('#EndLeaseDate').closest('div').addClass("has-error");
            count = count + 1;*/
        }
        else {
            $('#StartLeaseDate').closest('div').removeClass("has-error");
            $('#EndLeaseDate').closest('div').removeClass("has-error");
        }

        if (parseFloat(BondAmount) != '') {

            if (isNaN(parseFloat(BondAmount)) || parseFloat(BondAmount) < 0) {
                count = count + 1;
                $('#bondamount').closest('div').addClass("has-error");
            } else {
                $('#bondamount').closest('div').removeClass("has-error");
            }
        } else {
            $('#bondamount').closest('div').removeClass("has-error");
        }

        if (count <= 0) {
            flag = true;
        }
        else {
            flag = false;
        }
        return flag;
    }
</script>
<script>
    $(document).ready(function () {
        var uploaded_file_exist = <?php if($uploaded_file_exists) { ?> true <?php } else { ?> false <?php } ?> ;
        if (uploaded_file_exist) {
            show_file_anchor_not_input();
        } else {
            show_file_input_not_anchor();
        }
        $('#remove_file').on('click', function () {
            show_file_input_not_anchor();
        });
    });

    function show_file_input_not_anchor() {
        $('#signed_lease_agreement').show();
        $('#uploaded_file').hide();
        $('#remove_file').hide();
    }

    function show_file_anchor_not_input() {
        $('#signed_lease_agreement').hide();
        $('#uploaded_file').show();
        $('#remove_file').show();
    }
</script>
<script type="text/javascript">
    $(document).ready(function () {
        $('#StartLeaseDate, #EndLeaseDate').daterangepicker();
    });
    $(function () {
        $('#bond_start_date').datepicker({
            dateFormat: 'M d, yy'
        });
    });
</script>
</body>
</html>