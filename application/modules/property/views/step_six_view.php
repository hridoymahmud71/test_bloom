<?php $this->load->view('front/headlink'); ?>
<body>
<style>
    .red-border {
        border-color: red !important;
    }

    .red {
        color: red !important;
    }

    .green {
        color: green !important;
    }
</style>
<style>
    .td_payment_method {
        display: none;
    }

    .td_payment_update_status {
        display: none;
    }

    .td_payment_update_by {
        display: none;
    }
</style>
<div class="setup_multistape">
    <div class="container">
        <?php $this->load->view('front/head_nav'); ?>
        <h3></h3>
        <div class="row form-group">
            <?= $this->load->view('front/step_div'); ?>
        </div>
    </div>
    <form id="rent_form" class="container" action="property/insert_step_six" method="post">
        <div class="row setup-content" id="step-6">
            <div class="col-xs-12">
                <div class="col-md-12 well">
                    <h1 class="text-center"> And who actually pays the rent?</h1>
                    <table class="table">
                        <thead>
                        <tr>
                            <th scope="col"></th>
                            <th scope="col">Tenant Name</th>
                            <th scope="col">Share Paid</th>
                            <th scope="col">Pay Method</th>
                            <th scope="col">Payment Status</th>
                            <th scope="col"></th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php foreach ($get_tenant_list as $key => $row) { ?>
                            <input type="hidden" id="lease_detail_id_<?= $key ?>" name="lease_detail_id[]"
                                   value="<?= $row['lease_detail_id'] ?>">
                            <tr id="tenant_row_<?= $key; ?>" class="tenant_row" row-num="<?= $key ?>"
                                row-check-attr="<?= $row['payment_status'] == 1 ? 1 : 0 ?>">
                                <th id="td_payment_status_<?= $key ?>" class="td_payment_status tdcol">
                                    <input id="payment_status_<?= $key ?>" class="payment_status" type="hidden"
                                           name="payment_status[]" value="<?= $row['payment_status'] == 1 ? 1 : 0 ?>"/>
                                    <input id="dummy_payment_status_<?= $key; ?>"
                                           class="form-control dummy_payment_status" style="width:40px;"
                                           type="checkbox" <?php if ($row['payment_status'] == 1) {
                                        echo ' checked="checked" ';
                                    }else{echo ' checked="checked" ';} ?>>
                                </th>
                                <th scope="row"><?= $row['user_fname']; ?> <?= $row['user_lname']; ?></th>
                                <td id="td_share_paid_amount_<?= $key ?>" class="td_share_paid_amount tdcol">
                                    <input id="share_paid_amount_<?= $key ?>" class="form-control share_paid_amount"
                                           name="share_paid_amount[]" type="text"
                                           value="<?php if($row['share_paid_amount']==0){echo number_format($tenant_sharing_amount, 2, '.', '');}else{echo $row['share_paid_amount'];}?>"
                                           placeholder="0.00">
                                </td>
                                <td id="td_payment_method_<?= $key ?>" class="td_payment_method tdcol">
                                    <select id="payment_method_<?= $key ?>" class="form-control payment_method"
                                            name="payment_method[]">
                                        <option value="">Select Method</option>
                                        <option <?php if ($row['payment_method'] == 1) {
                                            echo "selected";
                                        } ?> value="1">Bank Transfer
                                        </option>
                                        <option <?php if ($row['payment_method'] == 2) {
                                            echo "selected";
                                        }else{echo "selected";} ?> value="2">Cash
                                        </option>
                                        <option <?php if ($row['payment_method'] == 3) {
                                            echo "selected";
                                        } ?> value="3">Cheque
                                        </option>
                                        <option <?php if ($row['payment_method'] == 4) {
                                            echo "selected";
                                        } ?> value="4">Credit Card
                                        </option>
                                        <option <?php if ($row['payment_method'] == 5) {
                                            echo "selected";
                                        } ?> value="5">Other method
                                        </option>
                                    </select>
                                </td>
                                <td id="td_payment_update_status_<?= $key ?>" class="td_payment_update_status tdcol">
                                    <select id="payment_update_status_<?= $key ?>"
                                            class="btn btn-md form-control payment_update_status"
                                            name="payment_update_status[]">
                                        <option disabled value="1" <?php if ($row['payment_update_status'] == 1) {
                                            echo "selected";
                                        } ?>>Up to Date
                                        </option>
                                        <option disabled value="3" <?php if ($row['payment_update_status'] == 3) {
                                            echo "selected";
                                        } ?>>Ahead of Schedule
                                        </option>
                                        <option disabled value="2" <?php if ($row['payment_update_status'] == 2) {
                                            echo "selected";
                                        } ?>>In Arrears
                                        </option>
                                        <option value="4" <?php if ($row['payment_update_status'] == 4) {
                                            echo "selected";
                                        }else{echo "selected";} ?>>I don't know
                                        </option>
                                    </select>
                                </td>
                                <td id="td_payment_update_by_<?= $key ?>" class="td_payment_update_by tdcol">
                                    by <span>&nbsp; &nbsp;
                                        <input id="payment_update_by_<?= $key ?>" class="payment_update_by"
                                               style="max-width:100px;" type="text"
                                               value="<?= $row['payment_update_by'] ? $row['payment_update_by'] : 0.00 ?>"
                                               attr-payment-update-status="1"
                                               name="payment_update_by[]">
                                    </span>
                                </td>
                            </tr>
                        <?php } ?>
                        </tbody>
                    </table>
                    <h3>Total paid per period: <span id="calculated_total_dollar_span">$</span>
                        <span id="calculated_total_span"></span>
                    </h3>
                    <h5 class="text-center">
                        Total amount should be equal to
                        <span>$</span><span><?= $lease_info[0]['lease_per_period_payment'] ? number_format($lease_info[0]['lease_per_period_payment'], 2, '.', '') : 0.00; ?></span>
                    </h5>
                    <div class="text-center extar_p">
                        <a href="stepFive/<?= $property_id; ?>/<?= $lease_info[0]['lease_id'];?>/back"
                           class="btn btn-light btn-md">Back</a>
                        <input type="hidden" name="property_id" value="<?= $property_id; ?>">
                        <input type="hidden" name="lease_id" value="<?= $lease_info[0]['lease_id']; ?>">
                        <input id="submit_btn" type="submit" class="btn btn-primary btn-md" name="save_value"
                               value="Next Step">
                    </div>
                </div>
            </div>
        </div>
    </form>
</div>
<script type="text/javascript">

    $(document).ready(function () {

            var total_amt = parseFloat("<?= $lease_info[0]['lease_per_period_payment'] ? $lease_info[0]['lease_per_period_payment'] : 0; ?>");
            var max_advance_amount = parseFloat("<?= $max_advance_amount ? $max_advance_amount : 0; ?>");
            var max_arrear_amount = parseFloat("<?= $max_arrear_amount ? $max_arrear_amount : 0; ?>");
            var calculated_total_span = $('#calculated_total_span');
            var calculated_total_dollar_span = $('#calculated_total_dollar_span');

            var class_dummy_payment_status = $('.dummy_payment_status');
            var class_share_paid_amount = $('.share_paid_amount');
            var class_payment_method = $('.payment_method');
            var class_payment_update_status = $('.payment_update_status');
            var class_payment_update_by = $('.payment_update_by');

            check_all_task_by_rows();

            function check_all_task_by_rows() {
                var last_error_count = 0;
                var last_validation_bool = false;
                class_dummy_payment_status.each(function (index, value) {
                    var dummy_payment_status = $(this);
                    var tenant_row = dummy_payment_status.closest('.tenant_row');
                    if (task_by_rows(tenant_row) == false) {
                        last_error_count++;
                    }
                });

                if (last_error_count == 0) {
                    last_validation_bool = true;
                }
                return last_validation_bool;
            }


            class_dummy_payment_status.on('change', function () {
                var dummy_payment_status = $(this);
                var tenant_row = dummy_payment_status.closest('.tenant_row');
                task_by_rows(tenant_row);
            });

            //-----------------------------------------------
            class_share_paid_amount.on('input', function () {
                var share_paid_amount = $(this);
                var tenant_row = share_paid_amount.closest('.tenant_row');
                task_by_rows(tenant_row);
            });
            class_share_paid_amount.on('focus', function () {
                var share_paid_amount = $(this);
                if (share_paid_amount.val() == 0) {
                    share_paid_amount.val('');
                }
            });
            class_share_paid_amount.on('blur', function () {
                var share_paid_amount = $(this);
                if (check_valid_amount(share_paid_amount) == false) {
                    share_paid_amount.val(0.00.toFixed(2));
                }
            });
            //-----------------------------------------------

            //-----------------------------------------------
            class_payment_update_by.on('input', function () {
                var payment_update_by = $(this);
                var tenant_row = payment_update_by.closest('.tenant_row');
                task_by_rows(tenant_row);
            });
            class_payment_update_by.on('focus', function () {
                var payment_update_by = $(this);
                if (payment_update_by.val() == 0) {
                    payment_update_by.val('');
                }
            });
            //-----------------------------------------------

            class_share_paid_amount.on('blur', function () {
                var share_paid_amount = $(this);
                if (check_valid_amount(share_paid_amount) == false) {
                    share_paid_amount.val(0.00.toFixed(2));
                }
            });

            class_payment_method.on('change', function () {
                var payment_method = $(this);
                var tenant_row = payment_method.closest('.tenant_row');
                task_by_rows(tenant_row);
            });

            class_payment_update_status.on('change', function () {
                var payment_update_status = $(this);
                var tenant_row = payment_update_status.closest('.tenant_row');
                var payment_update_by = tenant_row.find('.payment_update_by');
                payment_update_by.val('');
                task_by_rows(tenant_row);
            });


            $('#submit_btn').on('click', function (e) {
                e.preventDefault();

                if (check_all_task_by_rows() == true) {
                    $('#rent_form').submit();
                }
            });

            function task_by_rows(tenant_row) {

                var error_count = 0;
                var return_bool = false;

                var payment_status = tenant_row.find('.payment_status');
                var dummy_payment_status = tenant_row.find('.dummy_payment_status');
                var share_paid_amount = tenant_row.find('.share_paid_amount');
                var payment_method = tenant_row.find('.payment_method');
                var payment_update_status = tenant_row.find('.payment_update_status');
                var payment_update_by = tenant_row.find('.payment_update_by');

                var td_share_paid_amount = tenant_row.find('.td_share_paid_amount');
                var td_payment_method = tenant_row.find('.td_payment_method');
                var td_payment_update_status = tenant_row.find('.td_payment_update_status');
                var td_payment_update_by = tenant_row.find('.td_payment_update_by');

                if (dummy_payment_status.is(':checked')) {
                    payment_status.val(1);
                    tenant_row.attr('row-check-attr', 1);
                } else {
                    payment_status.val(0);
                    tenant_row.attr('row-check-attr', 0);
                }

                //------------------------------------------------------------------------------
                //2 for advance , 3 for arrear
                payment_update_by.attr('attr-payment-update-status', payment_update_status.val());

                if (payment_update_status.val() == 2) {
                    if (!payment_update_by.hasClass('payment_update_by_advance')) {
                        payment_update_by.addClass('payment_update_by_advance');
                    }
                } else {
                    if (payment_update_by.hasClass('payment_update_by_advance')) {
                        payment_update_by.removeClass('payment_update_by_advance');
                    }
                }

                if (payment_update_status.val() == 3) {
                    if (!payment_update_by.hasClass('payment_update_by_arrear')) {
                        payment_update_by.addClass('payment_update_by_arrear');
                    }
                } else {
                    if (payment_update_by.hasClass('payment_update_by_arrear')) {
                        payment_update_by.removeClass('payment_update_by_arrear');
                    }
                }
                //----------------------------------------------------------------------


                //-----------------------------------------------------------------
                var added_share_paid_amount = parseFloat(add_up_share_paid_amount());
                calculated_total_span.html(added_share_paid_amount);

                if (added_share_paid_amount != total_amt) {
                    error_count++;
                    change_calculated_amount_color('red');
                    change_border(share_paid_amount, 'red-border');
                } else {
                    change_calculated_amount_color('green');
                    change_border(share_paid_amount, 'none');
                    change_border(class_share_paid_amount, 'none');
                }
                //-----------------------------------------------------------------

                //-----------------------------------------------------------------
                var added_up_advance_amount = parseFloat(add_up_advance_or_arrear_amount('advance'));
                if (added_up_advance_amount > max_advance_amount) {
                    error_count++;
                    // console.log('---adv--red');
                    change_border($('.payment_update_by_advance'), 'red-border');

                } else {
                    // console.log('---adv--none');
                    change_border($('.payment_update_by_advance'), 'none');
                }
                //-----------------------------------------------------------------

                //-----------------------------------------------------------------
                var added_up_arrear_amount = parseFloat(add_up_advance_or_arrear_amount('arrear'));
                if (added_up_arrear_amount > max_arrear_amount) {
                    error_count++;
                    // console.log('---arr--red');
                    change_border($('.payment_update_by_arrear'), 'red-border');
                } else {
                    // console.log('---arr--none');
                    change_border($('.payment_update_by_arrear'), 'none');
                }

                //-----------------------------------------------------------------

                if (payment_status.val() == 1) {


                    if (check_valid_amount(share_paid_amount) == false) {
                        error_count++;
                    }

                    td_payment_method.show();
                    if (payment_method.val() == '' || payment_method.val() == null || payment_method.val() == false) {
                        change_border(payment_method, 'red-border');
                    } else {
                        change_border(payment_method, 'none');
                    }

                    //payment_update_by <starts>
                    td_payment_update_status.show();
                    if (payment_update_status.val() == 2 || payment_update_status.val() == 3) {
                        td_payment_update_by.show();
                        if (check_valid_amount(payment_update_by) == false) {
                            error_count++;
                        }
                    } else {
                        td_payment_update_by.hide();
                    }
                    //payment_update_by <ends>

                } else {
                    td_payment_method.hide();
                    td_payment_update_status.hide();
                    td_payment_update_by.hide();
                }


                if (error_count == 0) {
                    return_bool = true;
                }

                // console.log('error_count');
                // console.log(error_count);

                return return_bool;
            }

            function change_calculated_amount_color(color) {

                if (color == 'red') {

                    if (calculated_total_span.hasClass('green')) {
                        calculated_total_span.removeClass('green');
                    }
                    if (calculated_total_dollar_span.hasClass('green')) {
                        calculated_total_dollar_span.removeClass('green');
                    }

                    if (!calculated_total_span.hasClass('red')) {
                        calculated_total_span.addClass('red');
                    }
                    if (!calculated_total_dollar_span.hasClass('red')) {
                        calculated_total_dollar_span.addClass('red');
                    }

                }

                if (color == 'green') {

                    if (calculated_total_span.hasClass('red')) {
                        calculated_total_span.removeClass('red');
                    }
                    if (calculated_total_dollar_span.hasClass('red')) {
                        calculated_total_dollar_span.removeClass('red');
                    }

                    if (!calculated_total_span.hasClass('green')) {
                        calculated_total_span.addClass('green');
                    }
                    if (!calculated_total_dollar_span.hasClass('green')) {
                        calculated_total_dollar_span.addClass('green');
                    }

                }
            }

            function change_border(elem, border) {
                // console.log('change_border');
                // console.log(elem);
                // console.log(border);
                // console.log('^^^^^^^^^^^^^^');
                if (border == 'red-border') {
                    if (!elem.hasClass('red-border')) {
                        elem.addClass('red-border');
                    }
                }
                if (border == 'none') {
                    if (elem.hasClass('red-border')) {
                        elem.removeClass('red-border');
                    }
                }

            }

            function check_valid_amount(elem) {

                var e_c = 0;
                var ret = false;
                var decimal_regexp = /^\d+\.\d{0,2}$/; //upto 2 decimal places
                if (elem) {
                    if (
                        elem.val() == '' || $.trim(elem.val()) == '' || elem.val() == null || (elem.val() < 0) ||
                        elem.val() == false || elem.val() === 'undefined' || isNaN(elem.val())
                    ) {
                        e_c++;
                        change_border(elem, 'red-border');
                    } else {
                        change_border(elem, 'none');
                    }
                }

                if (e_c == 0) {
                    ret = true;
                }
                return ret;
            }

            function add_up_share_paid_amount() {
                var calculated_total_amount = 0.00;

                $('.share_paid_amount').each(function (index, value) {
                    var share_paid_amount = $(this);
                    var tenant_row = share_paid_amount.closest('.tenant_row');

                    if (tenant_row.attr('row-check-attr') == 1) {
                        if (check_valid_amount(share_paid_amount) == false) {
                            calculated_total_amount += 0.00;
                        } else {
                            calculated_total_amount += parseFloat(share_paid_amount.val());
                        }
                    }
                });

                return calculated_total_amount;
            }

            function add_up_advance_or_arrear_amount(status) {
                var calculated_advance_or_arrear_amount = 0.00;
                // console.log('status');
                // console.log(status);
                // console.log('^^^^^^^^^^^^^^^^^^^^^^^^');

                $('.payment_update_by').each(function (index, value) {
                    var payment_update_by = $(this);

                    var tenant_row = payment_update_by.closest('.tenant_row');

                    if (tenant_row.attr('row-check-attr') == 1) {
                        if (check_valid_amount(payment_update_by) == false) {
                            calculated_advance_or_arrear_amount += 0.00;
                        } else {
                            // attr-payment-update-status == 2 for advance
                            // attr-payment-update-status == 3 for arrear
                            if (status == 'advance') {
                                // console.log('chk adv');
                                if (payment_update_by.attr('attr-payment-update-status') == 2) {
                                    // console.log('chk sts 2');
                                    calculated_advance_or_arrear_amount += parseFloat(payment_update_by.val());
                                }

                            } else if (status == 'arrear') {
                                // console.log('chk arr');
                                if (payment_update_by.attr('attr-payment-update-status') == 3) {
                                    // console.log('chk sts 3');
                                    calculated_advance_or_arrear_amount += parseFloat(payment_update_by.val());
                                }

                            }

                        }
                    }
                });

                // console.log('calculated_advance_or_arrear_amount');
                // console.log(calculated_advance_or_arrear_amount);
                // console.log('^^^^^^^^^^^^^^^^^^^^^^^^');
                return calculated_advance_or_arrear_amount;
            }
        }
    );


</script>
<?php $this->load->view('front/footerlink'); ?>
</body>
</html>
