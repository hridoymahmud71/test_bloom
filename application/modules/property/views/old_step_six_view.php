        <?php $this->load->view('front/headlink');?>
        <body>
            <div class="setup_multistape">  
                 <div class="ss_main_container"></div>
                <div class="container">
                    <?php $this->load->view('front/head_nav'); ?>
                    <h3>You'll find out about all these once set up your first Bloom property</h3>
                    <div class="row form-group">
                        <?= $this->load->view('front/step_div'); ?>
                    </div>
                </div>
                <form class="container">
                    <div class="row setup-content" id="step-6">
                        <div class="col-xs-12">
                            <div class="col-md-12 well">
                                <h1 class="text-center"> And who actually pays the rent?</h1>
                                <table class="table">
                                    <thead>
                                        <tr>
                                            <th scope="col">Tenant Name</th>
                                            <th scope="col">Share Paid</th>
                                            <th scope="col">Pay Method</th>
                                            <th scope="col">Payment Status</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php foreach($get_tenant_list as $key=>$row){?>
                                        <tr>
                                            <th><input class="form-control" style="width:40px;" type="checkbox" name=""></th>
                                            <th scope="row"><?=$row['user_fname'];?> <?=$row['user_lname'];?></th>
                                            <td><input class="form-control" name="amount_due[]" type="number" value="" placeholder="0.00"></td>
                                            <td>
                                                <select class="form-control" id="inputday">
                                                    <option value="Select Method">Select Method</option>
                                                    <option value="Bank Transfer">Bank Transfer</option>
                                                    <option value="Cash">Cash</option>
                                                    <option value="Cheque">Cheque</option>
                                                    <option value="Credit Card">Credit Card</option>
                                                    <option value="Other method">Other method</option>
                                                </select>
                                            </td>
                                            <td><button class="btn btn-black btn-md">Up to Date</button></td>
                                            <td>
                                                by <span>&nbsp &nbsp <input style="max-width:100px;" type="" name=""></span>
                                            </td>
                                        </tr>
                                        <?php }?>
                                    </tbody>
                                </table>
                                <h3>Total paid per period: <span>$22.00</span></h3>
                                <div class="text-center extar_p">
                                    <a href="property/step_five" class="btn btn-light btn-md">Back</a>
                                    <a href="property/rent_schedule/<?php echo $property_id?>" class="btn btn-primary btn-md">Next Step</a>
                                </div>  
                            </div>
                        </div>
                    </div>
                </form>
            </div>
            <script type="text/javascript">
                
            </script>
            <?php $this->load->view('front/footerlink');?>
        </body>
        </html>
