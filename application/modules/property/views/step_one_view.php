    <?php $this->load->view('front/headlink');?>
    <body>
        <div class="setup_multistape">
            <div class="container">
                <?=$this->load->view('front/head_nav');?>
                <h3></h3>
                <div class="row form-group">
                    <?=$this->load->view('front/step_div');?>
                </div>
            </div>
            <form class="container">
                <div class="row setup-content" id="step-1">
                    <div class="col-xs-12">
                        <div class="col-md-12 well text-center">
                            <h1 class="extra_heading"> Let's get Started!</h1>
                            <a href="stepTwo" class="btn btn-primary btn-md">Set up your first Rent Simple Property</a>
                        </div>
                </div>
            </div>
        </form> 
    </div>
    <?php $this->load->view('front/footerlink');?>
</body>
</html>
