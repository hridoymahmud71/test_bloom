<?php $this->load->view('front/headlink');?>
<body>
    <div class="setup_multistape">  
        <div class="container">
            <?php $this->load->view('front/head_nav');?>
            <h3>You'll find out about all these once set up your first Bloom property</h3>
            <div class="row form-group">
                <div class="col-xs-12">
                    <ul class="nav nav-pills nav-justified thumbnail setup-panel">
                        <li class="active"><a href="#step-1">
                            <h4 class="list-group-item-heading">Step 1</h4>
                            <p class="list-group-item-text">Let's get Started!</p>
                        </a></li>
                        <li class="disabled"><a href="#step-2">
                            <h4 class="list-group-item-heading">Step 2</h4>
                            <p class="list-group-item-text">Add Rental Property</p>
                        </a></li>
                        <li class="disabled"><a href="#step-3">
                            <h4 class="list-group-item-heading">Step 3</h4>
                            <p class="list-group-item-text">Add Tenants</p>
                        </a></li>
                        <li class="disabled"><a href="#step-4">
                            <h4 class="list-group-item-heading">Step 4</h4>
                            <p class="list-group-item-text">Lease & Rent Details</p>
                        </a></li>
                        <li class="disabled"><a href="#step-5">
                            <h4 class="list-group-item-heading">Step 5</h4>
                            <p class="list-group-item-text">Lease & Rent Details</p>
                        </a></li>
                        <li class="disabled"><a href="#step-6">
                            <h4 class="list-group-item-heading">Step 6</h4>
                            <p class="list-group-item-text">Pays the rent</p>
                        </a></li>        
                    </ul>
                </div>
            </div>
        </div>  
        <form class="container">
            <div class="row setup-content" id="step-1">
                <div class="col-xs-12">
                    <div class="col-md-12 well text-center">
                        <h1> Let's get Started!</h1>
                        <button id="activate-step-2" class="btn btn-primary btn-md">Set up your first Bloom property</button>
                        <br/><br/>
                        <p> <a href="#" >Get organised in under 5-minutes</a></p>
                    </div>
                    <div class="welcome_list">
                        <p><strong>RentingSmart can also help you:</strong></p>
                        <div class="row-fluid">
                            <div class="col-md-6">
                                <ul>
                                    <li>Get paid faster</li>
                                    <li>Automate invoices, late notices</li>
                                    <li>Create an expense</li>
                                    <li>Report a maintenance issue</li>
                                </ul>
                            </div>
                            <div class="col-md-6">
                                <ul>
                                    <li>Download a tenancy agreement</li>
                                    <li>Invite your tenants</li>
                                    <li>Find tradesmen</li>
                                    <li>Cash flow reports for your accountant</li>
                                </ul>
                            </div>
                        </div>
                        <p>You'll find out about all these once
                            <a href="#" class="create_rental_link">set up your first rental property</a>
                        </p>
                    </div>
                </div>
            </div>
        </form>
        <form class="container">
            <div class="row setup-content" id="step-2">
                <div class="col-xs-12">
                    <div class="col-md-12 well">
                        <h1 class="text-center"> Add Rental Property</h1>
                        <br/>
                        <br/>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="inputPropertyAddress">Property Address *</label>
                                <input type="text" class="form-control" id="inputPropertyAddress">
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="inputCity">City/Suburb *</label>
                                <input type="text" class="form-control" id="inputCity">
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="inputZipCode">Zip/Postal Code *</label>
                                <input type="text" class="form-control" id="inputZipCode">
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="inputCountry">Country *</label>
                                <select class="form-control" id="inputCountry">
                                    <option>Select Country</option>
                                    <option>1</option>
                                    <option>2</option>
                                    <option>3</option>
                                    <option>4</option>
                                    <option>5</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="inputState">State *</label>
                                <input type="text" class="form-control" id="inputState">
                            </div>
                        </div>
                        <div class="text-center extar_p">
                            <button id="activate-step-1" class="btn btn-light btn-md">Back</button>
                            <button id="activate-step-3" class="btn btn-primary btn-md">Next Step</button>
                        </div>
                    </div>
                </div>
            </div>
        </form>
        <form class="container">
            <div class="row setup-content" id="step-3">
                <div class="col-xs-12">
                    <div class="col-md-12 well text-center">
                        <h1 class="text-center"> Add Tenants to the property at trdgre</h1>
                        <!-- <form> -->               
                            <div class="container col-xs-12">
                                <div class="row clearfix">
                                    <div class="col-md-12 column">
                                        <table class="table table-bordered table-hover" id="tab_logic">
                                            <thead>
                                                <tr >
                                                    <th class="text-left">
                                                        #
                                                    </th>
                                                    <th class="text-left">
                                                        Name *
                                                    </th>
                                                    <th class="text-left">
                                                        Surname *
                                                    </th>
                                                    <th class="text-left">
                                                        Telephone
                                                    </th>
                                                    <th class="text-left">
                                                        Email
                                                    </th>                       
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <tr id='addr0'>
                                                    <td>
                                                        1
                                                    </td>
                                                    <td>
                                                        <input type="text" name='name0'  placeholder='First Name' class="form-control"/>
                                                    </td>
                                                    <td>
                                                        <input type="text" name='sur0' placeholder='Last Name' class="form-control"/>
                                                    </td>
                                                    <td>
                                                        <input type="tel" name='phone0' placeholder='Telephone' class="form-control"/>
                                                    </td>
                                                    <td>
                                                        <input type="text" name='email0' placeholder='Email' class="form-control"/>
                                                    </td>
                                                </tr>
                                                <tr id='addr1'></tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                                <a id="add_row" class="btn btn-success pull-left">Add Row</a><a id='delete_row' class="btn btn-danger pull-right">Delete Row</a>
                            </div>
                            <!-- </form> -->   
                            <div class="text-center extar_p">
                                <button  class="btn btn-light btn-md" action="action" onclick="window.history.go(-1);">Back</button>
                                <button id="activate-step-4" class="btn btn-primary btn-md">Next Step</button>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
            <form class="container">
                <div class="row setup-content" id="step-4">
                    <div class="col-xs-12">
                        <div class="col-md-12 well text-center">
                            <h1 class="text-center"> Lease & Rent Details for the property at <span>Name</span></h1>
                            <!--<form>-->
                                <div class="form-group">
                                    <h4 for="inputLeaseName" class="col-sm-2 control-label">Lease Name</h4>
                                    <div class="col-sm-10">
                                        <input type="text" class="form-control" id="inputLeaseName" placeholder="Lease Name">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <h4 for="inputLeaseName" class="col-sm-2 control-label">Lease dates</h4>
                                    <div class="col-sm-5">
                                        <label class="pull-left ss_exta_lebal">from:</label>  
                                        <div id="fromDate" class="input-group date" data-date-format="mm-dd-yyyy">
                                            <input class="form-control" type="text" readonly />
                                            <span class="input-group-addon"><i class="glyphicon glyphicon-calendar"></i></span>
                                        </div>
                                    </div>
                                    <div class="col-sm-5">
                                        <label class="pull-left ss_exta_lebal">to:</label> 
                                        <div id="toDate" class="input-group date" data-date-format="mm-dd-yyyy">
                                            <input class="form-control" type="text" readonly />
                                            <span class="input-group-addon"><i class="glyphicon glyphicon-calendar"></i></span>
                                        </div>
                                    </div>
                                </div>   
                                <div class="form-group">
                                    <p class="text-left" style=" padding-left:30px;">
                                        <label>Add signed lease agreement</label> 
                                        <span class="file_upload_wrapper" style="display: inline;">
                                            <input id="signed_lease_agreement" name="lease_aggrement" type="file" style="position: relative; left: 0px; display: inline;">
                                        </span>
                                    </p>
                                </div>  
                                <div class="form-group text-left">
                                    <h4 style="padding-left:30px;">Rent payments</h4>
                                    <div class="row">
                                        <div class="col-md-4">
                                            <label class="col-md-5 ss_exta_lebal text-center">How often?</label>
                                            <div class="col-md-7">
                                                <select class="form-control" id="inputday">
                                                    <option id="Weekly" value="Weekly" selected="selected">Weekly</option>
                                                    <option id="Fortnightly" value="Fortnightly">Fortnightly</option>
                                                    <option id="4Weekly" value="4Weekly">4 Weekly</option>
                                                    <option id="Monthly" value="Monthly">Monthly</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <label class="col-md-5 ss_exta_lebal"> How much per period?</label>
                                            <div class="col-md-7">
                                                <input type="text" class="form-control" id="inputHowmuchperperiod" placeholder="$0">
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <label class="col-md-5 ss_exta_lebal">How often?</label>
                                            <div class="col-md-7">
                                                <select class="form-control" id="inputhowOftenday">
                                                    <option id="Monday" value="1" selected="selected">Monday</option>
                                                    <option id="Tuesday" value="2">Tuesday</option>
                                                    <option id="Wednesday" value="3">Wednesday</option>
                                                    <option id="Thursday" value="4">Thursday</option>
                                                    <option id="Friday" value="5">Friday</option>
                                                    <option id="Saturday" value="6">Saturday</option>
                                                    <option id="Sunday" value="7">Sunday</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>    
                                </div> 
                                <!-- </form> -->   
                                <div class="text-center extar_p">
                                    <button  class="btn btn-light btn-md" action="action" onclick="window.history.go(-1);">Back</button>
                                    <button id="activate-step-5" class="btn btn-primary btn-md">Generate Rent Schedule</button>
                                </div> 
                            </div>
                        </div>
                    </div>
                </form>
                <form class="container">
                    <div class="row setup-content" id="step-5">
                        <div class="col-xs-12">
                            <div class="col-md-12 well ">
                                <h1 class="text-center"> Lease & Rent Details <small>for the property at <span>name</span></small> </h1>
                                <div class="step_5_header">
                                    <div class="step_5_header_cat">
                                        <h4>Lease dates</h4>
                                        <h4>Rent payments</h4>
                                        <h4>Bond</h4>
                                    </div>
                                    <div class="step_5_header_content">
                                        <p><span id="lease_start_date">February 10, 2018</span> to <span id="lease_end_date">February 10, 2019</span></p>
                                        <p><span class="ss_color">$</span> <span id="per_period_amount">22.00</span>paid<span id="payment_period">every 1 week</span>on<span id="pay_day">Monday of every week</span></p>
                                        <p>Bond provided <span><span class="ss_color">$</span><span id="bond_value">0</span></span></p>
                                    </div>
                                </div>
                            </div>
                            <div class="ss_price_list">
                                <h2>We've created your lease payment schedule!</h2>
                                <h4>For most landlords, the below will be correct right away.</h4>
                                <h6>Something a little unique about your lease or rent schedule? <span style="text-decoration: underline;">Click the edit button to customise.</span></h6>
                                <div class="step_5_table">
                                    <ul class="table_header top-header-div">
                                        <li>Period</li>
                                        <li>Due Date</li>
                                        <li>Lease Period</li>
                                        <li class="s5_amount_due">Amount Due</li>
                                        <li>
                                        </li></ul>
                                        <ul class="table_row schedule_list">
                                            <li id="num">1</li>
                                            <li>
                                                <h1>Due Date</h1>
                                                <span class="input_text" id="due_date" style="display: inline;">10 February 2018</span>
                                                <div id="fromDate1" style="display:none" class="input-group input-small date" data-date-format="mm-dd-yyyy" name="due_date[]">
                                                    <input class="form-control" type="text" readonly />
                                                    <span class="input-group-addon"><i class="glyphicon glyphicon-calendar"></i></span>
                                                </div>
                                            </li>
                                            <li>
                                                <h1>Lease Period</h1>
                                                <span class="input_text" id="lease_payment_period" style="display: inline;">10 Feb - 11 Feb</span>
                                                <div style=" display:none;" id="toDate1" class="input-group input-small date" data-date-format="mm-dd-yyyy" name="due_date[]">
                                                    <input class="form-control" type="text" readonly />
                                                    <span class="input-group-addon"><i class="glyphicon glyphicon-calendar"></i></span>
                                                </div>
                                            </li>
                                            <li class="s5_amount_due">
                                                <h1>Amount Due</h1>
                                                <span class="input_text" id="period_amount" style="display: inline;">$50.29</span>
                                                <input class="input-small dollar_blue pay_period_amount" type="text" name="amount_due[]" value="6.29" style="display: none;">
                                            </li>
                                            <li>
                                                <span class="edit_lease_schedule" style="display: inline;"><a href="javascript:void(0);">edit</a></span>
                                                <span class="save_lease_schedule" style="display: none;"><a href="javascript:void(0);" class="save_edits">save</a> <a href="javascript:void(0);" class="cencel_edit" style="position:absolute; margin-left: 5px;">| cancel</a></span>
                                            </li>
                                        </ul>
                                    </div>
                                    <div class="text-center extar_p">
                                        <button  class="btn btn-light btn-md" action="action" onclick="window.history.go(-1);">Back</button>
                                        <button id="activate-step-6" class="btn btn-primary btn-md">Good to go, Captain</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                    <form class="container">
                        <div class="row setup-content" id="step-6">
                            <div class="col-xs-12">
                                <div class="col-md-12 well">
                                    <h1 class="text-center"> And who actually pays the rent?</h1>
                                    <table class="table">
                                        <thead>
                                            <tr>
                                                <th scope="col">Tenant Name</th>
                                                <th scope="col">Share Paid</th>
                                                <th scope="col">Pay Method</th>
                                                <th scope="col">Payment Status</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <th scope="row">Name <br> last name</th>
                                                <td><input class="form-control form-control-lg" name="amount_due[]" type="number" value="22.00" placeholder="0.00"></td>
                                                <td>
                                                    <select class="form-control" id="inputday">
                                                        <option value="Select Method">Select Method</option>
                                                        <option value="Bank Transfer">Bank Transfer</option>
                                                        <option value="Cash">Cash</option>
                                                        <option value="Cheque">Cheque</option>
                                                        <option value="Credit Card">Credit Card</option>
                                                        <option value="Other method">Other method</option>
                                                    </select>
                                                </td>
                                                <td><button class="btn btn-black btn-md">Up to Date</button></td>
                                            </tr>
                                        </tbody>
                                    </table>
                                    <h3>Total paid per period: <span>$22.00</span></h3>
                                    <div class="text-center extar_p">
                                        <button  class="btn btn-light btn-md" action="action" onclick="window.history.go(-1);">Back</button>
                                        <a href="confrimpayment.html"  class="btn btn-primary btn-md">Next Step</a>
                                    </div>  
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
                <?php $this->load->view('front/footerlink');?>
            </body>
            </html>
