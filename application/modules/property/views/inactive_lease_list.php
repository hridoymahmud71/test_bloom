<?php $this->load->view('front/headlink'); ?>
<?php $this->load->view('front/archived_top_menu'); ?>
<div class="container">
    <?php $this->load->view('front/head_nav'); ?>

    <h4>Add/Choose Lease
        <a href="#" data-toggle="modal" data-target="#myModal"><span class="glyphicon glyphicon-cog"
                                                                     aria-hidden="true"></span></a>
    </h4>
    <div class="modal fade" id="myModal" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Select a Lease or add a new lease</h4>
                </div>
                <form action="property/property_details/<?= $property_id; ?>" method="post">
                    <div class="modal-body">
                        <h4>Lease Option</h4>
                        <select class="form-control" name="lease_id">
                            <option value="new_lease">Add a new lease</option>
                            <?php if ($all_leases) { ?>
                                <?php foreach ($all_leases as $lease) { ?>
                                    <option value="<?= $lease['lease_id']; ?>"><?= $lease['lease_name']; ?></option>
                                <?php } ?>
                            <?php } ?>
                        </select>
                    </div>
                    <div class="modal-footer">
                        <input type="submit" class="btn btn-success" value="Confirm" name="">
                    </div>
                </form>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="ss_container" style="overflow: inherit">
            <h2> Archived Leases at&nbsp;<?= $property[0]['property_address'] ?>
            </h2>
            <div class="ss_bound_content" style="overflow: inherit">

                <!-- Tab panes -->
                <div class="tab-content">
                    <div class="row-fluid document-list-header top-table-name">
                        <div class="col-md-2">Created At</div>
                        <div class="col-md-2">Lease Name</div>
                        <div class="col-md-2">Lease Period</div>
                        <div class="col-md-2">Tenants</div>
                        <div class="col-md-2">Rent</div>
                        <div class="col-md-2">Action</div>
                    </div>
                    <div role="tabpanel" class="tab-pane active" id="dall">
                        <form id="all_submit_form" action="lease/doc_download" method="post">
                            <?php if (count($inactive_leases) > 0) { ?>
                            <?php foreach ($inactive_leases

                            as $inactive_lease) { ?>
                            <div class="row-fluid documents-list_area" style="overflow: inherit;">

                                <div class="rep_href row">
                                    <div class="col-md-2">
                                        <?= date("jS F, Y G:i a", strtotime($inactive_lease['created_at'])); ?>
                                    </div>
                                    <div class="col-md-2">
                                        <?= $inactive_lease['lease_name'] ?>
                                    </div>
                                    <div class="col-md-2">
                                        <?= date("jS F, Y", strtotime($inactive_lease['lease_start_date'])); ?>
                                        <br>
                                        to
                                        <br>
                                        <?= date("jS F, Y", strtotime($inactive_lease['lease_end_date'])); ?>
                                    </div>
                                    <span class="col-md-2">
                                                <?php if ($inactive_lease['tenants_with_lease_details']) { ?>
                                                    <?php foreach ($inactive_lease['tenants_with_lease_details'] as $tenant_with_lease_detail) { ?>
                                                        <li><?= $tenant_with_lease_detail['user_fname'] . ' ' . $tenant_with_lease_detail['user_lname']; ?></li>
                                                    <?php } ?>
                                                <?php } ?>
                                            </span>
                                    <div class="col-md-2">
                                        <?= number_format($inactive_lease['lease_per_period_payment'], 2, '.', ',') ?>
                                        <?php if (array_key_exists($inactive_lease['lease_pay_type'], $per_array)) {
                                            echo '/' . $per_array[$inactive_lease['lease_pay_type']];
                                        } ?>
                                    </div>
                                    <div class="col-md-2 ">

                                        <div class="row">
                                            <script>
                                                $( function() {
                                                    $( "#bmenu" ).menu();
                                                } );
                                            </script>
                                            <style>
                                                .ui-menu { width: 150px; }
                                            </style>
                                            <ul id="bmenu">
                                                <li>
                                                    <div>
                                                        List

                                                    </div>
                                                    <ul>
                                                        <li>
                                                            <div>
                                                                <a href="Dashboard/<?= $property_id; ?>?archived_lease_id=<?= $inactive_lease['lease_id'] ?>">Dashboard</a>
                                                            </div>
                                                        </li>
                                                        <li>
                                                            <div>
                                                                Lease
                                                            </div>
                                                            <ul>
                                                                <li>
                                                                    <div>
                                                                        <a href="leaseDescription/<?= $property_id; ?>/?archived_lease_id=<?= $inactive_lease['lease_id'] ?>">Lease</a>
                                                                    </div>
                                                                </li>
                                                                <li>
                                                                    <div>
                                                                        <a href="leaseBonds/<?= $property_id; ?>/?archived_lease_id=<?= $inactive_lease['lease_id'] ?>">Bonds</a>
                                                                    </div>
                                                                </li>
                                                                <li>
                                                                    <div>
                                                                        <a href="leaseTenants/<?= $property_id; ?>/?archived_lease_id=<?= $inactive_lease['lease_id'] ?>">Teants</a>
                                                                    </div>
                                                                </li>
                                                                <li>
                                                                    <div>
                                                                        <a href="leaseDocument/<?= $property_id; ?>/?archived_lease_id=<?= $inactive_lease['lease_id'] ?>">Tenancy
                                                                            Documentation
                                                                        </a>
                                                                    </div>
                                                                </li>
                                                            </ul>
                                                        </li>
                                                        <li>
                                                            <div>
                                                                Rent
                                                            </div>
                                                            <ul>
                                                                <li>
                                                                    <div>
                                                                        <a href="rentSchedule/<?= $property_id; ?>/0?archived_lease_id=<?= $inactive_lease['lease_id'] ?>">
                                                                            Rent Schedule
                                                                        </a>
                                                                    </div>
                                                                </li>
                                                            </ul>
                                                        </li>
                                                        <li>
                                                            <div>
                                                                Maintenance
                                                            </div>
                                                            <ul>
                                                                <li>
                                                                    <div>
                                                                        <a href="Allmaintenance/<?= $property_id; ?>/?archived_lease_id=<?= $inactive_lease['lease_id'] ?>">
                                                                            View All Maintenance
                                                                        </a>
                                                                    </div>
                                                                </li>
                                                            </ul>
                                                        </li>
                                                        <li>
                                                            <div>
                                                                Inspection
                                                            </div>
                                                            <ul>
                                                                <li>
                                                                    <div>
                                                                        <a href="inspectionList/<?= $property_id; ?>/all/?archived_lease_id=<?= $inactive_lease['lease_id'] ?>">
                                                                            All inspections
                                                                        </a>
                                                                    </div>
                                                                </li>
                                                            </ul>
                                                        </li>
                                                        <li>
                                                            <div>
                                                                Notes
                                                            </div>
                                                            <ul>
                                                                <li>
                                                                    <div>
                                                                        <a href="noteList/<?= $property_id; ?>/?archived_lease_id=<?= $inactive_lease['lease_id'] ?>">
                                                                            View notes
                                                                        </a>
                                                                    </div>
                                                                </li>
                                                            </ul>
                                                        </li>
                                                        <li>
                                                            <div>
                                                                Water
                                                            </div>
                                                            <ul>
                                                                <li>
                                                                    <div>
                                                                        <a href="waterInvoiceList/<?= $property_id; ?>/?archived_lease_id=<?= $inactive_lease['lease_id'] ?>">
                                                                            Water
                                                                            Invoice List
                                                                        </a>
                                                                    </div>
                                                                </li>
                                                            </ul>
                                                        </li>
                                                        <li>
                                                            <div>
                                                                Expenses
                                                            </div>
                                                            <ul>
                                                                <li>
                                                                    <div>
                                                                        <a href="viewExpense/<?= $property_id; ?>/?archived_lease_id=<?= $inactive_lease['lease_id'] ?>">
                                                                           View expenses
                                                                        </a>
                                                                    </div>
                                                                </li>
                                                            </ul>
                                                        </li>
                                                        <li>
                                                            <div>
                                                                Other
                                                            </div>
                                                            <ul>
                                                                <li>
                                                                    <div>
                                                                        <a href="moneyIn/<?= $property_id; ?>/?archived_lease_id=<?= $inactive_lease['lease_id'] ?>">Other money in</a>
                                                                    </div>
                                                                </li>
                                                            </ul>
                                                        </li>
                                                        <li>
                                                            <div>
                                                                Messages
                                                            </div>
                                                            <ul>
                                                                <li>
                                                                    <div>
                                                                        <a href="Message/<?= $property_id; ?>/?archived_lease_id=<?= $inactive_lease['lease_id'] ?>">Message</a>
                                                                    </div>
                                                                </li>
                                                            </ul>
                                                        </li>
                                                    </ul>
                                                </li>

                                            </ul>

                                        </div>
                                    </div>

                                </div>
                            </div>
                    </div>
                    <?php } ?>
                    <?php } ?>
                    <div id="get_val_here"></div>
                    </form>
                </div>



            </div>
        </div>
    </div>
</div>
</div>
<script src="//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>


<?php $this->load->view('front/footerlink'); ?>
</body>
</html>