<?php $this->load->view('front/headlink'); ?>
<body>
<div class="dashbord_poperty">
    <?php $this->load->view('front/top_menu'); ?>
    <div class="container">
        <?php $this->load->view('front/head_nav'); ?>

        <h4 class="ss_heading_s">Current Lease: <?= $lease_detail[0]['lease_name'] ?>
            <?php
            $lease_time_diff = (int)((strtotime($lease_detail[0]['lease_end_date']) - strtotime(date('Y-m-d'))) / 86400);
            $lease_time_diff_string = sprintf("(%d days to go)", $lease_time_diff);

            if ($lease_time_diff < 0) {
                $lease_time_diff_string = sprintf("(ended %d day(s) ago)", abs($lease_time_diff));
            }
            ?>
            <strong><?= $lease_time_diff_string ?></strong>
            <a href="#" data-toggle="modal" style="display: none" id="settings_btn">
                <span class="glyphicon glyphicon-cog" aria-hidden="true"></span>
            </a>
            <?php if ($lease_time_diff < 0 && !$is_archived) { ?>
                <a id="archiveBtn" class="btn btn-light" href="deactivateLease/<?= $lease_detail[0]['lease_id'] ?>">Archive
                    Lease</a>
            <?php } ?>
        </h4>
        <div class="container">
            <div class="modal fade" id="leaseModal" role="dialog">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                            <h4 class="modal-title">Select a Lease or add a new lease</h4>
                        </div>
                        <form action="property/property_details/<?= $property_id; ?>" method="post">
                            <div class="modal-body">
                                <h4>Lease Option</h4>
                                <select class="form-control" name="lease_id" id="lease">
                                    <option value="new_lease">Add a new lease</option>
                                    <?php foreach ($all_lease as $row) { ?>
                                        <option <?php if ($row['lease_id'] == $lease_detail[0]['lease_id']) {
                                            echo "selected";
                                        } ?> value="<?= $row['lease_id']; ?>"
                                             disabled><?= $row['lease_name']; ?></option>
                                    <?php } ?>
                                </select>
                            </div>
                            <div class="modal-footer">
                                <input type="submit" class="btn btn-success" value="Confirm" name="">
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <div class="modal fade" id="vacateModal" role="dialog">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                            <h4 class="modal-title">Vacate Tenants For
                                Lease: <?= $lease_detail[0]['lease_name'] ?></h4>

                        </div>
                        <?php
                        $vacatable = false;
                        $current_date = date('Y-m-d');

                        if (strtotime($lease_detail[0]['lease_end_date']) <= strtotime(date($current_date))) {
                            $vacatable = true;
                        }
                        ?>
                        <form id="vacate_form" vacate_form_success="0"
                              lease_vacated="<?= $lease_detail[0]['lease_vacated'] ?>"
                              action="property/vacate_tenants/<?= $lease_detail[0]['lease_id'] ?>"
                              method="post">
                            <div class="modal-body">
                                <h5><strong>Lease
                                        Period</strong>: <?= date('jS F, Y', strtotime($lease_detail[0]['lease_start_date'])); ?>
                                    to <?= date('jS F, Y', strtotime($lease_detail[0]['lease_end_date'])); ?></h5>

                                <div class="form-group" <?= $vacatable ? " style='display:none' " : ""; ?> >
                                    <br>
                                    <h5 style="color: darkred">**You cannot vacant tenants.Lease is not complete
                                        yet</h5>
                                </div>
                                <div class="form-group" <?= !$vacatable ? " style='display:none' " : ""; ?>>
                                    <label for="vacate_date">Vacate date</label>
                                    <input class="form-control" type="text" id="vacate_date"
                                           min_vacate_date="<?= date('Y,m,d', strtotime($lease_detail[0]['lease_end_date'])); ?>"
                                           value=""
                                           name="vacate_date" readonly>
                                    <small id="vacate_date_help" class="form-text text-muted">
                                        Enter a vacate date to mark this tenancy as completed
                                    </small>
                                </div>

                            </div>
                            <div class="modal-footer" <?= !$vacatable ? " style='display:none' " : ""; ?> >
                                <button type="submit" id="vacate_tenant_form_submit_btn" class="btn btn-success">
                                    Confirm
                                </button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <div><img src="assets/img/shadow_product_top.png" class="img-responsive"></div>
        <div class="row ss_main_content">

            <div class="col-sm-8">

                <!--<div class="ss_graph">
                    <div id="bar-graph"></div>
                    <div class="ss_color">
                        <div><span class="green_c"></span> Income</div>
                        <div><span class="green_r"></span> Expense</div>
                    </div>
                </div>-->
                <div class="ss_graph">
                    <div id="line-graph"></div>
                    <div class="ss_color">
                        <div><span class="green_c"></span> Income</div>
                        <div><span class="green_r"></span> Expense</div>
                    </div>
                </div>
                <div class="dashboard_element rent-payments">
                    <h1>Rent payments</h1>
                    <?php if (!$is_archived) { ?>
                        <a href="rentSchedule/<?= $property_id; ?>/3" id="mark_as_paid" class="btn btn-light"><span>Mark Rent Received</span></a>
                    <?php } ?>
                    <a href="rentSchedule/<?= $property_id; ?>/4<?= $archived_string ?>" id="by_rent_received"
                       class="btn btn-light"><span>Show Rent Received</span></a>
                </div>

                <!--<pre><?php /*print_r($tenants)*/ ?></pre>-->

                <br/> <br/>
                <div class="ss_no_bg">
                    <div class="row" id="ss_d_top_content">

                        <div class="col-md-6">
                            <div class="ss_box">
                                <h2>Rent</h2>

                                <?php {
                                    $rent_status_label_array = array('up_to_date', 'arrears', 'ahead', 'arrears');
                                    $rent_status = 4;//do not know
                                    $rent_status_text = $rent_status_text = ucwords(str_replace('_', ' ', $rent_status_label_array[$rent_status - 1]));
                                    $extra_rent_label_style = '';
                                    $rent_amt = 0.00;

                                    if ($lease_detail_tenant) {
                                        $rent_status = $lease_detail_tenant[0]['payment_update_status'];
                                        $rent_status_text = ucwords(str_replace('_', ' ', $rent_status_label_array[$rent_status - 1]));
                                        if ($rent_status == 2) {
                                            $extra_rent_label_style = " style='background:#ba2f2c;' ";
                                        }

                                        $rent_amt = array_sum(array_column($lease_detail_tenant, 'payment_update_by'));
                                    }
                                } ?>


                                <?php $rent_status = $this->utility_model->calculate_rent_status_by_lease($lease_detail[0]['lease_id']); ?>
                                <?php if (!empty($rent_status)) { ?>
                                    <h4 class="">
                                        <span class="ss_price_text">
                                            <?= $rent_status['amount'] != 0 ? "$" . $rent_status['amount'] : "" ?>
                                        </span>
                                    </h4>
                                    <p>
                                    <span id="rent_status" class=" label label-<?= $rent_status['label'] ?> label-paid">
                                     <?= $rent_status['status'] ?>
                                        </span>
                                    </p>

                                <?php } ?>
                                <br/>
                                <a href="rentSchedule/<?= $property_id; ?>/0<?= $archived_string ?>"
                                   class="btn btn-light">Go to
                                    Schedule</a>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="ss_box">
                                <h2>Expenses</h2>
                                <?php
                                $total_expense_paid = 0.00;
                                $tep_1 = !empty($expense_paid_amount[0]['total_paid']) ? $expense_paid_amount[0]['total_paid'] : 0.00;
                                $tep_2 = !empty($expense_paid_amount[0]['total_partial_paid']) ? $expense_paid_amount[0]['total_partial_paid'] : 0.00;
                                $total_expense_paid = $tep_1 + $tep_2;
                                ?>

                                <p>
                                    <strong>Paid</strong> this month
                                    <span class="ss_price_text">$<?= $total_expense_paid ?></span>
                                </p>
                                <p>
                                    <strong>Due</strong> in next 30 days
                                    <span class="ss_price_text">$<?php if ($expense_due_amount[0]['total_due'] == '') {
                                            echo '0.00';
                                        } else {
                                            echo $expense_due_amount[0]['total_due'];
                                        } ?>
                                    </span>
                                </p>
                                <br/>
                                <p>
                                    <strong>Overdue</strong>
                                    <?php if ($expense_overdue_amount[0]['total_overdue'] == '') { ?>
                                        <span class="label label-danger">
                                        $0.00
                                    </span>
                                    <?php } else { ?>
                                        <span class="label label-danger">
                                        $<?= ($expense_overdue_amount[0]['total_overdue']); ?>
                                    </span>
                                    <?php } ?>
                                </p>
                                <br/>
                                <a href="viewExpense/<?= $property_id; ?><?= $archived_string ?>" class="btn btn-light">Go
                                    to
                                    Expenses</a>
                            </div>
                        </div>


                        <div class="col-md-6">
                            <div class="ss_box">
                                <h2>Maintenance</h2>

                                <div class="col-md-3"></div>
                                <div class="col-md-6">
                                    <ul class="list-group ">
                                        <li class="list-group-item" style="background-color: #0bafd2;color: white"> New
                                            : <?= $count_new_maintanance; ?> </li>
                                        <li class="list-group-item" style="background-color: #0bafd2;color: white">Open
                                            : <?= $count_open_maintanance; ?> </li>
                                        <li class="list-group-item" style="background-color: #0bafd2;color: white">
                                            Closed : <?= $count_closed_maintanance; ?> </li>
                                        <li class="list-group-item" style="background-color: #0bafd2;color: white">
                                            Archived : <?= $count_archived_maintanance; ?> </li>

                                    </ul>
                                </div>
                                <div class="col-md-3"></div>


                                <a href="Allmaintenance/<?= $property_id; ?><?= $archived_string ?>"
                                   class="btn btn-light">View All</a>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="ss_box">

                                <h2>Water</h2>
                                <p><strong>Count</strong>&nbsp;Total:<?= $water_invoice_count; ?> |
                                    Paid:<?= $paid_water_invoice_count; ?> | Due:<?= $due_water_invoice_count; ?></p>
                                <p><strong>Amount</strong>&nbsp;Total:$<?= $water_invoice_total; ?> |
                                    Paid:$<?= $paid_water_invoice_total; ?> | Due:$<?= $due_water_invoice_total; ?></p>


                                <br/>
                                <a href="waterInvoiceList/<?= $property_id; ?><?= $archived_string ?>"
                                   class="btn btn-light">View All</a>
                            </div>

                        </div>
                        <div class=" rent-payments">

                            <div class="clear" style="height: 15px;"></div>
                            <?php $t = 0;
                            $tenant_names = array();
                            ?>

                            <?php foreach ($get_all_tenant as $row) { ?>
                                <?php $tenant_names[] = $row['user_fname'] . ' ' . $row['user_lname']; ?>
                            <?php } ?>
                            <?php foreach ($get_all_tenant as $row) {
                                $t++; ?>

                                <?php if ($t <= 1) { ?>
                                    <ul class="payments_list" style="display: none;">
                                        <li class="tenant_name"
                                            id="tenant-name">
                                            <?= implode(" & ", $tenant_names) ?>
                                        </li>
                                        <?php if ($row['payment_update_status'] == 1) { ?>
                                            <li>
                                                <p>
                                                    <strong><span id="tenant-total"></span></strong>
                                                    <span id="tenant-status" class="label label-paid">Up to date</span>
                                                </p>
                                            </li>
                                        <?php } elseif ($row['payment_update_status'] == 3) { ?>
                                            <li>
                                                <p>
                                                    <strong><span
                                                                id="tenant-total"><?= $row['payment_update_by'] * count($get_all_tenant) ?></span></strong>
                                                    <span id="tenant-status" class="label label-paid">Ahead</span>
                                                </p>
                                            </li>
                                        <?php } elseif ($row['payment_update_status'] == 4) { ?>
                                            <li>
                                                <p>
                                                    <strong><span
                                                                id="tenant-total"><?= $row['payment_update_by'] * count($get_all_tenant) ?></span></strong>
                                                    <span id="tenant-status" style="background:#337ab7;"
                                                          class="label label-paid">Arrears</span>
                                                </p>
                                            </li>
                                        <?php } elseif ($row['payment_update_status'] == 2) { ?>
                                            <li>
                                                <p>
                                                    <strong><span
                                                                id="tenant-total"><?= $row['payment_update_by'] * count($get_all_tenant) ?></span></strong>
                                                    <span id="tenant-status" style="background:#337ab7;"
                                                          class="label label-paid">Arrears</span>
                                                </p>
                                            </li>
                                        <?php } ?>
                                        <?php if ($row['payment_update_status'] == 2 || $row['payment_update_status'] == 4) { ?>
                                            <li>
                                                <p>
                                                    <button class="btn-light tenant_button">Show Details</button>
                                                </p>
                                            </li>
                                            <div>
                                                <table style="display:none;" class="table tenant_div">
                                                    <thead>
                                                    <tr>
                                                        <th>Period</th>
                                                        <th>Due Date</th>
                                                        <th>Amount</th>
                                                        <th>Late</th>
                                                    </tr>
                                                    </thead>
                                                    <tbody>
                                                    <?php foreach ($all_tenant_payment_info as $row_two) { ?>
                                                        <?php if ($row['tenant_id'] == $row_two['tenant_id']) { ?>
                                                            <tr>
                                                                <td><?= date('M d', strtotime($row_two['schedule_start_date'])); ?>
                                                                    - <?= date('M d', strtotime($row_two['schedule_end_date'])); ?></td>
                                                                <td><?= date('F d, Y', strtotime($row_two['schedule_start_date'])); ?></td>
                                                                <td>
                                                                    $<?= $row_two['shared_amount'] * count($get_all_tenant); ?></td>
                                                                <td> <?php echo(abs(strtotime($row_two['schedule_start_date']) - strtotime(date('y-m-d'))) / 86400); ?></td>
                                                            </tr>
                                                        <?php } ?>
                                                    <?php } ?>
                                                    </tbody>
                                                </table>
                                            </div>
                                        <?php } ?>
                                        <div id="item_1" style="display:none;">
                                            <a href="#" class="reminder" id="reminder">Send <span id="u-name">Kk</span>
                                                a
                                                reminder about outstanding rents</a>
                                        </div>
                                    </ul>
                                <?php } ?>
                            <?php } ?>
                            <ul id="last_flag1" style="display:none;"></ul>
                        </div>
                        <div class="clear"></div>
                    </div>
                </div>
            </div>
            <div class=" col-sm-4 ss_side_right">

                <div class="ss_box">
                    <h2>Routine Inspection</h2>
                    <?php if ($upcoming_inspections) { ?>
                        <?php foreach ($upcoming_inspections as $upcoming_inspection) { ?>
                            <?php
                            $curr_datetime = date("Y-m-d H:i:s");
                            $insp_st_datetime = $upcoming_inspection['inspection_date'] . ' ' . $upcoming_inspection['inspection_start_time'];

                            $insp_overdue = false;

                            if (strtotime($curr_datetime) > strtotime($insp_st_datetime)) {
                                $insp_overdue = true;
                            }
                            ?>

                            <div class="panel-body" style="margin: 2px 0 2px 0;border-bottom: 1px solid grey">
                                <p>
                                    <?= ' <span class="glyphicon glyphicon-calendar" aria-hidden="true"></span> &nbsp;'
                                    . date("jS F, Y", strtotime($upcoming_inspection['inspection_date']))
                                    . ' <span class="glyphicon glyphicon-time" aria-hidden="true"></span> '
                                    . date("g:i a", strtotime($upcoming_inspection['inspection_start_time']))
                                    . ' to '
                                    . date("g:i a", strtotime($upcoming_inspection['inspection_end_time']))

                                    ?>
                                </p>
                                <?php if ($insp_overdue) { ?>
                                    <p class="label label-danger">
                                        Overdue
                                    </p>
                                <?php } ?>
                            </div>


                        <?php } ?>
                    <?php } else { ?>
                        <p>No Routine Inspections currently scheduled. <br>
                            <?php if (!$archived_lease) { ?>
                                <a target="_blank" href="addInspection/<?= $property_id; ?>">Click
                                    here</a> to schedule one
                                now
                            <?php } ?>
                        </p>
                    <?php } ?>
                    <p>
                        <a href="inspectionList/<?= $property_id; ?>/upcoming<?= $archived_string ?>" type="submit"
                           class="btn btn-light btn-sm allside-margin-25">
                            Go to Page
                        </a>
                    </p>
                </div>

                <div class="ss_box">
                    <h2>Rent Details</h2>
                    <p>
                            <span class="ss_price_text">$
                                <?= $lease_detail[0]['lease_per_period_payment']; ?>
                            </span> <?php if ($lease_detail[0]['lease_pay_type'] == 1) {
                            echo "per week";
                        } elseif ($lease_detail[0]['lease_pay_type'] == 2) {
                            echo "per fornnight";
                        } elseif ($lease_detail[0]['lease_pay_type'] == 3) {
                            echo "every 28 days";
                        } else {
                            echo "in 1 month";
                        } ?>
                    </p>
                    <p>
                        <a href="rentSchedule/<?= $property_id; ?>/0<?= $archived_string ?>" type="submit"
                           class="btn btn-light btn-sm allside-margin-25">
                            Go to Page
                        </a>
                    </p>
                </div>

                <div class="ss_box">
                    <h2> Lease Details</h2>
                    <p>
                        <?php echo date('d F, Y', strtotime($lease_detail[0]['lease_start_date'])); ?>
                        - <?php echo date('d F, Y', strtotime($lease_detail[0]['lease_end_date'])); ?>
                    </p>
                    <?php if ($lease_time_diff < 0) { ?>
                        <p class="label label-danger">
                            Lease Expired
                        </p>
                    <?php } ?>
                    <p>
                        <a href="leaseDescription/<?= $property_id; ?><?= $archived_string ?>" type="submit"
                           class="btn btn-light btn-sm allside-margin-25">
                            Go to Page
                        </a>
                    </p>
                </div>
            </div>
        </div>
    </div>
</div>
<?php
$user_fname = $this->session->userdata('user_fname');
$user_lname = $this->session->userdata('user_lname');
?>
<?php foreach ($result as $data) {
} ?>
<!-- Modal -->
<div class="modal fade" data-backdrop="static" data-keyboard="false" id="property_photos_info" tabindex="-1"
     role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <form method="POST" action="property/update_property_photos_info/<?= $property_id; ?>"
                  enctype="multipart/form-data">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title"
                        id="myModalLabel"><?= $property_info[0]['property_address'] . ', ' . $property_info[0]['city'] . ', ' . $property_info[0]['zip_code']; ?></h4>
                </div>
                <div class="modal-body">
                    <?php foreach ($result2 as $data2) { ?>bBack to dashboa<?= $property_info[0]['property_address'] . ', ' . $property_info[0]['city'] . ', ' . $property_info[0]['zip_code']; ?>
                        <span id="pImg<?php echo $data2['property_photos_id']; ?>">
                  <img style="width: 80px; height: 80px;"
                       src="<?php echo base_url(); ?>uploads/property_file/<?php echo $data2['property_photo']; ?>">
                            <!--<span class="cross_2">x</span>   -->
                  <button type="button" name="delete_btn" data-id3="<?php echo $data2['property_photos_id']; ?>"
                          class="btn btn-xs btn-danger btn_delete">x</button>
                </span>
                    <?php } ?> <br> <br>
                    <label for="add_property_photos">Click to add more photos of your property</label>
                    <div class="form-group inputDnD">
                        <div class="dropzone">
                            <div class="dz-message">
                                <h3> Drop File</h3>
                            </div>
                        </div>
                        <div class="previews" id="preview"></div>
                    </div>
                    <div class="row-fluid">
                        <div class="col-md-6">
                            <label for="owner">Owner of the property (if not name of landlord)</label>
                            <input type="text" id="owner" name="owner" value="<?php echo $user_fname;
                            echo " ";
                            echo $user_lname; ?>" class="form-control" disabled="disabled">
                        </div>
                    </div>
                    <div class="row-fluid">
                        <div class="col-md-6 address_c">
                            <label class="" for="address">Property Address <span>*</span></label>
                            <input type="text" id="address" name="property_address"
                                   value="<?php echo $data['property_address']; ?>" class="form-control" required>
                        </div>
                        <div class="col-md-6">
                            <label class="" for="city">City/Suburb<span>*</span></label>
                            <input type="text" id="city" name="city" value="<?php echo $data['city']; ?>"
                                   class="form-control" required>
                        </div>
                    </div>
                    <div class="row-fluid">
                        <div class="col-md-4">
                            <label class="" for="postal">Zip/Postal Code<span>*</span></label>
                            <input type="text" id="postal" name="zip_code" value="<?php echo $data['zip_code']; ?>"
                                   class="form-control" required>
                        </div>
                        <input type="hidden" id="countryValue" name="country_val"
                               value="<?php echo $data['country']; ?>">
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="inputCountry">Country <span>*</span></label>
                                <select class="form-control" name="country" id="inputCountry"
                                        onChange="getState(this.value);" required>
                                    <!-- <option value="">Select Country</option> -->
                                    <option value="13">Australia</option>
                                </select>
                                <!-- <select class="form-control" name="country" id="inputCountry" onChange="getState(this.value);" required>
                      <option value="">Select Country</option>
                      <?php foreach ($all_country as $row) { ?>
                      <option value="<?= $row['country_id']; ?>" <?php if ($row['country_id'] == $data['country']) {
                                    echo "selected";
                                } ?>><?= $row['country_name']; ?></option>
                      <?php } ?>
                    </select> -->
                                <strong class="text-danger" id="err_cntry_msg"></strong>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label for="inputState">State <span>*</span></label>
                                <select class='form-control' name='state' id='inputState'>
                                    <option value="">Select State</option>
                                    <?php foreach ($get_state as $state_row) { ?>
                                        <option
                                            <?php if ($property_info[0]['state'] == $state_row['state_id']) {
                                                echo "selected";
                                            } ?>
                                                value="<?= $state_row['state_id']; ?>">
                                            <?= $state_row['state_name']; ?>
                                        </option>
                                    <?php } ?>
                                </select>
                                <strong class="text-danger" id="err_state_msg"></strong>
                            </div>
                        </div>
                    </div>
                    <div class="row-fluid">
                        <div class="col-md-4">
                            <label for="bedrooms">Bedrooms</label>
                            <select name="bedrooms" id="bedrooms" class="form-control">
                                <option value="1" <?php if ($data['bedrooms'] == 1) {
                                    echo "selected";
                                } ?> >1
                                </option>
                                <option value="2" <?php if ($data['bedrooms'] == 2) {
                                    echo "selected";
                                } ?>>2
                                </option>
                                <option value="3" <?php if ($data['bedrooms'] == 3) {
                                    echo "selected";
                                } ?>>3
                                </option>
                                <option value="4" <?php if ($data['bedrooms'] == '4') {
                                    echo "selected";
                                } ?>>4+
                                </option>
                            </select>
                        </div>
                        <div class="col-md-4">
                            <label for="bathrooms">Bathrooms</label>
                            <select name="bathrooms" class="form-control">
                                <option value="1" <?php if ($data['bathrooms'] == 1) {
                                    echo "selected";
                                } ?>>1
                                </option>
                                <option value="2" <?php if ($data['bathrooms'] == 2) {
                                    echo "selected";
                                } ?>>2
                                </option>
                                <option value="3" <?php if ($data['bathrooms'] == 3) {
                                    echo "selected";
                                } ?>>3
                                </option>
                                <option value="4" <?php if ($data['bathrooms'] == '4') {
                                    echo "selected";
                                } ?>>4+
                                </option>
                            </select>
                        </div>
                        <div class="col-md-4">
                            <label for="carspaces">Car spaces</label>
                            <select name="carspaces" class="form-control">
                                <option value="1" <?php if ($data['carspaces'] == 1) {
                                    echo "selected";
                                } ?>>1
                                </option>
                                <option value="2" <?php if ($data['carspaces'] == 2) {
                                    echo "selected";
                                } ?>>2
                                </option>
                                <option value="3" <?php if ($data['carspaces'] == 3) {
                                    echo "selected";
                                } ?>>3
                                </option>
                                <option value="4" <?php if ($data['carspaces'] == '4') {
                                    echo "selected";
                                } ?>>4+
                                </option>
                            </select>
                        </div>
                    </div>
                    <div class="row-fluid text_main_div">
                        <div class="col-md-6">
                            <label for="purchase_details">Purchase and Valuation Details</label>
                            <textarea rows="4" class="form-control" id="PV_detail"
                                      name="purchase_details"><?php echo $data['purchase_details']; ?></textarea>
                        </div>
                        <!-- will be foreach -->
                        <?php
                        if ($property_note) {
                            foreach ($property_note as $note) { ?>
                                <div class="col-md-6 notes_add" style="margin-left: 0;position: relative;"
                                     id="rep_notes<?php echo $note['property_note_id']; ?>">
                  <span>
                    <label for="property_notes">Notes about property</label>
                    <textarea rows="4" id="NA_property" class="form-control"
                              name="property_notess[]"><?php echo $note['property_note']; ?></textarea>
                    <input type="hidden" value="<?php echo $note['property_note_id'] ?>" name="property_note_id[]">
                  </span>
                                    <button type="button" name="btn_delete_note"
                                            data-id4="<?php echo $note['property_note_id']; ?>"
                                            class="btn btn-xs btn-danger btn_delete_note">x
                                    </button>
                                    <input type="hidden" name="note_id"
                                           value="<?php echo $note['property_note_id']; ?>">
                                    <span class="cross_2" style="display:none;">x</span>
                                </div>
                                <!-- will be foreach -->
                            <?php }
                        } ?>
                        <div class="property_notes"></div>
                        <div id="stop_notes" style="display: none;"></div>
                    </div>
                    <div class="row-fluid">
                        <div class="col-md-12">
                            <label for="new-property-note"><a class="new-property-note" id="add_property_note"
                                                              href="javascript:void(0);">Click to add</a> another note
                                about your property</label>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-light">Save changes</button>
                    <a href="property/delete_property?pid=<?php echo $data['property_id']; ?>" class="btn btn-danger"><i
                                class="glyphicon glyphicon-trash"></i> Delete Property </a>
                </div>
            </form>
        </div>
    </div>
</div>

<div class="modal fade" tabindex="-1" role="dialog" id="dashboard_modal">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <h3>Do you have a current tenant? You may need to mark some rental periods as paid.</h3>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">No thanks, my tenant is new</button>
                <a target="_blank" href="rentSchedule/<?= $property_id ?>/3" class="btn btn-light">Proceed</a>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<script>
    $(function () {

        //openDashboardModalOnCondition();

        function openDashboardModalOnCondition() {
            $.ajax({
                url: 'dashboard_modal_open/<?=$lease_detail[0]['lease_id']?>',
                type: 'get',
                success: function (bool) {
                    console.log(bool);
                    if (parseInt(bool) == 1) {

                        $('#dashboard_modal').modal('show');
                        markDashboardModalShown();
                    }

                },
                error: function (jqXHR, textStatus, errorThrown) {
                    alert('ERROR! Modal cannot be opened.');
                    console.log(textStatus, errorThrown);
                }
            });
        }

        function markDashboardModalShown() {
            $.ajax({
                url: 'dashboard_modal_shown/<?=$lease_detail[0]['lease_id']?>',
                type: 'get',
                success: function (bool) {
                    console.log('dashboard modal shown success');
                },
                error: function (jqXHR, textStatus, errorThrown) {
                    alert('ERROR! Problem while updating');
                    console.log(textStatus, errorThrown);
                }
            });
        }


    })
</script>

<script type="text/javascript">
    jQuery(document).ready(function () {

        var income_expense_graph_data = <?= $income_expense_graph_json?>;

        console.log(income_expense_graph_data);
        /*Morris.Bar ({
            element: 'bar-graph',
            data: income_expense_graph_data,
            xkey: 'mon',
            barColors:['green', 'red'],
            ykeys: ['income', 'expense'],
            labels: ['Income', 'Expense'],
            lineColors: ['green', 'red'],
            xLabelAngle: 60,

        });*/

        Morris.Line({
            element: 'line-graph',
            data: income_expense_graph_data,
            xkey: 'mon',
            ykeys: ['income', 'expense'],
            labels: ['Income', 'Expense'],
            parseTime: false,
            lineColors: ['green', 'red'],
            xLabelAngle: 60,

        });
    });
</script>

<?php $this->load->view('front/footerlink'); ?>
<script type="text/javascript">
    $(document).on('click', '.tenant_button', function (event) {
        if ($(this).closest('.tenant_button').html() == 'Show Details') {
            $(this).closest('.tenant_button').html('Hide Details');
            $(this).closest('.payments_list').find('.tenant_div').fadeIn();
        }
        else {
            $(this).closest('.tenant_button').html('Show Details');
            $(this).closest('.payments_list').find('.tenant_div').fadeOut();
        }
    });
</script>
<script type="text/javascript">
    <?php if($info_open == 1){?>
    $('#property_photos_info').modal('show');
    <?php }?>
</script>

<script>
    $('#add_property_note').click(function () {
        $('.property_notes').append('<div class="col-md-6 notes_add" style="margin-left: 0;position: relative;" id="rep_notes"> <span> <label for="property_notes">Notes about property</label> <textarea rows="4" id="NA_property" class="form-control" name="property_notess[]"></textarea><input type="hidden" value="0" name="property_note_id[]"></span> <span id="property_note_hide_span" class="btn btn-xs btn-danger" >X</span></div>');

    });
</script>
<script>
    $(document).on('click', '#property_note_hide_span', function () {
        $(this).parents('#rep_notes').remove();
    });
</script>
<script type="text/javascript">
    function getState(val) {
        $.ajax({
            type: "POST",
            url: "property/ajax_get_state",
            data: 'country_id=' + val,
            success: function (data) {
                var res = JSON.parse(data);
                $('#state_select_option').html(res.stateDiv);
            }
        });
    }
</script>
<!-- delete property image -->
<script type="text/javascript">
    $(document).on('click', '.btn_delete', function () {
        var id = $(this).data("id3");
        //alert(id);
        if (confirm("Are you sure you want to delete this?")) {
            $.ajax({
                url: "property/delete_property_image",
                method: "get",
                data: {id: id},

                dataType: "text",
                success: function (data) {
                    //alert("abcdef");
                    $("#pImg" + id).fadeOut();
                    // fetch_data();
                }
            });
        }
    });
</script>
<!-- delete property image end -->
<!-- delete property note -->
<script type="text/javascript">
    $(document).on('click', '.btn_delete_note', function () {
        var id = $(this).data("id4");
        //alert(id);
        if (confirm("Are you sure you want to delete this?")) {
            $.ajax({
                url: "property/delete_property_note",
                method: "get",
                data: {id: id},

                dataType: "text",
                success: function (data) {
                    //alert("abcdef");
                    $("#rep_notes" + id).fadeOut();
                    // fetch_data();
                }
            });
        }
    });
</script>
<!-- delete property note end -->
<script>
    $(document).ready(function () {

        //alert("dfsdfsdfdsf");
        var idd = '<?=$result[0]['country'];?>'
        //alert(idd);
        $.ajax({
            type: "POST",
            url: "property/ajax_get_state2",
            data: {country_idd: idd},
            //alert(country_id); die;
            success: function (data) {
                var res = JSON.parse(data);
                $('#state_select_option').html(res.stateDiv);
                // alert("success");
            }
        });

    });
</script>

<script type="text/javascript">
    Dropzone.autoDiscover = false;
    var foto_upload = new Dropzone(".dropzone", {
        url: "<?php echo base_url('property/property_photo') ?>",
        maxFilesize: 20,
        method: "post",
        acceptedFiles: ".jpg,.jpeg,.png,.gif",
        paramName: "userfile",
        dictInvalidFileType: "This File Type Not Supported",
        addRemoveLinks: true,
        init: function () {
            var count = 0;
            thisDropzone = this;
            this.on("success", function (file, json) {
                var obj = json;
                $('.previews').append(
                    "<input type='hidden' name='image[]' value='" + obj + "'>\n\
                <input type='hidden' name='width[]' value='" + file.width + "'>\n\
                <input type='hidden' name='height[]' value='" + file.height + "'>"
                );
            });
        }
    });
</script>

<style>
    .red {
        border: 2px solid red;
    }
</style>

<script>
    $(function () {

        //------------------------------
        var settings_btn = $("#settings_btn");
        var leaseModal = $("#leaseModal");
        var vacateModal = $("#vacateModal");
        var lease = $("#lease");
        //--------------------------------

        var vacate_form = $("#vacate_form");
        var vacate_tenant_form_submit_btn = $("#vacate_tenant_form_submit_btn");
        var vacate_date = $("#vacate_date");
        var min_vacate_date = new Date(vacate_date.attr('min_vacate_date'));
        var vacate_date_help = $("#vacate_date_help");
        var vacate_form_success = vacate_form.attr('vacate_form_success');
        var lease_vacated = vacate_form.attr('lease_vacated');

        var vacate_form_url = vacate_form.attr('action');
        var vacate_form_method = vacate_form.attr('method');

        vacate_date.datepicker({
            dateFormat: 'd M, yy',
            minDate: min_vacate_date
        });


        settings_btn.on("click", function () {
            toggleModal();
        });

        lease.on("change", function () {
            toggleModal();
        });

        function toggleModal() {
            if (lease.val() == 'new_lease' && vacate_form_success == "0" && lease_vacated == "0") {
                leaseModal.modal('hide');
                vacateModal.modal('show');
            } else {
                leaseModal.modal('show');
                vacateModal.modal('hide');
            }
        }

        vacate_tenant_form_submit_btn.on("click", function (e) {

            e.preventDefault();

            if (vacate_date.val() == '') {
                vacate_date.addClass('red');
            } else {
                vacate_date.removeClass('red');
                submitVacateForm();
            }

        });

        function submitVacateForm() {
            $.ajax({
                url: vacate_form_url,
                type: vacate_form_method,
                data: vacate_form.serialize(),
                success: function (response) {
                    console.log(response);
                    var parsed_response = JSON.parse(response);

                    if (parsed_response.success == 'true') {
                        vacate_form_success = "1";
                        toggleModal();
                    } else {
                        alert("Something Went Wrong!");
                    }

                },
                error: function (jqXHR, textStatus, errorThrown) {
                    alert('ERROR!');
                    console.log(textStatus, errorThrown);
                }
            });
        }


        $('#archiveBtn').on('click', function (e) {
            e.preventDefault();

            var href = $(this).attr("href");

            console.log(href);

            swal({
                    title: "Are you sure ?",
                    text: "Once you decide to archive lease it would be deactivated",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: "#DD6B55",
                    confirmButtonText: "I want to archive this lease",
                    cancelButtonText: "No",
                    closeOnConfirm: true,
                    closeOnCancel: true
                },
                function (isConfirm) {
                    if (isConfirm) {
                        window.location.href = href;
                    }
                });


        })


    });
</script>

