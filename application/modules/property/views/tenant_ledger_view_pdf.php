<!DOCTYPE html>
<base href="<?php echo base_url(); ?>">
<html>
<head>
    <link href="assets/css/bootstrap.min.css" rel="stylesheet">
    <link href="assets/css/style.css" rel="stylesheet">
    <style>
        body {
            background: none;
        }
    </style>

</head>
<body>
<div class="container">
    <?php
    if ($lease_detail[0]['lease_pay_type'] == 1) {
        $pay_type_duration = "Week";
        $lease_paying_type = 7;
    } elseif ($lease_detail[0]['lease_pay_type'] == 2) {
        $pay_type_duration = "Fortnight";
        $lease_paying_type = 14;
    } elseif ($lease_detail[0]['lease_pay_type'] == 3) {
        $pay_type_duration = "Four week";
        $lease_paying_type = 28;
    } else {
        $pay_type_duration = "Month";
        $lease_paying_type = 30;
    }
    $payment_method[1] = "Bank Transfer ";
    $payment_method[2] = "Cash";
    $payment_method[3] = "Cheque";
    $payment_method[4] = "Credit Card";
    $payment_method[4] = "Other method";
    ?>
    <div class="col-md-12">
        <div class="text-center">
            <h3><img style="max-width: 200px;" src="assets/img/logo.png"></h3>
        </div>
    </div>
    <div class="col-md-12">
        <h3 class="text-center">Rent Ledger</h3>
    </div>
    <br>
    <div>
        <p><span style="font-weight: bold">Property :</span>
            <?= $full_property_address?>
        </p>
        <p><span style="font-weight: bold">Tenants :</span> <?php foreach ($tenant_info as $key => $row) {
                if ($key != 0) {
                    echo ' & ';
                }
                echo $row['user_fname'] . ' ' . $row['user_lname'];
            } ?></p>
        <p><span style="font-weight: bold">Ledger Period :</span> <?= $tenant_ledger_start_date; ?>
            - <?= $tenant_ledger_end_date; ?> </p>
        <p><span style="font-weight: bold">Rent :</span> $<?= $total_tenant_amount; ?> per <?= $pay_type_duration; ?></p>
        <p>
            <span style="font-weight: bold">Landlord :</span> <?= $user_info[0]['user_fname'] ?> <?= $user_info[0]['user_lname'] ?>
        </p>
        <p><span style="font-weight: bold">Date Created :</span> <?= date('d/m/Y'); ?></p>
    </div>
    <br>
    <br>
    <br>

    <?php if (count($lease_log_payment_lease_list) > 0) { ?>
        <br><br>
        <h4>Rent Received</h4>
        <table class="table table-bordered table-hover" style="font-size: 15px;">
            <thead style="background-color: white">
            <tr>
                <td scope="row" style="text-align:center;background-color: #0bafd2;font-weight:bold">Amount Received
                </td>
                <td scope="row" style="text-align:center;background-color: #0bafd2;font-weight:bold">Date Received</td>
                <td scope="row" style="text-align:center;background-color: #0bafd2;font-weight:bold">Payment Method</td>
                <td scope="row" style="text-align:center;background-color: #0bafd2;font-weight:bold">Rental Periods</td>
            </tr>
            </thead>
            <tbody>
            <?php foreach ($lease_log_payment_lease_list as $lease_log_payment_lease_item) { ?>
                <tr>
                    <td style="text-align:center;width:15%;border-bottom:1px solid black" scope="row">
                        <span>$</span><?= $lease_log_payment_lease_item['lease_log_payment_amount_recieved'] ?>
                        <p style="word-break: break-all">
                            ( <?= $this->utility_model->get_rent_payment_ref($lease_log_payment_lease_item['lease_log_payment_id']) ?>
                            )
                        </p>
                    </td>

                    <td style="text-align:center;width:15%;border-bottom:1px solid black">
                        <?= date('d/m/y', strtotime($lease_log_payment_lease_item['lease_log_payment_time'])) ?>
                    </td>
                    <td style="text-align:center;width:15%;border-bottom:1px solid black">
                        <?php if (array_key_exists($lease_log_payment_lease_item['lease_log_payment_method'], $payment_method)) {
                            echo $payment_method[$lease_log_payment_lease_item['lease_log_payment_method']];
                        } ?>
                    </td>
                    <td style="text-align:center;width:55%;border-bottom:1px solid black">
                        <?php if (count($lease_log_payment_lease_item['log_payment_details_with_schedule_list']) > 0) { ?>
                            <table class="table table-bordered table-hover"
                                   style="font-size: 15px;">
                                <thead style="background-color: white">
                                <tr>
                                    <td style="text-align: center">Period
                                    </td>
                                    <td style="text-align: center">Amount received
                                    </td>
                                    <td style="text-align: center">Due
                                    </td>
                                </tr>
                                </thead>
                                <tbody>
                                <?php foreach ($lease_log_payment_lease_item['log_payment_details_with_schedule_list'] as $log_payment_details_with_schedule_item) { ?>
                                    <tr>
                                        <td style="text-align: center">
                                            <?= date('d/m/y', strtotime($log_payment_details_with_schedule_item['payment_start_period'])) ?>
                                            <br>to<br>
                                            <?= date('d/m/y', strtotime($log_payment_details_with_schedule_item['payment_end_period'])) ?>
                                        </td>
                                        <td style="text-align: center">
                                            $<?= $log_payment_details_with_schedule_item['log_lease_rent_recieved'] ?>
                                        </td>
                                        <td style="text-align: center">
                                            $<?= $log_payment_details_with_schedule_item['payment_due_amount'] ?>
                                        </td>
                                    </tr>
                                <?php } ?>
                                </tbody>
                            </table>
                        <?php } ?>
                    </td>
                </tr>
            <?php } ?>
            </tbody>
        </table>
    <?php } ?>

    <!-- ---------- water <starts> ---------- -->
    <?php if (count($water_invoices_with_payment_logs) > 0) { ?>
        <h4>Water</h4>
        <table class="table table-bordered table-hover" style="font-size: 15px;">
            <thead style="background-color: white">
            <tr>
                <td scope="row" style="text-align:center;background-color: #0bafd2;font-weight:bold;">Period</td>
                <td scope="row" style="text-align:center;background-color: #0bafd2;font-weight:bold;">Due Date</td>
                <td scope="row" style="text-align:center;background-color: #0bafd2;font-weight:bold;">Amount</td>
                <td scope="row" style="text-align:center;background-color: #0bafd2;font-weight:bold;">Paid /<br>
                    Outstanding
                </td>
                <td scope="row" style="text-align:center;background-color: #0bafd2;font-weight:bold;">Completed</td>
                <td scope="row" style="text-align:center;background-color: #0bafd2;font-weight:bold;">Payments</td>
            </tr>
            </thead>
            <tbody>
            <?php foreach ($water_invoices_with_payment_logs as $a_water_invoice_with_payment_logs) { ?>
                <tr>
                    <td style="text-align: center;border-bottom:1px solid black">
                        <?= date("F, Y", strtotime($a_water_invoice_with_payment_logs['water_invoice_period_start'])); ?>
                        <br>
                        to
                        <br>
                        <?= date("F, Y", strtotime($a_water_invoice_with_payment_logs['water_invoice_period_end'])); ?>
                    </td>
                    <td style="text-align: center;border-bottom:1px solid black"><?= date("d/m/y", strtotime($a_water_invoice_with_payment_logs['water_invoice_due_date'])); ?></td>
                    <td style="text-align: center;border-bottom:1px solid black">
                        $<?= $a_water_invoice_with_payment_logs['water_invoice_amount']; ?></td>
                    <td style="text-align: center;border-bottom:1px solid black">
                        $<?= $a_water_invoice_with_payment_logs['water_invoice_amount_paid']; ?> /
                        $<?= $a_water_invoice_with_payment_logs['water_invoice_amount_due']; ?>
                    </td>
                    <td style="text-align: center;border-bottom:1px solid black">
                        <?= $a_water_invoice_with_payment_logs['water_invoice_status'] == 1 ? "Yes" : "No"; ?>
                    </td>
                    <td style="text-align: center;border-bottom:1px solid black">
                        <?php if ($a_water_invoice_with_payment_logs['water_invoice_log']) { ?>

                            <table class="table table-bordered table-hover">
                                <thead style="background-color: white">
                                <tr>
                                    <th style="text-align: center" scope="col">Amount Paid</th>
                                    <th style="text-align: center" scope="col">Date Paid</th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php foreach ($a_water_invoice_with_payment_logs['water_invoice_log'] as $a_water_invoice_log) { ?>
                                    <tr>
                                        <td style="text-align: center">
                                            $<?= $a_water_invoice_log['water_invoice_log_amount']; ?>
                                        </td>
                                        <td style="text-align: center">
                                            <?= date("d/m/y", strtotime($a_water_invoice_log['water_invoice_payment_date'])); ?>
                                        </td>
                                    </tr>
                                <?php } ?>
                                </tbody>
                            </table>

                        <?php } else { ?>

                            <span style="text-align: center">No current payments</span>

                        <?php } ?>


                    </td>
                </tr>
            <?php } ?>
            </tbody>
        </table>
    <?php } ?>
    <!-- ---------- water <ends> ---------- -->

    <?php if (count($all_ledger_info) > 0) { ?>
        <br><br>
        <h4>Money In</h4>
        <table class="table table-bordered table-hover" style="font-size: 15px;">
            <thead style="background-color: white">
            <tr>
                <td style="text-align:center;width:20%;background-color: #0bafd2;font-weight:bold">Date Received</td>
                <td style="text-align:center;width:20%;background-color: #0bafd2;font-weight:bold">Amount</td>
                <td style="text-align:center;width:20%;background-color: #0bafd2;font-weight:bold">For</td>
                <td style="text-align:center;width:20%;background-color: #0bafd2;font-weight:bold">Paid By</td>
                <td style="text-align:center;width:20%;background-color: #0bafd2;font-weight:bold">Method</td>
                <td style="text-align:center;width:20%;background-color: #0bafd2;font-weight:bold">Period</td>
                <td style="text-align:center;width:20%;background-color: #0bafd2;font-weight:bold">Total</td>
            </tr>
            </thead>
            <tbody>
            <?php $start_count = 0; ?>
            <?php foreach ($all_ledger_info as $row) { ?>
                <tr>
                    <td style="text-align:center;width:20%;"><?= date('M d,Y', strtotime($row['schedule_start_date'])); ?></td>
                    <td style="text-align:center;width:20%;"><?= $row['shared_amount']; ?></td>
                    <td style="text-align:center;width:20%;"><?php if ($row['payment_for'] = 1) {
                            echo "rent";
                        } else {
                            echo "Money in";
                        } ?></td>
                    <td style="text-align:center;width:20%;"><?= $row['user_fname']; ?> <?= $row['user_lname']; ?></td>
                    <td style="text-align:center;width:20%;"><?php if ($row['payment_method'] == 1) {
                            echo "Bank Transfer";
                        } elseif ($row['payment_method'] == 2) {
                            echo "Cash";
                        } elseif ($row['payment_method'] == 3) {
                            echo "Cheque";
                        } elseif ($row['payment_method'] == 4) {
                            echo "Credit Card";
                        } else {
                            echo "Other method";
                        } ?></td>
                    <td style="text-align:center;width:20%;"><?= date('M d', strtotime($row['schedule_start_date'])); ?>
                        - <?= date('M d', strtotime($row['schedule_end_date'])); ?></td>
                    <td style="text-align:center;width:20%;"><?php $start_count = $start_count + $row['shared_amount'];
                        echo $start_count ?></td>
                </tr>
            <?php } ?>
            </tbody>
        </table>
    <?php } ?>
</div>
</body>
</html>