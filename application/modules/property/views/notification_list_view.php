
    <a href="#" class="dropdown-toggle mr_notice" data-toggle="dropdown" role="button"
       aria-haspopup="true" aria-expanded="false">
        <span class="glyphicon glyphicon-bell" aria-hidden="true"></span>
        <span id="noti_count" class="badge badge-light noti_count" noti-count = "<?= count($notifications)?>"><?= count($notifications)?></span>
    </a>


    <ul class="dropdown-menu mr_ul">
        <?php if(empty($notifications)) { ?>
            <li><a href="#">No new Notifications...</a></li>
        <?php } ?>

        <?php if(!empty($notifications)) { ?>
            <?php foreach ($notifications as $notification) { ?>
                <li>
                    <a href="<?= $notification["url"]?>">
                        <?= $notification["text"]?>
                    </a>
                    <span title="Mark as seen" notification-id="<?=$notification["notification_id"]?>" class="glyphicon glyphicon-ok sp_noti_mark " aria-hidden="true"></span>
                </li>
            <?php }?>
        <?php } ?>
    </ul>
