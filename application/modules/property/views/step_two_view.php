<?php $this->load->view('front/headlink');?>
<body>
    <div class="setup_multistape">
         <div class="ss_main_container"></div>
        <div class="container">
            <?php $this->load->view('front/head_nav');?>
            <h3></h3>
            <div class="row form-group">
                <?=$this->load->view('front/step_div');?>
            </div>
        </div>
        <form class="container " onsubmit="return check_step_two()" action="insert_property_step_two" method="post">
            <input type="hidden" name="property_id" value="<?php if($property_info){echo $property_info[0]['property_id'];}?>">
            <div class="row setup-content" id="step-2">
                <div class="">
                    <div class=" well">
                        <h1 class="extra_heading"> Add Rental Property</h1>
                        <!-- <br/> -->
                        <br/>
                        <?php if($this->session->userdata('insert_stp_two_err')){ ?>
                        <div class="alert alert-danger alert-dismissable">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>
                            <a href="javascript:;" class="alert-link"><?=$this->session->userdata('insert_stp_two_err');?></a>
                        </div>
                        <?php } $this->session->unset_userdata('insert_stp_two_err');?>

                        <div class="alert alert-danger" id="err_div" style="display: none">
                            <strong class="text-danger" id="err_msg"></strong>
                        </div>
                        <div class=" ">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="inputPropertyAddress">Property Address <span>*</span></label>
                                    <input type="text" name="property_address" class="form-control" id="inputPropertyAddress" 
                                           value="<?php if($property_info){echo $property_info[0]['property_address'];}?>">
                                    <strong class="text-danger" id="err_prpty_addrs_msg"></strong>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="inputCity">City/Suburb <span>*</span></label>
                                    <input type="text" name="city" class="form-control" id="inputCity" value="<?php if($property_info){echo $property_info[0]['city'];}?>">
                                    <strong class="text-danger" id="err_cty_msg"></strong>
                                </div>
                            </div>
                        </div>
                        <div class=" ">
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="inputZipCode">Zip/Postal Code <span>*</span></label>
                                    <input type="text" name="zip_code" class="form-control" id="inputZipCode" value="<?php if($property_info){echo $property_info[0]['zip_code'];}?>">
                                    <strong class="text-danger" id="err_zip_msg"></strong>
                                </div>
                                <input type="hidden" name="country"  readonly value="13">
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="inputCountry">Country <span>*</span></label>
                                    <input type="text" value="Australia" class="form-control" readonly>
                                    <!-- <select class="form-control" name="country" id="inputCountry" onChange="getState(this.value);">
                                        <option value="">Select Country</option>
                                        <?php foreach($all_country as $row){?>
                                        <option <?php if(!$property_info && ($row['country_id'] == 13)){echo 'selected';}?> value="<?=$row['country_id'];?>" <?php if($property_info && ($property_info[0]['country'] == $row['country_id'])){echo 'selected';}?>>
                                            <?=$row['country_name'];?>
                                        </option>
                                        <?php }?>
                                    </select> -->
                                    <strong class="text-danger" id="err_cntry_msg"></strong>
                                </div>
                            </div> 
                            <div class="col-md-4">
                                    <div class="form-group">
                                        <label for="inputState">State <span>*</span></label>
                                        <select class='form-control' name='state' id='inputState'>
                                            <option value="">Select State</option>
                                            <?php foreach($get_state as $state_row){?>
                                            <option 
                                                <?php if(isset($property_info[0]['state_id']))
                                                {  if($property_info[0]['state']==$state_row['state_id'])
                                                    {
                                                        echo "selected";
                                                    }
                                                } ?>
                                                value="<?=$state_row['state_id'];?>">
                                                <?=$state_row['state_name'];?>
                                            </option>
                                            <?php }?>
                                        </select>
                                        <strong class="text-danger" id="err_state_msg"></strong>
                                    </div>
                                </div>
                                <!-- <div class="col-md-4" id="state_select_option">
                                        <?php $this->load->view('state_div'); ?>
                                </div> -->
                        </div>
                        <div class="text-center extar_p">
                            <a href="property" class="btn btn-light btn-md">Back</a>
                            <input type="submit" class="btn btn-primary btn-md" value="Next Step">
                            <!-- <a href="property/step_three" class="btn btn-primary btn-md">Next Step</a> -->
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>

    <?php $this->load->view('front/footerlink');?>

    <script type="text/javascript">
        function getState(val) {
            $.ajax({
                type: "POST",
                url: "property/ajax_get_state",
                data:'country_id='+val,
                success: function(data){
                        var res = JSON.parse(data);
                        alert("success");
                        $('#state_select_option').html(res.stateDiv);
                    }
            });
        }

        $( document ).ready(function() {
            $.ajax({
                type: "POST",
                url: "property/ajax_get_state",
                data:'country_id=13',
                success: function(data){
                        var res = JSON.parse(data);
                        $('#state_select_option').html(res.stateDiv);
                    }
            });
        });

    </script>

	<script>
       function get_state2(){
          var idd = '<?php if($property_info){echo $property_info[0]['country'];}?>';
          $.ajax({
            type: "POST",
            url: "property/ajax_get_state2",
            data:{country_idd: idd},
                success: function(data){
                    alert(data);
                  var res = JSON.parse(data);
                  $('#state_select_option').html(res.stateDiv);
                      // alert("success");
                    }
                  });

	   }
    </script>
	
    <script>
        function check_step_two()
        {
            $("#err_div").hide();
            
            var ok=true;
            var property_address = $("#inputPropertyAddress").val();
            var city = $("#inputCity").val();
            var zip = $("#inputZipCode").val();
            var country = $("#inputCountry").val();
            // alert(country);
            var state = $("#inputState").val();
            if(property_address=='' || city=="" || zip=="" || country=="" || state=="")
            {
                $("#err_div").show();
                $("#err_msg").html('* All fields must be filled out');
                ok=false;
            }
            if(property_address=='')
            {
                $("#err_prpty_addrs_msg").html('* Property Address must be filled out');
                ok=false;
            }
            else
            {
                $("#err_prpty_addrs_msg").html('');
            }
            if(city=="")
            {
                $("#err_cty_msg").html('* City must be filled out');
                ok=false;
            }
            else
            {
                $("#err_cty_msg").html('');
            }
            if(zip=="")
            {
                $("#err_zip_msg").html('* Zip/Postal code must be filled out');
                ok=false;
            }
            else
            {
                $("#err_zip_msg").html('');
            }
            if(country=='')
            {
                $("#err_cntry_msg").html('* Country must be filled out');
                ok=false;
            }
            else
            {
                $("#err_cntry_msg").html('');
            }
            if(state=="")
            {
                $("#err_state_msg").html('* State must be filled out');
                ok=false;
            }
            else
            {
                $("#err_state_msg").html('');
            }
            return ok;
        }
    </script>
    
