<?php $this->load->view('front/headlink'); ?>
<?php $this->load->view('front/top_menu'); ?>
<div class="container">
    <?php $this->load->view('front/head_nav'); ?>
    <div style="margin-top:30px;">

        <h4>Current lease: <?= $lease_detail[0]['lease_name'] ?> (365 days to go) <a href="#" data-toggle="modal"
                                                                                     data-target="#myModal"><span
                        class="glyphicon glyphicon-cog" style="display: none"; aria-hidden="true"></span></a></h4>
        <div class="container">
            <div class="modal fade" id="myModal" role="dialog">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                            <h4 class="modal-title">Select a Lease or add a new lease</h4>
                        </div>
                        <form action="property/property_dashboard/<?= $property_id; ?>" method="post">
                            <div class="modal-body">
                                <h4>Lease Option</h4>
                                <select class="form-control" name="lease_id">
                                    <option value="new_lease">Add a new lease</option>
                                    <?php foreach ($all_lease as $row) { ?>
                                        <option <?php if ($row['lease_id'] == $lease_detail[0]['lease_id']) {
                                            echo "selected";
                                        } ?> value="<?= $row['lease_id']; ?>"><?= $row['lease_name']; ?></option>
                                    <?php } ?>
                                </select>
                            </div>
                            <div class="modal-footer">
                                <input type="submit" class="btn btn-success" value="Confirm" name="">
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row hi salim">
        <div class="col-sm-8">
            <div class="ss_dashboart">
                <div class="row" id="ss_d_top_content">
                    <div class="col-md-4">
                        <h2>Rent</h2>
                        <h4 class="">$<span class="ss_price_text">0.00</span></h4>
                        <p><span id="rent_status" class="label-paid">Up to date</span></p>
                        <br/><br/>
                        <a href="<?= base_url('rent/rent_schedule') ?>" class="btn btn btn-primary">Go to schedule</a>
                    </div>
                    <div class="col-md-4">
                        <h2>Expenses</h2>
                        <p><strong>Paid</strong> this month <span class="ss_price_text">$0.00</span></p>
                        <p><strong>Due</strong> in next 30 days <span class="ss_price_text">$0.00</span></p><br/><br/>
                        <a href="<?= base_url('expense/view_expense') ?>" class="btn btn btn-primary">Go to expenses</a>
                    </div>
                    <div class="col-md-4">
                        <h2>Maintenance</h2>
                        <p><strong id="count">0</strong> <span class="m_status m_status-new">New</span></p>
                        <p><strong id="active">0</strong> <span class="m_status m_status-active">Active</span></p>
                        <br/><br/>
                        <a href="<?= base_url('maintenance/view_all_maintenance') ?>" class="btn btn btn-primary">View
                            All</a>
                    </div>
                </div>
                <div class="dashboard_element rent-payments">
                    <h1>Rent payments</h1>
                    <a href="rent/mark_rent_recive" id="mark_as_paid"
                       class="btn btn btn-light"><span>Mark Rent Received</span></a>
                    <a href="rent/rent_schedule" id="by_rent_received" class="btn btn btn-light"><span>Show Rent Received</span></a>
                    <div class="clear" style="height: 15px;"></div>
                    <ul class="payments_list">
                        <?php foreach ($all_tenant as $row) { ?>
                            <li class="tenant_name"
                                id="tenant-name"><?= $row['user_fname']; ?> <?= $row['user_lname']; ?></li>
                            <li>
                                <p>
                                    <strong><span id="tenant-total"></span></strong>
                                    <span id="tenant-status" class="label label-paid">Up to date</span>
                                </p>
                            </li>
                            <div id="item_1" style="display:none;">
                                <a href="#" class="reminder" id="reminder">Send <span id="u-name">Kk</span> a reminder
                                    about outstanding rents</a>
                            </div>
                        <?php } ?>
                    </ul>
                    <ul id="last_flag1" style="display:none;"></ul>
                    <div class="clear"></div>
                </div>
            </div>
        </div>
        <div class=" col-sm-4 ss_side_right">
            <div class="ss_dashboart">
                <a href="#" target="_blank">
                    <div class="row">
                        <div class="col-sm-4">
                            <div class="prop_placeholder_wrapper">
                                <img src="<?php echo base_url(); ?>/assets/img/t4.png"
                                     class="img-responsive img-circle ">
                            </div>
                        </div>
                        <div class="col-sm-8">
                            <p>What's new at RentingSmart?<br>
                                Click to find out. We're always adding new features.</p>
                        </div>
                    </div>
                </a>
            </div>
            <div class="ss_dashboart">
                <a href="lease/lease_details" type="submit" class="btn btn btn-primary ">
                    Lease details:
                </a>
                <p>12 February 2018 - 12 February 2019 </p>
            </div>
            <div class="ss_dashboart">
                <a href="rent/rent_schedule" type="submit" class="btn btn btn-light ">
                    Rent details:
                </a>
                <p><span class="ss_price_text">$12.00</span> per week</p>
                <p>Due on monday of each week Next payment due in 7 days</p>
            </div>
            <div class="ss_dashboart">
                <a href="lease/add_tenants" type="submit" class="btn btn btn-light ">
                    Tenants:
                </a>
                <div class="row">
                    <div class="col-sm-4">
                        <div class="prop_placeholder_wrapper">
                            <img src="<?php echo base_url(); ?>/assets/img/t4.png" class="img-responsive img-circle ">
                        </div>
                    </div>
                    <div class="col-sm-8">
                        <p>Your Name<br>
                            Number isn't provided Email isn't provided</p>
                    </div>
                </div>
                <br><br>
                <a href="#" type="submit" class="btn btn btn-light ">
                    Invite to RentingSmart
                </a>
                <br><br>
            </div>
        </div>
    </div>
</div>



<?php $this->load->view('front/footerlink'); ?> 

