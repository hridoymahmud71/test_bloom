<?php $this->load->view('front/headlink');?>
<body>
    <div class="setup_multistape">  
        <div class="container">
            
            <?php $this->load->view('front/head_nav'); ?>
            
            <div class="ss_schedule">
                <h3>Rent Schedule for the property at <span>Name</span> </h3>
                <div class="col-md-4">
                    <div class="ss_views_by">
                        View by:
                        <ul>
                            <li  class="active"><a href="#rentreceived" aria-controls="rentreceived" role="tab" data-toggle="tab">Rent Received</a></li>
                            <li><a href="#periodschedule" aria-controls="periodschedule" role="tab" data-toggle="tab">Period Schedule</a></li>
                        </ul>
                    </div>
                </div>
                <div class="col-md-4">
                    <p class="tenant_box"><span id="tenant_name">Name lastname..</span> <span id="tenant_payment_stats" class="label label-info">$ 37.71 ahead</span> </p>
                </div>
                <div class="col-md-4">
                    <div class="ss_enter_rent">
                        <a data-toggle="modal" href="#recive_rent" id="mark_as_paid" class="btn btn-light btn-lg mark_paid">Enter Received Rent Now</a>
                        <a  data-toggle="modal" data-target="#print_ledger" href="javascript:void(0);" id="print_tenant_ledger " class="print_ledgers btn btn-primary btn-lg">
                            Print Ledgers &amp; Schedules
                        </a>
                    </div>
                </div>
                <div class="payment_table">
                    <div class="tab-content">
                        <div role="tabpanel" class="tab-pane active" id="rentreceived">
                            <div class="step_5_table">
                                <ul class="table_header top-header-div">
                                    <li>Amount Received</li>
                                    <li>Date Received</li>
                                    <li>Payer</li>
                                    <li>Paid by</li>
                                    <li>Period(s)</li>
                                    <li> Actions</li>
                                </ul>
                                <ul class="table_row schedule_list">
                                    <li id="num">
                                        <span class="input_text" id="period_amount" style="display: inline;">$50.29</span> 
                                        <input class="input-small form-control dollar_blue pay_period_amount" type="Number" name="amount_due[]" value="50.29" style="display: none;">
                                    </li>
                                    <li>
                                        <h1>Recived Date</h1>
                                        <span class="input_text" id="due_date" style="display: inline;">Feb 28, 2018</span>
                                        <div id="fromDate1" style="display:none" class="input-group input-small date" data-date-format="mm-dd-yyyy" name="due_date[]">
                                            <input class="form-control" type="text" readonly="">
                                            <span class="input-group-addon"><i class="glyphicon glyphicon-calendar"></i></span>
                                        </div>
                                    </li>
                                    <li>
                                        <span class="input_text" id="Name_lastname" style="display: inline;">Name lastname</span> 
                                        <input class="input-small dollar_blue pay_period_amount form-control" type="text"  value="Name lastname" style="display: none;">
                                    </li>
                                    <li>
                                        <span class="input_text" id="payment_mathod" style="display: inline;">Cash</span> 
                                        <select class="input-small form-control payment_method" style="display: none;">
                                            <option value="Select Method">Select Method</option>
                                            <option value="Bank Transfer">Bank Transfer</option>
                                            <option value="Cash" selected="selected">Cash</option>
                                            <option value="Cheque">Cheque</option>
                                            <option value="Credit Card">Credit Card</option>
                                            <option value="Other method">Other method</option>
                                        </select>
                                    </li>
                                    <li>
                                        <span class="" id="Name_lastname" style="display: inline;">Feb 17 - Feb 18 (part)</span> 
                                    </li>
                                    <li>
                                        <span class="edit_lease_schedule" style="display: inline;"><a href="javascript:void(0);">edit</a></span>
                                        <span class="save_lease_schedule" style="display: none;"><a href="javascript:void(0);" class="save_edits">save</a> <a href="javascript:void(0);" class="cencel_edit" style="position:absolute; margin-left: 5px;">| delete</a></span>
                                    </li>
                                </ul>
                            </div>
                        </div>
                        <div role="tabpanel" class="tab-pane" id="periodschedule"> 
                            <div class="ss_further">
                                <ul class="table_header top-header-div">
                                    <li><strong>Period</strong> </li>
                                    <li>Due Date</li>
                                    <li>Amount Due</li>
                                    <li>Rent Received</li>
                                    <li>Status</li>
                                </ul>
                                <p class="text-center">
                                    <span id="lease_dates_text">Lease dates:  February 10, 2018 – February 10, 2019.</span>
                                    <a href="javascript:void(0);" class="furter_rent_show_hide" >View more Future Periods</a>
                                </p>
                                <div id="furter_rent_list" style="display:none;">
                                    <ul class="table_row schedule_list">
                                        <li><strong>12 - Feb 18</strong> </li> 
                                        <li>Feb 12, 2018 </li>
                                        <li><strong>$22.00</strong> </li>
                                        <li><strong>$0.00</strong> </li>
                                        <li><span class="label label-info">Upcoming</span> </li>
                                    </ul>
                                    <ul class="table_row schedule_list">
                                        <li><strong>12 - Feb 18</strong> </li> 
                                        <li>Feb 12, 2018 </li>
                                        <li><strong>$22.00</strong> </li>
                                        <li><strong>$0.00</strong> </li>
                                        <li><span class="label label-info">Upcoming</span> </li>
                                    </ul>
                                    <ul class="table_row schedule_list">
                                        <li><strong>12 - Feb 18</strong> </li> 
                                        <li>Feb 12, 2018 </li>
                                        <li><strong>$22.00</strong> </li>
                                        <li><strong>$0.00</strong> </li>
                                        <li><span class="label label-info">Upcoming</span> </li>
                                    </ul>
                                    <ul class="table_row schedule_list">
                                        <li><strong>12 - Feb 18</strong> </li> 
                                        <li>Feb 12, 2018 </li>
                                        <li><strong>$22.00</strong> </li>
                                        <li><strong>$0.00</strong> </li>
                                        <li><span class="label label-info">Upcoming</span> </li>
                                    </ul>
                                    <ul class="table_row schedule_list">
                                        <li><strong>12 - Feb 18</strong> </li> 
                                        <li>Feb 12, 2018 </li>
                                        <li><strong>$22.00</strong> </li>
                                        <li><strong>$0.00</strong> </li>
                                        <li><span class="label label-info">Upcoming</span> </li>
                                    </ul>
                                    <ul class="table_row schedule_list">
                                        <li><strong>12 - Feb 18</strong> </li> 
                                        <li>Feb 12, 2018 </li>
                                        <li><strong>$22.00</strong> </li>
                                        <li><strong>$0.00</strong> </li>
                                        <li><span class="label label-info">Upcoming</span> </li>
                                    </ul>
                                </div>  
                                <ul class="table_row schedule_list">
                                    <li><strong>12 - Feb 18</strong> </li> 
                                    <li>Feb 12, 2018 </li>
                                    <li><strong>$22.00</strong> </li>
                                    <li><strong>$22.00</strong> </li>
                                    <li><span id="lease_label" class="label label-paid">Paid</span> </li>
                                </ul>
                                <ul class="table_row schedule_list paid_payment">
                                    <li><strong>12 - Feb 18</strong> </li> 
                                    <li>Feb 12, 2018 </li>
                                    <li><strong>$22.00</strong> </li>
                                    <li><strong>$22.00</strong> </li>
                                    <li><span id="lease_label" class="label label-paid">Paid</span> </li>
                                </ul>
                                <ul class="table_row schedule_list">
                                    <li><strong>12 - Feb 18</strong> </li> 
                                    <li>Feb 12, 2018 </li>
                                    <li><strong>$22.00</strong> </li>
                                    <li><strong>$22.00</strong> </li>
                                    <li><span id="lease_label" class="label label-paid">Paid</span> </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="text-center extar_p">
                    <button id="activate-step-1" class="btn btn-light btn-md">Back</button>
                    <button class="btn btn-primary btn-md"  data-toggle="modal" data-target="#complete_wizard">Complete Setup</button>
                </div>
            </div>
        </div>
    </div>	
</div>
<!-- Modal -->
<div class="modal fade" id="recive_rent" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h2 class="modal-title" id="myModalLabel">You've received some money</h2>
            </div>
            <div class="modal-body">
                <ul class="table_header top-header-div">
                    <li>Payer Name</li>
                    <li>Amount Received</li>
                    <li>Payment Method</li>
                    <li>Date Paid</li>
                    <li>Send Receipt?</li>                 
                    <li> </li>
                </ul>
                <ul class="table_row schedule_list"> 
                    <li class="payer_name"><label id="tenant_name" for="t-payment">aaaaaaaaaa bbbbbbbbb</label></li>
                    <li class="how_much">
                        <h1>Amount Received</h1>
                        <input type="text" class="input-small form-control" name="" id="t-payment" value="0.00">
                    </li>
                    <li class="payment_method">
                        <h1>Payment Method</h1>
                        <select class="input-small form-control" id="p-method" name="">
                            <option value="Select Method" selected="">Select Method</option>
                            <option id="Bank Transfer" value="Bank Transfer">Bank Transfer</option>
                            <option id="Cash" value="Cash">Cash</option>
                            <option id="Cheque" value="Cheque">Cheque</option>
                            <option id="Credit Card" value="Credit Card">Credit Card</option>
                            <option id="Other method" value="Other method">Other method</option>
                        </select>
                    </li>
                    <li><h1>Date Paid</h1>  
                        <div id="fromDate2" class="input-group date" data-date-format="mm-dd-yyyy">
                            <input class="form-control" type="text" readonly />
                            <span class="input-group-addon"><i class="glyphicon glyphicon-calendar"></i></span>
                        </div>
                    </li>
                    <li><h1>Send Receipt?</h1><input type="checkbox" id="send_receipt_5a7ef7fcdffaf" name="send_receipt[7277]">
                        <label class="send_receipt" id="send_receipt_label" for="send_receipt_5a7ef7fcdffaf">
                        </label>
                        <span class="left-text">Send</span>
                    </li>
                    <li><button type="button" class="close remove_payment">×</button></li>
                </ul>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default pull-left" data-dismiss="modal" >Close</button>
                <button type="button" class="btn btn-primary  pull-right"> Mark as paid</button>
            </div>
        </div>
    </div>
</div>
<!-- Modal -->
<div class="modal fade" id="complete_wizard" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content text-center">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Wizard Complete</h4>
            </div>
            <div class="modal-body">
                <h4>Next up is your personal dashboard<br/> and property control center.</h4> 
                <p> From there you can add maintenance issues,<br/> expenses, mark rents received,<br/> set up invoices and more!</p>
                <br/><br/><br/>
                <a href="confirmSchedule/<?php echo $property_id;?>" class="btn btn btn-primary btn-lg">Continue to get organised</a>
                <br/><br/><br/><br/><br/><br/>
            </div>
        </div>
    </div>
</div>
<div  class="modal fade" id="print_ledger" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" >
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">×</button>
        <div class="modal-title">
            <h4 class="print_ledger_title">Print and share with your tenants</h4>
        </div>
        <div class="form-chooser row-fluid">
            <div class="span6">
                <h2>
                    <span class="custom-radio" data-show-form="print-rent-form"> </span>
                    Rent Schedules
                </h2>
                <p>All rent payments, lease periods<br>and due dates for this lease</p>
            </div>
            <div class="span6">
                <h2>
                    <span class="custom-radio custom-radio-checked" data-show-form="print-ledger-form"> </span>
                    Tenant Ledgers
                </h2>
                <p>Historical rent payments paid<br>by individual tenant or all</p>
            </div>
        </div>
    </div>
    <div class="modal-body print-ledger-form">
        <div class="row-fluid print-rent-form">
            <div class="span12">
                <form id="print_rent_schedule_form" action="?r=RentingSmartTenantLedger&amp;print_ledger=1&amp;property_id=NDA2MA" method="post" name="print_rent_schedule_form">
                    <input id="print_rent_schedule" type="hidden" name="print_rent_schedule" value="1">
                    <div class="row-fluid group-row">
                        <div class="span6 title-column">
                            <p>View rent schedule for all<br> tenants or by individual tenant?</p>
                        </div>
                        <div class="span6 rent_schedule_tenants_name_section">
                            <div class="customize-radio clearfix radio-text">
                                <div>
                                    <input id="tenant_name_radio_0" type="radio" name="rent_schedule_tenant_id" class="Select_tenant_radio all_tenants" value="4937" checked="checked">
                                    <label for="tenant_name_radio_0">Show All Tenants</label>
                                </div>
                                <div id="rep_radio">
                                    <input id="tenant_name_radio_4937" type="radio" name="rent_schedule_tenant_id" class="Select_tenant_radio tenant_name_redio" value="4937">
                                    <label for="tenant_name_radio_4937" class="tenant_name_label">Aaaaaaaaaa Bbbbbbbbb</label>
                                </div>
                                <div style="display: none;" class="stop_radio"> </div>
                            </div>
                            <div class="span6 schedule_tenant_name_error" style="display: none; width:100%;  margin: 0px 0px 0px -28px;">
                                <p class="help-block error">
                                    <i class="fa fa-warning"> </i> Please select one tenant
                                </p>
                            </div>
                        </div>
                    </div>
                    <div class="row-fluid group-row">
                        <div class="span6 title-column">
                            <p>Show payment status<br>(paid, arrears, upcoming)?</p>
                        </div>
                        <div class="span6 status_section customize-radio customize-radio-inline radio-text">
                            <div>
                                <input id="ledger_tenant_status_no" type="radio" name="payment_status" class="payment_status Select_tenant_radio" value="No" checked="checked">
                                <label for="ledger_tenant_status_no">No</label>
                            </div>
                            <div>
                                <input id="ledger_tenant_status_yes" type="radio" name="payment_status" class="payment_status Select_tenant_radio" value="Yes">
                                <label for="ledger_tenant_status_yes">Yes</label>
                            </div>
                        </div>
                    </div>
                    <div class="row-fluid">
                        <div class="span3 rent_schedule_error ledger_msgs" style="display: none;">
                            <p class="help-block error">
                                <i class="fa fa-warning"> </i> Something went wrong please try again later...
                            </p>
                        </div>
                    </div>
                    <div class="do-it">
                        <a href="javascript:void(0);" class="print_rent_schedule">Generate &amp; Print</a>
                    </div>
                </form>
            </div>
        </div>
        <div class="row-fluid print-ledger-form">
            <div class="span12">
                <form id="print_ledger_form" action="?r=RentingSmartTenantLedger&amp;print_ledger=1&amp;property_id=NDA2MA" method="post" name="print_ledger_form">
                    <input id="print_tenant_ledgers" type="hidden" name="print_tenant_ledgers" value="1">
                    <div class="print_date row-fluid group-row">
                        <p class="period-from">Select period:</p>
                        <div class="input-box">
                            <input id="ledger_start_date" type="text" name="ledger_start_date" placeholder="Start date" class="ledger_dates hasDatepicker" value="Feb 11, 2018">
                        </div>
                        <p class="period-to">to</p>
                        <div class="input-box">
                            <input id="ledger_end_date" type="text" name="ledger_end_date" placeholder="End date" class="ledger_dates hasDatepicker" value="Feb 11, 2018">
                        </div>
                    </div>
                    <div class="row-fluid group-row">
                        <div class="span6 title-column">
                            <p>Ledger for which tenants?</p>
                        </div>
                        <div class="span6 rent_schedule_tenants_name_section">
                            <div class="customize-radio clearfix tenants_name_section">
                                <div id="show_all">
                                    <input id="ledger_tenant_name_radio_0" type="radio" name="ledger_tenant_id" class="Select_tenant_radio all_tenants_ids" value="4937" style="display: none;">
                                    <label for="ledger_tenant_name_radio_0">Show All Tenants</label>
                                </div>
                                <div class="rep_tenants_radio">
                                    <input id="ledger_tenant_name_radio_4937" type="radio" name="ledger_tenant_id" class="Select_tenant_radio ledger_tenant_name_radio" value="4937">
                                    <label for="ledger_tenant_name_radio_4937" class="ledger_tenant_name_label">Aaaaaaaaaa Bbbbbbbbb</label>
                                </div>
                                <div style="display: none;" class="stop_tenants_radio"> </div>
                            </div>
                            <div class="span3 tenant_name_error" style="width: 100%; margin: 0px 0px -30px -25px; display: none;">
                                <p class="help-block error">
                                    <i class="fa fa-warning"> </i> Please select one tenant
                                </p>
                            </div>
                        </div>
                    </div>
                    <div class="span6 ledger_error_msg ledger_msgs" style="margin: 0px 0px 10px 70px; display: none;">
                        <p class="help-block error">
                            <i class="fa fa-warning"> </i> Something went wrong please try again later...
                        </p>
                    </div>
                    <div class="do-it">
                        <a href="javascript:void(0);" class="print_ledger">Generate &amp; Print</a>
                    </div>
                </form>
            </div>
        </div> <!-- print-ledger-form -->
    </div>
    <div class="modal-footer">
        <img src="https://app.rentingsmart.com//RentingSmartHtml/schedule_wizard/../common/css/img/wizard_complete_bg_transparent.png" alt="Landlord of the year">
    </div>
</div>
<?php $this->load->view('front/footerlink');?>
</body>
</html>
