<?php $this->load->view('front/headlink'); ?>
<body>

<div class="setup_multistape">
    <div class="container">
        <?php $this->load->view('front/head_nav'); ?>
        <h3></h3>
        <div class="row form-group">
            <?= $this->load->view('front/step_div'); ?>
        </div>
    </div>
    <form class="container" onsubmit="return check_step_three_tenant()" action="property/insert_tanent" method="post">
        <div class="row setup-content" id="step-3">
            <div class="col-xs-12">
                <div class=" well text-center">
                    <h1 class="extra_heading"> Add Tenants
                        at <small>to the property <?= $this->utility_model->getFullPropertyAddress($property_id)?></small>
                    </h1>
                    <div class="container">
                        <div class="clearfix">
                            <div class="column">
                                <table class="table table-bordered table-hover" id="tab_logic">
                                    <thead>
                                    <tr>
                                        <th class="text-left">Name *</th>
                                        <th class="text-left">Surname *</th>
                                        <th class="text-left">Contact Details</th>
                                        <th class="text-left">Email</th>
                                        <th class="text-left">Recieve Correspondence</th>
                                        <th class="text-center"></th>
                                    </tr>
                                    </thead>
                                    <tbody class="input_fields_wrap table table-striped">
                                    <?php $i = 0;
                                    if ($all_tenant) {
                                        foreach ($all_tenant as $row) { ?>
                                            <tr>
                                                <td id="user_fname<?php echo $i; ?>">
                                                    <input type="hidden" name="user_id[]" class="form-control"
                                                           value="<?php echo $row['user_id'] ?>"/>
                                                    <input type="text" name='user_fname[]' placeholder='First Name'
                                                           class="form-control"
                                                           value="<?php echo $row['user_fname'] ?>"/>
                                                </td>
                                                <td id="user_lname<?php echo $i; ?>">
                                                    <input type="text" name='user_lname[]' placeholder='Last Name'
                                                           class="form-control"
                                                           value="<?php echo $row['user_lname'] ?>"/>
                                                </td>
                                                <td id="phone<?= $i; ?>">
                                                    <input type="tel" name='phone[]' placeholder='Contact Details'
                                                           class="form-control" value="<?php echo $row['phone'] ?>"/>
                                                </td>
                                                <td id="email<?= $i; ?>">
                                                    <input type="text" name='email[]' placeholder='Email'
                                                           class="form-control" value="<?php echo $row['email'] ?>"/>
                                                </td>
                                                <td id="correspondence<?= $i; ?>">
                                                    <div class="radio">
                                                        <label><input type="radio" value="1"
                                                                      name="correspondence[<?= $i; ?>]" <?php if ($row['correspondence'] == 1) {
                                                                echo "checked";
                                                            } ?>> Both&nbsp&nbsp</label>
                                                    </div>
                                                    <div class="radio">
                                                        <label><input type="radio" value="2"
                                                                      name="correspondence[<?= $i; ?>]" <?php if ($row['correspondence'] == 2) {
                                                                echo "checked";
                                                            } ?>>Phone</label>
                                                    </div>
                                                    <div class="radio">
                                                        <label><input type="radio" value="3"
                                                                      name="correspondence[<?= $i; ?>]" <?php if ($row['correspondence'] == 3) {
                                                                echo "checked";
                                                            } ?>>Email</label>
                                                    </div>
                                                </td>
                                                <td>
                                                    <a class="btn btn-danger pull-right remove_field">Delete Row</a>
                                                </td>
                                            </tr>
                                            <?php $i++;
                                        }
                                    } else { ?>
                                        <tr>
                                            <td id="user_fname0">
                                                <input type="hidden" name="user_id[]" class="form-control" value="0"/>
                                                <input type="text" name='user_fname[]' placeholder='First Name'
                                                       class="form-control"/>
                                            </td>
                                            <td id="user_lname0">
                                                <input type="text" name='user_lname[]' placeholder='Last Name'
                                                       class="form-control"/>
                                            </td>
                                            <td id="phone0">
                                                <input type="tel" name='phone[]' placeholder='Contact Details'
                                                       class="form-control"/>
                                            </td>
                                            <td id="email0">
                                                <input type="text" name='email[]' placeholder='Email'
                                                       class="form-control"/>
                                            </td>
                                            <td id="correspondence0">
                                                <div class="radio">
                                                    <label><input type="radio" value="1" checked
                                                                  name="correspondence[0]"> Both&nbsp&nbsp</label>
                                                </div>
                                                <div class="radio">
                                                    <label><input type="radio" value="2" name="correspondence[0]">Phone</label>
                                                </div>
                                                <div class="radio">
                                                    <label><input type="radio" value="3" name="correspondence[0]">Email</label>
                                                </div>
                                            </td>
                                            <td>
                                                <a class="btn btn-danger pull-right remove_field">Delete Row</a>
                                            </td>
                                        </tr>
                                    <?php } ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <a class="btn btn-primary pull-left add_field_button">Add Another Tenant</a>
                        <!--<a id='delete_row' class="btn btn-danger pull-right">Delete Row</a>-->
                    </div>
                    <div class="text-center extar_p">
                        <?php if ($add_another_lease == 'no') { ?>
                            <a href="stepTwo/<?= $property_id; ?>" class="btn btn-light btn-md">Back</a>
                        <?php } ?>
                        <input type="hidden" name="add_another_lease" value="<?= $add_another_lease; ?>">
                        <input type="hidden" name="property_id" value="<?= $property_id; ?>">
                        <input type="submit" value="Next Step" class="btn btn-primary btn-md">
                    </div>
                </div>
            </div>
        </div>
    </form>
</div>
<?php $this->load->view('front/footerlink'); ?>
<script src="//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
<script type="text/javascript">
    $(document).ready(function () {
        var max_fields = 999; //maximum input boxes allowed
        var wrapper = $(".input_fields_wrap"); //Fields wrapper
        var add_button = $(".add_field_button"); //Add button ID

        x = <?php if ($all_tenant) {
            echo count($all_tenant) - 1;
        } else {
            echo 0;
        }?>; //initlal text box count
        $(add_button).click(function (e) { //on add input button click
            e.preventDefault();
            if (x < max_fields) { //max input box allowed
                x++; //text box increment
                $(wrapper).append('<tr><td id="user_fname' + x + '"><input type="hidden" name="user_id[]" class="form-control" value="0"/><input type="text"  name="user_fname[]"  placeholder="First Name" class="form-control"/></td><td id="user_lname' + x + '"><input type="text" name="user_lname[]" placeholder="Last Name" class="form-control"/></td><td id="phone' + x + '"><input type="tel" name="phone[]" placeholder="Contact Details" class="form-control"/></td><td id="email' + x + '"><input type="text" name="email[]" placeholder="Email" class="form-control"/></td><td id="correspondence' + x + '"><div class="radio"><label><input type="radio" value="1" checked name="correspondence[' + x + ']"> Both&nbsp&nbsp</label></div><div class="radio"><label><input type="radio" value="2" name="correspondence[' + x + ']">Phone</label></div><div class="radio"><label><input type="radio" value="3" name="correspondence[' + x + ']">Email</label></div></td><td><a class="btn btn-danger pull-right remove_field">Delete Row</a></td></tr>'); //add input box
            }
        });

        $(wrapper).on("click", ".remove_field", function (e) { //user click on remove text
            if (x > 0) {
                e.preventDefault();
                $(this).closest('tr').remove();
                x--; //{{Dev.Mahmud: decreasing x creates problem}}
            }
        })
    });
</script>
<script>
    function check_step_three_tenant() {
        //var regex = new RegExp("^(?:\\+?88)?8801[15-9]\\d{8}$");
        var flag = true;
        var fname_ok = 0;
        var lname_ok = 0;
        var phone_ok = 0;
        var email_ok = 0;
        var first_name = document.getElementsByName('user_fname[]');
        var last_name = document.getElementsByName('user_lname[]');
        var phone = document.getElementsByName('phone[]');
        var email = document.getElementsByName('email[]');
        for (var i = 0; i < first_name.length; i++) {
            if (first_name[i].value == false || first_name[i].value == '') {
                $('#user_fname' + i).addClass("has-error");
                fname_ok = fname_ok + 1;
            }
            else {
                $('#user_fname' + i).removeClass("has-error");
            }
        }
        for (var j = 0; j < last_name.length; j++) {
            if (last_name[j].value == false || last_name[j].value == '') {
                $('#user_lname' + j).addClass("has-error");
                lname_ok = lname_ok + 1;
            }
            else {
                console.log('b');
                $('#user_lname' + j).removeClass("has-error");
            }
        }
        for (var k = 0; k < phone.length; k++) {

            if (phone[k].value != '') {
                if (!$.isNumeric(phone[k].value)) {
                    $('#phone' + k).addClass("has-error");
                    phone_ok = phone_ok + 1;
                } else {
                    $('#phone' + k).removeClass("has-error");
                }
            } else {
                $('#phone' + k).removeClass("has-error");
            }
        }
        for (var l = 0; l < email.length; l++) {
            if (email[l].value != '') {
                if (validateEmail(email[l].value) == false
                ) {
                    $('#email' + l).addClass("has-error");
                    email_ok = email_ok + 1;
                }
                else {
                    $('#email' + l).removeClass("has-error");
                }
            } else {
                $('#email' + l).removeClass("has-error");
            }

        }
        if (fname_ok <= 0 && lname_ok <= 0 && phone_ok <= 0 && email_ok <= 0) {
            flag = true;
        }
        else {
            flag = false;
        }
        return flag;
    }


    function validateEmail(email) {
        var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        return re.test(String(email).toLowerCase());
    }
</script>
</body>
</html>
