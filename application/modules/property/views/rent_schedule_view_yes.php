<!DOCTYPE html>
<html>
<head>
	<link href="assets/css/bootstrap.min.css" rel="stylesheet">
</head>
<body>
	<div class="container">
		<?php
			if($lease_detail[0]['lease_pay_type']==1)
			{
				$pay_type_duration = "week";
				$lease_paying_type = 7;
			}
			elseif($lease_detail[0]['lease_pay_type']==2)
			{
				$pay_type_duration = "fortnight";
				$lease_paying_type = 14;
			}
			elseif($lease_detail[0]['lease_pay_type']==3)
			{
				$pay_type_duration = "Four week";
				$lease_paying_type = 28;
			}
			else
			{
				$pay_type_duration = "Month";
				$lease_paying_type = 30;
			}
		?>

		<h3>RENT SCHEDULE</h3>
		<br>
		<br>
		<div>
			<p>Property: <?=$property_info[0]['property_address']?> </p>
			<p>Tenants: <?php foreach($tenant_info as $key=>$row){ if($key!=0){echo ' & ';}echo  $row['user_fname']. ' '.$row['user_lname'];}?></p>
			<p>Lease: <?=$lease_detail[0]['lease_name'];?></p>
			<p>Total Rent : $<?=$total_tenant_amount;?> per <?=$pay_type_duration;?> </p>
			<p>Landlord: <?=$user_info[0]['user_fname']?> <?=$user_info[0]['user_lname']?></p>
			<p>Date Created: <?=$property_info[0]['created_at'];?></p>
		</div>
		<br>

        <?php if(count($all_payments_info) > 0) {?>
            <br>
        <h2>Money In</h2>
		<table class="table">
			<thead>
				<tr>
					<td style="text-align:center;width:20%;"><h4>Due Date</h4></td>
					<td style="text-align:center;width:20%;"><h4>Period</h4></td>
					<td style="text-align:center;width:20%;"><h4>Amount</h4></td>
					<?php if($rent_payment_status==1) { ?>
						<td style="text-align:center;width:20%;"><h4>Rent Received</h4></td>
						<td style="text-align:center;width:20%;"><h4>Status</h4></td>
					<?php } ?>
				</tr>
			</thead>
			<tbody>
				<?php foreach($all_payments_info as $row){?>
				<tr>
					<td style="text-align:center;width:20%;"><?=date('d M,Y', strtotime($row['payment_start_period']));?></td>
					<td style="text-align:center;width:20%;"><?=date('d M', strtotime($row['payment_start_period']));?> - <?=date('M d', strtotime($row['payment_end_period']));?></td>
					<td style="text-align:center;width:20%;">$
						<?php echo number_format((float)(($total_tenant_amount/$lease_paying_type)*((abs(strtotime($row['payment_start_period'])-strtotime($row['payment_end_period']))/86400)+1)), 2, '.', '');?>
					</td>
					<?php if($rent_payment_status==1) { ?>
						<td style="text-align:center;width:20%;"><?php if($row['lease_rent_recieved']!=0){?>$<?=$row['lease_rent_recieved'];?><?php }else{echo "-";}?></td>
						<td style="text-align:center;width:20%;"><?php if($row['lease_present_status']==1){echo "Paid";}elseif($row['lease_present_status']==2){echo "Upcoming";}else{echo "Arrears";}?></td>
					<?php } ?>
				</tr>
				<?php }?>
			</tbody>
		</table>
        <?php }?>
	</div>
</body>
</html>