<?php $this->load->view('front/headlink'); ?>
<body>
<style>
    .red-border {
        border-color: red !important;
    }
</style>
<div class="setup_multistape">
    <div class="container">
        <?php $this->load->view('front/head_nav'); ?>
        <h3></h3>
        <div class="row form-group">
            <?= $this->load->view('front/step_div'); ?>
        </div>
    </div>
    <form class="container" method="post" id="lease_and_rent_form" action="insert_lease_and_rent_details"
          enctype="multipart/form-data">
        <div class=" setup-content" id="step-5">
            <div class="">
                <div class=" well ">
                    <!--<pre><?php /*print_r($property)*/?></pre>-->
                    <!--<button id="confirm_before_submit" class="btn btn-primary btn-md pull-right"  data-toggle="modal" data-target="#submit_btn_modal_call"><span class="glyphicon glyphicon-plus" aria-hidden="true"></span>Confirm Details</button>
                    --><h1  class="extra_heading"> Lease & Rent Details

                                <small>for the property at <?= $this->utility_model->getFullPropertyAddress($property_id)?></small>

                    </h1>
                    <div class="step_5_header">
                        <div class="step_5_header_cat" >
                            <h4 style="font-weight: bold">Lease Dates</h4>
                            <h4 style="font-weight: bold">Rent Payments</h4>
                            <h4 style="font-weight: bold">Bond</h4>
                        </div>
                        <div class="step_5_header_content">
                            <p>
                                <span id="lease_start_date"><?php echo date("M d, Y", strtotime($all_lease_info[0]['lease_start_date'])) ?></span>
                                to
                                <span id="lease_end_date"><?php echo date("M d, Y", strtotime($all_lease_info[0]['lease_end_date'])) ?></span>
                            </p>
                            <p>
                                <span style="text-transform:uppercase ">
                                    <span class="ss_color">$</span> <span id="per_period_amount"><?= $all_lease_info[0]['lease_per_period_payment'] ?></span>
                                    due
                                    <span id="pay_day"><?= $paying_time ?></span>
                                </span>

                            </p>
                            <p>Bond Lodged <span><span class="ss_color">$</span><span id="bond_value"><?php if($bond_info){echo $bond_info[0]['bond_amount'];}?></span></span>
                            </p>
                        </div>
                    </div>
                </div>
                <div class="ss_price_list">
                    <h5>
                        Below is your tenants rent schedule. This shows all rental payments due from the commencement date of the lease until the expiry.
                    </h5>

                    <h6><span style="text-decoration: underline;"></span></h6>
                    <div class="step_5_table">
                        <table class="table table-bordered table-striped text-center">
                            <thead>
                                <tr>
                                    <th class="text-center">Period</th>
                                    <th class="text-center">Due Date</th>
                                    <th class="text-center">Rent Period</th>
                                    <th class="text-center s5_amount_due">Amount Due</th>
                                </tr>
                            </thead>

                            <input type="hidden" name="property_id" value="<?= $property_id ?>">
                            <input type="hidden" name="lease_id" value="<?= $lease_id ?>">
                            <div id="list-wrapper">
                                <tbody>
                                <?php echo $step_five_ul_view ?>
                                </tbody>
                            </div>
                        </table>
                    </div>
                    <div class="text-center extar_p">
                        <a href="stepFour/<?= $property_id; ?>" class="btn btn-light btn-md" action="action"
                           onclick="window.history.go(-1);">Back</a>
                        <button id="confirm_before_submit_two" class="btn btn-primary btn-md"  data-toggle="modal" data-target="#submit_btn_modal_call">Confirm Details</button>
                        <div class="modal fade" id="submit_btn_modal_call" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                            <div class="modal-dialog" role="document">
                                <div class="modal-content text-center">
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                        <h4 class="modal-title" id="myModalLabel">Confirmation</h4>
                                    </div>
                                    <div class="modal-body">
                                        <h4>Please confirm all details have been entered in correctly before proceeding.</h4>
                                        <br/><br/><br/>
                                        <input type="button" class="btn btn-primary" data-dismiss="modal" aria-label="Close" value="Cancel">
                                        <button id="submit_btn" class="btn btn-primary btn-md">Yes Proceed</button>
                                        <br/><br/><br/>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </form>
</div>
<?php $this->load->view('front/footerlink'); ?>
</body>
</html>

<?php
function dateformat_PHP_to_jQueryUI($php_format)
{
    $SYMBOLS_MATCHING = array(
        // Day
        'd' => 'dd',
        'D' => 'D',
        'j' => 'd',
        'l' => 'DD',
        'N' => '',
        'S' => '',
        'w' => '',
        'z' => 'o',
        // Week
        'W' => '',
        // Month
        'F' => 'MM',
        'm' => 'mm',
        'M' => 'M',
        'n' => 'm',
        't' => '',
        // Year
        'L' => '',
        'o' => '',
        'Y' => 'yy',
        'y' => 'y',
        // Time
        'a' => '',
        'A' => '',
        'B' => '',
        'g' => '',
        'G' => '',
        'h' => '',
        'H' => '',
        'i' => '',
        's' => '',
        'u' => ''
    );
    $jqueryui_format = "";
    $escaping = false;
    for ($i = 0; $i < strlen($php_format); $i++) {
        $char = $php_format[$i];
        if ($char === '\\') // PHP date format escaping character
        {
            $i++;
            if ($escaping) $jqueryui_format .= $php_format[$i];
            else $jqueryui_format .= '\'' . $php_format[$i];
            $escaping = true;
        } else {
            if ($escaping) {
                $jqueryui_format .= "'";
                $escaping = false;
            }
            if (isset($SYMBOLS_MATCHING[$char]))
                $jqueryui_format .= $SYMBOLS_MATCHING[$char];
            else
                $jqueryui_format .= $char;
        }
    }
    return $jqueryui_format;
}

?>

<script>
    calculation();

    function calculation() {
        var jquery_ui_dateformat = '<?= dateformat_PHP_to_jQueryUI("d M Y")?>';

        $(document).ready(function () {

            $('.single_datepicker').each(function (index, value) {
                var single_datepicker = $(this);
                single_datepicker.datepicker({
                    dateFormat: jquery_ui_dateformat,
                });
            });

            $('.range_datepicker').each(function (index, value) {
                var range_datepicker = $(this);
                range_datepicker.daterangepicker({
                    locale: {
                        format: jquery_ui_dateformat,
                    },
                    maxDate: '<?= date('d M Y', strtotime($payment_start_date)) ?>',
                    mintDate: '<?= date('d M Y', strtotime($payment_end_date)) ?>',
                });
            });


            /* $('#StartLeaseDate').daterangepicker();*/

            $('.edit_lease_schedule').on('click', function () {

                var edit_span = $(this);
                var edit_span_id = edit_span.attr('id');
                var save_span = edit_span.closest('.edit_or_save').find('.save_lease_schedule');
                var save_span_id = save_span.attr('id');

                var save_anchor = save_span.find('.save_anchor');
                var save_anchor_id = save_anchor.attr('id');
                var cancel_anchor = save_span.find('.cancel_anchor');
                var cancel_anchor_id = cancel_anchor.attr('id');

                var schedule_row = edit_span.closest('.schedule_row');
                var schedule_row_id = schedule_row.attr('id');
                var schedule_row_ul_count = schedule_row.attr('ul-count-attr');

                //----------
                var single_date_span = schedule_row.find('.input_single_date_span');
                var single_date_span_id = single_date_span.attr('id');
                var single_date_div = schedule_row.find('.input_single_date_div');
                var single_date_div_id = single_date_span.attr('id');
                var single_datepicker = schedule_row.find('.single_datepicker');
                var single_datepicker_id = single_datepicker.attr('id');

                var range_date_span = schedule_row.find('.input_range_date_span');
                var range_date_span_id = range_date_span.attr('id');
                var range_date_div = schedule_row.find('.input_range_date_div');
                var range_date_div_id = range_date_div.attr('id');
                var range_datepicker = schedule_row.find('.range_datepicker');
                var range_datepicker_id = range_datepicker.attr('id');

                var span_amount = schedule_row.find('.span_amount');
                var span_amount_id = span_amount.attr('id');
                var input_amount = schedule_row.find('.input_amount');
                var input_amount_id = input_amount.attr('id');
                //----------

                single_datepicker.val($.trim(single_date_span.html()));
                range_datepicker.val($.trim(range_date_span.html()));
                input_amount.val(Number(parseFloat($.trim(span_amount.html()))).toFixed(2));

                edit_span.hide();
                save_span.show();

                single_date_span.hide();
                range_date_span.hide();
                span_amount.hide();

                single_date_div.show();
                range_date_div.show();
                input_amount.show();
            });

            $('.cancel_anchor').on('click', function () {

                var cancel_anchor = $(this);
                var cancel_anchor_id = cancel_anchor.attr('id');
                var save_span = cancel_anchor.closest('.edit_or_save').find('.save_lease_schedule');
                var save_span_id = save_span.attr('id');
                var edit_span = save_span.closest('.edit_or_save').find('.edit_lease_schedule');
                var edit_span_id = edit_span.attr('id');

                var save_anchor = save_span.find('.save_anchor');
                var save_anchor_id = save_anchor.attr('id');

                var schedule_row = edit_span.closest('.schedule_row');
                var schedule_row_id = schedule_row.attr('id');
                var schedule_row_ul_count = schedule_row.attr('ul-count-attr');

                //----------
                var single_date_span = schedule_row.find('.input_single_date_span');
                var single_date_span_id = single_date_span.attr('id');
                var single_date_div = schedule_row.find('.input_single_date_div');
                var single_date_div_id = single_date_span.attr('id');
                var single_datepicker = schedule_row.find('.single_datepicker');
                var single_datepicker_id = single_datepicker.attr('id');

                var range_date_span = schedule_row.find('.input_range_date_span');
                var range_date_span_id = range_date_span.attr('id');
                var range_date_div = schedule_row.find('.input_range_date_div');
                var range_date_div_id = range_date_div.attr('id');
                var range_datepicker = schedule_row.find('.range_datepicker');
                var range_datepicker_id = range_datepicker.attr('id');

                var span_amount = schedule_row.find('.span_amount');
                var span_amount_id = span_amount.attr('id');
                var input_amount = schedule_row.find('.input_amount');
                var input_amount_id = input_amount.attr('id');
                //----------

                save_span.hide();
                edit_span.show();

                single_date_div.hide();
                range_date_div.hide();
                input_amount.hide();

                single_date_span.show();
                range_date_span.show();
                span_amount.show();

            });

            $('.save_anchor').on('click', function () {

                var save_anchor = $(this);
                var save_anchor_id = save_anchor.attr('id');
                var save_span = save_anchor.closest('.edit_or_save').find('.save_lease_schedule');
                var save_span_id = save_span.attr('id');
                var edit_span = save_span.closest('.edit_or_save').find('.edit_lease_schedule');
                var edit_span_id = edit_span.attr('id');

                var cancel_anchor = save_span.find('.cancel_anchor');
                var cancel_anchor_id = cancel_anchor.attr('id');

                var schedule_row = edit_span.closest('.schedule_row');
                var schedule_row_id = schedule_row.attr('id');
                var schedule_row_ul_count = schedule_row.attr('ul-count-attr');

                //----------
                var single_date_span = schedule_row.find('.input_single_date_span');
                var single_date_span_id = single_date_span.attr('id');
                var single_date_div = schedule_row.find('.input_single_date_div');
                var single_date_div_id = single_date_span.attr('id');
                var single_datepicker = schedule_row.find('.single_datepicker');
                var single_datepicker_id = single_datepicker.attr('id');

                var range_date_span = schedule_row.find('.input_range_date_span');
                var range_date_span_id = range_date_span.attr('id');
                var range_date_div = schedule_row.find('.input_range_date_div');
                var range_date_div_id = range_date_div.attr('id');
                var range_datepicker = schedule_row.find('.range_datepicker');
                var range_datepicker_id = range_datepicker.attr('id');

                var span_amount = schedule_row.find('.span_amount');
                var span_amount_id = span_amount.attr('id');
                var input_amount = schedule_row.find('.input_amount');
                var input_amount_id = input_amount.attr('id');
                //----------


                if (checkSaveConditions(single_datepicker, range_datepicker, input_amount) == true) {
                    if (range_date_span.html() != range_datepicker.val()) {
                        console.log('change');
                        calculateAndChangeList(schedule_row_ul_count, range_datepicker.val(), "<?= date('d M Y', strtotime($payment_end_date)) ?>")
                    } else {
                        console.log('do not change');
                    }

                    single_date_span.html(single_datepicker.val());
                    range_date_span.html(range_datepicker.val());
                    span_amount.html(input_amount.val());

                    save_span.hide();
                    edit_span.show();

                    single_date_div.hide();
                    range_date_div.hide();
                    input_amount.hide();

                    single_date_span.show();
                    range_date_span.show();
                    span_amount.show();


                }


            });

            $('#confirm_before_submit, #confirm_before_submit_two').on('click', function (e) {
                e.preventDefault();

            });

            $('#submit_btn').on('click', function (e) {
                // e.preventDefault();

                if (validate_form() == true) {
                    $('#lease_and_rent_form').submit();
                }
            });

            function checkSaveConditions(single_datepicker, range_datepicker, input_amount) {

                var err_count = 0;

                var decimal_regexp = /^\d+\.\d{0,2}$/; //upto 2 decimal places

                var single_date = new Date(single_datepicker.val());
                var exploded_range_date = explode("-", range_datepicker.val());
                var range_date_st = new Date($.trim(exploded_range_date[0]));
                var range_date_en = new Date($.trim(exploded_range_date[1]));


                if (
                    input_amount.val() == '' || input_amount.val() == null ||
                    input_amount.val() == false || input_amount.val() === 'undefined' || !decimal_regexp.test(input_amount.val())
                ) {
                    err_count++;
                    if (!input_amount.hasClass('red-border')) {
                        input_amount.addClass('red-border');
                    }
                } else {
                    if (input_amount.hasClass('red-border')) {
                        input_amount.removeClass('red-border');
                    }
                }

                if (singleDateInRange(single_date, range_date_st, range_date_en) == false) {
                    err_count++;
                    if (!single_datepicker.hasClass('red-border')) {
                        single_datepicker.addClass('red-border');
                    }
                } else {
                    if (single_datepicker.hasClass('red-border')) {
                        single_datepicker.removeClass('red-border');
                    }
                }

                if (err_count > 0) {
                    return false;
                } else {
                    return true;
                }
            }

            function singleDateInRange(singe_date, range_date_st, range_date_en) {
                // console.log(singe_date);
                // console.log(range_date_st);
                // console.log(range_date_en);
                if (singe_date >= range_date_st && singe_date <= range_date_en) {
                    return true;
                } else {
                    return false;
                }
            }

            function validate_form() {
                var validated = false;
                var error_count = 0;

                $('.schedule_row').each(function (index, value) {
                    var schedule_row = $(this);
                    var schedule_row_ul_count = schedule_row.attr('ul-count-attr');
                    var schedule_row_id = schedule_row.attr('id');

                    //----------
                    var single_date_span = schedule_row.find('.input_single_date_span');
                    var single_date_span_id = single_date_span.attr('id');
                    var single_date_div = schedule_row.find('.input_single_date_div');
                    var single_date_div_id = single_date_span.attr('id');
                    var single_datepicker = schedule_row.find('.single_datepicker');
                    var single_datepicker_id = single_datepicker.attr('id');

                    var range_date_span = schedule_row.find('.input_range_date_span');
                    var range_date_span_id = range_date_span.attr('id');
                    var range_date_div = schedule_row.find('.input_range_date_div');
                    var range_date_div_id = range_date_div.attr('id');
                    var range_datepicker = schedule_row.find('.range_datepicker');
                    var range_datepicker_id = range_datepicker.attr('id');

                    var span_amount = schedule_row.find('.span_amount');
                    var span_amount_id = span_amount.attr('id');
                    var input_amount = schedule_row.find('.input_amount');
                    var input_amount_id = input_amount.attr('id');
                    //----------

                    if (checkSaveConditions(single_datepicker, range_datepicker, input_amount) == false) {
                        error_count++;
                    }

                });

                if (error_count == 0) {
                    validated = true;
                }

                return validated;
            }

        });
    }

    function calculateAndChangeList(count, range_val, final_date) {

        var exploded_range_val = explode("-", range_val);
        var start_point = $.trim(exploded_range_val[1]);
        var end_point = $.trim(final_date);

        $('.schedule_row').each(function (index, value) {
            var schedule_row = $(this);
            var schedule_row_ul_count = schedule_row.attr('ul-count-attr');

            if (schedule_row_ul_count > count) {
                schedule_row.remove();
            }
        });

        /*console.log(range_val);
        console.log(exploded_range_val);
        console.log(start_point);
        console.log(end_point);*/
        $('#loading').show();
        $.ajax({
            type: "post",
            url: "change_lease_schedule_by_ajax",
            data: {
                ul_starting_point: parseInt(count) + 1,
                start_point: start_point.toString(),
                end_point: end_point.toString(),
                property_id: '<?= $property_id?>',
                lease_id: '<?= $lease_id?>',
            },
            success: function (html) {
                $('#list-wrapper').append(html);
                calculation();
            }
        })
    }


    /*----- utility functions <starts> -----*/
    function explode(delimiter, string, limit) {
        //  discuss at: http://locutus.io/php/explode/
        // original by: Kevin van Zonneveld (http://kvz.io)
        //   example 1: explode(' ', 'Kevin van Zonneveld')
        //   returns 1: [ 'Kevin', 'van', 'Zonneveld' ]

        if (arguments.length < 2 ||
            typeof delimiter === 'undefined' ||
            typeof string === 'undefined') {
            return null
        }
        if (delimiter === '' ||
            delimiter === false ||
            delimiter === null) {
            return false
        }
        if (typeof delimiter === 'function' ||
            typeof delimiter === 'object' ||
            typeof string === 'function' ||
            typeof string === 'object') {
            return {
                0: ''
            }
        }
        if (delimiter === true) {
            delimiter = '1'
        }

        // Here we go...
        delimiter += '';
        string += '';

        var s = string.split(delimiter);

        if (typeof limit === 'undefined') return s;

        // Support for limit
        if (limit === 0) limit = 1;

        // Positive limit
        if (limit > 0) {
            if (limit >= s.length) {
                return s
            }
            return s
                .slice(0, limit - 1)
                .concat([s.slice(limit - 1)
                    .join(delimiter)
                ])
        }

        // Negative limit
        if (-limit >= s.length) {
            return []
        }

        s.splice(s.length + limit);
        return s
    }

    function inArray(needle, haystack) {
        var ret = false;
        if (haystack.indexOf(needle) != -1) {
            ret = true;
        }
        return ret;
    }

    /*----- utility functions <ends> -----*/
</script>
