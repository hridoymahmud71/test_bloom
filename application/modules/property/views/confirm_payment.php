<?php $this->load->view('front/headlink'); ?>
<body>
    
<div class="setup_multistape">
    <div class="container">
 <div class="ss_main_container"></div>
        <?php $this->load->view('front/head_nav'); ?>

        <div class="ss_schedule">
            <h3>Rent Schedule for the property at
                <span>
                <?= $this->utility_model->getFullPropertyAddress($property_info[0]['property_id'])?></span>
            </h3>
            <div class="col-md-4">
                <div class="ss_views_by">
                    View by:
                    <ul>
                        <li><a href="#rentreceived" aria-controls="rentreceived" role="tab" data-toggle="tab">Rent
                                Received</a></li>
                        <li class="active"><a href="#periodschedule" aria-controls="periodschedule" role="tab"
                                              data-toggle="tab">Period Schedule</a></li>
                    </ul>
                </div>
            </div>
            <div class="col-md-4">
                <?php foreach ($get_tenant_list as $row) { ?>
                    <p class="tenant_box">
                    <div class="col-md-6">
                        <?= $row['user_fname']; ?> <?= $row['user_lname']; ?>
                    </div>
                    <div class="col-md-6">
                        <?php if ($row['payment_update_status'] == 1) { ?>
                            <span id="tenant_payment_stats" class="label label-success">
                            up to date
                        </span>
                        <?php } elseif ($row['payment_update_status'] == 2) { ?>
                            <span id="tenant_payment_stats" class="label label-danger">
                            $ <?= $row['payment_update_by'] ?> arrears
                        </span>
                        <?php } elseif ($row['payment_update_status'] == 3) { ?>
                            <span id="tenant_payment_stats" class="label label-success">
                            $ <?= $row['payment_update_by'] ?> ahead
                        </span>
                        <?php } elseif ($row['payment_update_status'] == 4) { ?>
                            <span id="tenant_payment_stats" class="label label-danger">
                            $ <?= $row['payment_update_by'] ?> arrears
                        </span>
                        <?php } ?>
                    </div>
                    </p>
                <?php } ?>
            </div>
            <div class="col-md-4">
                <div class="ss_enter_rent">
                    <a data-toggle="modal" style="display: none;" href="#recive_rent" id="mark_as_paid"
                       class="btn btn-light btn-lg mark_paid">Enter Received Rent Now</a>
                    <a data-toggle="modal" data-target="#printshare" href="javascript:void(0);" id=""
                       class="btn btn-primary btn-lg">
                        Print Ledgers &amp; Schedules
                    </a>
                </div>
            </div>
            <div class="payment_table">
                <div class="tab-content">
                    <div role="tabpanel" class="tab-pane" id="rentreceived">
                        <form action="property/update_rent_recieved" method="post">
                            <div class="step_5_table">
                                <ul class="table_header top-header-div">
                                    <li>Amount Received</li>
                                    <li>Date Received</li>
                                    <li>Payer</li>
                                    <li>Paid by</li>
                                    <li>Period(s)</li>
                                    <li> Actions</li>
                                </ul>
                                <?php foreach ($all_tenant_payments_info as $row) { ?>
                                    <ul class="table_row schedule_list">
                                        <li id="num">
                                            <span class="input_text" id="period_amount"
                                                  style="display: inline;">$<?= $row['shared_amount'] ?></span>
                                            <input class="input-small form-control dollar_blue pay_period_amount"
                                                   type="Number" name="shared_amount[]"
                                                   value="<?= $row['shared_amount'] ?>" style="display: none;">
                                        </li>
                                        <li>
                                            <h1>Recived Date</h1>
                                            <span class="input_text" id="due_date"
                                                  style="display: inline;"><?= date('M d', strtotime($row['schedule_start_date'])) ?></span>
                                            <div style="display:none" class="input-group input-small">
                                                <input class="form-control due_date" type="text"
                                                       name="schedule_start_date[]"
                                                       value="<?= date('M d', strtotime($row['schedule_start_date'])) ?>"
                                                       readonly>
                                                <span class="input-group-addon"><i
                                                            class="glyphicon glyphicon-calendar"></i></span>
                                            </div>
                                        </li>
                                        <li>
                                            <span class="input_text" id="Name_lastname"
                                                  style="display: inline;"><?= $row['user_fname'] ?> <?= $row['user_lname'] ?></span>
                                            <select required class="input-small form-control tenant_id"
                                                    name="tenant_id[]" style="display: none;">
                                                <option value="">Select Tenant</option>
                                                <?php foreach ($get_tenant_list as $row2) { ?>
                                                    <option <?php if ($row2['tenant_id'] == $row['tenant_id']) {
                                                        echo "selected";
                                                    } ?> value="<?= $row2['tenant_id'] ?>"><?= $row2['user_fname'] ?> <?= $row['user_lname'] ?></option>
                                                <?php } ?>
                                            </select>
                                            <!-- <input class="input-small dollar_blue pay_period_amount form-control" type="text"  value="<?= $row['user_fname'] ?> <?= $row['user_lname'] ?>" style="display: none;"> -->
                                        </li>
                                        <li>
                                            <span class="input_text" id="payment_mathod"
                                                  style="display: inline;"><?php if ($row['payment_method'] == 1) {
                                                    echo "Bank Transfer";
                                                } elseif ($row['payment_method'] == 2) {
                                                    echo "cash";
                                                } elseif ($row['payment_method'] == 3) {
                                                    echo "Cheque";
                                                } elseif ($row['payment_method'] == 4) {
                                                    echo "credit card";
                                                } else {
                                                    echo "Other method";
                                                }; ?></span>
                                            <select required class="input-small form-control payment_method"
                                                    name="payment_method[]" style="display: none;">
                                                <option value="">Select Method</option>
                                                <option <?php if ($row['payment_method'] == 1) {
                                                    echo "selected";
                                                } ?> value="1">Bank Transfer
                                                </option>
                                                <option <?php if ($row['payment_method'] == 2) {
                                                    echo "selected";
                                                } ?> value="2">Cash
                                                </option>
                                                <option <?php if ($row['payment_method'] == 3) {
                                                    echo "selected";
                                                } ?> value="3">Cheque
                                                </option>
                                                <option <?php if ($row['payment_method'] == 4) {
                                                    echo "selected";
                                                } ?> value="4">Credit Card
                                                </option>
                                                <option <?php if ($row['payment_method'] == 5) {
                                                    echo "selected";
                                                } ?> value="5">Other method
                                                </option>
                                            </select>
                                        </li>
                                        <li>
                                            <span class="" id="Name_lastname"
                                                  style="display: inline;"><?= date('M d', strtotime($row['schedule_start_date'])) ?>
                                                - <?= date('M d', strtotime($row['schedule_end_date'])) ?></span>
                                        </li>
                                        <li>
                                            <span class="edit_lease_schedule" style="display: inline;"><a
                                                        href="javascript:void(0);">edit |</a></span> <a
                                                    href="property/delete_single_amount/<?= $row['lease_tenant_payment_scedhule_id'] ?>"
                                                    style="position:absolute; margin-left: 5px;"> delete</a>
                                            <!-- <span class="save_lease_schedule" style="display: none;"><a href="javascript:void(0);" class="save_edits">save</a> <a href="javascript:void(0);" class="cencel_edit" style="position:absolute; margin-left: 5px;">| delete</a></span> -->
                                        </li>
                                    </ul>
                                    <input type="hidden" name="lease_tenant_payment_scedhule_id[]"
                                           value="<?= $row['lease_tenant_payment_scedhule_id']; ?>">
                                <?php } ?>
                            </div>
                            <div class="text-center extar_p">
                                <a href="stepFive/<?= $property_id; ?>/<?= $lease_id; ?>/back"
                                   class="btn btn-light btn-md">Back</a>
                                <button id="activate-step-3" class="btn btn-primary btn-md" data-toggle="modal"
                                        data-target="#complete_wizard_update_rent_recieved">Complete Setup
                                </button>
                            </div>
                            <div class="modal fade" id="complete_wizard_update_rent_recieved" tabindex="-1"
                                 role="dialog" aria-labelledby="myModalLabel">
                                <div class="modal-dialog" role="document">
                                    <div class="modal-content text-center">
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                <span aria-hidden="true">&times;</span></button>
                                            <h4 class="modal-title" id="myModalLabel">Wizard Complete</h4>
                                        </div>
                                        <div class="modal-body">
                                            <h4>Next up is your personal dashboard<br/> and property control center.
                                            </h4>
                                            <p> From there you can add maintenance issues,<br/> expenses, mark rents
                                                received,<br/> set up invoices and more!</p>
                                            <br/><br/><br/>
                                            <input type="submit" class="btn btn btn-primary btn-lg"
                                                   value="Continue to get organised">
                                            <br/><br/><br/><br/><br/><br/>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                    <div role="tabpanel" class="tab-pane active" id="periodschedule">
                        <div class="ss_further">
                            <ul class="table_header top-header-div">
                                <li><strong>Period</strong></li>
                                <li>Due Date</li>
                                <li>Amount Due</li>
                                <li>Rent Received</li>
                                <li>Status</li>
                            </ul>
                            <p class="text-center">
                                <span id="lease_dates_text">Lease dates: <?= date('F d, Y', strtotime($lease_info[0]['lease_start_date'])); ?>
                                    – <?= date('F d, Y', strtotime($lease_info[0]['lease_end_date'])); ?>.</span>
                                <a href="javascript:void(0);" class="furter_rent_show_hide">View more Future Periods</a>
                            </p>

                            <!-- style="display:none;" -->
                            <div id="furter_rent_list" style="display:none;">
                                <?php foreach ($all_payments_info as $row) { ?>
                                    <?php if ($row['payment_start_period'] > date('Y-m-d')) { ?>
                                        <ul class="table_row schedule_list">
                                            <li><strong><?= date('M d', strtotime($row['payment_start_period'])); ?>
                                                    - <?= date('M d', strtotime($row['payment_end_period'])); ?></strong>
                                            </li>
                                            <li><?= date('M d, Y', strtotime($row['payment_due_date'])); ?></li>
                                            <li><strong>$<?= $row['payment_due_amount'] ?></strong></li>
                                            <li><strong>$<?= $row['lease_rent_recieved'] ?></strong></li>
                                            <?php if ($row['lease_present_status'] == 1) { ?>
                                                <li><span id="lease_label" class="label label-paid">Paid</span></li>
                                            <?php } elseif ($row['lease_present_status'] == 2) { ?>
                                                <li><span class="label label-info">Upcoming</span></li>
                                            <?php } elseif ($row['lease_present_status'] == 3) { ?>
                                                <li><span class="label label-danger">Arrears</span></li>
                                            <?php } ?>
                                        </ul>
                                    <?php }
                                } ?>
                            </div>
                            <?php foreach ($all_payments_info as $row) { ?>
                                <?php if ($row['payment_start_period'] <= date('Y-m-d')) { ?>
                                    <ul class="table_row schedule_list <?php if ($row['payment_start_period'] <= date('Y-m-d') && $row['payment_end_period'] >= date('Y-m-d')) {
                                        echo 'paid_payment';
                                    } ?>">
                                    <li><strong><?= date('M d', strtotime($row['payment_start_period'])); ?>
                                            - <?= date('M d', strtotime($row['payment_end_period'])); ?></strong></li>
                                    <li><?= date('M d, Y', strtotime($row['payment_due_date'])); ?></li>
                                    <li><strong>$<?= $row['payment_due_amount'] ?></strong></li>
                                        <!-- <?php if ($row['payment_start_period'] < date('Y-m-d')) { ?> -->
                                        <li><strong>$<?= $row['lease_rent_recieved'] ?></strong></li>
                                        <!-- <?php } else { ?> -->
                                        <li><strong>$<?= $row['lease_rent_recieved'] ?></strong></li>
                                        <!-- <?php } ?> -->
                                    <?php if ($row['lease_present_status'] == 1) { ?>
                                        <li><span id="lease_label" class="label label-paid">Paid</span></li>
                                    <?php } elseif ($row['lease_present_status'] == 2) { ?>
                                        <li><span class="label label-info">Upcoming</span></li>
                                    <?php } elseif ($row['lease_present_status'] == 3) { ?>
                                        <li><span class="label label-danger">Arrears</span></li>
                                    <?php }
                                } ?>
                                </ul>
                            <?php } ?>
                        </div>
                        <div class="text-center extar_p">
                            <a href="stepFive/<?= $property_id; ?>/<?= $lease_id; ?>/back" class="btn btn-light btn-md">Back</a>
                            <button id="activate-step-3" class="btn btn-primary btn-md" data-toggle="modal"
                                    data-target="#complete_wizard">Complete Setup
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Modal -->
<div class="modal fade" id="recive_rent" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span>
                </button>
                <h2 class="modal-title" id="myModalLabel">You've received some money</h2>
            </div>
            <form action="property/insert_payment_update" onsubmit="return chk_payment_update()" method="post">
                <div class="modal-body remove_wrap" id="cbcheck">
                    <ul class="table_header top-header-div">
                        <li>Payer Name</li>
                        <li>Amount Received</li>
                        <li>Payment Method</li>
                        <li>Date Paid</li>
                        <li>Send Receipt?</li>
                        <li></li>
                    </ul>
                    <?php foreach ($get_tenant_list as $row) { ?>
                        <ul class="table_row schedule_list">
                            <li class="payer_name"><label id="tenant_name"
                                                          for="t-payment"><?= $row['user_fname'] ?> <?= $row['user_lname'] ?></label>
                            </li>
                            <li class="how_much">
                                <h1>Amount Received</h1>
                                <input type="text" class="input-small p_amount form-control"
                                       id="p_amount<?= $row['lease_detail_id']; ?>" name="amount_recieved[]"
                                       value="<?php if ($row['payment_update_status'] == 2 || $row['payment_update_status'] == 4) {
                                           echo $row['payment_update_by'];
                                       } else {
                                           echo "0.00";
                                       } ?>">
                            </li>
                            <li class="payment_method">
                                <h1>Payment Method</h1>
                                <select class="input-small form-control p_method"
                                        id="p_method<?= $row['lease_detail_id']; ?>" name="payment_method[]">
                                    <option value="">Select Method</option>
                                    <option value="1">Bank Transfer</option>
                                    <option value="2">Cash</option>
                                    <option value="3">Cheque</option>
                                    <option value="4">Credit Card</option>
                                    <option value="5">Other method</option>
                                </select>
                            </li>
                            <li><h1>Date Paid</h1>
                                <div class="input-group">
                                    <input class="form-control date_hover" name="payment_date[]"
                                           value="<?= date('d/m/Y'); ?>" id="date_hover<?= $row['lease_detail_id']; ?>"
                                           type="text" readonly/>
                                    <span class="input-group-addon"><i class="glyphicon glyphicon-calendar"></i></span>
                                </div>
                            </li>
                            <li>
                                <h1>Send Receipt?</h1>
                                <input class="form-control p_checkbox"
                                       value="<?= $row['lease_detail_id']; ?>" <?php if ($row['payment_update_status'] == 2 || $row['payment_update_status'] == 4) {
                                    echo 'checked';
                                } ?> style="width:26%;float:left;margin-right:10%;margin-top:0%;" type="checkbox">
                                <input type="hidden" name="payment_sms_check[]" value="0">
                                <label class="send_receipt" id="send_receipt_label" for="send_receipt_5a7ef7fcdffaf">
                                </label>
                                <span class="left-text"><b>Send</b></span>
                            </li>
                            <li>
                                <button type="button" class="close remove_payment">×</button>
                            </li>
                        </ul>
                        <input type="hidden" name="lease_detail_id[]" value="<?= $row['lease_detail_id']; ?>">
                    <?php } ?>
                </div>
                <div class="modal-footer">
                    <input type="hidden" name="property_id" value="<?= $property_id; ?>">
                    <input type="hidden" name="lease_id" value="<?= $lease_id; ?>">
                    <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
                    <input type="submit" class="btn btn-primary  pull-right" value="Mark as paid">
                </div>
            </form>
        </div>
    </div>
</div>
<!-- Modal -->
<div class="modal fade" id="complete_wizard" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content text-center">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title" id="myModalLabel">Setup Complete</h4>
            </div>
            <div class="modal-body">
                <h4>You will now be taken to your personal dashboard and property control center.</h4>
                <p> From here you can add information in relation to maintenance, expenses, rent received, invoices and
                    more!</p>
                <br/><br/><br/>
                <a href="Dashboard/<?= $property_id; ?>" class="btn btn btn-primary btn-lg">Continue</a>
                <br/><br/><br/><br/><br/><br/>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="print_ledger" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">×</button>
        <div class="modal-title">
            <h4 class="print_ledger_title">Print and share with your tenants</h4>
        </div>
        <div class="form-chooser row-fluid">
            <div class="span6">
                <h2>
                    <span class="custom-radio" data-show-form="print-rent-form"> </span>
                    Rent Schedules
                </h2>
                <p>All rent payments, lease periods<br>and due dates for this lease</p>
            </div>
            <div class="span6">
                <h2>
                    <span class="custom-radio custom-radio-checked" data-show-form="print-ledger-form"> </span>
                    Tenant Ledgers
                </h2>
                <p>Historical rent payments paid<br>by individual tenant or all</p>
            </div>
        </div>
    </div>
    <div class="modal-body print-ledger-form">
        <div class="row-fluid print-rent-form">
            <div class="span12">
                <form id="print_rent_schedule_form"
                      action="?r=RentingSmartTenantLedger&amp;print_ledger=1&amp;property_id=NDA2MA" method="post"
                      name="print_rent_schedule_form">
                    <input id="print_rent_schedule" type="hidden" name="print_rent_schedule" value="1">
                    <div class="row-fluid group-row">
                        <div class="span6 title-column">
                            <p>View rent schedule for all<br> tenants or by individual tenant?</p>
                        </div>
                        <div class="span6 rent_schedule_tenants_name_section">
                            <div class="customize-radio clearfix radio-text">
                                <div>
                                    <input id="tenant_name_radio_0" type="radio" name="rent_schedule_tenant_id"
                                           class="Select_tenant_radio all_tenants" value="4937" checked="checked">
                                    <label for="tenant_name_radio_0">Show All Tenants</label>
                                </div>
                                <div id="rep_radio">
                                    <input id="tenant_name_radio_4937" type="radio" name="rent_schedule_tenant_id"
                                           class="Select_tenant_radio tenant_name_redio" value="4937">
                                    <label for="tenant_name_radio_4937" class="tenant_name_label">Aaaaaaaaaa
                                        Bbbbbbbbb</label>
                                </div>
                                <div style="display: none;" class="stop_radio"></div>
                            </div>
                            <div class="span6 schedule_tenant_name_error"
                                 style="display: none; width:100%;  margin: 0px 0px 0px -28px;">
                                <p class="help-block error">
                                    <i class="fa fa-warning"> </i> Please select one tenant
                                </p>
                            </div>
                        </div>
                    </div>
                    <div class="row-fluid group-row">
                        <div class="span6 title-column">
                            <p>Show payment status<br>(paid, arrears, upcoming)?</p>
                        </div>
                        <div class="span6 status_section customize-radio customize-radio-inline radio-text">
                            <div>
                                <input id="ledger_tenant_status_no" type="radio" name="payment_status"
                                       class="payment_status Select_tenant_radio" value="No" checked="checked">
                                <label for="ledger_tenant_status_no">No</label>
                            </div>
                            <div>
                                <input id="ledger_tenant_status_yes" type="radio" name="payment_status"
                                       class="payment_status Select_tenant_radio" value="Yes">
                                <label for="ledger_tenant_status_yes">Yes</label>
                            </div>
                        </div>
                    </div>
                    <div class="row-fluid">
                        <div class="span3 rent_schedule_error ledger_msgs" style="display: none;">
                            <p class="help-block error">
                                <i class="fa fa-warning"> </i> Something went wrong please try again later...
                            </p>
                        </div>
                    </div>
                    <div class="do-it">
                        <a href="javascript:void(0);" class="print_rent_schedule">Generate &amp; Print</a>
                    </div>
                </form>
            </div>
        </div>
        <div class="row-fluid print-ledger-form">
            <div class="span12">
                <form id="print_ledger_form"
                      action="?r=RentingSmartTenantLedger&amp;print_ledger=1&amp;property_id=NDA2MA" method="post"
                      name="print_ledger_form">
                    <input id="print_tenant_ledgers" type="hidden" name="print_tenant_ledgers" value="1">
                    <div class="print_date row-fluid group-row">
                        <p class="period-from">Select period:</p>
                        <div class="input-box">
                            <input id="ledger_start_date" type="text" name="ledger_start_date" placeholder="Start date"
                                   class="ledger_dates hasDatepicker" value="Feb 11, 2018">
                        </div>
                        <p class="period-to">to</p>
                        <div class="input-box">
                            <input id="ledger_end_date" type="text" name="ledger_end_date" placeholder="End date"
                                   class="ledger_dates hasDatepicker" value="Feb 11, 2018">
                        </div>
                    </div>
                    <div class="row-fluid group-row">
                        <div class="span6 title-column">
                            <p>Ledger for which tenants?</p>
                        </div>
                        <div class="span6 rent_schedule_tenants_name_section">
                            <div class="customize-radio clearfix tenants_name_section">
                                <div id="show_all">
                                    <input id="ledger_tenant_name_radio_0" type="radio" name="ledger_tenant_id"
                                           class="Select_tenant_radio all_tenants_ids" value="4937"
                                           style="display: none;">
                                    <label for="ledger_tenant_name_radio_0">Show All Tenants</label>
                                </div>
                                <div class="rep_tenants_radio">
                                    <input id="ledger_tenant_name_radio_4937" type="radio" name="ledger_tenant_id"
                                           class="Select_tenant_radio ledger_tenant_name_radio" value="4937">
                                    <label for="ledger_tenant_name_radio_4937" class="ledger_tenant_name_label">Aaaaaaaaaa
                                        Bbbbbbbbb</label>
                                </div>
                                <div style="display: none;" class="stop_tenants_radio"></div>
                            </div>
                            <div class="span3 tenant_name_error"
                                 style="width: 100%; margin: 0px 0px -30px -25px; display: none;">
                                <p class="help-block error">
                                    <i class="fa fa-warning"> </i> Please select one tenant
                                </p>
                            </div>
                        </div>
                    </div>
                    <div class="span6 ledger_error_msg ledger_msgs" style="margin: 0px 0px 10px 70px; display: none;">
                        <p class="help-block error">
                            <i class="fa fa-warning"> </i> Something went wrong please try again later...
                        </p>
                    </div>
                    <div class="do-it">
                        <a href="javascript:void(0);" class="print_ledger">Generate &amp; Print</a>
                    </div>
                </form>
            </div>
        </div> <!-- print-ledger-form -->
    </div>
    <div class="modal-footer">
        <img src="https://app.rentingsmart.com//RentingSmartHtml/schedule_wizard/../common/css/img/wizard_complete_bg_transparent.png"
             alt="Landlord of the year">
    </div>
</div>
<div class="modal fade" id="printshare" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span>
                </button>
                <h4 class="modal-title" id="myModalLabel">Print</h4>
            </div>
            <div class="modal-body">
                <br/><br/>
                <!-- Nav tabs -->
                <ul class="nav nav-tabs" role="tablist">
                    <li role="presentation" class="active"><a href="#psrentschedules" aria-controls="home" role="tab"
                                                              data-toggle="tab">
                            <h2>Rent Schedules</h2>
                            <p>All rent payments, lease periods and due dates for this lease</p>
                        </a></li>
                    <li role="presentation"><a href="#pstenantledgers" aria-controls="profile" role="tab"
                                               data-toggle="tab">
                            <h2>Tenant Ledgers</h2>
                            <p>Complete ledger of rent payments received by tenant</p>
                        </a></li>
                </ul>
                <!-- Tab panes -->
                <div class="tab-content">
                    <div role="tabpanel" class="tab-pane active" id="psrentschedules">
                        <form target="_blank" action="property/rent_schedule_pdf" method="post">
                            <div class="form-group">
                                <label class="col-md-5 text-right" for="exampleInputName2 ">View rent schedule for all
                                    tenants or by individual tenant?</label>
                                <div class="checkbox col-md-7">
                                    <label class="radio-inline">
                                        <input type="radio" name="tenant_rent_schedule" checked id="inlineRadio1"
                                               value="0"> Show All Tenants
                                    </label>
                                    <?php foreach ($get_tenant_list as $key => $row) { ?>
                                        <br/>
                                        <label class="radio-inline">
                                            <input type="radio" name="tenant_rent_schedule" id="inlineRadio2"
                                                   value="<?= $row['tenant_id'] ?>"><?= $row['user_fname'] ?> <?= $row['user_lname'] ?>
                                        </label>
                                    <?php } ?>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-5 text-right" for="exampleInputName2">Show payment status (paid,
                                    arrears, upcoming)?</label>
                                <div class="checkbox col-md-7">
                                    <label class="radio-inline">
                                        <input type="radio" name="rent_payment_status" checked id="inlineRadio1"
                                               value="0"> No
                                    </label>
                                    <br/>
                                    <label class="radio-inline">
                                        <input type="radio" name="rent_payment_status" id="inlineRadio2" value="1"> Yes
                                    </label>
                                </div>
                            </div>
                            <div class="text-center">
                                <br/><br/>
                                <input type="hidden" name="property_id" value="<?= $property_id; ?>">
                                <input type="submit" class=" btn btn-primary btn-lg" value="Generate & Print">
                                <br/>
                                <br/>
                            </div>
                        </form>
                    </div>
                    <div role="tabpanel" class="tab-pane" id="pstenantledgers">
                        <form action="property/tenant_ledgers_pdf" method="post" target="_blank">
                            <div class="form-group">
                                <label class="col-md-3 text-right" for="exampleInputName2 ">Select period:</label>
                                <div class="input-group date pull-left col-md-4">
                                    <input class="form-control tenant_ledger_start_date" name="tenant_ledger_start_date"
                                           type="text" readonly required/>
                                    <span class="input-group-addon"><i class="glyphicon glyphicon-calendar"></i></span>
                                </div>
                                <div class=" col-md-1 text-center" style="line-height:35px;">To</div>
                                <div class="input-group col-md-4">
                                    <input class="form-control tenant_ledger_end_date" name="tenant_ledger_end_date"
                                           type="text" readonly required/>
                                    <span class="input-group-addon"><i class="glyphicon glyphicon-calendar"></i></span>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-5 text-right" for="exampleInputName2 ">Ledger for which
                                    tenants?</label>
                                <div class="checkbox col-md-7">
                                    <label class="radio-inline">
                                        <input type="radio" checked name="tenant_ledger" id="inlineRadio1" value="0">
                                        Show All Tenants
                                    </label>
                                    <?php foreach ($get_tenant_list as $key => $row) { ?>
                                        <br/>
                                        <label class="radio-inline">
                                            <input type="radio" name="tenant_ledger" id="inlineRadio2"
                                                   value="<?= $row['tenant_id'] ?>"><?= $row['user_fname'] ?> <?= $row['user_lname'] ?>
                                        </label>
                                    <?php } ?>
                                </div>
                            </div>
                            <div class="text-center">
                                <br/><br/>
                                <input type="hidden" name="property_id" value="<?= $property_id; ?>">
                                <input type="submit" class=" btn btn-primary btn-lg" value="Generate & Print">
                                <br/>
                                <br/>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<style>
    .ui-datepicker {
        position: relative;
        z-index: 10000 !important;
    }
</style>

<?= $this->load->view('front/footerlink'); ?>
<script type="text/javascript">
    $(function () {
        $('.tenant_ledger_start_date, .tenant_ledger_end_date, .due_date').datepicker({
            required: true,
            dateFormat: 'd M, yy'
        });
    });
</script>
<script type="text/javascript">
    $("#cbcheck").on("change", ".p_checkbox", function () {
        if ($(this).is(":checked")) {
            $(this).next('input').val("1");
        }
        else {
            $(this).next('input').val("0");
        }
    });
    $(document).ready(function () {
        $('.remove_wrap').on("click", ".remove_payment", function (e) {
            e.preventDefault();
            $(this).closest('ul').remove();
        });
        $('.date_hover').each(function () {
            var date_hover = $(this);
            date_hover.datepicker();
        });
        $('.p_checkbox').each(function () {
            if ($(this).is(":checked")) {
                $(this).next('input').val("1");
            }
            else {
                $(this).next('input').val("0");
            }
        });
    });

    function chk_payment_update() {
        var ok = true;
        $('.p_method').each(function () {
            var p_method = $(this);
            var p_method_closest_li = p_method.closest('li');
            if (p_method.val() == '') {
                p_method_closest_li.addClass("has-error");
                ok = false;
            }
        });
        $('.p_amount').each(function () {
            var p_amount = $(this);
            // console.log(p_amount);
            var p_amount_closest_li = p_amount.closest('li');
            if (p_amount.val() == '') {
                p_amount_closest_li.addClass("has-error");
                ok = false;
            }
        });
        return ok;
    }
</script>
</body>
</html>