<?php $this->load->view('front/headlink'); ?>
    <body>

<style>
    .black {
        color: black;
    !important;
    }

    .black:hover {
        color: crimson;
    !important;
    }
</style>
<div class="setup_multistape">
    <div class="container">
        <?php $this->load->view('front/head_nav'); ?>
        <div><img src="assets/img/shadow_product_top.png" class="img-responsive"></div>
        <div class="ss_dashboart">
            <?php foreach ($all_property as $property) { ?>
                <div class="ss_accordent">
                    <?php if ($property['all_property_active_lease']) { ?>
                        <?php foreach ($property['all_property_active_lease'] as $a_property_active_lease) { ?>
                            <?php if ($a_property_active_lease['step_count'] == 7) { ?>
                                <div class="header_top name_anchor"
                                     anchor-attr="property/property_details/<?php echo $a_property_active_lease['property_id']; ?>">
                                    <?php echo $a_property_active_lease['property_address']; ?>
                                </div>
                                <div class="accordent_content">
                                    <a href="property/property_details/<?php echo $a_property_active_lease['property_id']; ?>">
                                        <p class="black">Rent Payments

                                            <?php
                                            $rent_status = $this->utility_model->calculate_rent_status_by_lease($a_property_active_lease['lease_id']);
                                            ?>

                                            <?php if (!empty($rent_status)) { ?>
                                                <span id="rent_status" class=" label label-<?= $rent_status['label'] ?>">

                                                <?= $rent_status['amount'] != 0 ? "$" . $rent_status['amount'] : "" ?>
                                                    <span> - <?= $rent_status['status'] ?></span>

                                            </span>
                                            <?php } ?>


                                        </p>
                                        <p class="black">
                                            Current Lease
                                            Ends: <?= date("F, j, Y", strtotime($a_property_active_lease['lease_end_date'])) ?>
                                        </p>
                                    </a>
                                </div>

                            <?php } else { ?>
                                <?php $flow = '';
                                if ($a_property_active_lease['step_count'] == '5') {
                                    $flow = '/front';
                                } ?>

                                <div class="header_top name_anchor"
                                     anchor-attr="steps/<?php echo $a_property_active_lease['property_id']; ?>/<?php echo $a_property_active_lease['step_count'] . $flow; ?>">
                                    <?php echo $a_property_active_lease['property_address']; ?>
                                </div>
                                <div class="accordent_content" style="text-decoration: underline;">
                                    <a href="steps/<?php echo $a_property_active_lease['property_id']; ?>/<?php echo $a_property_active_lease['step_count'] . $flow; ?>">
                                        <p class="black">Incomplete Setup.</p>
                                        <p class="black"> Continue setup this company</p>
                                    </a>
                                </div>

                            <?php } ?>
                        <?php } ?>
                    <?php } else { ?>
                        <!-- if no active lease-->

                        <?php if ($property['step_count'] < 7) { ?>

                            <?php $flow = '';
                            if ($property['step_count'] == 5) {
                                $flow = '/front';
                            } ?>

                            <div class="header_top name_anchor"
                                 anchor-attr="steps/<?php echo $property['property_id']; ?>/<?php echo $property['step_count'] . $flow; ?>">
                                <?php echo $property['property_address']; ?>
                            </div>
                            <div class="accordent_content" style="text-decoration: underline;">
                                <a href="steps/<?php echo $property['property_id']; ?>/<?php echo $property['step_count'] . $flow; ?>">
                                    <p class="black">Incomplete Setup.</p>
                                    <p class="black"> Continue setup this company</p>
                                </a>
                            </div>

                        <?php } else { ?>
                            <div class="header_top name_anchor"
                                 anchor-attr="ArchivedLeases/<?php echo $property['property_id']; ?>">
                                <?php echo $property['property_address']; ?>
                            </div>
                            <div class="accordent_content">
                                <a href="ArchivedLeases/<?php echo $property['property_id']; ?>">
                                    <p class="black">No active Lease</p>
                                </a>

                                <a href="stepThree/<?php echo $property['property_id']; ?>?add_another_lease=yes">
                                    <p class="black">Click here add another lease</p>
                                </a>
                            </div>
                        <?php } ?>

                    <?php } ?>
                    <div><img src="assets/img/shadow_sm.png" class="img-responsive"></div>
                </div>

            <?php } ?>
            <?php if (count($all_property) > 0) { ?>
                <div style="clear: both; text-align: center; padding-top: 30px">
                    <a href="stepTwo">
                        <button class="btn btn-primary">click to add another Rent Simple Property</button>

                    </a>
                </div>
            <?php } else { ?>
                <div style="clear: both; text-align: center; padding-top: 30px">
                    <a href="stepTwo">
                        <button class="btn btn-primary">Click Here to add your first Rent Simple Property</button>

                    </a>
                </div>
            <?php } ?>
        </div>
    </div>
</div>

<style>
    .subscribe-btn {
        position: fixed;
        left: 0;
        bottom: 50px;
        background-color: #00aced;
        color: white;
        padding: 10px 25px;
        font-size: 18px;
        z-index: 100000;
    }
</style>

<!--<a class="subscribe-btn" href="subscribe">Subscribe</a>-->

<script>
    $(function () {

        var name_anchor = $(".name_anchor");

        name_anchor.css("cursor", "pointer");

        name_anchor.on("click", function () {
            var route = $(this).attr("anchor-attr");

            $(this).css("crosshair", "pointer");
            window.location.href = route;
        })

    });
</script>

<?php $this->load->view('front/footerlink'); ?>