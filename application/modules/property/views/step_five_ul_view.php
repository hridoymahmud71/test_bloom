<?php
$i = $ul_starting_point;
foreach ($lease_payment_schedule as $leasePaymentSchedule) {

    $timeDue = strtotime($leasePaymentSchedule['payment_due_date']);
    $payment_due_date = date("d M Y", $timeDue);

    $timeLeaseStart = strtotime($leasePaymentSchedule['payment_start_period']);
    $payment_start_period = date("d M Y", $timeLeaseStart);

    $timeLeaseEnd = strtotime($leasePaymentSchedule['payment_end_period']);
    $payment_end_period = date("d M Y", $timeLeaseEnd);

    ?>

    <tr id="schedule_row_<?= $i ?>" ul-count-attr="<?= $i ?>" class="table_row schedule_list schedule_row">

        <td id="num"><?php echo $i; ?></td>
        <td>
            <span class="input_text input_single_date_span" id="due_date_<?= $i ?>"
                  style="display: inline;"><?php echo $payment_due_date; ?></span>
            <div style="display: none"
                 class="input-group input-small date input_single_date_div">
                <input name="input_payment_due_date[]" id="input_payment_due_date_<?= $i ?>"
                       value="<?php echo $payment_due_date; ?>"
                       class="form-control single_datepicker" type="text" readonly/>
                <span class="input-group-addon"><i class="glyphicon glyphicon-calendar"></i></span>
            </div>
        </td>
        <td>
            <span class="input_text input_range_date_span" id="lease_payment_period_<?= $i ?>"
                  style="display: inline;"><?php echo $payment_start_period . ' - ' . $payment_end_period; ?></span>
            <div style="display: none"
                 class="input-group input-small date input_range_date_div">
                <!--<input class="form-control " value="" name="lease_start_date" id="StartLeaseDate" readonly="" type="text">-->
                <input name="input_lease_payment_period[]" style=""
                       id="input_lease_payment_period_<?= $i ?>"
                       value="<?php echo $payment_start_period . ' - ' . $payment_end_period; ?>"
                       class="date_show form-control range_datepicker" type="text" readonly/>
                <span class="input-group-addon"><i class="glyphicon glyphicon-calendar"></i></span>
            </div>
        </td>
        <td class="s5_amount_due">
            <span class="input_text span_amount" id="span_amount_<?= $i ?>"
                  style="display: inline;"><?php echo $leasePaymentSchedule['payment_due_amount']; ?></span>
            <input name="input_amount[]" id="input_amount_<?= $i ?>" required
                   class="input-small dollar_blue pay_period_amount input_amount" type="text"
                   step="0.01" name="amount_due[]"
                   value="<?php echo $leasePaymentSchedule['payment_due_amount']; ?>"
                   style="display: none">
        </td>
        <td class="edit_or_save" style="display: none;">
                                    <span id="edit_lease_schedule_<?= $i ?>" class="edit_lease_schedule"
                                          style="display: inline;"><a href="javascript:void(0);">edit</a></span>
            <span id="save_lease_schedule_<?= $i ?>" class="save_lease_schedule" style="display: none;">
                  <a href="javascript:void(0);" id="save_anchor_<?= $i ?>" class="save_anchor save_edits">save</a>
                  <a href="javascript:void(0);" id="cancel_anchor_<?= $i ?>" class="cancel_anchor cencel_edit"
                     style="position:absolute; margin-left: 5px;">| cancel</a>
            </span>
        </td>
    </tr>

    <?php $i++;
} ?>