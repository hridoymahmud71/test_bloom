<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Admin extends MX_Controller 
{

    function __construct()
	{
		date_default_timezone_set('Asia/Dhaka');

        $this->load->model('admin_model');

        $this->load->library('sendsms_library');

		if($this->session->userdata('language_select')=='bangla')
        {
            $this->lang->load('admin', 'bangla');
        }
        else
        {
            $this->lang->load('admin', 'english');
        }

        $login_id=   $this->session->userdata('login_id');
        $reg_id =    $this->session->userdata('reg_id');
        $name   =    $this->session->userdata('name');
        $type   =    $this->session->userdata('type');
        $district_id=$this->session->userdata('state_id');

        if($login_id=='' || $type==2 || $type==3)
        {
            $this->session->unset_userdata('login_id');
            $this->session->unset_userdata('reg_id');
            $this->session->unset_userdata('name');
            $this->session->unset_userdata('state_id');
            $this->session->unset_userdata('type');
            $this->session->set_userdata('log_err','Enter Email and Password First');
            redirect('login','refresh');
        }
    }
	public function index()
	{
        $login_id = $this->session->userdata('login_id');
        $data['active']='Dashboard';
        $data['page_title']=$this->lang->line('dashboard_label');

       
        $data['breadcrumbs']= '<li><span class="active">'.$this->lang->line('dashboard_label').'</span></li>';

        // $data['total_market']=$this->admin_model->count_with_where('*', 'status!=3', 'market');
        // $data['total_shop']=$this->admin_model->count_with_where('*', 'status!=3', 'shop');
        // $data['total_user']=$this->admin_model->count_with_where('*', 'status=1 AND (type=2 OR type=3)', 'login');
        // $data['total_agent']=$this->admin_model->count_with_where('*', 'status=1 AND type=1', 'login');

        $data['total_market']=0;
        $data['total_shop']=0;
        $data['total_user']=0;
        $data['total_agent']=0;

        $data['month']=array(
            0=>'Jan',
            1=>'Feb',
            2=>'Mar',
            3=>'Apr',
            4=>'May',
            5=>'Jun',
            6=>'Jul',
            7=>'Aug',
            8=>'Sep',
            9=>'Oct',
            10=>'Nov',
            11=>'Dec',
        );
        // echo '<pre>';print_r($data['month']);die();

        $data['head_data']=$this->head_data();
        $this->load->view('index',$data);
	}



//////////////////********  Doctor Feature Start *******///////////////////////////

    //////////////////********  Doctor Start *******///////////////////////////

        public function doctor()
        {
            $data['active']='doctor';
            $data['page_title']=$this->lang->line('doctor_list_label');
            $data['add_btn']=$this->lang->line('btn_add_label').' '.$this->lang->line('doctor_label');

            $data['add_btn_link']='add_doctor';

            $data['breadcrumbs']= '<li><span class="active">'.$this->lang->line('doctor_list_label').'</span></li>';

            // registration table and login table theke ante hobe

            $data['user_list']=$this->admin_model->select_where_join('*','registration','login','login.id=registration.login_id','login.verify_status=1 AND (login.user_type=3 OR login.user_type=4)');
            $data['user_type']=$this->admin_model->select_all('user_type');
            $data['head_data']=$this->head_data();
            
            $this->load->view('user/user_list',$data);
        }

        public function add_doctor()
        {
            $data['active']='doctor';
            $data['page_title']=$this->lang->line('doctor_label').' '.$this->lang->line('btn_add_label');
     

            
            $data['breadcrumbs']= '<li><a href="admin/doctor">'.$this->lang->line('doctor_list_label').'</a><i class="fa fa-circle"></i></li>';
            $data['breadcrumbs'].= '<li><span class="active">'.$data['page_title'].'</span></li>';

            $data['form_icon']='fa fa-user-md';
            $data['form_label']=$this->lang->line('doctor_label').' '.$this->lang->line('btn_add_label').' '.$this->lang->line('form_label');

            $data['head_data']=$this->head_data();
            $this->session->set_userdata('tmp_user_type', 3);
            $data['user_type']=$this->admin_model->select_all('user_type');

            $this->load->view('user/add_user',$data);
       
        }


        public function add_doctor_post()
        {
            $data['name']=$this->input->post('name');
            $data['cat_id']=$this->input->post('cat_id');
            $data['sub_cat_id']=$this->input->post('sub_cat_id');
            $top_ad=$this->input->post('top_ad');
            $feature_ad=$this->input->post('feature_ad');

            $data['top_ad']=0;
            if($top_ad=='on')
            {
                $data['top_ad']=1;
            }

            $data['feature_ad']=0;
            if($feature_ad=='on')
            {
                $data['feature_ad']=1;
            }
            $data['price']=$this->input->post('price');
            $data['discount']=$this->input->post('discount');
            $data['description']=$this->input->post('description');
            $data['division_id']=$this->input->post('division_id');
            $data['district_id']=$this->input->post('district_id');
            $data['area_id']=$this->input->post('area_id');
            $data['status']=0;
            $data['created_at'] =date('Y-m-d H:i:s');
            $p_id = $this->admin_model->insert_ret('product', $data);

            $files = $_FILES;
            $cpt = count($_FILES['userfile']['name']);

            for($i=0; $i<$cpt; $i++)
            {           
                $_FILES['userfile']['name']= uniqid().'_'.underscore($files['userfile']['name'][$i]);
                $_FILES['userfile']['type']= $files['userfile']['type'][$i];
                $_FILES['userfile']['tmp_name']= $files['userfile']['tmp_name'][$i];
                $_FILES['userfile']['error']= $files['userfile']['error'][$i];
                $_FILES['userfile']['size']= $files['userfile']['size'][$i]; 

                $oldFileName = $_FILES['userfile']['name'];
                $_FILES['userfile']['name'] =str_replace("'","", $oldFileName);
                $this->upload->initialize($this->set_upload_options($_FILES['userfile']['name'],'product/'));
                $this->upload->do_upload();  

                if($i==0)
                {
                    $this->resize(200,200,'uploads/product/'.$_FILES['userfile']['name'],'uploads/product/thumbnail');
                    $main_image['main_image']=$_FILES['userfile']['name'];

                    $this->admin_model->update_function('id', $p_id, 'product', $main_image);

                }

                $imageInformation = getimagesize($_FILES['userfile']['tmp_name']);
                
                $imageWidth = $imageInformation[0]; //Contains the Width of the Image

                $imageHeight = $imageInformation[1];

                $this->watermark_image($imageWidth,$imageHeight);
                $watermark_path=$_FILES['userfile']['name'];

                $this->overlay($watermark_path);

                $data_image['p_id']=$p_id;
                $data_image['image']=$_FILES['userfile']['name'];
                $data_image['created_at'] =date('Y-m-d H:i:s');

                $this->admin_model->insert('product_image',$data_image);



            }

            $this->redirect('admin/product','refresh');
      
        }


        public function edit_doctor($id)
        {
            $data['active']='product';
            $data['page_title']=$this->lang->line('product_label').' '.$this->lang->line('btn_edit_label');
     

            
            $data['breadcrumbs']= '<li><a href="admin/product">'.$this->lang->line('product_list_label').'</a><i class="fa fa-circle"></i></li>';
            $data['breadcrumbs'].= '<li><span class="active">'.$data['page_title'].'</span></li>';

            $data['head_data']=$this->head_data();
            $data['division_list']=$this->admin_model->select_all('divisions');
            $data['district_list']=$this->admin_model->select_all('districts');
            $data['area_list']=$this->admin_model->select_all('area');
            $data['category_list']=$this->admin_model->select_all_name_ascending('name','category');
            $data['sub_category_list']=$this->admin_model->select_all_name_ascending('sub_name','sub_category');

            $data['product_details']=$this->admin_model->select_with_where('*','id='.$id,'product');

            $this->load->view('product/edit_product',$data);
        }




        public function update_doctor_post($id) 
        {
            $data['division_id'] = $this->input->post('district_id');
            $data['name'] = $this->input->post('dist_name');
            $data['bn_name'] = $this->input->post('dist_bn_name');
            
            $this->admin_model->update_function('id', $id, 'districts', $data);
            $this->session->set_userdata('scc_alt', $this->lang->line('update_scc_msg'));
            redirect('admin/district','refresh');
        }

        public function delete_doctor($id)
        {
      
            $this->admin_model->delete_function('districts', 'id', $id);
            $this->session->set_userdata('scc_alt', $this->lang->line('delete_scc_msg'));
         
            redirect('admin/district','refresh');
        }

        public function package()
        {
            $data['active']='package';
            $data['page_title']=$this->lang->line('package_list_label');
            
            $data['breadcrumbs']= '<li><span class="active">'.$this->lang->line('package_list_label').'</span></li>';

            //$data['doctor_list']=$this->admin_model->select_join('*,product.id as p_id,product.name as p_name,product.created_at as created_date','product','category','category.id=product.cat_id');
            $data['package_list']=array();
            $data['head_data']=$this->head_data();
            
            $this->load->view('doctor/package_list',$data);
        }


        public function appointment()
        {
            $data['active']='appointment';
            $data['page_title']=$this->lang->line('appointment_list_label');
            
            $data['breadcrumbs']= '<li><span class="active">'.$this->lang->line('appointment_list_label').'</span></li>';

            //$data['doctor_list']=$this->admin_model->select_join('*,product.id as p_id,product.name as p_name,product.created_at as created_date','product','category','category.id=product.cat_id');
            $data['appointmente_list']=array();
            $data['head_data']=$this->head_data();
            
            $this->load->view('doctor/appointment_list',$data);
        }

        public function specialization()
        {
            $data['active']='specialization';
            $data['page_title']=$this->lang->line('specialization_list_label');
            $data['add_btn']=$this->lang->line('btn_add_label').' '.$this->lang->line('specialization_label');
            $data['head_data']=$this->head_data();

            $data['breadcrumbs']= '<li><span class="active">'.$this->lang->line('specialization_label').'</span></li>';
            $data['specialization_list']=$this->admin_model->select_all_name_ascending('    sp_name','specialization');

            $this->load->view('user/specialization_list',$data);
        }
        

        public function add_specialization()
        {
            $data['sp_name']=$this->input->post('name');
            $this->admin_model->insert('specialization',$data);
            redirect('admin/specialization','refresh');
        }

        public function update_specialization_post()
        {
            $id=$this->input->post('id');
            $data['sp_name']=$this->input->post('name');
            $this->session->set_userdata('scc_alt', $this->lang->line('update_scc_msg'));
            $this->admin_model->update_function('id', $id, 'specialization', $data);
            redirect('admin/specialization','refresh');
        }

        public function delete_specialization($id='')
        {
            $this->admin_model->delete_function('specialization', 'id', $id);
            $this->session->set_userdata('scc_alt', $this->lang->line('delete_scc_msg'));
            redirect('admin/specialization','refresh');
        }




        public function degree()
        {
            $data['active']='degree';
            $data['page_title']=$this->lang->line('degree_list_label');
            $data['add_btn']=$this->lang->line('btn_add_label').' '.$this->lang->line('degree_label');
            $data['head_data']=$this->head_data();

            $data['breadcrumbs']= '<li><span class="active">'.$this->lang->line('degree_label').'</span></li>';
            $data['degree_list']=$this->admin_model->select_all_name_ascending('deg_name','doctor_degree');

            $this->load->view('user/degree_list',$data);
        }



        public function add_degree()
        {
            $data['deg_name']=$this->input->post('name');
            $this->admin_model->insert('doctor_degree',$data);
            redirect('admin/degree','refresh');
        }

        public function update_degree_post()
        {
            $id=$this->input->post('id');
            $data['deg_name']=$this->input->post('name');
            $this->session->set_userdata('scc_alt', $this->lang->line('update_scc_msg'));
            $this->admin_model->update_function('id', $id, 'doctor_degree', $data);
            redirect('admin/degree','refresh');
        }

        public function delete_degree($id='')
        {
            $this->admin_model->delete_function('doctor_degree', 'id', $id);
            $this->session->set_userdata('scc_alt', $this->lang->line('delete_scc_msg'));
            redirect('admin/degree','refresh');
        }



        public function doctor_chamber($doctor_name,$doctor_id='')
        {
            $data['active']='user_list';
            $data['page_title']=$this->lang->line('doctor_label').' '.$this->lang->line('chamber_label');
            $data['add_btn']=$this->lang->line('btn_add_label').' '.$this->lang->line('user_label');
            $data['doctor_id']=$doctor_id;

            $data['breadcrumbs']= '<li><span><a href="admin/user_profile/'.$doctor_name.'#dr_profile">'.$doctor_name.'</a></span><i class="fa fa-circle"></i></li>';
            $data['breadcrumbs'].= '<li><span class="active">'.$data['page_title'].'</span></li>';

            $data['doctor_name']=$doctor_name;
            $data['head_data']=$this->head_data();
            $data['user_type']=$this->admin_model->select_all('user_type');

            $data['division_list']=$this->admin_model->select_all_name_ascending('name','divisions');

            $data['chamber_list']=$this->admin_model->get_userlist_with_address('login.user_type=9 AND verify_status=1');
            
            //echo "<pre>";print_r($data['chamber_list']);die();
            $this->load->view('user/doctor_chamber',$data);
        }

        public function add_chamber_name()
        {

            $data_vf['name']=ucwords($this->input->post('chamber_name'));
            $exp_name=explode(' ', $data_vf['name']);
            
            $data_vf['country_code']='880';
            $data_vf['password']=$this->encryptIt('j12345');
            $data_vf['user_type']=9;
            $data_vf['verify_status']=1;
            $data_vf['created_at']=date('Y-m-d H:i:s');

            $verf_id=$this->admin_model->insert_ret('verify_user',$data_vf);
            $data_login['email']=$exp_name[0].'_medical'.$verf_id.'@gmail.com';
            $data_login['mobile_no']=sprintf("%010d", mt_rand(1, 9999999).''.$verf_id);

            $data_login['username'] = preg_replace('/[ ,]+/', '_', trim($data_vf['name']));
            $data_login['username']=$data_login['username'].'_'.$verf_id;
            $data_login['username'] = str_replace("'", '', $data_login['username']);

            $this->admin_model->update_function('id', $verf_id, 'verify_user', $data_login);

            
            $data_login['country_code']='880';
            $data_login['password']=$this->encryptIt('j12345');
            $data_login['user_type']=9;
            $data_login['verify_status']=1;
            $data_login['created_at']=date('Y-m-d H:i:s');

            $data_reg['login_id']=$this->admin_model->insert_ret('login',$data_login);

            $data_reg['name']=$data_vf['name'];
            $data_reg['division_id']=$this->input->post('division_id');
            $data_reg['district_id']=$this->input->post('district_id');
            $data_reg['area_id']=$this->input->post('area_id');
            $data_reg['created_at']=date('Y-m-d H:i:s');

            $this->admin_model->insert_ret('registration',$data_reg);


            $chamber_list=$this->admin_model->get_userlist_with_address('login.user_type=9 AND verify_status=1');
            
            echo '<option></option>';

            foreach ($chamber_list as $key => $row) { 
                echo '<option value="'.$row['reg_id'].'">'.$row['name'].'&nbsp;&nbsp;'.$row['area_title'].','.$row['district_name'].','.$row['division_name'].'</h5>
                </option>';
            }



        }
        public function add_dr_chamber_post($doctor_name,$doctor_id)
        {
            $data['doctor_id']=$doctor_id;
            $data['is_personal']=$this->input->post('is_personal');

            $data['day_list']='';
            $data['consult_fee']=$this->input->post('consult_fee');
            $data['chamber_id']=$this->input->post('chamber_id');
            $day_list=$this->input->post('day_list');
            $count_day=count($day_list);
            //echo "<pre>"; print_r($day_list);die();
            foreach ($day_list as $key => $value) 
            {
                if($count_day==($key+1))
                {
                    $data['day_list'].=$value;
                }
                else
                {
                    $data['day_list'].=$value.',';
                }
                
            }

            
            $data['start_time']=$this->input->post('start_time');
            $data['end_time']=$this->input->post('end_time');
            $data['start_time']=date('H:i:s', strtotime($data['start_time']));
            $data['end_time']=date('H:i:s', strtotime($data['end_time']));
            //echo "<pre>"; print_r($data); die();
            $data['phone_one']=$this->input->post('phone_one');
            $data['phone_two']=$this->input->post('phone_two');
            $data['created_at']=date('Y-m-d H:i:s');

            $chamber_id=$this->admin_model->insert_ret('doctor_chamber',$data);


            if($_FILES)
            {
                $files = $_FILES;
                $cpt = count($_FILES['userfile']['name']);

                for($i=0; $i<$cpt; $i++)
                {
                    if($_FILES['userfile']['name']!='')
                    {           
                        $_FILES['userfile']['name']= uniqid().'_'.underscore($files['userfile']['name'][$i]);
                        $_FILES['userfile']['type']= $files['userfile']['type'][$i];
                        $_FILES['userfile']['tmp_name']= $files['userfile']['tmp_name'][$i];
                        $_FILES['userfile']['error']= $files['userfile']['error'][$i];
                        $_FILES['userfile']['size']= $files['userfile']['size'][$i]; 

                        $oldFileName = $_FILES['userfile']['name'];
                        $_FILES['userfile']['name'] =str_replace("'","", $oldFileName);
                        $this->upload->initialize($this->set_upload_options($_FILES['userfile']['name'],'chamber_gallery/'));
                        $this->upload->do_upload();  

                        
                            $data['chamber_details']=$this->admin_model->select_with_where('*', 'id='.$chamber_id, 'doctor_chamber');

                            if($data['chamber_details'][0]['gallery_image']=='')
                            {
                                $data_image['gallery_image']=$_FILES['userfile']['name'];
                            }
                            else
                            {
                                $data_image['gallery_image']=$data['chamber_details'][0]['gallery_image'].','.$_FILES['userfile']['name'];
                            }
                            $this->admin_model->update_function('id', $chamber_id, 'doctor_chamber', $data_image);
                    }
                }
            }


            $this->session->set_userdata('scc_alt', $this->lang->line('insert_scc_msg'));
            redirect('admin/user_profile/'.$doctor_name.'#dr_profile','refresh');

        }

        public function chamber_details($doctor_name,$chamber_id='')
        {
            $data['active']='user_list';
            $data['page_title']=$this->lang->line('btn_edit_label').' '.$this->lang->line('doctor_label').' '.$doctor_name.' '.$this->lang->line('chamber_label');
            $data['add_btn']=$this->lang->line('btn_add_label').' '.$this->lang->line('user_label');
            
            $data['breadcrumbs']= '<li><span><a href="admin/user_profile/'.$doctor_name.'#dr_profile">'.$doctor_name.'</a></span><i class="fa fa-circle"></i></li>';
            $data['breadcrumbs'].= '<li><span class="active">'.$data['page_title'].'</span></li>';
            $data['doctor_name']=$doctor_name;
            $data['head_data']=$this->head_data();
            $data['user_type']=$this->admin_model->select_all('user_type');

            $data['chamber_list']=$this->admin_model->get_userlist_with_address('login.user_type=9 AND verify_status=1');
            $data['doctor_chamber_details']=$this->admin_model->select_with_where('*','id='.$chamber_id,'doctor_chamber');
            $this->load->view('user/include/edit_chamber', $data);
        }

        public function update_doctor_chamber_post($chamber_id='')
        {
            $doctor_name=$this->input->post('doctor_name');
            $data['is_personal']=$this->input->post('is_personal'); 
            $data['consult_fee']=$this->input->post('consult_fee');

            $data['day_list']='';
            $data['chamber_id']=$this->input->post('chamber_id');
            $day_list=$this->input->post('day_list');
            $count_day=count($day_list);
            //echo "<pre>"; print_r($day_list);die();
            foreach ($day_list as $key => $value) 
            {
                if($count_day==($key+1))
                {
                    $data['day_list'].=$value;
                }
                else
                {
                    $data['day_list'].=$value.',';
                }
                
            }

            
            $data['start_time']=$this->input->post('start_time');
            $data['end_time']=$this->input->post('end_time');
            $data['start_time']=date('H:i:s', strtotime($data['start_time']));
            $data['end_time']=date('H:i:s', strtotime($data['end_time']));
            $data['phone_one']=$this->input->post('phone_one');
            $data['phone_two']=$this->input->post('phone_two');


            $this->admin_model->update_function('id', $chamber_id, 'doctor_chamber', $data);

            if(!empty($_FILES))
            { 
                $files = $_FILES;
                $cpt = count($_FILES['userfile']['name']);

                for($i=0; $i<$cpt; $i++)
                {
                    if($_FILES['userfile']['name']!='')
                    {           
                        $_FILES['userfile']['name']= uniqid().'_'.underscore($files['userfile']['name'][$i]);
                        $_FILES['userfile']['type']= $files['userfile']['type'][$i];
                        $_FILES['userfile']['tmp_name']= $files['userfile']['tmp_name'][$i];
                        $_FILES['userfile']['error']= $files['userfile']['error'][$i];
                        $_FILES['userfile']['size']= $files['userfile']['size'][$i]; 

                        $oldFileName = $_FILES['userfile']['name'];
                        $_FILES['userfile']['name'] =str_replace("'","", $oldFileName);
                        $this->upload->initialize($this->set_upload_options($_FILES['userfile']['name'],'chamber_gallery/'));
                        $this->upload->do_upload();  

                        
                            $data['chamber_details']=$this->admin_model->select_with_where('*', 'id='.$chamber_id, 'doctor_chamber');

                            if($data['chamber_details'][0]['gallery_image']=='')
                            {
                                $data_image['gallery_image']=$_FILES['userfile']['name'];
                            }
                            else
                            {
                                $data_image['gallery_image']=$data['chamber_details'][0]['gallery_image'].','.$_FILES['userfile']['name'];
                            }
                            $this->admin_model->update_function('id', $chamber_id, 'doctor_chamber', $data_image);
                    }
                }
            }

            redirect('admin/chamber_details/'.$doctor_name.'/'.$chamber_id,'refresh');

        }



        public function delete_gallery_img()
        {
            $chamber_id=$this->input->post('chamber_id');
            $img_name=$this->input->post('img_name');

            $chamber_details=$this->admin_model->select_with_where('*', 'id='.$chamber_id, 'doctor_chamber');

            $img_gallery=explode(',', $chamber_details[0]['gallery_image']);
            $data_image['gallery_image']=NULL;
            foreach ($img_gallery as $key => $img) 
            {
               
               if($img==$img_name)
               {

               }
               else
               {
                    $data_image['gallery_image'].=$img.',';
               }
               
            }

            $data_image['gallery_image']=substr($data_image['gallery_image'], 0, -1); // remove last comma

            $path_to_file = 'uploads/chamber_gallery/'.$img_name;
            if(unlink($path_to_file)) 
            {
                $this->admin_model->update_function('id', $chamber_id, 'doctor_chamber', $data_image);
                echo "delete successfull";
            }
        }

    //////////////////********  Doctor End *******/////////////////////////// 

    //////////////***** Common User Starts******////////////////////////

        public function user_list()
        {
            $data['active']='user_list';
            $data['page_title']=$this->lang->line('verified_label').' '.$this->lang->line('users_list_label');
            $data['add_btn']=$this->lang->line('btn_add_label').' '.$this->lang->line('user_label');

            $data['add_btn_link']='add_user';

            $data['breadcrumbs']= '<li><span class="active">'.$data['page_title'].'</span></li>';

            $data['user_list']=$this->admin_model->select_with_where('*','verify_status=1', 'verify_user');
    
            $data['head_data']=$this->head_data();
            $data['user_type']=$this->admin_model->select_all('user_type');
            
            $this->load->view('user/user_list',$data);
        }

        public function unverfied_user_list()
        {
            $data['active']='user_list_un';
            $data['page_title']=$this->lang->line('unverified_label').' '.$this->lang->line('users_list_label');
            $data['add_btn']=$this->lang->line('btn_add_label').' '.$this->lang->line('user_label');

            $data['add_btn_link']='add_user';

            $data['breadcrumbs']= '<li><span class="active">'.$data['page_title'].'</span></li>';

            $data['user_list']=$this->admin_model->select_with_where('*','verify_status=0', 'verify_user');
    
            $data['head_data']=$this->head_data();
            $data['user_type']=$this->admin_model->select_all('user_type');
            
            $this->load->view('user/user_list',$data);
        }

        public function check_verification_code()
        {    
           $user_id=$this->input->post('user_id');
           $verify_code=$this->input->post('verify_code');
           $exist_code=$this->admin_model->select_with_where('*','verify_code="'.$verify_code.'" AND id='.$user_id,'verify_user');

           if(count($exist_code)>0)
           {
            $data_pro['verify_status']=1;
            $this->admin_model->update_function('id', $user_id, 'verify_user', $data_pro);
            $this->session->set_userdata('scc_alt', $this->lang->line('verify_scc_msg'));

            redirect('admin/unverfied_user_list','refresh');
           }
           else
           {
            $this->session->set_userdata('err_alt', $this->lang->line('verify_err_msg'));
            redirect('admin/unverfied_user_list','refresh');
           }
        }


        public function add_user()
        {
            $data['active']='add_user';
            $data['page_title']=$this->lang->line('user_label').' '.$this->lang->line('btn_add_label');
     

            
            $data['breadcrumbs']= '<li><a href="admin/user_list">'.$this->lang->line('users_list_label').'</a><i class="fa fa-circle"></i></li>';
            $data['breadcrumbs'].= '<li><span class="active">'.$data['page_title'].'</span></li>';

            $data['form_icon']='fa fa-user';
            $data['form_label']=$this->lang->line('user_label').' '.$this->lang->line('btn_add_label').' '.$this->lang->line('form_label');

            $data['user_type']=$this->admin_model->select_all('user_type');
            $data['head_data']=$this->head_data();

            $this->load->view('user/add_user',$data);
        }


        public function add_user_post()
        {
            $email=$this->input->post('email');
            $mobile_no=$this->input->post('mobile_no');
            $exist_email=$this->admin_model->select_with_where('*','email="'.$email.'"','verify_user');
            $exist_phone=$this->admin_model->select_with_where('*','mobile_no="'.$mobile_no.'"','verify_user');

            $data['name']=ucwords($this->input->post('name'));
            $data['password']=$this->encryptIt('j12345');
            $data['email']=$email;
            $data['country_code']=$this->input->post('country_code');
            $data['mobile_no']=$mobile_no;
            $data['verify_code']=sprintf("%06d", mt_rand(1, 999999));
            $data['user_type']=$this->input->post('user_type');
            $data['created_at']=date('Y-m-d H:i:s');

           // echo $mobile_no;die();

            if(count($exist_email)>0)
            {
                $this->session->set_userdata('tmp_name', $data['name']);
                $this->session->set_userdata('tmp_country_code', $data['country_code']);
                $this->session->set_userdata('tmp_mobile_no', $data['mobile_no']);
                $this->session->set_userdata('tmp_email', $data['email']);
                $this->session->set_userdata('tmp_user_type', $data['user_type']);

                $this->session->set_userdata('err_alt', $this->lang->line('email_exist_msg'));
                redirect('admin/add_user','refresh');
            }


            else if(count($exist_phone)>0)
            {
                $this->session->set_userdata('tmp_name', $data['name']);
                $this->session->set_userdata('tmp_country_code', $data['country_code']);
                $this->session->set_userdata('tmp_mobile_no', $data['mobile_no']);
                $this->session->set_userdata('tmp_email', $data['email']);
                $this->session->set_userdata('tmp_user_type', $data['user_type']);

                $this->session->set_userdata('err_alt', $this->lang->line('mobile_exist_msg'));
                redirect('admin/add_user','refresh');
            }
            else
            {
                

                
                $verf_id=$this->admin_model->insert_ret('verify_user',$data);
                $exist_code=$this->admin_model->select_with_where('*','verify_code="'.$data['verify_code'].'"','verify_user');

                $data_pro['username'] = preg_replace('/[ ,]+/', '_', trim($data['name']));
                $data_pro['username']=$data_pro['username'].'_'.$verf_id;
                $data_pro['username'] = str_replace("'", '', $data_pro['username']);
                $this->admin_model->update_function('id', $verf_id, 'verify_user', $data_pro);


                // Sending SMS portion
                $api_code=$this->admin_model->select_with_where('*','id=1','sms_content');
                $msg_to=$data['country_code'].$mobile_no;
                $msg_des='Your Verification Code: '.$data['verify_code'].'\nJolpie';

                $head_data=$this->head_data();
                $company_name=$head_data[0]['name'];

                $ret_data=$this->sendsms_library->send_single_sms($company_name,$msg_to,$msg_des);

                if ($ret_data=='err') 
                {
                    $data_sms['send_sms']=$api_code[0]['send_sms']+1;
                    $data_sms['failed']=$api_code[0]['failed']+1;
                    $this->admin_model->update_function('id',$api_code[0]['id'],'mobile_sms_api',$data_sms);
                    //echo "cURL Error #:" . $err;
                } 
                else 
                {
                    $data_sms['send_sms']=$api_code[0]['send_sms']+1;
                    $data_sms['delivered']=$api_code[0]['delivered']+1;
                    $this->admin_model->update_function('id',$api_code[0]['id'],'mobile_sms_api',$data_sms);
                }



                if($_FILES["userfile"]['name'])
                {

                    $data_img['profile_image'] ='';
                    $i_ext = explode('.', $_FILES['userfile']['name']);
                    $target_path = 'user_'.uniqid().'_'. $verf_id .'.' . end($i_ext);
                   
                    $size = getimagesize($_FILES['userfile']['tmp_name']);

                    if (move_uploaded_file($_FILES['userfile']['tmp_name'], 'uploads/user/' . $target_path))
                    {
                        $data_img['profile_image'] = $target_path;
                    }

                    
                    if ($size[0] == 150 || $size[1] == 150) {

                    } 
                    else {
                        $imageWidth = 150; //Contains the Width of the Image

                        $imageHeight = 150;

                        $this->resize($imageWidth, $imageHeight, "uploads/user/" . $target_path, "uploads/user/" . $target_path);
                    }

                    $this->admin_model->update_function('id', $verf_id, 'verify_user', $data_img);

                    $this->session->unset_userdata('tmp_name');
                    $this->session->unset_userdata('tmp_country_code');
                    $this->session->unset_userdata('tmp_mobile_no');
                    $this->session->unset_userdata('tmp_email');
                    $this->session->unset_userdata('tmp_user_type');
                }

               

                redirect('admin/unverfied_user_list','refresh');
                //echo "<pre>"; print_r($data);die();
            }

            
        }

        public function user_profile($username='')
        {
            $data['active']='user_list';
            $data['page_title']=$this->lang->line('user_label').' '.$this->lang->line('profile_tag_label');
            $data['add_btn']=$this->lang->line('btn_add_label').' '.$this->lang->line('users_list_label');
            $data['head_data']=$this->head_data();

            $data['breadcrumbs']= '<li><a href="admin/user_list">'.$this->lang->line('users_list_label').'</a><i class="fa fa-circle"></i></li>';
            $data['breadcrumbs'].= '<li><span class="active">'.$username.'</span></li>';

            //page data starts
            $data['username']=$username;
            $data['personal_info']=$this->admin_model->select_where_join('login.*,registration.*,registration.id as reg_id','login','registration','registration.login_id=login.id','login.username="'.$username.'"');
           $data['user_location']=$this->admin_model->get_individual_address('registration.id='.$data['personal_info'][0]['reg_id']);
            
            if($data['personal_info'][0]['user_type']==3 || $data['personal_info'][0]['user_type']==4)
            {
                

                $data['doctor_details']=$this->admin_model->select_where_two_join('*','doctor_details','doctor_designation_list','doctor_designation_list.id=doctor_details.designation','registration','registration.id=doctor_details.inst_id','doctor_details.doctor_id='.$data['personal_info'][0]['id']);

                $data['chamber_info']=$this->admin_model->select_where_join('doctor_chamber.*,registration.*,registration.id as reg_id','doctor_chamber','registration','registration.id=doctor_chamber.chamber_id','doctor_chamber.doctor_id='.$data['personal_info'][0]['id']);


            }

            //echo "<pre>";print_r($data['chamber_info']);die();
            $this->load->view('user/user_profile',$data);
        }

        public function get_user_profile_tab_ajax()
        {
           $tab_name=$this->input->post('tab_name');
           $username=$this->input->post('username');
           $user_id=$this->input->post('user_id');
           $data['user_id']=$user_id;
           $data['username']=$username;

            $data['personal_info']=$this->admin_model->select_where_join('*,registration.id as reg_id','login','registration','registration.login_id=login.id','login.username="'.$username.'"');

           if($tab_name=='account')
            {
              
               $data['user_location']=$this->admin_model->get_individual_address('registration.id='.$data['personal_info'][0]['reg_id']);
            }

            if($tab_name=='dr_profile')
            {
               $data['dr_profile']=$this->admin_model->select_where_join('*,registration.id as reg_id','login','registration','registration.login_id=login.id','login.username="'.$username.'"');

               $data['doctor_specialize']=$this->admin_model->select_with_where('*','doctor_id='.$user_id,'doctor_details');

               $data['specialization_list']=$this->admin_model->select_all('doctor_specialization_list');

               $data['designation_list']=$this->admin_model->select_all('doctor_designation_list');

               $data['degree_list']=$this->admin_model->select_all('doctor_degree_list');

               $data['doctor_education']=$this->admin_model->select_all('doctor_education');

               $data['doctor_experience']=$this->admin_model->select_where_two_join('*,doctor_experience.id as exp_id','doctor_experience','doctor_designation_list','doctor_designation_list.id=doctor_experience.designation_id','registration','registration.id=doctor_experience.hospital_id','doctor_experience.doctor_id='.$user_id);

               $data['hospital_list']=$this->admin_model->select_where_join('registration.name,registration.id as reg_id','registration','login','login.id=registration.login_id','login.user_type=9');


               $data['chamber_list']=$this->admin_model->select_where_left_join('doctor_chamber.*,doctor_chamber.id as chamber_id,registration.name as chamber_name,registration.id as reg_id','doctor_chamber','registration','registration.id=doctor_chamber.chamber_id','doctor_chamber.doctor_id='.$user_id);
               //echo "<pre>";print_r($data['chamber_list']);die();
            }

            if($tab_name=='dr_package')
            {
               $data['doctor_package_list']=$this->admin_model->select_where_join('*,doctor_package.id as pack_id','doctor_package','doctor_package_list','doctor_package_list.id=doctor_package.package_id','doctor_id='.$user_id.' AND doctor_package.package_type=1');
               
               
            }

            if($tab_name=='dr_consult')
            {
                $data['doctor_package_list']=$this->admin_model->select_where_join('*,doctor_package.id as pack_id','doctor_package','doctor_package_list','doctor_package_list.id=doctor_package.package_id','doctor_id='.$user_id.' AND doctor_package.package_type=1');
               $data['consult_text_list']=$this->admin_model->get_text_consult_list('doctor_package.doctor_id='.$user_id);
               //echo "<pre>";print_r($data['consult_text_list']);die();
               
            }

            $this->load->view('user/include/'.$tab_name, $data);
           
        }

        public function add_package_post($username)
        {

            $data['package_title']=$this->input->post('package_title');
            $data['created_at']=date('Y-m-d H:i:s');

            $doc_pack['package_id']=$this->admin_model->insert_ret('doctor_package_list',$data);

            $doc_pack['doctor_id']=$this->input->post('doctor_id');
            $doc_pack['price']=$this->input->post('price');
            $doc_pack['discount']=$this->input->post('discount');
            $doc_pack['package_validity']=$this->input->post('package_validity');
            $doc_pack['package_type']=$this->input->post('package_type');// 0=individual;1=package/combo
            $doc_pack['package_amount']=$this->input->post('text').','.$this->input->post('audio').','.$this->input->post('video');
            $doc_pack['package_validity']=$this->input->post('package_validity');
            $doc_pack['package_description']=$this->input->post('package_description');
            $doc_pack['created_at']=date('Y-m-d H:i:s');

// echo "<pre>"; print_r($doc_pack);die();
            $id=$this->admin_model->insert_ret('doctor_package',$doc_pack);

            if ($doc_pack['package_type']==1) 
            {
                if($_FILES["userfile"]['name'])
                {

                    $data_img['package_image'] ='';
                    $i_ext = explode('.', $_FILES['userfile']['name']);
                    $target_path = 'user_'.uniqid().'_'. $id .'.' . end($i_ext);
                   
                    $size = getimagesize($_FILES['userfile']['tmp_name']);

                    if (move_uploaded_file($_FILES['userfile']['tmp_name'], 'uploads/package/' . $target_path))
                    {
                        $data_img['package_image'] = $target_path;
                    }

                    if ($size[0] == 400 || $size[1] == 220) {

                    } 
                    else {
                        $imageWidth = 400; //Contains the Width of the Image

                        $imageHeight = 220;

                        $this->resize($imageWidth, $imageHeight, "uploads/package/" . $target_path, "uploads/package/" . $target_path);
                    }

                    $this->admin_model->update_function('id', $id, 'doctor_package', $data_img);
                }
            }
            redirect('admin/user_profile/'.$username.'#dr_package','refresh');
        }

        public function get_view_package_details()
        {
            $package_id=$this->input->post('package_id');
            $doctor_id=$this->input->post('doctor_id');
            $data['package_details']=$this->admin_model->select_where_join('*,doctor_package.id as pack_id','doctor_package','doctor_package_list','doctor_package_list.id=doctor_package.package_id','doctor_id='.$doctor_id.' AND doctor_package.id='.$package_id);
            $username=$this->admin_model->select_with_where('*','id='.$doctor_id,'login');
            $data['username']=$username[0]['username'];
            $data['doctor_id']=$doctor_id;
            //echo "heelo";
            $this->load->view('user/include/dr_package_details', $data);
        }

        public function update_package_post($pack_id)
        {

            $username=$this->input->post('username');
            $data['package_title']=$this->input->post('package_title');
            $data['created_at']=date('Y-m-d H:i:s');

            $exist_package_name=$this->admin_model->select_with_where('*','package_title="'.$data['package_title'].'"','doctor_package_list');

            //echo "<pre>";print_r($exist_package_name);die();
            $package_id=0;
            if(count($exist_package_name)>0)
            {
                $package_id=$exist_package_name[0]['id'];
            }
            else
            {
                $package_id=$this->admin_model->insert_ret('doctor_package_list',$data);
            }
            

            $doc_pack['package_id']=$package_id;
            $doc_pack['price']=$this->input->post('price');
            $doc_pack['discount']=$this->input->post('discount');
            $doc_pack['package_validity']=$this->input->post('package_validity');
            $doc_pack['package_type']=$this->input->post('package_type');
            $doc_pack['package_amount']=$this->input->post('text').','.$this->input->post('audio').','.$this->input->post('video');
            $doc_pack['package_description']=$this->input->post('package_description');


            $this->admin_model->update_function('id', $pack_id, 'doctor_package', $doc_pack);

            if($_FILES["userfile"]['name'])
                {

                    $data_img['package_image'] ='';
                    $i_ext = explode('.', $_FILES['userfile']['name']);
                    $target_path = 'user_'.uniqid().'_'. $pack_id .'.' . end($i_ext);
                   
                    $size = getimagesize($_FILES['userfile']['tmp_name']);

                    if (move_uploaded_file($_FILES['userfile']['tmp_name'], 'uploads/package/' . $target_path))
                    {
                        $data_img['package_image'] = $target_path;
                    }

                    
                    if ($size[0] == 400 || $size[1] == 220) {

                    } 
                    else {
                        $imageWidth = 400; //Contains the Width of the Image

                        $imageHeight = 220;

                        $this->resize($imageWidth, $imageHeight, "uploads/package/" . $target_path, "uploads/package/" . $target_path);
                    }

                    $this->admin_model->update_function('id', $pack_id, 'doctor_package', $data_img);
                }
            redirect('admin/user_profile/'.$username.'#dr_package','refresh');
        }

        public function get_text_consult_ajax()
        {
            $group_id=$this->input->post('group_id');
            $data['consult_text_details']=$this->admin_model->get_text_consult_details('consult_text.group_id='.$group_id);
            //echo "<pre>";print_r($data['consult_text_details']);die();
            $this->load->view('user/include/dr_consult_details', $data);
        }

        public function add_dr_education($doctor_id='')
        {
            $data['inst_id']=$this->input->post('inst_id');
            $username=$this->input->post('username');
            $data['degree_id']=$this->input->post('degree_id');
            $data['passing_year']=$this->input->post('passing_year');
            $data['doctor_id']=$doctor_id; 
            $data['created_at']=date('Y-m-d H:i:s'); 
            $this->admin_model->insert('doctor_education',$data);

            redirect('admin/user_profile/'.$username.'#dr_profile','refresh');
        }


        public function update_doctor_education()
        {
            $id=$this->input->post('id');

            $data['inst_id']=$this->input->post('inst_id');
            $data['degree_id']=$this->input->post('degree_id');
            $data['passing_year']=$this->input->post('passing_year');

            $this->admin_model->update_function('id',$id,'doctor_education',$data);
        }

        public function delete_doctor_education($username,$id)
        {
            $this->admin_model->delete_function('doctor_education','id',$id);
            redirect('admin/user_profile/'.$username.'#dr_profile','refresh');
        }

        public function add_dr_new_specialize($doctor_id)
        {

           $username=$this->input->post('username');
           $sp_id=$this->input->post('specialist');
           $exist_sp_id=$this->admin_model->select_with_where('specialist','doctor_id='.$doctor_id,'doctor_details');

           if($exist_sp_id[0]['specialist']!=null || $exist_sp_id[0]['specialist']!='')
           {

                $find_sp_id=$this->admin_model->select_with_where('*','doctor_id='.$doctor_id.' AND FIND_IN_SET('.$sp_id.',specialist)','doctor_details');
                if(count($find_sp_id)>0){}
                else
                {
                    $data['specialist']=$exist_sp_id[0]['specialist'].','.$sp_id;
                    $this->admin_model->update_function('doctor_id',$doctor_id,'doctor_details',$data);
                }
           }

           else
           {
            $data['specialist']=$sp_id;
            $this->admin_model->insert('doctor_details',$data);
           }

           redirect('admin/user_profile/'.$username.'#dr_profile','refresh');
        }
        public function insert_new_specialization()
        {
            $sp_name=$this->input->post('sp_name');
            $exist_sp_name=$this->admin_model->select_with_where('*','sp_name="'.$sp_name.'"','doctor_specialization_list');
            if(count($exist_sp_name)>0)
            {
                echo "error";
            }

            else
            {
                $data['sp_name']=$sp_name;
                $this->admin_model->insert('doctor_specialization_list',$data);
                $specialist_list=$this->admin_model->select_all('doctor_specialization_list');
                echo '<option value=""></option>';
                foreach ($specialist_list as $key => $value) 
                {
                    echo '<option value="'.$value['id'].'">'.$value['sp_name'].'</option>';
                }
                echo '<option value="add_new">Add New</option>';
            }
        }

        public function add_doctor_experience_post($doctor_id)
        {
            $username=$this->input->post('username');
            $data['designation_id']=$this->input->post('designation_id');
            $data['hospital_id']=$this->input->post('hospital_id');
            $data['date_from']=$this->input->post('date_from');
            $data['date_to']=$this->input->post('date_to');
            $data['doctor_id']=$doctor_id;
            $data['created_at']=date('Y-m-d H:i:s');

            $this->admin_model->insert('doctor_experience',$data);
            redirect('admin/user_profile/'.$username.'#dr_profile','refresh');
        }

        public function update_doctor_experience()
        {
            $id=$this->input->post('id');

            $data['designation_id']=$this->input->post('designation_id');
            $data['hospital_id']=$this->input->post('hospital_id');
            $data['date_from']=$this->input->post('date_from');
            $data['date_to']=$this->input->post('date_to');

            $this->admin_model->update_function('id',$id,'doctor_experience',$data);

        }
    //////////////***** Common User End******////////////////////////


     
///////////////////////********  Doctor Feature End *******////////////////////////////////
   
//////////////////********  Extra Feature Start *******///////////////////////////
    ///////////////////////********  Magazine Start *******////////////////////////////////
        public function magazine_category()
        {
            $data['active']='m_category';
            $data['page_title']=$this->lang->line('magazine_cat_label');
            $data['add_btn']=$this->lang->line('btn_add_label').' '.$this->lang->line('magazine_cat_label');
            $data['head_data']=$this->head_data();

            $data['breadcrumbs']= '<li><span class="active">'.$this->lang->line('magazine_cat_label').'</span></li>';
            $data['category_list']=$this->admin_model->select_all_name_ascending('cat_name','magazine_category');

            $this->load->view('extra/category_list',$data);
        }


        public function add_category_post()
        {
            $data['cat_name']=$this->input->post('cat_name');
            $data['created_at']=date('Y-m-d H:i:s');
            $this->admin_model->insert('magazine_category',$data);
            redirect('admin/magazine_category','refresh');
        }


        public function update_category_post()
        {
            $id=$this->input->post('cat_id');
            $data['cat_name']=$this->input->post('cat_name');

            $this->admin_model->update_function('id',$id,'magazine_category',$data);
            redirect('admin/magazine_category','refresh');
        }

        public function delete_category($id)
        {
            $this->admin_model->delete_function('magazine_category', 'id', $id);
            $this->session->set_userdata('scc_alt', $this->lang->line('delete_scc_msg'));
            redirect('admin/magazine_category','refresh');
        }


        public function magazine_tags($value='')
        {
            $data['active']='m_tags';
            $data['page_title']=$this->lang->line('magazine_tag_label');
            $data['add_btn']=$this->lang->line('btn_add_label').' '.$this->lang->line('tag_label');
            $data['head_data']=$this->head_data();

            $data['breadcrumbs']= '<li><span class="active">'.$this->lang->line('magazine_tag_label').'</span></li>';
            $data['tag_list']=$this->admin_model->select_all_name_ascending('tag_name','magazine_tag');

            $this->load->view('extra/tag_list',$data);
        }


        public function add_tag_post()
        {
            $data['tag_name']=$this->input->post('tag_name');
            $data['created_at']=date('Y-m-d H:i:s');
            $tag_id=$this->admin_model->insert_ret('magazine_tag',$data);



            if($_FILES["userfile"]['name'])
            {

                $data_img['tag_image'] ='tag_default.png';
                $i_ext = explode('.', $_FILES['userfile']['name']);
                $target_path = 'tag_'.uniqid().'_'. $tag_id .'.' . end($i_ext);
               
                $size = getimagesize($_FILES['userfile']['tmp_name']);

                if (move_uploaded_file($_FILES['userfile']['tmp_name'], 'uploads/tags/' . $target_path))
                {
                    $data_img['tag_image'] = $target_path;
                }

                
                if ($size[0] == 180 || $size[1] == 180) {

                } 
                else {
                    $imageWidth = 180; //Contains the Width of the Image

                    $imageHeight = 180;

                    $this->resize($imageWidth, $imageHeight, "uploads/tags/" . $target_path, "uploads/tags/" . $target_path);
                }

                $this->admin_model->update_function('id', $tag_id, 'magazine_tag', $data_img);

            }
            redirect('admin/magazine_tags','refresh');
        }


        public function update_tag_post()
        {
            $id=$this->input->post('tag_id');
            $data['tag_name']=$this->input->post('tag_name');

            $this->admin_model->update_function('id',$id,'magazine_tag',$data);
            redirect('admin/magazine_tags','refresh');
        }

        public function delete_tag($id)
        {
            $this->admin_model->delete_function('magazine_tag', 'id', $id);
            $this->session->set_userdata('scc_alt', $this->lang->line('delete_scc_msg'));
            redirect('admin/magazine_tags','refresh');
        }



        public function magazine()
        {
            $data['active']='magazine';
            $data['page_title']=$this->lang->line('magazine_list_label');
            $data['add_btn']=$this->lang->line('btn_add_label').' '.$this->lang->line('magazine_label');
            $data['head_data']=$this->head_data();
            $data['breadcrumbs']= '<li><span class="active">'.$this->lang->line('magazine_list_label').'</span></li>';
            $data['magazine_list']=$this->admin_model->select_where_left_join('*,magazine_post.id as m_id,magazine_post.created_at as c_date','magazine_post','magazine_category','magazine_category.id=magazine_post.cat_id','magazine_post.status=1');

           // $data['magazine_list']=$this->admin_model->select_where_left_join('*,magazine_post.id as m_id,magazine_post.created_at as c_date','magazine_post','magazine_category','magazine_category.id=magazine_post.cat_id','magazine_post.status=1 AND FIND_IN_SET(4,magazine_post.tag_id)');

            //echo "<pre>";print_r($data['magazine_list']);die();

            $this->load->view('extra/magazine_list',$data);
        }



        public function add_magazine()
        {
            $data['active']='magazine';
            $data['page_title']=$this->lang->line('magazine_label').' '.$this->lang->line('btn_add_label');
            $data['head_data']=$this->head_data();

            
            $data['breadcrumbs']= '<li><a href="admin/magazine">'.$this->lang->line('magazine_list_label').'</a><i class="fa fa-circle"></i></li>';
            $data['breadcrumbs'].= '<li><span class="active">'.$data['page_title'].'</span></li>';

            $data['category_list']=$this->admin_model->select_all_name_ascending('cat_name','magazine_category');

            $data['tag_list']=$this->admin_model->select_all_name_ascending('tag_name','magazine_tag');
            $this->load->view('extra/add_magazine',$data);
       
        }



        public function edit_magazine($id)
        {
            $data['active']='magazine';
            $data['page_title']=$this->lang->line('magazine_label').' '.$this->lang->line('btn_edit_label');
     
            $data['head_data']=$this->head_data();
            
            $data['breadcrumbs']= '<li><a href="admin/magazine">'.$this->lang->line('magazine_list_label').'</a><i class="fa fa-circle"></i></li>';
            $data['breadcrumbs'].= '<li><span class="active">'.$data['page_title'].'</span></li>';

            $data['category_list']=$this->admin_model->select_all_name_ascending('cat_name','magazine_category');

            $data['tag_list']=$this->admin_model->select_all_name_ascending('tag_name','magazine_tag');
            $data['magazine_details']=$this->admin_model->select_where_join('*,magazine_post.id as m_id','magazine_post','magazine_category','magazine_category.id=magazine_post.cat_id','magazine_post.id='.$id);

            $this->load->view('extra/edit_magazine',$data);
       
        }



        public function add_magazine_post()
        {
            $data['cat_id']=$this->input->post('cat_id');
            $tag_id=$this->input->post('tag_id');
            $data['tag_id']='';
            foreach ($tag_id as $key => $value) {
                if(($key+1)===count($tag_id))
                {
                    $data['tag_id'].=$value;
                }
                else
                {
                    $data['tag_id'].=$value.',';
                }
            }

            $data['title']=$this->input->post('title');
            $data['description']=$this->input->post('details');
            $data['created_at']=date('Y-m-d H:i:s');
            $data['user_id']=$this->session->userdata('user_id');

            $id=$this->admin_model->insert_ret('magazine_post',$data);
            
            $i_ext = explode('.', $_FILES['file']['name']);
            $target_path = 'magazine_' . $id . '.' . end($i_ext);
            $size = getimagesize($_FILES['file']['tmp_name']);
            if (move_uploaded_file($_FILES['file']['tmp_name'], 'uploads/magazine/' . $target_path)) {
                $data_img['image'] = $target_path;
            }

            $this->admin_model->update_function('id', $id, 'magazine_post', $data_img);

            if ($size[0] == 550 || $size[1] == 380) {

            } else {
                $imageWidth = 550; //Contains the Width of the Image

                $imageHeight = 380;

                $this->resize($imageWidth, $imageHeight, "uploads/magazine/" . $target_path, "uploads/magazine/" . $target_path);
            }

            redirect('admin/magazine','refresh');
       
        }




        public function update_magazine_post($id)
        {
            $data['cat_id']=$this->input->post('cat_id');
            $tag_id=$this->input->post('tag_id');
            $data['tag_id']='';
            foreach ($tag_id as $key => $value) {
                if(($key+1)===count($tag_id))
                {
                    $data['tag_id'].=$value;
                }
                else
                {
                    $data['tag_id'].=$value.',';
                }
            }

            $data['title']=$this->input->post('title');
            $data['description']=$this->input->post('details');

            $this->admin_model->update_function('id', $id, 'magazine_post', $data);
            


            if($_FILES['file']['name'])
            {
                $pre_img=$this->input->post('pre_img');
                $i_ext = explode('.', $_FILES['file']['name']);
                $target_path = $pre_img;
                $size = getimagesize($_FILES['file']['tmp_name']);
                if (move_uploaded_file($_FILES['file']['tmp_name'], 'uploads/magazine/' . $target_path)) {
                    $data_img['image'] = $target_path;
                }

                $this->admin_model->update_function('id', $id, 'magazine_post', $data_img);

                if ($size[0] == 550 || $size[1] == 380) {

                } else {
                    $imageWidth = 550; //Contains the Width of the Image

                    $imageHeight = 380;

                    $this->resize($imageWidth, $imageHeight, "uploads/magazine/" . $target_path, "uploads/magazine/" . $target_path);
                }
            }
            redirect('admin/magazine','refresh');
       
        }




        public function delete_magazine_post($id,$name)
        {
            $path_to_file = 'uploads/magazine/'.$name;
            
            if(unlink($path_to_file)) { 
                //echo 'deleted successfully';
                $this->admin_model->delete_function_cond('magazine_post', 'id='.$id);
                $this->session->set_userdata('scc_alt', $this->lang->line('delete_scc_msg'));
            }
            else {
                 //echo 'errors occured';
                 $this->session->set_userdata('err_alt', $this->lang->line('delete_err_msg'));
            }
            redirect('admin/magazine','refresh');
        }

    ///////////////////////********  Magazine End *******////////////////////////////////
//////////////////********  Extra Feature End *******///////////////////////////

///////////////////////********  Manage Area Start *******////////////////////////////////


    public function groups()
    {
        $data['active']='groups';
        $data['page_title']=$this->lang->line('groups_list_label');
        $data['add_btn']=$this->lang->line('btn_add_label').' '.$this->lang->line('groups_label');
        $data['head_data']=$this->head_data();

        $data['breadcrumbs']= '<li><span class="active">'.$this->lang->line('groups_label').'</span></li>';
        $data['groups_list']=$this->admin_model->select_with_where('*','status=1','groups');

        $this->load->view('manage/groups_list',$data);
    }

    public function sms_report()
    {
        $data['active']='sms_report';
        $data['page_title']=$this->lang->line('sms_report_label');
        
        $data['head_data']=$this->head_data();

        $data['breadcrumbs']= '<li><span class="active">'.$this->lang->line('sms_report_label').'</span></li>';
        $data['groups_list']=$this->admin_model->select_with_where('*','status=1','groups');

        $this->load->view('manage/groups_list',$data);
    }

    public function sms_content()
    {
        $data['active']='sms_content';
        $data['page_title']=$this->lang->line('sms_content_label');
        $data['add_btn']=$this->lang->line('btn_add_label').' '.$this->lang->line('sms_content_label');

        $data['head_data']=$this->head_data();

        $data['breadcrumbs']= '<li><span class="active">'.$this->lang->line('sms_content_label').'</span></li>';
        $data['content_list']=$this->admin_model->select_all('mobile_sms_message');

        $this->load->view('manage/sms_content_list',$data);
    }

    public function add_sms_content_post()
    {
        $data['sms_title']=$this->input->post('sms_title');
        $data['message']=$this->input->post('message');

        $this->session->set_userdata('scc_alt', $this->lang->line('insert_scc_msg'));
        $this->admin_model->insert('mobile_sms_message',$data);

        redirect('admin/sms_content','refresh');

    }

    public function update_sms_content()
    {
        $id=$this->input->post('sms_id');
        $data['sms_title']=$this->input->post('sms_title');
        $data['message']=$this->input->post('message');
        $this->admin_model->update_function('id', $id, 'mobile_sms_message', $data);
        $this->session->set_userdata('scc_alt', $this->lang->line('update_scc_msg'));
        redirect('admin/sms_content','refresh');

    }

    public function district()
    {
        $data['active']='district';
        $data['page_title']=$this->lang->line('district_list_label');
        $data['add_btn']=$this->lang->line('btn_add_label').' '.$this->lang->line('district_label');

        $data['breadcrumbs']= '<li><span class="active">'.$this->lang->line('district_list_label').'</span></li>';

        if($this->session->userdata('type')==0)
        {
            $data['district_list']=$this->admin_model->select_join('*,districts.id as dist_id,districts.name as dist_name,districts.bn_name as dist_bn_name','districts','divisions','divisions.id=districts.division_id');
            $data['head_data']=$this->head_data();
        
            $this->load->view('location/district_list',$data);
        }

        else
        {
            $data['unauthorised_msg']=$this->lang->line('unauthorised_msg');
            $this->load->view('blank',$data);
        }
    }

    public function add_district()
    {
        $data['active']='district';
        $data['page_title']=$this->lang->line('district_label').' '.$this->lang->line('btn_add_label');
 

        
        $data['breadcrumbs']= '<li><a href="admin/district">'.$this->lang->line('district_list_label').'</a><i class="fa fa-circle"></i></li>';
        $data['breadcrumbs'].= '<li><span class="active">'.$data['page_title'].'</span></li>';

        $data['head_data']=$this->head_data();
        if($this->session->userdata('type')==0)
        {
            $data['division_list']=$this->admin_model->select_all('divisions');

            $this->load->view('location/add_district',$data);

        }

        else
        {
            $data['unauthorised_msg']=$this->lang->line('unauthorised_msg');
            $this->load->view('blank',$data);
        }
   
    }


    public function edit_district($id)
    {
        $data['active']='district';
        $data['page_title']=$this->lang->line('district_label').' '.$this->lang->line('btn_edit_label');
 

        
        $data['breadcrumbs']= '<li><a href="admin/district">'.$this->lang->line('district_list_label').'</a><i class="fa fa-circle"></i></li>';
        $data['breadcrumbs'].= '<li><span class="active">'.$data['page_title'].'</span></li>';

        $data['head_data']=$this->head_data();

        if($this->session->userdata('type')==0)
        {
            $data['division_list']=$this->admin_model->select_all('divisions');

            $data['district_details']=$this->admin_model->select_with_where('*,districts.id as dist_id,districts.name as dist_name,districts.bn_name as dist_bn_name','id='.$id,'districts');

            $this->load->view('location/edit_district',$data);
        }
        else
        {
            $data['unauthorised_msg']=$this->lang->line('unauthorised_msg');
            $this->load->view('blank',$data);
        }
    }


    public function add_district_post() 
    {
        $data['division_id'] = $this->input->post('district_id');
        $data['name'] = $this->input->post('dist_name');
        $data['bn_name'] = $this->input->post('dist_bn_name');
        
        $area['area_id']=$this->admin_model->insert_ret('districts',$data);
        $this->session->set_userdata('scc_alt', $this->lang->line('insert_scc_msg'));
        redirect('admin/district','refresh');
    }


    public function update_district_post($id) 
    {
        $data['division_id'] = $this->input->post('district_id');
        $data['name'] = $this->input->post('dist_name');
        $data['bn_name'] = $this->input->post('dist_bn_name');
        
        $this->admin_model->update_function('id', $id, 'districts', $data);
        $this->session->set_userdata('scc_alt', $this->lang->line('update_scc_msg'));
        redirect('admin/district','refresh');
    }

    public function delete_district($id)
    {
  
        $this->admin_model->delete_function('districts', 'id', $id);
        $this->session->set_userdata('scc_alt', $this->lang->line('delete_scc_msg'));
     
        redirect('admin/district','refresh');
    }



    public function area()
    {
        $data['active']='area';
        $data['page_title']=$this->lang->line('area_list_label');
        $data['add_btn']=$this->lang->line('btn_add_label').' '.$this->lang->line('area_label');

        $data['breadcrumbs']= '<li><span class="active">'.$this->lang->line('area_list_label').'</span></li>';

        $data['head_data']=$this->head_data();

        if($this->session->userdata('type')==0)
        {
            $data['area_list']=$this->admin_model->select_join('*','area','districts','districts.id=area.district_id');
            $this->load->view('location/area_list',$data);
        }
        else
        {
            $data['unauthorised_msg']=$this->lang->line('unauthorised_msg');
            $this->load->view('blank',$data);
        }
    }


    public function add_area()
    {
        $data['active']='area';
        $data['page_title']=$this->lang->line('area_label').' '.$this->lang->line('btn_add_label');
 

        
        $data['breadcrumbs']= '<li><a href="admin/area">'.$this->lang->line('area_list_label').'</a><i class="fa fa-circle"></i></li>';
        $data['breadcrumbs'].= '<li><span class="active">'.$data['page_title'].'</span></li>';

        $data['head_data']=$this->head_data();

        if($this->session->userdata('type')==0)
        {
            $data['district_list']=$this->admin_model->select_all('districts');

            $this->load->view('location/add_area',$data);

        }
        else
        {
            $data['unauthorised_msg']=$this->lang->line('unauthorised_msg');
            $this->load->view('blank',$data);
        }
   
    }


    public function edit_area($id)
    {
        $data['active']='area';
        $data['page_title']=$this->lang->line('area_label').' '.$this->lang->line('btn_edit_label');
 

        
        $data['breadcrumbs']= '<li><a href="admin/area">'.$this->lang->line('area_list_label').'</a><i class="fa fa-circle"></i></li>';
        $data['breadcrumbs'].= '<li><span class="active">'.$data['page_title'].'</span></li>';

        $data['head_data']=$this->head_data();

        if($this->session->userdata('type')==0)
        {
            $data['district_list']=$this->admin_model->select_all('districts');

            $data['area_details']=$this->admin_model->select_with_where('*','area_id='.$id,'area');

            $this->load->view('location/edit_area',$data);
        }

        else
        {
            $data['unauthorised_msg']=$this->lang->line('unauthorised_msg');
            $this->load->view('blank',$data);
        }
    }



    public function add_area_post() 
    {

        $data['district_id'] = $this->input->post('district_id');
        $div_id=$this->admin_model->select_with_where('*', 'id='.$data['district_id'], 'districts');
        $data['division_id']=$div_id[0]['division_id'];
        $data['area_title'] = $this->input->post('area_title');
        $data['area_title_bangla'] = $this->input->post('area_title_bangla');
        $data['description'] = $this->input->post('description');
        
        $area['area_id']=$this->admin_model->insert_ret('area',$data);
        $this->session->set_userdata('scc_alt', $this->lang->line('insert_scc_msg'));
        redirect('admin/area','refresh');
    }


    public function update_area_post($id) 
    {
        $data['district_id'] = $this->input->post('district_id');
        $data['area_title'] = $this->input->post('area_title');
        $data['area_title_bangla'] = $this->input->post('area_title_bangla');
        $data['description'] = $this->input->post('description');
        
        $this->admin_model->update_function('area_id', $id, 'area', $data);
        $this->session->set_userdata('scc_alt', $this->lang->line('update_scc_msg'));
        redirect('admin/area','refresh');
    }


    public function delete_area($id)
    {
  
        $this->admin_model->delete_function('area', 'area_id', $id);
        $this->session->set_userdata('scc_alt', $this->lang->line('delete_scc_msg'));
     
        redirect('admin/area','refresh');
    }

///////////////////////********  Manage Area End *******////////////////////////////////




/////////////////////********  Company Info Start *******////////////////////////////
    public function company_info()
    {
        if($this->session->userdata('type')==0)
        {
            $data['active']='company_info';
            $data['page_title']=$this->lang->line('company_info_label');


            $data['breadcrumbs']= '<li><span class="active">'.$this->lang->line('btn_update_label').' '.$this->lang->line('company_info_label').'</span></li>';

            $data['company_info']=$this->admin_model->select_all('company');
            $data['head_data']=$this->head_data();

            //echo "<pre>";print_r($data['product_list']);die();
            
            $this->load->view('company',$data);
        }

        else
        {
            $data['unauthorised_msg']=$this->lang->line('unauthorised_msg');
            $this->load->view('blank',$data);
        }
    }

    public function update_company_info_post($id=1)
    {
        $data['name']=$this->input->post('name');
        $data['address']=$this->input->post('address');
        $data['phone']=$this->input->post('phone');
        $data['email']=$this->input->post('email');
        $data['fb_link']=$this->input->post('fb_link');
        $data['linked_link']=$this->input->post('linked_link');
        $data['twitter_link']=$this->input->post('twitter_link');
        $data['details']=$this->input->post('details');

        $this->admin_model->update_function('id', $id, 'company', $data);



        if($_FILES['userfile']['name'])
        {
            $_FILES['userfile']['name']= uniqid().'_'.underscore($_FILES['userfile']['name']);

            $oldFileName = $_FILES['userfile']['name'];
            $_FILES['userfile']['name'] =str_replace("'","", $oldFileName);
            $this->upload->initialize($this->set_upload_options($_FILES['userfile']['name'],''));
            $this->upload->do_upload();

            $main_image['logo']=$_FILES['userfile']['name'];
            $this->admin_model->update_function('id', $id, 'company', $main_image);
        }

        redirect('admin/company_info','refresh');
    }
/////////////////////********  Company Info End *******////////////////////////////

////////////////////********  General Function Starts *******/////////////////////////

    public function resize($height, $width, $source, $destination)
    {
        $this->load->library('image_lib');
        $config['image_library'] = 'gd2';
        $config['source_image'] = $source;
        $config['overwrite'] = TRUE;
        $image_config['quality'] = "100%";
        //$image_config['maintain_ratio'] = FALSE;
        $config['master_dim'] = 'width';
        $config['height'] = $height;
        $config['width'] = $width;
        $config['new_image'] = $destination; //you should have write permission here..
        $this->image_lib->initialize($config);
        $this->image_lib->resize();
    }

     public function resize2($height,$width)
    {
        $this->load->library('image_lib');
        $config['image_library'] = 'gd2';
        $config['source_image'] = "uploads/watermark.png";
        $config['overwrite'] = TRUE;
        $image_config['quality'] = "100%";
        $image_config['maintain_ratio'] = FALSE;
        $config['height'] = $height;
        $config['width'] = $width;
        $config['new_image'] = "uploads/product/";//you should have write permission here..
        $this->image_lib->initialize($config);
        $this->image_lib->resize();
    }



    private function set_upload_options($file_name,$folder_name)
    {   
        //upload an image options
        $url=base_url();

        $config = array();
        $config['file_name'] = $file_name;
        $config['upload_path'] = 'uploads/'.$folder_name;
        $config['allowed_types'] = 'gif|jpg|png|jpeg';
        $config['max_size']      = '0';
        $config['overwrite']     = TRUE;

        return $config;
    }




    public function watermark_image($imageWidth,$imageHeight)
    {
        $file = 'uploads/watermark.png';
        $newfile ='uploads/watermark.png';
         
        if (!copy($file, $newfile)) {
           // echo "failed to copy $file...\n";
        }else{
           // echo "copied $file into $newfile\n";
        }
        
        //shell_exec("cp -r ".$file." ".$newfile);
        //echo $imageWidth.' '.$imageHeight;
        //die();
        $this->resize2($imageWidth,$imageHeight);
                
            
    }




     public function overlay($path)
    {
        $config['image_library']    = 'gd2';
        $config['source_image']     = 'uploads/product/'.$path;
        $config['wm_type']          = 'overlay';
        $config['wm_overlay_path']  = 'uploads/watermark.png'; //the overlay image
        $config['wm_vrt_alignment'] = 'middle';
        $config['wm_hor_alignment'] = 'center';
        $config['wm_padding'] = '0';
     // $config['wm_x_transp'] = '200';
        //$config['wm_y_transp'] = '20';
        $config['wm_opacity'] = '50';

        $this->image_lib->initialize($config);
    
        
        if (!$this->image_lib->watermark()) {
            echo $this->image_lib->display_errors();
        }
    }

    public function head_data()
    {
        $id = 1;
        $data=$this->admin_model->select_with_where('*', 'id='.$id, 'company');
        return $data;
    }

    public function get_sub_category()
    {
        $cat_id=$this->input->post('cat_id');
        $get_sub_category=$this->admin_model->select_with_where('*', 'cat_id='.$cat_id, 'sub_category');

        echo "<option></option>";
        foreach ($get_sub_category as $key => $value) {
            echo '<option value="'.$value["id"].'">'.$value["sub_name"].'</option>';
        }
    }


    public function get_district()
    {
        $division_id=$this->input->post('division_id');
        $get_district=$this->admin_model->select_with_where('*', 'division_id='.$division_id, 'districts');

        echo "<option></option>";
        foreach ($get_district as $key => $value) {
            echo '<option value="'.$value["id"].'">'.$value["name"].' '.$value["bn_name"].'</option>';
        }
    }


    public function get_area()
    {
        $district_id=$this->input->post('district_id');
        $get_area=$this->admin_model->select_with_where('*', 'district_id='.$district_id, 'area');

        echo "<option></option>";
        foreach ($get_area as $key => $value) {
            echo '<option value="'.$value["area_id"].'">'.$value["area_title"].'</option>';
        }
    }

    public function update_shop_status()
    {
        $shop_id=$this->input->post('id');
        $status=$this->input->post('status');
        $data['status'];
        if($status==1)
        {
            $data['status']=0;
        }

        else
        {
            $data['status']=1;
        }

        $this->admin_model->update_function('id', $shop_id, 'shop', $data);
    }



    public function update_market_status()
    {
        $id=$this->input->post('id');
        $status=$this->input->post('status');
        $data['status'];
        if($status==1)
        {
            $data['status']=0;
        }

        else
        {
            $data['status']=1;
        }

        $this->admin_model->update_function('id', $id, 'market', $data);
    }

    function encryptIt($q) {
        $cryptKey = 'Lf6Q5htqdgnSn0AABqlsSddj1QNu0fJs';
        $qEncoded = base64_encode(mcrypt_encrypt(MCRYPT_RIJNDAEL_256, md5($cryptKey), $q, MCRYPT_MODE_CBC, md5(md5($cryptKey))));
        return( $qEncoded );
    }

////////////////////********  General Function End *******//////////////////////////

}

