<div class="row">
	<div class="col-md-2">
		<ul class="ver-inline-menu tabbable margin-bottom-10">

			<li class="active">
				<a data-toggle="tab" href="#add_consult">
					<i class="fa fa-plus"></i>Add Consult</a>
				<span class="after"> </span>
			</li>

			<li>
				<a data-toggle="tab" href="#consult_text_list">
					<i class="fa fa-comments"></i>Text</a>
				<span class="after"> </span>
			</li>
			<li class="">
				<a data-toggle="tab" href="#consult_audio_list">
					<i class="fa fa-microphone-slash"></i>Audio</a>
				<span class="after"> </span>
			</li>
			<li class="">
				<a data-toggle="tab" href="#consult_video_list">
					<i class="fa fa-video-camera"></i>Video</a>
				<span class="after"> </span>
			</li>
			
		</ul>
	</div>
	<div class="col-md-6">
		<div class="tab-content">

			<div id="add_consult" class="tab-pane active">
				<div class="portlet box green">
					<div class="portlet-title">
						<div class="caption">
							<i class="fa fa-briefcase"></i><?=$this->lang->line('package_label');?></div>
						<div class="tools">
							<a href="javascript:;" class="collapse" data-original-title="" title=""> </a>
							
							<a href="javascript:;" class="remove" data-original-title="" title=""> </a>
						</div>
					</div>
					<div class="portlet-body">
					   <form role="form" action="admin/add_package_post/<?=$personal_info[0]['username'];?>" method="post" enctype="multipart/form-data">
							
							<input type="hidden" name="doctor_id" value="<?=$personal_info[0]['id'];?>">

							<div class="form-group">
								<label class="control-label"><?=$this->lang->line('package_label').' '.$this->lang->line('name_tag_label');?></label>
								<input required="" type="text" id="typeahead_example_modal_1" placeholder="<?=$this->lang->line('package_label').' '.$this->lang->line('name_tag_label');?>" value="" name="package_title" class="form-control" /> 
							</div>

							<div class="form-group row">
								<div class="col-md-6"  id="package_format_div">
									<label class="control-label"><?=$this->lang->line('package_label').' '.$this->lang->line('format_label');?></label>
									<select onchange="package_type_function()" required="" id="package_format" name="package_format" class="form-control select2" data-placeholder="<?=$this->lang->line('format_label');?>">
										<option value="">---</option>
										<option value="1">Text</option>
										<option value="2">Audio</option>
										<option value="3">Video</option>
									</select>
								</div>
								<div class="col-md-6">
									<label class="control-label"><?=$this->lang->line('package_label').' '.$this->lang->line('validity_label');?></label>
									<input required="" name="package_validity" id="touchspin_9" type="text" value="" class="form-control"> 
								</div>
							</div>
							<input type="hidden" name="package_type" value="0">

							
							
							<div class="form-group row">
								<div class="col-md-4" id="text_div" style="display: none" >
									<label class="control-label"><?=$this->lang->line('text_label').' '.$this->lang->line('number_label');?></label>
									<input name="text" id="touchspin_5" type="text" value="0" class="form-control"> 
								</div>
								<div class="col-md-4" id="audio_div" style="display: none">
									<label class="control-label"><?=$this->lang->line('audio_label').' '.$this->lang->line('number_label');?></label>
									<input name="audio" id="touchspin_6" type="text" value="0" class="form-control"> 
								</div>
								<div class="col-md-4" id="video_div" style="display: none">
									<label class="control-label"><?=$this->lang->line('video_label').' '.$this->lang->line('number_label');?></label>
									<input name="video" id="touchspin_7" type="text" value="0" class="form-control"> 
								</div>
							</div>

							<div class="form-group row">
								<div class="col-md-6">
									<label class="control-label"><?=$this->lang->line('package_label').' '.$this->lang->line('price_label');?></label>
									<input required="" name="price" id="touchspin_2" type="text" value="0.00" class="form-control"> 
								</div>
								<div class="col-md-6">
									<label class="control-label"><?=$this->lang->line('package_label').' '.$this->lang->line('discount_label');?></label>
								<input name="discount" id="touchspin_1" type="text" value="0.00" class="form-control"> 
								</div>
							</div>

							


							<div class="form-actions">
                                <div class="row">
                                    <div class="col-md-offset-3 col-md-9">
                                        <button type="submit" class="btn green">
                                            <i class="fa fa-check"></i> <?=$this->lang->line('btn_submit_label');?>
                                        </button>
                                    </div>
                                </div>
                            </div>
							
						</form>
					</div>
				</div>
			</div>




		  
			<div id="consult_text_list" class="tab-pane">
				<div class="portlet box green">
					<div class="portlet-title">
						<div class="caption">
							<i class="fa fa-comments"></i><?=$this->lang->line('text_label').' '.$this->lang->line('consult_label');?></div>
						<div class="tools">
							<a href="javascript:;" class="collapse" data-original-title="" title=""> </a>
							
							<a href="javascript:;" class="remove" data-original-title="" title=""> </a>
						</div>
					</div>
					<div class="portlet-body">
						<div class="table-scrollable">
							
							<table class="table table-striped table-hover">
								<thead>
									<tr>
										<th>S.L</th>
										<th><?=$this->lang->line('patient_label').' '.$this->lang->line('name_tag_label');?></th>
										<th><?=$this->lang->line('package_label');?></th>
										<th><?=$this->lang->line('status_tag_label');?></th>
										<th><?=$this->lang->line('action_tag_label');?></th>
									</tr>
								</thead>
								<tbody>
									<?php foreach ($consult_text_list as $key => $value) { ?>
									<tr>
										<td><?=($key+1);?></td>
										<td><?=$value['patient_name'];?></td>
										<td><?=$value['package_title'];?></td>
										<td><?php if($value['chat_active']==0)
										{	
											echo '<span class="badge badge-warning">Inactive</span>';
										}
										else
										{
											echo '<span class="badge badge-primary">Active</span>';
										}
										?></td>
							 
										<td>
											<a href="javascript:0;" onclick="get_text_consult_ajax('<?=$value['group_id'];?>')" class="btn green tooltips btn-xs" data-container="body" data-placement="top" data-original-title="View and Edit">
												<?=$this->lang->line('details_tag_label');?>
											</a>
										</td>
									</tr>
									<?php } ?>
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>

			
		</div>
	</div>
	<div class="col-lg-4 col-xs-12 col-sm-12">
		<!-- BEGIN PORTLET-->
		<div class="portlet box green">
			<div class="portlet-title">
						<div class="caption">
							<i class="fa fa-comments"></i><?=$this->lang->line('consult_label').' '.$this->lang->line('details_tag_label');?></div>
						<div class="tools">
							<a href="javascript:;" class="collapse" data-original-title="" title=""> </a>
							
							<a href="javascript:;" class="remove" data-original-title="" title=""> </a>
						</div>
					</div>
			<div class="portlet-body" id="chats">
				<div id="consult_loading_img" style="display:none;position: relative;z-index: 10;top: 20px;left: 48%;width: 20px;">
		           <img style="width: 100%" src="back_assets/loading-spinner-blue.gif">     
		        </div>
				<div class="scroller" id="consult_text_details" style="height: 450px;" data-always-visible="1" data-rail-visible1="1">
					
				</div>
			</div>
		</div>
		<!-- END PORTLET-->
	</div>

	</div>



<script type="text/javascript">
            var ComponentsTypeahead = function () {

            var handleTwitterTypeahead = function() {
            }

            var handleTwitterTypeaheadModal = function() {

                // Example #1
                // instantiate the bloodhound suggestion engine
                var numbers = new Bloodhound({
                  datumTokenizer: function(d) { return Bloodhound.tokenizers.whitespace(d.num); },
                  queryTokenizer: Bloodhound.tokenizers.whitespace,
                  local: [
                   <?php foreach ($doctor_package_list as $key => $row) { ?>
                    { num: '<?=$row['package_title'];?>' },
                    <?php } ?>
                    
                  ]
                });
                 
                // initialize the bloodhound suggestion engine
                numbers.initialize();
                 
                // instantiate the typeahead UI
                if (App.isRTL()) {
                  $('#typeahead_example_modal_1').attr("dir", "rtl");  
                }
                $('#typeahead_example_modal_1').typeahead(null, {
                  displayKey: 'num',
                  hint: (App.isRTL() ? false : true),
                  source: numbers.ttAdapter()
                });
            }

            return {
                //main function to initiate the module
                init: function () {
                    handleTwitterTypeahead();
                    handleTwitterTypeaheadModal();
                }
            };

            }();

            jQuery(document).ready(function() {    
               ComponentsTypeahead.init(); 
            });
    </script>
	