 <div class="row">
	<div class="col-md-2">
		<ul class="ver-inline-menu tabbable margin-bottom-10">
			<li class="active">
				<a data-toggle="tab" href="#package_list">
					<i class="fa fa-list"></i>Package List</a>
				<span class="after"> </span>
			</li>
			<li>
				<a data-toggle="tab" href="#add_package">
					<i class="fa fa-puzzle-piece"></i>Add Package</a>
			</li>
		   
		</ul>
	</div>
	<div class="col-md-10">
		<div class="tab-content">
		  
			<div id="package_list" class="tab-pane active">
				<div class="portlet box green">
					<div class="portlet-title">
						<div class="caption">
							<i class="fa fa-briefcase"></i><?=$this->lang->line('package_label');?></div>
						<div class="tools">
							<a href="javascript:;" class="collapse" data-original-title="" title=""> </a>
							
							<a href="javascript:;" class="remove" data-original-title="" title=""> </a>
						</div>
					</div>
					<div class="portlet-body">
						<div class="table-scrollable">
							
							<table class="table table-striped table-hover">
								<thead>
									<tr>
										<th>S.L</th>
										<th><?=$this->lang->line('package_label').' '.$this->lang->line('name_tag_label');?></th>
										<th><?=$this->lang->line('price_label');?></th>
										<th><?=$this->lang->line('discount_label');?></th>
										<!-- <th><?=$this->lang->line('type_label');?></th> -->
										<th><?=$this->lang->line('validity_label');?></th>
										<th><?=$this->lang->line('status_tag_label');?></th>
										<th><?=$this->lang->line('action_tag_label');?></th>
									</tr>
								</thead>
								<tbody>
									<?php foreach ($doctor_package_list as $key => $value) {?>
									<tr>
										<td><?=($key+1);?></td>
										<td><?=$value['package_title'];?></td>
										<td><?=$value['price'];?>&#x9f3;</td>
										<td><?=$value['discount'];?>%</td>
										<!-- <td><?php if($value['package_type']==0)
											{
												echo '<span class="badge badge-info">Regular</span>';
											}
											else
											{
												echo '<span class="badge badge-info">Combine</span>';
											} ?>
										</td> -->
							 			<td><?=$value['package_validity'];?> days</td>
							 			<td><?php if($value['status']==0)
											{
												echo '<span class="badge badge-warning">Inactive</span>';
												
											}
											if($value['status']==1)
											{
												echo '<span class="badge badge-success">Active</span>';

											} ?>
										</td>
										<td>
											<a href="javascript:;" onclick="view_package_details('<?=$value["pack_id"];?>','<?=$value["doctor_id"];?>')" class="btn btn-xs green tooltips" data-container="body" title="Details">
                                                        <i class="fa fa-external-link"></i>
                                                    </a>
										</td>
										
									</tr>
									<?php } ?>
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>

			<div id="add_package" class="tab-pane">
				<div class="portlet box green">
					<div class="portlet-title">
						<div class="caption">
							<i class="fa fa-briefcase"></i><?=$this->lang->line('package_label');?></div>
						<div class="tools">
							<a href="javascript:;" class="collapse" data-original-title="" title=""> </a>
							
							<a href="javascript:;" class="remove" data-original-title="" title=""> </a>
						</div>
					</div>
					<div class="portlet-body">
					   <form role="form" action="admin/add_package_post/<?=$personal_info[0]['username'];?>" method="post" enctype="multipart/form-data">
							<!-- <div class="form-group">
								<label class="control-label"><?=$this->lang->line('package_label').' '.$this->lang->line('type_label');?></label>
								<div class="input-group">
									<div class="icheck-inline">
										<label class="">
											<div class="iradio_square-blue" style="position: relative;">
												<input type="radio" name="package_type" checked="" value="0" class="icheck" id="dr_package_type_regular" data-radio="iradio_square-blue" style="position: absolute; opacity: 0;">
												<ins class="iCheck-helper" style="position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; background: rgb(255, 255, 255); border: 0px; opacity: 0;"> 
												</ins>
											</div> Regular
										</label>
										<label class="">
											<div class="iradio_square-blue" style="position: relative;">
												<input value="1" type="radio" name="package_type" class="icheck" id="dr_package_type_combine" data-radio="iradio_square-blue" style="position: absolute; opacity: 0;">
												<ins class="iCheck-helper" style="position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; background: rgb(255, 255, 255); border: 0px; opacity: 0;"> 
												</ins>
											</div> Combine
										</label>
										
									</div>
								</div>
							</div> -->
							<input type="hidden" name="package_type" value="1">
							<input type="hidden" name="doctor_id" value="<?=$personal_info[0]['id'];?>">

							<div class="form-group">
								<label class="control-label"><?=$this->lang->line('package_label').' '.$this->lang->line('name_tag_label');?></label>
								<input type="text" id="typeahead_example_modal_1" placeholder="<?=$this->lang->line('package_label').' '.$this->lang->line('name_tag_label');?>" value="" name="package_title" class="form-control" /> 
							</div>

							
							
							<div class="form-group row">
								<div class="col-md-4" id="text_div" >
									<label class="control-label"><?=$this->lang->line('text_label').' '.$this->lang->line('number_label');?></label>
									<input name="text" id="touchspin_5" type="text" value="0" class="form-control"> 
								</div>
								<div class="col-md-4" id="audio_div" >
									<label class="control-label"><?=$this->lang->line('audio_label').' '.$this->lang->line('number_label');?></label>
									<input name="audio" id="touchspin_6" type="text" value="0" class="form-control"> 
								</div>
								<div class="col-md-4" id="video_div" >
									<label class="control-label"><?=$this->lang->line('video_label').' '.$this->lang->line('number_label');?></label>
									<input name="video" id="touchspin_7" type="text" value="0" class="form-control"> 
								</div>
							</div>

							<div class="form-group row">
								<div class="col-md-6">
									<label class="control-label"><?=$this->lang->line('package_label').' '.$this->lang->line('price_label');?></label>
									<input name="price" id="touchspin_2" type="text" value="0.00" class="form-control"> 
								</div>
								<div class="col-md-6">
									<label class="control-label"><?=$this->lang->line('package_label').' '.$this->lang->line('discount_label');?></label>
								<input name="discount" id="touchspin_1" type="text" value="0.00" class="form-control"> 
								</div>
							</div>

							<div class="form-group row">
								<!-- <div class="col-md-6" id="package_format_div">
									<label class="control-label"><?=$this->lang->line('package_label').' '.$this->lang->line('format_label');?></label>
									<select onchange="package_type_function()" required="" id="package_format" name="package_format" class="form-control select2" data-placeholder="<?=$this->lang->line('format_label');?>">
										<option value="0">---</option>
										<option value="1">Text</option>
										<option value="2">Audio</option>
										<option value="3">Video</option>
									</select>
								</div> -->
								<div class="col-md-6">
									<label class="control-label"><?=$this->lang->line('package_label').' '.$this->lang->line('validity_label');?></label>
									<input name="package_validity" id="touchspin_9" type="text" value="" class="form-control"> 
								</div>
							</div>

							<div class="form-group" id="package_image_div" >
								<label class="control-label col-md-12 col-sm-12"><?=$this->lang->line('package_label').' '.$this->lang->line('img_tag_label');?></label>
								<div class="fileinput fileinput-new" data-provides="fileinput">
									<div class="fileinput-new thumbnail" style="width: 200px; height: 150px;">
									
										<img src="back_assets/img_default.png" alt="Your Image" />
										
									</div>

								   
									<div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 200px; max-height: 150px;"> </div>
									<div>
										<span class="btn default btn-file">
											<span class="fileinput-new"><?=$this->lang->line('select_label').' '.$this->lang->line('img_tag_label');?></span>
											<span class="fileinput-exists"> <?=$this->lang->line('change_label');?> </span>
											<input type="file" name="userfile"> </span>
										<a href="javascript:;" class="btn red fileinput-exists" data-dismiss="fileinput"> <?=$this->lang->line('remove_label');?> </a>
									</div>
									<!-- <div class="clearfix margin-top-10">
										<span class="label label-danger">NOTE!</span> <?=$this->lang->line('img_resolution_msg');?> (550 x 380)px </div> -->
								</div>
							</div>

							<div class="form-group">
                                <label class="control-label col-md-12">
                                <?=$this->lang->line('package_label').' '.$this->lang->line('details_tag_label');?>
                                    
                                </label>
                                <div class="col-md-12">
                                    
                                   <textarea name="package_description" id="summernote_1"> 
                                   </textarea>
                                </div>
                            </div>


							<div class="form-actions">
                                <div class="row">
                                    <div class="col-md-offset-3 col-md-9">
                                        <button type="submit" class="btn green">
                                            <i class="fa fa-check"></i> <?=$this->lang->line('btn_submit_label');?>
                                        </button>
                                    </div>
                                </div>
                            </div>
							
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

		<script type="text/javascript">
            var ComponentsTypeahead = function () {

            var handleTwitterTypeahead = function() {}

            var handleTwitterTypeaheadModal = function() {

                // Example #1
                // instantiate the bloodhound suggestion engine
                var numbers = new Bloodhound({
                  datumTokenizer: function(d) { return Bloodhound.tokenizers.whitespace(d.num); },
                  queryTokenizer: Bloodhound.tokenizers.whitespace,
                  local: [
                   <?php foreach ($doctor_package_list as $key => $row) { ?>
                    { num: '<?=$row['package_title'];?>' },
                    <?php } ?>
                    
                  ]
                });
                 
                // initialize the bloodhound suggestion engine
                numbers.initialize();
                 
                // instantiate the typeahead UI
                if (App.isRTL()) {
                  $('#typeahead_example_modal_1').attr("dir", "rtl");  
                }
                $('#typeahead_example_modal_1').typeahead(null, {
                  displayKey: 'num',
                  hint: (App.isRTL() ? false : true),
                  source: numbers.ttAdapter()
                });
            }


            var handleTwitterTypeaheadModal2 = function() {

                // Example #1
                // instantiate the bloodhound suggestion engine
                var numbers = new Bloodhound({
                  datumTokenizer: function(d) { return Bloodhound.tokenizers.whitespace(d.num); },
                  queryTokenizer: Bloodhound.tokenizers.whitespace,
                  local: [
                   <?php foreach ($doctor_package_list as $key => $row) { ?>
                    { num: '<?=$row['package_title'];?>' },
                    <?php } ?>
                    
                  ]
                });
                 
                // initialize the bloodhound suggestion engine
                numbers.initialize();
                 
                // instantiate the typeahead UI
                if (App.isRTL()) {
                  $('#typeahead_example_modal_dr').attr("dir", "rtl");  
                }
                $('#typeahead_example_modal_dr').typeahead(null, {
                  displayKey: 'num',
                  hint: (App.isRTL() ? false : true),
                  source: numbers.ttAdapter()
                });
            }

            return {
                //main function to initiate the module
                init: function () {
                    handleTwitterTypeahead();
                    handleTwitterTypeaheadModal();
                    handleTwitterTypeaheadModal2();
                }
            };

            }();

            jQuery(document).ready(function() {    
               ComponentsTypeahead.init(); 
            });
        </script>






