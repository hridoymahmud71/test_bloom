<?php $this->load->view('admin/headlink'); ?>
<style>
    .input-group-btn {
        display: none;
    }
    .delete_img
    {
        position: absolute;
        top: 20px;
        color: #444 !important;
        font-weight: 700;
        border: 1px solid #ccc;
        padding: 5px;
        right: 30px;
        background: white !important;
        transition: .5s !important;
    }
    .delete_img:hover
    {
        color: white !important;
        background: #ed6b75 !important;
        transition: .5s !important;
    }
</style>
    <body class="page-container-bg-solid page-header-fixed page-sidebar-closed-hide-logo">
        <!-- BEGIN HEADER -->
       <?php $this->load->view('admin/head_nav'); ?>
        <!-- END HEADER -->
        <!-- BEGIN HEADER & CONTENT DIVIDER -->
        <div class="clearfix"> </div>
        <!-- END HEADER & CONTENT DIVIDER -->
        <!-- BEGIN CONTAINER -->
        <div class="page-container">
            <!-- BEGIN SIDEBAR -->
            <div class="page-sidebar-wrapper">
                <!-- BEGIN SIDEBAR -->
              <?php $this->load->view('admin/left_nav');?>
                <!-- END SIDEBAR -->
            </div>
            <!-- END SIDEBAR -->
            <!-- BEGIN CONTENT -->
            <div class="page-content-wrapper">
                <!-- BEGIN CONTENT BODY -->
                <div class="page-content">
                    <!-- BEGIN PAGE HEAD-->
                    <div class="page-head">
                        <!-- BEGIN PAGE TITLE -->
                        <?php $this->load->view('admin/page_title');?>
                        <!-- END PAGE TITLE -->
                        <!-- BEGIN PAGE TOOLBAR -->
                      
                        <!-- END PAGE TOOLBAR -->
                    </div>
                    <!-- END PAGE HEAD-->
                    <!-- BEGIN PAGE BREADCRUMB -->
                   
                    <!-- END PAGE BREADCRUMB -->
                    <!-- BEGIN PAGE BASE CONTENT -->
                    
                    <div class="row">
                        <div class="col-md-12">
                            
                            <div class="portlet box green ">
                                <div class="portlet-title">
                                    <div class="caption">
                                        <i class="fa fa-newspaper-o"></i> <?=$this->lang->line('chamber_label').' '.$this->lang->line('btn_edit_label').' '.$this->lang->line('form_label');?></div>
                                    <div class="tools">
                                        <a href="javascript:;" class="collapse"> </a>
                                        
                                        <a href="javascript:;" class="remove"> </a>
                                    </div>
                                </div>
                                <div class="portlet-body form">
                                    <!-- BEGIN FORM-->
                                    <form  class="form-horizontal form-bordered form-row-stripped" action="admin/update_doctor_chamber_post/<?=$doctor_chamber_details[0]['id'];?>" method="post" enctype="multipart/form-data">
                                        <div class="form-body">

                                        <input type="hidden" value="<?=$doctor_name;?>" name="doctor_name">


                                            <div class="form-group">
                                                <label class="control-label col-md-3">
                                                <?=$this->lang->line('chamber_label').' '.$this->lang->line('type_label');?>
                                                    
                                                </label>
                                                <div class="col-md-9">
                                                    <div class="icheck-inline">
                                                        <label class="">
                                                            <div class="iradio_square-blue" style="position: relative;">
                                                                <input type="radio" name="is_personal" <?php if($doctor_chamber_details[0]['is_personal']==0){echo "checked";} ?> value="0" class="icheck"  data-radio="iradio_square-blue" style="position: absolute; opacity: 0;">
                                                                <ins class="iCheck-helper" style="position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; background: rgb(255, 255, 255); border: 0px; opacity: 0;"> 
                                                                </ins>
                                                            </div> General
                                                        </label>
                                                        <label class="">
                                                            <div class="iradio_square-blue" style="position: relative;">
                                                                <input value="1" type="radio" name="is_personal"  <?php if($doctor_chamber_details[0]['is_personal']==1){echo "checked";} ?> class="icheck"  data-radio="iradio_square-blue" style="position: absolute; opacity: 0;">
                                                                <ins class="iCheck-helper" style="position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; background: rgb(255, 255, 255); border: 0px; opacity: 0;"> 
                                                                </ins>
                                                            </div> Personal
                                                        </label>
                                                        
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label class="control-label col-md-3">
                                                <?=$this->lang->line('chamber_label').' '.$this->lang->line('name_tag_label');?>
                                                    
                                                </label>
                                                <div class="col-md-9">
                                                    <select id="chamber_id" name="chamber_id"  class="form-control select2" data-placeholder="<?=$this->lang->line('choose_msg').' '.$this->lang->line('chamber_label');?>">
                                                        <option></option>
                                                        <?php foreach ($chamber_list as $key => $row) { ?>
                                                            <option <?php if($row['reg_id']==$doctor_chamber_details[0]['chamber_id']){echo "selected";} ?> value="<?=$row['reg_id'];?>">
                                                                <?=$row['name'].'&nbsp;&nbsp;'.$row['area_title'].','.$row['district_name'].','.$row['division_name'];?></h5>
                                                            </option>
                                                        <?php } ?>
                                                    </select>
                                                </div>
                                            </div>

                                            <div class="form-group">
                                               <label class="control-label col-md-3">
                                               <?=$this->lang->line('chamber_label').' '.$this->lang->line('schedule_label');?>
                                                </label>
                                                <?php $exist_day= explode(',',$doctor_chamber_details[0]['day_list']);?>
                                                <div class="col-md-3">
                                                    <select  name="day_list[]" class="form-control select2-multiple" multiple data-placeholder="<?=$this->lang->line('choose_msg').' '.$this->lang->line('day_label');?>">
                                                        <option></option>
                                                        <option <?php if(in_array("Sat",$exist_day )){echo "selected";}?> value="Sat">Sat</option>
                                                        <option <?php if(in_array("Sun",$exist_day )){echo "selected";}?> value="Sun">Sun</option>
                                                        <option <?php if(in_array("Mon",$exist_day )){echo "selected";}?> value="Mon">Mon</option>
                                                        <option <?php if(in_array("Tue",$exist_day )){echo "selected";}?> value="Tue">Tue</option>
                                                        <option <?php if(in_array("Wed",$exist_day )){echo "selected";}?> value="Wed">Wed</option>
                                                        <option <?php if(in_array("Thu",$exist_day )){echo "selected";}?> value="Thu">Thu</option>
                                                        <option <?php if(in_array("Fri",$exist_day )){echo "selected";}?> value="Fri">Fri</option>
                                                    </select>
                                                </div>

                                                <div class="col-md-3">
                                                    <label class="col-md-5 time-label"><?=$this->lang->line('opening_time_label');?>
                                                        
                                                    </label>
                                                    <div class="input-group">
                                                        <input value="<?=$doctor_chamber_details[0]['start_time'];?>" name="start_time" type="text" class="form-control timepicker timepicker-no-seconds">
                                                        <span class="input-group-btn">
                                                            <button class="btn default" type="button">
                                                                <i class="fa fa-clock-o"></i>
                                                            </button>
                                                        </span>
                                                    </div>
                                                </div>

                                                <div class="col-md-3">
                                                    <label class="col-md-5 time-label"><?=$this->lang->line('closing_time_label');?>
                                                        
                                                    </label>
                                                    <div class="input-group">
                                                        <input value="<?=$doctor_chamber_details[0]['end_time'];?>" name="end_time" type="text" class="form-control timepicker timepicker-no-seconds">
                                                        <span class="input-group-btn">
                                                            <button class="btn default" type="button">
                                                                <i class="fa fa-clock-o"></i>
                                                            </button>
                                                        </span>
                                                    </div>
                                                </div>    
                                            </div>

                                            <div class="form-group">
                                                <label class="control-label col-md-3"> <?=$this->lang->line('chamber_label').' '.$this->lang->line('fee_label');?></label>
                                                <div class="col-md-4">
                                                    <input name="consult_fee" id="touchspin_2" type="text" value="<?=$doctor_chamber_details[0]['consult_fee'];?>" class="form-control"> 
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label class="control-label col-md-3"> <?=$this->lang->line('chamber_label').' '.$this->lang->line('phone_tag_label');?></label>
                                                <div class="col-md-4">
                                                    <input value="<?=$doctor_chamber_details[0]['phone_one'];?>" class="form-control" name="phone_one" placeholder="phone no 1" id="" type="text" />
                                                    
                                                </div>
                                                <div class="col-md-4">
                                                    <input value="<?=$doctor_chamber_details[0]['phone_two'];?>" class="form-control" name="phone_two" placeholder="phone no 2"  type="text" />
                                                    
                                                </div>
                                            </div>
                                                

                                           
                      


                                            
                                            
                                           <div id="my_div">
                                            <div class="form-group" id="photo_div_end">
                                                <label class="control-label col-md-3">
                                                <?=$this->lang->line('market_label').' '.$this->lang->line('gallery_tag_label');?>
                                                    
                                                </label>
                                                
                                                   <?php $img_gallery=explode(',', $doctor_chamber_details[0]['gallery_image']);
                                                
                                             $counter=0; foreach ($img_gallery as $key => $img) {
                                                if($img=='0' || $img==''){}else{
                                                ?>

                                                <div class="col-md-3">
                                                <div class="fileinput fileinput-new" data-provides="fileinput">
                                                    <div class="fileinput-new thumbnail" style="width: 200px; height: 150px;">
                                                   
                                                        <img src="uploads/chamber_gallery/<?=$img;?>" alt="Your Image" />
                                                        <a href="javascript:;" class="btn btn-xs red delete_img" onclick="delete_gallery_img('<?=$doctor_chamber_details[0]['id'];?>','<?=$img;?>')" ><i class="fa fa-trash"></i></a>
                                                      
                                                    </div>

                                                    <div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 200px; max-height: 150px;"> 
                                                    </div>
                                                    <!-- <div>
                                                        <span class="btn default btn-file">
                                                            <span class="fileinput-new"><?=$this->lang->line('select_label').' '.$this->lang->line('img_tag_label');?></span>
                                                            <span class="fileinput-exists"> <?=$this->lang->line('change_label');?> </span>
                                                            <input type="file" value="<?=$img;?>" name="userfile[]"> </span>
                                                        <a href="javascript:;" class="btn red fileinput-exists" data-dismiss="fileinput"> <?=$this->lang->line('remove_label');?> </a>
                                                    </div> -->
                                                    
                                                </div>
                                                </div>

                                               <?php if(($key+1)==3 || ($key+1)==6)
                                                { ?>
                                                    <label class="control-label col-md-3"></label>
                                                
                                           <?php } $counter++;} }?>
                                                <input type="hidden" id="pre_img_count" value="<?=$counter;?>">
                                                <div class="col-md-3" id="add_image_div">
                                                    <a href="javascript:;" onclick="add_image_div()">
                                                        <div class="fileinput-new thumbnail" style="width: 200px; height: 150px;">
                                                            <img src="back_assets/add_more.png" alt="Your Image" />
                                                        </div>
                                                    </a>
                                                </div>
                                            </div>
                                            </div>
                   
                                        </div>
                                        <div class="form-actions">
                                            <div class="row">
                                                <div class="col-md-offset-3 col-md-9">
                                                    <button type="submit" class="btn green">
                                                        <i class="fa fa-check"></i> <?=$this->lang->line('btn_submit_label');?></button>
                                                    
                                                </div>
                                            </div>
                                        </div>
                                    </form>
                                    <!-- END FORM-->
                                </div>
                            </div>
                
                        </div>
                    </div>
                    <!-- END PAGE BASE CONTENT -->
                </div>
                <!-- END CONTENT BODY -->
            </div>
            <!-- END CONTENT -->


            <!-- BEGIN Right SIDEBAR -->
            <!-- END Right SIDEBAR -->


        </div>
        <!-- END CONTAINER -->
        <!-- BEGIN FOOTER -->
       <?php $this->load->view('admin/footer'); ?>

      <?php $this->load->view('admin/footerlink'); ?>

    <script>
var pre_img_count=$("#pre_img_count").val();
var div_count=1+parseFloat(pre_img_count);

        function delete_gallery_img(chamber_id,img_name) 
        {
            $.ajax({
                url: "<?php echo site_url('admin/delete_gallery_img');?>",
                type: "post",
                data: {chamber_id:chamber_id,img_name:img_name},
                success: function(msg)
                {
                    // alert(msg);
                    $("#my_div").load(location.href + " #my_div");
                    pre_img_count=pre_img_count-1;
                   
                    div_count=1+parseFloat(pre_img_count);

                    //alert(pre_img_count);
                   //$("#sub_cat_id").html(msg);
                }      
            });  
        }


        function get_sub_category() 
        {
            var cat_id=$("#cat_id").val();

            $.ajax({
                url: "<?php echo site_url('market_admin/get_sub_category');?>",
                type: "post",
                data: {cat_id:cat_id},
                success: function(msg)
                {
                    // alert(msg);
                   $("#sub_cat_id").html(msg);
                }      
            });  
        }


        function get_district() 
        {
            var division_id=$("#division_id").val();

            $.ajax({
                url: "<?php echo site_url('market_admin/get_district');?>",
                type: "post",
                data: {division_id:division_id},
                success: function(msg)
                {
                    // alert(msg);
                   $("#district_id").html(msg);
                }      
            });  
        }



        function get_area() 
        {
            var district_id=$("#district_id").val();

            $.ajax({
                url: "<?php echo site_url('market_admin/get_area');?>",
                type: "post",
                data: {district_id:district_id},
                success: function(msg)
                {
                    //alert(msg);
                   $("#area_id").html(msg);
                   
                }      
            });  
        }
    </script>
      <!-- Add Product Image Div -->

    <script>
 

        var div_count=1+parseFloat(pre_img_count);
        //alert(div_count);
        function add_image_div() 
        {
           //alert(div_count);
           if(div_count==9)
           {
            $("#add_image_div").hide();
           }

           
              $("#add_image_div").before(
                '<div class="col-md-3">'
                +'<div class="fileinput fileinput-new" data-provides="fileinput">'
                    +'<div class="fileinput-new thumbnail" style="width: 200px; height: 150px;">'
                        +'<img src="back_assets/img_default.png" alt="Your Image" /> </div>'
                    +'<div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 200px; max-height: 150px;"> </div>'
                    +'<div>'
                        +'<span class="btn default btn-file">'
                            +'<span class="fileinput-new"><?=$this->lang->line('select_label').' '.$this->lang->line('img_tag_label');?></span>'
                            +'<span class="fileinput-exists"> <?=$this->lang->line('change_label');?> </span>'
                            +'<input type="file" name="userfile[]"> </span>'
                        +'<a href="javascript:;" class="btn red fileinput-exists" data-dismiss="fileinput"> <?=$this->lang->line('remove_label');?> </a>'
                    +'</div>'
                   
                +'</div>'
                +'</div>');


              if(div_count==3 || div_count==6)
            {
                $("#add_image_div").before('<label class="control-label col-md-3"></label>');
            }
              div_count++;

          }
    </script>

    <script>
        function update_market_schedule(id) 
        {
            var replace_day=$("#replace_day_"+id).val();
            var open_time=$("#open_time_"+id).val();
            var close_time=$("#close_time_"+id).val();
            if(replace_day=='')
            {
                replace_day=$("#present_day_"+id).val();
            }


            $.ajax({
                url: "<?php echo site_url('market_admin/update_market_schedule');?>",
                type: "post",
                data: {id:id,day:replace_day,open_time:open_time,close_time:close_time},
                success: function(msg)
                {
                    //alert(msg);
                   //$("#market_schedule").load(document.URL +  ' #market_schedule');
                   location.reload();
                   
                }      
            });  
        }
    </script>

    </body>

</html>