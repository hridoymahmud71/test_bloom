<div class="row profile-account">
	<div class="col-md-3">
		<ul class="ver-inline-menu tabbable margin-bottom-10">
			<li class="active">
				<a data-toggle="tab" href="#tab_1-1">
					<i class="fa fa-cog"></i> Personal info </a>
				<span class="after"> </span>
			</li>
			<li>
				<a data-toggle="tab" href="#tab_2-2">
					<i class="fa fa-picture-o"></i> Change Image </a>
			</li>
			<li>
				<a data-toggle="tab" href="#tab_3-3">
					<i class="fa fa-lock"></i> Change Password </a>
			</li>
			<!-- <li>
				<a data-toggle="tab" href="#tab_4-4">
					<i class="fa fa-eye"></i> Privacity Settings </a>
			</li> -->
		</ul>
	</div>
	<div class="col-md-9">
		<div class="tab-content">
			<div id="tab_1-1" class="tab-pane active">
				<form role="form" action="#">
					<div class="form-group">
						<label class="control-label"><?=$this->lang->line('name_tag_label');?></label>
						<input type="text" placeholder="<?=$this->lang->line('name_tag_label');?>" value="<?=$personal_info[0]['name'];?>" class="form-control" /> 
					</div>
					<div class="form-group">
						<label class="control-label"><?=$this->lang->line('email_label');?></label>
						<input disabled="" type="text" placeholder="<?=$this->lang->line('email_label');?>" value="<?=$personal_info[0]['email'];?>" class="form-control" /> 
					</div>
					<div class="form-group">
						<label class="control-label"><?=$this->lang->line('phone_tag_label');?></label>
						<input type="text" placeholder="<?=$this->lang->line('phone_tag_label');?>" value="<?=$personal_info[0]['mobile_no'];?>" class="form-control" /> 
					</div>

					<div class="form-group">
						<label class="control-label"><?=$this->lang->line('phone_tag_label').' '.$this->lang->line('secondary_label');?></label>
						<input type="text" placeholder="<?=$this->lang->line('phone_tag_label');?>" value="<?=$personal_info[0]['phone_second'];?>" class="form-control" /> 
					</div>
					<div class="form-group">
						<label class="control-label"><?=$this->lang->line('dob_label');?></label>

						<div class="input-group input-medium date date-picker" data-date="$personal_info[0]['date_of_birth']" data-date-format="yyyy-mm-dd" data-date-viewmode="years">
						<input value="<?=date('d M Y', strtotime($personal_info[0]['date_of_birth']));?>" type="text" class="form-control" readonly="">
						<span class="input-group-btn">
						<button class="btn default" type="button">
							<i class="fa fa-calendar"></i>
						</button>
						</span>
						</div>
					</div>
					<div class="form-group">
						<label class="control-label"><?=$this->lang->line('nid_label');?></label>
						<input type="text" placeholder="<?=$this->lang->line('nid_label');?>" value="<?=$personal_info[0]['national_id'];?>" class="form-control" /> 
					</div>
					<div class="form-group">
						<label class="control-label"><?=$this->lang->line('reg_no_label');?></label>
						<input type="text" placeholder="<?=$this->lang->line('reg_no_label');?>" value="<?=$personal_info[0]['registration_number'];?>" class="form-control" /> 
					</div>
					<div class="form-group">
						<label class="control-label"><?=$this->lang->line('facebook_link_label');?></label>
						<input type="text" placeholder="<?=$this->lang->line('facebook_link_label');?>" value="<?=$personal_info[0]['facebook_link'];?>" class="form-control" /> 
					</div>
					<div class="form-group">
						<label class="control-label"><?=$this->lang->line('twitter_link_label');?></label>
						<input type="text" placeholder="<?=$this->lang->line('twitter_link_label');?>" value="<?=$personal_info[0]['twitter_link'];?>" class="form-control" /> 
					</div>
					<div class="form-group">
						<label class="control-label"><?=$this->lang->line('linked_link_label');?></label>
						<input type="text" placeholder="<?=$this->lang->line('linked_link_label');?>" value="<?=$personal_info[0]['linkedin_link'];?>" class="form-control" /> 
					</div>
					<div class="margiv-top-10">
						<a href="javascript:;" class="btn green"> Save Changes </a>
						<a href="javascript:;" class="btn default"> Cancel </a>
					</div>
				</form>
			</div>
			<div id="tab_2-2" class="tab-pane">
				
				<form action="admin/change_user_image" role="form">
					<div class="form-group">
						<div class="fileinput fileinput-new" data-provides="fileinput">
							<div class="fileinput-new thumbnail" style="width: 150px; height: 150px;">
								<img src="uploads/user/<?=$personal_info[0]['profile_image'];?>" alt="Your Image" /> </div>
								<div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 150px; max-height: 150px;"> </div>
							<div>
								<span class="btn default btn-file">
									<span class="fileinput-new"><?=$this->lang->line('select_label').' '.$this->lang->line('img_tag_label');?></span>
									<span class="fileinput-exists"> <?=$this->lang->line('change_label');?> </span>
									<input type="file" name="userfile"> 
								</span>
								<a href="javascript:;" class="btn default fileinput-exists" data-dismiss="fileinput"> Remove </a>
							</div>
						</div>
						 <div class="clearfix margin-top-10">
					<span class="label label-danger">NOTE!</span> <?=$this->lang->line('img_resolution_msg');?> (150 x 150)px </div>
					</div>
					<div class="margin-top-10">
						<button type="submit" class="btn green">
						<i class="fa fa-check"></i> <?=$this->lang->line('btn_submit_label');?></button>
					</div>
				</form>
			</div>
			<div id="tab_3-3" class="tab-pane">
				<form action="#">
					<div class="form-group">
						<label class="control-label">Current Password</label>
						<input type="password" class="form-control" /> </div>
					<div class="form-group">
						<label class="control-label">New Password</label>
						<input type="password" class="form-control" /> </div>
					<div class="form-group">
						<label class="control-label">Re-type New Password</label>
						<input type="password" class="form-control" /> </div>
					<div class="margin-top-10">
						<a href="javascript:;" class="btn green"> Change Password </a>
						<a href="javascript:;" class="btn default"> Cancel </a>
					</div>
				</form>
			</div>
			
		</div>
	</div>
	<!--end col-md-9-->
</div>