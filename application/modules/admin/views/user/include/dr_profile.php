<div class="row">
	<div class="col-md-2">
		<ul class="ver-inline-menu tabbable margin-bottom-10">
			<li class="active">
				<a data-toggle="tab" href="#education">
					<i class="fa fa-graduation-cap"></i> Education</a>
				<span class="after"> </span>
			</li>
			<li>
				<a data-toggle="tab" href="#specialize">
					<i class="fa fa-puzzle-piece"></i>Specialize.</a>
			</li>
			<li>
				<a data-toggle="tab" href="#experience">
					<i class="fa fa-cogs"></i>Experience</a>
			</li>
			<li>
				<a data-toggle="tab" href="#chamber">
					<i class="fa fa-briefcase"></i>Chamber</a>
			</li>
			<li>
				<a data-toggle="tab" href="#membership">
					<i class="fa fa-group"></i>Membership</a>
			</li>
			<li>
				<a data-toggle="tab" href="#award">
					<i class="fa fa-trophy"></i>Award</a>
			</li>
			
		   
		</ul>
	</div>
	<div class="col-md-10">
		<div class="tab-content">
			<div id="education" class="tab-pane active">
				<div class="portlet box green">
					<div class="portlet-title">
						<div class="caption">
							<i class="fa fa-graduation-cap"></i><?=$this->lang->line('education_label');?>
							<span class="label label-primary" id="edu_msg"></span>
						</div>
						<div class="tools">
							<a href="javascript:;" class="collapse" data-original-title="" title=""> </a>
							
							<a href="javascript:;" class="remove" data-original-title="" title=""> </a>
						</div>
					</div>
					<div class="portlet-body">
						<div class="table-scrollable">
						<form action="admin/add_dr_education/<?=$user_id;?>" method="post">
							<table class="table table-striped table-hover">
								<thead>
									<tr>
										<th>S.L</th>
										<th><?=$this->lang->line('Institution_label').' '.$this->lang->line('name_tag_label');?></th>
										<th><?=$this->lang->line('degree_label');?></th>
										<th><?=$this->lang->line('passing_label').' '.$this->lang->line('year_label');?></th>
										<th><?=$this->lang->line('action_tag_label');?></th>
									</tr>
								</thead>
								<tbody>
									<?php foreach ($doctor_education as $key => $row) { ?>
									<tr>
										<td><?=($key+1);?> </td>
										<td> 
											<select id="inst_id<?=$row['id'];?>" class="form-control select2" data-placeholder="<?=$this->lang->line('choose_msg').' '.$this->lang->line('institution_label');?>">
												<option></option>
												<?php foreach ($hospital_list as $key => $value) { ?>
												<option <?php if($row['inst_id']==$value['reg_id']){echo "selected";} ?> value="<?=$value['reg_id']?>">
													<?=$value['name']?>
												</option>
												<?php } ?>
											</select> 
										</td>
										<td>
											<select  id="degree_id<?=$row['id'];?>" class="form-control select2" data-placeholder="<?=$this->lang->line('choose_msg').' '.$this->lang->line('degree_label');?>">
											<option></option>
											<?php foreach ($degree_list as $key => $value) { ?>
												<option <?php if($row['degree_id']==$value['id']){echo "selected";} ?> value="<?=$value['id']?>">
													<?=$value['deg_name']?>
												</option>
												<?php } ?>

											</select> 
										</td>
										<td>
											<div style="width: 140px !important;" class="input-group input-medium date date-picker" data-date="<?=date('Y-m-d');?>" data-date-format="yyyy-mm-dd" data-date-viewmode="years">
												<input id="passing_year<?=$row['id'];?>" value="<?=$row['passing_year'];?>" type="text" class="form-control" readonly="">
												<span class="input-group-btn">
												<button class="btn default" type="button">
													<i class="fa fa-calendar"></i>
												</button>
												</span>
											</div>
										</td>
										<td>
											<a title="Update" href="javascript:0;" onclick="update_dr_education('<?=$row["id"];?>')" class="btn btn-xs green" data-container="body">
												<i class="fa fa-pencil-square-o"></i>
											</a>

											<a href="admin/delete_doctor_education/<?=$username;?>/<?=$row["id"];?>" class="btn btn-xs red" data-container="body" title="Delete">
												<i class="fa fa-trash"></i>
											</a>
										</td>
									</tr>

									<?php } ?>

									<tr>
										<input type="hidden" value="<?=$username;?>" name="username">
										<td> <i class="fa fa-plus"></i> </td>
										<td> 
											<select required="" name="inst_id" class="form-control select2" data-placeholder="<?=$this->lang->line('choose_msg').' '.$this->lang->line('institution_label');?>">
												<option></option>
												<?php foreach ($hospital_list as $key => $value) { ?>
												<option value="<?=$value['reg_id']?>">
													<?=$value['name']?>
												</option>
												<?php } ?>
											</select> 
										</td>
										<td>
											<select required="" name="degree_id" class="form-control select2" data-placeholder="<?=$this->lang->line('choose_msg').' '.$this->lang->line('degree_label');?>">
											<option></option>
											<?php foreach ($degree_list as $key => $value) { ?>
												<option value="<?=$value['id']?>">
													<?=$value['deg_name']?>
												</option>
												<?php } ?>

											</select> 
										</td>
										<td>
											<div style="width: 140px !important;" class="input-group input-medium date date-picker" data-date="<?=date('Y-m-d');?>" data-date-format="yyyy-mm-dd" data-date-viewmode="years">
												<input name="passing_year" type="text" class="form-control" readonly="">
												<span class="input-group-btn">
												<button class="btn default" type="button">
													<i class="fa fa-calendar"></i>
												</button>
												</span>
											</div>
										</td>
										<td>
											<button type="submit" class="btn btn-sm blue tooltips" data-container="body" data-placement="top" data-original-title="Update">
												<?=$this->lang->line('btn_add_label');?>
											</button>
										</td>
									</tr>
									
								</tbody>
							</table>
							</form>
						</div>
					</div>
				</div>
			</div>
			<div id="specialize" class="tab-pane">
				<div class="portlet box green">
					<div class="portlet-title">
						<div class="caption">
							<i class="fa fa-puzzle-piece"></i><?=$this->lang->line('specialization_label');?></div>
						<div class="tools">
							<a href="javascript:;" class="collapse" data-original-title="" title=""> </a>
							
							<a href="javascript:;" class="remove" data-original-title="" title=""> </a>
						</div>
					</div>
					<div class="portlet-body">
						<div class="table-scrollable">
						<form action="admin/add_dr_new_specialize/<?=$user_id;?>" method="post">
							<table class="table table-striped table-hover">
								<thead>
									<tr>
										<th>S.L</th>
										<th><?=$this->lang->line('specialization_label').' '.$this->lang->line('name_tag_label');?></th>
										<th><?=$this->lang->line('action_tag_label');?></th>
									</tr>
								</thead>
								<tbody>
									<?php $doctor_specialize=explode(',', $doctor_specialize[0]['specialist']);?>
									<?php foreach ($doctor_specialize as $key => $value) {?>
									<tr>
										<td><?=($key+1);?></td>
										<td> 

											<select required="" name="country_code" class="form-control select2" data-placeholder="<?=$this->lang->line('choose_msg').' '.$this->lang->line('specialization_label');?>">
												<?php foreach ($specialization_list as $key => $row) {
												if($value==$row['id'])	{?>
												<option id="<?=$row['id'];?>"><?=$row['sp_name'];?></option>
												<?php } } ?>
											</select> 
										</td>
										
							 
										<td>
											<a href="admin/delete_dr_specialize/" class="btn red tooltips" data-container="body" data-placement="top" data-original-title="Delete"><i class="fa fa-trash"></i></a>
										</td>
									</tr>
									<?php } ?>
								
									<tr>
										<td><i class="fa fa-plus-square text-primary"></i></td>
										<input type="hidden" name="username" value="<?=$username;?>">
										<td> 
											<select onchange="add_specialize()" id="specialize_id" required="" name="specialist" class="form-control select2" data-placeholder="<?=$this->lang->line('choose_msg').' '.$this->lang->line('specialization_label');?>">
												<option value=""></option>
												<?php foreach ($specialization_list as $key => $value) {?>
												<option value="<?=$value['id'];?>"><?=$value['sp_name'];?></option>
												<?php } ?>
												<option value="add_new">Add New</option>
											</select> 
										</td>
										
							 
										<td>
											<button type="submit" class="btn blue tooltips" data-container="body" data-placement="top" data-original-title="Add New Specialization"</button>
												<?=$this->lang->line('btn_add_label');?>
											</a>
										</td>
									</tr>
								</tbody>
							</table>
							</form>  
						</div>
					</div>
				</div>
			</div>
			<div id="experience" class="tab-pane">
				<div class="portlet box green">
					<div class="portlet-title">
						<div class="caption">
							<i class="fa fa-puzzle-piece"></i><?=$this->lang->line('experience_label');?>

							<span class="text text-primary" id="exp_msg"></span>
						</div>
						<div class="tools">
							<a href="javascript:;" class="collapse" data-original-title="" title=""> </a>
							
							<a href="javascript:;" class="remove" data-original-title="" title=""> </a>
						</div>

					</div>
					<div class="portlet-body">
						<div class="table-scrollable">
						<form action="admin/add_doctor_experience_post/<?=$user_id;?>" method="post">

							<table class="table table-striped table-hover">
								<thead>
									<tr>
										<th>S.L</th>
										<th style="width: 100px !important"><?=$this->lang->line('designation_label');?></th>
										<th><?=$this->lang->line('hospital_label').' '.$this->lang->line('name_tag_label');?></th>
										
										<th>From</th>
										<th>To</th>
										<th><?=$this->lang->line('action_tag_label');?></th>
									</tr>
								</thead>
								<tbody>
									<?php foreach ($doctor_experience as $key => $value) {?>
									<tr>
										<td><?=($key+1);?></td>
										<td>
											<select id="designation_id_<?=$value["exp_id"];?>" required="" class="form-control select2" data-placeholder="<?=$this->lang->line('select_label').' '.$this->lang->line('designation_label');?>">
												<?php foreach ($designation_list as $key => $row) {?>
												<option <?php if($value['designation_id']==$row['id']){echo "selected";}?> value="<?=$row['id'];?>"><?=$row['designation_name'];?></option>
												<?php } ?>
											</select> 
										</td>
										<input type="hidden" name="username" value="<?=$username;?>">
										<td > 
											<select id="hospital_id_<?=$value["exp_id"];?>" required="" class="form-control select2" data-placeholder="<?=$this->lang->line('select_label').' '.$this->lang->line('hospital_label');?>">
												<?php foreach ($hospital_list as $key => $row) {?>
												<option <?php if($value['hospital_id']==$row['reg_id']){echo "selected";}?> value="<?=$row['reg_id'];?>"><?=$row['name'];?></option>
												<?php } ?>
											</select> 
										</td>
										
										<td>
											<div style="width: 142px !important" class="input-group input-medium date date-picker" data-date="<?=$value['date_from'];?>" data-date-format="yyyy-mm-dd" data-date-viewmode="years" data-date-minviewmode="months">
												<input id="date_from_<?=$value["exp_id"];?>" type="text" class="form-control" value="<?=$value['date_from'];?>" readonly="">
												<span class="input-group-btn">
													<button class="btn default" type="button">
														<i class="fa fa-calendar"></i>
													</button>
												</span>
											</div>
										</td>

										<td>
											<div style="width: 141px !important" class="input-group input-medium date date-picker" data-date="<?=$value['date_to'];?>" data-date-format="yyyy-mm-dd" data-date-viewmode="years" data-date-minviewmode="months">
												<input id="date_to_<?=$value["exp_id"];?>" type="text" value="<?=$value['date_to'];?>" class="form-control" readonly="">
												<span class="input-group-btn">
													<button class="btn default" type="button">
														<i class="fa fa-calendar"></i>
													</button>
												</span>
											</div>
										</td>
							 
										<td>
											<a href="javascript:0;" onclick="update_dr_experience('<?=$value["exp_id"];?>')" class="btn green tooltips" data-container="body" data-placement="top" data-original-title="Update">
												<?=$this->lang->line('btn_update_label');?>
											</a>
										</td>
									</tr>
									<?php } ?>
									<tr>
										<td><i class="fa fa-plus-square text-primary"></i> 
										</td>
										<td>
											<select name="designation_id" required="" class="form-control select2" data-placeholder="<?=$this->lang->line('select_label').' '.$this->lang->line('designation_label');?>">
												<?php foreach ($designation_list as $key => $value) {?>
												<option value="<?=$value['id'];?>"><?=$value['designation_name'];?></option>
												<?php } ?>
											</select> 
										</td>
										<td > 
											<select  name="hospital_id" required=""  class="form-control select2" data-placeholder="<?=$this->lang->line('select_label').' '.$this->lang->line('hospital_label');?>">
												<?php foreach ($hospital_list as $key => $row) {?>
												<option value="<?=$row['reg_id'];?>"><?=$row['name'];?></option>
												<?php } ?>
											</select> 
										</td>
										<td>
											<div style="width: auto !important" class="input-group input-medium date date-picker" data-date="<?=date('Y-m-d');?>" data-date-format="yyyy-mm-dd" data-date-viewmode="years" data-date-minviewmode="months">
												<input name="date_from" type="text" class="form-control" readonly="">
												<span class="input-group-btn">
													<button class="btn default" type="button">
														<i class="fa fa-calendar"></i>
													</button>
												</span>
											</div>
										</td>

										<td>
											<div style="width: auto !important" class="input-group input-medium date date-picker" data-date="<?=date('Y-m-d');?>" data-date-format="yyyy-mm-dd" data-date-viewmode="years" data-date-minviewmode="months">
												<input name="date_to" type="text" class="form-control" readonly="">
												<span class="input-group-btn">
													<button class="btn default" type="button">
														<i class="fa fa-calendar"></i>
													</button>
												</span>
											</div>
										</td>
							 
							 
										<td>
											<button type="submit" class="btn blue tooltips" data-container="body" data-placement="top" data-original-title="Add New Experience">
												<?=$this->lang->line('btn_add_label');?>
											</button>
										</td>
									</tr>
								  
								</tbody>
							</table>
						</form>  
						</div>
					</div>
				</div>
			</div>
			<div id="chamber" class="tab-pane">
				<div class="portlet box green">
					<div class="portlet-title">
						<div class="caption">
							<i class="fa fa-briefcase"></i><?=$this->lang->line('chamber_label');?></div>
						<div class="tools">
							<a href="javascript:;" class="collapse" data-original-title="" title=""> </a>
							
							<a href="javascript:;" class="remove" data-original-title="" title=""> </a>
						</div>
					</div>
					<div class="portlet-body">
						<div class="table-scrollable">
							<a href="admin/doctor_chamber/<?=$username;?>/<?=$user_id;?>" class="btn btn-primary btn-md"><i class="fa fa-plus-circle"></i> Add New</a>
							<table class="table table-striped table-hover">
								<thead>
									<tr>
										<th>S.L</th>
										<th><?=$this->lang->line('chamber_label').' '.$this->lang->line('name_tag_label');?></th>
										<th><?=$this->lang->line('day_label');?></th>
										<th><?=$this->lang->line('time_label');?></th>
										<th><?=$this->lang->line('action_tag_label');?></th>
									</tr>
								</thead>
								<tbody>
									<?php foreach ($chamber_list as $key => $value) {?>
									<tr>
										<td><?=($key+1)?></td>
										<td><?=$value['chamber_name'];?></td>
										<td><?=$value['day_list'];?></td>
										<td><?=date('h:i:a', strtotime($value['start_time'])).' - '.date('h:i:a', strtotime($value['end_time']));?></td>
							 
										<td>
											<a href="admin/chamber_details/<?=$username;?>/<?=$value['chamber_id'];?>" class="btn green tooltips btn-xs" data-container="body" data-placement="top" data-original-title="View and Edit">
												<?=$this->lang->line('details_tag_label');?>
											</a>
										</td>
									</tr>
									<?php } ?>
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>

			<div id="membership" class="tab-pane">
				<div id="accordion3" class="panel-group">
					<div class="panel panel-danger">
						<div class="panel-heading">
							<h4 class="panel-title">
								<a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion3" href="#accordion3_1"> 1. Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry ? </a>
							</h4>
						</div>
						<div id="accordion3_1" class="panel-collapse collapse in">
							<div class="panel-body">
								<p> Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt
									laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. </p>
								<p> Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt
									laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. </p>
								<p> Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica,
									craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt
									you probably haven't heard of them accusamus labore sustainable VHS. </p>
							</div>
						</div>
					</div>
					<div class="panel panel-primary">
						<div class="panel-heading">
							<h4 class="panel-title">
								<a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion3" href="#accordion3_2"> 2. Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry ? </a>
							</h4>
						</div>
						<div id="accordion3_2" class="panel-collapse collapse">
							<div class="panel-body"> Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Anim pariatur cliche reprehenderit,
								enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf
								moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente
								ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard of them accusamus labore sustainable
								VHS. </div>
						</div>
					</div>
					<div class="panel panel-default">
						<div class="panel-heading">
							<h4 class="panel-title">
								<a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion3" href="#accordion3_3"> 3. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor ? </a>
							</h4>
						</div>
						<div id="accordion3_3" class="panel-collapse collapse">
							<div class="panel-body"> Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Anim pariatur cliche reprehenderit,
								enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf
								moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente
								ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard of them accusamus labore sustainable
								VHS. </div>
						</div>
					</div>
					<div class="panel panel-default">
						<div class="panel-heading">
							<h4 class="panel-title">
								<a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion3" href="#accordion3_4"> 4. Wolf moon officia aute, non cupidatat skateboard dolor brunch ? </a>
							</h4>
						</div>
						<div id="accordion3_4" class="panel-collapse collapse">
							<div class="panel-body"> 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee
								nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat
								craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard of them accusamus labore sustainable VHS. </div>
						</div>
					</div>
					<div class="panel panel-default">
						<div class="panel-heading">
							<h4 class="panel-title">
								<a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion3" href="#accordion3_5"> 5. Leggings occaecat craft beer farm-to-table, raw denim aesthetic ? </a>
							</h4>
						</div>
						<div id="accordion3_5" class="panel-collapse collapse">
							<div class="panel-body"> 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee
								nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat
								craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard of them accusamus labore sustainable VHS. Food truck quinoa nesciunt laborum eiusmod. Brunch 3
								wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et </div>
						</div>
					</div>
					<div class="panel panel-default">
						<div class="panel-heading">
							<h4 class="panel-title">
								<a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion3" href="#accordion3_6"> 6. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth ? </a>
							</h4>
						</div>
						<div id="accordion3_6" class="panel-collapse collapse">
							<div class="panel-body"> 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee
								nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat
								craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard of them accusamus labore sustainable VHS. Food truck quinoa nesciunt laborum eiusmod. Brunch 3
								wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et </div>
						</div>
					</div>
					<div class="panel panel-default">
						<div class="panel-heading">
							<h4 class="panel-title">
								<a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion3" href="#accordion3_7"> 7. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft ? </a>
							</h4>
						</div>
						<div id="accordion3_7" class="panel-collapse collapse">
							<div class="panel-body"> 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee
								nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat
								craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard of them accusamus labore sustainable VHS. Food truck quinoa nesciunt laborum eiusmod. Brunch 3
								wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et </div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>




<script type="text/javascript">
    var ComponentsTypeahead = function () {

    var handleTwitterTypeahead = function() {}

    var handleTwitterTypeaheadModal = function() {

        // Example #1
        // instantiate the bloodhound suggestion engine
        var numbers = new Bloodhound({
          datumTokenizer: function(d) { return Bloodhound.tokenizers.whitespace(d.num); },
          queryTokenizer: Bloodhound.tokenizers.whitespace,
          local: [
           <?php foreach ($specialization_list as $key => $row) { ?>
            { num: '<?=$row['sp_name'];?>' },
            <?php } ?>
            
          ]
        });
         
        // initialize the bloodhound suggestion engine
        numbers.initialize();
         
        // instantiate the typeahead UI
        if (App.isRTL()) {
          $('#typeahead_example_modal_1').attr("dir", "rtl");  
        }
        $('#typeahead_example_modal_1').typeahead(null, {
          displayKey: 'num',
          hint: (App.isRTL() ? false : true),
          source: numbers.ttAdapter()
        });
    }


  
    return {
        //main function to initiate the module
        init: function () {
            handleTwitterTypeahead();
            handleTwitterTypeaheadModal();
        }
    };

    }();

    jQuery(document).ready(function() {    
       ComponentsTypeahead.init(); 
    });
</script>
