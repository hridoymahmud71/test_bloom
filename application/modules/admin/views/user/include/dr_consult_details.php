<ul class="chats">
	<?php $chat_current=$consult_text_details[0]['chat_from'];foreach ($consult_text_details as $key => $value) { ?>
	<?php 
	    $only_date_array=explode(' ', $value['c_date']);
	    $only_date=$only_date_array[0];
	?>
	<?php if($value['chat_from']==$chat_current){ ?>
	<li class="out">
		<img class="avatar" title="<?=$value['patient_name'];?>" alt="<?=$value['patient_name'];?>" src="uploads/user/<?=$value['patient_image'];?>" />
		<div class="message">
			<span class="arrow"> </span>
			<a href="javascript:;" class="name"><?=date('d M Y', strtotime($only_date));?> </a>
			<span class="datetime"> at <?=date('h:i a', strtotime($only_date_array[1]));?> </span>
			<span class="body"><?=$value['message'];?></span>
		</div>
	</li>
	<?php  } else { ?>
	<li class="in">
		<img class="avatar" title="<?=$value['doctor_name'];?>" alt="<?=$value['doctor_name'];?>" src="uploads/user/<?=$value['doctor_image'];?>" />
		<div class="message">
			<span class="arrow"> </span>
			<a href="javascript:;" class="name"><?=date('d M Y', strtotime($only_date));?></a>
			<span class="datetime"> at <?=date('h:i a', strtotime($only_date_array[1]));?> </span>
			<span class="body"><?=$value['message'];?></span>
		</div>
	</li>
   <?php } } ?>	
</ul>