<div class="modal-dialog modal-lg">
<form action="admin/update_package_post/<?=$package_details[0]['pack_id'];?>" method="post" class="form-horizontal form-row-seperated" enctype="multipart/form-data">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
            <h4 class="modal-title text text-primary"><?=$package_details[0]['package_title'];?></h4>
        </div>
        <div class="modal-body">
               <div class="form-group col-md-12">
                <label class="control-label"><?=$this->lang->line('package_label').' '.$this->lang->line('type_label');?></label>
                <div class="input-group">
                    <div class="icheck-inline">
                        <label class="">
                            <div class="iradio_square-blue" style="position: relative;">
                                <input type="radio" name="package_type" <?php if($package_details[0]['package_type']==0){echo "checked";}?> value="0" class="icheck" id="dr_package_type_regular" data-radio="iradio_square-blue" style="position: absolute; opacity: 0;">
                                <ins class="iCheck-helper" style="position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; background: rgb(255, 255, 255); border: 0px; opacity: 0;"> 
                                </ins>
                            </div> Regular
                        </label>
                        <label class="">
                            <div class="iradio_square-blue" style="position: relative;">
                                <input value="1" type="radio" <?php if($package_details[0]['package_type']==1){echo "checked";}?> name="package_type" class="icheck" id="dr_package_type_combine" data-radio="iradio_square-blue" style="position: absolute; opacity: 0;">
                                <ins class="iCheck-helper" style="position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; background: rgb(255, 255, 255); border: 0px; opacity: 0;"> 
                                </ins>
                            </div> Combine
                        </label>
                        
                    </div>
                </div>
            </div>
            <?php $package_info=$package_details[0]['package_amount'];

            $package_amount=explode(',', $package_info);
            //echo "<pre>"; print_r($package_amount);die();
            $text_amount=$package_amount[0];
            $audio_amount=$package_amount[1];
            $video_amount=$package_amount[2];
            ?>

            <input type="hidden" name="doctor_id" value="<?=$doctor_id;?>">
            <input type="hidden" name="username" value="<?=$username;?>">

            <div class="form-group col-md-12">
                <label class="control-label"><?=$this->lang->line('package_label').' '.$this->lang->line('name_tag_label');?></label>
                <input type="text" id="typeahead_example_modal_dr" placeholder="<?=$this->lang->line('package_label').' '.$this->lang->line('name_tag_label');?>" value="<?=$package_details[0]['package_title'];?>" name="package_title" class="form-control" /> 
            </div>

            <div class="form-group row">
                <div class="col-md-6" id="package_format_div">
                    <label class="control-label"><?=$this->lang->line('package_label').' '.$this->lang->line('format_label');?></label>
                    <select onchange="package_type_function()" required="" id="package_format" name="package_format" class="form-control select2" data-placeholder="<?=$this->lang->line('format_label');?>">
                        <option value="0">---</option>
                        <option <?php if($package_details[0]['package_type']==0){ if($text_amount!=0){echo "selected";}}?> value="1">Text</option>
                        <option <?php if($package_details[0]['package_type']==0){ if($audio_amount!=0){echo "selected";}}?> value="2">Audio</option>
                        <option <?php if($package_details[0]['package_type']==0){ if($video_amount!=0){echo "selected";}}?> value="3">Video</option>
                    </select>
                </div>
                <div class="col-md-6">
                    <label class="control-label"><?=$this->lang->line('package_label').' '.$this->lang->line('validity_label');?></label>
                    <input name="package_validity" id="days_modal" type="text" value="<?=$package_details[0]['package_validity'];?>" class="form-control"> 
                </div>
            </div>
            
            <div class="form-group row">
                <div class="col-md-4" id="text_div" >
                    <label class="control-label"><?=$this->lang->line('text_label').' '.$this->lang->line('number_label');?></label>
                    <input name="text" id="touchspin_12" type="text" value="<?=$text_amount;?>" class="form-control"> 
                </div>
                <div class="col-md-4" id="audio_div" >
                    <label class="control-label"><?=$this->lang->line('audio_label').' '.$this->lang->line('number_label');?></label>
                    <input name="audio" id="touchspin_13" type="text" value="<?=$audio_amount;?>" class="form-control"> 
                </div>
                <div class="col-md-4" id="video_div" >
                    <label class="control-label"><?=$this->lang->line('video_label').' '.$this->lang->line('number_label');?></label>
                    <input name="video" id="touchspin_14" type="text" value="<?=$video_amount;?>" class="form-control"> 
                </div>
            </div>

            <div class="form-group row">
                <div class="col-md-6">
                    <label class="control-label"><?=$this->lang->line('package_label').' '.$this->lang->line('price_label');?></label>
                    <input name="price" id="touchspin_16" type="text" value="<?=$package_details[0]['price'];?>" class="form-control"> 
                </div>
                <div class="col-md-6">
                    <label class="control-label"><?=$this->lang->line('package_label').' '.$this->lang->line('discount_label');?></label>
                <input name="discount" id="touchspin_15" type="text" value="<?=$package_details[0]['discount'];?>" class="form-control"> 
                </div>
            </div>

            

            <div class="form-group" style="padding: 15px;">
                <div class="fileinput fileinput-new" data-provides="fileinput">
                    <div class="fileinput-new thumbnail" style="width: 200px; height: 150px;">
                        
                        <?php if($package_details[0]['package_image']=='' || $package_details[0]['package_image']==null)
                        { ?>
                            <img src="back_assets/img_default.png" alt="Your Image" />
                        <?php } else{ ?>

                            <img src="uploads/package/<?=$package_details[0]['package_image'];?>" alt="Package Image" />
                        <?php } ?>
                    </div>
                    <div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 200px; max-height: 150px;"> 
                    </div>
                    <div>
                        <span class="btn default btn-file">
                            <span class="fileinput-new"><?=$this->lang->line('select_label').' '.$this->lang->line('img_tag_label');?></span>
                            <span class="fileinput-exists"> <?=$this->lang->line('change_label');?> </span>
                            <input type="file" name="userfile"> </span>
                        <a href="javascript:;" class="btn red fileinput-exists" data-dismiss="fileinput"> <?=$this->lang->line('remove_label');?> </a>
                    </div>
                </div>
            </div>
            
            <div class="form-group" style="padding: 15px;">     
            <label class="control-label"><?=$this->lang->line('package_label').' '.$this->lang->line('description_label');?></label>         
               <textarea name="package_description" id="summernote_2"> <?=$package_details[0]['package_description'];?>
               </textarea>
            </div>
            

        </div>
        <div class="modal-footer">
            <button type="button" class="btn grey-salsa btn-outline" data-dismiss="modal"><?=$this->lang->line('btn_close_label');?></button>
            <button type="submit" class="btn green">
                <i class="fa fa-check"></i><?=$this->lang->line('btn_update_label');?></button>
        </div>
    </div>
    
</form>


</div>