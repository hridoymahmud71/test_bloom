<?php $this->load->view('admin/headlink'); ?>
<style>
    .input-group-btn {
        display: none;
    }
    .input-group .select2-container--bootstrap {
        width: 12% !important;
    }
</style>

    <body class="page-container-bg-solid page-header-fixed page-sidebar-closed-hide-logo">
        <!-- BEGIN HEADER -->
       <?php $this->load->view('admin/head_nav'); ?>
        <!-- END HEADER -->
        <!-- BEGIN HEADER & CONTENT DIVIDER -->
        <div class="clearfix"> </div>
        <!-- END HEADER & CONTENT DIVIDER -->
        <!-- BEGIN CONTAINER -->
        <div class="page-container">
            <!-- BEGIN SIDEBAR -->
            <div class="page-sidebar-wrapper">
                <!-- BEGIN SIDEBAR -->
              <?php $this->load->view('admin/left_nav');?>
                <!-- END SIDEBAR -->
            </div>
            <!-- END SIDEBAR -->
            <!-- BEGIN CONTENT -->
            <div class="page-content-wrapper">
                <!-- BEGIN CONTENT BODY -->
                <div class="page-content">
                    <!-- BEGIN PAGE HEAD-->
                    <div class="page-head">
                        <!-- BEGIN PAGE TITLE -->
                        <?php $this->load->view('admin/page_title');?>
                        <!-- END PAGE TITLE -->
                        <!-- BEGIN PAGE TOOLBAR -->
                      
                        <!-- END PAGE TOOLBAR -->
                    </div>
                    <!-- END PAGE HEAD-->
                    <!-- BEGIN PAGE BREADCRUMB -->
                   
                    <!-- END PAGE BREADCRUMB -->
                    <!-- BEGIN PAGE BASE CONTENT -->
                    
                    <div class="row">
                        <div class="col-md-12">
                            
                            <div class="portlet box green ">
                                <div class="portlet-title">
                                    <div class="caption">
                                        <i class="<?=$form_icon;?>"></i> <?=$form_label;?></div>
                                    <div class="tools">
                                        <a href="javascript:;" class="collapse"> </a>
                                        
                                        <a href="javascript:;" class="remove"> </a>
                                    </div>
                                </div>
                                <div class="portlet-body form">
                                    <?php if($this->session->userdata('scc_alt')){ ?>
                                    <div class="alert alert-success alert-dismissable">
                                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>
                                        <a href="javascript:;" class="alert-link"><?=$this->session->userdata('scc_alt');?></a>
                                    </div>
                                    <?php } $this->session->unset_userdata('scc_alt');?>

                                    <?php if($this->session->userdata('err_alt')){ ?>
                                    <div class="alert alert-danger alert-dismissable">
                                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>
                                        <a href="javascript:;" class="alert-link"><?=$this->session->userdata('err_alt');?></a>
                                    </div>
                                    <?php } $this->session->unset_userdata('err_alt');?>

                                    <!-- BEGIN FORM-->
                                    <form onsubmit="return check_data()" class="form-horizontal form-bordered form-row-stripped" action="admin/add_user_post" method="post" enctype="multipart/form-data">
                                        <div class="form-body">


                                            <div class="form-group">
                                                <label class="control-label col-md-3">
                                                <?=$this->lang->line('name_tag_label');?>
                                                    
                                                </label>
                                                <div class="col-md-9">
                                                    <input value="<?php if($this->session->userdata('tmp_name')){echo $this->session->userdata('tmp_name');}?>" name="name" required="" type="text" class="form-control"> </div>
                                            </div>

                                            <div class="form-group">
                                                <label class="control-label col-md-3">
                                                <?=$this->lang->line('email_label');?>
                                                    
                                                </label>
                                                <div class="col-md-9">
                                                    <input value="<?php if($this->session->userdata('tmp_email')){echo $this->session->userdata('tmp_email');}?>" name="email" required="" type="email" class="form-control"> </div>
                                            </div>



                                            <div class="form-group">
                                                <label class="control-label col-md-3">
                                                    <?=$this->lang->line('phone_tag_label');?>
                                                        
                                                </label>
                                                <div class="col-md-9">
                                                    <div class="input-group">
                                                        <select onchange="get_sub_category()" required="" name="country_code" class="form-control select2" data-placeholder="<?=$this->lang->line('choose_msg').' '.$this->lang->line('code_label');?>">
                                                            <option></option>
                                                            
                                                                <option selected="" value="880">+880</option>
                                                            
                                                        </select>

                                                        <div class="col-md-10">
                                                        <input value="<?php if($this->session->userdata('tmp_mobile_no')){echo $this->session->userdata('tmp_mobile_no');}?>" name="mobile_no" id="mobile_no" required="" type="text" class="form-control">
                                                        <span class="label label-danger" id="mobile_err_msg"></span>
                                                         </div>
                                                    </div>
                                                    
                                                </div>
                                                
                                            </div>

                                            <div class="form-group">
                                                <label class="control-label col-md-3">
                                                <?=$this->lang->line('type_label');?>
                                                    
                                                </label>
                                                <div class="col-md-9">
                                                    <select onchange="get_sub_category()" required="" name="user_type" class="form-control select2" data-placeholder="<?=$this->lang->line('choose_msg').' '.$this->lang->line('type_label');?>">
                                                        <option></option>
                                                        <?php foreach ($user_type as $key => $value) { ?>
                                                        <option <?php if($this->session->userdata('tmp_user_type')){if($this->session->userdata('tmp_user_type')==$value['id']){echo "selected";}}?>  value="<?=$value['id'];?>"><?=$value['type_name'];?></option>
                                                        <?php } ?>
                                                    </select>
                                                </div>
                                            </div>         
                                            
                                            <div class="form-group">
                                                <label class="control-label col-md-3">
                                                <?=$this->lang->line('profile_tag_label').' '.$this->lang->line('img_tag_label');?>
                                                    
                                                </label>
                                                <div class="col-md-9">
                                                <div class="fileinput fileinput-new" data-provides="fileinput">
                                                    <div class="fileinput-new thumbnail" style="width: 150px; height: 150px;">
                                                    
                                                        <img src="back_assets/img_default.png" alt="Your Image" /> 
                                                    </div>
                                                    <div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 150px; max-height: 150px;"> </div>
                                                    <div>
                                                        <span class="btn default btn-file">
                                                            <span class="fileinput-new"><?=$this->lang->line('select_label').' '.$this->lang->line('img_tag_label');?></span>
                                                            <span class="fileinput-exists"> <?=$this->lang->line('change_label');?> </span>
                                                            <input type="file" name="userfile"> 
                                                        </span>
                                                        <a href="javascript:;" class="btn red fileinput-exists" data-dismiss="fileinput"> <?=$this->lang->line('remove_label');?> </a>
                                                    </div>
                                                    <div class="clearfix margin-top-10">
                                                        <span class="label label-danger">NOTE!</span> <?=$this->lang->line('img_resolution_msg');?> (150 x 150)px </div>
                                                </div>
                                                </div>
                                            </div>



                                            <div class="form-actions">
                                                <div class="row">
                                                    <div class="col-md-offset-3 col-md-9">
                                                        <button type="submit" class="btn green">
                                                            <i class="fa fa-check"></i> <?=$this->lang->line('btn_submit_label');?></button>
                                                        
                                                    </div>
                                                </div>
                                            </div>
                                    </form>
                                    <!-- END FORM-->
                                </div>
                            </div>
                
                        </div>
                    </div>
                    <!-- END PAGE BASE CONTENT -->
                </div>
                <!-- END CONTENT BODY -->
            </div>
            <!-- END CONTENT -->


            <!-- BEGIN Right SIDEBAR -->
            <!-- END Right SIDEBAR -->


        </div>
        <!-- END CONTAINER -->
        <!-- BEGIN FOOTER -->
       <?php $this->load->view('admin/footer'); ?>

      <?php $this->load->view('admin/footerlink'); ?>

    <script>
        function check_data() 
        {
            var ok=true;
            var mobile=$("#mobile_no").val();
            if(mobile.length>10 || mobile.length<10)
            {
                $("#mobile_err_msg").html('Invalid Mobile No');
                ok=false;
            }

            return ok;

           
        }

    </script>

    </body>

</html>