<?php $this->load->view('admin/headlink'); ?>
<link href="back_assets/pages/css/profile-2.min.css" rel="stylesheet" type="text/css" />
    <body class="page-container-bg-solid page-header-fixed page-sidebar-closed-hide-logo">
        <!-- BEGIN HEADER -->
       <?php $this->load->view('admin/head_nav'); ?>
        <!-- END HEADER -->
        <!-- BEGIN HEADER & CONTENT DIVIDER -->
        <div class="clearfix"> </div>
        <!-- END HEADER & CONTENT DIVIDER -->
        <!-- BEGIN CONTAINER -->
        <div class="page-container">
            <!-- BEGIN SIDEBAR -->
            <div class="page-sidebar-wrapper">
                <!-- BEGIN SIDEBAR -->
              <?php $this->load->view('admin/left_nav');?>
                <!-- END SIDEBAR -->
            </div>
            <!-- END SIDEBAR -->
            <!-- BEGIN CONTENT -->
            <div class="page-content-wrapper">
                <!-- BEGIN CONTENT BODY -->
                <div class="page-content">
                    <!-- BEGIN PAGE HEAD-->
                    <div class="page-head">
                        <!-- BEGIN PAGE TITLE -->
                        <?php $this->load->view('admin/page_title');?>
                        <!-- END PAGE TITLE -->
                        <!-- BEGIN PAGE TOOLBAR -->
                      
                        <!-- END PAGE TOOLBAR -->
                    </div>
                    <!-- END PAGE HEAD-->
                    <!-- BEGIN PAGE BREADCRUMB -->
                   
                    <!-- END PAGE BREADCRUMB -->
                    <!-- BEGIN PAGE BASE CONTENT -->
                    
                    <div class="row">
                        <div class="col-md-12">

                            <?php if($this->session->userdata('scc_alt')){ ?>
                            <div class="alert alert-success alert-dismissable">
                                <button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>
                                <a href="javascript:;" class="alert-link"><?=$this->session->userdata('scc_alt');?></a>
                            </div>
                            <?php } $this->session->unset_userdata('scc_alt');?>

                            <?php if($this->session->userdata('err_alt')){ ?>
                            <div class="alert alert-danger alert-dismissable">
                                <button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>
                                <a href="javascript:;" class="alert-link"><?=$this->session->userdata('err_alt');?></a>
                            </div>
                            <?php } $this->session->unset_userdata('err_alt');?>


                            <!-- BEGIN EXAMPLE TABLE PORTLET-->
                            <div class="profile">
                        <div class="tabbable-line tabbable-full-width">
                            <ul class="nav nav-tabs">
                                <li class="active">
                                    <a href="#overview" data-toggle="tab"> Overview </a>
                                </li>
                                <li>
                                    <a href="#account" data-toggle="tab" onclick="get_user_profile_tab_ajax('<?=$username;?>','<?=$personal_info[0]['id'];?>','account')"> Account </a>
                                </li>
                                <li>
                                    <a href="#dr_profile" data-toggle="tab" onclick="get_user_profile_tab_ajax('<?=$username;?>','<?=$personal_info[0]['id'];?>','dr_profile')">DR. Profile</a>
                                </li>
                                <li>
                                    <a href="#dr_package" data-toggle="tab" onclick="get_user_profile_tab_ajax('<?=$username;?>','<?=$personal_info[0]['id'];?>','dr_package')">Package</a>
                                </li>
                                <li>
                                    <a href="#dr_consult" data-toggle="tab" onclick="get_user_profile_tab_ajax('<?=$username;?>','<?=$personal_info[0]['id'];?>','dr_consult')">Consult</a>
                                </li>
                                <li>
                                    <a href="#extra" data-toggle="tab"> Extra </a>
                                </li>   
                            </ul>
                            <div class="tab-content">

                                <div id="tab_loading_img" style="display:none;position: relative;z-index: 10;top: 0px;left: 48%;width: 20px;">
                                   <img style="width: 100%" src="back_assets/loading-spinner-blue.gif">     
                                </div>
                                <div class="tab-pane active" id="overview">
                                    <div class="row">
                                        <div class="col-md-3">
                                            <ul class="list-unstyled profile-nav">
                                                <li>
                                                    <img style="width: 100%" src="uploads/user/<?=$personal_info[0]['profile_image'];?>" class="img-responsive pic-bordered" alt="" />
                                                    <!-- <a href="javascript:;" class="profile-edit"> edit </a> -->
                                                </li>
                                                <li>
                                                    <a href="javascript:;"> 
                                                    <i class="fa fa-calendar text-primary"></i>
                                                    <?= date('d M Y', strtotime($personal_info[0]['date_of_birth']));?>
                                                     </a>
                                                </li>
                                                <li>
                                                    <a href="javascript:;"> 
                                                    <?php $counter; for ($i=1; $i <= $personal_info[0]['avg_review']; $i++) { ?>
                                                    
                                                        <i class="fa fa-star text-primary"></i>

                                                    
                                                    <?php $counter=$i; } ?>

                                                    <?php if($personal_info[0]['avg_review']>$counter){ ?>
                                                    <i class="fa fa-star-half-o text-primary"></i>
                                                    <?php $counter++;} ?>

                                                    <?php for($i=5;$i>$counter;$i--){ ?>
                                                    <i class="fa fa-star text-default"></i>
                                                    <?php } ?>

                                                        <span> <?=$personal_info[0]['avg_review'];?> </span>
                                                    </a>
                                                </li>
                                                
                                            </ul>
                                        </div>
                                        <div class="col-md-9">
                                            <div class="row">
                                                <div class="col-md-8 profile-info">
                                                    <h1 class="font-green sbold uppercase"><?=$personal_info[0]['name'];?></h1>
                                                    <?php if($personal_info[0]['user_type']==3 || $personal_info[0]['user_type']==4){?>
                                                    <cite class="text text-info"><?=$doctor_details[0]['designation_name'];?></cite><br>
                                                    <cite class="text text-info"><?=$doctor_details[0]['name'];?></cite>

                                                    <?php } ?>
                                                    <ul class="list-unstyled">
                                                        <li>
                                                            <i class="fa fa-envelope"></i> <?=$personal_info[0]['email'];?>
                                                        </li>
                                                        <li>
                                                            <i class="fa fa-mobile"></i> <?=$personal_info[0]['mobile_no'];?>
                                                        </li>
                                                        <?php if($personal_info[0]['phone_second']!=''){?>
                                                        <li>
                                                            <i class="fa fa-phone"></i><?=$personal_info[0]['phone_second'];?> 
                                                        </li>
                                                        <?php } ?>
                                                        <?php if($personal_info[0]['facebook_link']!=''){?>
                                                        <li>
                                                            <i class="fa fa-facebook-square"></i> 
                                                            <a href="javascript:;"> <?=$personal_info[0]['facebook_link'];?> </a>
                                                        </li>
                                                        <?php } ?>
                                                        <?php if($personal_info[0]['twitter_link']!=''){?>
                                                        <li>
                                                            <i class="fa fa-twitter-square"></i> 
                                                            <a href="javascript:;"> <?=$personal_info[0]['twitter_link'];?> </a>
                                                        </li>
                                                        <?php } ?><?php if($personal_info[0]['linkedin_link']!=''){?>
                                                        <li>
                                                            <i class="fa fa-linkedin-square"></i> 
                                                            <a href="javascript:;"> <?=$personal_info[0]['linkedin_link'];?> </a>
                                                        </li>
                                                        <?php } ?>

                                                    </ul>

                                                    <?php if($personal_info[0]['user_type']==3 || $personal_info[0]['user_type']==4){?>
                                                        <ul class="list-unstyled">
                                                            <?php foreach ($chamber_info as $key => $ch_info) {?>
                                                            <li>
                                                                <i class="fa fa-hospital-o"></i> <?=$ch_info['name'];?><br>
                                                                <i class="fa fa-money"></i> <cite>Consult Fee: <?=$ch_info['consult_fee'];?>&#x9f3;</cite>
                                                            </li>
                                                            <?php } ?>
                                                        </ul>

                                                    <?php } ?>
                                                    
                                                    <ul class="list-unstyled">
                                                        <li>
                                                            <i class="fa fa-map-marker"></i> <?=$user_location[0]['area_title'].','.$user_location[0]['district_name'].','.$user_location[0]['division_name'];?> 
                                                        </li>
                                                        <li>
                                                            <i class="fa fa-globe"></i> <?=$user_location[0]['country_name'];?>
                                                        </li>
                                                       
                                                    </ul>
                                                </div>
                                                <!--end col-md-8-->
                                                <div class="col-md-4">
                                                    <div class="portlet sale-summary">
                                                        <div class="portlet-title">
                                                            <div class="caption font-red sbold"><?=$this->lang->line('summary_label');?></div>
                                                            <div class="tools">
                                                                <a class="reload" href="javascript:;"> </a>
                                                            </div>
                                                        </div>
                                                        <div class="portlet-body">
                                                            <ul class="list-unstyled">
                                                                <li>
                                                                    <span class="sale-info">
                                                                    <i class="fa fa-user-md"></i> <?=$this->lang->line('doctor_label');?>
                                                                        <i class="fa fa-img-up"></i>
                                                                    </span>
                                                                    <span class="sale-num"> 23 </span>
                                                                </li>
                                                                <li>
                                                                    <span class="sale-info"> <i class="fa fa-cogs"></i> <?=$this->lang->line('service_label');?>
                                                                        <i class="fa fa-img-down"></i>
                                                                    </span>
                                                                    <span class="sale-num"> 87 </span>
                                                                </li>
                                                                
                                                            </ul>
                                                        </div>
                                                    </div>
                                                </div>
                                                <!--end col-md-4-->
                                            </div>
                                            <!--end row-->
                                            <div class="tabbable-line tabbable-custom-profile">
                                                <ul class="nav nav-tabs">
                                                    <li class="active">
                                                        <a href="#latest_consult_list" data-toggle="tab"> Latest Consult List</a>
                                                    </li>
                                                    <li>
                                                        <a href="#latest_sold_package" data-toggle="tab"> Latest Sold Package</a>
                                                    </li>

                                                    <li>
                                                        <a href="#latest_appointment" data-toggle="tab"> Latest Appointment</a>
                                                    </li>
                                                </ul>
                                                <div class="tab-content">
                                                    <div class="tab-pane active" id="latest_consult_list">
                                                        <div class="portlet-body">
                                                            <table class="table table-striped table-bordered table-advance table-hover">
                                                                <thead>
                                                                    <tr>
                                                                        <th>
                                                                            <i class="fa fa-briefcase"></i> Company </th>
                                                                        <th class="hidden-xs">
                                                                            <i class="fa fa-question"></i> Descrition </th>
                                                                        <th>
                                                                            <i class="fa fa-bookmark"></i> Amount </th>
                                                                        <th> </th>
                                                                    </tr>
                                                                </thead>
                                                                <tbody>
                                                                    <tr>
                                                                        <td>
                                                                            <a href="javascript:;"> Pixel Ltd </a>
                                                                        </td>
                                                                        <td class="hidden-xs"> Server hardware purchase </td>
                                                                        <td> 52560.10$
                                                                            <span class="label label-success label-sm"> Paid </span>
                                                                        </td>
                                                                        <td>
                                                                            <a class="btn btn-sm grey-salsa btn-outline" href="javascript:;"> View </a>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>
                                                                            <a href="javascript:;"> Smart House </a>
                                                                        </td>
                                                                        <td class="hidden-xs"> Office furniture purchase </td>
                                                                        <td> 5760.00$
                                                                            <span class="label label-warning label-sm"> Pending </span>
                                                                        </td>
                                                                        <td>
                                                                            <a class="btn btn-sm grey-salsa btn-outline" href="javascript:;"> View </a>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>
                                                                            <a href="javascript:;"> FoodMaster Ltd </a>
                                                                        </td>
                                                                        <td class="hidden-xs"> Company Anual Dinner Catering </td>
                                                                        <td> 12400.00$
                                                                            <span class="label label-success label-sm"> Paid </span>
                                                                        </td>
                                                                        <td>
                                                                            <a class="btn btn-sm grey-salsa btn-outline" href="javascript:;"> View </a>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>
                                                                            <a href="javascript:;"> WaterPure Ltd </a>
                                                                        </td>
                                                                        <td class="hidden-xs"> Payment for Jan 2013 </td>
                                                                        <td> 610.50$
                                                                            <span class="label label-danger label-sm"> Overdue </span>
                                                                        </td>
                                                                        <td>
                                                                            <a class="btn btn-sm grey-salsa btn-outline" href="javascript:;"> View </a>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>
                                                                            <a href="javascript:;"> Pixel Ltd </a>
                                                                        </td>
                                                                        <td class="hidden-xs"> Server hardware purchase </td>
                                                                        <td> 52560.10$
                                                                            <span class="label label-success label-sm"> Paid </span>
                                                                        </td>
                                                                        <td>
                                                                            <a class="btn btn-sm grey-salsa btn-outline" href="javascript:;"> View </a>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>
                                                                            <a href="javascript:;"> Smart House </a>
                                                                        </td>
                                                                        <td class="hidden-xs"> Office furniture purchase </td>
                                                                        <td> 5760.00$
                                                                            <span class="label label-warning label-sm"> Pending </span>
                                                                        </td>
                                                                        <td>
                                                                            <a class="btn btn-sm grey-salsa btn-outline" href="javascript:;"> View </a>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>
                                                                            <a href="javascript:;"> FoodMaster Ltd </a>
                                                                        </td>
                                                                        <td class="hidden-xs"> Company Anual Dinner Catering </td>
                                                                        <td> 12400.00$
                                                                            <span class="label label-success label-sm"> Paid </span>
                                                                        </td>
                                                                        <td>
                                                                            <a class="btn btn-sm grey-salsa btn-outline" href="javascript:;"> View </a>
                                                                        </td>
                                                                    </tr>
                                                                </tbody>
                                                            </table>
                                                        </div>
                                                    </div>
                                                    <!--tab-pane-->
                                                    <div class="tab-pane" id="latest_sold_package">
                                                        <div class="tab-pane active" id="latest_sold_package">
                                                            <div class="scroller" data-height="290px" data-always-visible="1" data-rail-visible1="1">
                                                                <ul class="feeds">
                                                                    <li>
                                                                        <div class="col1">
                                                                            <div class="cont">
                                                                                <div class="cont-col1">
                                                                                    <div class="label label-success">
                                                                                        <i class="fa fa-bell-o"></i>
                                                                                    </div>
                                                                                </div>
                                                                                <div class="cont-col2">
                                                                                    <div class="desc"> You have 4 pending tasks.
                                                                                        <span class="label label-danger label-sm"> Take action
                                                                                            <i class="fa fa-share"></i>
                                                                                        </span>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <div class="col2">
                                                                            <div class="date"> Just now </div>
                                                                        </div>
                                                                    </li>

                                                                    <li>
                                                                        <div class="col1">
                                                                            <div class="cont">
                                                                                <div class="cont-col1">
                                                                                    <div class="label label-info">
                                                                                        <i class="fa fa-bullhorn"></i>
                                                                                    </div>
                                                                                </div>
                                                                                <div class="cont-col2">
                                                                                    <div class="desc"> New order received. Please take care of it. </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <div class="col2">
                                                                            <div class="date"> 22 hours </div>
                                                                        </div>
                                                                    </li>
                                                                </ul>
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="tab-pane" id="latest_appointment">
                                                        <div class="portlet-body">
                                                            <table class="table table-striped table-bordered table-advance table-hover">
                                                                <thead>
                                                                    <tr>
                                                                        <th>
                                                                            <i class="fa fa-briefcase"></i> Company </th>
                                                                        <th class="hidden-xs">
                                                                            <i class="fa fa-question"></i> Descrition </th>
                                                                        <th>
                                                                            <i class="fa fa-bookmark"></i> Amount </th>
                                                                        <th> </th>
                                                                    </tr>
                                                                </thead>
                                                                <tbody>
                                                                    <tr>
                                                                        <td>
                                                                            <a href="javascript:;"> Pixel Ltd </a>
                                                                        </td>
                                                                        <td class="hidden-xs"> Server hardware purchase </td>
                                                                        <td> 52560.10$
                                                                            <span class="label label-success label-sm"> Paid </span>
                                                                        </td>
                                                                        <td>
                                                                            <a class="btn btn-sm grey-salsa btn-outline" href="javascript:;"> View </a>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>
                                                                            <a href="javascript:;"> Smart House </a>
                                                                        </td>
                                                                        <td class="hidden-xs"> Office furniture purchase </td>
                                                                        <td> 5760.00$
                                                                            <span class="label label-warning label-sm"> Pending </span>
                                                                        </td>
                                                                        <td>
                                                                            <a class="btn btn-sm grey-salsa btn-outline" href="javascript:;"> View </a>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>
                                                                            <a href="javascript:;"> FoodMaster Ltd </a>
                                                                        </td>
                                                                        <td class="hidden-xs"> Company Anual Dinner Catering </td>
                                                                        <td> 12400.00$
                                                                            <span class="label label-success label-sm"> Paid </span>
                                                                        </td>
                                                                        <td>
                                                                            <a class="btn btn-sm grey-salsa btn-outline" href="javascript:;"> View </a>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>
                                                                            <a href="javascript:;"> WaterPure Ltd </a>
                                                                        </td>
                                                                        <td class="hidden-xs"> Payment for Jan 2013 </td>
                                                                        <td> 610.50$
                                                                            <span class="label label-danger label-sm"> Overdue </span>
                                                                        </td>
                                                                        <td>
                                                                            <a class="btn btn-sm grey-salsa btn-outline" href="javascript:;"> View </a>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>
                                                                            <a href="javascript:;"> Pixel Ltd </a>
                                                                        </td>
                                                                        <td class="hidden-xs"> Server hardware purchase </td>
                                                                        <td> 52560.10$
                                                                            <span class="label label-success label-sm"> Paid </span>
                                                                        </td>
                                                                        <td>
                                                                            <a class="btn btn-sm grey-salsa btn-outline" href="javascript:;"> View </a>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>
                                                                            <a href="javascript:;"> Smart House </a>
                                                                        </td>
                                                                        <td class="hidden-xs"> Office furniture purchase </td>
                                                                        <td> 5760.00$
                                                                            <span class="label label-warning label-sm"> Pending </span>
                                                                        </td>
                                                                        <td>
                                                                            <a class="btn btn-sm grey-salsa btn-outline" href="javascript:;"> View </a>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>
                                                                            <a href="javascript:;"> FoodMaster Ltd </a>
                                                                        </td>
                                                                        <td class="hidden-xs"> Company Anual Dinner Catering </td>
                                                                        <td> 12400.00$
                                                                            <span class="label label-success label-sm"> Paid </span>
                                                                        </td>
                                                                        <td>
                                                                            <a class="btn btn-sm grey-salsa btn-outline" href="javascript:;"> View </a>
                                                                        </td>
                                                                    </tr>
                                                                </tbody>
                                                            </table>
                                                        </div>
                                                    </div>
                                                    <!--tab-pane-->
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!--tab_1_2-->
                                <div class="tab-pane" id="account">
                                   
                                </div>
                                <!--end tab-pane-->
                                <div class="tab-pane" id="dr_profile">
                                    
                                </div>
                                <!--end tab-pane-->
                                <div class="tab-pane" id="dr_consult">
                                    
                                </div>

                                <div class="tab-pane" id="dr_package">
                                   
                                </div>

                                 <div class="tab-pane" id="extra">
                                    <div class="row">
                                        <div class="col-md-2">
                                            <ul class="ver-inline-menu tabbable margin-bottom-10">
                                                <li class="active">
                                                    <a data-toggle="tab" href="#tab_1">
                                                        <i class="fa fa-comments"></i> Consult</a>
                                                    <span class="after"> </span>
                                                </li>
                                                <li>
                                                    <a data-toggle="tab" href="#tab_1">
                                                        <i class="fa fa-briefcase"></i>Package</a>
                                                </li>
                                                <li>
                                                    <a data-toggle="tab" href="#tab_2">
                                                        <i class="fa fa-heartbeat"></i>Health Feed</a>
                                                </li>
                                                <li>
                                                    <a data-toggle="tab" href="#tab_2">
                                                        <i class="fa fa-star"></i>Review</a>
                                                </li>
                                                <li>
                                                    <a data-toggle="tab" href="#tab_3">
                                                        <i class="fa fa-file-image-o"></i>Gallery</a>
                                                </li>
                                                
                                               
                                            </ul>
                                        </div>
                                        <div class="col-md-10">
                                            <div class="tab-content">
                                                <div id="tab_1" class="tab-pane active">
                                                    <div id="accordion1" class="panel-group">
                                                        <div class="panel panel-default">
                                                            <div class="panel-heading">
                                                                <h4 class="panel-title">
                                                                    <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion1" href="#accordion1_1"> 1. Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry ? </a>
                                                                </h4>
                                                            </div>
                                                            <div id="accordion1_1" class="panel-collapse collapse in">
                                                                <div class="panel-body"> Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt
                                                                    laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes
                                                                    anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven't
                                                                    heard of them accusamus labore sustainable VHS. </div>
                                                            </div>
                                                        </div>
                                                        <div class="panel panel-default">
                                                            <div class="panel-heading">
                                                                <h4 class="panel-title">
                                                                    <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion1" href="#accordion1_2"> 2. Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry ? </a>
                                                                </h4>
                                                            </div>
                                                            <div id="accordion1_2" class="panel-collapse collapse">
                                                                <div class="panel-body"> Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Anim pariatur cliche reprehenderit,
                                                                    enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf
                                                                    moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente
                                                                    ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard of them accusamus labore sustainable
                                                                    VHS. </div>
                                                            </div>
                                                        </div>
                                                        <div class="panel panel-primary">
                                                            <div class="panel-heading">
                                                                <h4 class="panel-title">
                                                                    <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion1" href="#accordion1_3"> 3. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor ? </a>
                                                                </h4>
                                                            </div>
                                                            <div id="accordion1_3" class="panel-collapse collapse">
                                                                <div class="panel-body"> Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Anim pariatur cliche reprehenderit,
                                                                    enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf
                                                                    moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente
                                                                    ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard of them accusamus labore sustainable
                                                                    VHS. </div>
                                                            </div>
                                                        </div>
                                                        <div class="panel panel-warning">
                                                            <div class="panel-heading">
                                                                <h4 class="panel-title">
                                                                    <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion1" href="#accordion1_4"> 4. Wolf moon officia aute, non cupidatat skateboard dolor brunch ? </a>
                                                                </h4>
                                                            </div>
                                                            <div id="accordion1_4" class="panel-collapse collapse">
                                                                <div class="panel-body"> 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee
                                                                    nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat
                                                                    craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard of them accusamus labore sustainable VHS. </div>
                                                            </div>
                                                        </div>
                                                        <div class="panel panel-danger">
                                                            <div class="panel-heading">
                                                                <h4 class="panel-title">
                                                                    <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion1" href="#accordion1_5"> 5. Leggings occaecat craft beer farm-to-table, raw denim aesthetic ? </a>
                                                                </h4>
                                                            </div>
                                                            <div id="accordion1_5" class="panel-collapse collapse">
                                                                <div class="panel-body"> 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee
                                                                    nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat
                                                                    craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard of them accusamus labore sustainable VHS. Food truck quinoa nesciunt laborum eiusmod. Brunch 3
                                                                    wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et </div>
                                                            </div>
                                                        </div>
                                                        <div class="panel panel-default">
                                                            <div class="panel-heading">
                                                                <h4 class="panel-title">
                                                                    <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion1" href="#accordion1_6"> 6. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth ? </a>
                                                                </h4>
                                                            </div>
                                                            <div id="accordion1_6" class="panel-collapse collapse">
                                                                <div class="panel-body"> 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee
                                                                    nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat
                                                                    craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard of them accusamus labore sustainable VHS. Food truck quinoa nesciunt laborum eiusmod. Brunch 3
                                                                    wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et </div>
                                                            </div>
                                                        </div>
                                                        <div class="panel panel-default">
                                                            <div class="panel-heading">
                                                                <h4 class="panel-title">
                                                                    <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion1" href="#accordion1_7"> 7. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft ? </a>
                                                                </h4>
                                                            </div>
                                                            <div id="accordion1_7" class="panel-collapse collapse">
                                                                <div class="panel-body"> 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee
                                                                    nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat
                                                                    craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard of them accusamus labore sustainable VHS. Food truck quinoa nesciunt laborum eiusmod. Brunch 3
                                                                    wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div id="tab_2" class="tab-pane">
                                                    <div id="accordion2" class="panel-group">
                                                        <div class="panel panel-warning">
                                                            <div class="panel-heading">
                                                                <h4 class="panel-title">
                                                                    <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion2" href="#accordion2_1"> 1. Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry ? </a>
                                                                </h4>
                                                            </div>
                                                            <div id="accordion2_1" class="panel-collapse collapse in">
                                                                <div class="panel-body">
                                                                    <p> Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt
                                                                        laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore
                                                                        wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably
                                                                        haven't heard of them accusamus labore sustainable VHS. </p>
                                                                    <p> Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt
                                                                        laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore
                                                                        wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably
                                                                        haven't heard of them accusamus labore sustainable VHS. </p>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="panel panel-danger">
                                                            <div class="panel-heading">
                                                                <h4 class="panel-title">
                                                                    <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion2" href="#accordion2_2"> 2. Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry ? </a>
                                                                </h4>
                                                            </div>
                                                            <div id="accordion2_2" class="panel-collapse collapse">
                                                                <div class="panel-body"> Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Anim pariatur cliche reprehenderit,
                                                                    enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf
                                                                    moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente
                                                                    ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard of them accusamus labore sustainable
                                                                    VHS. </div>
                                                            </div>
                                                        </div>
                                                        <div class="panel panel-primary">
                                                            <div class="panel-heading">
                                                                <h4 class="panel-title">
                                                                    <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion2" href="#accordion2_3"> 3. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor ? </a>
                                                                </h4>
                                                            </div>
                                                            <div id="accordion2_3" class="panel-collapse collapse">
                                                                <div class="panel-body"> Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Anim pariatur cliche reprehenderit,
                                                                    enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf
                                                                    moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente
                                                                    ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard of them accusamus labore sustainable
                                                                    VHS. </div>
                                                            </div>
                                                        </div>
                                                        <div class="panel panel-default">
                                                            <div class="panel-heading">
                                                                <h4 class="panel-title">
                                                                    <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion2" href="#accordion2_4"> 4. Wolf moon officia aute, non cupidatat skateboard dolor brunch ? </a>
                                                                </h4>
                                                            </div>
                                                            <div id="accordion2_4" class="panel-collapse collapse">
                                                                <div class="panel-body"> 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee
                                                                    nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat
                                                                    craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard of them accusamus labore sustainable VHS. </div>
                                                            </div>
                                                        </div>
                                                        <div class="panel panel-default">
                                                            <div class="panel-heading">
                                                                <h4 class="panel-title">
                                                                    <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion2" href="#accordion2_5"> 5. Leggings occaecat craft beer farm-to-table, raw denim aesthetic ? </a>
                                                                </h4>
                                                            </div>
                                                            <div id="accordion2_5" class="panel-collapse collapse">
                                                                <div class="panel-body"> 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee
                                                                    nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat
                                                                    craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard of them accusamus labore sustainable VHS. Food truck quinoa nesciunt laborum eiusmod. Brunch 3
                                                                    wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et </div>
                                                            </div>
                                                        </div>
                                                        <div class="panel panel-default">
                                                            <div class="panel-heading">
                                                                <h4 class="panel-title">
                                                                    <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion2" href="#accordion2_6"> 6. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth ? </a>
                                                                </h4>
                                                            </div>
                                                            <div id="accordion2_6" class="panel-collapse collapse">
                                                                <div class="panel-body"> 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee
                                                                    nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat
                                                                    craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard of them accusamus labore sustainable VHS. Food truck quinoa nesciunt laborum eiusmod. Brunch 3
                                                                    wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et </div>
                                                            </div>
                                                        </div>
                                                        <div class="panel panel-default">
                                                            <div class="panel-heading">
                                                                <h4 class="panel-title">
                                                                    <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion2" href="#accordion2_7"> 7. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft ? </a>
                                                                </h4>
                                                            </div>
                                                            <div id="accordion2_7" class="panel-collapse collapse">
                                                                <div class="panel-body"> 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee
                                                                    nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat
                                                                    craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard of them accusamus labore sustainable VHS. Food truck quinoa nesciunt laborum eiusmod. Brunch 3
                                                                    wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div id="tab_3" class="tab-pane">
                                                    <div id="accordion3" class="panel-group">
                                                        <div class="panel panel-danger">
                                                            <div class="panel-heading">
                                                                <h4 class="panel-title">
                                                                    <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion3" href="#accordion3_1"> 1. Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry ? </a>
                                                                </h4>
                                                            </div>
                                                            <div id="accordion3_1" class="panel-collapse collapse in">
                                                                <div class="panel-body">
                                                                    <p> Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt
                                                                        laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. </p>
                                                                    <p> Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt
                                                                        laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. </p>
                                                                    <p> Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica,
                                                                        craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt
                                                                        you probably haven't heard of them accusamus labore sustainable VHS. </p>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="panel panel-primary">
                                                            <div class="panel-heading">
                                                                <h4 class="panel-title">
                                                                    <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion3" href="#accordion3_2"> 2. Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry ? </a>
                                                                </h4>
                                                            </div>
                                                            <div id="accordion3_2" class="panel-collapse collapse">
                                                                <div class="panel-body"> Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Anim pariatur cliche reprehenderit,
                                                                    enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf
                                                                    moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente
                                                                    ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard of them accusamus labore sustainable
                                                                    VHS. </div>
                                                            </div>
                                                        </div>
                                                        <div class="panel panel-default">
                                                            <div class="panel-heading">
                                                                <h4 class="panel-title">
                                                                    <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion3" href="#accordion3_3"> 3. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor ? </a>
                                                                </h4>
                                                            </div>
                                                            <div id="accordion3_3" class="panel-collapse collapse">
                                                                <div class="panel-body"> Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Anim pariatur cliche reprehenderit,
                                                                    enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf
                                                                    moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente
                                                                    ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard of them accusamus labore sustainable
                                                                    VHS. </div>
                                                            </div>
                                                        </div>
                                                        <div class="panel panel-default">
                                                            <div class="panel-heading">
                                                                <h4 class="panel-title">
                                                                    <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion3" href="#accordion3_4"> 4. Wolf moon officia aute, non cupidatat skateboard dolor brunch ? </a>
                                                                </h4>
                                                            </div>
                                                            <div id="accordion3_4" class="panel-collapse collapse">
                                                                <div class="panel-body"> 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee
                                                                    nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat
                                                                    craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard of them accusamus labore sustainable VHS. </div>
                                                            </div>
                                                        </div>
                                                        <div class="panel panel-default">
                                                            <div class="panel-heading">
                                                                <h4 class="panel-title">
                                                                    <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion3" href="#accordion3_5"> 5. Leggings occaecat craft beer farm-to-table, raw denim aesthetic ? </a>
                                                                </h4>
                                                            </div>
                                                            <div id="accordion3_5" class="panel-collapse collapse">
                                                                <div class="panel-body"> 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee
                                                                    nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat
                                                                    craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard of them accusamus labore sustainable VHS. Food truck quinoa nesciunt laborum eiusmod. Brunch 3
                                                                    wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et </div>
                                                            </div>
                                                        </div>
                                                        <div class="panel panel-default">
                                                            <div class="panel-heading">
                                                                <h4 class="panel-title">
                                                                    <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion3" href="#accordion3_6"> 6. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth ? </a>
                                                                </h4>
                                                            </div>
                                                            <div id="accordion3_6" class="panel-collapse collapse">
                                                                <div class="panel-body"> 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee
                                                                    nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat
                                                                    craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard of them accusamus labore sustainable VHS. Food truck quinoa nesciunt laborum eiusmod. Brunch 3
                                                                    wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et </div>
                                                            </div>
                                                        </div>
                                                        <div class="panel panel-default">
                                                            <div class="panel-heading">
                                                                <h4 class="panel-title">
                                                                    <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion3" href="#accordion3_7"> 7. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft ? </a>
                                                                </h4>
                                                            </div>
                                                            <div id="accordion3_7" class="panel-collapse collapse">
                                                                <div class="panel-body"> 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee
                                                                    nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat
                                                                    craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard of them accusamus labore sustainable VHS. Food truck quinoa nesciunt laborum eiusmod. Brunch 3
                                                                    wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!--end tab-pane-->
                            </div>
                        </div>
                    </div>
                            <!-- END EXAMPLE TABLE PORTLET-->
                           
                        </div>
                    </div>
                    <!-- END PAGE BASE CONTENT -->
                </div>
                <!-- END CONTENT BODY -->
            </div>
            <!-- END CONTENT -->


            <!-- Package Details Modal Start-->
                <div id="dr_package_details_modal" class="modal fade" role="dialog" aria-hidden="true">
                    
                </div>
            <!-- Package Details Modal End-->

            <!-- New Specialization Add Modal Start-->
                <div id="add_new_specialize_modal" class="modal fade" role="dialog" aria-hidden="true">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                                <h4 class="modal-title"><?=$this->lang->line('btn_add_label').' '.$this->lang->line('specialization_label');?></h4>
                            </div>
                            <div class="modal-body">
                                <div class="form-group">
                                    <label class="col-sm-4 control-label"><?=$this->lang->line('specialization_label').' '.$this->lang->line('name_tag_label');?></label>
                                    <div class="col-sm-8">
                                        <div class="input-group">
                                            <span class="input-group-addon">
                                                <i class="fa fa-tag"></i>
                                            </span>
                                            <input type="text" id="typeahead_example_modal_1" name="sp_name" class="form-control" /> 
                                        </div>
                                        <span id="error_msg" class="text-danger"></span>
                                    </div>
                                </div>
                            </div>
                            <div class="modal-footer">
                                <button  type="button" class="btn grey-salsa btn-outline" data-dismiss="modal"><?=$this->lang->line('btn_close_label');?></button>
                                <button onclick="add_new_specialize_btn()" type="button" class="btn green">
                                    <i class="fa fa-check"></i><?=$this->lang->line('btn_save_label');?></button>
                            </div>
                        </div>
                    </div>
                </div>
            <!-- New Specialization Add Modal End-->


            <!-- BEGIN Right SIDEBAR -->
            <!-- END Right SIDEBAR -->


        </div>
        <!-- END CONTAINER -->
        <!-- BEGIN FOOTER -->
       <?php $this->load->view('admin/footer'); ?>

       <?php $this->load->view('admin/footerlink'); ?>

        <script>
            function get_user_profile_tab_ajax(username,user_id,tab_name) 
            {
                $("#tab_loading_img").show();
                $.ajax({
                    url: "<?php echo site_url('admin/get_user_profile_tab_ajax');?>",
                    type: "post",
                    data: {tab_name:tab_name,username:username,user_id:user_id},
                    success: function(msg)
                    {
                      //console.log(msg);
                        $("#"+tab_name).html(msg);
                         ComponentsSelect2.init();
                         ComponentsBootstrapTouchSpin.init();
                         FormiCheck.init();
                         $(".icheck").iCheck({
                           checkboxClass: 'icheckbox_square-blue',
                           radioClass: 'iradio_square-blue',
                           increaseArea: '20%' // optional
                       });

                        $('.icheck').on('ifChecked', function(event){
                          if($(this).val()==0)
                          {
                            $("#package_image_div").hide();
                            $("#text_div").hide();
                            $("#audio_div").hide();
                            $("#video_div").hide();
                            $("#    ").show();
                          }
                          else
                          {
                            $("#package_image_div").show();
                            $("#package_format_div").hide();
                            $("#text_div").show();
                            $("#audio_div").show();
                            $("#video_div").show();
                          }
                        });
                        Dashboard.init();
                        ComponentsEditors.init();
                        ComponentsDateTimePickers.init(); 
                        $("#tab_loading_img").hide();
                    }      
                });   
            }
        </script>
        <script>
            function package_type_function() 
            {
                $("#text_div").hide();
                $("#audio_div").hide();
                $("#video_div").hide();
                if($("#package_format").val()==1)
                {
                    $("#text_div").show();
                }
                if($("#package_format").val()==2)
                {
                    $("#audio_div").show();
                }
                if($("#package_format").val()==3)
                {
                    $("#video_div").show();
                }

                console.log($("#package_format").val());
            }
        </script>
        <script>
            function view_package_details(package_id,doctor_id) 
            {
                $("#tab_loading_img").show();
                $.ajax({
                    url: "<?php echo site_url('admin/get_view_package_details');?>",
                    type: "post",
                    data: {package_id:package_id,doctor_id:doctor_id},
                    success: function(msg)
                    {
                      //console.log(msg);
                        $('#dr_package_details_modal').modal('show');
                        $('#dr_package_details_modal').html(msg);
                        ComponentsSelect2.init();
                        
                        ComponentsBootstrapTouchSpin.init();
                        FormiCheck.init();

                         $(".icheck").iCheck({
                           checkboxClass: 'icheckbox_square-blue',
                           radioClass: 'iradio_square-blue',
                           increaseArea: '20%' // optional
                       });


                        $('#summernote_2').summernote({height: 300});
                           
                         ComponentsTypeahead.init(); 
                         $("#tab_loading_img").hide();


                    }      
                });   
            }
        </script>
        <script>
            function get_text_consult_ajax(group_id) 
            {
               $("#consult_loading_img").show();
               $.ajax({
                    url: "<?php echo site_url('admin/get_text_consult_ajax');?>",
                    type: "post",
                    data: {group_id:group_id},
                    success: function(msg)
                    {
                      //console.log(msg);
                        $("#consult_text_details").html(msg);
                        $(".scroller").slimScroll("update");
                        $("#consult_loading_img").hide();
                    }
                });   
            }
        </script>
        <script>
            function add_specialize() 
            {
               var sp_name=$("#specialize_id").val();
               if(sp_name=='add_new')
               {
                    $('#add_new_specialize_modal').modal('show');
                    ComponentsTypeahead.init(); 
               }
            }

            function add_new_specialize_btn() 
            {
                var sp_name=$("#typeahead_example_modal_1").val();
                if(sp_name=='')
                {
                    $("#error_msg").html("<i class='fa fa-exclamation-triangle'></i>&nbsp;Specialize name can't be empty");
                }
                else
                {
                    $("#error_msg").html('');
                    console.log(sp_name);
                    $.ajax({
                          type: 'POST',
                          cache: false,
                          url: "<?php echo site_url('admin/insert_new_specialization');?>",
                          data: {sp_name: sp_name},
                          success: function(msg) 
                          {         
                            //console.log(msg);
                            if(msg=='error')
                            {
                                $("#error_msg").html("<i class='fa fa-exclamation-triangle'></i>&nbsp;Specialize name already exist");
                            }
                            else
                            {
                                $("#specialize_id").html(msg).fadeIn().delay(500);
                                $("#add_new_specialize_modal").modal ('hide');
                                $("#specialize_id").on('change', function(){        
                                        if($(this).find(":selected").val()=='add_new'){
                                           // alert($(this).find(":selected").val());
                                           $('#add_new_specialize_modal').modal('show');
                                        }
                                });
                            }
                          }
                    });
                }
            }
        </script>
        <script>
            function update_dr_experience(id) 
            {
               $("#tab_loading_img").show();
               var designation_id=$("#designation_id_"+id).val();
               var hospital_id=$("#hospital_id_"+id).val();
               var date_from=$("#date_from_"+id).val();
               var date_to=$("#date_to_"+id).val();
               console.log(designation_id+' '+hospital_id+' '+date_from+' '+date_to);
               $.ajax({
                    url: "<?php echo site_url('admin/update_doctor_experience');?>",
                    type: "post",
                    data: {id:id,designation_id:designation_id,hospital_id:hospital_id,date_from:date_from,date_to:date_to},
                    success: function(msg)
                    {
                      $("#exp_msg").html('Update Successful');
                      $("#tab_loading_img").hide();
                    }      
                }); 
            }
        </script>


        <script>
            function update_dr_education(id) 
            {
               $("#tab_loading_img").show();
               var inst_id=$("#inst_id"+id).val();
               var degree_id=$("#degree_id"+id).val();
               var passing_year=$("#passing_year"+id).val();

               
               $.ajax({
                    url: "<?php echo site_url('admin/update_doctor_education');?>",
                    type: "post",
                    data: {id:id,inst_id:inst_id,degree_id:degree_id,passing_year:passing_year},
                    success: function(msg)
                    {
                      $("#edu_msg").html('Update Successful');
                      $("#tab_loading_img").hide();
                    }      
                }); 
            }
        </script>


    </body>

</html>