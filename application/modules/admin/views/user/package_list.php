<?php $this->load->view('admin/headlink'); ?>

    <body class="page-container-bg-solid page-header-fixed page-sidebar-closed-hide-logo">
        <!-- BEGIN HEADER -->
       <?php $this->load->view('admin/head_nav'); ?>
        <!-- END HEADER -->
        <!-- BEGIN HEADER & CONTENT DIVIDER -->
        <div class="clearfix"> </div>
        <!-- END HEADER & CONTENT DIVIDER -->
        <!-- BEGIN CONTAINER -->
        <div class="page-container">
            <!-- BEGIN SIDEBAR -->
            <div class="page-sidebar-wrapper">
                <!-- BEGIN SIDEBAR -->
              <?php $this->load->view('admin/left_nav');?>
                <!-- END SIDEBAR -->
            </div>
            <!-- END SIDEBAR -->
            <!-- BEGIN CONTENT -->
            <div class="page-content-wrapper">
                <!-- BEGIN CONTENT BODY -->
                <div class="page-content">
                    <!-- BEGIN PAGE HEAD-->
                    <div class="page-head">
                        <!-- BEGIN PAGE TITLE -->
                        <?php $this->load->view('admin/page_title');?>
                        <!-- END PAGE TITLE -->
                        <!-- BEGIN PAGE TOOLBAR -->
                      
                        <!-- END PAGE TOOLBAR -->
                    </div>
                    <!-- END PAGE HEAD-->
                    <!-- BEGIN PAGE BREADCRUMB -->
                   
                    <!-- END PAGE BREADCRUMB -->
                    <!-- BEGIN PAGE BASE CONTENT -->
                    
                    <div class="row">
                        <div class="col-md-12">

                            <?php if($this->session->userdata('scc_alt')){ ?>
                            <div class="alert alert-success alert-dismissable">
                                <button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>
                                <a href="javascript:;" class="alert-link"><?=$this->session->userdata('scc_alt');?></a>
                            </div>
                            <?php } $this->session->unset_userdata('scc_alt');?>

                            <?php if($this->session->userdata('err_alt')){ ?>
                            <div class="alert alert-danger alert-dismissable">
                                <button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>
                                <a href="javascript:;" class="alert-link"><?=$this->session->userdata('err_alt');?></a>
                            </div>
                            <?php } $this->session->unset_userdata('err_alt');?>


                            <!-- BEGIN EXAMPLE TABLE PORTLET-->
                            <div class="portlet light bordered">
                                <div class="portlet-title">
                                    <div class="tools"> </div>
                                </div>
                                <div class="portlet-body">
                                    <table class="table table-striped table-bordered table-hover" id="sample_1">
                                        <thead>
                                            <tr>
                                                <th><?=$this->lang->line('id_tag_label');?></th>
                                                <th><?=$this->lang->line('package_label').' '.$this->lang->line('name_tag_label');?></th>
                                                <th><?=$this->lang->line('doctor_label').' '.$this->lang->line('name_tag_label');?></th>
                                                <th><?=$this->lang->line('package_label').' '.$this->lang->line('details_tag_label');?></th>
                                                <th><?=$this->lang->line('price_label');?></th>
                                                <th><?=$this->lang->line('status_tag_label');?></th>
                                                <th><?=$this->lang->line('action_tag_label');?></th>
                                                
                                            </tr>
                                        </thead>
                                        
                                        <tbody>
                                            
                                            <?php foreach ($package_list as $key => $row) { ?>
                                            <tr>
                                                <td><?=($key+1);?></td>
                                                <td class="text-center"><img class="table-image" src="uploads/product/thumbnail/<?=$row['main_image'];?>" alt=""></td>
                                                <td><a href="admin/edit_product/<?=$row['p_id'];?>"><?=$row['p_name'];?></a></td>
                                                <td><span class="badge badge-primary"><?=$row['name'];?></span></td>
                                                <td>
                                                  <?php 
                                                    $only_date_array=explode(' ', $row['created_date']);
                                                    $only_date=$only_date_array[0];
                                                    echo date('d M Y', strtotime($only_date));
                                                  ?>
                                                </td>
                                                <td>
                                                    <a href="admin/edit_product/<?=$row['p_id'];?>" class="btn btn-xs green tooltips" data-container="body" data-placement="top" data-original-title="Edit">
                                                        <i class="fa fa-edit"></i>
                                                    </a>
                                                    <a href="admin/delete_slider/<?=$row['p_id'];?>/<?=$row['main_image'];?>" class="btn btn-xs red"  data-toggle="confirmation" data-original-title="<?=$this->lang->line('delete_confirm_msg');?> ?" title=""><i class="fa fa-trash"></i></a>
                                                </td>
                                               
                                            </tr>
                                           
                                           <?php } ?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            <!-- END EXAMPLE TABLE PORTLET-->
                           
                        </div>
                    </div>
                    <!-- END PAGE BASE CONTENT -->
                </div>
                <!-- END CONTENT BODY -->
            </div>
            <!-- END CONTENT -->


            <!-- BEGIN Right SIDEBAR -->
            <!-- END Right SIDEBAR -->


        </div>
        <!-- END CONTAINER -->
        <!-- BEGIN FOOTER -->
       <?php $this->load->view('admin/footer'); ?>

      <?php $this->load->view('admin/footerlink'); ?>


    </body>

</html>