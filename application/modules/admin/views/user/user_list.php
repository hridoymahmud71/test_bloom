<?php $this->load->view('admin/headlink'); ?>

    <body class="page-container-bg-solid page-header-fixed page-sidebar-closed-hide-logo">
        <!-- BEGIN HEADER -->
       <?php $this->load->view('admin/head_nav'); ?>
        <!-- END HEADER -->
        <!-- BEGIN HEADER & CONTENT DIVIDER -->
        <div class="clearfix"> </div>
        <!-- END HEADER & CONTENT DIVIDER -->
        <!-- BEGIN CONTAINER -->
        <div class="page-container">
            <!-- BEGIN SIDEBAR -->
            <div class="page-sidebar-wrapper">
                <!-- BEGIN SIDEBAR -->
              <?php $this->load->view('admin/left_nav');?>
                <!-- END SIDEBAR -->
            </div>
            <!-- END SIDEBAR -->
            <!-- BEGIN CONTENT -->
            <div class="page-content-wrapper">
                <!-- BEGIN CONTENT BODY -->
                <div class="page-content">
                    <!-- BEGIN PAGE HEAD-->
                    <div class="page-head">
                        <!-- BEGIN PAGE TITLE -->
                        <?php $this->load->view('admin/page_title');?>
                        <!-- END PAGE TITLE -->
                        <!-- BEGIN PAGE TOOLBAR -->
                      
                        <!-- END PAGE TOOLBAR -->
                    </div>
                    <!-- END PAGE HEAD-->
                    <!-- BEGIN PAGE BREADCRUMB -->
                   
                    <!-- END PAGE BREADCRUMB -->
                    <!-- BEGIN PAGE BASE CONTENT -->
                    
                    <div class="row">
                        <div class="col-md-12">

                            <?php if($this->session->userdata('scc_alt')){ ?>
                            <div class="alert alert-success alert-dismissable">
                                <button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>
                                <a href="javascript:;" class="alert-link"><?=$this->session->userdata('scc_alt');?></a>
                            </div>
                            <?php } $this->session->unset_userdata('scc_alt');?>

                            <?php if($this->session->userdata('err_alt')){ ?>
                            <div class="alert alert-danger alert-dismissable">
                                <button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>
                                <a href="javascript:;" class="alert-link"><?=$this->session->userdata('err_alt');?></a>
                            </div>
                            <?php } $this->session->unset_userdata('err_alt');?>


                            <!-- BEGIN EXAMPLE TABLE PORTLET-->
                            <div class="portlet light bordered">
                                <div class="portlet-title">
                                    <a href="admin/<?=$add_btn_link;?>" role="button" class="btn green" >
                                        <i class="fa fa-plus-square"></i> <?=$add_btn;?>
                                    </a>
                                    <div class="tools"> </div>
                                </div>
                                <div class="portlet-body">
                                    <table class="table table-striped table-bordered table-hover" id="sample_1">
                                        <thead>
                                            <tr>
                                                <th><?=$this->lang->line('id_tag_label');?></th>
                                                <th><?=$this->lang->line('img_tag_label');?></th>
                                                <th><?=$this->lang->line('name_tag_label');?></th>
                                                <th><?=$this->lang->line('email_label');?></th>
                                                <th><?=$this->lang->line('phone_tag_label');?></th>
                                                <th><?=$this->lang->line('type_label');?></th>
                                                <th><?=$this->lang->line('status_tag_label');?></th>
                                                <th><?=$this->lang->line('action_tag_label');?></th>
                                                
                                            </tr>
                                        </thead>
                                        
                                        <tbody>
                                            
                                            <?php foreach ($user_list as $key => $row) { ?>
                                            <tr>
                                                <td><?=($key+1);?></td>
                                                <td class="text-center"><img class="table-image img-responsive" src="uploads/user/<?=$row['profile_image'];?>" alt="">
                                                </td>

                                                <td>
                                                    <?php if($row['verify_status']==1){?>
                                                    <a href="admin/user_profile/<?=$row['username'];?>"><?=$row['name'];?>
                                                    </a>
                                                    <?php } else { ?>
                                                    <a href="javascript:0;"><?=$row['name'];?>
                                                    </a>
                                                    <?php } ?>
                                                </td>
                                                <td><span class="badge badge-primary"><?=$row['email'];?></span></td>
                                                <td><span class="badge badge-primary"><?=$row['mobile_no'];?></span></td>
                                                <td>
                                              
                                                    <?php foreach ($user_type as $key => $value) { if($value['id']==$row['user_type']) {?>
                                                    <?=$value['type_name'];?>
                                                    <?php }} ?>
                                                   
                                                </td>
                                                <td>
                                                    <?php if($row['verify_status']==0){?>
                                                    <span class="badge badge-danger">inactive</span>
                                                    <?php } else{?>
                                                        <span class="badge badge-success">Verified</span>
                                                        <?php } ?>

                                                </td>
                                                <td>
                                                    <?php if($row['verify_status']==0){?>
                                                    <a href="javascript:0;" onclick="verify_now('<?=$row["id"];?>')" class="btn btn-xs green tooltips" data-container="body" data-placement="top" data-original-title="Verify Now">
                                                        <i class="fa fa-check"></i>
                                                    </a>
                                                    <?php } else{?>
                                                    <a href="admin/user_profile/<?=$row['username'];?>" class="btn btn-xs green tooltips" data-container="body" data-placement="top" data-original-title="Profile">
                                                        <i class="fa fa-edit"></i>
                                                    </a>
                                                    <?php } ?>
                                                    <a href="admin/delete_slider/<?=$row['id'];?>/<?=$row['id'];?>" class="btn btn-xs red"  data-toggle="confirmation" data-original-title="<?=$this->lang->line('delete_confirm_msg');?> ?" title=""><i class="fa fa-trash"></i></a>
                                                </td>
                                               
                                            </tr>
                                           
                                           <?php } ?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            <!-- END EXAMPLE TABLE PORTLET-->
                           
                        </div>
                    </div>
                    <!-- END PAGE BASE CONTENT -->
                </div>
                <!-- END CONTENT BODY -->
            </div>
            <!-- END CONTENT -->
            

        <!-- Verify User Modal Start-->
            <div id="verify_now_modal" class="modal fade" role="dialog" aria-hidden="true">
                <div class="modal-dialog">
                <form action="admin/check_verification_code" method="post" class="form-horizontal form-row-seperated">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                            <h4 class="modal-title"><?=$this->lang->line('user_label').' '.$this->lang->line('verification_label');?></h4>
                        </div>
                        <div class="modal-body">
                                <div class="form-group">
                                    <label class="col-sm-4 control-label"><?=$this->lang->line('verification_label').' '.$this->lang->line('code_label');?></label>
                                    <div class="col-sm-8">
                                        <div class="input-group">
                                            <span class="input-group-addon">
                                                <i class="fa fa-check"></i>
                                            </span>
                                            <input type="text" name="verify_code" class="form-control" /> 
                                        </div>

                                            <input type="hidden" name="user_id" id="user_id" class="form-control" />
                                    </div>
                                </div>
               
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn grey-salsa btn-outline" data-dismiss="modal"><?=$this->lang->line('btn_close_label');?></button>
                            <button type="submit" class="btn green">
                                <i class="fa fa-check"></i><?=$this->lang->line('btn_submit_label');?></button>
                        </div>
                    </div>
                </form>


                </div>
            </div>

        <!-- Verify User Modal End-->

            <!-- BEGIN Right SIDEBAR -->
            <!-- END Right SIDEBAR -->


        </div>
        <!-- END CONTAINER -->
        <!-- BEGIN FOOTER -->
        <?php $this->load->view('admin/footer'); ?>

        <?php $this->load->view('admin/footerlink'); ?>
        <script>
            function verify_now(id) 
            {
                $('#user_id').val(id);
                $('#verify_now_modal').modal('show');
               
            }
        </script>

    </body>

</html>