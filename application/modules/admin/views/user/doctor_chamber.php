<?php $this->load->view('admin/headlink'); ?>
<style>
    .input-group-btn {
        display: none;
    }
    .delete_img
    {
        position: absolute;
        top: 20px;
        color: #444 !important;
        font-weight: 700;
        border: 1px solid #ccc;
        padding: 5px;
        right: 30px;
        background: white !important;
        transition: .5s !important;
    }
    .delete_img:hover
    {
        color: white !important;
        background: #ed6b75 !important;
        transition: .5s !important;
    }
</style>
    <body class="page-container-bg-solid page-header-fixed page-sidebar-closed-hide-logo">
        <!-- BEGIN HEADER -->
       <?php $this->load->view('admin/head_nav'); ?>
        <!-- END HEADER -->
        <!-- BEGIN HEADER & CONTENT DIVIDER -->
        <div class="clearfix"> </div>
        <!-- END HEADER & CONTENT DIVIDER -->
        <!-- BEGIN CONTAINER -->
        <div class="page-container">
            <!-- BEGIN SIDEBAR -->
            <div class="page-sidebar-wrapper">
                <!-- BEGIN SIDEBAR -->
              <?php $this->load->view('admin/left_nav');?>
                <!-- END SIDEBAR -->
            </div>
            <!-- END SIDEBAR -->
            <!-- BEGIN CONTENT -->
            <div class="page-content-wrapper">
                <!-- BEGIN CONTENT BODY -->
                <div class="page-content">
                    <!-- BEGIN PAGE HEAD-->
                    <div class="page-head">
                        <!-- BEGIN PAGE TITLE -->
                        <?php $this->load->view('admin/page_title');?>
                        <!-- END PAGE TITLE -->
                        <!-- BEGIN PAGE TOOLBAR -->
                      
                        <!-- END PAGE TOOLBAR -->
                    </div>
                    <!-- END PAGE HEAD-->
                    <!-- BEGIN PAGE BREADCRUMB -->
                   
                    <!-- END PAGE BREADCRUMB -->
                    <!-- BEGIN PAGE BASE CONTENT -->
                    
                    <div class="row">
                        <div class="col-md-12">
                            
                            <div class="portlet box green ">
                                <div class="portlet-title">
                                    <div class="caption">
                                        <i class="fa fa-newspaper-o"></i> <?=$this->lang->line('chamber_label').' '.$this->lang->line('btn_edit_label').' '.$this->lang->line('form_label');?></div>
                                    <div class="tools">
                                        <a href="javascript:;" class="collapse"> </a>
                                        
                                        <a href="javascript:;" class="remove"> </a>
                                    </div>
                                </div>
                                <div class="portlet-body form">
                                    <!-- BEGIN FORM-->
                                    <a href="#add_modal" data-toggle="modal" class="btn btn-primary btn-md"><i class="fa fa-plus-circle"></i> <?=$this->lang->line('btn_add_label').' '.$this->lang->line('new_label').' '.$this->lang->line('chamber_label');?></a>
                                    <form  class="form-horizontal form-bordered form-row-stripped" action="admin/add_dr_chamber_post/<?=$doctor_name;?>/<?=$doctor_id;?>" method="post" enctype="multipart/form-data">
                                        <div class="form-body">
                                            

                                            <div class="form-group">
                                                <label class="control-label col-md-3">
                                                <?=$this->lang->line('chamber_label').' '.$this->lang->line('type_label');?>
                                                    
                                                </label>
                                                <div class="col-md-9">
                                                    <div class="icheck-inline">
                                                        <label class="">
                                                            <div class="iradio_square-blue" style="position: relative;">
                                                                <input type="radio" name="is_personal" checked="" value="0" class="icheck"  data-radio="iradio_square-blue" style="position: absolute; opacity: 0;">
                                                                <ins class="iCheck-helper" style="position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; background: rgb(255, 255, 255); border: 0px; opacity: 0;"> 
                                                                </ins>
                                                            </div> General
                                                        </label>
                                                        <label class="">
                                                            <div class="iradio_square-blue" style="position: relative;">
                                                                <input value="1" type="radio" name="is_personal" class="icheck"  data-radio="iradio_square-blue" style="position: absolute; opacity: 0;">
                                                                <ins class="iCheck-helper" style="position: absolute; top: 0%; left: 0%; display: block; width: 100%; height: 100%; margin: 0px; padding: 0px; background: rgb(255, 255, 255); border: 0px; opacity: 0;"> 
                                                                </ins>
                                                            </div> Personal
                                                        </label>
                                                        
                                                    </div>
                                                </div>
                                            </div>


                                            <div class="form-group">
                                                <label class="control-label col-md-3">
                                                <?=$this->lang->line('chamber_label').' '.$this->lang->line('name_tag_label');?>
                                                    
                                                </label>
                                                <div class="col-md-9">
                                                    <select id="chamber_id" required="" name="chamber_id"  class="form-control select2" data-placeholder="<?=$this->lang->line('choose_msg').' '.$this->lang->line('chamber_label');?>">
                                                        <option></option>
                                                        <?php foreach ($chamber_list as $key => $row) { ?>
                                                            <option value="<?=$row['reg_id'];?>">
                                                                <?=$row['name'].'&nbsp;&nbsp;'.$row['area_title'].','.$row['district_name'].','.$row['division_name'];?>
                                                            </option>
                                                        <?php } ?>
                                                    </select>
                                                </div>
                                            </div>

                                            <div class="form-group">
                                               <label class="control-label col-md-3">
                                               <?=$this->lang->line('chamber_label').' '.$this->lang->line('schedule_label');?>
                                                </label>

                                                <div class="col-md-3">
                                                    <select required=""  name="day_list[]" class="form-control select2-multiple" multiple data-placeholder="<?=$this->lang->line('choose_msg').' '.$this->lang->line('day_label');?>">
                                                        <option></option>
                                                        <option value="Sat">Sat</option>
                                                        <option value="Sun">Sun</option>
                                                        <option value="Mon">Mon</option>
                                                        <option value="Tue">Tue</option>
                                                        <option value="Wed">Wed</option>
                                                        <option value="Thu">Thu</option>
                                                        <option value="Fri">Fri</option>
                                                    </select>
                                                </div>

                                                <div class="col-md-3">
                                                    <label class="col-md-5 time-label"><?=$this->lang->line('opening_time_label');?>
                                                        
                                                    </label>
                                                    <div class="input-group">
                                                        <input required="" value="<?=date('h:i a');?>" name="start_time" type="text" class="form-control timepicker timepicker-no-seconds">
                                                        <span class="input-group-btn">
                                                            <button class="btn default" type="button">
                                                                <i class="fa fa-clock-o"></i>
                                                            </button>
                                                        </span>
                                                    </div>
                                                </div>

                                                <div class="col-md-3">
                                                    <label class="col-md-5 time-label"><?=$this->lang->line('closing_time_label');?>
                                                        
                                                    </label>
                                                    <div class="input-group">
                                                        <input required="" value="<?=date('h:i a');?>" name="end_time" type="text" class="form-control timepicker timepicker-no-seconds">
                                                        <span class="input-group-btn">
                                                            <button class="btn default" type="button">
                                                                <i class="fa fa-clock-o"></i>
                                                            </button>
                                                        </span>
                                                    </div>
                                                </div>    
                                            </div>

                                            <div class="form-group">
                                                <label class="control-label col-md-3"> <?=$this->lang->line('chamber_label').' '.$this->lang->line('fee_label');?></label>
                                                <div class="col-md-4">
                                                    <input name="consult_fee" id="touchspin_2" type="text" value="0.00" class="form-control"> 
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label class="control-label col-md-3"> <?=$this->lang->line('chamber_label').' '.$this->lang->line('phone_tag_label');?></label>
                                                <div class="col-md-4">
                                                    <input value="" class="form-control" name="phone_one" placeholder="phone no 1" id="" type="text" />
                                                    
                                                </div>
                                                <div class="col-md-4">
                                                    <input value="" class="form-control" name="phone_two" placeholder="phone no 2"  type="text" />
                                                    
                                                </div>
                                            </div>


                                            <div class="form-group" id="photo_div_end">
                                                <label class="control-label col-md-3">
                                                <?=$this->lang->line('file_label').' '.$this->lang->line('gallery_tag_label');?>
                                                    
                                                </label>
                                                
                                                   
                                               
                                                <div class="col-md-3" id="add_image_div">
                                                    <a href="javascript:;" onclick="add_image_div()">
                                                        <div class="fileinput-new thumbnail" style="width: 200px; height: 150px;">
                                                            <img src="back_assets/add_more.png" alt="Your Image" />
                                                        </div>
                                                    </a>
                                                </div>
                                            </div>  
                                           
                                        <div class="form-actions">
                                            <div class="row">
                                                <div class="col-md-offset-3 col-md-9">
                                                    <button type="submit" class="btn green">
                                                        <i class="fa fa-check"></i> <?=$this->lang->line('btn_submit_label');?></button>
                                                    
                                                </div>
                                            </div>
                                        </div>
                                    </form>
                                    <!-- END FORM-->
                                </div>
                            </div>
                
                        </div>
                    </div>
                    <!-- END PAGE BASE CONTENT -->
                </div>
                <!-- END CONTENT BODY -->
            </div>
            <!-- END CONTENT -->


            <!-- BEGIN Right SIDEBAR -->
            <!-- END Right SIDEBAR -->

            <!-- Chamber Add Modal Start-->
            <div id="add_modal" class="modal fade" role="dialog" aria-hidden="true">
                <div class="modal-dialog">
                <form action="admin/add_category_post" method="post" class="form-horizontal form-row-seperated">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                            <h4 class="modal-title"><?=$add_btn;?></h4>
                        </div>
                        <div class="modal-body">
                            <div class="form-group">
                                <label class="col-sm-4 control-label"><?=$this->lang->line('chamber_label').' '.$this->lang->line('name_tag_label');?></label>
                                <div class="col-sm-8">
                                    <div class="input-group">
                                        <span class="input-group-addon">
                                            <i class="fa fa-tag"></i>
                                        </span>
                                        <input type="text" id="typeahead_example_modal_1" name="" class="form-control" /> </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="control-label col-md-4">
                                <?=$this->lang->line('chamber_label').' '.$this->lang->line('division_label');?> 
                                </label>
                                <div class="col-md-8">
                                    <select onchange="get_district()" id="division_id" name="division_id"  class="form-control select2" data-placeholder="<?=$this->lang->line('choose_msg').' '.$this->lang->line('division_label');?>">
                                        <option></option>
                                        <?php foreach ($division_list as $key => $row) { ?>
                                            <option value="<?=$row['id'];?>"><?=$row['name'].' '.$row['bn_name'];?> </option>
                                        <?php } ?>
                                    </select>
                                    <div class="clearfix margin-top-10">
                                        <span class="label label-danger">NOTE!</span> You can not modify it later
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="control-label col-md-4">
                                <?=$this->lang->line('chamber_label').' '.$this->lang->line('district_label');?>
                                    
                                </label>
                                <div class="col-md-8">
                                    <select onchange="get_area()" id="district_id" name="district_id" class="form-control select2" data-placeholder="<?=$this->lang->line('choose_msg').' '.$this->lang->line('district_label');?>">
                                        <option></option>
                                        <?php foreach ($district_list as $key => $row) { ?>
                                            <option value="<?=$row['id'];?>"><?=$row['name'].' '.$row['bn_name'];?> </option>
                                        <?php } ?>
                                    </select>
                                    <div class="clearfix margin-top-10">
                                        <span class="label label-danger">NOTE!</span> You can not modify it later
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="control-label col-md-4">
                                <?=$this->lang->line('chamber_label').' '.$this->lang->line('area_label');?> 
                                </label>
                                <div class="col-md-8">
                                    <select id="area_id" name="area_id" class="form-control select2" data-placeholder="<?=$this->lang->line('choose_msg').' '.$this->lang->line('area_label');?>">
                                        <option></option>
                                        <?php foreach ($area_list as $key => $row) { ?>
                                            <option value="<?=$row['area_id'];?>"><?=$row['area_title'].' '.$row['area_title_bangla'];?> </option>
                                        <?php } ?>
                                    </select>
                                    <div class="clearfix margin-top-10">
                                        <span class="label label-danger">NOTE!</span> You can not modify it later
                                    </div>
                                </div>
                            </div>
               
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn grey-salsa btn-outline" data-dismiss="modal"><?=$this->lang->line('btn_close_label');?></button>
                            <a onclick="add_doctor_chamber()" class="btn green">
                                <i class="fa fa-check"></i><?=$this->lang->line('btn_save_label');?></a>
                        </div>
                    </div>
                </form>


                </div>
            </div>
        <!-- Chamber Add Modal End-->


        </div>
        <!-- END CONTAINER -->
        <!-- BEGIN FOOTER -->
       <?php $this->load->view('admin/footer'); ?>

      <?php $this->load->view('admin/footerlink'); ?>

    <script>
        var pre_img_count=$("#pre_img_count").val();
        var div_count=1+parseFloat(pre_img_count);

        function delete_gallery_img(shop_id,img_name) 
        {
            $.ajax({
                url: "<?php echo site_url('admin/delete_gallery_img');?>",
                type: "post",
                data: {shop_id:shop_id,img_name:img_name},
                success: function(msg)
                {
                    // alert(msg);
                    $("#my_div").load(location.href + " #my_div");
                    pre_img_count=pre_img_count-1;
                   
                    div_count=1+parseFloat(pre_img_count);

                    //alert(pre_img_count);
                   //$("#sub_cat_id").html(msg);
                }      
            });  
        }


        

        function get_district() 
        {
            var division_id=$("#division_id").val();

            $.ajax({
                url: "<?php echo site_url('admin/get_district');?>",
                type: "post",
                data: {division_id:division_id},
                success: function(msg)
                {
                   // alert(msg);
                   $("#district_id").html(msg);
                }      
            });  
        }



        function get_area() 
        {
            var district_id=$("#district_id").val();

            $.ajax({
                url: "<?php echo site_url('admin/get_area');?>",
                type: "post",
                data: {district_id:district_id},
                success: function(msg)
                {
                    //alert(msg);
                   $("#area_id").html(msg);
                   
                }      
            });  
        }
    </script>
      <!-- Add Product Image Div -->

    <script>
 

        var div_count=1;
        //alert(div_count);
        function add_image_div() 
        {
           //alert(div_count);
           if(div_count==9)
           {
            $("#add_image_div").hide();
           }

           
              $("#add_image_div").before(
                '<div class="col-md-3">'
                +'<div class="fileinput fileinput-new" data-provides="fileinput">'
                    +'<div class="fileinput-new thumbnail" style="width: 200px; height: 150px;">'
                        +'<img src="back_assets/img_default.png" alt="Your Image" /> </div>'
                    +'<div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 200px; max-height: 150px;"> </div>'
                    +'<div>'
                        +'<span class="btn default btn-file">'
                            +'<span class="fileinput-new"><?=$this->lang->line('select_label').' '.$this->lang->line('img_tag_label');?></span>'
                            +'<span class="fileinput-exists"> <?=$this->lang->line('change_label');?> </span>'
                            +'<input type="file" name="userfile[]"> </span>'
                        +'<a href="javascript:;" class="btn red fileinput-exists" data-dismiss="fileinput"> <?=$this->lang->line('remove_label');?> </a>'
                    +'</div>'
                   
                +'</div>'
                +'</div>');


              if(div_count==3 || div_count==6)
            {
                $("#add_image_div").before('<label class="control-label col-md-3"></label>');
            }
              div_count++;

          }
    </script>

    <script type="text/javascript">
            var ComponentsTypeahead = function () {

            var handleTwitterTypeahead = function() {
            }

            var handleTwitterTypeaheadModal = function() {

                // Example #1
                // instantiate the bloodhound suggestion engine
                var numbers = new Bloodhound({
                  datumTokenizer: function(d) { return Bloodhound.tokenizers.whitespace(d.num); },
                  queryTokenizer: Bloodhound.tokenizers.whitespace,
                  local: [
                   <?php foreach ($chamber_list as $key => $row) { ?>
                    { num: '<?=$row['name'];?>---<?=$row['division_name'];?>,<?=$row['district_name'];?>,<?=$row['area_title'];?>' },
                    <?php } ?>
                    
                  ]
                });
                 
                // initialize the bloodhound suggestion engine
                numbers.initialize();
                 
                // instantiate the typeahead UI
                if (App.isRTL()) {
                  $('#typeahead_example_modal_1').attr("dir", "rtl");  
                }
                $('#typeahead_example_modal_1').typeahead(null, {
                  displayKey: 'num',
                  hint: (App.isRTL() ? false : true),
                  source: numbers.ttAdapter()
                });
            }

            return {
                //main function to initiate the module
                init: function () {
                    handleTwitterTypeahead();
                    handleTwitterTypeaheadModal();
                }
            };

            }();

            jQuery(document).ready(function() {    
               ComponentsTypeahead.init(); 
            });
    </script>
    <script>
        function add_doctor_chamber() 
        {
            
           var chamber_name=$("#typeahead_example_modal_1").val();
           var division_id=$("#division_id").val();
           var district_id=$("#district_id").val();
           var area_id=$("#area_id").val();
           $.ajax({
                url: "<?php echo site_url('admin/add_chamber_name');?>",
                type: "post",
                data: {chamber_name:chamber_name,division_id:division_id,district_id:district_id,area_id:area_id},
                success: function(msg)
                {
                  $("#chamber_id").html(msg);
                  $("#add_modal").modal('hide');
                 
                }      
            }); 
        }
    </script>

    </body>

</html>