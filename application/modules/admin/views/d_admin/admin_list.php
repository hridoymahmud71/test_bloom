<?php $this->load->view('admin/headlink'); ?>

    <body class="page-container-bg-solid page-header-fixed page-sidebar-closed-hide-logo">
        <!-- BEGIN HEADER -->
       <?php $this->load->view('admin/head_nav'); ?>
        <!-- END HEADER -->
        <!-- BEGIN HEADER & CONTENT DIVIDER -->
        <div class="clearfix"> </div>
        <!-- END HEADER & CONTENT DIVIDER -->
        <!-- BEGIN CONTAINER -->
        <div class="page-container">
            <!-- BEGIN SIDEBAR -->
            <div class="page-sidebar-wrapper">
                <!-- BEGIN SIDEBAR -->
              <?php $this->load->view('admin/left_nav');?>
                <!-- END SIDEBAR -->
            </div>
            <!-- END SIDEBAR -->
            <!-- BEGIN CONTENT -->
            <div class="page-content-wrapper">
                <!-- BEGIN CONTENT BODY -->
                <div class="page-content">
                    <!-- BEGIN PAGE HEAD-->
                    <div class="page-head">
                        <!-- BEGIN PAGE TITLE -->
                        <?php $this->load->view('admin/page_title');?>
                        <!-- END PAGE TITLE -->
                        <!-- BEGIN PAGE TOOLBAR -->
                      
                        <!-- END PAGE TOOLBAR -->
                    </div>
                    <!-- END PAGE HEAD-->
                    <!-- BEGIN PAGE BREADCRUMB -->
                   
                    <!-- END PAGE BREADCRUMB -->
                    <!-- BEGIN PAGE BASE CONTENT -->
                    
                    <div class="row">
                        <div class="col-md-12">

                            <?php if($this->session->userdata('scc_alt')){ ?>
                            <div class="alert alert-success alert-dismissable">
                                <button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>
                                <a href="javascript:;" class="alert-link"><?=$this->session->userdata('scc_alt');?></a>
                            </div>
                            <?php } $this->session->unset_userdata('scc_alt');?>


                            <!-- BEGIN EXAMPLE TABLE PORTLET-->
                            <div class="portlet light bordered">
                                <div class="portlet-title">
                                    <a href="admin/add_district_admin" role="button" class="btn green" > 
                                        <i class="fa fa-plus-square"></i> <?=$add_btn;?>
                                    </a>

                                  
                                    <div class="tools"> </div>
                                </div>

                            
                                <div class="portlet-body">
                                    <table class="table table-striped table-bordered table-hover" id="sample_1">
                                        <thead>
                                            <tr>
                                                <th><?=$this->lang->line('id_tag_label');?></th>
                                               
                                                <th><?=$this->lang->line('name_tag_label');?></th>
                                                <th><?=$this->lang->line('district_label').' '.$this->lang->line('name_tag_label');?></th>
                                               
                                               
                                                <th class="text-center"><?=$this->lang->line('action_tag_label');?></th>
                                                
                                            </tr>
                                        </thead>
                                        
                                        <tbody>
                                            
                                            <?php foreach ($district_admin_list as $key => $row) { ?>
                                            <tr>
                                                <td><?=($key+1);?></td>
                                              
                                                <td><a  href="javascript:;"><?=$row['d_name'];?></a></td>
                                               
                                                
                                                <td><?=$row['name'];?></td>
                                               
                                               
                                                <td class="text-center">
                                                    <a href="admin/edit_district_admin/<?=$row['d_id'];?>" class="btn btn-xs green tooltips" data-container="body" data-placement="top" data-original-title="Edit">
                                                        <i class="fa fa-edit"></i>
                                                    </a>
                                                    <a href="admin/delete_market/<?=$row['d_id'];?>" class="btn btn-xs red"  data-toggle="confirmation" data-original-title="<?=$this->lang->line('delete_confirm_msg');?> ?" title=""><i class="fa fa-trash"></i></a>
                                                </td>
                                               
                                            </tr>
                                           
                                           <?php } ?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            <!-- END EXAMPLE TABLE PORTLET-->
                           
                        </div>
                    </div>
                    <!-- END PAGE BASE CONTENT -->
                </div>
                <!-- END CONTENT BODY -->
            </div>
            <!-- END CONTENT -->


            <!-- BEGIN Right SIDEBAR -->
            <!-- END Right SIDEBAR -->


        </div>
        <!-- END CONTAINER -->
        <!-- BEGIN FOOTER -->
       <?php $this->load->view('admin/footer'); ?>

       <?php $this->load->view('admin/footerlink'); ?>

        <script>
            function edit_modal(id,name) {
               $("#cat_id_hidden").val(id);
               $("#cat_name").val(name);
               $('#cat_edit_modal').modal('show'); 
            }
        </script>

         <script>
            function edit_sub_modal(cat_id,id,name) {
               $("#cat_id_modal").val(cat_id).change();
               $("#sub_cat_id_hidden").val(id);
               $("#sub_name_modal").val(name);
               $('#sub_cat_edit_modal').modal('show'); 
            }
        </script>
        <script type="text/javascript">
            var ComponentsTypeahead = function () {

            var handleTwitterTypeahead = function() {
            }

            var handleTwitterTypeaheadModal = function() {

                // Example #1
                // instantiate the bloodhound suggestion engine
                var numbers = new Bloodhound({
                  datumTokenizer: function(d) { return Bloodhound.tokenizers.whitespace(d.num); },
                  queryTokenizer: Bloodhound.tokenizers.whitespace,
                  local: [
                   <?php foreach ($category_list as $key => $row) { ?>
                    { num: '<?=$row['name'];?>' },
                    <?php } ?>
                    
                  ]
                });
                 
                // initialize the bloodhound suggestion engine
                numbers.initialize();
                 
                // instantiate the typeahead UI
                if (App.isRTL()) {
                  $('#typeahead_example_modal_1').attr("dir", "rtl");  
                }
                $('#typeahead_example_modal_1').typeahead(null, {
                  displayKey: 'num',
                  hint: (App.isRTL() ? false : true),
                  source: numbers.ttAdapter()
                });
            }

            return {
                //main function to initiate the module
                init: function () {
                    handleTwitterTypeahead();
                    handleTwitterTypeaheadModal();
                }
            };

            }();

            jQuery(document).ready(function() {    
               ComponentsTypeahead.init(); 
            });
        </script>
    </body>

</html>