<?php $this->load->view('admin/headlink'); ?>
<style>
    .input-group-btn {
        display: none;
    }
</style>
    <body class="page-container-bg-solid page-header-fixed page-sidebar-closed-hide-logo">
        <!-- BEGIN HEADER -->
       <?php $this->load->view('admin/head_nav'); ?>
        <!-- END HEADER -->
        <!-- BEGIN HEADER & CONTENT DIVIDER -->
        <div class="clearfix"> </div>
        <!-- END HEADER & CONTENT DIVIDER -->
        <!-- BEGIN CONTAINER -->
        <div class="page-container">
            <!-- BEGIN SIDEBAR -->
            <div class="page-sidebar-wrapper">
                <!-- BEGIN SIDEBAR -->
              <?php $this->load->view('admin/left_nav');?>
                <!-- END SIDEBAR -->
            </div>
            <!-- END SIDEBAR -->
            <!-- BEGIN CONTENT -->
            <div class="page-content-wrapper">
                <!-- BEGIN CONTENT BODY -->
                <div class="page-content">
                    <!-- BEGIN PAGE HEAD-->
                    <div class="page-head">
                        <!-- BEGIN PAGE TITLE -->
                        <?php $this->load->view('admin/page_title');?>
                        <!-- END PAGE TITLE -->
                        <!-- BEGIN PAGE TOOLBAR -->
                      
                        <!-- END PAGE TOOLBAR -->
                    </div>
                    <!-- END PAGE HEAD-->
                    <!-- BEGIN PAGE BREADCRUMB -->
                   
                    <!-- END PAGE BREADCRUMB -->
                    <!-- BEGIN PAGE BASE CONTENT -->
                    
                    <div class="row">
                        <div class="col-md-12">
                            
                            <div class="portlet box green ">
                                <div class="portlet-title">
                                    <div class="caption">
                                        <i class="fa fa-newspaper-o"></i> <?=$this->lang->line('admin_label').' '.$this->lang->line('btn_edit_label').' '.$this->lang->line('form_label');?></div>
                                    <div class="tools">
                                        <a href="javascript:;" class="collapse"> </a>
                                        
                                        <a href="javascript:;" class="remove"> </a>
                                    </div>
                                </div>
                                <div class="portlet-body form">
                                    <!-- BEGIN FORM-->
                                    <form  class="form-horizontal form-bordered form-row-stripped" action="admin/update_district_admin_post/<?=$admin_details[0]['d_id'];?>/<?=$admin_details[0]['id'];?>" method="post" enctype="multipart/form-data">
                                        <div class="form-body">



                                            <!-- <div class="form-group">
                                                <label class="control-label col-md-3">
                                                <?=$this->lang->line('district_admin_label').' '.$this->lang->line('district_label');?>
                                                    
                                                </label>
                                                <div class="col-md-9">
                                                    <select required="" id="district_id" name="district_id" class="form-control select2" data-placeholder="<?=$this->lang->line('choose_msg').' '.$this->lang->line('district_label');?>">
                                                        <option></option>
                                                        <?php foreach ($district_list as $key => $row) { ?>
                                                            <option <?php if($admin_details[0]['district_id']==$row['id']){echo "selected";} ?> value="<?=$row['id'];?>"><?=$row['name'].' '.$row['bn_name'];?> </option>
                                                        <?php } ?>
                                                    </select>
                                                </div>
                                            </div> -->



                                            <div class="form-group">
                                                <label class="control-label col-md-3">
                                                <?=$this->lang->line('district_admin_label').' '.$this->lang->line('name_tag_label');?>
                                                    
                                                </label>
                                                <div class="col-md-9">
                                                    <input value="<?=$admin_details[0]['name'];?>" name="name" required="" type="text" class="form-control"> </div>
                                            </div>


                                            <div class="form-group">
                                                <label class="control-label col-md-3">
                                                <?=$this->lang->line('district_admin_label').' '.$this->lang->line('email_label');?>
                                                    
                                                </label>
                                                <div class="col-md-9">
                                                    <input value="<?=$admin_details[0]['email'];?>" name="email" required="" type="text" class="form-control"> </div>
                                            </div>


                                            


                                           
                                           
                                        </div>
                                        <div class="form-actions">
                                            <div class="row">
                                                <div class="col-md-offset-3 col-md-9">
                                                    <button type="submit" class="btn green">
                                                        <i class="fa fa-check"></i> <?=$this->lang->line('btn_update_label');?></button>
                                                    
                                                </div>
                                            </div>
                                        </div>
                                    </form>
                                    <!-- END FORM-->
                                </div>
                            </div>
                
                        </div>
                    </div>
                    <!-- END PAGE BASE CONTENT -->
                </div>
                <!-- END CONTENT BODY -->
            </div>
            <!-- END CONTENT -->


            <!-- BEGIN Right SIDEBAR -->
            <!-- END Right SIDEBAR -->


        </div>
        <!-- END CONTAINER -->
        <!-- BEGIN FOOTER -->
       <?php $this->load->view('admin/footer'); ?>

      <?php $this->load->view('admin/footerlink'); ?>

    <script>
        function get_sub_category() 
        {
            var cat_id=$("#cat_id").val();

            $.ajax({
                url: "<?php echo site_url('admin/get_sub_category');?>",
                type: "post",
                data: {cat_id:cat_id},
                success: function(msg)
                {
                    // alert(msg);
                   $("#sub_cat_id").html(msg);
                }      
            });  
        }


        function get_district() 
        {
            var division_id=$("#division_id").val();

            $.ajax({
                url: "<?php echo site_url('admin/get_district');?>",
                type: "post",
                data: {division_id:division_id},
                success: function(msg)
                {
                    // alert(msg);
                   $("#district_id").html(msg);
                }      
            });  
        }



        function get_area() 
        {
            var district_id=$("#district_id").val();

            $.ajax({
                url: "<?php echo site_url('admin/get_area');?>",
                type: "post",
                data: {district_id:district_id},
                success: function(msg)
                {
                    //alert(msg);
                   $("#area_id").html(msg);
                }      
            });  
        }
    </script>
      <!-- Add Product Image Div -->

    <script>
        var div_count=1;
        function add_image_div() 
        {
           
           if(div_count==5)
           {
            $("#add_image_div").hide();
           }

            if(div_count==4)
            {
                $("#add_image_div").before('<label class="control-label col-md-3"></label>');
            }
              $("#add_image_div").before(
                '<div class="col-md-3">'
                +'<div class="fileinput fileinput-new" data-provides="fileinput">'
                    +'<div class="fileinput-new thumbnail" style="width: 200px; height: 150px;">'
                        +'<img src="back_assets/img_default.png" alt="Your Image" /> </div>'
                    +'<div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 200px; max-height: 150px;"> </div>'
                    +'<div>'
                        +'<span class="btn default btn-file">'
                            +'<span class="fileinput-new"><?=$this->lang->line('select_label').' '.$this->lang->line('img_tag_label');?></span>'
                            +'<span class="fileinput-exists"> <?=$this->lang->line('change_label');?> </span>'
                            +'<input type="file" name="userfile[]"> </span>'
                        +'<a href="javascript:;" class="btn red fileinput-exists" data-dismiss="fileinput"> <?=$this->lang->line('remove_label');?> </a>'
                    +'</div>'
                    +'<div class="clearfix margin-top-10">'
                        +'<span class="label label-danger">NOTE!</span> <?=$this->lang->line('img_resolution_msg');?> (550 x 380)px </div>'
                +'</div>'
                +'</div>');
              div_count++;

          }
    </script>

    </body>

</html>