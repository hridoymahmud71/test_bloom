<?php $this->load->view('admin/headlink'); ?>
<style>
    .input-group-btn {
        display: none;
    }
</style>
    <body class="page-container-bg-solid page-header-fixed page-sidebar-closed-hide-logo">
        <!-- BEGIN HEADER -->
       <?php $this->load->view('admin/head_nav'); ?>
        <!-- END HEADER -->
        <!-- BEGIN HEADER & CONTENT DIVIDER -->
        <div class="clearfix"> </div>
        <!-- END HEADER & CONTENT DIVIDER -->
        <!-- BEGIN CONTAINER -->
        <div class="page-container">
            <!-- BEGIN SIDEBAR -->
            <div class="page-sidebar-wrapper">
                <!-- BEGIN SIDEBAR -->
              <?php $this->load->view('admin/left_nav');?>
                <!-- END SIDEBAR -->
            </div>
            <!-- END SIDEBAR -->
            <!-- BEGIN CONTENT -->
            <div class="page-content-wrapper">
                <!-- BEGIN CONTENT BODY -->
                <div class="page-content">
                    <!-- BEGIN PAGE HEAD-->
                    <div class="page-head">
                        <!-- BEGIN PAGE TITLE -->
                        <?php $this->load->view('admin/page_title');?>
                        <!-- END PAGE TITLE -->
                        <!-- BEGIN PAGE TOOLBAR -->
                      
                        <!-- END PAGE TOOLBAR -->
                    </div>
                    <!-- END PAGE HEAD-->
                    <!-- BEGIN PAGE BREADCRUMB -->
                   
                    <!-- END PAGE BREADCRUMB -->
                    <!-- BEGIN PAGE BASE CONTENT -->
                    
                    <div class="row">
                        <div class="col-md-12">
                            
                            <div class="portlet box green ">
                                <div class="portlet-title">
                                    <div class="caption">
                                        <i class="fa fa-newspaper-o"></i> <?=$this->lang->line('new_label').' '.$this->lang->line('company_label').' '.$this->lang->line('btn_add_label').' '.$this->lang->line('form_label');?></div>
                                    <div class="tools">
                                        <a href="javascript:;" class="collapse"> </a>
                                        
                                        <a href="javascript:;" class="remove"> </a>
                                    </div>
                                </div>
                                <div class="portlet-body form">
                                    <!-- BEGIN FORM-->
                                    <form  class="form-horizontal form-bordered form-row-stripped" action="admin/update_company_info_post" method="post" enctype="multipart/form-data">
                                        <div class="form-body">

                                            


                                            <div class="form-group">
                                                <label class="control-label col-md-3">
                                                <?=$this->lang->line('company_label').' '.$this->lang->line('name_tag_label');?>
                                                </label>
                                                <div class="col-md-9">
                                                    <input name="name" value="<?=$company_info[0]['name'];?>" required="" type="text" class="form-control"> 
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label class="control-label col-md-3">
                                                <?=$this->lang->line('company_label').' '.$this->lang->line('address_tag_label');?>
                                                </label>
                                                <div class="col-md-9">
                                                    <input name="address" value="<?=$company_info[0]['address'];?>" type="text" class="form-control"> 
                                                </div>
                                            </div>


                                            <div class="form-group">
                                                <label class="control-label col-md-3"> <?=$this->lang->line('company_label').' '.$this->lang->line('phone_tag_label');?></label>
                                                <div class="col-md-4">
                                                    <input class="form-control" value="<?=$company_info[0]['phone'];?>" name="phone" id="mask_number" type="text" />
                                                    
                                                </div>
                                            </div>

                                            <div class="form-group">
                                                <label class="control-label col-md-3">
                                                <?=$this->lang->line('company_label').' '.$this->lang->line('email_label');?>
                                                </label>
                                                <div class="col-md-9">
                                                    <input name="email" value="<?=$company_info[0]['email'];?>" type="email" required class="form-control"> 
                                                </div>
                                            </div>


                                            <div class="form-group">
                                                <label class="control-label col-md-3">
                                                <?=$this->lang->line('company_label').' '.$this->lang->line('facebook_link_label');?>
                                                </label>
                                                <div class="col-md-9">
                                                    <input name="fb_link" value="<?=$company_info[0]['fb_link'];?>" type="text" class="form-control"> 
                                                </div>
                                            </div>


                                            <div class="form-group">
                                                <label class="control-label col-md-3">
                                                <?=$this->lang->line('company_label').' '.$this->lang->line('twitter_link_label');?>
                                                </label>
                                                <div class="col-md-9">
                                                    <input name="twitter_link" value="<?=$company_info[0]['twitter_link'];?>" type="text" class="form-control"> 
                                                </div>
                                            </div>

                                             <div class="form-group">
                                                <label class="control-label col-md-3">
                                                <?=$this->lang->line('company_label').' '.$this->lang->line('linked_link_label');?>
                                                </label>
                                                <div class="col-md-9">
                                                    <input name="linked_link" value="<?=$company_info[0]['linked_link'];?>" type="text" class="form-control"> 
                                                </div>
                                            </div>



                                            <div class="form-group">
                                                <label class="control-label col-md-3">
                                                <?=$this->lang->line('company_label').' '.$this->lang->line('profile_tag_label').' '.$this->lang->line('logo_label');?>
                                                    
                                                </label>
                                                <div class="col-md-9">
                                                <div class="fileinput fileinput-new" data-provides="fileinput">
                                                    <div class="fileinput-new thumbnail" style="width: 200px; height: 150px;">
                                                    <?php if($company_info[0]['logo']=='' || $company_info[0]['logo']== null)
                                                        { ?>
                                                        <img src="back_assets/img_default.png" alt="Your Image" />
                                                        <?php }else {  ?>
                                                        <img src="uploads/<?=$company_info[0]['logo'];?>" alt="Your Image" />
                                                        <?php } ?>
                                                    </div>
                                                    <input type="hidden" name="main_image" value="<?=$company_info[0]['logo'];?>">
                                                    <div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 200px; max-height: 150px;"> </div>
                                                    <div>
                                                        <span class="btn default btn-file">
                                                            <span class="fileinput-new"><?=$this->lang->line('select_label').' '.$this->lang->line('img_tag_label');?></span>
                                                            <span class="fileinput-exists"> <?=$this->lang->line('change_label');?> </span>
                                                            <input type="file" name="userfile"> </span>
                                                        <a href="javascript:;" class="btn red fileinput-exists" data-dismiss="fileinput"> <?=$this->lang->line('remove_label');?> </a>
                                                    </div>
                                                    
                                                </div>
                                                </div>
                                            </div>


                                            <div class="form-group">
                                                <label class="control-label col-md-3">
                                                <?=$this->lang->line('company_label').' '.$this->lang->line('details_tag_label');?>
                                                    
                                                </label>
                                                <div class="col-md-9">
                                                    
                                                   <textarea name="details" id="summernote_1"><?=$company_info[0]['details'];?></textarea>
                                                </div>
                                            </div>
                                            


                                           
                                           
                                        </div>
                                        <div class="form-actions">
                                            <div class="row">
                                                <div class="col-md-offset-3 col-md-9">
                                                    <button type="submit" class="btn green">
                                                        <i class="fa fa-check"></i> <?=$this->lang->line('btn_submit_label');?></button>
                                                    
                                                </div>
                                            </div>
                                        </div>
                                    </form>
                                    <!-- END FORM-->
                                </div>
                            </div>
                
                        </div>
                    </div>
                    <!-- END PAGE BASE CONTENT -->
                </div>
                <!-- END CONTENT BODY -->
            </div>
            <!-- END CONTENT -->


            <!-- BEGIN Right SIDEBAR -->
            <!-- END Right SIDEBAR -->


    <div id="myModal_autocomplete" class="modal fade" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
            
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                        <h4 class="modal-title"><?=$this->lang->line('company_label').' '.$this->lang->line('tag_label');?></h4>
                    </div>
                    <div class="modal-body">
                            <div class="form-group" style="overflow: hidden;">
                                <label class="col-sm-4 control-label"><?=$this->lang->line('tag_label').' '.$this->lang->line('name_tag_label');?></label>
                                <div class="col-sm-8">
                                    <div class="input-group">
                                        <span class="input-group-addon">
                                            <i class="fa fa-tag"></i>
                                        </span>
                                        <input type="text" id="typeahead_example_modal_1" class="form-control" /> </div>
                                </div>
                            </div>
           
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn grey-salsa btn-outline" data-dismiss="modal"><?=$this->lang->line('btn_close_label');?></button>
                        <button type="submit" onclick="save_new_tag()" class="btn green">
                            <i class="fa fa-check"></i><?=$this->lang->line('btn_save_label');?></button>
                    </div>
                </div>
          
        </div>
    </div>






        </div>
        <!-- END CONTAINER -->
        <!-- BEGIN FOOTER -->
       <?php $this->load->view('admin/footer'); ?>

      <?php $this->load->view('admin/footerlink'); ?>

    <script>
        function get_sub_category() 
        {
            var cat_id=$("#cat_id").val();

            $.ajax({
                url: "<?php echo site_url('shop_admin/get_sub_category');?>",
                type: "post",
                data: {cat_id:cat_id},
                success: function(msg)
                {
                    // alert(msg);
                   $("#sub_cat_id").html(msg);
                }      
            });  
        }


        function get_district() 
        {
            var division_id=$("#division_id").val();

            $.ajax({
                url: "<?php echo site_url('shop_admin/get_district');?>",
                type: "post",
                data: {division_id:division_id},
                success: function(msg)
                {
                    // alert(msg);
                   $("#district_id").html(msg);
                }      
            });  
        }


        function add_new_tag() 
        {
            $tag_id=$("#tag").val();
            if($tag_id=='new')
            {
                $("#myModal_autocomplete").modal('show');
            }
        }

        function save_new_tag() 
        {
            var tag_name=$("#typeahead_example_modal_1").val();

            $.ajax({
                url: "<?php echo site_url('shop_admin/insert_tag_name');?>",
                type: "post",
                data: {tag_name:tag_name},
                success: function(msg)
                {
                    //alert(msg);
                   $("#tag").html(msg);
                   $("#myModal_autocomplete").modal('hide');
                }      
            });  
        }



        function get_area() 
        {
            var district_id=$("#district_id").val();

            $.ajax({
                url: "<?php echo site_url('shop_admin/get_area');?>",
                type: "post",
                data: {district_id:district_id},
                success: function(msg)
                {
                    //alert(msg);
                   $("#area_id").html(msg);
                }      
            });  
        }
    </script>
      <!-- Add Product Image Div -->

    <script>
        var div_count=1;
        function add_image_div() 
        {
           
           if(div_count==9)
           {
            $("#add_image_div").hide();
           }

            if(div_count==4 || div_count==7)
            {
                $("#add_image_div").before('<label class="control-label col-md-3"></label>');
            }
              $("#add_image_div").before(
                '<div class="col-md-3">'
                +'<div class="fileinput fileinput-new" data-provides="fileinput">'
                    +'<div class="fileinput-new thumbnail" style="width: 200px; height: 150px;">'
                        +'<img src="back_assets/img_default.png" alt="Your Image" /> </div>'
                    +'<div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 200px; max-height: 150px;"> </div>'
                    +'<div>'
                        +'<span class="btn default btn-file">'
                            +'<span class="fileinput-new"><?=$this->lang->line('select_label').' '.$this->lang->line('img_tag_label');?></span>'
                            +'<span class="fileinput-exists"> <?=$this->lang->line('change_label');?> </span>'
                            +'<input type="file" name="userfile[]"> </span>'
                        +'<a href="javascript:;" class="btn red fileinput-exists" data-dismiss="fileinput"> <?=$this->lang->line('remove_label');?> </a>'
                    +'</div>'
                    +'<div class="clearfix margin-top-10">'
                        +'<span class="label label-danger">NOTE!</span> <?=$this->lang->line('img_resolution_msg');?> (550 x 380)px </div>'
                +'</div>'
                +'</div>');
              div_count++;

          }
    </script>


    <script type="text/javascript">
        var ComponentsTypeahead = function () {

        var handleTwitterTypeahead = function() {


        }

        var handleTwitterTypeaheadModal = function() {

            // Example #1
            // instantiate the bloodhound suggestion engine
            var numbers = new Bloodhound({
              datumTokenizer: function(d) { return Bloodhound.tokenizers.whitespace(d.num); },
              queryTokenizer: Bloodhound.tokenizers.whitespace,
              local: [
               <?php foreach ($tag_list as $key => $row) { ?>
                { num: '<?=$row['tag_name'];?>' },
                <?php } ?>
                
              ]
            });
             
            // initialize the bloodhound suggestion engine
            numbers.initialize();
             
            // instantiate the typeahead UI
            if (App.isRTL()) {
              $('#typeahead_example_modal_1').attr("dir", "rtl");  
            }
            $('#typeahead_example_modal_1').typeahead(null, {
              displayKey: 'num',
              hint: (App.isRTL() ? false : true),
              source: numbers.ttAdapter()
            });

         

        }

        return {
            //main function to initiate the module
            init: function () {
                handleTwitterTypeahead();
                handleTwitterTypeaheadModal();
            }
        };

        }();

        jQuery(document).ready(function() {    
           ComponentsTypeahead.init(); 
        });


    </script>

    </body>

</html>