<?php $this->load->view('admin/headlink'); ?>

    <body class="page-container-bg-solid page-header-fixed page-sidebar-closed-hide-logo">
        <!-- BEGIN HEADER -->
       <?php $this->load->view('admin/head_nav'); ?>
        <!-- END HEADER -->
        <!-- BEGIN HEADER & CONTENT DIVIDER -->
        <div class="clearfix"> </div>
        <!-- END HEADER & CONTENT DIVIDER -->
        <!-- BEGIN CONTAINER -->
        <div class="page-container">
            <!-- BEGIN SIDEBAR -->
            <div class="page-sidebar-wrapper">
                <!-- BEGIN SIDEBAR -->
              <?php $this->load->view('admin/left_nav');?>
                <!-- END SIDEBAR -->
            </div>
            <!-- END SIDEBAR -->
            <!-- BEGIN CONTENT -->
            <div class="page-content-wrapper">
                <!-- BEGIN CONTENT BODY -->
                <div class="page-content">
                    <!-- BEGIN PAGE HEAD-->
                    <div class="page-head">
                        <!-- BEGIN PAGE TITLE -->
                        <?php $this->load->view('admin/page_title');?>
                        <!-- END PAGE TITLE -->
                        <!-- BEGIN PAGE TOOLBAR -->
                      
                        <!-- END PAGE TOOLBAR -->
                    </div>
                    <!-- END PAGE HEAD-->
                    <!-- BEGIN PAGE BREADCRUMB -->
                   
                    <!-- END PAGE BREADCRUMB -->
                    <!-- BEGIN PAGE BASE CONTENT -->
                    
                    <div class="row">
                        <div class="col-md-12">

                            <?php if($this->session->userdata('scc_alt')){ ?>
                            <div class="alert alert-success alert-dismissable">
                                <button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>
                                <a href="javascript:;" class="alert-link"><?=$this->session->userdata('scc_alt');?></a>
                            </div>
                            <?php } $this->session->unset_userdata('scc_alt');?>


                            <!-- BEGIN EXAMPLE TABLE PORTLET-->
                            <div class="portlet light bordered">
                                <div class="portlet-title">
                                    <a href="#magazine_cat_add_modal" role="button" class="btn green" data-toggle="modal"> 
                                        <i class="fa fa-plus-square"></i> <?=$add_btn;?>
                                    </a>
                                    <div class="tools"> </div>
                                </div>
                                <div class="portlet-body">
                                    <table class="table table-striped table-bordered table-hover" id="sample_1">
                                        <thead>
                                            <tr>
                                                <th><?=$this->lang->line('id_tag_label');?></th>
                                                <th><?=$this->lang->line('name_tag_label');?></th>
                                                <th><?=$this->lang->line('total_label').' '.$this->lang->line('member_label');?></th>
                                                <th><?=$this->lang->line('total_label').' '.$this->lang->line('post_label');?></th>
                                                <th><?=$this->lang->line('create_date_tag_label');?></th>
                                                <th class="text-center"><?=$this->lang->line('action_tag_label');?></th>
                                                
                                            </tr>
                                        </thead>
                                        
                                        <tbody>
                                            
                                            <?php foreach ($groups_list as $key => $row) { ?>
                                            <tr>
                                                <td><?=($key+1);?></td>
                                                <td><a onclick="m_cat_edit_modal('<?=$row['group_id'];?>','<?=$row['group_name'];?>')" href="javascript:;"><?=$row['group_name'];?></a></td>
                                                <td>
                                                    <?php $m_array=explode(',', $row['member_id']);?>
                                                    <?=count($m_array);?>
                                                </td>
                                                 <td>
                                                    <?=$row['total_post'];?>
                                                </td>
                                                <td>
                                                  <?php 
                                                    $only_date_array=explode(' ', $row['created_at']);
                                                    $only_date=$only_date_array[0];
                                                    echo date('d M Y', strtotime($only_date));
                                                  ?>
                                                </td>
                                                <td class="text-center">
                                                    <a onclick="m_cat_edit_modal('<?=$row['group_id'];?>','<?=$row['group_name'];?>')" href="javascript:;" class="btn btn-xs green tooltips" data-container="body" data-placement="top" data-original-title="Edit">
                                                        <i class="fa fa-edit"></i>
                                                    </a>
                                                    <a href="admin/delete_category/<?=$row['group_id'];?>" class="btn btn-xs red"  data-toggle="confirmation" data-original-title="<?=$this->lang->line('delete_confirm_msg');?> ?" title=""><i class="fa fa-trash"></i></a>
                                                </td>
                                               
                                            </tr>
                                           
                                           <?php } ?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            <!-- END EXAMPLE TABLE PORTLET-->
                           
                        </div>
                    </div>
                    <!-- END PAGE BASE CONTENT -->
                </div>
                <!-- END CONTENT BODY -->
            </div>
            <!-- END CONTENT -->


            <!-- BEGIN Right SIDEBAR -->
            <!-- END Right SIDEBAR -->

        <!-- Magazine Category Add Modal Start-->
            <div id="magazine_cat_add_modal" class="modal fade" role="dialog" aria-hidden="true">
                <div class="modal-dialog">
                <form action="admin/add_category_post" method="post" class="form-horizontal form-row-seperated">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                            <h4 class="modal-title"><?=$add_btn;?></h4>
                        </div>
                        <div class="modal-body">
                                <div class="form-group">
                                    <label class="col-sm-4 control-label"><?=$this->lang->line('magazine_cat_name_label');?></label>
                                    <div class="col-sm-8">
                                        <div class="input-group">
                                            <span class="input-group-addon">
                                                <i class="fa fa-tag"></i>
                                            </span>
                                            <input type="text" id="typeahead_example_modal_1" name="cat_name" class="form-control" /> </div>
                                    </div>
                                </div>
               
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn grey-salsa btn-outline" data-dismiss="modal"><?=$this->lang->line('btn_close_label');?></button>
                            <button type="submit" class="btn green">
                                <i class="fa fa-check"></i><?=$this->lang->line('btn_save_label');?></button>
                        </div>
                    </div>
                </form>


                </div>
            </div>
        <!-- Magazine Category Add Modal End-->







        <!-- Magazine Category Edit Modal Start-->
            <div id="magazine_cat_edit_modal" class="modal fade" role="dialog" aria-hidden="true">
                <div class="modal-dialog">
                <form action="admin/update_category_post" method="post" class="form-horizontal form-row-seperated">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                            <h4 class="modal-title"><?=$this->lang->line('btn_edit_label').' '.$this->lang->line('groups_label');?></h4>
                        </div>
                        <div class="modal-body">
                                <div class="form-group">
                                    <label class="col-sm-4 control-label"><?=$this->lang->line('magazine_cat_name_label');?></label>
                                    <div class="col-sm-8">
                                        <div class="input-group">
                                            <span class="input-group-addon">
                                                <i class="fa fa-tag"></i>
                                            </span>
                                            <input type="text" id="m_cat_edit_name" name="cat_name" class="form-control" /> 

                                            <input type="hidden" id="cat_id_hidden" name="cat_id">
                                        </div>
                                    </div>
                                </div>
               
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn grey-salsa btn-outline" data-dismiss="modal"><?=$this->lang->line('btn_close_label');?></button>
                            <button type="submit" class="btn green">
                                <i class="fa fa-check"></i><?=$this->lang->line('btn_update_label');?></button>
                        </div>
                    </div>
                </form>


                </div>
            </div>
        <!-- Magazine Category Edit Modal End-->



        </div>
        <!-- END CONTAINER -->
        <!-- BEGIN FOOTER -->
       <?php $this->load->view('admin/footer'); ?>

       <?php $this->load->view('admin/footerlink'); ?>

        <script>
            function m_cat_edit_modal(id,name) {
               $("#cat_id_hidden").val(id);
               $("#m_cat_edit_name").val(name);
               $('#magazine_cat_edit_modal').modal('show'); 
            }
        </script>
        <script type="text/javascript">
            var ComponentsTypeahead = function () {

            var handleTwitterTypeahead = function() {
            }

            var handleTwitterTypeaheadModal = function() {

                // Example #1
                // instantiate the bloodhound suggestion engine
                var numbers = new Bloodhound({
                  datumTokenizer: function(d) { return Bloodhound.tokenizers.whitespace(d.num); },
                  queryTokenizer: Bloodhound.tokenizers.whitespace,
                  local: [
                   <?php foreach ($groups_list as $key => $row) { ?>
                    { num: '<?=$row['group_name'];?>' },
                    <?php } ?>
                    
                  ]
                });
                 
                // initialize the bloodhound suggestion engine
                numbers.initialize();
                 
                // instantiate the typeahead UI
                if (App.isRTL()) {
                  $('#typeahead_example_modal_1').attr("dir", "rtl");  
                }
                $('#typeahead_example_modal_1').typeahead(null, {
                  displayKey: 'num',
                  hint: (App.isRTL() ? false : true),
                  source: numbers.ttAdapter()
                });
            }

            return {
                //main function to initiate the module
                init: function () {
                    handleTwitterTypeahead();
                    handleTwitterTypeaheadModal();
                }
            };

            }();

            jQuery(document).ready(function() {    
               ComponentsTypeahead.init(); 
            });
        </script>
    </body>

</html>