<?php $this->load->view('admin/headlink'); ?>

    <body class="page-container-bg-solid page-header-fixed page-sidebar-closed-hide-logo">
        <!-- BEGIN HEADER -->
       <?php $this->load->view('admin/head_nav'); ?>
        <!-- END HEADER -->
        <!-- BEGIN HEADER & CONTENT DIVIDER -->
        <div class="clearfix"> </div>
        <!-- END HEADER & CONTENT DIVIDER -->
        <!-- BEGIN CONTAINER -->
        <div class="page-container">
            <!-- BEGIN SIDEBAR -->
            <div class="page-sidebar-wrapper">
                <!-- BEGIN SIDEBAR -->
              <?php $this->load->view('admin/left_nav');?>
                <!-- END SIDEBAR -->
            </div>
            <!-- END SIDEBAR -->
            <!-- BEGIN CONTENT -->
            <div class="page-content-wrapper">
                <!-- BEGIN CONTENT BODY -->
                <div class="page-content">
                    <!-- BEGIN PAGE HEAD-->
                    <div class="page-head">
                        <!-- BEGIN PAGE TITLE -->
                        <?php $this->load->view('admin/page_title');?>
                        <!-- END PAGE TITLE -->
                        <!-- BEGIN PAGE TOOLBAR -->
                      
                        <!-- END PAGE TOOLBAR -->
                    </div>
                    <!-- END PAGE HEAD-->
                    <!-- BEGIN PAGE BREADCRUMB -->
                   
                    <!-- END PAGE BREADCRUMB -->
                    <!-- BEGIN PAGE BASE CONTENT -->
                    
                    <div class="row">
                        <div class="col-md-12">
                            
                            <div class="portlet box green ">
                                <div class="portlet-title">
                                    <div class="caption">
                                        <i class="fa fa-newspaper-o"></i> <?=$this->lang->line('magazine_label').' '.$this->lang->line('btn_edit_label').' '.$this->lang->line('form_label');?></div>
                                    <div class="tools">
                                        <a href="javascript:;" class="collapse"> </a>
                                        
                                        <a href="javascript:;" class="remove"> </a>
                                    </div>
                                </div>
                                <div class="portlet-body form">
                                    <!-- BEGIN FORM-->
                                    <form  class="form-horizontal form-bordered form-row-stripped" action="admin/update_magazine_post/<?=$magazine_details[0]['m_id'];?>" method="post" enctype="multipart/form-data">
                                        <div class="form-body">


                                            <div class="form-group">
                                                <label class="control-label col-md-3">
                                                <?=$this->lang->line('magazine_label').' '.$this->lang->line('title_tag_label');?>
                                                    
                                                </label>
                                                <div class="col-md-9">
                                                    <input value="<?=$magazine_details[0]['title'];?>" name="title" required="" type="text" class="form-control"> </div>
                                            </div>


                                            <div class="form-group">
                                                <label class="control-label col-md-3">
                                                <?=$this->lang->line('magazine_label').' '.$this->lang->line('cat_tag_label');?>
                                                    
                                                </label>
                                                <div class="col-md-9">
                                                    <select required="" id="single" name="cat_id" class="form-control select2" data-placeholder="<?=$this->lang->line('choose_msg').' '.$this->lang->line('cat_tag_label');?>">
                                                        <option></option>
                                                        <?php foreach ($category_list as $key => $row) { ?>
                                                            <option <?php if($magazine_details[0]['cat_id']==$row['id']){echo 'selected';}?> value="<?=$row['id'];?>"><?=$row['cat_name'];?></option>
                                                 <?php } ?>
                                                    </select>
                                                </div>
                                            </div>
                                      
                                            <?php 
                                                $m_tag=explode(',', $magazine_details[0]['tag_id']);
                                            ?>
                                            <div class="form-group">
                                                <label class="control-label col-md-3">
                                                <?=$this->lang->line('magazine_label').' '.$this->lang->line('tag_label');?>
                                                    
                                                </label>
                                                <div class="col-md-9">
                                                    <select  name="tag_id[]" id="select2-multiple" class="form-control select2-multiple" multiple data-placeholder="<?=$this->lang->line('choose_msg').' '.$this->lang->line('tag_label');?>">
                                                        <option></option>
                                                        
                                                          
                                                        <?php foreach ($tag_list as $key => $row) {?>
                                                            <option <?php foreach ($m_tag as  $t) {
                                                                if($t==$row['id']){echo 'selected';}}?> value="<?=$row['id'];?>"><?=$row['tag_name'];?></option>

                                                        <?php } ?>
                                                     
                                                    </select>
                                                </div>
                                            </div>
                                            
                                            <div class="form-group">
                                                <label class="control-label col-md-3">
                                                <?=$this->lang->line('magazine_label').' '.$this->lang->line('img_tag_label');?>
                                                    
                                                </label>
                                                <div class="col-md-9">
                                                <input type="hidden" name="pre_img" value="<?=$magazine_details[0]['image'];?>">
                                                <div class="fileinput fileinput-new" data-provides="fileinput">
                                                    <div class="fileinput-new thumbnail" style="width: 200px; height: 150px;">
                                                        <img src="uploads/magazine/<?=$magazine_details[0]['image'];?>" alt="Your Image" /> </div>
                                                    <div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 200px; max-height: 150px;"> </div>
                                                    <div>
                                                        <span class="btn default btn-file">
                                                            <span class="fileinput-new"><?=$this->lang->line('select_label').' '.$this->lang->line('img_tag_label');?></span>
                                                            <span class="fileinput-exists"> <?=$this->lang->line('change_label');?> </span>
                                                            <input type="file" name="file"> </span>
                                                        <a href="javascript:;" class="btn red fileinput-exists" data-dismiss="fileinput"> <?=$this->lang->line('remove_label');?> </a>
                                                    </div>
                                                    <div class="clearfix margin-top-10">
                                                        <span class="label label-danger">NOTE!</span> <?=$this->lang->line('img_resolution_msg');?> (550 x 380)px </div>
                                                </div>
                                                </div>
                                            </div>


                                            <div class="form-group">
                                                <label class="control-label col-md-3">
                                                <?=$this->lang->line('magazine_label').' '.$this->lang->line('details_tag_label');?>
                                                    
                                                </label>
                                                <div class="col-md-9">
                                                    
                                                   <textarea name="details" id="summernote_1"><?=$magazine_details[0]['description'];?> </textarea>
                                                </div>
                                            </div>


                                           
                                           
                                        </div>
                                        <div class="form-actions">
                                            <div class="row">
                                                <div class="col-md-offset-3 col-md-9">
                                                    <button type="submit" class="btn green">
                                                        <i class="fa fa-check"></i> <?=$this->lang->line('btn_update_label');?></button>
                                                    
                                                </div>
                                            </div>
                                        </div>
                                    </form>
                                    <!-- END FORM-->
                                </div>
                            </div>
                
                        </div>
                    </div>
                    <!-- END PAGE BASE CONTENT -->
                </div>
                <!-- END CONTENT BODY -->
            </div>
            <!-- END CONTENT -->


            <!-- BEGIN Right SIDEBAR -->
            <!-- END Right SIDEBAR -->


        </div>
        <!-- END CONTAINER -->
        <!-- BEGIN FOOTER -->
       <?php $this->load->view('admin/footer'); ?>

      <?php $this->load->view('admin/footerlink'); ?>

    </body>

</html>