<?php $this->load->view('admin/headlink'); ?>

    <body class="page-container-bg-solid page-header-fixed page-sidebar-closed-hide-logo">
        <!-- BEGIN HEADER -->
       <?php $this->load->view('admin/head_nav'); ?>
        <!-- END HEADER -->
        <!-- BEGIN HEADER & CONTENT DIVIDER -->
        <div class="clearfix"> </div>
        <!-- END HEADER & CONTENT DIVIDER -->
        <!-- BEGIN CONTAINER -->
        <div class="page-container">
            <!-- BEGIN SIDEBAR -->
            <div class="page-sidebar-wrapper">
                <!-- BEGIN SIDEBAR -->
              <?php $this->load->view('admin/left_nav');?>
                <!-- END SIDEBAR -->
            </div>
            <!-- END SIDEBAR -->
            <!-- BEGIN CONTENT -->
            <div class="page-content-wrapper">
                <!-- BEGIN CONTENT BODY -->
                <div class="page-content">
                    <!-- BEGIN PAGE HEAD-->
                    <div class="page-head">
                        <!-- BEGIN PAGE TITLE -->
                        <?php $this->load->view('admin/page_title');?>
                        <!-- END PAGE TITLE -->
                        <!-- BEGIN PAGE TOOLBAR -->
                      
                        <!-- END PAGE TOOLBAR -->
                    </div>
                    <!-- END PAGE HEAD-->
                    <!-- BEGIN PAGE BREADCRUMB -->
                   
                    <!-- END PAGE BREADCRUMB -->
                    <!-- BEGIN PAGE BASE CONTENT -->
                    <div class="row widget-row">
                        <div class="col-md-3">
                            <!-- BEGIN WIDGET THUMB -->
                            <div class="widget-thumb widget-bg-color-white text-uppercase margin-bottom-20 bordered">
                                <h4 class="widget-thumb-heading"><?=$this->lang->line('total_label').' '.$this->lang->line('doctor_label');?></h4>
                                <div class="widget-thumb-wrap">
                                    <i class="widget-thumb-icon bg-green fa fa-user-md"></i>
                                    <div class="widget-thumb-body">
                                        <span class="widget-thumb-subtitle"><?=$this->lang->line('registered_label');?></span>
                                        <span class="widget-thumb-body-stat" data-counter="counterup" data-value="<?=$total_market;?>">0</span>
                                    </div>
                                </div>
                            </div>
                            <!-- END WIDGET THUMB -->
                        </div>
                        <div class="col-md-3">
                            <!-- BEGIN WIDGET THUMB -->
                            <div class="widget-thumb widget-bg-color-white text-uppercase margin-bottom-20 bordered">
                                <h4 class="widget-thumb-heading"><?=$this->lang->line('total_label').' '.$this->lang->line('patient_label');?></h4>
                                <div class="widget-thumb-wrap">
                                    <i class="widget-thumb-icon bg-red fa fa-user-plus"></i>
                                    <div class="widget-thumb-body">
                                        <span class="widget-thumb-subtitle"><?=$this->lang->line('registered_label');?></span>
                                        <span class="widget-thumb-body-stat" data-counter="counterup" data-value="<?=$total_shop;?>">0</span>
                                    </div>
                                </div>
                            </div>
                            <!-- END WIDGET THUMB -->
                        </div>
                        <div class="col-md-3">
                            <!-- BEGIN WIDGET THUMB -->
                            <div class="widget-thumb widget-bg-color-white text-uppercase margin-bottom-20 bordered">
                                <h4 class="widget-thumb-heading"><?=$this->lang->line('total_label').' '.$this->lang->line('hospital_label');?></h4>
                                <div class="widget-thumb-wrap">
                                    <i class="widget-thumb-icon bg-purple fa fa-hospital-o"></i>
                                    <div class="widget-thumb-body">
                                        <span class="widget-thumb-subtitle"><?=$this->lang->line('registered_label');?></span>
                                        <span class="widget-thumb-body-stat" data-counter="counterup" data-value="<?=$total_user;?>">0</span>
                                    </div>
                                </div>
                            </div>
                            <!-- END WIDGET THUMB -->
                        </div>
                        <div class="col-md-3">
                            <!-- BEGIN WIDGET THUMB -->
                            <div class="widget-thumb widget-bg-color-white text-uppercase margin-bottom-20 bordered">
                                <h4 class="widget-thumb-heading"><?=$this->lang->line('total_label').' '.$this->lang->line('pharmacy_label');?></h4>
                                <div class="widget-thumb-wrap">
                                    <i class="widget-thumb-icon bg-blue fa fa-map-marker"></i>
                                    <div class="widget-thumb-body">
                                        <span class="widget-thumb-subtitle"><?=$this->lang->line('registered_label');?></span>
                                        <span class="widget-thumb-body-stat" data-counter="counterup" data-value="<?=$total_agent;?>">0</span>
                                    </div>
                                </div>
                            </div>
                            <!-- END WIDGET THUMB -->
                        </div>
                    </div>

                     <div class="row widget-row">
                        <div class="col-md-3">
                            <!-- BEGIN WIDGET THUMB -->
                            <div class="widget-thumb widget-bg-color-white text-uppercase margin-bottom-20 bordered">
                                <h4 class="widget-thumb-heading"><?=$this->lang->line('total_label').' '.$this->lang->line('alt_doctor_label');?></h4>
                                <div class="widget-thumb-wrap">
                                    <i class="widget-thumb-icon bg-green fa fa-heartbeat"></i>
                                    <div class="widget-thumb-body">
                                        <span class="widget-thumb-subtitle"><?=$this->lang->line('registered_label');?></span>
                                        <span class="widget-thumb-body-stat" data-counter="counterup" data-value="<?=$total_market;?>">0</span>
                                    </div>
                                </div>
                            </div>
                            <!-- END WIDGET THUMB -->
                        </div>
                        <div class="col-md-3">
                            <!-- BEGIN WIDGET THUMB -->
                            <div class="widget-thumb widget-bg-color-white text-uppercase margin-bottom-20 bordered">
                                <h4 class="widget-thumb-heading"><?=$this->lang->line('total_label').' '.$this->lang->line('groups_label');?></h4>
                                <div class="widget-thumb-wrap">
                                    <i class="widget-thumb-icon bg-red fa fa-users"></i>
                                    <div class="widget-thumb-body">
                                        <span class="widget-thumb-subtitle"><?=$this->lang->line('registered_label');?></span>
                                        <span class="widget-thumb-body-stat" data-counter="counterup" data-value="<?=$total_shop;?>">0</span>
                                    </div>
                                </div>
                            </div>
                            <!-- END WIDGET THUMB -->
                        </div>
                        <div class="col-md-3">
                            <!-- BEGIN WIDGET THUMB -->
                            <div class="widget-thumb widget-bg-color-white text-uppercase margin-bottom-20 bordered">
                                <h4 class="widget-thumb-heading"><?=$this->lang->line('total_label').' '.$this->lang->line('bsl_label');?></h4>
                                <div class="widget-thumb-wrap">
                                    <i class="widget-thumb-icon bg-purple fa fa-hospital-o"></i>
                                    <div class="widget-thumb-body">
                                        <span class="widget-thumb-subtitle"><?=$this->lang->line('registered_label');?></span>
                                        <span class="widget-thumb-body-stat" data-counter="counterup" data-value="<?=$total_user;?>">0</span>
                                    </div>
                                </div>
                            </div>
                            <!-- END WIDGET THUMB -->
                        </div>
                        <div class="col-md-3">
                            <!-- BEGIN WIDGET THUMB -->
                            <div class="widget-thumb widget-bg-color-white text-uppercase margin-bottom-20 bordered">
                                <h4 class="widget-thumb-heading"><?=$this->lang->line('total_label').' '.$this->lang->line('agent_admin_label');?></h4>
                                <div class="widget-thumb-wrap">
                                    <i class="widget-thumb-icon bg-blue fa fa-user-secret"></i>
                                    <div class="widget-thumb-body">
                                        <span class="widget-thumb-subtitle"><?=$this->lang->line('registered_label');?></span>
                                        <span class="widget-thumb-body-stat" data-counter="counterup" data-value="<?=$total_agent;?>">0</span>
                                    </div>
                                </div>
                            </div>
                            <!-- END WIDGET THUMB -->
                        </div>
                    </div>


                    

                    <div class="row">
                        <div class="col-md-12">
                            <div class="portlet light portlet-fit bordered">
                                <div class="portlet-title">
                                    <div class="caption">
                                        <i class=" icon-layers font-green"></i>
                                        <span class="caption-subject font-green bold uppercase">Doctor-Patient Subscription&nbsp;(Monthly)</span>
                                    </div>
                                    <div class="actions">
                                        <a class="btn btn-circle btn-icon-only btn-default" href="javascript:;">
                                            <i class="icon-cloud-upload"></i>
                                        </a>
                                        <a class="btn btn-circle btn-icon-only btn-default" href="javascript:;">
                                            <i class="icon-wrench"></i>
                                        </a>
                                        <a class="btn btn-circle btn-icon-only btn-default" href="javascript:;">
                                            <i class="icon-trash"></i>
                                        </a>
                                    </div>
                                </div>
                                <div class="portlet-body">
                                    <div id="morris_chart_3" style="height:500px;"></div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <!-- END PAGE BASE CONTENT -->
                </div>
                <!-- END CONTENT BODY -->
            </div>
            <!-- END CONTENT -->
            <!-- BEGIN QUICK SIDEBAR -->
           
            <!-- END QUICK SIDEBAR -->
        </div>
        <!-- END CONTAINER -->
        <!-- BEGIN FOOTER -->
       <?php $this->load->view('admin/footer'); ?>

       <?php $this->load->view('admin/footerlink'); ?>
        <script>
            jQuery(document).ready(function() {
              new Morris.Bar({
                element: 'morris_chart_3',
                data: [
                <?php for ($i=0; $i < 12; $i++) {?>
                  { y: '<?=$month[$i];?>', a: <?=rand(1,500);?>, b: <?=rand(1,500);?> },
                  <?php } ?>
                ],
                xkey: 'y',
                ykeys: ['a', 'b'],
                labels: ['Doctor', 'Patient']
              });
            });

        </script>

    </body>

</html>