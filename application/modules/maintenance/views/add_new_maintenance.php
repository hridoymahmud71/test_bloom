<?php $this->load->view('front/headlink'); ?>

<div class="dashbord_poperty">
    <?php $this->load->view('front/top_menu'); ?>
    <div class="container">
        
        <?php $this->load->view('front/head_nav'); ?>
        
        <div class="row">  
            <div class="ss_container">
                <h3 class="extra_heading">New Maintenance Order</h3>
                <form class="form-horizontal" action="maintenance/insert_new_maintenance" method="post" enctype="multipart/form-data" id="myForm">
                    <div class="row">
                        <div class="col-md-12">
                            <!-- <div class="form-group"> -->
                                <!-- <br/><br/>  -->
                                <!-- <h5 for="inputLeaseName" class="col-sm-4 control-label">Status</h5> -->
                                <!-- <div class="col-sm-8"> -->
                                    <input type="hidden" name="maintenance_status" value="1">
                                    <!-- <select class="form-control" id="inputStatus" name="maintenance_status" required="">
                                        <option value="1">New</option>
                                        <option value="2">Open</option>
                                        <option value="3">To Do Later</option>
                                        <option value="4">Closed</option>
                                        <option value="5">Archive</option>
                                    </select> -->
                                <!-- </div> -->
                            <!-- </div> -->
                            <div class="form-group">
                                <br/><br/> 
                                <label for="inputLeaseName" class="col-sm-4 control-label">Urgency</label>
                                <div class="col-sm-8">
                                    <select class="form-control" id="inputUrgency" name="maintenance_urgency" required="">
                                        <option value="Low Priority">Low</option>
                                        <!-- <option value="Within a Week">Within a Week</option> -->
                                        <option value="High Priority">High</option>
                                        <option value="Urgent">Urgent</option>                                    
                                    </select>
                                </div>
                            </div>
                            <div class="clear"></div>
                            <div class="form-group">
                                <br/> 
                                <label for="inputLeaseName" class="col-sm-4 control-label">What's the issue?<br/>
                                    <small>(Title) - 30 characters max.</small></label>
                                <div class="col-sm-8">
                                    <input type="text" name="maintenance_issue" maxlength="30" placeholder="Broken Pipe in Kitchen" name="title" class="form-control"  value="" id="maintannane_issue">
                                    <span class="" id="maintainance_case" style="color:red"></span>
                                </div>
                            </div> 
                            <div class="clear"></div>
                            <div class="form-group">
                                <br/> 
                                <label for="inputLeaseName" class="col-sm-4 control-label">Property Address</label>
                                <div class="col-sm-8">
                                    <input type="text" id="maintenance_location" name="maintenance_location" maxlength="30" placeholder=" " name="title" class="form-control"
                                           value="<?php if (!empty($property)) {echo "{$property['property_address']} ,{$property['city']}"; } ?>" >
                                    <span class="" id="maintainance_location" style="color:red"></span>
                                </div>
                            </div>
                            <div class="clear"></div>
                            <div class="form-group">
                                <br/> 
                                <label for="inputLeaseName" class="col-sm-4 control-label">More details<br/> </label>
                                <div class="col-sm-8">
                                    <textarea class="form-control" name="maintenance_desc" rows="3"></textarea>
                                </div>
                            </div>   
                            <div class="clear"></div>
                            <div class="form-group">
                                <br/> 
                                <label for="inputLeaseName" class="col-sm-4 control-label">
                                    Upload photos, videos, documents in relation to the maintenance:<br/>
                                </label>
                                <div class="col-sm-8">
                                    <div class="dropzone" >
                                        <span style="display: none"  class="my-dz-message pull-right">
                                            <h3>Click Here to upload another file</h3>
                                        </span>
                                        <div class="dz-message" >
                                            <h3>Upload Files Here</h3>
                                        </div>
                                    </div>
                                    <div class="previews" id="preview"></div>
                                    <div>* Max 20 MB per file</div>

                                    <!--                                <div class="inputDnD">
                                                                        <input type='file' onchange="readURL(this);" name="maintenance_file"  data-title="Drag and drop a file" class="form-control-file text-primary font-weight-bold" id="inputFile"/>
                                                                        <img id="blah" src="" class="img-responsive" alt="your image" />
                                                                    </div>-->
                                </div>
                            </div>
                        </div>
                        <!--<div class="col-md-4">
                            <div class="tenant_access">
                                <p>Which tenants (will) have access to this maintenance issue?</p>

                                <ul class="repeate">Please First send invite to tenants to send maintenance issue.</ul>

                            </div>
                        </div>-->
                        <div class="clear"></div>

                    </div>




                    <div class="clear"></div>
<div class="text-right">
                    <br/><br/>
                    <button type="submit"  class="print_ledgers btn btn-primary btn-lg">
                        Log Maintenance Order
                    </button> 
                    <a href="Allmaintenance/<?=$property_id;?>" class="btn btn-light btn-lg mark_paid">
                        Cancel</a>


                    <br/>
                    <br/>
</div>
                </form>
            </div>
        </div>    
    </div>
</div>  

<?php $this->load->view('front/footerlink'); ?> 


<script type="text/javascript">
//    function readURL(input) {
//        if (input.files && input.files[0]) {
//            var reader = new FileReader();
//
//            reader.onload = function (e) {
//                $('#blah')
//                        .attr('src', e.target.result)
//                        .width(150)
//                        .height(200);
//            };
//
//            reader.readAsDataURL(input.files[0]);
//        }
//    }
</script>


<script>
    $("#maintainance_case").hide();
    $("#maintainance_location").hide();

    $("#myForm").submit(function () {

        var user_length = $("#maintannane_issue").val().length;
        var location = $('#maintenance_location').val().length;
        if (user_length == 0) {
            $("#maintainance_case").html("This Field is Required....");
            $("#maintainance_case").show();
            return false;
        } else {
            $("#maintainance_case").hide();
        }
        if (location == 0) {
            $("#maintainance_location").html("This Field is Required....");
            $("#maintainance_location").show();
            return false;
        } else {
            $("#maintainance_location").hide();
        }

    });
</script>

<script type="text/javascript">

    Dropzone.autoDiscover = false;

    var foto_upload = new Dropzone(".dropzone", {
        url: "<?php echo base_url('maintenance/multi') ?>",
        maxFilesize: 20,
        method: "post",
        acceptedFiles: ".jpg,.jpeg,.png,.gif,.pdf,.doc,.docx,.csv,.csvx,.mp4,.mov,.avi,.mpeg4,.flv",
        paramName: "userfile",
        dictInvalidFileType: "Type file ini tidak dizinkan",
        addRemoveLinks: true,
        init: function () {
            var count = 0;
            thisDropzone = this;

            this.on("success", function (file, json) {

                var obj = json;
//                       console.log(json);
                //         alert(obj.width);
                $('.previews').
                        append(
                                "<input type='hidden' name='image[]' value='" + obj + "'>\n\
                           <input type='hidden' name='width[]' value='" + file.width + "'>\n\
                           <input type='hidden' name='height[]' value='" + file.height + "'>"
                                );
                $(".my-dz-message").show();
                //                        $("select").select2();

            });
        }
        //Event ketika Memulai mengupload
//            foto_upload.on("sending",function(a,b,c){
//                a.token=Math.random();
//                c.append("token_foto",a.token); 
    });
</script>



