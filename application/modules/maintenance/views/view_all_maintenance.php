<?php $this->load->view('front/headlink'); ?>

    <div class="dashbord_poperty">
        <?php $this->load->view('front/top_menu'); ?>
        <div class="container">

            <?php $this->load->view('front/head_nav'); ?>

            <style>
                .maintenance_list_details h4 {
                    font-weight: bold;
                }

                .maintenance_list_details .col-md-4 p strong {
                    color: black;
                }
                .urgency-label{
                    margin-top: 40% !important;
                    display: inline-block;
                }

            </style>
            <div class="row">
                <div class="ss_container">
                    <h3 class="extra_heading">Maintenance Reported
                        <small>for the property at <?= $this->utility_model->getFullPropertyAddress($property_id)?></small>
                    </h3>
                    <div style="margin-bottom: 10px">
                        <?php if(!$is_archived) { ?>
                        <a href="Addmaintenance/<?= $property_id; ?>"
                           class="pull-right btn btn-primary"><span
                                    class="glyphicon glyphicon-plus"
                                    aria-hidden="true"></span>
                            Add New Maintenance Order
                        </a>
                        <?php } ?>
                    </div>
                    <div class="ss_bound_content" style="background: transparent">
                        <br/><br/><br/>
                        <!-- Nav tabs -->
                        <ul class="nav nav-tabs" role="tablist">
                            <li>View:</li>
                            <li role="presentation" class="active"><a href="#mall" aria-controls="All" role="tab"
                                                                      data-toggle="tab">All</a></li>
                            <li role="presentation"><a href="#mnew" aria-controls="New" role="tab"
                                                       data-toggle="tab">New</a>
                            </li>
                            <li role="presentation"><a href="#marchived" aria-controls="Archived" role="tab"
                                                       data-toggle="tab">Archived</a></li>
                            <li role="presentation"><a href="#mclosed" aria-controls="Closed" role="tab"
                                                       data-toggle="tab">Closed</a>
                            </li>
                        </ul>
                        <div class="row">
                            <?php if ($this->session->flashdata('add_success')) { ?>
                                <div class="panel panel-primary">
                                    <div class="panel-heading">Success!</div>
                                    <div class="panel-body">
                                        Successfully added Invoice
                                        <?php if ($this->session->flashdata('flash_maintenance_id')) { ?>                                            &nbsp;
                                            <a class="btn btn-primary btn-sm"
                                               href="editMaintenanceOrderInvoice/<?= $this->session->flashdata('flash_maintenance_id') ?>">Edit
                                                Invoice
                                            </a>
                                        <?php } ?>
                                    </div>
                                </div>
                            <?php } ?>
                            <?php if ($this->session->flashdata('update_success')) { ?>
                                <div class="panel panel-primary">
                                    <div class="panel-heading">Success!</div>
                                    <div class="panel-body">
                                        Successfully updated invoice
                                        <?php if ($this->session->flashdata('flash_maintenance_id')) { ?>
                                            &nbsp;
                                            <a class="btn btn-primary btn-sm"
                                               href="editMaintenanceOrderInvoice/<?= $this->session->flashdata('flash_maintenance_id') ?>">Edit
                                                Invoice
                                            </a>
                                        <?php } ?>
                                    </div>
                                </div>
                            <?php } ?>
                            <?php if ($this->session->flashdata('error_maintenance_has_invoice')) { ?>
                                <div class="panel panel-danger">
                                    <div class="panel-heading">Error!</div>
                                    <div class="panel-body">
                                        <?= $this->session->flashdata('error_maintenance_has_invoice') ?>
                                        <?php if ($this->session->flashdata('flash_maintenance_id')) { ?>
                                            &nbsp;
                                            <a class="btn btn-primary btn-sm"
                                               href="editMaintenanceOrderInvoice/<?= $this->session->flashdata('flash_maintenance_id') ?>">Edit
                                                Invoice
                                            </a>
                                        <?php } ?>
                                    </div>
                                </div>
                            <?php } ?>
                        </div>
                        <!-- Tab panes -->
                        <div class="tab-content">
                            <div role="tabpanel" class="tab-pane active" id="mall">
                                <div class="maintenance_list">
                                    <div class="maintenance_list_title">
                                        <div class="col-md-6"><strong>Title</strong></div>
                                        <div class="col-md-2"><strong>Status</strong></div>
                                        <div class="col-md-2"><strong>Urgency</strong></div>
                                        <div class="col-md-2"><strong>Photo / Video</strong></div>
                                    </div>
                                    <?php if (empty($result)) { ?>
                                        <h4 style="font-style: italic;;color:grey">There are currently no Maintenance Orders</h4>
                                    <?php } ?>
                                    <div class="maintenance_list_details">

                                        <ul>
                                            <?php
                                            foreach ($result as $data) { ?>
                                                <?php
                                                $time = strtotime($data['create_at']);
                                                $time2 = strtotime($data['updated_at']);

                                                if (!$time || $time < 0) {
                                                    $time = 0;
                                                }
                                                if (!$time2 || $time2 < 0) {
                                                    $time2 = 0;
                                                }

                                                $created_at = date("d/m/Y", $time);
                                                $updated_by = date("d/m/Y", $time2);
                                                ?>

                                                <li>
                                                    <a href="Editmaintenance/<?php echo $property_id; ?>/<?php echo $data['maintenance_id']; ?><?= $archived_string?>">
                                                        <div class="col-md-6">

                                                            <h4 id="maintenance_title" class="force_black text-center"><?php echo ucfirst($data['maintenance_issue'])  ?></h4>
                                                            <h5 id="maintenance_location"><span
                                                                        class="glyphicon glyphicon-map-marker"
                                                                        aria-hidden="true"></span>
                                                                <strong><?php echo $data['maintenance_location'] ?></strong>
                                                            </h5>
                                                            <div class=" changes_list row-fluid">
                                                                <div class="col-md-4">
                                                                    <p><strong>Created by:</strong></p>
                                                                    <?php if ($time2) { ?> <p><strong>Updated
                                                                            by:</strong>
                                                                    </p> <?php } ?>
                                                                </div>
                                                                <?php
                                                                ?>
                                                                <div class="col-md-8">
                                                                    <p>
                                                                        <span class="user_mod  created_by_uname"><strong><?php echo $data['user_fname'] ?></strong></span>
                                                                        <span style="margin-left: 4%;" 
                                                                              id="created_time"><strong><?php echo $created_at; ?></strong></span>
                                                                    </p>
                                                                    <?php if ($time2) { ?> <p class="updated_by_title">
                                                                        <span
                                                                                class="user_mod  updated_by_uname"><strong><?php echo $data['user_fname'] ?></strong></span>
                                                                        <span style="margin-left: 4%;"
                                                                              id="updated_time"><strong><?php echo $updated_by; ?></strong></span>
                                                                        </p><?php } ?>
                                                                </div>
                                                            </div>
                                                            <div class=" changes_list row-fluid">
                                                                <div class="col-md-4">
                                                                    <p><strong>Lease Name:</strong></p>
                                                                </div>
                                                                <div class="col-md-8">
                                                                    <p>
                                                                        <span class="user_mod lease_name_created"><strong><?= $this->session->userdata('lease_name'); ?></strong></span>
                                                                    </p>
                                                                </div>
                                                            </div>
                                                        </div>


                                                        <?php
                                                        if ($data['maintenance_status'] == 2) {
                                                            $statusClass = 'open_status label';
                                                        }
                                                        if ($data['maintenance_status'] == 4) {
                                                            $statusClass = 'close_status label';
                                                        }
                                                        if ($data['maintenance_status'] == 1) {
                                                            $statusClass = 'new_status label';
                                                        }
                                                        if ($data['maintenance_status'] == 3) {
                                                            $statusClass = 'todo_status label';
                                                        }
                                                        if ($data['maintenance_status'] == 5) {
                                                            $statusClass = 'archived_status label';
                                                        }
                                                        if ($data['maintenance_status'] == 0) {
                                                            $statusClass = '';
                                                        }
                                                        ?>

                                                    </a>
                                                    <div class="col-md-2">
                                                        <p class=" <?php echo $statusClass; ?>">
                                                            <?php
                                                            if ($data['maintenance_status'] == 1) {
                                                                echo "New";
                                                            } elseif ($data['maintenance_status'] == 2) {
                                                                echo "Open";
                                                            } elseif ($data['maintenance_status'] == 3) {
                                                                echo "To do later";
                                                            } elseif ($data['maintenance_status'] == 4) {
                                                                echo "Closed";
                                                            } else {
                                                                echo "Archived";
                                                            } ?>
                                                        </p>
                                                        <?php if (in_array($data['maintenance_status'], array(1, 2, 3)) && $data['maintenance_has_invoice'] == 0) { ?>
                                                            <p style="display: none">
                                                                <a style="color: green"
                                                                   href="addMaintenanceOrderInvoice/<?= $data['maintenance_id']; ?>">Add
                                                                    Invoice</a>
                                                            </p>
                                                        <?php } ?>
                                                        <?php if ($data['maintenance_has_invoice'] == 1) { ?>
                                                            <p style="display: none">
                                                                <a style="color: green"
                                                                   href="editMaintenanceOrderInvoice/<?= $data['maintenance_id']; ?>">Edit
                                                                    Invoice</a>
                                                            </p>
                                                        <?php } ?>
                                                    </div>
                                                    <div class="col-md-2">
                                                        <p class="label <?= !empty($data['maintenance_urgency']) && $data['maintenance_urgency'] == "High Priority"?"label-danger":"label-default"; ?> urgency-label">
                                                            <?= !empty($data['maintenance_urgency'])?$data['maintenance_urgency']:"Unknown"; ?>
                                                        </p>
                                                    </div>
                                                    <div class="col-md-2">

                                                        <?php
                                                        foreach ($result2 as $doc) {

                                                            if ($data['maintenance_id'] == $doc['maintenance_id']) {
                                                                $last3chars = strtolower(substr($doc['image'], -3));
                                                                ?>

                                                                <?php if (($last3chars) == 'jpg' || ($last3chars) == 'peg' || ($last3chars) == 'png') { ?>
                                                                    <a href="<?php echo base_url(); ?>uploads/maintenance_file/<?php echo $doc['image']; ?>">
                                                                        <img style="height: 50px; width: 50px;"
                                                                             src="<?php echo base_url(); ?>uploads/maintenance_file/<?php echo $doc['image']; ?>"></a>
                                                                <?php } ?>

                                                                <?php if (($last3chars) == 'doc' || ($last3chars) == 'ocx') { ?>
                                                                    <a href="<?php echo base_url(); ?>uploads/maintenance_file/<?php echo $doc['image']; ?>">
                                                                        <img style="height: 50px; width: 50px;"
                                                                             src="<?php echo base_url(); ?>assets/img/docxLogo.png"></a>
                                                                <?php } ?>

                                                                <?php if (($last3chars) == 'pdf') { ?>
                                                                    <a href="<?php echo base_url(); ?>uploads/maintenance_file/<?php echo $doc['image']; ?>">
                                                                        <img style="height: 50px; width: 50px;"
                                                                             src="<?php echo base_url(); ?>assets/img/pdfLogo.png ?>"></a>
                                                                <?php } ?>

                                                                <?php if (($last3chars) == 'csv' || ($last3chars) == 'svx') { ?>
                                                                    <a href="<?php echo base_url(); ?>uploads/maintenance_file/<?php echo $doc['image']; ?>">
                                                                        <img style="height: 50px; width: 50px;"
                                                                             src="<?php echo base_url(); ?>assets/img/excelLogo.png ?>"></a>
                                                                <?php } ?>

                                                                <?php if (($last3chars) == 'mp4' || ($last3chars) == 'mpg') { ?>
                                                                    <a href="<?php echo base_url(); ?>uploads/maintenance_file/<?php echo $doc['maintenance_comment_file']; ?>">
                                                                        <img style="height: 50px; width: 50px;"
                                                                             src="<?php echo base_url(); ?>assets/img/videoLogo.png ?>"></a>
                                                                <?php } ?>
                                                                <?php
                                                            }
                                                        }
                                                        ?>
                                                    </div>
                                                </li>

                                            <?php } ?>


                                        </ul>
                                    </div>
                                </div>
                            </div>
                            <div role="tabpanel" class="tab-pane" id="mnew">
                                <div class="maintenance_list">
                                    <div class="maintenance_list_title">
                                        <div class="col-md-6"><strong>Title</strong></div>
                                        <div class="col-md-2"><strong>Status</strong></div>
                                        <div class="col-md-2"><strong>Urgency</strong></div>
                                        <div class="col-md-2"><strong>Photo / Video</strong></div>
                                    </div>
                                    <div class="maintenance_list_details">
                                        <ul>
                                            <?php
                                            foreach ($new as $data2) {
                                                ?>

                                                <?php
                                                $time = strtotime($data2['create_at']);
                                                $time2 = strtotime($data2['updated_at']);

                                                if (!$time || $time < 0) {
                                                    $time = 0;
                                                }
                                                if (!$time2 || $time2 < 0) {
                                                    $time2 = 0;
                                                }

                                                $created_at = date("d/m/Y", $time);
                                                $updated_by = date("d/m/Y", $time2);
                                                ?>

                                                <li>
                                                    <a href="Editmaintenance/<?php echo $property_id; ?>/<?php echo $data2['maintenance_id']; ?><?= $archived_string?>">
                                                        <div class="col-md-6">

                                                            <h4 id="maintenance_title" class="force_black text-center"><?php echo ucfirst($data2['maintenance_issue']) ?></h4>
                                                            <h5 id="maintenance_location"><span
                                                                        class="glyphicon glyphicon-map-marker"
                                                                        aria-hidden="true"></span><strong><?php echo $data2['maintenance_location'] ?></strong>
                                                            </h5>
                                                            <div class=" changes_list row-fluid">
                                                                <div class="col-md-4">
                                                                    <p><strong>Created by:</strong></p>
                                                                    <?php if ($time2) { ?>
                                                                        <p><strong>Updated
                                                                            by:</strong>
                                                                        </p>
                                                                    <?php } ?>
                                                                </div>
                                                                <div class="col-md-8">
                                                                    <p>
                                                                        <span class="user_mod  created_by_uname"><strong><?php echo $data2['user_fname'] ?></strong></span>
                                                                        <span style="margin-left: 4%;" 
                                                                              id="created_time"><strong><?php echo $created_at; ?></strong></span>
                                                                    </p>
                                                                    <?php if ($time2) { ?>
                                                                        <p class="updated_by_title">
                                                                        <span class="user_mod  updated_by_uname">
                                                                            <strong>
                                                                                <?php echo $data2['user_fname'] ?>
                                                                            </strong>
                                                                        </span>
                                                                        <span style="margin-left: 4%;"
                                                                                  id="updated_time">
                                                                            <strong>
                                                                                <?php echo $updated_by; ?>
                                                                            </strong>
                                                                        </span>
                                                                        </p>
                                                                    <?php } ?>
                                                                </div>
                                                            </div>
                                                            <div class=" changes_list row-fluid">
                                                                <div class="col-md-4">
                                                                    <p><strong>Lease Name:</strong></p>
                                                                </div>
                                                                <div class="col-md-8">
                                                                    <p>
                                                                        <span class="user_mod lease_name_created"><strong><?= $this->session->userdata('lease_name'); ?></strong></span>
                                                                    </p>
                                                                </div>
                                                            </div>

                                                        </div>
                                                        <div class="col-md-2">
                                                            <p class=" new_status label">New</p>
                                                            <?php if (in_array($data2['maintenance_status'], array(1, 2, 3)) && $data2['maintenance_has_invoice'] == 0) { ?>
                                                                <p style="display: none">
                                                                    <a style="color: green"
                                                                       href="addMaintenanceOrderInvoice/<?= $data2['maintenance_id']; ?>">Add
                                                                        Invoice</a>
                                                                </p>
                                                            <?php } ?>
                                                            <?php if ($data2['maintenance_has_invoice'] == 1) { ?>
                                                                <p style="display: none">
                                                                    <a style="color: green"
                                                                       href="editMaintenanceOrderInvoice/<?= $data2['maintenance_id']; ?>">Edit
                                                                        Invoice</a>
                                                                </p>
                                                            <?php } ?>
                                                        </div>
                                                        <div class="col-md-2">
                                                            <p class="label <?= !empty($data2['maintenance_urgency']) && $data2['maintenance_urgency'] == "High Priority"?"label-danger":"label-default"; ?> urgency-label">
                                                                <?= !empty($data2['maintenance_urgency'])?$data2['maintenance_urgency']:"Unknown"; ?>
                                                            </p>
                                                        </div>
                                                        <div class="col-md-2">

                                                            <?php
                                                            foreach ($result2 as $doc) {

                                                                if ($data2['maintenance_id'] == $doc['maintenance_id']) {
                                                                    $last3chars = substr($doc['image'], -3);
                                                                    ?>

                                                                    <?php if (($last3chars) == 'jpg' || ($last3chars) == 'peg') { ?>
                                                                        <a href="<?php echo base_url(); ?>uploads/maintenance_file/<?php echo $doc['image']; ?>">
                                                                            <img style="height: 50px; width: 50px;"
                                                                                 src="<?php echo base_url(); ?>uploads/maintenance_file/<?php echo $doc['image']; ?>"></a>
                                                                    <?php } ?>

                                                                    <?php if (($last3chars) == 'doc' || ($last3chars) == 'ocx') { ?>
                                                                        <a href="<?php echo base_url(); ?>uploads/maintenance_file/<?php echo $doc['image']; ?>">
                                                                            <img style="height: 50px; width: 50px;"
                                                                                 src="<?php echo base_url(); ?>assets/img/docxLogo.png"></a>
                                                                    <?php } ?>

                                                                    <?php if (($last3chars) == 'pdf') { ?>
                                                                        <a href="<?php echo base_url(); ?>uploads/maintenance_file/<?php echo $doc['image']; ?>">
                                                                            <img style="height: 50px; width: 50px;"
                                                                                 src="<?php echo base_url(); ?>assets/img/pdfLogo.png ?>"></a>
                                                                    <?php } ?>

                                                                    <?php if (($last3chars) == 'csv' || ($last3chars) == 'svx') { ?>
                                                                        <a href="<?php echo base_url(); ?>uploads/maintenance_file/<?php echo $doc['image']; ?>">
                                                                            <img style="height: 50px; width: 50px;"
                                                                                 src="<?php echo base_url(); ?>assets/img/excelLogo.png ?>"></a>
                                                                    <?php } ?>

                                                                    <?php if (($last3chars) == 'mp4' || ($last3chars) == 'mpg') { ?>
                                                                        <a href="<?php echo base_url(); ?>uploads/maintenance_file/<?php echo $doc['maintenance_comment_file']; ?>">
                                                                            <img style="height: 50px; width: 50px;"
                                                                                 src="<?php echo base_url(); ?>assets/img/videoLogo.png ?>"></a>
                                                                    <?php } ?>
                                                                    <?php
                                                                }
                                                            }
                                                            ?>
                                                        </div>
                                                </li>
                                            <?php } ?>

                                        </ul>
                                    </div>
                                </div>
                            </div>
                            <div role="tabpanel" class="tab-pane" id="marchived">
                                <div class="maintenance_list">
                                    <div class="maintenance_list_title">
                                        <div class="col-md-6"><strong>Title</strong></div>
                                        <div class="col-md-2"><strong>Status</strong></div>
                                        <div class="col-md-2"><strong>Urgency</strong></div>
                                        <div class="col-md-2"><strong>Photo / Video</strong></div>
                                    </div>
                                    <div class="maintenance_list_details">
                                        <ul>
                                            <?php
                                            foreach ($archive as $data6) {
                                                ?>
                                                <?php
                                                $time = strtotime($data6['create_at']);
                                                $time2 = strtotime($data6['updated_at']);

                                                if (!$time || $time < 0) {
                                                    $time = 0;
                                                }
                                                if (!$time2 || $time2 < 0) {
                                                    $time2 = 0;
                                                }

                                                $created_at = date("d/m/Y", $time);
                                                $updated_by = date("d/m/Y", $time2);
                                                ?>
                                                <li>
                                                    <a href="Editmaintenance/<?php echo $property_id; ?>/<?php echo $data6['maintenance_id']; ?><?= $archived_string?>">
                                                        <div class="col-md-6">
                                                            <h4 id="maintenance_title" class="force_black text-center"><?php echo ucfirst($data6['maintenance_issue'])  ?></h4>
                                                            <h5 id="maintenance_location"><span
                                                                        class="glyphicon glyphicon-map-marker"
                                                                        aria-hidden="true"></span><strong></strong> <?php echo $data6['maintenance_location'] ?>
                                                            </h5>
                                                            <div class=" changes_list row-fluid">
                                                                <div class="col-md-4">
                                                                    <p><strong>Created by:</strong></p>
                                                                    <?php if ($time2) { ?> <p><strong>Updated
                                                                            by:</strong>
                                                                    </p> <?php } ?>
                                                                </div>
                                                                <?php
                                                                ?>
                                                                <div class="col-md-8">
                                                                    <p>
                                                                        <span class="user_mod  created_by_uname"><strong><?php echo $data6['user_fname'] ?></strong></span>
                                                                        <span style="margin-left: 4%;" 
                                                                              id="created_time"><strong><?php echo $created_at; ?></strong></span>
                                                                    </p>
                                                                    <?php if ($time2) { ?> <p class="updated_by_title">
                                                                        <span
                                                                                class="user_mod  updated_by_uname"><strong><?php echo $data6['user_fname'] ?></strong></span>
                                                                        <span style="margin-left: 4%;"
                                                                              id="updated_time"><strong><?php echo $updated_by; ?></strong></span>
                                                                        </p><?php } ?>
                                                                </div>
                                                            </div>
                                                            <div class=" changes_list row-fluid">
                                                                <div class="col-md-4">
                                                                    <p><strong>Lease Name:</strong></p>
                                                                </div>
                                                                <div class="col-md-8">
                                                                    <p>
                                                                        <span class="user_mod lease_name_created"><strong><?= $this->session->userdata('lease_name'); ?></strong></span>
                                                                    </p>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-2">
                                                            <p class=" archived_status label">Archived</p>
                                                            <?php if (in_array($data6['maintenance_status'], array(1, 2, 3)) && $data6['maintenance_has_invoice'] == 0) { ?>
                                                                <p style="display: none">
                                                                    <a style="color: green"
                                                                       href="addMaintenanceOrderInvoice/<?= $data6['maintenance_id']; ?>">Add
                                                                        Invoice</a>
                                                                </p>
                                                            <?php } ?>
                                                            <?php if ($data6['maintenance_has_invoice'] == 1) { ?>
                                                                <p style="display: none">
                                                                    <a style="color: green"
                                                                       href="editMaintenanceOrderInvoice/<?= $data6['maintenance_id']; ?>">Edit
                                                                        Invoice</a>
                                                                </p>
                                                            <?php } ?>
                                                        </div>
                                                        <div class="col-md-2">
                                                            <p class="label <?= !empty($data6['maintenance_urgency']) && $data6['maintenance_urgency'] == "High Priority"?"label-danger":"label-default"; ?> urgency-label">
                                                                <?= !empty($data6['maintenance_urgency'])?$data6['maintenance_urgency']:"Unknown"; ?>
                                                            </p>
                                                        </div>
                                                        <div class="col-md-2">
                                                            <?php
                                                            foreach ($result2 as $doc) {
                                                                if ($data6['maintenance_id'] == $doc['maintenance_id']) {
                                                                    $last3chars = substr($doc['image'], -3);
                                                                    ?>
                                                                    <?php if (($last3chars) == 'jpg' || ($last3chars) == 'peg') { ?>
                                                                        <a href="<?php echo base_url(); ?>uploads/maintenance_file/<?php echo $doc['image']; ?>">
                                                                            <img style="height: 50px; width: 50px;"
                                                                                 src="<?php echo base_url(); ?>uploads/maintenance_file/<?php echo $doc['image']; ?>"></a>
                                                                    <?php } ?>

                                                                    <?php if (($last3chars) == 'doc' || ($last3chars) == 'ocx') { ?>
                                                                        <a href="<?php echo base_url(); ?>uploads/maintenance_file/<?php echo $doc['image']; ?>">
                                                                            <img style="height: 50px; width: 50px;"
                                                                                 src="<?php echo base_url(); ?>assets/img/docxLogo.png"></a>
                                                                    <?php } ?>

                                                                    <?php if (($last3chars) == 'pdf') { ?>
                                                                        <a href="<?php echo base_url(); ?>uploads/maintenance_file/<?php echo $doc['image']; ?>">
                                                                            <img style="height: 50px; width: 50px;"
                                                                                 src="<?php echo base_url(); ?>assets/img/pdfLogo.png ?>"></a>
                                                                    <?php } ?>

                                                                    <?php if (($last3chars) == 'csv' || ($last3chars) == 'svx') { ?>
                                                                        <a href="<?php echo base_url(); ?>uploads/maintenance_file/<?php echo $doc['image']; ?>">
                                                                            <img style="height: 50px; width: 50px;"
                                                                                 src="<?php echo base_url(); ?>assets/img/excelLogo.png ?>"></a>
                                                                    <?php } ?>

                                                                    <?php if (($last3chars) == 'mp4' || ($last3chars) == 'mpg') { ?>
                                                                        <a href="<?php echo base_url(); ?>uploads/maintenance_file/<?php echo $doc['maintenance_comment_file']; ?>">
                                                                            <img style="height: 50px; width: 50px;"
                                                                                 src="<?php echo base_url(); ?>assets/img/videoLogo.png ?>"></a>
                                                                    <?php } ?>
                                                                    <?php
                                                                }
                                                            }
                                                            ?>
                                                        </div>
                                                    </a>
                                                </li>
                                            <?php } ?>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                            <div role="tabpanel" class="tab-pane" id="mclosed">
                                <div class="maintenance_list">
                                    <div class="maintenance_list_title">
                                        <div class="col-md-6"><strong>Title</strong></div>
                                        <div class="col-md-2"><strong>Status</strong></div>
                                        <div class="col-md-2"><strong>Urgency</strong></div>
                                        <div class="col-md-2"><strong>Photo / Video</strong></div>
                                    </div>
                                    <div class="maintenance_list_details">
                                        <ul>
                                            <?php
                                            foreach ($closed as $data5) {
                                                ?>
                                                <?php
                                                $time = strtotime($data5['create_at']);
                                                $time2 = strtotime($data5['updated_at']);

                                                if (!$time || $time < 0) {
                                                    $time = 0;
                                                }
                                                if (!$time2 || $time2 < 0) {
                                                    $time2 = 0;
                                                }

                                                $created_at = date("d/m/Y", $time);
                                                $updated_by = date("d/m/Y", $time2);
                                                ?>
                                                <li>
                                                    <a href="Editmaintenance/<?php echo $property_id; ?>/<?php echo $data5['maintenance_id']; ?><?= $archived_string?>">
                                                        <div class="col-md-6">
                                                            <h4 id="maintenance_title" class="force_black text-center"><?php echo ucfirst($data5['maintenance_issue']) ?></h4>
                                                            <h5 id="maintenance_location"><span
                                                                        class="glyphicon glyphicon-map-marker"
                                                                        aria-hidden="true"></span><strong><?php echo $data5['maintenance_location'] ?></strong>
                                                            </h5>
                                                            <div class=" changes_list row-fluid">
                                                                <div class="col-md-4">
                                                                    <p><strong>Created by:</strong></p>
                                                                    <?php if ($time2) { ?> <p><strong>Updated
                                                                            by:</strong>
                                                                    </p> <?php } ?>
                                                                </div>
                                                                <?php
                                                                ?>
                                                                <div class="col-md-8">
                                                                    <p>
                                                                        <span class="user_mod  created_by_uname"><strong><?php echo $data5['user_fname'] ?></strong></span>
                                                                        <span style="margin-left: 4%;" 
                                                                              id="created_time"><strong><?php echo $created_at; ?></strong></span>
                                                                    </p>
                                                                    <?php if ($time2) { ?> <p class="updated_by_title">
                                                                        <span
                                                                                class="user_mod  updated_by_uname"><strong><?php echo $data5['user_fname'] ?></strong></span>
                                                                        <span style="margin-left: 4%;"
                                                                              id="updated_time"><strong><?php echo $updated_by; ?></strong></span>
                                                                        </p><?php } ?>
                                                                </div>
                                                            </div>
                                                            <div class=" changes_list row-fluid">
                                                                <div class="col-md-4">
                                                                    <p><strong>Lease Name:</strong></p>
                                                                </div>
                                                                <div class="col-md-8">
                                                                    <p>
                                                                        <span class="user_mod lease_name_created"><strong><?= $this->session->userdata('lease_name'); ?></strong></span>
                                                                    </p>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-2">
                                                            <p class=" close_status label">Closed</p>
                                                            <?php if (in_array($data5['maintenance_status'], array(1, 2, 3)) && $data5['maintenance_has_invoice'] == 0) { ?>
                                                                <p style="display: none">
                                                                    <a style="color: green"
                                                                       href="addMaintenanceOrderInvoice/<?= $data5['maintenance_id']; ?>">Add
                                                                        Invoice</a>
                                                                </p>
                                                            <?php } ?>
                                                            <?php if ($data5['maintenance_has_invoice'] == 1) { ?>
                                                                <p style="display: none">
                                                                    <a style="color: green"
                                                                       href="editMaintenanceOrderInvoice/<?= $data5['maintenance_id']; ?>">Edit
                                                                        Invoice</a>
                                                                </p>
                                                            <?php } ?>
                                                        </div>
                                                        <div class="col-md-2">
                                                            <p class="label <?= !empty($data5['maintenance_urgency']) && $data5['maintenance_urgency'] == "High Priority"?"label-danger":"label-default"; ?> urgency-label">
                                                                <?= !empty($data5['maintenance_urgency'])?$data5['maintenance_urgency']:"Unknown"; ?>
                                                            </p>
                                                        </div>
                                                        <div class="col-md-2">
                                                            <?php
                                                            foreach ($result2 as $doc) {
                                                                if ($data5['maintenance_id'] == $doc['maintenance_id']) {
                                                                    $last3chars = substr($doc['image'], -3);
                                                                    ?>
                                                                    <?php if (($last3chars) == 'jpg' || ($last3chars) == 'peg') { ?>
                                                                        <a href="<?php echo base_url(); ?>uploads/maintenance_file/<?php echo $doc['image']; ?>">
                                                                            <img style="height: 50px; width: 50px;"
                                                                                 src="<?php echo base_url(); ?>uploads/maintenance_file/<?php echo $doc['image']; ?>"></a>
                                                                    <?php } ?>
                                                                    <?php if (($last3chars) == 'doc' || ($last3chars) == 'ocx') { ?>
                                                                        <a href="<?php echo base_url(); ?>uploads/maintenance_file/<?php echo $doc['image']; ?>">
                                                                            <img style="height: 50px; width: 50px;"
                                                                                 src="<?php echo base_url(); ?>assets/img/docxLogo.png"></a>
                                                                    <?php } ?>
                                                                    <?php if (($last3chars) == 'pdf') { ?>
                                                                        <a href="<?php echo base_url(); ?>uploads/maintenance_file/<?php echo $doc['image']; ?>">
                                                                            <img style="height: 50px; width: 50px;"
                                                                                 src="<?php echo base_url(); ?>assets/img/pdfLogo.png ?>"></a>
                                                                    <?php } ?>
                                                                    <?php if (($last3chars) == 'csv' || ($last3chars) == 'svx') { ?>
                                                                        <a href="<?php echo base_url(); ?>uploads/maintenance_file/<?php echo $doc['image']; ?>">
                                                                            <img style="height: 50px; width: 50px;"
                                                                                 src="<?php echo base_url(); ?>assets/img/excelLogo.png ?>"></a>
                                                                    <?php } ?>
                                                                    <?php if (($last3chars) == 'mp4' || ($last3chars) == 'mpg') { ?>
                                                                        <a href="<?php echo base_url(); ?>uploads/maintenance_file/<?php echo $doc['maintenance_comment_file']; ?>">
                                                                            <img style="height: 50px; width: 50px;"
                                                                                 src="<?php echo base_url(); ?>assets/img/videoLogo.png ?>"></a>
                                                                    <?php } ?>
                                                                    <?php
                                                                }
                                                            }
                                                            ?>
                                                        </div>
                                                    </a>
                                                </li>
                                            <?php } ?>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div style="margin-top: 50px">
                            <a class="btn btn-light pull-right" href="Dashboard/<?= $property_id ?>">Back To Dashboard</a>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>

<?php $this->load->view('front/footerlink'); ?>