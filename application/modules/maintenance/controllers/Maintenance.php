<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Maintenance extends MX_Controller
{

    //public $counter=0;
    function __construct()
    {
        parent::__construct();
        $this->load->model('Maintenance_model');
        $this->load->model('utility/utility_model');
        //$this->load->model('home/home_model');
        // $this->load->helper('inflector');
        // $this->load->library('encrypt'); 
        $this->load->library('email');
        $this->utility_model->check_auth();

    }

    public function index()
    {


        //$this->load->view('index');
        redirect('maintenance/view_all_maintenance');
    }

    public function add_maintenance($property_id)
    {
        $data['property_id'] = $property_id;
        $data['property'] = $this->Maintenance_model->getProperty($property_id);

        $this->session->set_userdata('property_id', $data['property_id']);
        $this->load->view('add_new_maintenance', $data);
    }

    public function insert_new_maintenance()
    {


        // $this->session->user_name('user_name');
        $property_id = $this->session->userdata('property_id');
        $data['property_id'] = $property_id;
        $data['maintenance_status'] = $this->input->post('maintenance_status');
        $data['maintenance_urgency'] = $this->input->post('maintenance_urgency');
        $data['maintenance_issue'] = $this->input->post('maintenance_issue');
        $data['maintenance_location'] = $this->input->post('maintenance_location');
        $data['maintenance_desc'] = $this->input->post('maintenance_desc');
        $data['created_by'] = $this->session->userdata('user_id');
        $data['updated_by'] = $this->session->userdata('user_id');

        $lease = $this->Maintenance_model->getActiveLease($property_id);
        if ($lease) {
            $data['lease_id'] = $lease['lease_id'];
        } else {
            $data['lease_id'] = 0;
        }


        $image = $this->input->post('image');
        //image upload code end here        

        $this->form_validation->set_rules('maintenance_status', 'Maintenance Status', 'required');
        $this->form_validation->set_rules('maintenance_urgency', 'Maintenance Urgency', 'required');
        $this->form_validation->set_rules('maintenance_issue', 'Maintenance Issue', 'required');
        $this->form_validation->set_rules('maintenance_location', 'Maintenance Location', 'required');
        //$this->form_validation->set_rules('maintenance_desc', 'Maintenance More', 'trim');
        //$this->form_validation->set_rules('maintenance_file', 'Maintenance Documents', 'trim|required');

        if ($this->form_validation->run() == FALSE) {
            //echo "validation false";
            $this->load->view('add_new_maintenance');
        } else {
            $maintenance_id = $this->Maintenance_model->insertId('maintenance', $data);

            $log_data['maintenance_id'] = $maintenance_id;
            $log_data['maintenance_create_status'] = 1;
            $log_data['maintenance_issue'] = $data['maintenance_issue'];
            $log_data['maintenance_status'] = $data['maintenance_status'];
            $log_data['maintenance_urgency'] = $data['maintenance_urgency'];
            $log_data['maintenance_location'] = $data['maintenance_location'];
            $log_data['maintenance_desc'] = $data['maintenance_desc'];
            $log_data['maintenance_created_by'] = $this->session->userdata('user_id');
            $log_data['log_time'] = date('Y-m-d H:i:s');
            if (count($image) > 0) {
                $log_data['log_attachement'] = 1;
            } else {
                $log_data['log_attachement'] = 0;
            }
            $maintenance_log_id = $this->Maintenance_model->insertId('maintenance_log', $log_data);

            if ($image) {
                foreach ($image as $key => $value) {
                    $image_data['image'] = $value;
                    $image_data['maintenance_id'] = $maintenance_id;
                    $image_data['property_id'] = $this->session->userdata('property_id');
                    $image_data['lease_id'] = $data['lease_id'];
                    $this->Maintenance_model->insert('maintenance_image', $image_data);

                    $image_log_data['log_image'] = $value;
                    $image_log_data['maintenance_log_id'] = $maintenance_log_id;
                    $image_log_data['property_id'] = $this->session->userdata('property_id');
                    $image_log_data['lease_id'] = $data['lease_id'];
                    $this->Maintenance_model->insert('maintenance_log_image', $image_log_data);
                }
            }
            if ($maintenance_id) {
                $this->sendMaintenanceCreateEmail($maintenance_id);
                redirect('Allmaintenance/' . $property_id);
            } else {

                $this->session->set_flashdata('errorMessage', 'Something May Wrong!!');
                redirect('add_new_maintenance');
            }
        }
    }

    private function joinNameParts($fname, $lname)
    {
        return $fname . ' ' . $lname;
    }

    public function sendMaintenanceCreateEmail($maintenance_id)
    {
        $site_name = $this->config->item('site_name');
        $base = $this->config->base_url();
        $img_src = "{$base}assets/img/logo.png";
        $img = "<img src='$img_src'><br>";

        $maintenance = $this->Maintenance_model->getMaintenance($maintenance_id);
        if (empty($maintenance)) {
            exit;
        }

        $dollar_sign = "$";
        $property = $this->utility_model->getProperty($maintenance['property_id']);
        $active_lease = $this->utility_model->getLastActiveLeaseOfProperty($maintenance['property_id']);
        $property_with_landlord = $this->utility_model->getPropertyWithLandlord($maintenance['property_id']);
        $tenants_with_lease_detail = $this->utility_model->getTenantsWithLeaseDetails($active_lease['lease_id']);

        $full_property_address = $this->utility_model->getFullPropertyAddress($maintenance['property_id']);

        $tenant_names = "";
        if (!empty($tenants_with_lease_detail)) {
            $user_fname = array_column($tenants_with_lease_detail, 'user_fname');
            $user_lname = array_column($tenants_with_lease_detail, 'user_lname');
            $tenant_names_array = (array_map("self::joinNameParts", $user_fname, $user_lname));
        }


        if (count($tenant_names_array) > 0) {
            $tenant_names = implode(' & ', $tenant_names_array);
        }

        $landlord_name = "";
        $landlord_phone = "";
        if (!empty($property_with_landlord)) {
            $landlord_name = $property_with_landlord['user_fname'] . ' ' . $property_with_landlord['user_lname'];
            $landlord_phone = $property_with_landlord['phone'];
        }

        $creation_date = date("jS F, Y", strtotime($maintenance['created_at']));
        $bill_status = "Not Paid";

        if (empty($property_with_landlord)) {
            exit;
        }

        $to = $property_with_landlord['email'];
        $mail_data['to'] = $to;
        $subject = "Maintenance order lodged - {$maintenance['maintenance_issue']}";

        $message = "Hi {$landlord_name} ,<br>
                        This email is to confirm you have successfully lodged a maintenance order for the following tenancy:<br>
                         --------------------------------- <br>
                         <b>Tenant(s):</b> $tenant_names<br> <br>   
                         <b>Address:</b> {$full_property_address} <br><br>                                              
                         <b>Details of the order:</b><br>
                         --------------------------------- <br>   
                         <b>Urgency:</b> {$maintenance['maintenance_urgency']}<br>   
                         <b>Issue:</b> {$maintenance['maintenance_issue']}<br>
                         <b>Details:</b> {$maintenance['maintenance_desc']}<br>
                         --------------------------------- <br><br>                         
                         This email has been generated by {$site_name} Software.<br><br>
                         {$img}
                         ";

        $mail_data['subject'] = $subject;
        $mail_data['message'] = $message;

        $this->sendEmail($mail_data);
    }

    private
    function sendEmail($mail_data)
    {
        $site_name = $this->config->item('site_name');
        $site_email = $this->config->item('site_email');

        try {

            //$mail_data['to'] = 'mahmud@sahajjo.com';
            $this->email->initialize(array('priority' => 1));
            $this->email->from($site_email, $site_name);
            $this->email->to($mail_data['to']);
            $this->email->bcc('mahmud@sahajjo.com');

            $this->email->subject($mail_data['subject']);
            $this->email->message($mail_data['message']);
            $this->email->set_mailtype("html");

            /*echo '<hr>' . '<br>';
            echo $mail_data['subject'] . '<br>';
            echo $mail_data['message'], '<br>';
            echo '<hr>' . '<br>';exit;*/

            @$this->email->send();

            /*$headers = 'From: '.$site_email.'\r\n';
            mail(mail_data['to'],$mail_data['subject'],$mail_data['message'],$headers);*/

        } catch (Exception $e) {
            // echo $e->getMessage();
        }

    }

    private function set_upload_options($file_name, $folder_name)
    {
        $url = base_url();

        $config = array();
        $config['file_name'] = $file_name;
        $config['upload_path'] = 'uploads/' . $folder_name;
        $config['allowed_types'] = 'jpg|png|gif|jpeg|xls|doc|docx|pdf|csv|mp3|mp4';
        $config['max_size'] = 0;
        $config['overwrite'] = TRUE;
        return $config;
    }

    public function view_all_maintenance($property_id)
    {

        $property_idd = $property_id;
        $lease_id = 0;
        $lease = $this->Maintenance_model->getActiveLease($property_id);
        //-------------------------------------------------------------------------
        $lease_id = !empty($lease) ? $lease['lease_id'] : 0;
        $is_archived = $this->utility_model->isArchived();
        $archived_string = $this->utility_model->getArchivedString();
        if ($is_archived) {
            $lease_id = $this->utility_model->getArchivedLeaseId();
            $lease = $this->utility_model->getLease($lease_id);
        }
        $data['is_archived'] = $is_archived;
        $data['archived_string'] = $archived_string;
        //-------------------------------------------------------------------------

        $lease = $this->Maintenance_model->select_with_where('lease_name', "property_id={$property_idd} AND lease_id={$lease_id}", 'lease');
        $lease_name = $lease[0]['lease_name'];
        $this->session->set_userdata('lease_name', $lease_name);
        $data['property_id'] = $property_idd;

        $data['result'] = $this->Maintenance_model->all_maintenance_by_lease($property_idd, $lease_id);
        $data['result2'] = $this->Maintenance_model->select_with_where('*', "property_id={$property_idd} AND lease_id={$lease_id}", 'maintenance_image');


        $data['new'] = $this->Maintenance_model->maintenance_todo_with_lease($property_idd, $lease_id, '1');
        $data['open'] = $this->Maintenance_model->maintenance_todo_with_lease($property_idd, $lease_id, '2');
        $data['todo'] = $this->Maintenance_model->maintenance_todo_with_lease($property_idd, $lease_id, '3');
        $data['closed'] = $this->Maintenance_model->maintenance_todo_with_lease($property_idd, $lease_id, '4');
        $data['archive'] = $this->Maintenance_model->maintenance_todo_with_lease($property_idd, $lease_id, '5');
        $this->load->view('view_all_maintenance', $data);
    }

    public function edit_maintenance($property_id, $maintenance_id)
    {

        $id = $maintenance_id;
        $data['property_id'] = $property_id;
        $data['result'] = $this->Maintenance_model->view_maintenance($id);

        $data['maintenance_log'] = $this->Maintenance_model->get_maintance_log_info($data['result'][0]['maintenance_id']);

        foreach ($data['maintenance_log'] as $key => $val) {
            $data['maintenance_log_image'][] = $this->Maintenance_model->select_with_where('*', 'maintenance_log_id=' . $data['maintenance_log'][$key]['maintenance_log_id'] . '', 'maintenance_log_image');
        }
        //-----------------------------------
        $is_archived = $this->utility_model->isArchived();
        $archived_string = $this->utility_model->getArchivedString();
        $data['is_archived'] = $is_archived;
        $data['archived_string'] = $archived_string;
        //-----------------------------------
        $data['result2'] = $this->Maintenance_model->view_maintenance_comment($id);
        $data['result3'] = $this->Maintenance_model->view_maintenance_comment_file($id);
        $data['maintenance_id'] = $maintenance_id;
        // echo '<pre>'; print_r($data['result']); die;
        $this->load->view('edit_maintenance', $data);
    }

    public function update_maintenance()
    {

        date_default_timezone_set('Australia/Sydney');

        $id = $data['id'] = $this->input->post('maintenance_id');
        $data['maintenance_info'] = $this->Maintenance_model->select_with_where('*', "maintenance_id=" . $id . "", 'maintenance');

        $data['maintenance_status'] = $this->input->post('maintenance_status');
        $data['maintenance_urgency'] = $this->input->post('maintenance_urgency');
        $data['maintenance_issue'] = $this->input->post('maintenance_issue');
        $data['maintenance_location'] = $this->input->post('maintenance_location');
        $data['maintenance_desc'] = $this->input->post('maintenance_desc');
        $data['updated_by'] = $this->session->userdata('user_id');

        if ($data['maintenance_info'][0]['maintenance_status'] != $data['maintenance_status']) {
            $log_data['maintenance_status'] = $data['maintenance_status'];
        }
        if ($data['maintenance_info'][0]['maintenance_urgency'] != $data['maintenance_urgency']) {
            $log_data['maintenance_urgency'] = $data['maintenance_urgency'];
        }
        if ($data['maintenance_info'][0]['maintenance_issue'] != $data['maintenance_issue']) {
            $log_data['maintenance_issue'] = $data['maintenance_issue'];
        }
        if ($data['maintenance_info'][0]['maintenance_location'] != $data['maintenance_location']) {
            $log_data['maintenance_location'] = $data['maintenance_location'];
        }
        if ($data['maintenance_info'][0]['maintenance_desc'] != $data['maintenance_desc']) {
            $log_data['maintenance_desc'] = $data['maintenance_desc'];
        }
        $log_data['maintenance_created_by'] = $data['updated_by'];
        $log_data['maintenance_create_status'] = 2;
        $log_data['maintenance_id'] = $id;
        $log_data['log_time'] = date('Y-m-d H:i:s');


        if ($this->input->post('maintenance_comment')) {
            $data2['maintenance_id'] = $id;
            $data2['maintenance_comments'] = $this->input->post('maintenance_comment');

            $date = date('M d Y');
            $time = date('H:i a');
            $data2['created_date'] = $date;
            $data2['created_time'] = $time;
            $data2['comment_by'] = $this->session->userdata('user_fname');

            $log_data['log_comment'] = $data2['maintenance_comments'];
        }
        $image = $this->input->post('image');

        $data['updated_at'] = date('Y-m-d H:i:s');

        $result = $this->Maintenance_model->update_maintenance($data);
        if ($result) {
            $this->session->set_flashdata("update_success", "update_success");
        }

        if ($data2) {
            $result2 = $this->Maintenance_model->insert_maintenance_comment($data2);
        }
        if ($result2) {
            if ($image) {
                $log_data['log_attachement'] = 1;
            }
        }
        $maintenance_log_id = $this->Maintenance_model->insertId('maintenance_log', $log_data);
        if ($result2) {
            if ($image) {
                foreach ($image as $key => $value) {
                    $image_data['maintenance_comments_id'] = $result2;
                    $image_data['maintenance_id'] = $id;
                    $image_data['maintenance_comment_file'] = $value;
                    $this->Maintenance_model->insert('maintenance_comment_file', $image_data);

                    $image_log_data['log_image'] = $value;
                    $image_log_data['maintenance_log_id'] = $maintenance_log_id;
                    $image_log_data['property_id'] = $this->session->userdata('property_id');
                    $this->Maintenance_model->insert('maintenance_log_image', $image_log_data);
                }
            }
        }

        if ($result) {
            $property_id = $this->session->userdata('property_id');
            redirect('Editmaintenance/' . $this->input->post('property_id') . '/' . $this->input->post('maintenance_id'));
        }

    }

    private function set_upload_options_multiple()
    {
        //upload an image options
        $config = array();
        $config['upload_path'] = './uploads/maintenance_file/';
        $config['allowed_types'] = 'jpg|png|gif|jpeg|xls|doc|docx|pdf|csv|mp3|mp4';
        $config['max_size'] = '10000';
        $config['remove_spaces'] = TRUE;
        $config['encrypt_name'] = TRUE;
        $config['overwrite'] = FALSE;

        return $config;
    }

    public function multi()
    {
        $files = $_FILES;
        if (isset($_FILES['userfile']) && !empty($_FILES['userfile']['name'])) {
            $config['remove_spaces'] = TRUE;
            $config['encrypt_name'] = TRUE;
            $config['upload_path'] = 'uploads/maintenance_file';
            $config['allowed_types'] = 'jpg|png|gif|jpeg|xls|doc|docx|pdf|csv|avi|mp4|flv|mpg';
            $config['max_size'] = '0';
            $this->load->library('upload', $config);
            $this->upload->initialize($config);
            if (!$this->upload->do_upload('userfile')) {
                $status = 'error';
                $msg = $this->upload->display_errors('', '');
//                echo json_encode($msg);
            } else {
                $fileinfo = $this->upload->data();
                echo $fileinfo['file_name'];
            }
        }
    }


    public function maintenance_comment_file()
    {

        $files = $_FILES;
        if (isset($_FILES['userfile']) && !empty($_FILES['userfile']['name'])) {
            $config['remove_spaces'] = TRUE;
            $config['encrypt_name'] = TRUE;
            $config['upload_path'] = 'uploads/maintenance_file';
            $config['allowed_types'] = 'jpg|png|gif|jpeg|xls|doc|docx|pdf|csv|avi|mp4|flv|mpg';
            $config['max_size'] = '0';
            $this->load->library('upload', $config);
            $this->upload->initialize($config);
            if (!$this->upload->do_upload('userfile')) {
                $status = 'error';
                $msg = $this->upload->display_errors('', '');
                echo json_encode($msg);
            } else {
                $fileinfo = $this->upload->data();
                echo $fileinfo['file_name'];
            }
        }
    }


    public function add_maintenance_order_invoice($maintenance_id)
    {
        $data['property_id'] = 0;
        $data['maintenance'] = $this->Maintenance_model->getMaintenance($maintenance_id);
        if ($data['maintenance']) {
            $data['property_id'] = $data['maintenance']['property_id'];

            if ($data['maintenance']['maintenance_has_invoice'] == 1) {
                $error_maintenance_has_invoice = "This Maintenance Already Has an Invoice";
                $this->session->set_flashdata('error_maintenance_has_invoice', $error_maintenance_has_invoice);
                $this->session->set_flashdata('flash_maintenance_id', $maintenance_id);
                redirect('Allmaintenance/' . $data['maintenance']['property_id']);
            }

        }

        $data['maintenance_invoice'] = null;

        $data['which_form'] = 'add';
        $data['form_action'] = 'insertMaintenanceOrderInvoice';

        $data['maintenance_order_invoice'] = null;
        $data['water_invoice_logs'] = null;

        $this->load->view('maintenance_invoice_form', $data);
    }

    private function generateInvoiceNumber()
    {
        return mt_rand(100000, 999999) . date('YmdHisu');
    }

    public function insert_maintenance_order_invoice()
    {
        /*echo "<pre>";
        print_r($_POST);
        echo "</pre>";
        die();*/

        $maintenance_id = $this->input->post('maintenance_id');
        $property_id = $this->input->post('property_id');
        $maintence_invoice_data['maintenance_id'] = $maintenance_id;
        $maintence_invoice_data['maintenance_invoice_number'] = $this->generateInvoiceNumber();
        $maintence_invoice_data['maintenance_invoice_description'] = $this->input->post('maintenance_invoice_description');
        $maintence_invoice_data['maintenance_invoice_amount'] = $this->input->post('maintenance_invoice_amount');

        $maintence_invoice_data['maintenance_invoice_due_date'] = date('Y-m-d', strtotime($this->input->post('maintenance_invoice_due_date')));
        $maintence_invoice_data['maintenance_invoice_created_at'] = date('Y-m-d H:i:s');
        $maintence_invoice_data['maintenance_invoice_updated_at'] = date('Y-m-d H:i:s');

        $this->form_validation->set_rules('maintenance_invoice_description', 'Maintenance Invoice Description', 'required');
        $this->form_validation->set_rules('maintenance_invoice_amount', 'Maintenance Invoice Amount', 'required');
        $this->form_validation->set_rules('maintenance_invoice_due_date', 'Maintenance Invoice Due Date', 'required');


        if ($this->form_validation->run() == false) {
            $this->session->set_flashdata('validation_errors', validation_errors());
            redirect('Allmaintenance/' . $property_id);
        }

        $maintenance_invoice_id = $this->Maintenance_model->insert_ret('maintenance_invoice', $maintence_invoice_data);

        $maintenance_invoice_documents = $this->input->post('maintenance_invoice_documents');

        if ($maintenance_invoice_documents) {
            foreach ($maintenance_invoice_documents as $maintenance_invoice_document) {
                $u_data['maintenance_invoice_document'] = $maintenance_invoice_document;
                $this->db->where('maintenance_invoice_id', $maintenance_invoice_id);
                $this->db->update('maintenance_invoice', $u_data);
            }
        }

        $upd_data['maintenance_status'] = 4;
        $upd_data['maintenance_has_invoice'] = 1;

        $this->Maintenance_model->update_function('maintenance_id', $maintenance_id, 'maintenance', $upd_data);

        $this->session->set_flashdata('add_success', 'add_success');
        $this->session->set_flashdata('flash_maintenance_id', $maintenance_id);
        redirect('Allmaintenance/' . $property_id);

    }

    public function edit_maintenance_order_invoice($maintenance_id)
    {
        $data['property_id'] = 0;
        $data['maintenance'] = $this->Maintenance_model->getMaintenance($maintenance_id);
        if ($data['maintenance']) {
            $data['property_id'] = $data['maintenance']['property_id'];
        }

        $data['maintenance_invoice'] = $this->Maintenance_model->getMaintenanceInvoice($maintenance_id);

        $data['which_form'] = 'edit';
        $data['form_action'] = 'updateMaintenanceOrderInvoice';

        $data['maintenance_order_invoice'] = null;
        $data['water_invoice_logs'] = null;

        $this->load->view('maintenance_invoice_form', $data);
    }

    public function update_maintenance_order_invoice()
    {
        /*echo "<pre>";
        print_r($_POST);
        echo "</pre>";
        die();*/

        $maintenance_id = $this->input->post('maintenance_id');
        $property_id = $this->input->post('property_id');
        $maintence_invoice_data['maintenance_invoice_description'] = $this->input->post('maintenance_invoice_description');
        $maintence_invoice_data['maintenance_invoice_amount'] = $this->input->post('maintenance_invoice_amount');
        $maintence_invoice_data['maintenance_invoice_due_date'] = date('Y-m-d', strtotime($this->input->post('maintenance_invoice_due_date')));
        $maintence_invoice_data['maintenance_invoice_updated_at'] = date('Y-m-d H:i:s');

        $this->form_validation->set_rules('maintenance_invoice_description', 'Maintenance Invoice Description', 'required');
        $this->form_validation->set_rules('maintenance_invoice_amount', 'Maintenance Invoice Amount', 'required');
        $this->form_validation->set_rules('maintenance_invoice_due_date', 'Maintenance Invoice Due Date', 'required');


        if ($this->form_validation->run() == false) {
            $this->session->set_flashdata('validation_errors', validation_errors());
            redirect('Allmaintenance/' . $property_id);
        }

        $this->Maintenance_model->update_function('maintenance_id', $maintenance_id, 'maintenance_invoice', $maintence_invoice_data);

        $maintenance_invoice_documents = $this->input->post('maintenance_invoice_documents');

        if ($maintenance_invoice_documents) {
            foreach ($maintenance_invoice_documents as $maintenance_invoice_document) {
                $u_data['maintenance_invoice_document'] = $maintenance_invoice_document;
                $this->db->where('maintenance_id', $maintenance_id);
                $this->db->update('maintenance_invoice', $u_data);
            }
        }

        $this->session->set_flashdata('update_success', 'update_success');
        $this->session->set_flashdata('flash_maintenance_id', $maintenance_id);
        redirect('Allmaintenance/' . $property_id);

    }

    public function maintenance_invoice_document()
    {
        $files = $_FILES;
        if (isset($_FILES['userfile']) && !empty($_FILES['userfile']['name'])) {
            $config['remove_spaces'] = TRUE;
            $config['encrypt_name'] = TRUE;
            $config['upload_path'] = 'uploads/maintenance_invoice_document';
            $config['allowed_types'] = 'jpg|png|gif|jpeg|pdf|doc|docx|xls|xlsx';
            $config['max_size'] = '0';
            $this->load->library('upload', $config);
            $this->upload->initialize($config);
            if (!$this->upload->do_upload('userfile')) {
                $status = 'error';
                $msg = $this->upload->display_errors('', '');

            } else {
                $fileinfo = $this->upload->data();
                echo $fileinfo['file_name'];
            }
        }
    }


}
