<!DOCTYPE html>
<base href="<?php echo base_url();?>">
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Rent Simple</title>
    <link href="assets/css/bootstrap.min.css" rel="stylesheet">
    <link href="assets/css/style.css" rel="stylesheet">

<!--[if lt IE 9]>
<script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
<![endif]-->
</head>
<body>
    <div class="login">  
        <div class="container">
            <div class="row">
                <div class="main">
                    <div class="logo"> <img src="assets/img/logo.png"/></div>
                    <h3>Reset your password</h3>
                    <div class="row" id="forget_div">
                        <form role="form"  onsubmit="return check_confirm_pass()" action="forget/update_password" method="post">
                            
                            <?php $tmp_email= $this->session->userdata('email');
                            if($message_error_password!=NULL){ ?>
                            <div class="alert alert-danger">
                                <button class="close" data-close="alert"></button>
                                <span><?=$message_error_password;?></span>
                            </div>
                            <?php } ?>

                            <div class="alert alert-danger" id="err_confirm_div" style="display: none">
                                <strong id="err_confirm_msg"></strong> 
                            </div>
                            <div class="alert alert-danger" id="err_both_confirm_div" style="display: none">
                                <strong id="err_both_confirm_msg"></strong> 
                            </div>
                            <div class="form-group">
                                <label for="inputEmail">Password</label>
                                <input type="password" name="password" class="form-control" id="inputpassword">
                            </div>
                            <div class="form-group">
                                <label for="inputEmail">Confirm Password</label>
                                <input type="password" name="confirm_password" class="form-control" id="inputconfirmpassword">
                            </div>
                            <div class="text-center"><input type="submit" class="btn btn-primary"></div>
                        </form>
                    </div>
                </div>

            </div>
        </div>
    </div>


    <script>
        function check_confirm_pass() 
        {
            $("#err_confirm_div").hide();
            $("#err_both_confirm_div").hide();

            var ok=true;
            var inputpasswords= $("#inputpassword").val();
            var inputconfirmpasswords= $("#inputconfirmpassword").val();
            if(inputpasswords =='' || inputconfirmpasswords=='')
            {
                $("#err_confirm_div").show();
                $("#err_confirm_msg").html('* Email fields must be filled out');
                ok=false;
            }
            else
            {
                $("#err_confirm_div").hide();
            }
            if(inputpasswords != inputconfirmpasswords)
            {
                $("#err_both_confirm_div").show();
                $("#err_both_confirm_msg").html('* Both Password field doesn\'t match');
                ok=false;
            }
            else
            {
                $("#err_both_confirm_div").hide();
            }
            return ok;
        }
    </script>

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <script src="assets/js/bootstrap.min.js"></script>
    <!-- <script src="assets/js/custom.js"></script> -->
</body>
</html>
